from emporium import models


def test_password_hash_saved():
    user = models.User(first_name='Jack', last_name='Black', role='emperor')
    assert user.password is None

    user.set_password('secret')
    assert user.password is not None

def test_password_hash_not_set():
    user = models.User(first_name='Jack', last_name='Black', role='emperor')
    assert not user.check_password('secret')

def test_correct_password():
    user = models.User(first_name='Jack', last_name='Black', role='emperor')
    user.set_password('secret')
    assert user.check_password('secret')

def test_incorrect_password():
    user = models.User(first_name='Jack', last_name='Black', role='emperor')
    user.set_password('secret')
    assert not user.check_password('incorrect')
