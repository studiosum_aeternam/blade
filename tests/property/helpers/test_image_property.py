# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.image
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(asset_name=st.text(), asset_type=st.just("img"))
def test_fuzz_CommonHelpers_asset_path(asset_name, asset_type):
    emporium.helpers.image.CommonHelpers.asset_path(
        asset_name=asset_name, asset_type=asset_type
    )


@given(word=st.nothing())
def test_fuzz_CommonHelpers_capword(word):
    emporium.helpers.image.CommonHelpers.capword(word=word)


@given(number=st.one_of(st.integers(), st.floats()))
def test_fuzz_CommonHelpers_decimal_with_two_places(number):
    emporium.helpers.image.CommonHelpers.decimal_with_two_places(number=number)


@given(request=st.nothing(), item=st.nothing())
def test_fuzz_CommonHelpers_delete_failure(request, item):
    emporium.helpers.image.CommonHelpers.delete_failure(request=request, item=item)


@given(image_list=st.nothing(), image_types=st.nothing())
def test_fuzz_CommonHelpers_delete_image(image_list, image_types):
    emporium.helpers.image.CommonHelpers.delete_image(
        image_list=image_list, image_types=image_types
    )


@given(date=st.nothing())
def test_fuzz_CommonHelpers_format_date(date):
    emporium.helpers.image.CommonHelpers.format_date(date=date)


@given(request=st.nothing(), availability_attributes=st.nothing())
def test_fuzz_CommonHelpers_initialize_filter_params(request, availability_attributes):
    emporium.helpers.image.CommonHelpers.initialize_filter_params(
        request=request, availability_attributes=availability_attributes
    )


@given(value=st.nothing(), sex=st.nothing())
def test_fuzz_CommonHelpers_plural_pl(value, sex):
    emporium.helpers.image.CommonHelpers.plural_pl(value=value, sex=sex)


@given(price=st.nothing(), tax=st.nothing())
def test_fuzz_CommonHelpers_price_gross_number(price, tax):
    emporium.helpers.image.CommonHelpers.price_gross_number(price=price, tax=tax)


@given(price=st.nothing(), tax_value=st.nothing())
def test_fuzz_CommonHelpers_price_gross_replacement(price, tax_value):
    emporium.helpers.image.CommonHelpers.price_gross_replacement(
        price=price, tax_value=tax_value
    )


@given(price=st.nothing(), tax_value=st.nothing())
def test_fuzz_CommonHelpers_price_net(price, tax_value):
    emporium.helpers.image.CommonHelpers.price_net(price=price, tax_value=tax_value)


@given(price=st.nothing())
def test_fuzz_CommonHelpers_price_precision(price):
    emporium.helpers.image.CommonHelpers.price_precision(price=price)


@given(price=st.nothing(), tax=st.nothing())
def test_fuzz_CommonHelpers_price_tax(price, tax):
    emporium.helpers.image.CommonHelpers.price_tax(price=price, tax=tax)


@given(number=st.one_of(st.integers(), st.floats()))
def test_fuzz_CommonHelpers_quantize_number(number):
    emporium.helpers.image.CommonHelpers.quantize_number(number=number)


@given(text=st.text())
def test_fuzz_CommonHelpers_tag_cleaner(text):
    emporium.helpers.image.CommonHelpers.tag_cleaner(text=text)


@given(filter_params=st.builds(list), query_items=st.builds(list))
def test_fuzz_CommonHelpers_term_filter_format(filter_params, query_items):
    emporium.helpers.image.CommonHelpers.term_filter_format(
        filter_params=filter_params, query_items=query_items
    )


@given(filter_params=st.nothing())
def test_fuzz_CommonHelpers_term_search(filter_params):
    emporium.helpers.image.CommonHelpers.term_search(filter_params=filter_params)


@given(args=st.nothing())
def test_fuzz_ImageHelpers_image_banner(args):
    emporium.helpers.image.ImageHelpers.image_banner(args=args)


@given(args=st.nothing())
def test_fuzz_ImageHelpers_image_convertjpg(args):
    emporium.helpers.image.ImageHelpers.image_convertjpg(args=args)


@given(image_pil=st.nothing(), width=st.integers(min_value=0), height=st.nothing())
def test_fuzz_ImageHelpers_image_resize(image_pil, width, height):
    emporium.helpers.image.ImageHelpers.image_resize(
        image_pil=image_pil, width=width, height=height
    )


@given(image=st.nothing(), full=st.booleans(), base=st.booleans())
def test_fuzz_ImageHelpers_image_resolver(image, full, base):
    emporium.helpers.image.ImageHelpers.image_resolver(
        image=image, full=full, base=base
    )


@given(
    file=st.nothing(),
    file_ext=st.nothing(),
    destination=st.nothing(),
    img_type=st.nothing(),
)
def test_fuzz_ImageHelpers_image_save(file, file_ext, destination, img_type):
    emporium.helpers.image.ImageHelpers.image_save(
        file=file, file_ext=file_ext, destination=destination, img_type=img_type
    )


@given(args=st.nothing())
def test_fuzz_ImageHelpers_image_thumbnail(args):
    emporium.helpers.image.ImageHelpers.image_thumbnail(args=args)


@given(images=st.nothing())
def test_fuzz_ImageHelpers_images_format(images):
    emporium.helpers.image.ImageHelpers.images_format(images=images)


@given(im=st.nothing())
def test_fuzz_ImageHelpers_set_rgb_color(im):
    emporium.helpers.image.ImageHelpers.set_rgb_color(im=im)


@given(name=st.text(), size=st.integers(min_value=0))
def test_fuzz_Sizes(name, size):
    emporium.helpers.image.Sizes(name=name, size=size)

