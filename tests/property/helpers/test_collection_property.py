# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.collection
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(
    home=st.nothing(),
    url_listing=st.nothing(),
    category=st.nothing(),
    collection_list=st.nothing(),
)
def test_fuzz_CollectionHelpers_add_collection_list_to_urls_container(
    home, url_listing, category, collection_list
):
    emporium.helpers.collection.CollectionHelpers.add_collection_list_to_urls_container(
        home=home,
        url_listing=url_listing,
        category=category,
        collection_list=collection_list,
    )


@given(collection_list=st.builds(list), manufacturer_list=st.builds(list))
def test_fuzz_CollectionHelpers_admin_collections_format(
    collection_list, manufacturer_list
):
    emporium.helpers.collection.CollectionHelpers.admin_collections_format(
        collection_list=collection_list, manufacturer_list=manufacturer_list
    )


@given(
    collection=st.text(),
    collection_list=st.builds(list),
    query_items=st.builds(list),
    manufacturer=st.booleans(),
)
def test_fuzz_CollectionHelpers_collections_filter_format(
    collection, collection_list, query_items, manufacturer
):
    emporium.helpers.collection.CollectionHelpers.collections_filter_format(
        collection=collection,
        collection_list=collection_list,
        query_items=query_items,
        manufacturer=manufacturer,
    )


@given(collection_list=st.builds(list))
def test_fuzz_CollectionHelpers_collections_related_format(collection_list):
    emporium.helpers.collection.CollectionHelpers.collections_related_format(
        collection_list=collection_list
    )

