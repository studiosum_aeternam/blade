# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.order
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(request=st.nothing(), session=st.nothing())
def test_fuzz_OrderHelpers_remove_order_cookies(request, session):
    emporium.helpers.order.OrderHelpers.remove_order_cookies(
        request=request, session=session
    )


@given(request=st.nothing(), session=st.nothing())
def test_fuzz_OrderHelpers_remove_session_order_variables(request, session):
    emporium.helpers.order.OrderHelpers.remove_session_order_variables(
        request=request, session=session
    )

