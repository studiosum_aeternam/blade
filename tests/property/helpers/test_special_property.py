# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.special
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(
    special_list=st.nothing(),
    special_products=st.nothing(),
    number_elements=st.nothing(),
)
def test_fuzz_SpecialHelpers_special_products(
    special_list, special_products, number_elements
):
    emporium.helpers.special.SpecialHelpers.special_products(
        special_list=special_list,
        special_products=special_products,
        number_elements=number_elements,
    )


@given(special_list=st.nothing())
def test_fuzz_SpecialHelpers_specials_format(special_list):
    emporium.helpers.special.SpecialHelpers.specials_format(special_list=special_list)

