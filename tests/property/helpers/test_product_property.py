# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.product
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(products_list=st.builds(list), url_base=st.text())
def test_fuzz_ProductHelpers_add_products_to_urls_container(products_list, url_base):
    emporium.helpers.product.ProductHelpers.add_products_to_urls_container(
        products_list=products_list, url_base=url_base
    )


@given(images=st.nothing())
def test_fuzz_ProductHelpers_additional_images_format(images):
    emporium.helpers.product.ProductHelpers.additional_images_format(images=images)


@given(product_list=st.builds(list))
def test_fuzz_ProductHelpers_create_product_dict_key_ean(product_list):
    emporium.helpers.product.ProductHelpers.create_product_dict_key_ean(
        product_list=product_list
    )


@given(special_price=st.nothing())
def test_fuzz_ProductHelpers_create_special_price_dict_key_id(special_price):
    emporium.helpers.product.ProductHelpers.create_special_price_dict_key_id(
        special_price=special_price
    )


@given(
    dimension_name=st.text(), dimension_value=st.floats(), query_items=st.builds(list)
)
def test_fuzz_ProductHelpers_dimension_filter_format(
    dimension_name, dimension_value, query_items
):
    emporium.helpers.product.ProductHelpers.dimension_filter_format(
        dimension_name=dimension_name,
        dimension_value=dimension_value,
        query_items=query_items,
    )


@given(featured_product=st.nothing(), tax_value=st.nothing())
def test_fuzz_ProductHelpers_featured_products_format_dict(featured_product, tax_value):
    emporium.helpers.product.ProductHelpers.featured_products_format_dict(
        featured_product=featured_product, tax_value=tax_value
    )


@given(filter_params=st.builds(dict), query_items=st.builds(list))
def test_fuzz_ProductHelpers_filter_params_dimensions(filter_params, query_items):
    emporium.helpers.product.ProductHelpers.filter_params_dimensions(
        filter_params=filter_params, query_items=query_items
    )


@given(updated_products=st.builds(dict), product_dict=st.builds(dict))
def test_fuzz_ProductHelpers_merge_product_dicts(updated_products, product_dict):
    emporium.helpers.product.ProductHelpers.merge_product_dicts(
        updated_products=updated_products, product_dict=product_dict
    )


@given(
    collection_dict=st.nothing(),
    products_from_collections=st.nothing(),
    product_id=st.nothing(),
)
def test_fuzz_ProductHelpers_other_products_from_common_collections(
    collection_dict, products_from_collections, product_id
):
    emporium.helpers.product.ProductHelpers.other_products_from_common_collections(
        collection_dict=collection_dict,
        products_from_collections=products_from_collections,
        product_id=product_id,
    )


@given(filter_params=st.builds(list), query_items=st.builds(list))
def test_fuzz_ProductHelpers_price_filter_format(filter_params, query_items):
    emporium.helpers.product.ProductHelpers.price_filter_format(
        filter_params=filter_params, query_items=query_items
    )


@given(product_band_list=st.nothing())
def test_fuzz_ProductHelpers_product_band_format(product_band_list):
    emporium.helpers.product.ProductHelpers.product_band_format(
        product_band_list=product_band_list
    )


@given(
    data_options=st.nothing(),
    data_options_filtered=st.nothing(),
    product_list=st.nothing(),
    settings=st.nothing(),
)
def test_fuzz_ProductHelpers_product_category_format(
    data_options, data_options_filtered, product_list, settings
):
    emporium.helpers.product.ProductHelpers.product_category_format(
        data_options=data_options,
        data_options_filtered=data_options_filtered,
        product_list=product_list,
        settings=settings,
    )


@given(product_collection_list=st.nothing())
def test_fuzz_ProductHelpers_product_collection_format(product_collection_list):
    emporium.helpers.product.ProductHelpers.product_collection_format(
        product_collection_list=product_collection_list
    )


@given(
    data_options=st.nothing(),
    data_options_filtered=st.nothing(),
    product_list=st.nothing(),
    settings=st.nothing(),
)
def test_fuzz_ProductHelpers_product_format_cart_dict(
    data_options, data_options_filtered, product_list, settings
):
    emporium.helpers.product.ProductHelpers.product_format_cart_dict(
        data_options=data_options,
        data_options_filtered=data_options_filtered,
        product_list=product_list,
        settings=settings,
    )


@given(item=st.nothing())
def test_fuzz_ProductHelpers_product_json_format(item):
    emporium.helpers.product.ProductHelpers.product_json_format(item=item)


@given(product_list=st.builds(list))
def test_fuzz_ProductHelpers_product_list_filter_manual_prices(product_list):
    emporium.helpers.product.ProductHelpers.product_list_filter_manual_prices(
        product_list=product_list
    )


@given(product_storage_list=st.nothing())
def test_fuzz_ProductHelpers_product_storage_format(product_storage_list):
    emporium.helpers.product.ProductHelpers.product_storage_format(
        product_storage_list=product_storage_list
    )


@given(authors=st.nothing())
def test_fuzz_ProductHelpers_product_to_author_format(authors):
    emporium.helpers.product.ProductHelpers.product_to_author_format(authors=authors)


@given(product_files_id_list=st.nothing(), product_files_passed=st.nothing())
def test_fuzz_ProductHelpers_product_to_file_generate_delete_list(
    product_files_id_list, product_files_passed
):
    emporium.helpers.product.ProductHelpers.product_to_file_generate_delete_list(
        product_files_id_list=product_files_id_list,
        product_files_passed=product_files_passed,
    )


@given(product_images_id_list=st.nothing(), product_images_passed=st.nothing())
def test_fuzz_ProductHelpers_product_to_image_generate_delete_list(
    product_images_id_list, product_images_passed
):
    emporium.helpers.product.ProductHelpers.product_to_image_generate_delete_list(
        product_images_id_list=product_images_id_list,
        product_images_passed=product_images_passed,
    )


@given(products=st.nothing())
def test_fuzz_ProductHelpers_product_to_product_combined_format(products):
    emporium.helpers.product.ProductHelpers.product_to_product_combined_format(
        products=products
    )


@given(products=st.nothing())
def test_fuzz_ProductHelpers_product_to_product_format(products):
    emporium.helpers.product.ProductHelpers.product_to_product_format(products=products)


@given(product_to_relation_old=st.nothing(), product_to_relation_passed=st.nothing())
def test_fuzz_ProductHelpers_product_to_relation_generate_delete_list(
    product_to_relation_old, product_to_relation_passed
):
    emporium.helpers.product.ProductHelpers.product_to_relation_generate_delete_list(
        product_to_relation_old=product_to_relation_old,
        product_to_relation_passed=product_to_relation_passed,
    )


@given(items=st.nothing())
def test_fuzz_ProductHelpers_shuffle_items(items):
    emporium.helpers.product.ProductHelpers.shuffle_items(items=items)


@given(item=st.nothing(), data_options=st.nothing())
def test_fuzz_ProductHelpers_single_product_format(item, data_options):
    emporium.helpers.product.ProductHelpers.single_product_format(
        item=item, data_options=data_options
    )

