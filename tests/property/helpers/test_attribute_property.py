# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.attribute
from hypothesis import given, strategies as st


@given(
    attribute_group_list=st.builds(list),
    attribute_list=st.builds(list),
    attributes=st.builds(set),
    query_items=st.builds(list),
)
def test_fuzz_AttributeHelpers_attributes_filter_format(
    attribute_group_list, attribute_list, attributes, query_items
):
    emporium.helpers.attribute.AttributeHelpers.attributes_filter_format(
        attribute_group_list=attribute_group_list,
        attribute_list=attribute_list,
        attributes=attributes,
        query_items=query_items,
    )


@given(attributes=st.builds(dict))
def test_fuzz_AttributeHelpers_create_attribute_dict_id_value(attributes):
    emporium.helpers.attribute.AttributeHelpers.create_attribute_dict_id_value(
        attributes=attributes
    )


@given(attributes=st.builds(dict))
def test_fuzz_AttributeHelpers_create_attribute_dict_type_example_groups(attributes):
    emporium.helpers.attribute.AttributeHelpers.create_attribute_dict_type_example_groups(
        attributes=attributes
    )
