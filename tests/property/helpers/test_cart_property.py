# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

from emporium import data_containers, helpers
from hypothesis import given, HealthCheck, settings, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(cart_contents=st.builds(dict), product_list=st.builds(dict))
def test_fuzz_CartHelpers_cart_contents_get_current_product_values(
    cart_contents, product_list
):
    helpers.cart.CartHelpers.cart_contents_get_current_product_values(
        cart_contents=cart_contents, product_list=product_list
    )


@given(
    cart_products=st.builds(list),
    product_list=st.builds(dict),
    featured_product_dict=st.builds(dict),
    tax_dict=st.builds(dict),
)
@settings(suppress_health_check=(HealthCheck.function_scoped_fixture,))
def test_fuzz_CartHelpers_cart_parameters(
    dummy_request, cart_products, product_list, featured_product_dict, tax_dict
):
    helpers.cart.CartHelpers.cart_parameters(
        request=dummy_request,
        cart_products=cart_products,
        product_list=product_list,
        featured_product_dict=featured_product_dict,
        tax_dict=tax_dict,
    )


@given(session_cart=st.builds(dict))
@settings(suppress_health_check=(HealthCheck.function_scoped_fixture,))
def test_fuzz_CartHelpers_remove_cart_from_db(dummy_request, session_cart):
    helpers.cart.CartHelpers.remove_cart_from_db(
        request=dummy_request, session_cart=session_cart
    )


@given(cart=st.builds(dict))
@settings(suppress_health_check=(HealthCheck.function_scoped_fixture,))
def test_fuzz_CartHelpers_remove_cart_from_db_and_session(dummy_request, cart):
    helpers.cart.CartHelpers.remove_cart_from_db_and_session(
        request=dummy_request, cart=cart
    )


@given()
@settings(suppress_health_check=(HealthCheck.function_scoped_fixture,))
def test_fuzz_CartHelpers_remove_cart_variables_from_session(dummy_request):
    helpers.cart.CartHelpers.remove_cart_variables_from_session(request=dummy_request)


@given(params=st.nothing(), all_products_free=st.nothing())
def test_fuzz_CartHelpers_update_delivery_methods(params, all_products_free):
    helpers.cart.CartHelpers.update_delivery_methods(
        params=params, all_products_free=all_products_free
    )
