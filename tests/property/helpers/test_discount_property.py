# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.discount
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(discounts=st.nothing())
def test_fuzz_DiscountHelpers_create_discount_dict_key_id(discounts):
    emporium.helpers.discount.DiscountHelpers.create_discount_dict_key_id(
        discounts=discounts
    )


@given(discounts=st.nothing())
def test_fuzz_DiscountHelpers_discounts_format(discounts):
    emporium.helpers.discount.DiscountHelpers.discounts_format(discounts=discounts)

