# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.transport
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with an appropriate strategy


@given(transports=st.nothing())
def test_fuzz_TransportHelpers_create_transport_dict_key_id(transports):
    emporium.helpers.transport.TransportHelpers.create_transport_dict_key_id(
        transports=transports
    )

