# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.band
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with an appropriate strategy


@given(band=st.text(), band_list=st.builds(list), query_items=st.builds(list))
def test_fuzz_BandHelpers_bands_filter_format(band, band_list, query_items):
    emporium.helpers.band.BandHelpers.bands_filter_format(
        band=band, band_list=band_list, query_items=query_items
    )


@given(bands=st.builds(dict))
def test_fuzz_BandHelpers_bands_format(bands):
    emporium.helpers.band.BandHelpers.bands_format(bands=bands)
