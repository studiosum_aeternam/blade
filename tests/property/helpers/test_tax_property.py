# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.tax
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with an appropriate strategy


@given(taxes=st.nothing())
def test_fuzz_TaxHelpers_create_tax_dict_key_id(taxes):
    emporium.helpers.tax.TaxHelpers.create_tax_dict_key_id(taxes=taxes)

