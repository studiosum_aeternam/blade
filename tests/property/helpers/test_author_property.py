# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.author
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(authors=st.builds(dict))
def test_fuzz_AuthorHelpers_authors_format(authors):
    emporium.helpers.author.AuthorHelpers.authors_format(authors=authors)


@given(authors_passed=st.builds(dict))
def test_fuzz_AuthorHelpers_product_author_generate_create_list(authors_passed):
    emporium.helpers.author.AuthorHelpers.product_author_generate_create_list(
        authors_passed=authors_passed
    )


@given(authors_passed=st.builds(dict))
def test_fuzz_AuthorHelpers_product_author_generate_edit_list(authors_passed):
    emporium.helpers.author.AuthorHelpers.product_author_generate_edit_list(
        authors_passed=authors_passed
    )
