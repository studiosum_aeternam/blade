# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.storage
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(storages=st.nothing())
def test_fuzz_StorageHelpers_create_storage_dict_key_id(storages):
    emporium.helpers.storage.StorageHelpers.create_storage_dict_key_id(
        storages=storages
    )


@given(storage=st.text(), storage_list=st.builds(list), query_items=st.builds(list))
def test_fuzz_StorageHelpers_storages_filter_format(storage, storage_list, query_items):
    emporium.helpers.storage.StorageHelpers.storages_filter_format(
        storage=storage, storage_list=storage_list, query_items=query_items
    )


@given(storages=st.nothing())
def test_fuzz_StorageHelpers_storages_format(storages):
    emporium.helpers.storage.StorageHelpers.storages_format(storages=storages)

