# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.page
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(home=st.nothing(), url_listing=st.nothing(), pages=st.nothing())
def test_fuzz_PageHelpers_add_pages_to_urls_container(home, url_listing, pages):
    emporium.helpers.page.PageHelpers.add_pages_to_urls_container(
        home=home, url_listing=url_listing, pages=pages
    )

