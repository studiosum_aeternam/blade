# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.category
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(category_list=st.nothing())
def test_fuzz_CategoryHelpers_build_category_tree(category_list):
    emporium.helpers.category.CategoryHelpers.build_category_tree(
        category_list=category_list
    )


@given(category_list=st.nothing())
def test_fuzz_CategoryHelpers_categories_filter_format(category_list):
    emporium.helpers.category.CategoryHelpers.categories_filter_format(
        category_list=category_list
    )


@given(categories=st.nothing())
def test_fuzz_CategoryHelpers_categories_format(categories):
    emporium.helpers.category.CategoryHelpers.categories_format(categories=categories)


@given(main_tree=st.nothing(), category_list=st.nothing())
def test_fuzz_CategoryHelpers_traverse_category_tree(main_tree, category_list):
    emporium.helpers.category.CategoryHelpers.traverse_category_tree(
        main_tree=main_tree, category_list=category_list
    )

