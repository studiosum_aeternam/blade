# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.manufacturer
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with an appropriate strategy


@given(manufacturers=st.nothing())
def test_fuzz_ManufacturerHelpers_create_manufacturers_dict_key_id(manufacturers):
    emporium.helpers.manufacturer.ManufacturerHelpers.create_manufacturers_dict_key_id(
        manufacturers=manufacturers
    )


@given(
    manufacturer=st.text(),
    manufacturer_list=st.builds(list),
    query_items=st.builds(list),
)
def test_fuzz_ManufacturerHelpers_manufacturers_filter_format(
    manufacturer, manufacturer_list, query_items
):
    emporium.helpers.manufacturer.ManufacturerHelpers.manufacturers_filter_format(
        manufacturer=manufacturer,
        manufacturer_list=manufacturer_list,
        query_items=query_items,
    )

