# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.post
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(home=st.nothing(), url_listing=st.nothing(), posts=st.nothing())
def test_fuzz_PostHelpers_add_posts_to_urls_container(home, url_listing, posts):
    emporium.helpers.post.PostHelpers.add_posts_to_urls_container(
        home=home, url_listing=url_listing, posts=posts
    )

