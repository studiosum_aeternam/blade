# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.common
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(asset_name=st.text(), asset_type=st.just("img"))
def test_fuzz_CommonHelpers_asset_path(asset_name, asset_type):
    emporium.helpers.common.CommonHelpers.asset_path(
        asset_name=asset_name, asset_type=asset_type
    )


@given(word=st.nothing())
def test_fuzz_CommonHelpers_capword(word):
    emporium.helpers.common.CommonHelpers.capword(word=word)


@given(number=st.one_of(st.integers(), st.floats()))
def test_fuzz_CommonHelpers_decimal_with_two_places(number):
    emporium.helpers.common.CommonHelpers.decimal_with_two_places(number=number)


@given(request=st.nothing(), item=st.nothing())
def test_fuzz_CommonHelpers_delete_failure(request, item):
    emporium.helpers.common.CommonHelpers.delete_failure(request=request, item=item)


@given(image_list=st.nothing(), image_types=st.nothing())
def test_fuzz_CommonHelpers_delete_image(image_list, image_types):
    emporium.helpers.common.CommonHelpers.delete_image(
        image_list=image_list, image_types=image_types
    )


@given(date=st.nothing())
def test_fuzz_CommonHelpers_format_date(date):
    emporium.helpers.common.CommonHelpers.format_date(date=date)


@given(request=st.nothing(), availability_attributes=st.nothing())
def test_fuzz_CommonHelpers_initialize_filter_params(request, availability_attributes):
    emporium.helpers.common.CommonHelpers.initialize_filter_params(
        request=request, availability_attributes=availability_attributes
    )


@given(value=st.nothing(), sex=st.nothing())
def test_fuzz_CommonHelpers_plural_pl(value, sex):
    emporium.helpers.common.CommonHelpers.plural_pl(value=value, sex=sex)


@given(price=st.nothing(), tax=st.nothing())
def test_fuzz_CommonHelpers_price_gross_number(price, tax):
    emporium.helpers.common.CommonHelpers.price_gross_number(price=price, tax=tax)


@given(price=st.nothing(), tax_value=st.nothing())
def test_fuzz_CommonHelpers_price_gross_replacement(price, tax_value):
    emporium.helpers.common.CommonHelpers.price_gross_replacement(
        price=price, tax_value=tax_value
    )


@given(price=st.nothing(), tax_value=st.nothing())
def test_fuzz_CommonHelpers_price_net(price, tax_value):
    emporium.helpers.common.CommonHelpers.price_net(price=price, tax_value=tax_value)


@given(price=st.nothing())
def test_fuzz_CommonHelpers_price_precision(price):
    emporium.helpers.common.CommonHelpers.price_precision(price=price)


@given(price=st.nothing(), tax=st.nothing())
def test_fuzz_CommonHelpers_price_tax(price, tax):
    emporium.helpers.common.CommonHelpers.price_tax(price=price, tax=tax)


@given(number=st.one_of(st.integers(), st.floats()))
def test_fuzz_CommonHelpers_quantize_number(number):
    emporium.helpers.common.CommonHelpers.quantize_number(number=number)


@given(text=st.text())
def test_fuzz_CommonHelpers_tag_cleaner(text):
    emporium.helpers.common.CommonHelpers.tag_cleaner(text=text)


@given(filter_params=st.builds(list), query_items=st.builds(list))
def test_fuzz_CommonHelpers_term_filter_format(filter_params, query_items):
    emporium.helpers.common.CommonHelpers.term_filter_format(
        filter_params=filter_params, query_items=query_items
    )


@given(filter_params=st.nothing())
def test_fuzz_CommonHelpers_term_search(filter_params):
    emporium.helpers.common.CommonHelpers.term_search(filter_params=filter_params)


@given(wrapped=st.nothing())
def test_fuzz_log_timer(wrapped):
    emporium.helpers.common.log_timer(wrapped=wrapped)

