# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.
import typing
from hypothesis import given, strategies as st

import emporium.helpers.banner

# TODO: replace st.nothing() with an appropriate strategy


@given(banners=st.builds(list))
def test_fuzz_BannerHelpers_banners_format(banners):
    emporium.helpers.banner.BannerHelpers.banners_format(banners=banners)
