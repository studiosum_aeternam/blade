# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.helpers.setting
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(xsettings=st.nothing())
def test_fuzz_SettingHelpers_admin_settings(xsettings):
    emporium.helpers.setting.SettingHelpers.admin_settings(xsettings=xsettings)


@given(request=st.nothing())
def test_fuzz_SettingHelpers_default_options(request):
    emporium.helpers.setting.SettingHelpers.default_options(request=request)


@given(xsettings=st.nothing())
def test_fuzz_SettingHelpers_shop_settings(xsettings):
    emporium.helpers.setting.SettingHelpers.shop_settings(xsettings=xsettings)

