# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.operations.collection
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(request=st.nothing(), manufacturer_id=st.nothing())
def test_fuzz_CollectionOperations_delete_all_collections_dispatch_filter_manufacturer_id(
    request, manufacturer_id
):
    emporium.operations.collection.CollectionOperations.delete_all_collections_dispatch_filter_manufacturer_id(
        request=request, manufacturer_id=manufacturer_id
    )


@given(request=st.nothing(), filter_params=st.nothing())
def test_fuzz_CollectionOperations_get_collection_extended_data(request, filter_params):
    emporium.operations.collection.CollectionOperations.get_collection_extended_data(
        request=request, filter_params=filter_params
    )

