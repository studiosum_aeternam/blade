# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.operations.product
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(product=st.nothing(), tax=st.nothing())
def test_fuzz_ProductOperations_calculate_base_unit_price(product, tax):
    emporium.operations.product.ProductOperations.calculate_base_unit_price(
        product=product, tax=tax
    )


@given(discounts=st.nothing(), item=st.nothing())
def test_fuzz_ProductOperations_calculate_current_price(discounts, item):
    emporium.operations.product.ProductOperations.calculate_current_price(
        discounts=discounts, item=item
    )


@given(product=st.nothing(), tax=st.nothing())
def test_fuzz_ProductOperations_calculate_unit_price(product, tax):
    emporium.operations.product.ProductOperations.calculate_unit_price(
        product=product, tax=tax
    )


@given(
    request=st.nothing(),
    action=st.nothing(),
    product_id=st.nothing(),
    product_categories=st.nothing(),
    categories_passed=st.nothing(),
    category_tree=st.nothing(),
)
def test_fuzz_ProductOperations_categories_update(
    request, action, product_id, product_categories, categories_passed, category_tree
):
    emporium.operations.product.ProductOperations.categories_update(
        request=request,
        action=action,
        product_id=product_id,
        product_categories=product_categories,
        categories_passed=categories_passed,
        category_tree=category_tree,
    )


@given(request=st.nothing(), form=st.nothing(), category=st.nothing())
def test_fuzz_ProductOperations_create_product_to_category(request, form, category):
    emporium.operations.product.ProductOperations.create_product_to_category(
        request=request, form=form, category=category
    )


@given(
    request=st.nothing(),
    settings=st.nothing(),
    filter_params=st.nothing(),
    query_items=st.nothing(),
    product_availabilities=st.nothing(),
)
def test_fuzz_ProductOperations_current_data_options(
    request, settings, filter_params, query_items, product_availabilities
):
    emporium.operations.product.ProductOperations.current_data_options(
        request=request,
        settings=settings,
        filter_params=filter_params,
        query_items=query_items,
        product_availabilities=product_availabilities,
    )


@given(request=st.nothing(), band_id=st.nothing())
def test_fuzz_ProductOperations_delete_all_products_dispatch_filter_band_id(
    request, band_id
):
    emporium.operations.product.ProductOperations.delete_all_products_dispatch_filter_band_id(
        request=request, band_id=band_id
    )


@given(request=st.nothing(), manufacturer_id=st.nothing())
def test_fuzz_ProductOperations_delete_all_products_dispatch_filter_manufacturer_id(
    request, manufacturer_id
):
    emporium.operations.product.ProductOperations.delete_all_products_dispatch_filter_manufacturer_id(
        request=request, manufacturer_id=manufacturer_id
    )


@given(
    request=st.nothing(), image_for_deletion=st.nothing(), product_to_image=st.nothing()
)
def test_fuzz_ProductOperations_delete_product_image(
    request, image_for_deletion, product_to_image
):
    emporium.operations.product.ProductOperations.delete_product_image(
        request=request,
        image_for_deletion=image_for_deletion,
        product_to_image=product_to_image,
    )


@given(
    request=st.nothing(),
    product_for_deletion=st.nothing(),
    product_to_product=st.nothing(),
)
def test_fuzz_ProductOperations_delete_related_products(
    request, product_for_deletion, product_to_product
):
    emporium.operations.product.ProductOperations.delete_related_products(
        request=request,
        product_for_deletion=product_for_deletion,
        product_to_product=product_to_product,
    )


@given(input_date=st.nothing())
def test_fuzz_ProductOperations_format_date_for_database(input_date):
    emporium.operations.product.ProductOperations.format_date_for_database(
        input_date=input_date
    )


@given(
    request=st.nothing(),
    settings=st.nothing(),
    filter_params=st.nothing(),
    data_options=st.nothing(),
    number_elements=st.nothing(),
)
def test_fuzz_ProductOperations_formatted_featured_products(
    request, settings, filter_params, data_options, number_elements
):
    emporium.operations.product.ProductOperations.formatted_featured_products(
        request=request,
        settings=settings,
        filter_params=filter_params,
        data_options=data_options,
        number_elements=number_elements,
    )


@given(request=st.nothing(), product_id=st.nothing(), cache=st.nothing())
def test_fuzz_ProductOperations_generate_additional_images_list_filter_id(
    request, product_id, cache
):
    emporium.operations.product.ProductOperations.generate_additional_images_list_filter_id(
        request=request, product_id=product_id, cache=cache
    )


@given(
    request=st.nothing(),
    settings=st.nothing(),
    filter_params=st.nothing(),
    query_items=st.nothing(),
    product_availabilities=st.nothing(),
)
def test_fuzz_ProductOperations_generate_attribute_list(
    request, settings, filter_params, query_items, product_availabilities
):
    emporium.operations.product.ProductOperations.generate_attribute_list(
        request=request,
        settings=settings,
        filter_params=filter_params,
        query_items=query_items,
        product_availabilities=product_availabilities,
    )


@given(request=st.nothing(), settings=st.nothing())
def test_fuzz_ProductOperations_generate_availability_list(request, settings):
    emporium.operations.product.ProductOperations.generate_availability_list(
        request=request, settings=settings
    )


@given(request=st.nothing(), availability=st.nothing(), cache=st.nothing())
def test_fuzz_ProductOperations_generate_availability_list_filter_id(
    request, availability, cache
):
    emporium.operations.product.ProductOperations.generate_availability_list_filter_id(
        request=request, availability=availability, cache=cache
    )


@given(
    request=st.nothing(),
    settings=st.nothing(),
    filter_params=st.nothing(),
    query_items=st.nothing(),
)
def test_fuzz_ProductOperations_generate_band_list(
    request, settings, filter_params, query_items
):
    emporium.operations.product.ProductOperations.generate_band_list(
        request=request,
        settings=settings,
        filter_params=filter_params,
        query_items=query_items,
    )


@given(
    request=st.nothing(),
    products_ids=st.nothing(),
    cache=st.nothing(),
    query_items=st.nothing(),
)
def test_fuzz_ProductOperations_generate_band_list_filter_products_ids(
    request, products_ids, cache, query_items
):
    emporium.operations.product.ProductOperations.generate_band_list_filter_products_ids(
        request=request, products_ids=products_ids, cache=cache, query_items=query_items
    )


@given(request=st.nothing(), settings=st.nothing(), filter_params=st.nothing())
def test_fuzz_ProductOperations_generate_categories_list(
    request, settings, filter_params
):
    emporium.operations.product.ProductOperations.generate_categories_list(
        request=request, settings=settings, filter_params=filter_params
    )


@given(request=st.nothing(), products_ids=st.nothing(), cache=st.nothing())
def test_fuzz_ProductOperations_generate_categories_list_filter_product_id(
    request, products_ids, cache
):
    emporium.operations.product.ProductOperations.generate_categories_list_filter_product_id(
        request=request, products_ids=products_ids, cache=cache
    )


@given(
    request=st.nothing(),
    settings=st.nothing(),
    filter_params=st.nothing(),
    query_items=st.nothing(),
)
def test_fuzz_ProductOperations_generate_collection_list(
    request, settings, filter_params, query_items
):
    emporium.operations.product.ProductOperations.generate_collection_list(
        request=request,
        settings=settings,
        filter_params=filter_params,
        query_items=query_items,
    )


@given(
    request=st.nothing(),
    products_ids=st.nothing(),
    cache=st.nothing(),
    query_items=st.nothing(),
)
def test_fuzz_ProductOperations_generate_collection_list_filter_product_id(
    request, products_ids, cache, query_items
):
    emporium.operations.product.ProductOperations.generate_collection_list_filter_product_id(
        request=request, products_ids=products_ids, cache=cache, query_items=query_items
    )


@given(
    request=st.nothing(),
    settings=st.nothing(),
    filter_params=st.nothing(),
    query_items=st.nothing(),
)
def test_fuzz_ProductOperations_generate_manufacturer_list(
    request, settings, filter_params, query_items
):
    emporium.operations.product.ProductOperations.generate_manufacturer_list(
        request=request,
        settings=settings,
        filter_params=filter_params,
        query_items=query_items,
    )


@given(
    request=st.nothing(),
    manufacturers_ids=st.nothing(),
    cache=st.nothing(),
    query_items=st.nothing(),
)
def test_fuzz_ProductOperations_generate_manufacturer_list_filter_manufacturer_id(
    request, manufacturers_ids, cache, query_items
):
    emporium.operations.product.ProductOperations.generate_manufacturer_list_filter_manufacturer_id(
        request=request,
        manufacturers_ids=manufacturers_ids,
        cache=cache,
        query_items=query_items,
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_type=st.nothing(),
    cache=st.nothing(),
)
def test_fuzz_ProductOperations_generate_related_product_list_filter_product_id(
    request, product_id, product_type, cache
):
    emporium.operations.product.ProductOperations.generate_related_product_list_filter_product_id(
        request=request, product_id=product_id, product_type=product_type, cache=cache
    )


@given(
    request=st.nothing(),
    settings=st.nothing(),
    filter_params=st.nothing(),
    query_items=st.nothing(),
)
def test_fuzz_ProductOperations_generate_special_list(
    request, settings, filter_params, query_items
):
    emporium.operations.product.ProductOperations.generate_special_list(
        request=request,
        settings=settings,
        filter_params=filter_params,
        query_items=query_items,
    )


@given(
    request=st.nothing(),
    settings=st.nothing(),
    filter_params=st.nothing(),
    query_items=st.nothing(),
)
def test_fuzz_ProductOperations_generate_storage_list(
    request, settings, filter_params, query_items
):
    emporium.operations.product.ProductOperations.generate_storage_list(
        request=request,
        settings=settings,
        filter_params=filter_params,
        query_items=query_items,
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    query_items=st.nothing(),
    cache=st.nothing(),
)
def test_fuzz_ProductOperations_generate_storage_list_filter_product_id(
    request, product_id, query_items, cache
):
    emporium.operations.product.ProductOperations.generate_storage_list_filter_product_id(
        request=request, product_id=product_id, query_items=query_items, cache=cache
    )


@given(request=st.nothing(), product_id=st.nothing(), cache=st.nothing())
def test_fuzz_ProductOperations_generate_tax_list_filter_product_id(
    request, product_id, cache
):
    emporium.operations.product.ProductOperations.generate_tax_list_filter_product_id(
        request=request, product_id=product_id, cache=cache
    )


@given(request=st.nothing())
def test_fuzz_ProductOperations_generate_transport_list(request):
    emporium.operations.product.ProductOperations.generate_transport_list(
        request=request
    )


@given(settings=st.nothing(), data_options=st.nothing(), product=st.nothing())
def test_fuzz_ProductOperations_get_catalog_price_using_shop_settings(
    settings, data_options, product
):
    emporium.operations.product.ProductOperations.get_catalog_price_using_shop_settings(
        settings=settings, data_options=data_options, product=product
    )


@given(settings=st.nothing(), tax=st.nothing(), product=st.nothing())
def test_fuzz_ProductOperations_get_price_based_on_system_settings(
    settings, tax, product
):
    emporium.operations.product.ProductOperations.get_price_based_on_system_settings(
        settings=settings, tax=tax, product=product
    )


@given(request=st.nothing(), product_list=st.nothing())
def test_fuzz_ProductOperations_mass_product_storage_create(request, product_list):
    emporium.operations.product.ProductOperations.mass_product_storage_create(
        request=request, product_list=product_list
    )


@given(request=st.nothing(), product_list_storage_data=st.nothing())
def test_fuzz_ProductOperations_mass_product_storage_dispatch(
    request, product_list_storage_data
):
    emporium.operations.product.ProductOperations.mass_product_storage_dispatch(
        request=request, product_list_storage_data=product_list_storage_data
    )


@given(request=st.nothing(), product_list_storage_data=st.nothing())
def test_fuzz_ProductOperations_mass_product_storage_update(
    request, product_list_storage_data
):
    emporium.operations.product.ProductOperations.mass_product_storage_update(
        request=request, product_list_storage_data=product_list_storage_data
    )


@given(
    request=st.nothing(),
    action=st.nothing(),
    product_id=st.nothing(),
    product_attributes=st.nothing(),
    attributes_passed=st.nothing(),
)
def test_fuzz_ProductOperations_product_attributes_update(
    request, action, product_id, product_attributes, attributes_passed
):
    emporium.operations.product.ProductOperations.product_attributes_update(
        request=request,
        action=action,
        product_id=product_id,
        product_attributes=product_attributes,
        attributes_passed=attributes_passed,
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_to_author_current_elements=st.nothing(),
)
def test_fuzz_ProductOperations_product_author_update(
    request, product_id, product_to_author_current_elements
):
    emporium.operations.product.ProductOperations.product_author_update(
        request=request,
        product_id=product_id,
        product_to_author_current_elements=product_to_author_current_elements,
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_to_author_old_elements=st.nothing(),
    product_to_author_current_elements=st.nothing(),
)
def test_fuzz_ProductOperations_product_authors_dispatch(
    request,
    product_id,
    product_to_author_old_elements,
    product_to_author_current_elements,
):
    emporium.operations.product.ProductOperations.product_authors_dispatch(
        request=request,
        product_id=product_id,
        product_to_author_old_elements=product_to_author_old_elements,
        product_to_author_current_elements=product_to_author_current_elements,
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_to_product_current_elements=st.nothing(),
)
def test_fuzz_ProductOperations_product_component_update(
    request, product_id, product_to_product_current_elements
):
    emporium.operations.product.ProductOperations.product_component_update(
        request=request,
        product_id=product_id,
        product_to_product_current_elements=product_to_product_current_elements,
    )


@given(
    request=st.nothing(),
    action=st.nothing(),
    product_id=st.nothing(),
    product_components=st.nothing(),
    products_passed=st.nothing(),
    relation_type=st.nothing(),
)
def test_fuzz_ProductOperations_product_components_update(
    request, action, product_id, product_components, products_passed, relation_type
):
    emporium.operations.product.ProductOperations.product_components_update(
        request=request,
        action=action,
        product_id=product_id,
        product_components=product_components,
        products_passed=products_passed,
        relation_type=relation_type,
    )


@given(
    request=st.nothing(),
    action=st.nothing(),
    form=st.nothing(),
    product_id=st.nothing(),
    product_description=st.nothing(),
)
def test_fuzz_ProductOperations_product_description_update(
    request, action, form, product_id, product_description
):
    emporium.operations.product.ProductOperations.product_description_update(
        request=request,
        action=action,
        form=form,
        product_id=product_id,
        product_description=product_description,
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_file_current_elements=st.nothing(),
)
def test_fuzz_ProductOperations_product_file_update(
    request, product_id, product_file_current_elements
):
    emporium.operations.product.ProductOperations.product_file_update(
        request=request,
        product_id=product_id,
        product_file_current_elements=product_file_current_elements,
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_files_old_elements=st.nothing(),
    product_file_current_elements=st.nothing(),
)
def test_fuzz_ProductOperations_product_files_dispatch(
    request, product_id, product_files_old_elements, product_file_current_elements
):
    emporium.operations.product.ProductOperations.product_files_dispatch(
        request=request,
        product_id=product_id,
        product_files_old_elements=product_files_old_elements,
        product_file_current_elements=product_file_current_elements,
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_image_current_elements=st.nothing(),
)
def test_fuzz_ProductOperations_product_image_update(
    request, product_id, product_image_current_elements
):
    emporium.operations.product.ProductOperations.product_image_update(
        request=request,
        product_id=product_id,
        product_image_current_elements=product_image_current_elements,
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_images_old_elements=st.nothing(),
    product_image_current_elements=st.nothing(),
)
def test_fuzz_ProductOperations_product_images_dispatch(
    request, product_id, product_images_old_elements, product_image_current_elements
):
    emporium.operations.product.ProductOperations.product_images_dispatch(
        request=request,
        product_id=product_id,
        product_images_old_elements=product_images_old_elements,
        product_image_current_elements=product_image_current_elements,
    )


@given(request=st.nothing(), settings=st.nothing(), filter_params=st.nothing())
def test_fuzz_ProductOperations_product_list_filtered(request, settings, filter_params):
    emporium.operations.product.ProductOperations.product_list_filtered(
        request=request, settings=settings, filter_params=filter_params
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_to_special_current_elements=st.nothing(),
    price_input_setting=st.nothing(),
    tax_value=st.nothing(),
)
def test_fuzz_ProductOperations_product_special_update(
    request,
    product_id,
    product_to_special_current_elements,
    price_input_setting,
    tax_value,
):
    emporium.operations.product.ProductOperations.product_special_update(
        request=request,
        product_id=product_id,
        product_to_special_current_elements=product_to_special_current_elements,
        price_input_setting=price_input_setting,
        tax_value=tax_value,
    )


@given(request=st.nothing(), product_id=st.nothing(), storage_list=st.nothing())
def test_fuzz_ProductOperations_product_storage_create(
    request, product_id, storage_list
):
    emporium.operations.product.ProductOperations.product_storage_create(
        request=request, product_id=product_id, storage_list=storage_list
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_list_storage_data=st.nothing(),
)
def test_fuzz_ProductOperations_product_storage_dispatch(
    request, product_id, product_list_storage_data
):
    emporium.operations.product.ProductOperations.product_storage_dispatch(
        request=request,
        product_id=product_id,
        product_list_storage_data=product_list_storage_data,
    )


@given(request=st.nothing(), product_list_storage_data=st.nothing())
def test_fuzz_ProductOperations_product_storage_update(
    request, product_list_storage_data
):
    emporium.operations.product.ProductOperations.product_storage_update(
        request=request, product_list_storage_data=product_list_storage_data
    )


@given(category_id=st.nothing(), product_id=st.nothing())
def test_fuzz_ProductOperations_product_to_category_create(category_id, product_id):
    emporium.operations.product.ProductOperations.product_to_category_create(
        category_id=category_id, product_id=product_id
    )


@given(
    request=st.nothing(),
    action=st.nothing(),
    col_ids=st.nothing(),
    product_id=st.nothing(),
    related_collections=st.nothing(),
)
def test_fuzz_ProductOperations_product_to_collection_create(
    request, action, col_ids, product_id, related_collections
):
    emporium.operations.product.ProductOperations.product_to_collection_create(
        request=request,
        action=action,
        col_ids=col_ids,
        product_id=product_id,
        related_collections=related_collections,
    )


@given(
    request=st.nothing(),
    col_ids=st.nothing(),
    product_to_collection=st.nothing(),
    related_collections=st.nothing(),
)
def test_fuzz_ProductOperations_product_to_collection_delete(
    request, col_ids, product_to_collection, related_collections
):
    emporium.operations.product.ProductOperations.product_to_collection_delete(
        request=request,
        col_ids=col_ids,
        product_to_collection=product_to_collection,
        related_collections=related_collections,
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    to_add=st.nothing(),
    relation_type=st.nothing(),
)
def test_fuzz_ProductOperations_product_to_product_create(
    request, product_id, to_add, relation_type
):
    emporium.operations.product.ProductOperations.product_to_product_create(
        request=request,
        product_id=product_id,
        to_add=to_add,
        relation_type=relation_type,
    )


@given(to_edit=st.nothing(), product_to_product=st.nothing())
def test_fuzz_ProductOperations_product_to_product_update(to_edit, product_to_product):
    emporium.operations.product.ProductOperations.product_to_product_update(
        to_edit=to_edit, product_to_product=product_to_product
    )


@given(request=st.nothing(), product_to_relation_delete=st.nothing())
def test_fuzz_ProductOperations_product_to_relation_delete(
    request, product_to_relation_delete
):
    emporium.operations.product.ProductOperations.product_to_relation_delete(
        request=request, product_to_relation_delete=product_to_relation_delete
    )


@given(
    request=st.nothing(),
    action=st.nothing(),
    datetime=st.nothing(),
    form=st.nothing(),
    product=st.nothing(),
)
def test_fuzz_ProductOperations_product_update(
    request, action, datetime, form, product
):
    emporium.operations.product.ProductOperations.product_update(
        request=request, action=action, datetime=datetime, form=form, product=product
    )


@given(
    request=st.nothing(),
    settings=st.nothing(),
    today=st.nothing(),
    product=st.nothing(),
    data_options=st.nothing(),
    data_options_filtered=st.nothing(),
    number_elements=st.nothing(),
)
def test_fuzz_ProductOperations_products_related_to_current_view(
    request,
    settings,
    today,
    product,
    data_options,
    data_options_filtered,
    number_elements,
):
    emporium.operations.product.ProductOperations.products_related_to_current_view(
        request=request,
        settings=settings,
        today=today,
        product=product,
        data_options=data_options,
        data_options_filtered=data_options_filtered,
        number_elements=number_elements,
    )


@given(product=st.nothing())
def test_fuzz_ProductOperations_real_price(product):
    emporium.operations.product.ProductOperations.real_price(product=product)


@given(request=st.nothing())
def test_fuzz_ProductOperations_return_correct_unit_values(request):
    emporium.operations.product.ProductOperations.return_correct_unit_values(
        request=request
    )


@given(request=st.nothing(), settings=st.nothing(), today=st.nothing())
def test_fuzz_ProductOperations_return_product(request, settings, today):
    emporium.operations.product.ProductOperations.return_product(
        request=request, settings=settings, today=today
    )


@given(price_input_setting=st.nothing(), tax_value=st.nothing(), price=st.nothing())
def test_fuzz_ProductOperations_set_price_based_on_system_settings(
    price_input_setting, tax_value, price
):
    emporium.operations.product.ProductOperations.set_price_based_on_system_settings(
        price_input_setting=price_input_setting, tax_value=tax_value, price=price
    )


@given(
    product=st.nothing(),
    measurement_attributes=st.nothing(),
    attributes_passed=st.nothing(),
)
def test_fuzz_ProductOperations_set_product_dimensions(
    product, measurement_attributes, attributes_passed
):
    emporium.operations.product.ProductOperations.set_product_dimensions(
        product=product,
        measurement_attributes=measurement_attributes,
        attributes_passed=attributes_passed,
    )


@given(
    request=st.nothing(),
    product_type=st.nothing(),
    availability=st.nothing(),
    manufacturer=st.nothing(),
    product_id=st.nothing(),
    cache=st.nothing(),
    query_items=st.nothing(),
)
def test_fuzz_ProductOperations_single_product_data_options(
    request, product_type, availability, manufacturer, product_id, cache, query_items
):
    emporium.operations.product.ProductOperations.single_product_data_options(
        request=request,
        product_type=product_type,
        availability=availability,
        manufacturer=manufacturer,
        product_id=product_id,
        cache=cache,
        query_items=query_items,
    )


@given(value=st.nothing())
def test_fuzz_ProductOperations_strip_value(value):
    emporium.operations.product.ProductOperations.strip_value(value=value)


@given(
    request=st.nothing(),
    action=st.nothing(),
    band_supplemental_id=st.nothing(),
    product_id=st.nothing(),
    product_band=st.nothing(),
)
def test_fuzz_ProductOperations_supplemental_band_update(
    request, action, band_supplemental_id, product_id, product_band
):
    emporium.operations.product.ProductOperations.supplemental_band_update(
        request=request,
        action=action,
        band_supplemental_id=band_supplemental_id,
        product_id=product_id,
        product_band=product_band,
    )


@given(catalog_price=st.nothing(), item=st.nothing())
def test_fuzz_ProductOperations_update_catalog_price(catalog_price, item):
    emporium.operations.product.ProductOperations.update_catalog_price(
        catalog_price=catalog_price, item=item
    )


@given(
    merged_dicts=st.builds(dict), product_list=st.builds(list), current_date=st.text()
)
def test_fuzz_ProductOperations_update_catalog_prices(
    merged_dicts, product_list, current_date
):
    emporium.operations.product.ProductOperations.update_catalog_prices(
        merged_dicts=merged_dicts, product_list=product_list, current_date=current_date
    )


@given(product_list=st.builds(list), discounts=st.builds(list), current_date=st.text())
def test_fuzz_ProductOperations_update_customer_prices(
    product_list, discounts, current_date
):
    emporium.operations.product.ProductOperations.update_customer_prices(
        product_list=product_list, discounts=discounts, current_date=current_date
    )


@given(discount_id=st.nothing(), item=st.nothing())
def test_fuzz_ProductOperations_update_discount_value(discount_id, item):
    emporium.operations.product.ProductOperations.update_discount_value(
        discount_id=discount_id, item=item
    )


@given(settings=st.nothing(), data_options=st.nothing(), form=st.nothing())
def test_fuzz_ProductOperations_update_price_based_on_system_settings(
    settings, data_options, form
):
    emporium.operations.product.ProductOperations.update_price_based_on_system_settings(
        settings=settings, data_options=data_options, form=form
    )


@given(
    request=st.nothing(),
    product=st.nothing(),
    product_to_author_old_elements=st.nothing(),
)
def test_fuzz_ProductOperations_update_product_authors(
    request, product, product_to_author_old_elements
):
    emporium.operations.product.ProductOperations.update_product_authors(
        request=request,
        product=product,
        product_to_author_old_elements=product_to_author_old_elements,
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_to_product_old_elements=st.nothing(),
    product_to_product_current_elements=st.nothing(),
)
def test_fuzz_ProductOperations_update_product_components(
    request,
    product_id,
    product_to_product_old_elements,
    product_to_product_current_elements,
):
    emporium.operations.product.ProductOperations.update_product_components(
        request=request,
        product_id=product_id,
        product_to_product_old_elements=product_to_product_old_elements,
        product_to_product_current_elements=product_to_product_current_elements,
    )


@given(request=st.nothing(), product_id=st.nothing())
def test_fuzz_ProductOperations_update_promotions(request, product_id):
    emporium.operations.product.ProductOperations.update_promotions(
        request=request, product_id=product_id
    )


@given(
    request=st.nothing(),
    product_id=st.nothing(),
    product_to_special_old_elements=st.nothing(),
    product_to_special_current_elements=st.nothing(),
    price_input_setting=st.nothing(),
    tax_value=st.nothing(),
)
def test_fuzz_ProductOperations_update_special_products(
    request,
    product_id,
    product_to_special_old_elements,
    product_to_special_current_elements,
    price_input_setting,
    tax_value,
):
    emporium.operations.product.ProductOperations.update_special_products(
        request=request,
        product_id=product_id,
        product_to_special_old_elements=product_to_special_old_elements,
        product_to_special_current_elements=product_to_special_current_elements,
        price_input_setting=price_input_setting,
        tax_value=tax_value,
    )

