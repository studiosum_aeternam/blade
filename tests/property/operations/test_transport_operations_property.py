# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.operations.transport
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(request=st.nothing(), params=st.nothing())
def test_fuzz_TransportOperations_cheapest_transport_with_payment(request, params):
    emporium.operations.transport.TransportOperations.cheapest_transport_with_payment(
        request=request, params=params
    )


@given(params=st.nothing())
def test_fuzz_TransportOperations_transport_payment_cartesian_matrix(params):
    emporium.operations.transport.TransportOperations.transport_payment_cartesian_matrix(
        params=params
    )


@given(
    request=st.nothing(), payment_methods=st.nothing(), transport_methods=st.nothing()
)
def test_fuzz_TransportOperations_transport_payment_matrix(
    request, payment_methods, transport_methods
):
    emporium.operations.transport.TransportOperations.transport_payment_matrix(
        request=request,
        payment_methods=payment_methods,
        transport_methods=transport_methods,
    )

