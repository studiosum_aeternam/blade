# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.operations.cart
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(
    request=st.nothing(),
    cart_contents=st.builds(list),
    featured_product_dict=st.builds(dict),
    tax_dict=st.builds(dict),
)
def test_fuzz_CartOperations_calculate_cart_params(
    request, cart_contents, featured_product_dict, tax_dict
):
    emporium.operations.cart.CartOperations.calculate_cart_params(
        request=request,
        cart_contents=cart_contents,
        featured_product_dict=featured_product_dict,
        tax_dict=tax_dict,
    )


@given(item=st.builds(dict))
def test_fuzz_CartOperations_calculate_packages(item):
    emporium.operations.cart.CartOperations.calculate_packages(item=item)


@given(
    item=st.builds(dict),
    featured_product_list=st.builds(list),
    tax_dict=st.builds(dict),
)
def test_fuzz_CartOperations_calculate_price_and_value(
    item, featured_product_list, tax_dict
):
    emporium.operations.cart.CartOperations.calculate_price_and_value(
        item=item, featured_product_list=featured_product_list, tax_dict=tax_dict
    )


@given(
    params=st.builds(dict), item=st.builds(dict), featured_product_list=st.builds(list)
)
def test_fuzz_CartOperations_calculate_tax(params, item, featured_product_list):
    emporium.operations.cart.CartOperations.calculate_tax(
        params=params, item=item, featured_product_list=featured_product_list
    )


@given(params=st.builds(dict))
def test_fuzz_CartOperations_calculate_transport_unit(params):
    emporium.operations.cart.CartOperations.calculate_transport_unit(params=params)


@given(request=st.nothing(), today=st.nothing())
def test_fuzz_CartOperations_cart_filter_cart_or_customer(request, today):
    emporium.operations.cart.CartOperations.cart_filter_cart_or_customer(
        request=request, today=today
    )


@given(request=st.nothing())
def test_fuzz_CartOperations_cart_merge(request):
    emporium.operations.cart.CartOperations.cart_merge(request=request)


@given(params=st.builds(dict), item=st.builds(dict))
def test_fuzz_CartOperations_check_payment_rules(params, item):
    emporium.operations.cart.CartOperations.check_payment_rules(
        params=params, item=item
    )


@given(params=st.builds(dict), item=st.builds(dict))
def test_fuzz_CartOperations_check_transport_rules(params, item):
    emporium.operations.cart.CartOperations.check_transport_rules(
        params=params, item=item
    )


@given(params=st.builds(dict), item=st.builds(dict))
def test_fuzz_CartOperations_forbidden_delivery_check(params, item):
    emporium.operations.cart.CartOperations.forbidden_delivery_check(
        params=params, item=item
    )


@given(params=st.builds(dict))
def test_fuzz_CartOperations_format_tax_dict_values(params):
    emporium.operations.cart.CartOperations.format_tax_dict_values(params=params)


@given(params=st.builds(dict), item=st.builds(dict))
def test_fuzz_CartOperations_free_delivery_check(params, item):
    emporium.operations.cart.CartOperations.free_delivery_check(
        params=params, item=item
    )


@given(product=st.nothing(), data_options=st.nothing())
def test_fuzz_CartOperations_product_requires_special_transport(product, data_options):
    emporium.operations.cart.CartOperations.product_requires_special_transport(
        product=product, data_options=data_options
    )


@given(params=st.builds(dict), item=st.builds(dict))
def test_fuzz_CartOperations_summarize_cart_params(params, item):
    emporium.operations.cart.CartOperations.summarize_cart_params(
        params=params, item=item
    )


@given(
    cart_contents=st.builds(list),
    params=st.builds(dict),
    featured_product_dict=st.builds(dict),
    tax_dict=st.builds(dict),
)
def test_fuzz_CartOperations_update_cart_params(
    cart_contents, params, featured_product_dict, tax_dict
):
    emporium.operations.cart.CartOperations.update_cart_params(
        cart_contents=cart_contents,
        params=params,
        featured_product_dict=featured_product_dict,
        tax_dict=tax_dict,
    )

