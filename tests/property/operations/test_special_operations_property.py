# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.operations.special
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(special_prices=st.nothing())
def test_fuzz_SpecialOperations_create_special_prices_dict(special_prices):
    emporium.operations.special.SpecialOperations.create_special_prices_dict(
        special_prices=special_prices
    )


@given(value=st.nothing())
def test_fuzz_SpecialOperations_disable_default_price_for_other_specials(value):
    emporium.operations.special.SpecialOperations.disable_default_price_for_other_specials(
        value=value
    )


@given(product_dict=st.nothing())
def test_fuzz_SpecialOperations_set_default_special_price(product_dict):
    emporium.operations.special.SpecialOperations.set_default_special_price(
        product_dict=product_dict
    )


@given(request=st.nothing())
def test_fuzz_SpecialOperations_set_lowest_price_all_promotions(request):
    emporium.operations.special.SpecialOperations.set_lowest_price_all_promotions(
        request=request
    )


@given(request=st.nothing(), product_id=st.nothing())
def test_fuzz_SpecialOperations_set_lowest_price_multiple_products(request, product_id):
    emporium.operations.special.SpecialOperations.set_lowest_price_multiple_products(
        request=request, product_id=product_id
    )


@given(request=st.nothing(), product_id=st.nothing())
def test_fuzz_SpecialOperations_set_lowest_price_single_product(request, product_id):
    emporium.operations.special.SpecialOperations.set_lowest_price_single_product(
        request=request, product_id=product_id
    )

