# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.operations.manufacturer
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(request=st.nothing(), filter_params=st.nothing())
def test_fuzz_ManufacturerOperations_get_manufacturer_extended_data(
    request, filter_params
):
    emporium.operations.manufacturer.ManufacturerOperations.get_manufacturer_extended_data(
        request=request, filter_params=filter_params
    )

