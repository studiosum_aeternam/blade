# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.operations.category
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(
    request=st.nothing(),
    category_id=st.nothing(),
    products_ids=st.nothing(),
    new_product_list=st.nothing(),
)
def test_fuzz_CategoryOperations_add_category_related_products(
    request, category_id, products_ids, new_product_list
):
    emporium.operations.category.CategoryOperations.add_category_related_products(
        request=request,
        category_id=category_id,
        products_ids=products_ids,
        new_product_list=new_product_list,
    )


@given(categories_passed=st.nothing(), category_tree=st.nothing())
def test_fuzz_CategoryOperations_add_parent_categories(
    categories_passed, category_tree
):
    emporium.operations.category.CategoryOperations.add_parent_categories(
        categories_passed=categories_passed, category_tree=category_tree
    )


@given(form=st.nothing())
def test_fuzz_CategoryOperations_category_create(form):
    emporium.operations.category.CategoryOperations.category_create(form=form)


@given(
    request=st.nothing(),
    category=st.nothing(),
    filter_params=st.nothing(),
    misc_options=st.nothing(),
    meta_options=st.nothing(),
)
def test_fuzz_CategoryOperations_category_front_page_description_and_meta(
    request, category, filter_params, misc_options, meta_options
):
    emporium.operations.category.CategoryOperations.category_front_page_description_and_meta(
        request=request,
        category=category,
        filter_params=filter_params,
        misc_options=misc_options,
        meta_options=meta_options,
    )


@given(
    request=st.nothing(),
    category=st.nothing(),
    filter_params=st.nothing(),
    misc_options=st.nothing(),
    data_options=st.nothing(),
    meta_options=st.nothing(),
    filler=st.nothing(),
)
def test_fuzz_CategoryOperations_category_manufacturer_front_page_description_and_meta(
    request, category, filter_params, misc_options, data_options, meta_options, filler
):
    emporium.operations.category.CategoryOperations.category_manufacturer_front_page_description_and_meta(
        request=request,
        category=category,
        filter_params=filter_params,
        misc_options=misc_options,
        data_options=data_options,
        meta_options=meta_options,
        filler=filler,
    )


@given(request=st.nothing(), form=st.nothing())
def test_fuzz_CategoryOperations_create_category(request, form):
    emporium.operations.category.CategoryOperations.create_category(
        request=request, form=form
    )


@given(request=st.nothing(), form=st.nothing(), category=st.nothing())
def test_fuzz_CategoryOperations_create_category_meta_tree_description(
    request, form, category
):
    emporium.operations.category.CategoryOperations.create_category_meta_tree_description(
        request=request, form=form, category=category
    )


@given(request=st.nothing(), products_ids=st.nothing(), new_product_list=st.nothing())
def test_fuzz_CategoryOperations_delete_category_related_products(
    request, products_ids, new_product_list
):
    emporium.operations.category.CategoryOperations.delete_category_related_products(
        request=request, products_ids=products_ids, new_product_list=new_product_list
    )


@given(form=st.nothing())
def test_fuzz_CategoryOperations_get_product_list_from_form(form):
    emporium.operations.category.CategoryOperations.get_product_list_from_form(
        form=form
    )


@given(request=st.nothing(), settings=st.nothing(), product_list=st.nothing())
def test_fuzz_CategoryOperations_return_product_related_data(
    request, settings, product_list
):
    emporium.operations.category.CategoryOperations.return_product_related_data(
        request=request, settings=settings, product_list=product_list
    )


@given(category=st.nothing(), form=st.nothing())
def test_fuzz_CategoryOperations_update_category_data(category, form):
    emporium.operations.category.CategoryOperations.update_category_data(
        category=category, form=form
    )


@given(category_description=st.nothing(), form=st.nothing())
def test_fuzz_CategoryOperations_update_category_description_data(
    category_description, form
):
    emporium.operations.category.CategoryOperations.update_category_description_data(
        category_description=category_description, form=form
    )


@given(
    request=st.nothing(),
    category_id=st.nothing(),
    form=st.nothing(),
    products_ids=st.nothing(),
)
def test_fuzz_CategoryOperations_update_category_information(
    request, category_id, form, products_ids
):
    emporium.operations.category.CategoryOperations.update_category_information(
        request=request, category_id=category_id, form=form, products_ids=products_ids
    )

