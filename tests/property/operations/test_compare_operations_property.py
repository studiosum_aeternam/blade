# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.operations.compare
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with an appropriate strategy


@given(request=st.nothing())
def test_fuzz_CompareOperations_compare_merge(request):
    emporium.operations.compare.CompareOperations.compare_merge(request=request)

