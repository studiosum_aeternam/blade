# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.operations.filter
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with appropriate strategies


@given(request=st.nothing(), filter_params=st.nothing())
def test_fuzz_FilterOperations_check_for_first_category_page(request, filter_params):
    emporium.operations.filter.FilterOperations.check_for_first_category_page(
        request=request, filter_params=filter_params
    )


@given(filter_params=st.nothing())
def test_fuzz_FilterOperations_filter_attribute_params(filter_params):
    emporium.operations.filter.FilterOperations.filter_attribute_params(
        filter_params=filter_params
    )


@given(filter_params=st.nothing())
def test_fuzz_FilterOperations_filter_collection_params(filter_params):
    emporium.operations.filter.FilterOperations.filter_collection_params(
        filter_params=filter_params
    )


@given(request=st.nothing(), session=st.nothing())
def test_fuzz_FilterOperations_filter_items_per_page(request, session):
    emporium.operations.filter.FilterOperations.filter_items_per_page(
        request=request, session=session
    )


@given(filter_params=st.nothing())
def test_fuzz_FilterOperations_filter_main_page_description(filter_params):
    emporium.operations.filter.FilterOperations.filter_main_page_description(
        filter_params=filter_params
    )


@given(filter_params=st.nothing())
def test_fuzz_FilterOperations_filter_manufactuter_params(filter_params):
    emporium.operations.filter.FilterOperations.filter_manufactuter_params(
        filter_params=filter_params
    )


@given(filter_params=st.nothing(), query_items=st.nothing())
def test_fuzz_FilterOperations_filter_price_params(filter_params, query_items):
    emporium.operations.filter.FilterOperations.filter_price_params(
        filter_params=filter_params, query_items=query_items
    )


@given(request=st.nothing(), availability_attributes=st.nothing())
def test_fuzz_FilterOperations_generate_filter_params(request, availability_attributes):
    emporium.operations.filter.FilterOperations.generate_filter_params(
        request=request, availability_attributes=availability_attributes
    )


@given(request=st.nothing(), filter_params=st.nothing(), session=st.nothing())
def test_fuzz_FilterOperations_get_display_mode_params(request, filter_params, session):
    emporium.operations.filter.FilterOperations.get_display_mode_params(
        request=request, filter_params=filter_params, session=session
    )


@given(price=st.nothing())
def test_fuzz_FilterOperations_sanitize_price_filters(price):
    emporium.operations.filter.FilterOperations.sanitize_price_filters(price=price)

