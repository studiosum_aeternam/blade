# This test code was written by the `hypothesis.extra.ghostwriter` module
# and is provided under the Creative Commons Zero public domain dedication.

import emporium.operations.tax
from hypothesis import given, strategies as st

# TODO: replace st.nothing() with an appropriate strategy


@given(request=st.nothing())
def test_fuzz_TaxOperations_generate_tax_list(request):
    emporium.operations.tax.TaxOperations.generate_tax_list(request=request)

