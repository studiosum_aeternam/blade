from decimal import Decimal
import operator

from emporium import helpers, models, services

from tests import unit


def test_create_discount_dict_key_id(dummy_request, dbsession):
    discount_1 = unit.makeDiscount(1, 1, -15.00, False)
    discount_2 = unit.makeDiscount(1, 1, -20.00, False)
    discount_3 = unit.makeDiscount(1, 1, -30.00, False)
    discount_4 = unit.makeDiscount(1, 1, 30.00, False)
    discount_5 = unit.makeDiscount(1, 1, 20.00, False)
    discount_6 = unit.makeDiscount(1, 1, 15.00, False)
    dbsession.add_all(
        [discount_1, discount_2, discount_3, discount_4, discount_5, discount_6]
    )
    dbsession.flush()
    discounts = services.DiscountService.discount_list_id_value_filter_discount_id(
        dummy_request,
        [
            discount_1.discount_id,
            discount_2.discount_id,
            discount_3.discount_id,
            discount_4.discount_id,
            discount_5.discount_id,
            discount_6.discount_id,
        ],
    )
    discount_dict = helpers.DiscountHelpers.create_discount_dict_key_id(discounts)
    assert discount_dict == {
        discount_1.discount_id: (discount_1.discount_id, Decimal("-15.00")),
        discount_2.discount_id: (discount_2.discount_id, Decimal("-20.00")),
        discount_3.discount_id: (discount_3.discount_id, Decimal("-30.00")),
        discount_4.discount_id: (discount_4.discount_id, Decimal("30.00")),
        discount_5.discount_id: (discount_5.discount_id, Decimal("20.00")),
        discount_6.discount_id: (discount_6.discount_id, Decimal("15.00")),
    }


def test_discounts_format(dummy_request, dbsession):
    discount_1 = unit.makeDiscount(1, 1, -15.00, False)
    discount_2 = unit.makeDiscount(1, 1, -20.00, False)
    discount_3 = unit.makeDiscount(1, 1, -30.00, False)
    discount_4 = unit.makeDiscount(1, 1, 30.00, False)
    discount_5 = unit.makeDiscount(1, 1, 20.00, False)
    discount_6 = unit.makeDiscount(1, 1, 15.00, False)
    manufacturer_1 = unit.makeManufacturer(
        "2021-01-01",
        "2021-01-01",
        "2021-01-01",
        "folder/fist-manufacturer.jpg",
        "Fist Manufacturer",
        1,
        1,
        0,
        0,
    )
    manufacturer_2 = unit.makeManufacturer(
        "2021-01-01",
        "2021-01-01",
        "2021-01-01",
        "folder/fist-manufacturer.jpg",
        "Secont Manufacturer",
        1,
        0,
        1,
        0,
    )
    dbsession.add_all(
        [
            discount_1,
            discount_2,
            discount_3,
            discount_4,
            discount_5,
            discount_6,
            manufacturer_1,
            manufacturer_2,
        ]
    )
    dbsession.flush()
    manufacturer_to_discount_1 = unit.makeManufacturerToDiscount(
        discount_1.discount_id, manufacturer_1.manufacturer_id
    )
    manufacturer_to_discount_2 = unit.makeManufacturerToDiscount(
        discount_2.discount_id, manufacturer_2.manufacturer_id
    )
    manufacturer_to_discount_3 = unit.makeManufacturerToDiscount(
        discount_3.discount_id, manufacturer_1.manufacturer_id
    )
    manufacturer_to_discount_4 = unit.makeManufacturerToDiscount(
        discount_4.discount_id, manufacturer_2.manufacturer_id
    )
    manufacturer_to_discount_5 = unit.makeManufacturerToDiscount(
        discount_5.discount_id, manufacturer_1.manufacturer_id
    )
    manufacturer_to_discount_6 = unit.makeManufacturerToDiscount(
        discount_6.discount_id, manufacturer_2.manufacturer_id
    )
    dbsession.add_all(
        [
            manufacturer_to_discount_1,
            manufacturer_to_discount_2,
            manufacturer_to_discount_3,
            manufacturer_to_discount_4,
            manufacturer_to_discount_5,
            manufacturer_to_discount_6,
        ]
    )
    dbsession.flush()
    discount_dict = helpers.DiscountHelpers.discounts_format(
        services.DiscountService.discount_id_value_manufacturer_filter_active(
            dummy_request
        )
    )
    mockup_list = [
        {
            "discount_id": discount_3.discount_id,
            "manufacturer_id": manufacturer_1.manufacturer_id,
            "value": discount_3.value,
        },
        {
            "discount_id": discount_2.discount_id,
            "manufacturer_id": manufacturer_2.manufacturer_id,
            "value": discount_2.value,
        },
        {
            "discount_id": discount_1.discount_id,
            "manufacturer_id": manufacturer_1.manufacturer_id,
            "value": discount_1.value,
        },
        {
            "discount_id": discount_6.discount_id,
            "manufacturer_id": manufacturer_2.manufacturer_id,
            "value": discount_6.value,
        },
        {
            "discount_id": discount_5.discount_id,
            "manufacturer_id": manufacturer_1.manufacturer_id,
            "value": discount_5.value,
        },
        {
            "discount_id": discount_4.discount_id,
            "manufacturer_id": manufacturer_2.manufacturer_id,
            "value": discount_4.value,
        },
    ]
    assert discount_dict == mockup_list
