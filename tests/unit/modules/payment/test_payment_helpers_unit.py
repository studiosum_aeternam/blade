from collections import namedtuple
from decimal import Decimal

from emporium import data_containers, helpers
from dataclasses import dataclass


# Simulating object returned by the SQLAlchemy
@dataclass
class PaymentObject:
    special_method: int
    description: str
    excerpt: str
    module_name: str
    module_settings: dict
    name: str
    payment_id: int


def test_generate_transport_container_no_payment(dummy_request):
    payment_list = []
    payment_formatted_dict = helpers.PaymentHelpers.generate_payment_container(
        payment_list
    )
    expected_dict = {}
    assert payment_formatted_dict == expected_dict


def test_generate_transport_container_single_payment(dummy_request):
    payment_list = [
        PaymentObject(
            special_method=True,
            description="",
            excerpt="Money transfer",
            module_name="money_transfer",
            module_settings={},
            name="Money transfer",
            payment_id=1,
        )
    ]
    payment_formatted_dict = helpers.PaymentHelpers.generate_payment_container(
        payment_list
    )
    expected_dict = {
        "money_transfer": data_containers.PaymentContainer(
            special_method=True,
            description="",
            excerpt="Money transfer",
            module_name="money_transfer",
            module_settings={},
            name="Money transfer",
            payment_id=1,
        )
    }
    assert payment_formatted_dict == expected_dict


def test_generate_transport_container_multiple_payments(dummy_request):
    payment_list = [
        PaymentObject(
            special_method=True,
            description="",
            excerpt="Money transfer for object",
            module_name="money_transfer",
            module_settings={},
            name="Money transfer",
            payment_id=1,
        ),
        PaymentObject(
            special_method=False,
            description="",
            excerpt="Cash on delivery - excerpt",
            module_name="cod",
            module_settings={},
            name="Cash on delivery",
            payment_id=2,
        ),
    ]
    payment_formatted_dict = helpers.PaymentHelpers.generate_payment_container(
        payment_list
    )
    expected_dict = {
        "money_transfer": data_containers.PaymentContainer(
            special_method=True,
            description="",
            excerpt="Money transfer for object",
            module_name="money_transfer",
            module_settings={},
            name="Money transfer",
            payment_id=1,
        ),
        "cod": data_containers.PaymentContainer(
            special_method=False,
            description="",
            excerpt="Cash on delivery - excerpt",
            module_name="cod",
            module_settings={},
            name="Cash on delivery",
            payment_id=2,
        ),
    }
    assert payment_formatted_dict == expected_dict


def test_generate_transport_container_multiple_payments_with_settings(dummy_request):
    payment_list = [
        PaymentObject(
            special_method=True,
            description="",
            excerpt="Money transfer for object",
            module_name="money_transfer",
            module_settings={},
            name="Money transfer",
            payment_id=1,
        ),
        PaymentObject(
            special_method=False,
            description="",
            excerpt="Cash on delivery - excerpt",
            module_name="cod",
            module_settings={"fee": 20},
            name="Cash on delivery",
            payment_id=2,
        ),
    ]
    payment_formatted_dict = helpers.PaymentHelpers.generate_payment_container(
        payment_list
    )
    expected_dict = {
        "money_transfer": data_containers.PaymentContainer(
            special_method=True,
            description="",
            excerpt="Money transfer for object",
            module_name="money_transfer",
            module_settings={},
            name="Money transfer",
            payment_id=1,
        ),
        "cod": data_containers.PaymentContainer(
            special_method=False,
            description="",
            excerpt="Cash on delivery - excerpt",
            module_name="cod",
            module_settings={"fee": 20},
            name="Cash on delivery",
            payment_id=2,
        ),
    }

    assert payment_formatted_dict == expected_dict
