from collections import namedtuple
from decimal import Decimal

from emporium import data_containers, helpers
from dataclasses import dataclass
from webob.multidict import MultiDict


# Simulating object returned by the SQLAlchemy
@dataclass
class TransportObject:
    special_method: bool
    description: str
    excerpt: str
    module_name: str
    module_settings: dict
    name: str
    transport_id: int
    slots: dict


@dataclass
class ProductToTransportSlotObject:
    local_settings: dict
    product_id: int
    product_transport_id: int
    transport_slot_id: int


@dataclass
class TransportSlotObject:
    slot_applicability: int
    slot_special_method: bool
    slot_description: str
    slot_excerpt: str
    slot_name: str
    slot_order: int
    transport_id: int
    transport_slot_id: int
    slot_settings: dict


def test_transport_with_product_local_settings_format_no_params(dummy_request):
    transport_dict = {}
    transport_formatted_dict = (
        helpers.TransportHelpers.transport_with_product_local_settings_format(
            transport_dict
        )
    )
    expected_dict = {}
    assert transport_formatted_dict == expected_dict


def transport_transport_with_product_local_settings_format_no_local(dummy_request):
    # fmt: off
    transport_dict = [
        ProductToTransportSlotObject(
            product_id=31555,
            product_transport_id=1,
            transport_slot_id=7,
            local_settings={},
        )
    ]
    #fmt : on
    transport_formatted_dict = helpers.TransportHelpers.transport_with_product_local_settings_format(
        transport_dict
    )
    # fmt: off
    expected_dict = {
        31555:{
        7: data_containers.ProductTransportContainer(
            product_transport_id=1, 
            local_settings={})}}
    # fmt: on
    assert transport_formatted_dict == expected_dict


def test_transport_with_product_local_settings_format_with_local(
    dummy_request,
):
    # fmt: off
    transport_dict = [
        ProductToTransportSlotObject(
            product_id=31555,
            product_transport_id=1,
            transport_slot_id=7,
            local_settings={'min_units':4},
        )
    ]
    #fmt : on
    transport_formatted_dict = helpers.TransportHelpers.transport_with_product_local_settings_format(
        transport_dict
    )
    # fmt: off
    expected_dict = {
        31555:{
        7: data_containers.ProductTransportContainer(
            product_transport_id=1,  
            local_settings={'min_units':4})}}
    # fmt: on
    assert transport_formatted_dict == expected_dict


def test_transport_with_product_local_settings_format_with_multiple_locals(
    dummy_request,
):
    # fmt: off
    transport_dict = [
        ProductToTransportSlotObject(
            product_id=31555,
            product_transport_id=1,
            transport_slot_id=7,
            local_settings={'min_units':4},
        ),
        ProductToTransportSlotObject(
            product_id=555,
            product_transport_id=2,
            transport_slot_id=3,
            local_settings={'max_weight':200},
        ),
    ]
    #fmt : on
    transport_formatted_dict = helpers.TransportHelpers.transport_with_product_local_settings_format(
        transport_dict
    )
    # fmt: off
    expected_dict = {
        31555:{
        7: data_containers.ProductTransportContainer(
            product_transport_id=1,  
            local_settings={'min_units':4})}
        ,
        555:{
        3: data_containers.ProductTransportContainer(
            product_transport_id=2,  
            local_settings={'max_weight':200})}
    }
    # fmt: on
    assert transport_formatted_dict == expected_dict


def test_transport_with_product_local_settings_format_with_multiple_transport_options(
    dummy_request,
):
    # fmt: off
    transport_dict = [
        ProductToTransportSlotObject(
            product_id=31555,
            product_transport_id=1,
            transport_slot_id=7,
            local_settings={'min_units':4},
        ),
        ProductToTransportSlotObject(
            product_id=31555,
            product_transport_id=2,
            transport_slot_id=3,
            local_settings={'max_weight':200},
        ),
    ]
    #fmt : on
    transport_formatted_dict = helpers.TransportHelpers.transport_with_product_local_settings_format(
        transport_dict
    )
    # fmt: off
    expected_dict = {
        31555:{
            7: data_containers.ProductTransportContainer(
                product_transport_id=1,  
                local_settings={'min_units':4}),    
            3: data_containers.ProductTransportContainer(
                product_transport_id=2,  
                local_settings={'max_weight':200})
        }
    }
    # fmt: on
    assert transport_formatted_dict == expected_dict


############


def test_transport_with_product_local_settings_format_no_params(dummy_request):
    transport_dict = {}
    transport_formatted_dict = (
        helpers.TransportHelpers.transport_with_product_local_settings_format(
            transport_dict
        )
    )
    expected_dict = {}
    assert transport_formatted_dict == expected_dict


def transport_transport_with_product_local_settings_format_no_local(dummy_request):
    # fmt: off
    transport_dict = [
        ProductToTransportSlotObject(
            product_id=31555,
            product_transport_id=1,
            transport_slot_id=7,
            local_settings={},
        )
    ]
    #fmt : on
    transport_formatted_dict = helpers.TransportHelpers.transport_with_product_local_settings_format(
        transport_dict
    )
    # fmt: off
    expected_dict = {
        31555:{
        7: data_containers.ProductTransportContainer(
            product_transport_id=1, 
            local_settings={})}}
    # fmt: on
    assert transport_formatted_dict == expected_dict


def test_transport_with_product_local_settings_format_with_local(
    dummy_request,
):
    # fmt: off
    transport_dict = [
        ProductToTransportSlotObject(
            product_id=31555,
            product_transport_id=1,
            transport_slot_id=7,
            local_settings={'min_units':4},
        )
    ]
    #fmt : on
    transport_formatted_dict = helpers.TransportHelpers.transport_with_product_local_settings_format(
        transport_dict
    )
    # fmt: off
    expected_dict = {
        31555:{
        7: data_containers.ProductTransportContainer(
            product_transport_id=1,  
            local_settings={'min_units':4})}}
    # fmt: on
    assert transport_formatted_dict == expected_dict


def test_transport_with_product_local_settings_format_with_multiple_locals(
    dummy_request,
):
    # fmt: off
    transport_dict = [
        ProductToTransportSlotObject(
            product_id=31555,
            product_transport_id=1,
            transport_slot_id=7,
            local_settings={'min_units':4},
        ),
        ProductToTransportSlotObject(
            product_id=555,
            product_transport_id=2,
            transport_slot_id=3,
            local_settings={'max_weight':200},
        ),
    ]
    #fmt : on
    transport_formatted_dict = helpers.TransportHelpers.transport_with_product_local_settings_format(
        transport_dict
    )
    # fmt: off
    expected_dict = {
        31555:{
        7: data_containers.ProductTransportContainer(
            product_transport_id=1,  
            local_settings={'min_units':4})}
        ,
        555:{
        3: data_containers.ProductTransportContainer(
            product_transport_id=2,  
            local_settings={'max_weight':200})}
    }
    # fmt: on
    assert transport_formatted_dict == expected_dict


def test_transport_with_product_local_settings_format_with_multiple_transport_options(
    dummy_request,
):
    # fmt: off
    transport_dict = [
        ProductToTransportSlotObject(
            product_id=31555,
            product_transport_id=1,
            transport_slot_id=7,
            local_settings={'min_units':4},
        ),
        ProductToTransportSlotObject(
            product_id=31555,
            product_transport_id=2,
            transport_slot_id=3,
            local_settings={'max_weight':200},
        ),
    ]
    #fmt : on
    transport_formatted_dict = helpers.TransportHelpers.transport_with_product_local_settings_format(
        transport_dict
    )
    # fmt: off
    expected_dict = {
        31555:{
            7: data_containers.ProductTransportContainer(
                product_transport_id=1,  
                local_settings={'min_units':4}),    
            3: data_containers.ProductTransportContainer(
                product_transport_id=2,  
                local_settings={'max_weight':200})
        }
    }
    # fmt: on
    assert transport_formatted_dict == expected_dict


def test_transport_container_blank(dummy_request):
    transport_dict = {}
    transport_formatted_dict = helpers.TransportHelpers.generate_transport_container(
        transport_dict
    )
    expected_dict = {}
    assert transport_formatted_dict == expected_dict


def test_transport_container(dummy_request):
    transport_dict = TransportObject(
        special_method=True,
        description="None",
        excerpt="",
        module_name="free_delivery",
        module_settings={"removed_transports": [6, 5, 10]},
        name="Darmowy transport",
        transport_id=11,
        slots={},
    )
    transport_formatted_dict = helpers.TransportHelpers.generate_transport_container(
        transport_dict
    )
    expected_dict = data_containers.TransportContainer(
        special_method=True,
        description="None",
        excerpt="",
        module_name="free_delivery",
        module_settings={"removed_transports": [6, 5, 10]},
        name="Darmowy transport",
        transport_id=11,
        slots={},
    )
    assert transport_formatted_dict == expected_dict


def test_transport_slot_container_blank(dummy_request):
    transport_dict = {}
    transport_formatted_dict = (
        helpers.TransportHelpers.generate_transport_slot_container(transport_dict)
    )
    expected_dict = {}
    assert transport_formatted_dict == expected_dict


def test_transport_slot_container(dummy_request):
    transport_dict = TransportSlotObject(
        slot_applicability=0,
        slot_special_method=True,
        slot_description=None,
        slot_excerpt=None,
        slot_name="asortyment w promocji",
        slot_order=1,
        transport_id=11,
        transport_slot_id=12,
        slot_settings={"min_value": "0"},
    )
    transport_formatted_dict = (
        helpers.TransportHelpers.generate_transport_slot_container(transport_dict)
    )
    expected_dict = data_containers.TransportSlotContainer(
        slot_applicability=0,
        slot_special_method=True,
        slot_description=None,
        slot_excerpt=None,
        slot_name="asortyment w promocji",
        slot_order=1,
        transport_id=11,
        transport_slot_id=12,
        slot_settings={"min_value": "0"},
    )
    assert transport_formatted_dict == expected_dict


def test_build_dictionary_of_transport_methods_blank(dummy_request):
    transport_dict = {}
    transport_formatted_dict = (
        helpers.TransportHelpers.build_dictionary_of_transport_methods(transport_dict)
    )
    expected_dict = {}
    assert transport_formatted_dict == expected_dict


def test_build_dictionary_of_transport_methods_blank(dummy_request):
    transport_dict = TransportObject(
        special_method=True,
        description="None",
        excerpt="",
        module_name="free_delivery",
        module_settings={"removed_transports": [6, 5, 10]},
        name="Darmowy transport",
        transport_id=11,
        slots={},
    )
    transport_formatted_dict = helpers.TransportHelpers.generate_transport_container(
        transport_dict
    )
    expected_dict = data_containers.TransportContainer(
        special_method=True,
        description="None",
        excerpt="",
        module_name="free_delivery",
        module_settings={"removed_transports": [6, 5, 10]},
        name="Darmowy transport",
        transport_id=11,
        slots={},
    )
    assert transport_formatted_dict == expected_dict
