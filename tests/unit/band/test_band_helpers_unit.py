from dataclasses import dataclass

from emporium import data_containers, helpers
from tests import unit


@dataclass
class Band:
    band_id: int
    band_type: str
    name: str
    seo_slug: str = None

def test_bands_filter_format_default_view(dummy_request, dbsession):
    band = set()
    band_list = [
        Band(190, None, "Eskalacja", "eskalacja"),
        Band(192, None, "Father’s Friends", "fathers-friends"),
        Band(183, None, "Fatum", "fatum"),
        Band(193, None, "INTERNAL QUIET", "internal-quiet"),
        Band(194, None, "Kers", "kers"),
        Band(191, None, "Kosa Śmierci", "kosa-smierci"),
        Band(188, None, "Snakedoctors", "snakedoctors"),
        Band(187, None, "Wojtek Oliński", "wojtek-olinski"),
    ]
    query_items = []
    band_dict = helpers.BandHelpers.bands_filter_format(band, band_list, query_items)
    mockup_dict = {
        190: data_containers.BandContainer(
            band_id=190,
            name="Eskalacja",
            url=None,
            band_type=None,
            seo_slug="eskalacja",
        ),
        192: data_containers.BandContainer(
            band_id=192,
            name="Father’s Friends",
            url=None,
            band_type=None,
            seo_slug="fathers-friends",
        ),
        183: data_containers.BandContainer(
            band_id=183, name="Fatum", url=None, band_type=None, seo_slug="fatum"
        ),
        193: data_containers.BandContainer(
            band_id=193,
            name="INTERNAL QUIET",
            url=None,
            band_type=None,
            seo_slug="internal-quiet",
        ),
        194: data_containers.BandContainer(
            band_id=194, name="Kers", url=None, band_type=None, seo_slug="kers"
        ),
        191: data_containers.BandContainer(
            band_id=191,
            name="Kosa Śmierci",
            url=None,
            band_type=None,
            seo_slug="kosa-smierci",
        ),
        188: data_containers.BandContainer(
            band_id=188,
            name="Snakedoctors",
            url=None,
            band_type=None,
            seo_slug="snakedoctors",
        ),
        187: data_containers.BandContainer(
            band_id=187,
            name="Wojtek Oliński",
            url=None,
            band_type=None,
            seo_slug="wojtek-olinski",
        ),
    }
    assert band_dict == mockup_dict


def test_bands_filter_format_filter_band(dummy_request, dbsession):
    band = {188}
    band_list = [
        Band(190, None, "Eskalacja", "eskalacja"),
        Band(192, None, "Father’s Friends", "fathers-friends"),
        Band(183, None, "Fatum", "fatum"),
        Band(193, None, "INTERNAL QUIET", "internal-quiet"),
        Band(194, None, "Kers", "kers"),
        Band(191, None, "Kosa Śmierci", "kosa-smierci"),
        Band(188, None, "Snakedoctors", "snakedoctors"),
        Band(187, None, "Wojtek Oliński", "wojtek-olinski"),
    ]
    query_items = [
        ("band", "188"),
        ("width_min", ""),
        ("width_max", ""),
        ("length_min", ""),
        ("length_max", ""),
    ]
    band_dict = helpers.BandHelpers.bands_filter_format(band, band_list, query_items)
    mockup_dict = {
        190: data_containers.BandContainer(
            band_id=190,
            name="Eskalacja",
            url=None,
            band_type=None,
            seo_slug="eskalacja",
        ),
        192: data_containers.BandContainer(
            band_id=192,
            name="Father’s Friends",
            url=None,
            band_type=None,
            seo_slug="fathers-friends",
        ),
        183: data_containers.BandContainer(
            band_id=183,
            name="Fatum",
            url=None,
            band_type=None,
            seo_slug="fatum",
        ),
        193: data_containers.BandContainer(
            band_id=193,
            name="INTERNAL QUIET",
            url=None,
            band_type=None,
            seo_slug="internal-quiet",
        ),
        194: data_containers.BandContainer(
            band_id=194,
            name="Kers",
            url=None,
            band_type=None,
            seo_slug="kers",
        ),
        191: data_containers.BandContainer(
            band_id=191,
            name="Kosa Śmierci",
            url=None,
            band_type=None,
            seo_slug="kosa-smierci",
        ),
        188: data_containers.BandContainer(
            band_id=188,
            name="Snakedoctors",
            url=[
                ("width_min", ""),
                ("width_max", ""),
                ("length_min", ""),
                ("length_max", ""),
            ],
            band_type=None,
            seo_slug="snakedoctors",
        ),
        187: data_containers.BandContainer(
            band_id=187,
            name="Wojtek Oliński",
            url=None,
            band_type=None,
            seo_slug="wojtek-olinski",
        ),
    }
    assert band_dict == mockup_dict


def test_bands_format_full_list(dummy_request):
    band_list = [
        Band(190, None, "Eskalacja", "eskalacja"),
        Band(192, None, "Father’s Friends", "fathers-friends"),
        Band(183, None, "Fatum", "fatum"),
        Band(193, None, "INTERNAL QUIET", "internal-quiet"),
        Band(194, None, "Kers", "kers"),
        Band(191, None, "Kosa Śmierci", "kosa-smierci"),
        Band(188, None, "Snakedoctors", "snakedoctors"),
        Band(187, None, "Wojtek Oliński", "wojtek-olinski"),
    ]
    band_dict = helpers.BandHelpers.bands_format(band_list)
    mockup_dict = {
        190: data_containers.BandContainer(
            band_id=190,
            name="Eskalacja",
            url=None,
            band_type=None,
            seo_slug="eskalacja",
        ),
        192: data_containers.BandContainer(
            band_id=192,
            name="Father’s Friends",
            url=None,
            band_type=None,
            seo_slug="fathers-friends",
        ),
        183: data_containers.BandContainer(
            band_id=183,
            name="Fatum",
            url=None,
            band_type=None,
            seo_slug="fatum",
        ),
        193: data_containers.BandContainer(
            band_id=193,
            name="INTERNAL QUIET",
            url=None,
            band_type=None,
            seo_slug="internal-quiet",
        ),
        194: data_containers.BandContainer(
            band_id=194,
            name="Kers",
            url=None,
            band_type=None,
            seo_slug="kers",
        ),
        191: data_containers.BandContainer(
            band_id=191,
            name="Kosa Śmierci",
            url=None,
            band_type=None,
            seo_slug="kosa-smierci",
        ),
        188: data_containers.BandContainer(
            band_id=188,
            name="Snakedoctors",
            url=None,
            band_type=None,
            seo_slug="snakedoctors",
        ),
        187: data_containers.BandContainer(
            band_id=187,
            name="Wojtek Oliński",
            url=None,
            band_type=None,
            seo_slug="wojtek-olinski",
        ),
    }
    assert band_dict == mockup_dict


def test_bands_format_empty_list(dummy_request):
    # Band = namedtuple("Band", "band_id name seo_slug")
    band_list = []
    band_dict = helpers.BandHelpers.bands_format(band_list)
    mockup_dict = {}
    assert band_dict == mockup_dict
