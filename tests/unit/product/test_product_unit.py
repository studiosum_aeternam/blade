from emporium import models, helpers, operations


def test_update_strip_from_data(dummy_request, dbsession):
    product_dict = {
        "ean": "  5909690592940  ",
        "model": "  Testing create form - missing description model   ",
        "name": "    Testing create form - missing description name   ",
    }
    for k, v in product_dict.items():
        product_dict[k] = operations.ProductOperations.strip_value(product_dict[k])

    assert product_dict == {
        "ean": "5909690592940",
        "model": "Testing create form - missing description model",
        "name": "Testing create form - missing description name",
    }


#     discount_dict = product.create_discount_dict_key_id(dummy_request)
#     assert discount_dict == {
#         discount_1.discount_id: (discount_1.discount_id, Decimal("15.00"), 1),
#         discount_2.discount_id: (discount_2.discount_id, Decimal("20.00"), 1),
#         discount_3.discount_id: (discount_3.discount_id, Decimal("30.00"), 1),
#         discount_4.discount_id: (discount_4.discount_id, Decimal("30.00"), 0),
#         discount_5.discount_id: (discount_5.discount_id, Decimal("20.00"), 0),
#         discount_6.discount_id: (discount_6.discount_id, Decimal("15.00"), 0),
#     }
