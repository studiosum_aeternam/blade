# from collections import namedtuple
from dataclasses import dataclass, field
from tests import unit

from emporium import data_containers, helpers


@dataclass
class AttributeGroupContainer:
    attribute_display_type_id: int
    attribute_group_id: int
    attribute_name: str
    name: str
    sort_order: int
    visible: bool
    attributes: dict = field(default_factory=dict)


@dataclass
class AttributeContainer:
    attribute_group_id: int
    attribute_id: int
    value: str


def test_attributes_extended_attribute_dict(dummy_request, dbsession):
    attribute_group_list = {
        1: data_containers.AttributeGroupContainer(
            4, 1, "format_group", "Grupa formatowa", 2, False, {}
        ),
        2: data_containers.AttributeGroupContainer(
            4, 2, "", "Rektyfikacja", 7, False, {}
        ),
        3: data_containers.AttributeGroupContainer(
            3, 3, "internal_external", "Zastosowanie", 14, False, {}
        ),
        4: data_containers.AttributeGroupContainer(
            1, 4, "", "Mrozoodporność", 5, False, {}
        ),
        6: data_containers.AttributeGroupContainer(
            3, 6, "room", "Pomieszczenia", 12, False, {}
        ),
        7: data_containers.AttributeGroupContainer(
            4, 7, "", "Powierzchnia", 6, False, {}
        ),
        8: data_containers.AttributeGroupContainer(
            3, 8, "colour", "Kolor wiodący", 13, False, {}
        ),
        9: data_containers.AttributeGroupContainer(
            1, 9, "availability", "Dostępność", 0, False, {}
        ),
        11: data_containers.AttributeGroupContainer(
            4, 11, "quality", "Gatunek", 3, False, {}
        ),
    }

    attribute_list = [
        AttributeContainer(1, 10, "60x60"),
        AttributeContainer(2, 15, "tak"),
        AttributeContainer(3, 18, "wewnątrz"),
        AttributeContainer(4, 21, "nie"),
        AttributeContainer(6, 30, "łazienka"),
        AttributeContainer(6, 31, "kuchnia"),
        AttributeContainer(6, 32, "salon"),
        AttributeContainer(7, 37, "mat"),
        AttributeContainer(8, 45, "szary"),
        AttributeContainer(8, 83, "mix"),
        AttributeContainer(9, 46, "gotowe do wysyłki"),
        AttributeContainer(11, 104, "pierwszy"),
    ]
    attributes = {}
    query_items = [
        ("collections", ""),
        ("availability", ""),  # ("attributes", "46"),
        ("width_min", ""),
        ("width_max", ""),
        ("length_min", ""),
        ("length_max", ""),
        ("price_min", ""),
        ("price_max", ""),
    ]
    mockup_dict = {
        1: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=1,
            attribute_name="format_group",
            name="Grupa formatowa",
            sort_order=2,
            visible=False,
            attributes={
                10: data_containers.AttributeContainer(
                    attribute_id=10, value="60x60", url=None
                )
            },
        ),
        2: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=2,
            attribute_name="",
            name="Rektyfikacja",
            sort_order=7,
            visible=False,
            attributes={
                15: data_containers.AttributeContainer(
                    attribute_id=15, value="tak", url=None
                )
            },
        ),
        3: data_containers.AttributeGroupContainer(
            attribute_display_type_id=3,
            attribute_group_id=3,
            attribute_name="internal_external",
            name="Zastosowanie",
            sort_order=14,
            visible=False,
            attributes={
                18: data_containers.AttributeContainer(
                    attribute_id=18, value="wewnątrz", url=None
                )
            },
        ),
        4: data_containers.AttributeGroupContainer(
            attribute_display_type_id=1,
            attribute_group_id=4,
            attribute_name="",
            name="Mrozoodporność",
            sort_order=5,
            visible=False,
            attributes={
                21: data_containers.AttributeContainer(
                    attribute_id=21, value="nie", url=None
                )
            },
        ),
        6: data_containers.AttributeGroupContainer(
            attribute_display_type_id=3,
            attribute_group_id=6,
            attribute_name="room",
            name="Pomieszczenia",
            sort_order=12,
            visible=False,
            attributes={
                30: data_containers.AttributeContainer(
                    attribute_id=30, value="łazienka", url=None
                ),
                31: data_containers.AttributeContainer(
                    attribute_id=31, value="kuchnia", url=None
                ),
                32: data_containers.AttributeContainer(
                    attribute_id=32, value="salon", url=None
                ),
            },
        ),
        7: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=7,
            attribute_name="",
            name="Powierzchnia",
            sort_order=6,
            visible=False,
            attributes={
                37: data_containers.AttributeContainer(
                    attribute_id=37, value="mat", url=None
                )
            },
        ),
        8: data_containers.AttributeGroupContainer(
            attribute_display_type_id=3,
            attribute_group_id=8,
            attribute_name="colour",
            name="Kolor wiodący",
            sort_order=13,
            visible=False,
            attributes={
                45: data_containers.AttributeContainer(
                    attribute_id=45, value="szary", url=None
                ),
                83: data_containers.AttributeContainer(
                    attribute_id=83, value="mix", url=None
                ),
            },
        ),
        9: data_containers.AttributeGroupContainer(
            attribute_display_type_id=1,
            attribute_group_id=9,
            attribute_name="availability",
            name="Dostępność",
            sort_order=0,
            visible=False,
            attributes={
                46: data_containers.AttributeContainer(
                    attribute_id=46, value="gotowe do wysyłki", url=None
                )
            },
        ),
        11: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=11,
            attribute_name="quality",
            name="Gatunek",
            sort_order=3,
            visible=False,
            attributes={
                104: data_containers.AttributeContainer(
                    attribute_id=104, value="pierwszy", url=None
                )
            },
        ),
    }

    attribute_dict = helpers.AttributeHelpers.build_extended_attribute_dict(
        attribute_group_list,
        attribute_list,
        attributes,
        query_items,
    )
    assert attribute_dict == mockup_dict


def test_attributes_filter_format_unfiltered_view(dummy_request, dbsession):
    attribute_group_list = [
        data_containers.AttributeGroupContainer(
            4, 1, "format_group", "Grupa formatowa", 2, False
        ),
        data_containers.AttributeGroupContainer(4, 2, "", "Rektyfikacja", 7, False),
        data_containers.AttributeGroupContainer(
            3, 3, "internal_external", "Zastosowanie", 14, False
        ),
        data_containers.AttributeGroupContainer(1, 4, "", "Mrozoodporność", 5, False),
        data_containers.AttributeGroupContainer(
            3, 6, "room", "Pomieszczenia", 12, False
        ),
        data_containers.AttributeGroupContainer(4, 7, "", "Powierzchnia", 6, False),
        data_containers.AttributeGroupContainer(
            3, 8, "colour", "Kolor wiodący", 13, False
        ),
        data_containers.AttributeGroupContainer(
            1, 9, "availability", "Dostępność", 0, False
        ),
        data_containers.AttributeGroupContainer(4, 11, "quality", "Gatunek", 3, False),
    ]

    attribute_list = [
        AttributeContainer(1, 10, "60x60"),
        AttributeContainer(2, 15, "tak"),
        AttributeContainer(3, 18, "wewnątrz"),
        AttributeContainer(4, 21, "nie"),
        AttributeContainer(6, 30, "łazienka"),
        AttributeContainer(6, 31, "kuchnia"),
        AttributeContainer(6, 32, "salon"),
        AttributeContainer(7, 37, "mat"),
        AttributeContainer(8, 45, "szary"),
        AttributeContainer(8, 83, "mix"),
        AttributeContainer(9, 46, "gotowe do wysyłki"),
        AttributeContainer(11, 104, "pierwszy"),
    ]
    attributes = {}
    query_items = [
        ("collections", ""),
        ("availability", ""),  # ("attributes", "46"),
        ("width_min", ""),
        ("width_max", ""),
        ("length_min", ""),
        ("length_max", ""),
        ("price_min", ""),
        ("price_max", ""),
    ]
    mockup_dict = {
        9: data_containers.AttributeGroupContainer(
            attribute_display_type_id=1,
            attribute_group_id=9,
            name="Dostępność",
            attribute_name="availability",
            sort_order=0,
            visible=False,
            attributes={
                46: data_containers.AttributeContainer(
                    attribute_id=46,
                    value="gotowe do wysyłki",
                    url=None,
                )
            },
        ),
        1: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=1,
            name="Grupa formatowa",
            sort_order=2,
            visible=False,
            attribute_name="format_group",
            attributes={
                10: data_containers.AttributeContainer(
                    attribute_id=10,
                    value="60x60",
                    url=None,
                )
            },
        ),
        11: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=11,
            name="Gatunek",
            sort_order=3,
            visible=False,
            attribute_name="quality",
            attributes={
                104: data_containers.AttributeContainer(
                    attribute_id=104, value="pierwszy", url=None
                )
            },
        ),
        4: data_containers.AttributeGroupContainer(
            attribute_display_type_id=1,
            attribute_group_id=4,
            name="Mrozoodporność",
            sort_order=5,
            visible=False,
            attribute_name="",
            attributes={
                21: data_containers.AttributeContainer(
                    attribute_id=21, value="nie", url=None
                )
            },
        ),
        7: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=7,
            name="Powierzchnia",
            sort_order=6,
            visible=False,
            attribute_name="",
            attributes={
                37: data_containers.AttributeContainer(
                    attribute_id=37, value="mat", url=None
                )
            },
        ),
        2: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=2,
            name="Rektyfikacja",
            sort_order=7,
            visible=False,
            attribute_name="",
            attributes={
                15: data_containers.AttributeContainer(
                    attribute_id=15, value="tak", url=None
                )
            },
        ),
        6: data_containers.AttributeGroupContainer(
            attribute_display_type_id=3,
            attribute_group_id=6,
            name="Pomieszczenia",
            sort_order=12,
            visible=False,
            attribute_name="room",
            attributes={
                31: data_containers.AttributeContainer(
                    attribute_id=31, value="kuchnia", url=None
                ),
                30: data_containers.AttributeContainer(
                    attribute_id=30, value="łazienka", url=None
                ),
                32: data_containers.AttributeContainer(
                    attribute_id=32, value="salon", url=None
                ),
            },
        ),
        8: data_containers.AttributeGroupContainer(
            attribute_display_type_id=3,
            attribute_group_id=8,
            name="Kolor wiodący",
            sort_order=13,
            visible=False,
            attribute_name="colour",
            attributes={
                83: data_containers.AttributeContainer(
                    attribute_id=83, value="mix", url=None
                ),
                45: data_containers.AttributeContainer(
                    attribute_id=45, value="szary", url=None
                ),
            },
        ),
        3: data_containers.AttributeGroupContainer(
            attribute_display_type_id=3,
            attribute_group_id=3,
            name="Zastosowanie",
            sort_order=14,
            visible=False,
            attribute_name="internal_external",
            attributes={
                18: data_containers.AttributeContainer(
                    attribute_id=18, value="wewnątrz", url=None
                )
            },
        ),
    }
    attribute_dict = helpers.AttributeHelpers.attributes_filter_format(
        attribute_group_list,
        attribute_list,
        attributes,
        query_items,
    )
    assert attribute_dict == mockup_dict


def test_attributes_filter_format_filtered_view(dummy_request, dbsession):
    attribute_group_list = [
        data_containers.AttributeGroupContainer(
            1, 9, "availability", "Dostępność", 0, False
        ),
        data_containers.AttributeGroupContainer(
            4, 1, "format_group", "Grupa formatowa", 2, False
        ),
    ]
    attribute_list = [
        AttributeContainer(9, 46, "gotowe do wysyłki"),
        AttributeContainer(1, 10, "60x60"),
    ]
    attributes = {46}
    query_items = [
        ("collections", "1855"),
        ("availability", "46"),  # ("attributes", "46"),
        ("width_min", ""),
        ("width_max", ""),
        ("length_min", ""),
        ("length_max", ""),
        ("price_min", ""),
        ("price_max", ""),
    ]
    mockup_dict = {
        9: data_containers.AttributeGroupContainer(
            attribute_display_type_id=1,
            attribute_group_id=9,
            name="Dostępność",
            attribute_name="availability",
            sort_order=0,
            visible=True,
            attributes={
                46: data_containers.AttributeContainer(
                    attribute_id=46,
                    value="gotowe do wysyłki",
                    url=[
                        ("collections", "1855"),
                        ("availability", "46"),
                        ("width_min", ""),
                        ("width_max", ""),
                        ("length_min", ""),
                        ("length_max", ""),
                        ("price_min", ""),
                        ("price_max", ""),
                    ],
                )
            },
        ),
        1: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=1,
            name="Grupa formatowa",
            sort_order=2,
            visible=False,
            attribute_name="format_group",
            attributes={
                10: data_containers.AttributeContainer(
                    attribute_id=10,
                    value="60x60",
                    url=None,
                )
            },
        ),
    }
    attribute_dict = helpers.AttributeHelpers.attributes_filter_format(
        attribute_group_list,
        attribute_list,
        attributes,
        query_items,
    )
    assert attribute_dict == mockup_dict


def test_attributes_filter_format_remove_unused_attribute_group(
    dummy_request, dbsession
):
    attribute_group_list = [
        data_containers.AttributeGroupContainer(
            1, 9, "availability", "Dostępność", 0, False
        ),
        data_containers.AttributeGroupContainer(
            4, 10, "", "Antypoślizgowość", 1, False
        ),
    ]

    attribute_list = [
        AttributeContainer(9, 46, "gotowe do wysyłki"),
    ]
    attributes = {46}
    query_items = [
        ("collections", "1855"),
        ("availability", "46"),  # ("attributes", "46"),
        ("width_min", ""),
        ("width_max", ""),
        ("length_min", ""),
        ("length_max", ""),
        ("price_min", ""),
        ("price_max", ""),
    ]
    mockup_dict = {
        9: data_containers.AttributeGroupContainer(
            attribute_display_type_id=1,
            attribute_group_id=9,
            name="Dostępność",
            attribute_name="availability",
            sort_order=0,
            visible=True,
            attributes={
                46: data_containers.AttributeContainer(
                    attribute_id=46,
                    value="gotowe do wysyłki",
                    url=[
                        ("collections", "1855"),
                        ("availability", "46"),
                        ("width_min", ""),
                        ("width_max", ""),
                        ("length_min", ""),
                        ("length_max", ""),
                        ("price_min", ""),
                        ("price_max", ""),
                    ],
                )
            },
        ),
    }
    attribute_dict = helpers.AttributeHelpers.attributes_filter_format(
        attribute_group_list,
        attribute_list,
        attributes,
        query_items,
    )
    assert attribute_dict == mockup_dict


def test_attributes_filter_format_non_filtered_view(dummy_request, dbsession):
    attribute_group_list = [
        data_containers.AttributeGroupContainer(
            1, 9, "availability", "Dostępność", 0, False
        ),
        data_containers.AttributeGroupContainer(
            4, 10, "", "Antypoślizgowość", 1, False
        ),
        data_containers.AttributeGroupContainer(
            4, 1, "format_group", "Grupa formatowa", 2, False
        ),
        data_containers.AttributeGroupContainer(4, 11, "quality", "Gatunek", 3, False),
        data_containers.AttributeGroupContainer(
            4, 5, "", "Klasa ścieralności", 4, False
        ),
        data_containers.AttributeGroupContainer(1, 4, "", "Mrozoodporność", 5, False),
        data_containers.AttributeGroupContainer(4, 7, "", "Powierzchnia", 6, False),
        data_containers.AttributeGroupContainer(4, 2, "", "Rektyfikacja", 7, False),
        data_containers.AttributeGroupContainer(
            3, 6, "room", "Pomieszczenia", 12, False
        ),
        data_containers.AttributeGroupContainer(
            3, 8, "colour", "Kolor wiodący", 13, False
        ),
        data_containers.AttributeGroupContainer(
            3, 3, "internal_external", "Zastosowanie", 14, False
        ),
    ]
    attribute_list = [
        AttributeContainer(8, 146, "antracyt"),
        AttributeContainer(11, 105, "drugi"),
        AttributeContainer(9, 46, "gotowe do wysyłki"),
        AttributeContainer(5, 23, "PEI I"),
        AttributeContainer(11, 104, "pierwszy"),
        AttributeContainer(10, 78, "R9"),
        AttributeContainer(8, 40, "beż"),
        AttributeContainer(5, 24, "PEI II"),
        AttributeContainer(10, 79, "R10"),
        AttributeContainer(9, 47, "1-3 dni robocze"),
        AttributeContainer(8, 128, "beż / brąz"),
        AttributeContainer(5, 25, "PEI III"),
        AttributeContainer(10, 80, "R11"),
        AttributeContainer(1, 151, "1,5x75"),
        AttributeContainer(9, 52, "5 dni roboczych"),
        AttributeContainer(8, 41, "biały"),
        AttributeContainer(5, 26, "PEI IV"),
        AttributeContainer(10, 81, "R12"),
        AttributeContainer(9, 48, "7 dni roboczych"),
        AttributeContainer(8, 132, "biały / czarny"),
        AttributeContainer(5, 27, "PEI V"),
        AttributeContainer(10, 82, "R13"),
        AttributeContainer(9, 49, "14 dni roboczych"),
        AttributeContainer(8, 97, "bordo"),
        AttributeContainer(5, 29, "nie podano"),
        AttributeContainer(9, 50, "21 dni roboczych"),
        AttributeContainer(1, 157, "5x25"),
        AttributeContainer(8, 42, "brąz"),
        AttributeContainer(5, 28, "nie dotyczy"),
        AttributeContainer(1, 156, "6,5x40"),
        AttributeContainer(8, 43, "czarny"),
        AttributeContainer(9, 51, "pytaj przed zakupem"),
        AttributeContainer(1, 141, "7,4x30"),
        AttributeContainer(8, 54, "czerwony"),
        AttributeContainer(9, 53, "niedostępne"),
        AttributeContainer(1, 155, "7,5x15"),
        AttributeContainer(8, 77, "ecru"),
        AttributeContainer(1, 2, "10x10"),
        AttributeContainer(8, 57, "fioletowy"),
        AttributeContainer(1, 3, "10x20"),
        AttributeContainer(8, 44, "grafit"),
        AttributeContainer(1, 63, "10x30"),
        AttributeContainer(8, 76, "granat"),
        AttributeContainer(1, 64, "10x40"),
        AttributeContainer(8, 99, "krem"),
        AttributeContainer(1, 126, "10x60"),
        AttributeContainer(8, 133, "miedziany"),
        AttributeContainer(1, 88, "15x15"),
        AttributeContainer(8, 83, "mix"),
        AttributeContainer(1, 142, "15x30"),
        AttributeContainer(8, 58, "niebieski"),
        AttributeContainer(1, 89, "15x45"),
        AttributeContainer(8, 149, "oliwkowy"),
        AttributeContainer(1, 72, "15x60"),
        AttributeContainer(8, 59, "pomarańczowy"),
        AttributeContainer(1, 92, "15x90"),
        AttributeContainer(8, 60, "różowy"),
        AttributeContainer(8, 62, "srebrny"),
        AttributeContainer(1, 117, "15x120"),
        AttributeContainer(8, 45, "szary"),
        AttributeContainer(1, 143, "17,5x80"),
        AttributeContainer(8, 130, "turkus"),
        AttributeContainer(8, 55, "zielony"),
        AttributeContainer(8, 61, "złoty"),
        AttributeContainer(1, 4, "20x20"),
        AttributeContainer(8, 56, "żółty"),
        AttributeContainer(1, 65, "20x40"),
        AttributeContainer(1, 66, "20x50"),
        AttributeContainer(1, 67, "20x60"),
        AttributeContainer(1, 137, "20x80"),
        AttributeContainer(1, 68, "20x90"),
        AttributeContainer(1, 12, "20x120"),
        AttributeContainer(1, 136, "20x150"),
        AttributeContainer(1, 125, "20x160"),
        AttributeContainer(1, 158, "20x180"),
        AttributeContainer(1, 69, "25x25"),
        AttributeContainer(1, 70, "25x50"),
        AttributeContainer(1, 98, "25x60"),
        AttributeContainer(1, 7, "25x75"),
        AttributeContainer(1, 5, "30x30"),
        AttributeContainer(1, 6, "30x60"),
        AttributeContainer(1, 85, "30x75"),
        AttributeContainer(1, 8, "30x90"),
        AttributeContainer(1, 107, "30x100"),
        AttributeContainer(1, 73, "30x120"),
        AttributeContainer(1, 139, "30x180"),
        AttributeContainer(1, 90, "40x40"),
        AttributeContainer(1, 94, "40x80"),
        AttributeContainer(1, 13, "40x120"),
        AttributeContainer(1, 9, "45x45"),
        AttributeContainer(1, 86, "45x90"),
        AttributeContainer(1, 103, "50x100"),
        AttributeContainer(1, 102, "50x150"),
        AttributeContainer(1, 10, "60x60"),
        AttributeContainer(1, 147, "60x90"),
        AttributeContainer(1, 14, "60x120"),
        AttributeContainer(1, 11, "80x80"),
        AttributeContainer(1, 153, "75x90"),
        AttributeContainer(1, 134, "75x150"),
        AttributeContainer(1, 138, "80x160"),
        AttributeContainer(1, 96, "90x90"),
        AttributeContainer(1, 127, "90x180"),
        AttributeContainer(1, 101, "100x100"),
        AttributeContainer(1, 87, "120x120"),
        AttributeContainer(1, 129, "120x240"),
        AttributeContainer(1, 1, "inny"),
        AttributeContainer(1, 160, "8x60"),
        AttributeContainer(7, 145, "lappato+mat"),
        AttributeContainer(7, 108, "3D"),
        AttributeContainer(6, 31, "kuchnia"),
        AttributeContainer(7, 84, "lapatto"),
        AttributeContainer(6, 30, "łazienka"),
        AttributeContainer(7, 37, "mat"),
        AttributeContainer(7, 112, "metalizowana"),
        AttributeContainer(7, 119, "mikrogranilia"),
        AttributeContainer(3, 19, "na zewnątrz"),
        AttributeContainer(4, 21, "nie"),
        AttributeContainer(2, 16, "nie"),
        AttributeContainer(2, 17, "nie dotyczy"),
        AttributeContainer(4, 22, "nie dotyczy"),
        AttributeContainer(7, 34, "polerowana"),
        AttributeContainer(7, 95, "polerowana matowa"),
        AttributeContainer(7, 35, "półpolerowana"),
        AttributeContainer(7, 36, "połysk"),
        AttributeContainer(7, 71, "połysk+mat"),
        AttributeContainer(6, 32, "salon"),
        AttributeContainer(7, 100, "satyna"),
        AttributeContainer(7, 38, "struktura"),
        AttributeContainer(7, 74, "struktura+mat"),
        AttributeContainer(7, 113, "struktura+metalizowana"),
        AttributeContainer(7, 120, "struktura+mikrogranilia"),
        AttributeContainer(7, 75, "struktura+połysk"),
        AttributeContainer(7, 106, "struktura+satyna"),
        AttributeContainer(7, 39, "szkło"),
        AttributeContainer(4, 20, "tak"),
        AttributeContainer(2, 15, "tak"),
        AttributeContainer(6, 33, "taras"),
        AttributeContainer(3, 18, "wewnątrz"),
    ]
    attributes = {}
    query_items = []
    mockup_dict = {
        9: data_containers.AttributeGroupContainer(
            attribute_display_type_id=1,
            attribute_group_id=9,
            name="Dostępność",
            sort_order=0,
            visible=False,
            attribute_name="availability",
            attributes={
                46: data_containers.AttributeContainer(
                    attribute_id=46, value="gotowe do wysyłki", url=None
                ),
                47: data_containers.AttributeContainer(
                    attribute_id=47, value="1-3 dni robocze", url=None
                ),
                52: data_containers.AttributeContainer(
                    attribute_id=52, value="5 dni roboczych", url=None
                ),
                48: data_containers.AttributeContainer(
                    attribute_id=48, value="7 dni roboczych", url=None
                ),
                49: data_containers.AttributeContainer(
                    attribute_id=49, value="14 dni roboczych", url=None
                ),
                50: data_containers.AttributeContainer(
                    attribute_id=50, value="21 dni roboczych", url=None
                ),
                51: data_containers.AttributeContainer(
                    attribute_id=51, value="pytaj przed zakupem", url=None
                ),
                53: data_containers.AttributeContainer(
                    attribute_id=53, value="niedostępne", url=None
                ),
            },
        ),
        10: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=10,
            name="Antypoślizgowość",
            sort_order=1,
            visible=False,
            attribute_name="",
            attributes={
                78: data_containers.AttributeContainer(
                    attribute_id=78, value="R9", url=None
                ),
                79: data_containers.AttributeContainer(
                    attribute_id=79, value="R10", url=None
                ),
                80: data_containers.AttributeContainer(
                    attribute_id=80, value="R11", url=None
                ),
                81: data_containers.AttributeContainer(
                    attribute_id=81, value="R12", url=None
                ),
                82: data_containers.AttributeContainer(
                    attribute_id=82, value="R13", url=None
                ),
            },
        ),
        1: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=1,
            name="Grupa formatowa",
            sort_order=2,
            visible=False,
            attribute_name="format_group",
            attributes={
                151: data_containers.AttributeContainer(
                    attribute_id=151, value="1,5x75", url=None
                ),
                157: data_containers.AttributeContainer(
                    attribute_id=157, value="5x25", url=None
                ),
                156: data_containers.AttributeContainer(
                    attribute_id=156, value="6,5x40", url=None
                ),
                141: data_containers.AttributeContainer(
                    attribute_id=141, value="7,4x30", url=None
                ),
                155: data_containers.AttributeContainer(
                    attribute_id=155, value="7,5x15", url=None
                ),
                2: data_containers.AttributeContainer(
                    attribute_id=2, value="10x10", url=None
                ),
                3: data_containers.AttributeContainer(
                    attribute_id=3, value="10x20", url=None
                ),
                63: data_containers.AttributeContainer(
                    attribute_id=63, value="10x30", url=None
                ),
                64: data_containers.AttributeContainer(
                    attribute_id=64, value="10x40", url=None
                ),
                126: data_containers.AttributeContainer(
                    attribute_id=126, value="10x60", url=None
                ),
                88: data_containers.AttributeContainer(
                    attribute_id=88, value="15x15", url=None
                ),
                142: data_containers.AttributeContainer(
                    attribute_id=142, value="15x30", url=None
                ),
                89: data_containers.AttributeContainer(
                    attribute_id=89, value="15x45", url=None
                ),
                72: data_containers.AttributeContainer(
                    attribute_id=72, value="15x60", url=None
                ),
                92: data_containers.AttributeContainer(
                    attribute_id=92, value="15x90", url=None
                ),
                117: data_containers.AttributeContainer(
                    attribute_id=117, value="15x120", url=None
                ),
                143: data_containers.AttributeContainer(
                    attribute_id=143, value="17,5x80", url=None
                ),
                4: data_containers.AttributeContainer(
                    attribute_id=4, value="20x20", url=None
                ),
                65: data_containers.AttributeContainer(
                    attribute_id=65, value="20x40", url=None
                ),
                66: data_containers.AttributeContainer(
                    attribute_id=66, value="20x50", url=None
                ),
                67: data_containers.AttributeContainer(
                    attribute_id=67, value="20x60", url=None
                ),
                137: data_containers.AttributeContainer(
                    attribute_id=137, value="20x80", url=None
                ),
                68: data_containers.AttributeContainer(
                    attribute_id=68, value="20x90", url=None
                ),
                12: data_containers.AttributeContainer(
                    attribute_id=12, value="20x120", url=None
                ),
                136: data_containers.AttributeContainer(
                    attribute_id=136, value="20x150", url=None
                ),
                125: data_containers.AttributeContainer(
                    attribute_id=125, value="20x160", url=None
                ),
                158: data_containers.AttributeContainer(
                    attribute_id=158, value="20x180", url=None
                ),
                69: data_containers.AttributeContainer(
                    attribute_id=69, value="25x25", url=None
                ),
                70: data_containers.AttributeContainer(
                    attribute_id=70, value="25x50", url=None
                ),
                98: data_containers.AttributeContainer(
                    attribute_id=98, value="25x60", url=None
                ),
                7: data_containers.AttributeContainer(
                    attribute_id=7, value="25x75", url=None
                ),
                5: data_containers.AttributeContainer(
                    attribute_id=5, value="30x30", url=None
                ),
                6: data_containers.AttributeContainer(
                    attribute_id=6, value="30x60", url=None
                ),
                85: data_containers.AttributeContainer(
                    attribute_id=85, value="30x75", url=None
                ),
                8: data_containers.AttributeContainer(
                    attribute_id=8, value="30x90", url=None
                ),
                107: data_containers.AttributeContainer(
                    attribute_id=107, value="30x100", url=None
                ),
                73: data_containers.AttributeContainer(
                    attribute_id=73, value="30x120", url=None
                ),
                139: data_containers.AttributeContainer(
                    attribute_id=139, value="30x180", url=None
                ),
                90: data_containers.AttributeContainer(
                    attribute_id=90, value="40x40", url=None
                ),
                94: data_containers.AttributeContainer(
                    attribute_id=94, value="40x80", url=None
                ),
                13: data_containers.AttributeContainer(
                    attribute_id=13, value="40x120", url=None
                ),
                9: data_containers.AttributeContainer(
                    attribute_id=9, value="45x45", url=None
                ),
                86: data_containers.AttributeContainer(
                    attribute_id=86, value="45x90", url=None
                ),
                103: data_containers.AttributeContainer(
                    attribute_id=103, value="50x100", url=None
                ),
                102: data_containers.AttributeContainer(
                    attribute_id=102, value="50x150", url=None
                ),
                10: data_containers.AttributeContainer(
                    attribute_id=10, value="60x60", url=None
                ),
                147: data_containers.AttributeContainer(
                    attribute_id=147, value="60x90", url=None
                ),
                14: data_containers.AttributeContainer(
                    attribute_id=14, value="60x120", url=None
                ),
                11: data_containers.AttributeContainer(
                    attribute_id=11, value="80x80", url=None
                ),
                153: data_containers.AttributeContainer(
                    attribute_id=153, value="75x90", url=None
                ),
                134: data_containers.AttributeContainer(
                    attribute_id=134, value="75x150", url=None
                ),
                138: data_containers.AttributeContainer(
                    attribute_id=138, value="80x160", url=None
                ),
                96: data_containers.AttributeContainer(
                    attribute_id=96, value="90x90", url=None
                ),
                127: data_containers.AttributeContainer(
                    attribute_id=127, value="90x180", url=None
                ),
                101: data_containers.AttributeContainer(
                    attribute_id=101, value="100x100", url=None
                ),
                87: data_containers.AttributeContainer(
                    attribute_id=87, value="120x120", url=None
                ),
                129: data_containers.AttributeContainer(
                    attribute_id=129, value="120x240", url=None
                ),
                1: data_containers.AttributeContainer(
                    attribute_id=1, value="inny", url=None
                ),
                160: data_containers.AttributeContainer(
                    attribute_id=160, value="8x60", url=None
                ),
            },
        ),
        11: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=11,
            name="Gatunek",
            sort_order=3,
            visible=False,
            attribute_name="quality",
            attributes={
                105: data_containers.AttributeContainer(
                    attribute_id=105, value="drugi", url=None
                ),
                104: data_containers.AttributeContainer(
                    attribute_id=104, value="pierwszy", url=None
                ),
            },
        ),
        5: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=5,
            name="Klasa ścieralności",
            sort_order=4,
            visible=False,
            attribute_name="",
            attributes={
                23: data_containers.AttributeContainer(
                    attribute_id=23, value="PEI I", url=None
                ),
                24: data_containers.AttributeContainer(
                    attribute_id=24, value="PEI II", url=None
                ),
                25: data_containers.AttributeContainer(
                    attribute_id=25, value="PEI III", url=None
                ),
                26: data_containers.AttributeContainer(
                    attribute_id=26, value="PEI IV", url=None
                ),
                27: data_containers.AttributeContainer(
                    attribute_id=27, value="PEI V", url=None
                ),
                29: data_containers.AttributeContainer(
                    attribute_id=29, value="nie podano", url=None
                ),
                28: data_containers.AttributeContainer(
                    attribute_id=28, value="nie dotyczy", url=None
                ),
            },
        ),
        4: data_containers.AttributeGroupContainer(
            attribute_display_type_id=1,
            attribute_group_id=4,
            name="Mrozoodporność",
            sort_order=5,
            visible=False,
            attribute_name="",
            attributes={
                21: data_containers.AttributeContainer(
                    attribute_id=21, value="nie", url=None
                ),
                22: data_containers.AttributeContainer(
                    attribute_id=22, value="nie dotyczy", url=None
                ),
                20: data_containers.AttributeContainer(
                    attribute_id=20, value="tak", url=None
                ),
            },
        ),
        7: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=7,
            name="Powierzchnia",
            sort_order=6,
            visible=False,
            attribute_name="",
            attributes={
                145: data_containers.AttributeContainer(
                    attribute_id=145, value="lappato+mat", url=None
                ),
                108: data_containers.AttributeContainer(
                    attribute_id=108, value="3D", url=None
                ),
                84: data_containers.AttributeContainer(
                    attribute_id=84, value="lapatto", url=None
                ),
                37: data_containers.AttributeContainer(
                    attribute_id=37, value="mat", url=None
                ),
                112: data_containers.AttributeContainer(
                    attribute_id=112, value="metalizowana", url=None
                ),
                119: data_containers.AttributeContainer(
                    attribute_id=119, value="mikrogranilia", url=None
                ),
                34: data_containers.AttributeContainer(
                    attribute_id=34, value="polerowana", url=None
                ),
                95: data_containers.AttributeContainer(
                    attribute_id=95, value="polerowana matowa", url=None
                ),
                35: data_containers.AttributeContainer(
                    attribute_id=35, value="półpolerowana", url=None
                ),
                36: data_containers.AttributeContainer(
                    attribute_id=36, value="połysk", url=None
                ),
                71: data_containers.AttributeContainer(
                    attribute_id=71, value="połysk+mat", url=None
                ),
                100: data_containers.AttributeContainer(
                    attribute_id=100, value="satyna", url=None
                ),
                38: data_containers.AttributeContainer(
                    attribute_id=38, value="struktura", url=None
                ),
                74: data_containers.AttributeContainer(
                    attribute_id=74, value="struktura+mat", url=None
                ),
                113: data_containers.AttributeContainer(
                    attribute_id=113, value="struktura+metalizowana", url=None
                ),
                120: data_containers.AttributeContainer(
                    attribute_id=120, value="struktura+mikrogranilia", url=None
                ),
                75: data_containers.AttributeContainer(
                    attribute_id=75, value="struktura+połysk", url=None
                ),
                106: data_containers.AttributeContainer(
                    attribute_id=106, value="struktura+satyna", url=None
                ),
                39: data_containers.AttributeContainer(
                    attribute_id=39, value="szkło", url=None
                ),
            },
        ),
        2: data_containers.AttributeGroupContainer(
            attribute_display_type_id=4,
            attribute_group_id=2,
            name="Rektyfikacja",
            sort_order=7,
            visible=False,
            attribute_name="",
            attributes={
                16: data_containers.AttributeContainer(
                    attribute_id=16, value="nie", url=None
                ),
                17: data_containers.AttributeContainer(
                    attribute_id=17, value="nie dotyczy", url=None
                ),
                15: data_containers.AttributeContainer(
                    attribute_id=15, value="tak", url=None
                ),
            },
        ),
        6: data_containers.AttributeGroupContainer(
            attribute_display_type_id=3,
            attribute_group_id=6,
            name="Pomieszczenia",
            sort_order=12,
            visible=False,
            attribute_name="room",
            attributes={
                31: data_containers.AttributeContainer(
                    attribute_id=31, value="kuchnia", url=None
                ),
                30: data_containers.AttributeContainer(
                    attribute_id=30, value="łazienka", url=None
                ),
                32: data_containers.AttributeContainer(
                    attribute_id=32, value="salon", url=None
                ),
                33: data_containers.AttributeContainer(
                    attribute_id=33, value="taras", url=None
                ),
            },
        ),
        8: data_containers.AttributeGroupContainer(
            attribute_display_type_id=3,
            attribute_group_id=8,
            name="Kolor wiodący",
            sort_order=13,
            visible=False,
            attribute_name="colour",
            attributes={
                146: data_containers.AttributeContainer(
                    attribute_id=146, value="antracyt", url=None
                ),
                40: data_containers.AttributeContainer(
                    attribute_id=40, value="beż", url=None
                ),
                128: data_containers.AttributeContainer(
                    attribute_id=128, value="beż / brąz", url=None
                ),
                41: data_containers.AttributeContainer(
                    attribute_id=41, value="biały", url=None
                ),
                132: data_containers.AttributeContainer(
                    attribute_id=132, value="biały / czarny", url=None
                ),
                97: data_containers.AttributeContainer(
                    attribute_id=97, value="bordo", url=None
                ),
                42: data_containers.AttributeContainer(
                    attribute_id=42, value="brąz", url=None
                ),
                43: data_containers.AttributeContainer(
                    attribute_id=43, value="czarny", url=None
                ),
                54: data_containers.AttributeContainer(
                    attribute_id=54, value="czerwony", url=None
                ),
                77: data_containers.AttributeContainer(
                    attribute_id=77, value="ecru", url=None
                ),
                57: data_containers.AttributeContainer(
                    attribute_id=57, value="fioletowy", url=None
                ),
                44: data_containers.AttributeContainer(
                    attribute_id=44, value="grafit", url=None
                ),
                76: data_containers.AttributeContainer(
                    attribute_id=76, value="granat", url=None
                ),
                99: data_containers.AttributeContainer(
                    attribute_id=99, value="krem", url=None
                ),
                133: data_containers.AttributeContainer(
                    attribute_id=133, value="miedziany", url=None
                ),
                83: data_containers.AttributeContainer(
                    attribute_id=83, value="mix", url=None
                ),
                58: data_containers.AttributeContainer(
                    attribute_id=58, value="niebieski", url=None
                ),
                149: data_containers.AttributeContainer(
                    attribute_id=149, value="oliwkowy", url=None
                ),
                59: data_containers.AttributeContainer(
                    attribute_id=59, value="pomarańczowy", url=None
                ),
                60: data_containers.AttributeContainer(
                    attribute_id=60, value="różowy", url=None
                ),
                62: data_containers.AttributeContainer(
                    attribute_id=62, value="srebrny", url=None
                ),
                45: data_containers.AttributeContainer(
                    attribute_id=45, value="szary", url=None
                ),
                130: data_containers.AttributeContainer(
                    attribute_id=130, value="turkus", url=None
                ),
                55: data_containers.AttributeContainer(
                    attribute_id=55, value="zielony", url=None
                ),
                61: data_containers.AttributeContainer(
                    attribute_id=61, value="złoty", url=None
                ),
                56: data_containers.AttributeContainer(
                    attribute_id=56, value="żółty", url=None
                ),
            },
        ),
        3: data_containers.AttributeGroupContainer(
            attribute_display_type_id=3,
            attribute_group_id=3,
            name="Zastosowanie",
            sort_order=14,
            visible=False,
            attribute_name="internal_external",
            attributes={
                19: data_containers.AttributeContainer(
                    attribute_id=19, value="na zewnątrz", url=None
                ),
                18: data_containers.AttributeContainer(
                    attribute_id=18, value="wewnątrz", url=None
                ),
            },
        ),
    }
    attribute_dict = helpers.AttributeHelpers.attributes_filter_format(
        attribute_group_list,
        attribute_list,
        attributes,
        query_items,
    )
    assert attribute_dict == mockup_dict


def test_create_attribute_dict_id_value(dummy_request, dbsession):
    attribute_list = [
        AttributeContainer(1, 146, "antracyt"),
        AttributeContainer(2, 105, "drugi"),
        AttributeContainer(6, 46, "gotowe do wysyłki"),
        AttributeContainer(3, 23, "PEI I"),
        AttributeContainer(2, 104, "pierwszy"),
        AttributeContainer(4, 78, "R9"),
        AttributeContainer(1, 40, "beż"),
        AttributeContainer(4, 24, "PEI II"),
        AttributeContainer(4, 79, "R10"),
        AttributeContainer(6, 47, "1-3 dni robocze"),
        AttributeContainer(1, 128, "beż / brąz"),
        AttributeContainer(3, 25, "PEI III"),
        AttributeContainer(4, 80, "R11"),
        AttributeContainer(5, 151, "1,5x75"),
    ]
    attribute_dict = helpers.AttributeHelpers.create_attribute_dict_id_value(
        attribute_list,
    )
    mockup_dict = {
        146: "antracyt",
        105: "drugi",
        46: "gotowe do wysyłki",
        23: "PEI I",
        104: "pierwszy",
        78: "R9",
        40: "beż",
        24: "PEI II",
        79: "R10",
        47: "1-3 dni robocze",
        128: "beż / brąz",
        25: "PEI III",
        80: "R11",
        151: "1,5x75",
    }
    assert attribute_dict == mockup_dict


def test_create_attribute_dict_type_example_groups(dummy_request, dbsession):
    attribute_group_list = [
        data_containers.AttributeGroupContainer(
            4, 1, "format_group", "Grupa formatowa", 2, False
        ),
        data_containers.AttributeGroupContainer(4, 2, "", "Rektyfikacja", 7, False),
        data_containers.AttributeGroupContainer(
            3, 3, "internal_external", "Zastosowanie", 14, False
        ),
        data_containers.AttributeGroupContainer(1, 4, "", "Mrozoodporność", 5, False),
        data_containers.AttributeGroupContainer(
            3, 6, "room", "Pomieszczenia", 12, False
        ),
        data_containers.AttributeGroupContainer(4, 7, "", "Powierzchnia", 6, False),
        data_containers.AttributeGroupContainer(
            3, 8, "colour", "Kolor wiodący", 13, False
        ),
        data_containers.AttributeGroupContainer(
            1, 9, "availability", "Dostępność", 0, False
        ),
        data_containers.AttributeGroupContainer(4, 11, "quality", "Gatunek", 3, False),
    ]
    attribute_dict = helpers.AttributeHelpers.create_attribute_dict_type_example_groups(
        attribute_group_list,
    )
    mockup_dict = {
        1: ("Grupa formatowa", "format_group"),
        2: ("Rektyfikacja", ""),
        3: ("Zastosowanie", "internal_external"),
        4: ("Mrozoodporność", ""),
        6: ("Pomieszczenia", "room"),
        7: ("Powierzchnia", ""),
        8: ("Kolor wiodący", "colour"),
        9: ("Dostępność", "availability"),
        11: ("Gatunek", "quality"),
    }
    assert attribute_dict == mockup_dict
