from dataclasses import dataclass

from emporium import data_containers, helpers


@dataclass
class Banner:
    banner_id: int
    image: str
    caption: str 
    caption_secondary: str 
    status: bool


def test_banner_format_full_list(dummy_request):
    banner_list = [
        Banner(11, "palatina_halcon.jpg", "Halcon", "Palatina", True),
        Banner(10, "barcelona-keros-ceramica.jpg", "Barcelona", "Keros Ceramica", True),
        Banner(9, "crash-beige-polished", "Crash Beige Polished", "Geotiles", True),
        Banner(
            7, "nit-black-super-polished", "Nit Black Super Polished", "Geotiles", True
        ),
        Banner(8, "crash-blanco-polished", "Crash Blanco Polished", "Geotiles", True),
    ]
    banner_dict = helpers.BannerHelpers.banners_format(banner_list)
    mockup_dict = [
        {
            "banner_id": 11,
            "image": "palatina_halcon.jpg",
            "caption": "Halcon",
            "caption_secondary": "Palatina",
            "status": True,
        },
        {
            "banner_id": 10,
            "image": "barcelona-keros-ceramica.jpg",
            "caption": "Barcelona",
            "caption_secondary": "Keros Ceramica",
            "status": True,
        },
        {
            "banner_id": 9,
            "image": "crash-beige-polished",
            "caption": "Crash Beige Polished",
            "caption_secondary": "Geotiles",
            "status": True,
        },
        {
            "banner_id": 7,
            "image": "nit-black-super-polished",
            "caption": "Nit Black Super Polished",
            "caption_secondary": "Geotiles",
            "status": True,
        },
        {
            "banner_id": 8,
            "image": "crash-blanco-polished",
            "caption": "Crash Blanco Polished",
            "caption_secondary": "Geotiles",
            "status": True,
        },
    ]
    assert banner_dict == mockup_dict


def test_banner_format_empty_list(dummy_request):
    banner_list = []
    banner_dict = helpers.BannerHelpers.banners_format(banner_list)
    mockup_dict = []
    assert banner_dict == mockup_dict
