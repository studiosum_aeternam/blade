from datetime import datetime

from pyramid.testing import DummySecurityPolicy
from webob.multidict import MultiDict, NestedMultiDict

from emporium import helpers
from emporium import models
from emporium import services
from emporium import data_containers


def makeAttribute(attribute_group_id=None, sort_order=None, status=None, value=None):
    return models.Attribute(
        attribute_group_id=attribute_group_id,
        sort_order=sort_order,
        status=status,
        value=value,
    )


def makeAttributeDisplayType(
    name=None, description=False, sort_order=None, status=None
):
    return models.AttributeDisplayType(
        name=name, description=description, sort_order=sort_order, status=status
    )


def makeAttributeGroup(
    attribute_display_type_id=None,
    name=None,
    required=False,
    sort_order=None,
    status=None,
):
    return models.AttributeGroup(
        attribute_display_type_id=None,
        name=name,
        required=required,
        sort_order=sort_order,
        status=status,
    )


def makeCategory(
    date_added=None,
    date_modified=None,
    image=None,
    name=None,
    parent_id=None,
    placeholder=None,
    sort_order=None,
    status=None,
    top=None,
):
    return models.Category(
        date_added=date_added,
        date_modified=date_modified,
        image=image,
        name=name,
        parent_id=parent_id,
        placeholder=placeholder,
        sort_order=sort_order,
        status=status,
        top=top,
    )


def makeDiscount(status=None, subtract=None, value=None, default=False):
    return models.Discount(
        status=status, subtract=subtract, value=value, default=default
    )


def makeUser(first_name, last_name, login, password, role):
    user = models.User(
        first_name=first_name, last_name=last_name, login=login, role=role
    )
    user.set_password(password)
    return user


def setUser(config, user):
    config.set_security_policy(DummySecurityPolicy(identity=user))


def makeCategoryMetaTreeDescription(
    category_id=None,
    collection_id=None,
    description=None,
    description_long=None,
    language_id=None,
    manufacturer_id=None,
    meta_description=None,
    seo_slug=None,
    u_h1=None,
    u_title=None,
):
    return models.CategoryMetaTreeDescriptions(
        category_id=category_id,
        collection_id=collection_id,
        description=description,
        description_long=description_long,
        language_id=language_id,
        manufacturer_id=manufacturer_id,
        meta_description=meta_description,
        seo_slug=seo_slug,
        u_h1=u_h1,
        u_title=u_title,
    )


def makeManufacturer(
    date_added=None,
    date_available=None,
    date_modified=None,
    image=None,
    name=None,
    priority=None,
    sort_order=None,
    status=1,
    code="",
    manufacturer_type=0,
):
    return models.Manufacturer(
        date_added=date_added,
        date_available=date_available,
        date_modified=date_modified,
        image=image,
        name=name,
        priority=priority,
        sort_order=sort_order,
        status=status,
        code=code,
        manufacturer_type=manufacturer_type,
    )


def makeManufacturerToDiscount(discount_id, manufacturer_id):
    return models.ManufacturerToDiscount(
        discount_id=discount_id, manufacturer_id=manufacturer_id
    )


def makeProduct(
    # allowed_free_transport=None,
    # allowed_free_transport_minimum_amount=None,
    availability_id=None,
    catalog_price=None,
    date_added=None,
    date_available=None,
    date_modified=None,
    discount_id=None,
    ean=None,
    # forbidden_transport_methods=[],
    height=None,
    image=None,
    isbn=None,
    jan=None,
    length=None,
    length_class_id=None,
    location=None,
    manual_discount=None,
    manufacturer_id=None,
    minimum=None,
    model=None,
    mpn=None,
    name=None,
    pieces=None,
    points=None,
    price=None,
    quantity=None,
    seo_slug=None,
    sku=None,
    sort_order=None,
    square_meter=None,
    status=0,
    stock_status_id=None,
    subtract=None,
    one_batch=None,
    tax_id=None,
    unit=None,
    upc=None,
    viewed=0,
    virtual=0,
    weight=None,
    weight_class_id=None,
    width=None,
):
    return models.Product(
        # allowed_free_transport=allowed_free_transport,
        # allowed_free_transport_minimum_amount=allowed_free_transport_minimum_amount,
        availability_id=availability_id,
        catalog_price=catalog_price,
        date_added=date_added,
        date_available=date_available,
        date_modified=date_modified,
        discount_id=discount_id,
        ean=ean,
        # forbidden_transport_methods=forbidden_transport_methods,
        height=height,
        image=image,
        isbn=isbn,
        jan=jan,
        length=length,
        length_class_id=length_class_id,
        location=location,
        manual_discount=manual_discount,
        manufacturer_id=manufacturer_id,
        minimum=minimum,
        model=model,
        mpn=mpn,
        name=name,
        pieces=pieces,
        points=points,
        price=price,
        quantity=quantity,
        seo_slug=seo_slug,
        sku=sku,
        sort_order=sort_order,
        square_meter=square_meter,
        status=status,
        stock_status_id=stock_status_id,
        subtract=subtract,
        one_batch=one_batch,
        tax_id=tax_id,
        unit=unit,
        upc=upc,
        viewed=viewed,
        virtual=virtual,
        weight=weight,
        weight_class_id=weight_class_id,
        width=width,
    )


def makeProductToAttribute(attribute_id, product_id):
    return models.ProductToAttribute(attribute_id=attribute_id, product_id=product_id)


def makeProductToCategory(category_id, product_id):
    return models.ProductToCategory(category_id=category_id, product_id=product_id)


def makeProductDescription(
    description=None,
    description_technical=None,
    excerpt=None,
    language_id=None,
    meta_description=None,
    name=None,
    product_id=None,
    tag=None,
    u_h1=None,
    u_title=None,
):
    return models.ProductDescription(
        description=description,
        description_technical=description_technical,
        excerpt=excerpt,
        language_id=language_id,
        meta_description=meta_description,
        name=name,
        product_id=product_id,
        tag=tag,
        u_h1=u_h1,
        u_title=u_title,
    )


def makeSetting(key, value, setting_type):
    return models.Setting(key=key, value=value, setting_type=setting_type)


def makeTransport(
    special_method=False,
    excerpt=None,
    module_name=None,
    module_settings={},
    name=None,
):
    return models.Transport(
        special_method=special_method,
        excerpt=excerpt,
        module_name=module_name,
        module_settings=module_settings,
        name=name,
    )


def makeTax(status=None, tax_type=None, value=None):
    return models.Tax(status=status, tax_type=tax_type, value=value)


def makeTaxDescription(description=None, language_id=None, name=None, tax_id=None):
    return models.TaxDescription(
        description=description, language_id=language_id, name=name, tax_id=tax_id
    )
