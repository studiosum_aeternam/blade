from collections import namedtuple
from decimal import Decimal
from dataclasses import dataclass, field
from emporium import data_containers, helpers


@dataclass
class DataObject:
    payment_dict: dict
    transport_dict: dict


##################################
# Return cart products as a dict #
##################################


# can be a dict or a list (old data stored in the db) - testing fallback mechanics
def test_return_dict_from_cart_products_input_dict(dummy_request):
    products = {
        31555: {"product_quantity": Decimal("2.56"), "product_single_set": "true"}
    }
    cart_products = helpers.CartHelpers.return_dict_from_cart_products(products)
    mockup_cart_products = {
        31555: {"product_quantity": Decimal("2.56"), "product_single_set": "true"}
    }
    assert cart_products == mockup_cart_products


def test_return_dict_form_cart_products_input_empty_dict(dummy_request):
    products = {}
    cart_products = helpers.CartHelpers.return_dict_from_cart_products(products)
    mockup_cart_products = False
    assert cart_products == mockup_cart_products


def test_return_dict_form_cart_products_input_list(dummy_request):
    products = [
        {
            "product_id": 31555,
            "product_quantity": Decimal("2.56"),
            "product_single_set": "true",
        }
    ]
    cart_products = helpers.CartHelpers.return_dict_from_cart_products(products)
    mockup_cart_products = {
        31555: {"product_quantity": Decimal("2.56"), "product_single_set": "true"}
    }
    assert cart_products == mockup_cart_products


def test_return_dict_form_cart_products_input_empty_list(dummy_request):
    products = []
    cart_products = helpers.CartHelpers.return_dict_from_cart_products(products)
    mockup_cart_products = False
    assert cart_products == mockup_cart_products


def test_return_dict_form_cart_products_input_not_list_or_dict(dummy_request):
    products = None
    cart_products = helpers.CartHelpers.return_dict_from_cart_products(products)
    mockup_cart_products = False
    assert cart_products == mockup_cart_products


###########################################
# Find disabled payment/transport methods #
###########################################


#####################################
# Remove disabled transport mehtods #
#####################################


#########################################
# Remove incompatible transport mehtods #
#########################################


#########################################
# Remove incompatible payment mehtods #
#########################################


########################################
# Update payment and transport methods #
########################################


###############################
# Get current cart parameters #
###############################


def test_get_current_cart_products_single(dummy_request):
    cart_products = [
        (
            31366,
            {
                "product_quantity": Decimal("3.00"),
                "product_single_set": "true",
                "product_packages": 3,
            },
        )
    ]
    product, quantity = helpers.CartHelpers.get_current_cart_parameters(cart_products)
    expected_product = {31366: Decimal("3.00")}
    expected_quantity = 3
    assert expected_product == product
    assert expected_quantity == quantity


def test_get_current_cart_two_products(dummy_request):
    cart_products = [
        (
            31366,
            {
                "product_quantity": Decimal("3.00"),
                "product_single_set": "true",
                "product_packages": 3,
            },
        ),
        (
            31558,
            {
                "product_quantity": Decimal("1.25"),
                "product_single_set": "true",
                "product_packages": 1,
            },
        ),
    ]
    product, quantity = helpers.CartHelpers.get_current_cart_parameters(cart_products)
    expected_product = {31366: Decimal("3.00"), 31558: Decimal("1.25")}
    expected_quantity = 4
    assert expected_product == product
    assert expected_quantity == quantity


##############################
# Get current product values #
##############################


def test_cart_contents_get_current_single_product(dummy_request):
    cart_products = {
        30207: {
            "product_quantity": Decimal("4.32"),
            "product_single_set": "true",
            "product_packages": 3,
        }
    }
    product_list = {
        30207: {
            "product_id": 30207,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("141.70"),
            "catalog_price": Decimal("80.0000"),
            "collection": "Pisa",
            "discount_id": 1,
            "ean": "",
            "image": "pamesa/pisa-gold-mat-120x60-satyna-piraues",
            "manual_discount": False,
            "manufacturer": "Pamesa",
            "manufactured": 1,
            "manufacturer_id": 162,
            "minimum": Decimal("1.00"),
            "model": "PISA GOLD MAT 120x60 SATYNA",
            "name": "PISA GOLD MAT 120x60 Satyna PIRAUES",
            "one_batch": True,
            "pieces": 2,
            "price": Decimal("98.40"),
            "product_type": 0,
            "price_net": Decimal("80.00"),
            "slug": "pisa-gold-mat-120x60-satyna-piraues",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.44"),
            "unit_tax": Decimal("18.40"),
            "virtual": 0,
            "weight": Decimal("19.00000000"),
        }
    }
    cart_contents = helpers.CartHelpers.cart_contents_get_current_product_values(
        cart_products, product_list
    )
    expected_cart_contents = {
        30207: {
            "product_quantity": Decimal("4.32"),
            "product_single_set": "true",
            "product_packages": 3,
            "product_id": 30207,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("141.70"),
            "catalog_price": Decimal("80.0000"),
            "collection": "Pisa",
            "discount_id": 1,
            "ean": "",
            "image": "pamesa/pisa-gold-mat-120x60-satyna-piraues",
            "manual_discount": False,
            "manufacturer": "Pamesa",
            "manufactured": 1,
            "manufacturer_id": 162,
            "minimum": Decimal("1.00"),
            "model": "PISA GOLD MAT 120x60 SATYNA",
            "name": "PISA GOLD MAT 120x60 Satyna PIRAUES",
            "one_batch": True,
            "pieces": 2,
            "price": Decimal("98.40"),
            "product_type": 0,
            "price_net": Decimal("80.00"),
            "slug": "pisa-gold-mat-120x60-satyna-piraues",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.44"),
            "unit_tax": Decimal("18.40"),
            "virtual": 0,
            "weight": Decimal("19.00000000"),
        }
    }

    assert cart_contents == expected_cart_contents


def test_cart_contents_get_current_product_values_two_products(dummy_request):
    cart_products = {
        30207: {
            "product_quantity": Decimal("4.32"),
            "product_single_set": "true",
            "product_packages": 3,
        },
        31555: {
            "product_quantity": Decimal("1.28"),
            "product_single_set": "true",
            "product_packages": 1,
        },
        31366: {
            "product_quantity": Decimal("1.00"),
            "product_single_set": "true",
            "product_packages": 1,
        },
    }
    product_list = {
        30207: {
            "product_id": 30207,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("141.70"),
            "catalog_price": Decimal("80.0000"),
            "collection": "Pisa",
            "discount_id": 1,
            "ean": "",
            "image": "pamesa/pisa-gold-mat-120x60-satyna-piraues",
            "manual_discount": False,
            "manufacturer": "Pamesa",
            "manufactured": 1,
            "manufacturer_id": 162,
            "minimum": Decimal("1.00"),
            "model": "PISA GOLD MAT 120x60 SATYNA",
            "name": "PISA GOLD MAT 120x60 Satyna PIRAUES",
            "one_batch": True,
            "pieces": 2,
            "price": Decimal("98.40"),
            "product_type": 0,
            "price_net": Decimal("80.00"),
            "slug": "pisa-gold-mat-120x60-satyna-piraues",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.44"),
            "unit_tax": Decimal("18.40"),
            "virtual": 0,
            "weight": Decimal("19.00000000"),
        },
        31366: {
            "product_id": 31366,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("136.53"),
            "catalog_price": Decimal("111.0000"),
            "collection": "Toscana",
            "discount_id": 1,
            "ean": "",
            "image": "cicogres/toscana-perla-mat-100x100",
            "manual_discount": False,
            "manufacturer": "Cicogres",
            "manufactured": 1,
            "manufacturer_id": 96,
            "minimum": Decimal("1.00"),
            "model": "TOSCANA PERLA MAT 100x100",
            "name": "TOSCANA PERLA MAT 100x100",
            "one_batch": False,
            "pieces": 1,
            "price": Decimal("136.53"),
            "product_type": 0,
            "price_net": Decimal("111.00"),
            "slug": "toscana-perla-mat-100x100",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.00"),
            "unit_tax": Decimal("25.53"),
            "virtual": 0,
            "weight": Decimal("20.00000000"),
        },
        31555: {
            "product_id": 31555,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("153.50"),
            "catalog_price": Decimal("97.5000"),
            "collection": "Terrazzo",
            "discount_id": 1,
            "ean": "",
            "image": "undefasa/terrazzo-gris-80x80",
            "manual_discount": False,
            "manufacturer": "Undefasa",
            "manufactured": 1,
            "manufacturer_id": 186,
            "minimum": Decimal("1.00"),
            "model": "TERRAZZO GRIS 80x80",
            "name": "TERRAZZO GRIS 80x80",
            "one_batch": True,
            "pieces": 2,
            "price": Decimal("119.92"),
            "product_type": 0,
            "price_net": Decimal("97.50"),
            "slug": "terrazzo-gris-80x80",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.28"),
            "unit_tax": Decimal("22.42"),
            "virtual": 0,
            "weight": Decimal("19.00000000"),
        },
    }

    cart_contents = helpers.CartHelpers.cart_contents_get_current_product_values(
        cart_products, product_list
    )
    expected_cart_contents = {
        30207: {
            "product_quantity": Decimal("4.32"),
            "product_single_set": "true",
            "product_packages": 3,
            "product_id": 30207,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("141.70"),
            "catalog_price": Decimal("80.0000"),
            "collection": "Pisa",
            "discount_id": 1,
            "ean": "",
            "image": "pamesa/pisa-gold-mat-120x60-satyna-piraues",
            "manual_discount": False,
            "manufacturer": "Pamesa",
            "manufactured": 1,
            "manufacturer_id": 162,
            "minimum": Decimal("1.00"),
            "model": "PISA GOLD MAT 120x60 SATYNA",
            "name": "PISA GOLD MAT 120x60 Satyna PIRAUES",
            "one_batch": True,
            "pieces": 2,
            "price": Decimal("98.40"),
            "product_type": 0,
            "price_net": Decimal("80.00"),
            "slug": "pisa-gold-mat-120x60-satyna-piraues",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.44"),
            "unit_tax": Decimal("18.40"),
            "virtual": 0,
            "weight": Decimal("19.00000000"),
        },
        31366: {
            "product_quantity": Decimal("1.00"),
            "product_single_set": "true",
            "product_packages": 1,
            "product_id": 31366,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("136.53"),
            "catalog_price": Decimal("111.0000"),
            "collection": "Toscana",
            "discount_id": 1,
            "ean": "",
            "image": "cicogres/toscana-perla-mat-100x100",
            "manual_discount": False,
            "manufacturer": "Cicogres",
            "manufactured": 1,
            "manufacturer_id": 96,
            "minimum": Decimal("1.00"),
            "model": "TOSCANA PERLA MAT 100x100",
            "name": "TOSCANA PERLA MAT 100x100",
            "one_batch": False,
            "pieces": 1,
            "price": Decimal("136.53"),
            "product_type": 0,
            "price_net": Decimal("111.00"),
            "slug": "toscana-perla-mat-100x100",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.00"),
            "unit_tax": Decimal("25.53"),
            "virtual": 0,
            "weight": Decimal("20.00000000"),
        },
        31555: {
            "product_quantity": Decimal("1.28"),
            "product_single_set": "true",
            "product_packages": 1,
            "product_id": 31555,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("153.50"),
            "catalog_price": Decimal("97.5000"),
            "collection": "Terrazzo",
            "discount_id": 1,
            "ean": "",
            "image": "undefasa/terrazzo-gris-80x80",
            "manual_discount": False,
            "manufacturer": "Undefasa",
            "manufactured": 1,
            "manufacturer_id": 186,
            "minimum": Decimal("1.00"),
            "model": "TERRAZZO GRIS 80x80",
            "name": "TERRAZZO GRIS 80x80",
            "one_batch": True,
            "pieces": 2,
            "price": Decimal("119.92"),
            "product_type": 0,
            "price_net": Decimal("97.50"),
            "slug": "terrazzo-gris-80x80",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.28"),
            "unit_tax": Decimal("22.42"),
            "virtual": 0,
            "weight": Decimal("19.00000000"),
        },
    }

    assert cart_contents == expected_cart_contents


def test_cart_contents_product_removed_due_to_availability_change(dummy_request):
    cart_products = {
        31783: {"product_quantity": Decimal("2.88"), "product_single_set": "true"},
        31555: {
            "product_quantity": Decimal("17.92"),
            "product_single_set": "true",
            "product_packages": 14,
        },
        29747: {
            "product_quantity": Decimal("1.28"),
            "product_single_set": "true",
            "product_packages": 1,
        },
    }
    product_list = {
        29747: {
            "product_id": 29747,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("127.53"),
            "catalog_price": Decimal("81.0000"),
            "collection": "Carrara Poler",
            "discount_id": 1,
            "ean": "",
            "image": "cuarto/carrara-blanco-gres-polerowany-rekt-80x80",
            "manual_discount": False,
            "manufacturer": "Cuarto",
            "manufactured": 1,
            "manufacturer_id": 154,
            "minimum": Decimal("1.00"),
            "model": "CARRARA BLANCO GRES POLEROWANY  REKT. 80x80",
            "name": "1 CARRARA BLANCO GRES POLEROWANY REKT. 80x80",
            "one_batch": True,
            "pieces": 2,
            "price": Decimal("99.63"),
            "product_type": 0,
            "price_net": Decimal("81.00"),
            "slug": "1-carrara-blanco-gres-polerowany-rekt-80x80",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.28"),
            "unit_tax": Decimal("18.63"),
            "virtual": 0,
            "weight": Decimal("20.00000000"),
        }
    }
    cart_contents = helpers.CartHelpers.cart_contents_get_current_product_values(
        cart_products, product_list
    )
    expected_cart_contents = {
        29747: {
            "product_quantity": Decimal("1.28"),
            "product_single_set": "true",
            "product_packages": 1,
            "product_id": 29747,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("127.53"),
            "catalog_price": Decimal("81.0000"),
            "collection": "Carrara Poler",
            "discount_id": 1,
            "ean": "",
            "image": "cuarto/carrara-blanco-gres-polerowany-rekt-80x80",
            "manual_discount": False,
            "manufacturer": "Cuarto",
            "manufactured": 1,
            "manufacturer_id": 154,
            "minimum": Decimal("1.00"),
            "model": "CARRARA BLANCO GRES POLEROWANY  REKT. 80x80",
            "name": "1 CARRARA BLANCO GRES POLEROWANY REKT. 80x80",
            "one_batch": True,
            "pieces": 2,
            "price": Decimal("99.63"),
            "product_type": 0,
            "price_net": Decimal("81.00"),
            "slug": "1-carrara-blanco-gres-polerowany-rekt-80x80",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.28"),
            "unit_tax": Decimal("18.63"),
            "virtual": 0,
            "weight": Decimal("20.00000000"),
        }
    }

    assert cart_contents == expected_cart_contents


def test_cart_contents_product_removed_due_to_status_change(dummy_request):
    cart_products = {
        31783: {"product_quantity": Decimal("2.88"), "product_single_set": "true"},
        31555: {
            "product_quantity": Decimal("6.40"),
            "product_single_set": "true",
            "product_packages": 5,
        },
        29747: {
            "product_quantity": Decimal("3.84"),
            "product_single_set": "true",
            "product_packages": 3,
        },
    }
    product_list = {
        29747: {
            "product_id": 29747,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("127.53"),
            "catalog_price": Decimal("81.0000"),
            "collection": "Carrara Poler",
            "discount_id": 1,
            "ean": "",
            "image": "cuarto/carrara-blanco-gres-polerowany-rekt-80x80",
            "manual_discount": False,
            "manufacturer": "Cuarto",
            "manufactured": 1,
            "manufacturer_id": 154,
            "minimum": Decimal("1.00"),
            "model": "CARRARA BLANCO GRES POLEROWANY  REKT. 80x80",
            "name": "1 CARRARA BLANCO GRES POLEROWANY REKT. 80x80",
            "one_batch": True,
            "pieces": 2,
            "price": Decimal("99.63"),
            "product_type": 0,
            "price_net": Decimal("81.00"),
            "slug": "1-carrara-blanco-gres-polerowany-rekt-80x80",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.28"),
            "unit_tax": Decimal("18.63"),
            "virtual": 0,
            "weight": Decimal("20.00000000"),
        }
    }
    cart_contents = helpers.CartHelpers.cart_contents_get_current_product_values(
        cart_products, product_list
    )
    expected_cart_contents = {
        29747: {
            "product_quantity": Decimal("3.84"),
            "product_single_set": "true",
            "product_packages": 3,
            "product_id": 29747,
            "availability": "gotowe do wysyłki",
            "availability_id": 46,
            "band": "",
            "calculated_price": Decimal("127.53"),
            "catalog_price": Decimal("81.0000"),
            "collection": "Carrara Poler",
            "discount_id": 1,
            "ean": "",
            "image": "cuarto/carrara-blanco-gres-polerowany-rekt-80x80",
            "manual_discount": False,
            "manufacturer": "Cuarto",
            "manufactured": 1,
            "manufacturer_id": 154,
            "minimum": Decimal("1.00"),
            "model": "CARRARA BLANCO GRES POLEROWANY  REKT. 80x80",
            "name": "1 CARRARA BLANCO GRES POLEROWANY REKT. 80x80",
            "one_batch": True,
            "pieces": 2,
            "price": Decimal("99.63"),
            "product_type": 0,
            "price_net": Decimal("81.00"),
            "slug": "1-carrara-blanco-gres-polerowany-rekt-80x80",
            "storage_current": {},
            "not_promoted_storage_summary": 0,
            "square_meter": 1,
            "status": 1,
            "tax_id": 1,
            "unit": Decimal("1.28"),
            "unit_tax": Decimal("18.63"),
            "virtual": 0,
            "weight": Decimal("20.00000000"),
        }
    }

    assert cart_contents == expected_cart_contents
