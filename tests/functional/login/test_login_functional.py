import pytest
import transaction

from emporium import models
from tests import functional

basic_login = {"login": "basic", "password": "basic", "role": "customer"}
editor_login = {"login": "editor", "password": "editor", "role": "emperor"}


@pytest.fixture(scope="session", autouse=True)
def dummy_data(app):
    """
    Add some dummy data to the database.

    Note that this is a session fixture that commits data to the database.
    Think about it similarly to running the ``initialize_db`` script at the
    start of the test suite.

    This data should not conflict with any other data added throughout the
    test suite or there will be issues - so be careful with this pattern!

    """
    tm = transaction.TransactionManager(explicit=True)
    with tm:
        dbsession = models.get_tm_session(app.registry["dbsession_factory"], tm)
        functional.clearDatabase(dbsession)

        setting1 = models.Setting(key="cache", value=True, setting_type=0)
        setting2 = models.Setting(key="shop_status", value=True, setting_type=0)
        setting3 = models.Setting(
            key="name", value="Emporatorium test name", setting_type=0
        )
        editor = models.User(
            first_name="Jack", last_name="Black", role="emperor", login="editor"
        )
        editor.set_password("editor")
        basic = models.User(
            first_name="Jack", last_name="Black", role="customer", login="basic"
        )
        basic.set_password("basic")
        dbsession.add_all([basic, editor, setting1, setting2, setting3])
        dbsession.flush()
        # page1 = models.Page(
        #     title="FrontPage", content="This is the front page", status=1
        # )
        # page1.creator_id = editor.id
        # page2 = models.Page(title="BackPage", content="This is the back page", status=1)
        # page2.creator_id = basic.id
        # dbsession.add_all([page1, page2])
        # dbsession.flush()
        # global page_id_1
        # page_id_1 = page1.page_id
        # global page_id_2
        # page_id_2 = page2.page_id


# def test_root(testapp):
#     res = testapp.get("/", status=200)
#     assert res.location == "http://example.com"


def test_unsuccessful_log_in(testapp):
    params = dict(
        **basic_login,
        csrf_token=testapp.get_csrf_token(),
    )
    res = testapp.post("/emporatorium/sign/in", params, status=302).follow()
    assert b"login" in res.body


def test_successful_log_in(testapp):
    params = dict(
        **editor_login,
        csrf_token=testapp.get_csrf_token(),
    )
    res = testapp.post("/emporatorium/sign/in", params, status=302)
    assert "http://example.com/emporatorium" in res.location


def test_successful_log_with_next(testapp):
    params = dict(
        **editor_login,
        next="/emporatorium/product_list",
        csrf_token=testapp.get_csrf_token(),
    )
    res = testapp.post("/emporatorium/sign/in", params, status=302)
    assert res.location == "http://example.com/emporatorium/product_list"


def test_failed_log_in(testapp):
    params = dict(
        login="editor",
        password="incorrect",
        csrf_token=testapp.get_csrf_token(),
    )
    res = testapp.post("/emporatorium", params, status=200)
    assert b"login" in res.body


def test_logout_link_present_when_logged_in(testapp):
    testapp.login(editor_login)
    res = testapp.get("/emporatorium/", status=200)
    assert b"logout" in res.body


def test_logout_link_not_present_after_logged_out(testapp):
    testapp.login(editor_login)
    testapp.get("/emporatorium/", status=200)
    params = dict(csrf_token=testapp.get_csrf_token())
    res = testapp.post("/emporatorium/sign/out", params, status=302).follow()
    assert b"Logout" not in res.body
    assert b"Login" in res.body


def test_admin_bar_is_hidden_form_the_visitor(testapp):
    res = testapp.get("/", status=200)
    assert b"emporatorium-top-bar" not in res.body


def test_admin_bar_is_visible_for_logged_admin(testapp):
    testapp.login(editor_login)
    res = testapp.get("/", status=200)
    assert b"emporatorium-top-bar" in res.body
