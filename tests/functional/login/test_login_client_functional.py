from datetime import datetime
import pytest
import transaction

from emporium import helpers, models
from tests import functional


@pytest.fixture(scope="session", autouse=True)
def dummy_data(app):
    """
    Add some dummy data to the database.

    Note that this is a session fixture that commits data to the database.
    Think about it similarly to running the ``initialize_db`` script at the
    start of the test suite.

    This data should not conflict with any other data added throughout the
    test suite or there will be issues - so be careful with this pattern!

    """
    tm = transaction.TransactionManager(explicit=True)
    with tm:
        dbsession = models.get_tm_session(app.registry["dbsession_factory"], tm)
        functional.clearDatabase(dbsession)

        setting_functional_product_1 = models.Setting(
            key="cache", value=False, setting_type=0
        )
        setting_functional_product_2 = models.Setting(
            key="shop_status", value=True, setting_type=0
        )
        setting_functional_product_3 = models.Setting(
            key="name", value="Emporatorium test name", setting_type=0
        )
        editor_functional_product = models.User(
            first_name="Jack", last_name="Black", role="emperor", login="editor"
        )
        editor_functional_product.set_password("editor")
        customer_functional_cart = models.Customer(
            first_name="Jack",
            last_name="Black",
            mail="jack.black@gmail.com",
            telephone="732248710",
            customer_type=0,
            psswd_hsh="basic",
        )
        dbsession.add_all(
            [
                customer_functional_cart,
                editor_functional_product,
                setting_functional_product_1,
                setting_functional_product_2,
            ]
        )
        dbsession.flush()
        setting_template_1 = models.SettingTemplate(
            description="Setting template description",
            name="Tile template",
            order=1,
            status=1,
            template_json={
                "manufactured": 1,
                "virtual": 0,
                "subtract_quantity": 0,
                "storage_type": "",
                "attribute_type_list": [],
                "unit_label": 2,
            },
            template_type=1,
        )
        dbsession.add_all((setting_template_1,))
        dbsession.flush()


client_wrong_login = {"mail": "jack.black@gmail.com", "password": "wuong password"}
client_login = {"mail": "jack.black@gmail.com", "password": "basic"}


def test_unsuccessful_log_in(testapp):
    params = dict(
        **client_wrong_login,
        csrf_token=testapp.get_csrf_token(),
    )
    res = testapp.post("/sign/in", params, status=302).follow()
    assert b"home-page" in res.body


def test_successful_log_in(testapp):
    params = dict(
        **client_login,
        csrf_token=testapp.get_csrf_token(),
    )
    res = testapp.post("/sign/in", params, status=302).follow()
    # assert res.location == "http://example.com/"
    assert b"home-page" in res.body
    assert b"sign/out" in res.body


def test_admin_bar_is_hidden_form_the_logged_customer(testapp):
    testapp.login_customer(client_login)
    res = testapp.get("/", status=200)
    assert b"sign/out" in res.body
    assert b"emporatorium-top-bar" not in res.body
