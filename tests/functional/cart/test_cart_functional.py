from datetime import datetime
from decimal import Decimal
import json
import pytest
import transaction

from emporium import helpers, models

from tests import functional


@pytest.fixture(scope="session", autouse=True)
def dummy_data(app):
    """
    Add some dummy data to the database.

    Note that this is a session fixture that commits data to the database.
    Think about it similarly to running the ``initialize_db`` script at the
    start of the test suite.

    This data should not conflict with any other data added throughout the
    test suite or there will be issues - so be careful with this pattern!

    """
    tm = transaction.TransactionManager(explicit=True)
    with tm:
        dbsession = models.get_tm_session(app.registry["dbsession_factory"], tm)
        functional.clearDatabase(dbsession)

        setting_functional_product_1 = models.Setting(
            key="cache", value=False, setting_type=0
        )
        setting_functional_product_2 = models.Setting(
            key="shop_status", value=True, setting_type=0
        )
        setting_functional_product_3 = models.Setting(
            key="name", value="Emporatorium test name", setting_type=0
        )
        # fmt: off
        setting_functional_product_4 = models.Setting(
            key="image_settings",
            complex_values={"image_sizes": {"base": [1200, 1200], "thumbnails": [500, 500]},"image_extensions": [["JPEG", "jpg", "RGB"],["WEBP", "webp", "RGBA"]]},
            setting_type=0,
        )
        # fmt: on
        editor_functional_product = models.User(
            first_name="Jack", last_name="Black", role="emperor", login="editor"
        )
        editor_functional_product.set_password("editor")
        global customer_password_1
        customer_password_1 = "basic"
        global customer_password_2
        customer_password_2 = "basic"
        global customer_password_3
        customer_password_3 = "not_so_basic"
        customer_functional_cart_1 = models.Customer(
            customer_type=0,
            first_name="Jack",
            last_name="Black",
            mail="jack.black@gmail.com",
            # password="temporary",
            psswd_hsh=customer_password_1,
            telephone="732248710",
        )
        customer_functional_cart_2 = models.Customer(
            customer_type=0,
            first_name="Joanna",
            last_name="Darq",
            mail="joana.darq@gmail.com",
            # password="temporary",
            psswd_hsh=customer_password_2,
            telephone="734448710",
        )
        customer_functional_cart_3 = models.Customer(
            customer_type=0,
            first_name="Dasha",
            last_name="Astafieva",
            mail="dashaaa@gmail.com",
            # password="temporary",
            psswd_hsh=customer_password_3,
            telephone="73111232",
        )
        attribute_display_type_functional_product_1 = models.AttributeDisplayType(
            name="select", description="Multiple select box", sort_order="1", status="1"
        )
        transport_1 = models.Transport(
            special_method=False,
            description="",
            excerpt="",
            module_name="courier",
            module_settings={
                "manipulation_fee": "0",
                "unit_fee": "139",
                "unit_weight": "1000",
            },
            name="Kurier",
            status=True,
        )
        transport_2 = models.Transport(
            special_method=True,
            description="",
            excerpt="",
            module_name="courier_bulk",
            module_settings={
                "manipulation_fee": "180",
                "removed_transports": [],
                "unit_fee": "139",
                "unit_weight": "1000",
            },
            name="Kurier gabaryt",
            status=True,
        )
        transport_3 = models.Transport(
            special_method=True,
            description="",
            excerpt="",
            module_name="free_delivery",
            module_settings={"removed_transports": []},
            name="Darmowy transport",
            status=True,
        )
        dbsession.add_all(
            [
                customer_functional_cart_1,
                customer_functional_cart_2,
                customer_functional_cart_3,
                editor_functional_product,
                setting_functional_product_1,
                setting_functional_product_2,
                setting_functional_product_3,
                setting_functional_product_4,
                attribute_display_type_functional_product_1,
                attribute_display_type_functional_product_1,
                transport_1,
                transport_2,
                transport_3,
            ]
        )
        dbsession.flush()
        transport_2.module_settings["removed_transports"] = [transport_1.transport_id]
        transport_3.module_settings["removed_transports"] = [
            transport_1.transport_id,
            transport_2.transport_id,
        ]
        global customer_id_1
        customer_id_1 = customer_functional_cart_1.id
        global customer_id_2
        customer_id_2 = customer_functional_cart_2.id
        global customer_id_3
        customer_id_3 = customer_functional_cart_3.id
        attribute_group_functional_product_1 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Antislip",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_2 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Availability",
            required=True,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_3 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Frost",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_4 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Measurements",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_5 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Pei",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_6 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Quality",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_7 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Rekt",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_8 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Surface",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_9 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Tiles",
            required=False,
            sort_order=3,
            status=1,
        )
        tax_functional_product_1 = models.Tax(status=1, value=23, tax_type=1)
        dbsession.add_all(
            (
                attribute_group_functional_product_1,
                attribute_group_functional_product_2,
                attribute_group_functional_product_3,
                attribute_group_functional_product_4,
                attribute_group_functional_product_5,
                attribute_group_functional_product_6,
                attribute_group_functional_product_7,
                attribute_group_functional_product_8,
                attribute_group_functional_product_9,
                tax_functional_product_1,
            )
        )
        dbsession.flush()
        tax_description_functional_product_1 = models.TaxDescription(
            tax_id=tax_functional_product_1.tax_id,
            language_id=2,
            name="VAT 23% (FUNCTIONAL)",
            description="PODATEK VAT OPIS",
        )
        attribute_functional_product_1 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_product_2 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Magazyn",
        )
        attribute_functional_product_3 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_3.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_product_4 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="10x20",
        )
        attribute_functional_product_5 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_5.attribute_group_id,
            sort_order=1,
            status=1,
            value="PEI III",
        )
        attribute_functional_product_6 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_6.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pierwsza",
        )
        attribute_functional_product_7 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_product_8 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Poler",
        )
        attribute_functional_product_9 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Nie",
        )
        attribute_functional_product_10 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="2-3 dni",
        )
        attribute_functional_product_11 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Niedostępne",
        )
        attribute_functional_product_12 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Zapytaj o dostępność",
        )
        attribute_functional_product_13 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="20x20",
        )
        attribute_functional_product_14 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="20x30",
        )
        attribute_functional_product_15 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="30x30",
        )
        attribute_functional_product_16 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="40x40",
        )
        attribute_functional_product_17 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="40x60",
        )
        attribute_functional_product_18 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_5.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pei I",
        )
        attribute_functional_product_19 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_5.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pei II",
        )
        attribute_functional_product_20 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Nie",
        )
        attribute_functional_product_21 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Mat",
        )
        attribute_functional_product_22 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Połysk",
        )
        attribute_functional_product_23 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_product_24 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_9.attribute_group_id,
            sort_order=1,
            status=1,
            value="Nie",
        )
        discount_functional_product_1 = models.Discount(
            status=1, subtract=1, value=0.00, default=True
        )
        transport_functional_product_1 = models.Transport(
            special_method=True,
            description="",
            excerpt="",
            module_name="courier",
            module_settings={
                "manipulation_fee": "0",
                "unit_fee": "139",
                "unit_weight": "1000",
            },
            name="Kurier",
            status=True,
        )
        manufacturer_functional_product_1 = models.Manufacturer(
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fist-manufacturer.jpg",
            sort_order=1,
            name="Other Fist Manufacturer",
            priority=0,
            status=1,
            code="",
            manufacturer_type=0,
        )
        manufacturer_functional_product_2 = models.Manufacturer(
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/second-manufacturer.jpg",
            sort_order=1,
            name="Opoczno",
            priority=0,
            status=1,
            code="",
            manufacturer_type=0,
        )
        manufacturer_functional_product_3 = models.Manufacturer(
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/third-manufacturer.jpg",
            sort_order=1,
            name="Paradyż",
            priority=0,
            status=1,
            code="",
            manufacturer_type=0,
        )
        manufacturer_functional_product_4 = models.Manufacturer(
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fourth-manufacturer.jpg",
            sort_order=1,
            name="Cicogres",
            priority=0,
            status=1,
            code="",
            manufacturer_type=0,
        )
        category_functional_product_1 = models.Category(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/first_category.jpg",
            name="Płytki",
            parent_id=None,
            placeholder=False,
            sort_order=1,
            status=1,
        )
        category_functional_product_2 = models.Category(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/second_category.jpg",
            name="Bestsellery",
            parent_id=None,
            placeholder=False,
            sort_order=1,
            status=1,
        )
        setting_template_1 = models.SettingTemplate(
            description="Setting template description",
            name="Tile template",
            order=1,
            status=1,
            template_json={
                "manufactured": 1,
                "virtual": 0,
                "subtract_quantity": 0,
                "storage_type": "",
                "attribute_type_list": [
                    attribute_group_functional_product_1.attribute_group_id,
                    attribute_group_functional_product_2.attribute_group_id,
                    attribute_group_functional_product_3.attribute_group_id,
                    attribute_group_functional_product_4.attribute_group_id,
                    attribute_group_functional_product_5.attribute_group_id,
                    attribute_group_functional_product_6.attribute_group_id,
                    attribute_group_functional_product_7.attribute_group_id,
                    attribute_group_functional_product_8.attribute_group_id,
                    attribute_group_functional_product_9.attribute_group_id,
                ],
                "unit_label": 2,
            },
            template_type=1,
        )
        dbsession.add_all(
            (
                attribute_functional_product_1,
                attribute_functional_product_10,
                attribute_functional_product_11,
                attribute_functional_product_12,
                attribute_functional_product_13,
                attribute_functional_product_14,
                attribute_functional_product_15,
                attribute_functional_product_16,
                attribute_functional_product_17,
                attribute_functional_product_18,
                attribute_functional_product_19,
                attribute_functional_product_2,
                attribute_functional_product_20,
                attribute_functional_product_21,
                attribute_functional_product_22,
                attribute_functional_product_23,
                attribute_functional_product_24,
                attribute_functional_product_3,
                attribute_functional_product_4,
                attribute_functional_product_5,
                attribute_functional_product_6,
                attribute_functional_product_7,
                attribute_functional_product_8,
                attribute_functional_product_9,
                category_functional_product_1,
                category_functional_product_2,
                discount_functional_product_1,
                manufacturer_functional_product_1,
                manufacturer_functional_product_2,
                manufacturer_functional_product_3,
                manufacturer_functional_product_4,
                tax_description_functional_product_1,
                transport_functional_product_1,
                setting_template_1,
            )
        )
        dbsession.flush()
        category_functional_product_3 = models.Category(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/third-manufacturer.jpg",
            name="Drewnopodobne",
            parent_id=category_functional_product_1.category_id,
            placeholder=False,
            sort_order=1,
            status=1,
        )
        product_functional_product_1 = models.Product(
            # allowed_free_transport==0,
            ##allowed_free_transport=_minimum_amount==2,
            availability_id=attribute_functional_product_2.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_product_1.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="folder/testing-product-model.jpg",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_product_1.manufacturer_id,
            minimum=1.01,
            model="Functional testing cart - product model",
            mpn=2,
            name="Functional testing cart - first product name - tpf1",
            one_batch=True,
            pieces=1,
            points=0,
            price=20.55,
            quantity=1,
            seo_slug="testing-functional-cart-first-product-name",
            setting_template_id=setting_template_1.setting_template_id,
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_product_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=28.00,
            weight_class_id=1,
            width=1,
        )
        product_functional_product_2 = models.Product(
            # allowed_free_transport==0,
            ##allowed_free_transport=_minimum_amount==2,
            availability_id=attribute_functional_product_10.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_product_1.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="folder/testing-cart-product-model.jpg",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_product_2.manufacturer_id,
            minimum=1.01,
            model="Functional testing cart - second product model",
            mpn=2,
            name="Functional testing cart - second product name - tpf2",
            one_batch=True,
            pieces=1,
            points=0,
            price=30.55,
            quantity=1,
            seo_slug="testing-functional-cart-second-product-name",
            setting_template_id=setting_template_1.setting_template_id,
            sku=5,
            sort_order=0,
            square_meter=0,  # Needed to display input pack only
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_product_1.tax_id,
            unit=1,  # Needed to display input pack only
            upc=6,
            viewed=0,
            virtual=0,
            weight=1.00,
            weight_class_id=1,
            width=1,
        )
        product_functional_product_3 = models.Product(
            # allowed_free_transport==0,
            ##allowed_free_transport=_minimum_amount==2,
            availability_id=attribute_functional_product_11.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_product_1.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="folder/testing-third-product-model.jpg",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_product_3.manufacturer_id,
            minimum=1,
            model="Functional testing cart - third product model",
            mpn=2,
            name="Functional testing cart - third product name - tpf3",
            one_batch=True,
            pieces=1,
            points=0,
            price=24.37,
            quantity=1,
            seo_slug="testing-functional-cart-third-product-name",
            setting_template_id=setting_template_1.setting_template_id,
            sku=5,
            sort_order=0,
            square_meter=0,  # Needed to display input pack only
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_product_1.tax_id,
            unit=1,  # Needed to display input pack only
            upc=6,
            viewed=0,
            virtual=0,
            weight=1.00,
            weight_class_id=1,
            width=1,
        )
        product_functional_product_4 = models.Product(
            # allowed_free_transport==0,
            ##allowed_free_transport=_minimum_amount==2,
            availability_id=attribute_functional_product_12.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_product_1.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="folder/testing-fourth-product-model.jpg",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_product_4.manufacturer_id,
            minimum=1,
            model="Functional testing cart - fourth product model",
            mpn=2,
            name="Functional testing cart - fourth product name - tpf4",
            one_batch=True,
            pieces=1,
            points=0,
            price=69.00,
            quantity=1,
            seo_slug="testing-functional-fourth-product-name",
            setting_template_id=setting_template_1.setting_template_id,
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_product_1.tax_id,
            unit=Decimal(1.44),
            upc=6,
            viewed=0,
            virtual=0,
            weight=1.00,
            weight_class_id=1,
            width=1,
        )
        dbsession.add_all(
            [
                category_functional_product_3,
                product_functional_product_1,
                product_functional_product_2,
                product_functional_product_3,
                product_functional_product_4,
            ]
        )
        dbsession.flush()
        global product_id_1
        product_id_1 = str(product_functional_product_1.product_id)
        global product_id_2
        product_id_2 = str(product_functional_product_2.product_id)
        global product_id_3
        product_id_3 = str(product_functional_product_3.product_id)
        global product_id_4
        product_id_4 = str(product_functional_product_4.product_id)
        product_description_functional_product_1 = models.ProductDescription(
            description="This is a product description",
            language_id=2,
            meta_description="this is a meta_description",
            name="This is THE OTHER name",
            product_id=product_functional_product_1.product_id,
            tag="This is a tag",
            u_h1="This is a h1 headline",
            u_title="This is a u_title",
            description_technical="This is a techical description",
            excerpt="This is an excerpt",
        )
        product_description_functional_product_2 = models.ProductDescription(
            description="This is a second product description",
            language_id=2,
            meta_description="this is a second meta_description",
            name="This is THE second OTHER name",
            product_id=product_functional_product_2.product_id,
            tag="This is sedond a tag",
            u_h1="This is a second h1 headline",
            u_title="This is a second u_title",
            description_technical="This is a techical description",
            excerpt="This is an excerpt",
        )
        product_description_functional_product_3 = models.ProductDescription(
            description="This is a third product description",
            language_id=2,
            meta_description="this is a third meta_description",
            name="This is THE third OTHER name",
            product_id=product_functional_product_3.product_id,
            tag="This is a third tag",
            u_h1="This is a third h1 headline",
            u_title="This is a third u_title",
            description_technical="This is a techical description",
            excerpt="This is an excerpt",
        )
        product_description_functional_product_4 = models.ProductDescription(
            description="This is a fourth product description",
            language_id=2,
            meta_description="this is a fourth meta_description",
            name="This is THE fourth OTHER name",
            product_id=product_functional_product_4.product_id,
            tag="This is a fourth tag",
            u_h1="This is a fourth h1 headline",
            u_title="This is a fourth u_title",
            description_technical="This is a techical description",
            excerpt="This is an excerpt",
        )
        collection_functional_product_1 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/first-collection.jpg",
            manufacturer_id=manufacturer_functional_product_1.manufacturer_id,
            name="First collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_product_2 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/second-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_product_1.manufacturer_id,
            name="Second collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_product_3 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/third-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_product_2.manufacturer_id,
            name="Third collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_product_4 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fourth-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_product_3.manufacturer_id,
            name="Fourth collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_product_5 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fifth-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_product_4.manufacturer_id,
            name="Fifth collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        dbsession.add_all(
            [
                product_description_functional_product_1,
                product_description_functional_product_2,
                product_description_functional_product_3,
                product_description_functional_product_4,
                collection_functional_product_1,
                collection_functional_product_2,
                collection_functional_product_3,
                collection_functional_product_4,
                collection_functional_product_5,
            ]
        )
        dbsession.flush()
        product_attribute_functional_product_1 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_1.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_2 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_2.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_3 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_3.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_4 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_4.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_5 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_5.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_6 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_6.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_7 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_7.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_8 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_8.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_9 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_9.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_10 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_13.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_11 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_18.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_12 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_20.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_13 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_21.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_14 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_23.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_15 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_24.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_16 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_10.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_17 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_13.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_18 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_18.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_19 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_20.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_20 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_21.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_21 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_23.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_22 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_24.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_23 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_11.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_24 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_15.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_25 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_19.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_25 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_20.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_26 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_22.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_27 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_23.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_28 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_24.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_collection_functional_product_1 = models.ProductToCollection(
            collection_id=collection_functional_product_1.collection_id,
            product_id=product_functional_product_1.product_id,
        )
        product_collection_functional_product_2 = models.ProductToCollection(
            collection_id=collection_functional_product_2.collection_id,
            product_id=product_functional_product_1.product_id,
        )
        product_collection_functional_product_3 = models.ProductToCollection(
            collection_id=collection_functional_product_3.collection_id,
            product_id=product_functional_product_1.product_id,
        )
        product_collection_functional_product_4 = models.ProductToCollection(
            collection_id=collection_functional_product_2.collection_id,
            product_id=product_functional_product_2.product_id,
        )
        product_collection_functional_product_5 = models.ProductToCollection(
            collection_id=collection_functional_product_3.collection_id,
            product_id=product_functional_product_2.product_id,
        )
        product_collection_functional_product_6 = models.ProductToCollection(
            collection_id=collection_functional_product_4.collection_id,
            product_id=product_functional_product_2.product_id,
        )
        product_collection_functional_product_7 = models.ProductToCollection(
            collection_id=collection_functional_product_1.collection_id,
            product_id=product_functional_product_3.product_id,
        )
        product_collection_functional_product_8 = models.ProductToCollection(
            collection_id=collection_functional_product_2.collection_id,
            product_id=product_functional_product_3.product_id,
        )
        product_collection_functional_product_9 = models.ProductToCollection(
            collection_id=collection_functional_product_4.collection_id,
            product_id=product_functional_product_3.product_id,
        )
        product_collection_functional_product_10 = models.ProductToCollection(
            collection_id=collection_functional_product_1.collection_id,
            product_id=product_functional_product_4.product_id,
        )
        product_collection_functional_product_11 = models.ProductToCollection(
            collection_id=collection_functional_product_2.collection_id,
            product_id=product_functional_product_4.product_id,
        )
        product_collection_functional_product_12 = models.ProductToCollection(
            collection_id=collection_functional_product_4.collection_id,
            product_id=product_functional_product_4.product_id,
        )
        product_category_functional_product_1 = models.ProductToCategory(
            category_id=category_functional_product_1.category_id,
            product_id=product_functional_product_1.product_id,
        )
        product_category_functional_product_2 = models.ProductToCategory(
            category_id=category_functional_product_2.category_id,
            product_id=product_functional_product_1.product_id,
        )
        product_category_functional_product_3 = models.ProductToCategory(
            category_id=category_functional_product_3.category_id,
            product_id=product_functional_product_2.product_id,
        )
        product_category_functional_product_4 = models.ProductToCategory(
            category_id=category_functional_product_2.category_id,
            product_id=product_functional_product_2.product_id,
        )
        product_category_functional_product_5 = models.ProductToCategory(
            category_id=category_functional_product_3.category_id,
            product_id=product_functional_product_3.product_id,
        )
        dbsession.add_all(
            [
                product_attribute_functional_product_1,
                product_attribute_functional_product_2,
                product_attribute_functional_product_3,
                product_attribute_functional_product_4,
                product_attribute_functional_product_5,
                product_attribute_functional_product_6,
                product_attribute_functional_product_7,
                product_attribute_functional_product_8,
                product_attribute_functional_product_9,
                product_attribute_functional_product_10,
                product_attribute_functional_product_11,
                product_attribute_functional_product_12,
                product_attribute_functional_product_13,
                product_attribute_functional_product_14,
                product_attribute_functional_product_15,
                product_attribute_functional_product_16,
                product_attribute_functional_product_17,
                product_attribute_functional_product_18,
                product_attribute_functional_product_19,
                product_attribute_functional_product_20,
                product_attribute_functional_product_21,
                product_attribute_functional_product_22,
                product_attribute_functional_product_23,
                product_attribute_functional_product_24,
                product_attribute_functional_product_25,
                product_attribute_functional_product_26,
                product_attribute_functional_product_27,
                product_attribute_functional_product_28,
                product_collection_functional_product_1,
                product_collection_functional_product_2,
                product_collection_functional_product_3,
                product_collection_functional_product_4,
                product_collection_functional_product_5,
                product_collection_functional_product_6,
                product_collection_functional_product_7,
                product_collection_functional_product_8,
                product_collection_functional_product_9,
                product_collection_functional_product_10,
                product_collection_functional_product_11,
                product_collection_functional_product_12,
                product_category_functional_product_1,
                product_category_functional_product_2,
                product_category_functional_product_3,
                product_category_functional_product_4,
                product_category_functional_product_5,
            ]
        )
        cart_1 = models.Cart(
            date_creation=datetime.now(),
            date_accessed=datetime.now(),
            products={
                product_functional_product_1.product_id: {
                    "product_quantity": Decimal(product_functional_product_1.unit),
                    "product_single_set": "true",
                }
            },
        )
        cart_2 = models.Cart(
            customer_id=customer_id_1,
            date_creation=datetime.now(),
            date_accessed=datetime.now(),
            products={
                product_functional_product_2.product_id: {
                    "product_quantity": Decimal(product_functional_product_2.unit),
                    "product_single_set": "true",
                }
            },
        )
        cart_3 = models.Cart(
            customer_id=customer_id_2,
            date_creation=datetime.now(),
            date_accessed=datetime.now(),
            products={
                product_functional_product_3.product_id: {
                    "product_quantity": Decimal(product_functional_product_3.unit),
                    "product_single_set": "true",
                },
                product_functional_product_4.product_id: {
                    "product_quantity": Decimal(2.88),
                    "product_single_set": "true",
                },
            },
        )
        cart_4 = models.Cart(
            customer_id=customer_id_3,
            date_creation=datetime.now(),
            date_accessed=datetime.now(),
            products={
                product_functional_product_3.product_id: {
                    "product_quantity": Decimal(product_functional_product_3.unit),
                    "product_single_set": "true",
                },
                product_functional_product_4.product_id: {
                    "product_quantity": Decimal(28.8),
                    "product_single_set": "true",
                },
            },
        )
        product_functional_transport_1 = models.ProductToTransport(
            product_id=product_functional_product_3.product_id,
            transport_id=transport_1.transport_id,
            local_settings={
                "apply_to_cart": True,
            },
        )
        product_functional_transport_2 = models.ProductToTransport(
            product_id=product_functional_product_3.product_id,
            transport_id=transport_2.transport_id,
            local_settings={
                "minium_quantity": 18,
            },
        )
        product_functional_transport_3 = models.ProductToTransport(
            product_id=product_functional_product_4.product_id,
            transport_id=transport_2.transport_id,
            local_settings={
                "minium_quantity": 20,
            },
        )
        dbsession.add_all(
            [
                cart_1,
                cart_2,
                cart_3,
                cart_4,
                product_functional_transport_1,
                product_functional_transport_2,
                product_functional_transport_1,
            ]
        )
        dbsession.flush()

        global customer_mail_1
        customer_mail_1 = customer_functional_cart_1.mail

        global customer_mail_2
        customer_mail_2 = customer_functional_cart_2.mail

        global customer_mail_3
        customer_mail_3 = customer_functional_cart_3.mail


def test_no_products_in_cart(testapp):
    res = testapp.get("/cart/", status=200)
    assert b"cart_empty" in res.body


def test_logging_customer_loads_his_cart(testapp):
    testapp.login_customer(
        {
            "mail": customer_mail_1,
            "password": customer_password_1,
            "next": "s_home",
        }
    )
    res = testapp.get("/cart/", status=200)
    assert b"sign/out" in res.body
    assert b"cart_full" in res.body
    assert bytes("product_name_" + product_id_2, "utf-8") in res.body
    assert bytes(product_id_2, "utf-8") in res.body


def test_cart_displays_single_pack_input(testapp):
    testapp.login_customer(
        {
            "mail": customer_mail_1,
            "password": customer_password_1,
        }
    )
    res = testapp.get("/cart/", status=200)
    assert bytes("product_name_" + product_id_2, "utf-8") in res.body
    assert bytes(product_id_2 + "_pack", "utf-8") in res.body
    assert bytes(product_id_2 + "_meters", "utf-8") not in res.body


def test_cart_displays_two_products(testapp):
    testapp.login_customer(
        {
            "mail": customer_mail_2,
            "password": customer_password_2,
        }
    )
    res = testapp.get("/cart/", status=200)
    assert bytes("product_name_" + product_id_3, "utf-8") in res.body
    assert bytes("product_name_" + product_id_4, "utf-8") in res.body


def test_cart_displays_two_products_with_different_inputs(testapp):
    testapp.login_customer(
        {
            "mail": customer_mail_2,
            "password": customer_password_2,
        }
    )
    res = testapp.get("/cart/", status=200)
    # You can buy product no. 3 by calculating the number of units using package input only.
    # Meters input should be not present in the code.
    assert bytes(product_id_3 + "_pack", "utf-8") in res.body
    assert bytes(product_id_3 + "_meters", "utf-8") not in res.body
    # You can buy product no. 4 by calculating the number of units using packages or meters inputs.
    assert bytes(product_id_4 + "_pack", "utf-8") in res.body
    assert bytes(product_id_4 + "_meters", "utf-8") in res.body


def test_cart_displays_correct_unit_and_sq_meter_prices_for_every_product_in_the_row(
    testapp,
):
    testapp.login_customer(
        {
            "mail": customer_mail_2,
            "password": customer_password_2,
        }
    )
    res = testapp.get("/cart/", status=200)
    # Price can be displayed both as per square meter and per unit. The price with dot is hidden.
    # Product no. 3
    assert b"84,87" in res.body
    assert b"122.21" in res.body
    # Product no. 4
    assert b"29,98" in res.body
    assert b"29.98" in res.body


def test_cart_calculate_summarized_prices_for_every_product_in_the_row(testapp):
    testapp.login_customer(
        {
            "mail": customer_mail_2,
            "password": customer_password_2,
        }
    )
    res = testapp.get("/cart/", status=200)
    # Every product displays summarized price for all the units in cart.
    # Product no. 3
    assert b"244,43" in res.body
    # Product no. 4
    assert b"29,98" in res.body


def test_cart_for_more_than_two_units_of_the_same_product_display_single_set_input(
    testapp,
):
    testapp.login_customer(
        {
            "mail": customer_mail_2,
            "password": customer_password_2,
        }
    )
    res = testapp.get("/cart/", status=200)
    # Buing >2 packages of tiles forces to display select input "single set".
    # For this cart and user "single set" input should be visible for product no. 3 only.
    assert bytes("single_set_" + product_id_3, "utf-8") not in res.body
    assert bytes("single_set_" + product_id_4, "utf-8") in res.body
