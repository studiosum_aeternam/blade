from emporium import helpers, models


def clearDatabase(dbsession):
    dbsession.query(models.Category).delete()
    dbsession.query(models.ProductToCategory).delete()
    dbsession.query(models.ProductToAttribute).delete()
    dbsession.query(models.ProductToCollection).delete()
    dbsession.query(models.Collection).delete()
    dbsession.query(models.Product).delete()
    dbsession.query(models.Attribute).delete()
    dbsession.query(models.AttributeGroup).delete()
    dbsession.query(models.AttributeDisplayType).delete()
    dbsession.query(models.Setting).delete()
    helpers.CacheHelpers.flush_cache()
