import pytest
import transaction

from emporium import helpers, models
from tests import functional

basic_login = {"login": "basic", "password": "basic", "role": "customer"}
editor_login = {"login": "editor", "password": "editor", "role": "emperor"}


@pytest.fixture(scope="session", autouse=True)
def dummy_data(app):
    """
    Add some dummy data to the database.

    Note that this is a session fixture that commits data to the database.
    Think about it similarly to running the ``initialize_db`` script at the
    start of the test suite.

    This data should not conflict with any other data added throughout the
    test suite or there will be issues - so be careful with this pattern!

    """
    tm = transaction.TransactionManager(explicit=True)
    with tm:
        dbsession = models.get_tm_session(app.registry["dbsession_factory"], tm)
        functional.clearDatabase(dbsession)

        setting_functional_product_1 = models.Setting(
            key="cache", value=False, setting_type=0
        )
        setting_functional_product_2 = models.Setting(
            key="shop_status", value=True, setting_type=0
        )
        setting_functional_product_3 = models.Setting(
            key="name", value="Emporatorium test name", setting_type=0
        )
        # fmt: off
        setting_functional_product_4 = models.Setting(
            key="image_settings",
            complex_values={"image_sizes": {"base": [1200, 1200], "thumbnails": [500, 500]},"image_extensions": [["JPEG", "jpg", "RGB"],["WEBP", "webp", "RGBA"]]},
            setting_type=0,
        )
        setting_functional_product_5 = models.Setting(
            key="product_settings",
            complex_values={
            "artisan_products_display":1,
            "default_download_limit":1,
            "default_substract_quantity":1,
            "description_technical": 1,
            "display_ean_on_product":1,
            "display_excerpt_on_catalog":0,
            "display_price_filter_on_catalog":0,
            "display_weight":1,
            "downlodable_products":1,
            "excerpt":0,
            "product_attributes_templates":0,
            "product_require_storage":1
            },
            setting_type=0,
        )
        # fmt: on
        editor_functional_product = models.User(
            first_name="Jack", last_name="Black", role="emperor", login="editor"
        )
        editor_functional_product.set_password("editor")
        basic_functional_product = models.User(
            first_name="Jack", last_name="Black", role="customer", login="basic"
        )
        basic_functional_product.set_password("basic")
        attribute_display_type_functional_product_1 = models.AttributeDisplayType(
            name="select", description="Multiple select box", sort_order="1", status="1"
        )
        dbsession.add_all(
            [
                basic_functional_product,
                editor_functional_product,
                setting_functional_product_1,
                setting_functional_product_2,
                setting_functional_product_3,
                setting_functional_product_4,
                setting_functional_product_5,
                attribute_display_type_functional_product_1,
                attribute_display_type_functional_product_1,
            ]
        )
        dbsession.flush()
        attribute_group_functional_product_1 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Antislip",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_2 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Availability",
            required=True,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_3 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Frost",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_4 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Measurements",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_5 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Pei",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_6 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Quality",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_7 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Rekt",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_8 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Surface",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_product_9 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_functional_product_1.attribute_display_type_id,
            name="Tiles",
            required=False,
            sort_order=3,
            status=1,
        )
        tax_functional_product_1 = models.Tax(status=1, value=23, tax_type=1)
        dbsession.add_all(
            (
                attribute_group_functional_product_1,
                attribute_group_functional_product_2,
                attribute_group_functional_product_3,
                attribute_group_functional_product_4,
                attribute_group_functional_product_5,
                attribute_group_functional_product_6,
                attribute_group_functional_product_7,
                attribute_group_functional_product_8,
                attribute_group_functional_product_9,
                tax_functional_product_1,
            )
        )
        dbsession.flush()
        tax_description_functional_product_1 = models.TaxDescription(
            tax_id=tax_functional_product_1.tax_id,
            language_id=2,
            name="VAT 23% (FUNCTIONAL)",
            description="PODATEK VAT OPIS",
        )
        attribute_functional_product_1 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_product_2 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Magazyn",
        )
        attribute_functional_product_3 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_3.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_product_4 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="10x20",
        )
        attribute_functional_product_5 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_5.attribute_group_id,
            sort_order=1,
            status=1,
            value="PEI III",
        )
        attribute_functional_product_6 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_6.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pierwsza",
        )
        attribute_functional_product_7 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_product_8 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Poler",
        )
        attribute_functional_product_9 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Nie",
        )
        attribute_functional_product_10 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="2-3 dni",
        )
        attribute_functional_product_11 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Niedostępne",
        )
        attribute_functional_product_12 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Zapytaj o dostępność",
        )
        attribute_functional_product_13 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="20x20",
        )
        attribute_functional_product_14 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="20x30",
        )
        attribute_functional_product_15 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="30x30",
        )
        attribute_functional_product_16 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="40x40",
        )
        attribute_functional_product_17 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="40x60",
        )
        attribute_functional_product_18 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_5.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pei I",
        )
        attribute_functional_product_19 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_5.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pei II",
        )
        attribute_functional_product_20 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Nie",
        )
        attribute_functional_product_21 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Mat",
        )
        attribute_functional_product_22 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Połysk",
        )
        attribute_functional_product_23 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_product_24 = models.Attribute(
            attribute_group_id=attribute_group_functional_product_9.attribute_group_id,
            sort_order=1,
            status=1,
            value="Nie",
        )
        discount_functional_product_1 = models.Discount(
            status=1, subtract=1, value=0.00, default=True
        )
        transport_functional_product_1 = models.Transport(
            special_method=True,
            description="",
            excerpt="",
            module_name="courier",
            module_settings={
                "manipulation_fee": "0",
                "unit_fee": "139",
                "unit_weight": "1000",
            },
            name="Kurier",
            status=True,
        )
        transport_functional_product_2 = models.Transport(
            special_method=False,
            description="",
            excerpt="",
            module_name="courier_bulk",
            module_settings={
                "manipulation_fee": "180",
                "removed_transports": [],
                "unit_fee": "139",
                "unit_weight": "1000",
            },
            name="Kurier gabaryt",
            status=True,
        )
        transport_functional_product_3 = models.Transport(
            special_method=False,
            description="",
            excerpt="",
            module_name="free_delivery",
            module_settings={
                "removed_transports": [],
            },
            name="Darmowy transport",
            status=True,
        )
        manufacturer_functional_product_1 = models.Manufacturer(
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fist-manufacturer.jpg",
            sort_order=1,
            name="Other Fist Manufacturer",
            priority=0,
            seo_slug="other_first_manufacturer",
            status=1,
            code="",
            manufacturer_type=0,
        )
        manufacturer_functional_product_2 = models.Manufacturer(
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/second-manufacturer.jpg",
            sort_order=1,
            name="Opoczno",
            priority=0,
            seo_slug="second_manufacturer",
            status=1,
            code="",
            manufacturer_type=0,
        )
        manufacturer_functional_product_3 = models.Manufacturer(
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/third-manufacturer.jpg",
            sort_order=1,
            name="Paradyż",
            priority=0,
            seo_slug="third_manufacturer",
            status=1,
            code="",
            manufacturer_type=0,
        )
        manufacturer_functional_product_4 = models.Manufacturer(
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fourth-manufacturer.jpg",
            sort_order=1,
            name="Cicogres",
            priority=0,
            status=1,
            code="",
            seo_slug="fourth_manufacturer",
            manufacturer_type=0,
        )
        category_functional_product_1 = models.Category(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/first_category.jpg",
            name="Płytki",
            parent_id=None,
            placeholder=False,
            sort_order=1,
            status=1,
        )
        category_functional_product_2 = models.Category(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/second_category.jpg",
            name="Bestsellery",
            parent_id=None,
            placeholder=False,
            sort_order=1,
            status=1,
        )
        setting_template_1 = models.SettingTemplate(
            description="Setting template description",
            name="Tile template",
            order=1,
            status=1,
            template_json={
                "manufactured": 1,
                "virtual": 0,
                "subtract_quantity": 0,
                "storage_type": "",
                "attribute_type_list": [
                    attribute_group_functional_product_1.attribute_group_id,
                    attribute_group_functional_product_2.attribute_group_id,
                    attribute_group_functional_product_3.attribute_group_id,
                    attribute_group_functional_product_4.attribute_group_id,
                    attribute_group_functional_product_5.attribute_group_id,
                    attribute_group_functional_product_6.attribute_group_id,
                    attribute_group_functional_product_7.attribute_group_id,
                    attribute_group_functional_product_8.attribute_group_id,
                    attribute_group_functional_product_9.attribute_group_id,
                ],
                "unit_label": 2,
            },
            template_type=1,
        )
        dbsession.add_all(
            (
                attribute_functional_product_1,
                attribute_functional_product_10,
                attribute_functional_product_11,
                attribute_functional_product_12,
                attribute_functional_product_13,
                attribute_functional_product_14,
                attribute_functional_product_15,
                attribute_functional_product_16,
                attribute_functional_product_17,
                attribute_functional_product_18,
                attribute_functional_product_19,
                attribute_functional_product_2,
                attribute_functional_product_20,
                attribute_functional_product_21,
                attribute_functional_product_22,
                attribute_functional_product_23,
                attribute_functional_product_24,
                attribute_functional_product_3,
                attribute_functional_product_4,
                attribute_functional_product_5,
                attribute_functional_product_6,
                attribute_functional_product_7,
                attribute_functional_product_8,
                attribute_functional_product_9,
                category_functional_product_1,
                category_functional_product_2,
                discount_functional_product_1,
                manufacturer_functional_product_1,
                manufacturer_functional_product_2,
                manufacturer_functional_product_3,
                manufacturer_functional_product_4,
                tax_description_functional_product_1,
                transport_functional_product_1,
                transport_functional_product_2,
                transport_functional_product_3,
                setting_template_1,
            )
        )
        dbsession.flush()
        category_functional_product_3 = models.Category(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/third-manufacturer.jpg",
            name="Drewnopodobne",
            parent_id=category_functional_product_1.category_id,
            placeholder=False,
            sort_order=1,
            status=1,
        )
        product_functional_product_1 = models.Product(
            # allowed_free_transport==0,
            # allowed_free_transport=_minimum_amount=2,
            availability_id=attribute_functional_product_2.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_product_1.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="testing-product-model",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_product_1.manufacturer_id,
            minimum=1.01,
            model="Functional testing product model",
            mpn=2,
            name="Functional testing first product name - tpf1",
            one_batch=True,
            pieces=1,
            points=0,
            price=20.55,
            quantity=1,
            seo_slug="testing-functional-first-product-name",
            setting_template_id=setting_template_1.setting_template_id,
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_product_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=28.00,
            weight_class_id=1,
            width=1,
        )
        product_functional_product_2 = models.Product(
            # allowed_free_transport==0,
            # allowed_free_transport=_minimum_amount=2,
            availability_id=attribute_functional_product_10.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_product_1.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="testing-product-model",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_product_2.manufacturer_id,
            minimum=1.01,
            model="Functional testing second product model",
            mpn=2,
            name="Functional testing second product name - tpf2",
            one_batch=True,
            pieces=1,
            points=0,
            price=30.55,
            quantity=1,
            seo_slug="testing-functional-second-product-name",
            setting_template_id=setting_template_1.setting_template_id,
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_product_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=1.00,
            weight_class_id=1,
            width=1,
        )
        product_functional_product_3 = models.Product(
            # allowed_free_transport==0,
            # allowed_free_transport=_minimum_amount=2,
            availability_id=attribute_functional_product_11.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_product_1.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="testing-third-product-model",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_product_3.manufacturer_id,
            minimum=1.01,
            model="Functional testing third product model",
            mpn=2,
            name="Functional testing third product name - tpf3",
            one_batch=True,
            pieces=1,
            points=0,
            price=40.55,
            quantity=1,
            seo_slug="testing-functional-third-product-name",
            setting_template_id=setting_template_1.setting_template_id,
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_product_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=1.00,
            weight_class_id=1,
            width=1,
        )
        product_functional_product_4 = models.Product(
            # allowed_free_transport==0,
            # allowed_free_transport=_minimum_amount=2,
            availability_id=attribute_functional_product_12.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_product_1.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="testing-fourth-product-model",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_product_4.manufacturer_id,
            minimum=1.01,
            model="Functional testing fourth product model",
            mpn=2,
            name="Functional testing fourth product name - tpf4",
            one_batch=True,
            pieces=1,
            points=0,
            price=50.55,
            quantity=1,
            seo_slug="testing-functional-fourth-product-name",
            setting_template_id=setting_template_1.setting_template_id,
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_product_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=1.00,
            weight_class_id=1,
            width=1,
        )
        dbsession.add_all(
            [
                category_functional_product_3,
                product_functional_product_1,
                product_functional_product_2,
                product_functional_product_3,
                product_functional_product_4,
            ]
        )
        dbsession.flush()
        product_description_functional_product_1 = models.ProductDescription(
            description="This is a product description",
            description_technical="This is a techical description",
            excerpt="This is an excerpt",
            language_id=2,
            meta_description="this is a meta_description",
            name="This is THE OTHER name",
            product_id=product_functional_product_1.product_id,
            tag="This is a tag",
            u_h1="This is a h1 headline",
            u_title="This is a u_title",
        )
        product_description_functional_product_2 = models.ProductDescription(
            description="This is a second product description",
            description_technical="This is a techical description",
            excerpt="This is an excerpt",
            language_id=2,
            meta_description="this is a second meta_description",
            name="This is THE second OTHER name",
            product_id=product_functional_product_2.product_id,
            tag="This is sedond a tag",
            u_h1="This is a second h1 headline",
            u_title="This is a second u_title",
        )
        product_description_functional_product_3 = models.ProductDescription(
            description="This is a third product description",
            description_technical="",
            excerpt="This is an excerpt",
            language_id=2,
            meta_description="this is a third meta_description",
            name="This is THE third OTHER name",
            product_id=product_functional_product_3.product_id,
            tag="This is a third tag",
            u_h1="This is a third h1 headline",
            u_title="This is a third u_title",
        )
        product_description_functional_product_4 = models.ProductDescription(
            description="This is a fourth product description",
            description_technical="",
            excerpt="This is an excerpt",
            language_id=2,
            meta_description="this is a fourth meta_description",
            name="This is THE fourth OTHER name",
            product_id=product_functional_product_4.product_id,
            tag="This is a fourth tag",
            u_h1="This is a fourth h1 headline",
            u_title="This is a fourth u_title",
        )
        collection_functional_product_1 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/first-collection.jpg",
            manufacturer_id=manufacturer_functional_product_1.manufacturer_id,
            name="First collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_product_2 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/second-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_product_1.manufacturer_id,
            name="Second collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_product_3 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/third-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_product_2.manufacturer_id,
            name="Third collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_product_4 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fourth-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_product_3.manufacturer_id,
            name="Fourth collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_product_5 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fifth-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_product_4.manufacturer_id,
            name="Fifth collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        dbsession.add_all(
            [
                product_description_functional_product_1,
                product_description_functional_product_2,
                product_description_functional_product_3,
                product_description_functional_product_4,
                collection_functional_product_1,
                collection_functional_product_2,
                collection_functional_product_3,
                collection_functional_product_4,
                collection_functional_product_5,
            ]
        )
        dbsession.flush()
        product_attribute_functional_product_1 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_1.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_2 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_2.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_3 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_3.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_4 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_4.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_5 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_5.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_6 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_6.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_7 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_7.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_8 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_8.attribute_id,
            product_id=product_functional_product_1.product_id,
        )
        product_attribute_functional_product_9 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_9.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_10 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_13.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_11 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_18.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_12 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_20.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_13 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_21.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_14 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_23.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_15 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_24.attribute_id,
            product_id=product_functional_product_2.product_id,
        )
        product_attribute_functional_product_16 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_10.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_17 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_13.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_18 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_18.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_19 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_20.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_20 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_21.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_21 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_23.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_22 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_24.attribute_id,
            product_id=product_functional_product_3.product_id,
        )
        product_attribute_functional_product_23 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_11.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_24 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_15.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_25 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_19.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_25 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_20.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_26 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_22.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_27 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_23.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_attribute_functional_product_28 = models.ProductToAttribute(
            attribute_id=attribute_functional_product_24.attribute_id,
            product_id=product_functional_product_4.product_id,
        )
        product_collection_functional_product_1 = models.ProductToCollection(
            collection_id=collection_functional_product_1.collection_id,
            product_id=product_functional_product_1.product_id,
        )
        product_collection_functional_product_2 = models.ProductToCollection(
            collection_id=collection_functional_product_2.collection_id,
            product_id=product_functional_product_1.product_id,
        )
        product_collection_functional_product_3 = models.ProductToCollection(
            collection_id=collection_functional_product_3.collection_id,
            product_id=product_functional_product_1.product_id,
        )
        product_collection_functional_product_4 = models.ProductToCollection(
            collection_id=collection_functional_product_2.collection_id,
            product_id=product_functional_product_2.product_id,
        )
        product_collection_functional_product_5 = models.ProductToCollection(
            collection_id=collection_functional_product_3.collection_id,
            product_id=product_functional_product_2.product_id,
        )
        product_collection_functional_product_6 = models.ProductToCollection(
            collection_id=collection_functional_product_4.collection_id,
            product_id=product_functional_product_2.product_id,
        )
        product_collection_functional_product_7 = models.ProductToCollection(
            collection_id=collection_functional_product_1.collection_id,
            product_id=product_functional_product_3.product_id,
        )
        product_collection_functional_product_8 = models.ProductToCollection(
            collection_id=collection_functional_product_2.collection_id,
            product_id=product_functional_product_3.product_id,
        )
        product_collection_functional_product_9 = models.ProductToCollection(
            collection_id=collection_functional_product_4.collection_id,
            product_id=product_functional_product_3.product_id,
        )
        product_collection_functional_product_10 = models.ProductToCollection(
            collection_id=collection_functional_product_1.collection_id,
            product_id=product_functional_product_4.product_id,
        )
        product_collection_functional_product_11 = models.ProductToCollection(
            collection_id=collection_functional_product_2.collection_id,
            product_id=product_functional_product_4.product_id,
        )
        product_collection_functional_product_12 = models.ProductToCollection(
            collection_id=collection_functional_product_4.collection_id,
            product_id=product_functional_product_4.product_id,
        )
        product_category_functional_product_1 = models.ProductToCategory(
            category_id=category_functional_product_1.category_id,
            product_id=product_functional_product_1.product_id,
        )
        product_category_functional_product_2 = models.ProductToCategory(
            category_id=category_functional_product_2.category_id,
            product_id=product_functional_product_1.product_id,
        )
        product_category_functional_product_3 = models.ProductToCategory(
            category_id=category_functional_product_3.category_id,
            product_id=product_functional_product_2.product_id,
        )
        product_category_functional_product_4 = models.ProductToCategory(
            category_id=category_functional_product_2.category_id,
            product_id=product_functional_product_2.product_id,
        )
        product_category_functional_product_5 = models.ProductToCategory(
            category_id=category_functional_product_3.category_id,
            product_id=product_functional_product_3.product_id,
        )
        product_transport_functional_product_1 = models.ProductToTransport(
            product_id=product_functional_product_2.product_id,
            transport_id=transport_functional_product_2.transport_id,
            local_settings={"minimum_requirement": 20},
        )
        dbsession.add_all(
            [
                product_attribute_functional_product_1,
                product_attribute_functional_product_2,
                product_attribute_functional_product_3,
                product_attribute_functional_product_4,
                product_attribute_functional_product_5,
                product_attribute_functional_product_6,
                product_attribute_functional_product_7,
                product_attribute_functional_product_8,
                product_attribute_functional_product_9,
                product_attribute_functional_product_10,
                product_attribute_functional_product_11,
                product_attribute_functional_product_12,
                product_attribute_functional_product_13,
                product_attribute_functional_product_14,
                product_attribute_functional_product_15,
                product_attribute_functional_product_16,
                product_attribute_functional_product_17,
                product_attribute_functional_product_18,
                product_attribute_functional_product_19,
                product_attribute_functional_product_20,
                product_attribute_functional_product_21,
                product_attribute_functional_product_22,
                product_attribute_functional_product_23,
                product_attribute_functional_product_24,
                product_attribute_functional_product_25,
                product_attribute_functional_product_26,
                product_attribute_functional_product_27,
                product_attribute_functional_product_28,
                product_collection_functional_product_1,
                product_collection_functional_product_2,
                product_collection_functional_product_3,
                product_collection_functional_product_4,
                product_collection_functional_product_5,
                product_collection_functional_product_6,
                product_collection_functional_product_7,
                product_collection_functional_product_8,
                product_collection_functional_product_9,
                product_collection_functional_product_10,
                product_collection_functional_product_11,
                product_collection_functional_product_12,
                product_category_functional_product_1,
                product_category_functional_product_2,
                product_category_functional_product_3,
                product_category_functional_product_4,
                product_category_functional_product_5,
                product_transport_functional_product_1,
            ]
        )
        dbsession.flush()
        ###
        global product_id_functional_product_1
        product_id_functional_product_1 = product_functional_product_1.product_id
        global product_functional_product_name_1
        product_functional_product_name_1 = product_functional_product_1.name
        global product_slug_functional_product_1
        product_slug_functional_product_1 = product_functional_product_1.seo_slug

        global product_id_functional_product_2
        product_id_functional_product_2 = product_functional_product_2.product_id
        global product_functional_product_name_2
        product_functional_product_name_2 = product_functional_product_2.name
        global product_slug_functional_product_2
        product_slug_functional_product_2 = product_functional_product_2.seo_slug

        global product_id_functional_product_3
        product_id_functional_product_3 = product_functional_product_3.product_id
        # global product_functional_product_name_2
        # product_functional_product_name_2 = product_functional_product_2.name
        global product_slug_functional_product_3
        product_slug_functional_product_3 = product_functional_product_3.seo_slug

        global manufacturer_id_functional_product_1
        manufacturer_id_functional_product_1 = (
            manufacturer_functional_product_1.manufacturer_id
        )
        global manufacturer_id_functional_product_2
        manufacturer_id_functional_product_2 = (
            manufacturer_functional_product_2.manufacturer_id
        )
        global manufacturer_id_functional_product_3
        manufacturer_id_functional_product_3 = (
            manufacturer_functional_product_3.manufacturer_id
        )
        global manufacturer_id_functional_product_4
        manufacturer_id_functional_product_4 = (
            manufacturer_functional_product_4.manufacturer_id
        )
        global category_id_functional_product_1
        category_id_functional_product_1 = category_functional_product_1.category_id
        global category_id_functional_product_2
        category_id_functional_product_2 = category_functional_product_2.category_id
        global collection_id_functional_product_1
        collection_id_functional_product_1 = (
            collection_functional_product_1.collection_id
        )
        global collection_id_functional_product_2
        collection_id_functional_product_2 = (
            collection_functional_product_2.collection_id
        )
        global collection_id_functional_product_3
        collection_id_functional_product_3 = (
            collection_functional_product_3.collection_id
        )
        global collection_id_functional_product_4
        collection_id_functional_product_4 = (
            collection_functional_product_4.collection_id
        )
        global collection_id_functional_product_5
        collection_id_functional_product_5 = (
            collection_functional_product_5.collection_id
        )
        global attribute_id_functional_product_1
        attribute_id_functional_product_1 = attribute_functional_product_1.attribute_id
        global attribute_id_functional_product_3
        attribute_id_functional_product_3 = attribute_functional_product_3.attribute_id
        global attribute_id_functional_product_4
        attribute_id_functional_product_4 = attribute_functional_product_4.attribute_id
        global attribute_id_functional_product_5
        attribute_id_functional_product_5 = attribute_functional_product_5.attribute_id
        global attribute_id_functional_product_6
        attribute_id_functional_product_6 = attribute_functional_product_6.attribute_id
        global attribute_id_functional_product_7
        attribute_id_functional_product_7 = attribute_functional_product_7.attribute_id
        global attribute_id_functional_product_8
        attribute_id_functional_product_8 = attribute_functional_product_8.attribute_id
        global attribute_id_functional_product_9
        attribute_id_functional_product_9 = attribute_functional_product_9.attribute_id
        global attribute_id_functional_product_10
        attribute_id_functional_product_10 = (
            attribute_functional_product_10.attribute_id
        )


# def test_root( testapp):
#     res = testapp.get("/", status=200)
#     assert res.location == "http://example.com"


def test_display_selected_product(testapp):
    res = testapp.get(
        "/"
        + product_slug_functional_product_1
        + "-"
        + str(product_id_functional_product_1),
        status=200,
    )
    assert b"tpf1" in res.body


def test_missing_product(testapp):
    res = testapp.get("/some-product-997766", status=301)
    assert res.location == "http://example.com/"


#################
#  Permissions  #
#################


def test_anonymous_user_cannot_edit(testapp):
    res = testapp.get(
        "/emporatorium/product_edit?product_id=" + str(product_id_functional_product_1),
        status=303,
    ).follow()
    assert b"Login" in res.body


def test_anonymous_user_cannot_add(testapp):
    res = testapp.get("/emporatorium/product_create", status=303).follow()
    assert b"Login" in res.body


def test_customer_cannot_edit(testapp):
    testapp.login(basic_login)
    res = testapp.get(
        "/emporatorium/product_edit?product_id_functional_product_1="
        + str(product_id_functional_product_1),
        status=303,
    ).follow()
    assert b"Login" in res.body


def test_customer_can_add(testapp):
    testapp.login(basic_login)
    res = testapp.get("/emporatorium/product_create", status=303).follow()
    assert b"Login" in res.body


def test_emperors_member_user_can_edit(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_edit?product_id=" + str(product_id_functional_product_1),
        status=200,
    )
    assert b"product_edit_header" in res.body


def test_emperors_member_user_can_add(testapp):
    testapp.login(editor_login)
    res = testapp.get("/emporatorium/product_create", status=200)
    assert b"product_create_header" in res.body


def test_emperors_member_user_can_view(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/"
        + product_slug_functional_product_1
        + "-"
        + str(product_id_functional_product_1),
        status=200,
    )
    assert b"tpf1" in res.body


def test_redirect_to_edit_for_existing_product(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_edit?product_id=" + str(product_id_functional_product_1),
        status=200,
    )
    assert b"product_edit_header" in res.body
    assert b"Functional testing product model" in res.body


#############
# Transport #
#############


def test_create_product_module_special_transport_is_visible(testapp):
    testapp.login(editor_login)
    res = testapp.get("/emporatorium/product_create", status=200)
    assert b"module_transport_courier_bulk" in res.body


def test_create_product_module_special_transport_is_visible(testapp):
    testapp.login(editor_login)
    res = testapp.get("/emporatorium/product_create", status=200)
    assert b"module_transport_courier_bulk" in res.body


def test_edit_product_all_special_transport_modules_are_visible(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_edit?product_id=" + str(product_id_functional_product_1),
        status=200,
    )
    res = testapp.get("/emporatorium/product_create", status=200)
    assert b"module_transport_courier_bulk" in res.body
    assert b"module_transport_free_delivery" in res.body


def test_display_special_transport_rules_on_product_page(testapp):
    res = testapp.get(
        "/"
        + product_slug_functional_product_2
        + "-"
        + str(product_id_functional_product_2),
        status=200,
    )
    assert b"special_transport_rules" in res.body


def test_display_description_technical_on_product_page(testapp):
    res = testapp.get(
        "/"
        + product_slug_functional_product_1
        + "-"
        + str(product_id_functional_product_1),
        status=200,
    )
    assert b"description_technical" in res.body


def test_display_description_technical_hidden_on_product_page(testapp):
    res = testapp.get(
        "/"
        + product_slug_functional_product_3
        + "-"
        + str(product_id_functional_product_3),
        status=200,
    )
    assert b"description_technical" not in res.body


def test_display_admin_product_list(testapp):
    testapp.login(editor_login)
    res = testapp.get("/emporatorium/product_list", status=200)
    assert b"product_list_header" in res.body
    assert b"tpf1" in res.body
    assert b"tpf2" in res.body
    assert b"tpf3" in res.body
    assert b"tpf4" in res.body
    # assert res.body == False


def test_display_admin_product_list_filter_term(testapp):
    testapp.login(editor_login)
    res = testapp.get("/emporatorium/product_list?term=tpf1", status=200)
    assert b"product_list_header" in res.body
    assert b"tpf1" in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_term_no_results(testapp):
    testapp.login(editor_login)
    res = testapp.get("/emporatorium/product_list?term=funfela", status=200)
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_manufacturer(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?status=2&manufacturer="
        + str(manufacturer_id_functional_product_2),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_manufacturer_no_results(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?status=2&manufacturer=99999", status=200
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_category(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&category="
        + str(category_id_functional_product_1),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_category_manufacturer(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&category="
        + str(category_id_functional_product_2)
        + "&manufacturer="
        + str(manufacturer_id_functional_product_1),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_collection(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&collection="
        + str(collection_id_functional_product_1),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" in res.body
    assert b"tpf4" in res.body


def test_display_admin_product_list_filter_collection_no_results(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&collection="
        + str(collection_id_functional_product_5),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_collection_manufacturer(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&collection="
        + str(collection_id_functional_product_4)
        + "&manufacturer="
        + str(manufacturer_id_functional_product_3),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_price_min(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&price_min=38", status=200
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" in res.body
    assert b"tpf4" in res.body


def test_display_admin_product_list_filter_price_max(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&price_max=38", status=200
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" in res.body
    assert b"tpf2" in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_price_max_no_results(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&price_max=10", status=200
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_price_min_and_max(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&price_min=38&price_max=50",
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_price_min_and_max_no_results(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&price_min=138&price_max=250",
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_collection_price_min_and_max(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&price_min=34&price_max=38"
        + "&collection="
        + str(collection_id_functional_product_3),
        status=200,
    )
    # assert res.body == False
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_collection_price_min_and_max_no_results(
    testapp,
):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&price_min=380&price_max=500"
        + "&collection="
        + str(collection_id_functional_product_3),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_manufacturer_price_min_and_max(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&price_min=38&price_max=50"
        + "&manufacturer="
        + str(manufacturer_id_functional_product_3),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_manufacturer_price_min_and_max_no_results(
    testapp,
):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&price_min=380&price_max=500"
        + "&manufacturer="
        + str(manufacturer_id_functional_product_3),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_collection_manufacturer_price_min_and_max(
    testapp,
):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&price_min=19&price_max=28"
        + "&collection="
        + str(collection_id_functional_product_1)
        + "&manufacturer="
        + str(manufacturer_id_functional_product_1),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_collection_manufacturer_price_min_and_max_no_results(
    testapp,
):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&price_min=28&price_max=58"
        + "&collection="
        + str(collection_id_functional_product_1)
        + "&manufacturer="
        + str(manufacturer_id_functional_product_1),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" not in res.body
    assert b"tpf2" not in res.body
    assert b"tpf3" not in res.body
    assert b"tpf4" not in res.body


def test_display_admin_product_list_filter_attribute(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&attribute="
        + str(attribute_id_functional_product_1),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" in res.body
    assert b"tpf2" in res.body
    assert b"tpf3" in res.body
    assert b"tpf4" in res.body


def test_display_admin_product_list_filter_unique_attribute(testapp):
    testapp.login(editor_login)
    res = testapp.get(
        "/emporatorium/product_list?term=&status=2&attribute="
        + str(attribute_id_functional_product_3),
        status=200,
    )
    assert b"product_list_header" in res.body
    assert b"tpf1" in res.body
    assert b"tpf2" in res.body
    assert b"tpf3" in res.body
    assert b"tpf4" in res.body
