import pytest
import transaction

from emporium import helpers, models

from tests import functional


@pytest.fixture(scope="session", autouse=True)
def dummy_data(app):
    """
    Add some dummy data to the database.

    Note that this is a session fixture that commits data to the database.
    Think about it similarly to running the ``initialize_db`` script at the
    start of the test suite.

    This data should not conflict with any other data added throughout the
    test suite or there will be issues - so be careful with this pattern!

    """
    tm = transaction.TransactionManager(explicit=True)
    with tm:
        dbsession = models.get_tm_session(app.registry["dbsession_factory"], tm)
        functional.clearDatabase(dbsession)

        category_setting_1 = models.Setting(key="cache", value=False, setting_type=0)
        category_setting_2 = models.Setting(
            key="shop_status", value=True, setting_type=0
        )
        category_setting_3 = models.Setting(
            key="name", value="Emporatorium test name", setting_type=0
        )
        # fmt: off
        category_setting_4 = models.Setting(
            key="image_settings",
            complex_values={"image_sizes": {"base": [1200, 1200], "thumbnails": [500, 500]},"image_extensions": [["JPEG", "jpg", "RGB"],["WEBP", "webp", "RGBA"]]},
            setting_type=0,
        )
        # fmt: on
        attribute_display_type_1 = models.AttributeDisplayType(
            name="select", description="Multiple select box", sort_order="1", status="1"
        )
        dbsession.add_all(
            [
                category_setting_1,
                category_setting_2,
                category_setting_3,
                category_setting_4,
                attribute_display_type_1,
            ]
        )
        dbsession.flush()
        attribute_group_functional_category_1 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_1.attribute_display_type_id,
            name="Antislip",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_category_2 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_1.attribute_display_type_id,
            name="Availability",
            required=True,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_category_3 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_1.attribute_display_type_id,
            name="Frost",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_category_4 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_1.attribute_display_type_id,
            name="Measurements",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_category_5 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_1.attribute_display_type_id,
            name="Pei",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_category_6 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_1.attribute_display_type_id,
            name="Quality",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_category_7 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_1.attribute_display_type_id,
            name="Rekt",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_category_8 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_1.attribute_display_type_id,
            name="Surface",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group_functional_category_9 = models.AttributeGroup(
            attribute_display_type_id=attribute_display_type_1.attribute_display_type_id,
            name="Tiles",
            required=False,
            sort_order=3,
            status=1,
        )
        tax_functional_category_1 = models.Tax(status=1, value=23, tax_type=1)
        dbsession.add_all(
            (
                attribute_group_functional_category_1,
                attribute_group_functional_category_2,
                attribute_group_functional_category_3,
                attribute_group_functional_category_4,
                attribute_group_functional_category_5,
                attribute_group_functional_category_6,
                attribute_group_functional_category_7,
                attribute_group_functional_category_8,
                attribute_group_functional_category_9,
                tax_functional_category_1,
            )
        )
        dbsession.flush()
        tax_description_1 = models.TaxDescription(
            tax_id=tax_functional_category_1.tax_id,
            language_id=2,
            name="VAT 23% (FUNCTIONAL)",
            description="PODATEK VAT OPIS",
        )
        attribute_functional_category_1 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_category_2 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Magazyn",
        )
        attribute_functional_category_3 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_3.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_category_4 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="10x20",
        )
        attribute_functional_category_5 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_5.attribute_group_id,
            sort_order=1,
            status=1,
            value="PEI III",
        )
        attribute_functional_category_6 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_6.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pierwsza",
        )
        attribute_functional_category_7 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_category_8 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Poler",
        )
        attribute_functional_category_9 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Nie",
        )
        attribute_functional_category_10 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="2-3 dni",
        )
        attribute_functional_category_11 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Niedostępne",
        )
        attribute_functional_category_12 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Zapytaj o dostępność",
        )
        attribute_functional_category_13 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="20x20",
        )
        attribute_functional_category_14 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="20x30",
        )
        attribute_functional_category_15 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="30x30",
        )
        attribute_functional_category_16 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="40x40",
        )
        attribute_functional_category_17 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_4.attribute_group_id,
            sort_order=1,
            status=1,
            value="40x60",
        )
        attribute_functional_category_18 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_5.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pei I",
        )
        attribute_functional_category_19 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_5.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pei II",
        )
        attribute_functional_category_20 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Nie",
        )
        attribute_functional_category_21 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Mat",
        )
        attribute_functional_category_22 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Połysk",
        )
        attribute_functional_category_23 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute_functional_category_24 = models.Attribute(
            attribute_group_id=attribute_group_functional_category_9.attribute_group_id,
            sort_order=1,
            status=1,
            value="Nie",
        )
        discount_functional_category = models.Discount(
            status=1, subtract=1, value=0.00, default=True
        )
        transport_functional_category = models.Transport(
            special_method=True,
            excerpt="Courier - palette with max weight per unit",
            module_name="courier",
            module_settings={"fee": 0, "unit_fee": 160, "max_weight": 1000},
            name="Courier - palette with max weight per unit",
        )
        transport_functional_category_1 = models.Transport(
            special_method=True,
            description="",
            excerpt="",
            module_name="courier",
            module_settings={
                "manipulation_fee": "0",
                "unit_fee": "139",
                "unit_weight": "1000",
            },
            name="Kurier",
            status=True,
        )
        transport_functional_category_2 = models.Transport(
            special_method=False,
            description="",
            excerpt="",
            module_name="courier_bulk",
            module_settings={
                "manipulation_fee": "180",
                "removed_transports": [],
                "unit_fee": "139",
                "unit_weight": "1000",
            },
            name="Kurier gabaryt",
            status=True,
        )
        transport_functional_category_3 = models.Transport(
            special_method=False,
            description="",
            excerpt="",
            module_name="free_delivery",
            module_settings={
                "removed_transports": [],
            },
            name="Darmowy transport",
            status=True,
        )
        manufacturer_functional_category_1 = models.Manufacturer(
            code="",
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fist-manufacturer.jpg",
            manufacturer_type=0,
            name="Other Fist Manufacturer",
            priority=0,
            sort_order=1,
            status=1,
            seo_slug="other-fist-manufacturer",
        )
        manufacturer_functional_category_2 = models.Manufacturer(
            code="",
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/second-manufacturer.jpg",
            manufacturer_type=0,
            name="Opoczno",
            priority=0,
            sort_order=1,
            status=1,
            seo_slug="other-fist-manufacturer",
        )
        manufacturer_functional_category_3 = models.Manufacturer(
            code="",
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/third-manufacturer.jpg",
            manufacturer_type=0,
            name="Paradyż",
            priority=0,
            sort_order=1,
            status=1,
            seo_slug="other-fist-manufacturer",
        )
        manufacturer_functional_category_4 = models.Manufacturer(
            code="",
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fourth-manufacturer.jpg",
            manufacturer_type=0,
            name="Cicogres",
            priority=0,
            sort_order=1,
            status=1,
            seo_slug="other-fist-manufacturer",
        )
        category_functional_category_1 = models.Category(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/first_category.jpg",
            name="Płytki",
            parent_id=None,
            placeholder=False,
            sort_order=1,
            status=1,
        )
        category_functional_category_2 = models.Category(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/second_category.jpg",
            name="Bestsellery",
            parent_id=None,
            placeholder=False,
            sort_order=1,
            status=1,
        )
        category_functional_category_4 = models.Category(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/second_category.jpg",
            name="Empty category",
            parent_id=None,
            placeholder=False,
            sort_order=1,
            status=1,
        )
        dbsession.add_all(
            (
                tax_description_1,
                attribute_functional_category_1,
                attribute_functional_category_2,
                attribute_functional_category_3,
                attribute_functional_category_4,
                attribute_functional_category_5,
                attribute_functional_category_6,
                attribute_functional_category_7,
                attribute_functional_category_8,
                attribute_functional_category_9,
                attribute_functional_category_10,
                attribute_functional_category_11,
                attribute_functional_category_12,
                attribute_functional_category_13,
                attribute_functional_category_14,
                attribute_functional_category_15,
                attribute_functional_category_16,
                attribute_functional_category_17,
                attribute_functional_category_18,
                attribute_functional_category_19,
                attribute_functional_category_20,
                attribute_functional_category_21,
                attribute_functional_category_22,
                attribute_functional_category_23,
                attribute_functional_category_24,
                discount_functional_category,
                manufacturer_functional_category_1,
                manufacturer_functional_category_2,
                manufacturer_functional_category_3,
                manufacturer_functional_category_4,
                transport_functional_category,
                transport_functional_category_1,
                transport_functional_category_2,
                transport_functional_category_3,
                category_functional_category_1,
                category_functional_category_2,
                category_functional_category_4,
            )
        )
        dbsession.flush()
        category_functional_category_3 = models.Category(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/third-manufacturer.jpg",
            name="Drewnopodobne",
            parent_id=category_functional_category_1.category_id,
            placeholder=False,
            sort_order=1,
            status=1,
        )
        product_functional_category_1 = models.Product(
            # allowed_free_transport=0,
            # allowed_free_transport_minimum_amount=2,
            availability_id=attribute_functional_category_2.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_category.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="folder/testing-product-model.jpg",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_category_1.manufacturer_id,
            minimum=1.01,
            model="Functional category testing product model",
            mpn=2,
            name="Functional category testing first product name",
            one_batch=True,
            pieces=1,
            points=0,
            price=20.55,
            quantity=1,
            seo_slug="testing-functional-category-first-product-name",
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_category_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=28.00,
            weight_class_id=1,
            width=1,
        )
        product_functional_category_2 = models.Product(
            ##allowed_free_transport=0,
            # allowed_free_transport_minimum_amount=2,
            availability_id=attribute_functional_category_10.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_category.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="folder/testing-product-model.jpg",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_category_2.manufacturer_id,
            minimum=1.01,
            model="Functional category testing second product model",
            mpn=2,
            name="Functional category testing second product name",
            one_batch=True,
            pieces=1,
            points=0,
            price=30.55,
            quantity=1,
            seo_slug="testing-functional-category-second-product-name",
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_category_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=1.00,
            weight_class_id=1,
            width=1,
        )
        product_functional_category_3 = models.Product(
            # allowed_free_transport=0,
            # allowed_free_transport_minimum_amount=2,
            availability_id=attribute_functional_category_11.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_category.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="folder/testing-third-product-model.jpg",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_category_3.manufacturer_id,
            minimum=1.01,
            model="Functional category testing third product model",
            mpn=2,
            name="Functional category testing third product name",
            one_batch=True,
            pieces=1,
            points=0,
            price=40.55,
            quantity=1,
            seo_slug="testing-functional-category-third-product-name",
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_category_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=1.00,
            weight_class_id=1,
            width=1,
        )
        product_functional_category_4 = models.Product(
            # allowed_free_transport=0,
            # allowed_free_transport_minimum_amount=2,
            availability_id=attribute_functional_category_12.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount_functional_category.discount_id,
            ean="",
            # forbidden_transport_methods=[1, 2],
            height=1.00,
            image="folder/testing-fourth-product-model.jpg",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer_functional_category_4.manufacturer_id,
            minimum=1.01,
            model="Functional category testing fourth product model",
            mpn=2,
            name="Functional category testing fourth product name",
            one_batch=True,
            pieces=1,
            points=0,
            price=50.55,
            quantity=1,
            seo_slug="testing-functional-category-fourth-product-name",
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_functional_category_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=1.00,
            weight_class_id=1,
            width=1,
        )
        dbsession.add_all(
            [
                product_functional_category_1,
                product_functional_category_2,
                product_functional_category_3,
                product_functional_category_4,
                category_functional_category_3,
            ]
        )
        dbsession.flush()
        category_description_functional_category_1 = models.CategoryDescription(
            category_id=category_functional_category_1.category_id,
            description="Category 1 description",
            description_long="Category 1 description lonk",
            language_id=2,
            meta_description="Category 1 meta description",
            seo_slug="first_category_slug",
            u_h1="Category 1 H1 ",
            u_title="Category 1 Title",
        )
        category_description_functional_category_2 = models.CategoryDescription(
            category_id=category_functional_category_1.category_id,
            description="Category 2 description",
            description_long="Category 2 description lonk",
            language_id=2,
            meta_description="Category 2 meta description",
            seo_slug="first_category_slug",
            u_h1="Category 2 H1 ",
            u_title="Category 2 Title",
        )
        category_description_functional_category_3 = models.CategoryDescription(
            category_id=category_functional_category_1.category_id,
            description="Category 3 description",
            description_long="Category 3 description lonk",
            language_id=2,
            meta_description="Category 3 meta description",
            seo_slug="first_category_slug",
            u_h1="Category 3 H1 ",
            u_title="Category 3 Title",
        )
        category_description_functional_category_4 = models.CategoryDescription(
            category_id=category_functional_category_4.category_id,
            description="Category 4 description",
            description_long="Category 4 description lonk",
            language_id=2,
            meta_description="Category 4 meta description",
            seo_slug="first_category_slug",
            u_h1="Category 3 H1 ",
            u_title="Category 3 Title",
        )
        product_description_functional_category_1 = models.ProductDescription(
            description="This is a product description",
            language_id=2,
            meta_description="this is a meta_description",
            name="This is THE OTHER name",
            product_id=product_functional_category_1.product_id,
            tag="This is a tag",
            u_h1="This is a h1 headline",
            u_title="This is a u_title",
            description_technical="This is a techical description",
            excerpt="This is an excerpt",
        )
        product_description_functional_category_2 = models.ProductDescription(
            description="This is a second product description",
            language_id=2,
            meta_description="this is a second meta_description",
            name="This is THE second OTHER name",
            product_id=product_functional_category_2.product_id,
            tag="This is sedond a tag",
            u_h1="This is a second h1 headline",
            u_title="This is a second u_title",
            description_technical="This is a techical description",
            excerpt="This is an excerpt",
        )
        product_description_functional_category_3 = models.ProductDescription(
            description="This is a third product description",
            language_id=2,
            meta_description="this is a third meta_description",
            name="This is THE third OTHER name",
            product_id=product_functional_category_3.product_id,
            tag="This is a third tag",
            u_h1="This is a third h1 headline",
            u_title="This is a third u_title",
            description_technical="This is a techical description",
            excerpt="This is an excerpt",
        )
        product_description_functional_category_4 = models.ProductDescription(
            description="This is a fourth product description",
            language_id=2,
            meta_description="this is a fourth meta_description",
            name="This is THE fourth OTHER name",
            product_id=product_functional_category_4.product_id,
            tag="This is a fourth tag",
            u_h1="This is a fourth h1 headline",
            u_title="This is a fourth u_title",
            description_technical="This is a techical description",
            excerpt="This is an excerpt",
        )
        collection_functional_category_1 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/first-collection.jpg",
            manufacturer_id=manufacturer_functional_category_1.manufacturer_id,
            name="First collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_category_2 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/second-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_category_1.manufacturer_id,
            name="Second collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_category_3 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/third-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_category_2.manufacturer_id,
            name="Third collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_category_4 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fourth-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_category_3.manufacturer_id,
            name="Fourth collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        collection_functional_category_5 = models.Collection(
            date_added="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fifth-manufacturer.jpg",
            manufacturer_id=manufacturer_functional_category_2.manufacturer_id,
            name="Fifth collection",
            priority=0,
            sort_order=0,
            status=1,
        )
        dbsession.add_all(
            [
                category_description_functional_category_1,
                category_description_functional_category_2,
                category_description_functional_category_3,
                category_description_functional_category_4,
                product_description_functional_category_1,
                product_description_functional_category_2,
                product_description_functional_category_3,
                product_description_functional_category_4,
                collection_functional_category_1,
                collection_functional_category_2,
                collection_functional_category_3,
                collection_functional_category_4,
                collection_functional_category_5,
            ]
        )
        dbsession.flush()
        product_attribute_functional_category_1 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_1.attribute_id,
            product_id=product_functional_category_1.product_id,
        )
        product_attribute_functional_category_2 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_2.attribute_id,
            product_id=product_functional_category_1.product_id,
        )
        product_attribute_functional_category_3 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_3.attribute_id,
            product_id=product_functional_category_1.product_id,
        )
        product_attribute_functional_category_4 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_4.attribute_id,
            product_id=product_functional_category_1.product_id,
        )
        product_attribute_functional_category_5 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_5.attribute_id,
            product_id=product_functional_category_1.product_id,
        )
        product_attribute_functional_category_6 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_6.attribute_id,
            product_id=product_functional_category_1.product_id,
        )
        product_attribute_functional_category_7 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_7.attribute_id,
            product_id=product_functional_category_1.product_id,
        )
        product_attribute_functional_category_8 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_8.attribute_id,
            product_id=product_functional_category_1.product_id,
        )
        product_attribute_functional_category_9 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_9.attribute_id,
            product_id=product_functional_category_2.product_id,
        )
        product_attribute_functional_category_10 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_13.attribute_id,
            product_id=product_functional_category_2.product_id,
        )
        product_attribute_functional_category_11 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_18.attribute_id,
            product_id=product_functional_category_2.product_id,
        )
        product_attribute_functional_category_12 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_20.attribute_id,
            product_id=product_functional_category_2.product_id,
        )
        product_attribute_functional_category_13 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_21.attribute_id,
            product_id=product_functional_category_2.product_id,
        )
        product_attribute_functional_category_14 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_23.attribute_id,
            product_id=product_functional_category_2.product_id,
        )
        product_attribute_functional_category_15 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_24.attribute_id,
            product_id=product_functional_category_2.product_id,
        )
        product_attribute_functional_category_16 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_10.attribute_id,
            product_id=product_functional_category_3.product_id,
        )
        product_attribute_functional_category_17 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_13.attribute_id,
            product_id=product_functional_category_3.product_id,
        )
        product_attribute_functional_category_18 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_18.attribute_id,
            product_id=product_functional_category_3.product_id,
        )
        product_attribute_functional_category_19 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_20.attribute_id,
            product_id=product_functional_category_3.product_id,
        )
        product_attribute_functional_category_20 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_21.attribute_id,
            product_id=product_functional_category_3.product_id,
        )
        product_attribute_functional_category_21 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_23.attribute_id,
            product_id=product_functional_category_3.product_id,
        )
        product_attribute_functional_category_22 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_24.attribute_id,
            product_id=product_functional_category_3.product_id,
        )
        product_attribute_functional_category_23 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_11.attribute_id,
            product_id=product_functional_category_4.product_id,
        )
        product_attribute_functional_category_24 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_15.attribute_id,
            product_id=product_functional_category_4.product_id,
        )
        product_attribute_functional_category_25 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_19.attribute_id,
            product_id=product_functional_category_4.product_id,
        )
        product_attribute_functional_category_25 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_20.attribute_id,
            product_id=product_functional_category_4.product_id,
        )
        product_attribute_functional_category_26 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_22.attribute_id,
            product_id=product_functional_category_4.product_id,
        )
        product_attribute_functional_category_27 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_23.attribute_id,
            product_id=product_functional_category_4.product_id,
        )
        product_attribute_functional_category_28 = models.ProductToAttribute(
            attribute_id=attribute_functional_category_24.attribute_id,
            product_id=product_functional_category_4.product_id,
        )
        # Collections
        product_collection_functional_category_1 = models.ProductToCollection(
            collection_id=collection_functional_category_1.collection_id,
            product_id=product_functional_category_1.product_id,
        )
        product_collection_functional_category_2 = models.ProductToCollection(
            collection_id=collection_functional_category_2.collection_id,
            product_id=product_functional_category_1.product_id,
        )
        product_collection_functional_category_3 = models.ProductToCollection(
            collection_id=collection_functional_category_3.collection_id,
            product_id=product_functional_category_1.product_id,
        )
        product_collection_functional_category_4 = models.ProductToCollection(
            collection_id=collection_functional_category_2.collection_id,
            product_id=product_functional_category_2.product_id,
        )
        product_collection_functional_category_5 = models.ProductToCollection(
            collection_id=collection_functional_category_3.collection_id,
            product_id=product_functional_category_2.product_id,
        )
        product_collection_functional_category_6 = models.ProductToCollection(
            collection_id=collection_functional_category_4.collection_id,
            product_id=product_functional_category_2.product_id,
        )
        product_collection_functional_category_7 = models.ProductToCollection(
            collection_id=collection_functional_category_1.collection_id,
            product_id=product_functional_category_3.product_id,
        )
        product_collection_functional_category_8 = models.ProductToCollection(
            collection_id=collection_functional_category_2.collection_id,
            product_id=product_functional_category_3.product_id,
        )
        product_collection_functional_category_9 = models.ProductToCollection(
            collection_id=collection_functional_category_4.collection_id,
            product_id=product_functional_category_3.product_id,
        )
        product_collection_functional_category_10 = models.ProductToCollection(
            collection_id=collection_functional_category_1.collection_id,
            product_id=product_functional_category_4.product_id,
        )
        product_collection_functional_category_11 = models.ProductToCollection(
            collection_id=collection_functional_category_2.collection_id,
            product_id=product_functional_category_4.product_id,
        )
        product_collection_functional_category_12 = models.ProductToCollection(
            collection_id=collection_functional_category_4.collection_id,
            product_id=product_functional_category_4.product_id,
        )
        # Category
        product_category_functional_category_1 = models.ProductToCategory(
            category_id=category_functional_category_1.category_id,
            product_id=product_functional_category_1.product_id,
        )
        product_category_functional_category_2 = models.ProductToCategory(
            category_id=category_functional_category_2.category_id,
            product_id=product_functional_category_1.product_id,
        )
        product_category_functional_category_3 = models.ProductToCategory(
            category_id=category_functional_category_3.category_id,
            product_id=product_functional_category_2.product_id,
        )
        product_category_functional_category_4 = models.ProductToCategory(
            category_id=category_functional_category_2.category_id,
            product_id=product_functional_category_2.product_id,
        )
        product_category_functional_category_5 = models.ProductToCategory(
            category_id=category_functional_category_3.category_id,
            product_id=product_functional_category_3.product_id,
        )
        product_transport_functional_product_1 = models.ProductToTransport(
            product_id=product_functional_category_1.product_id,
            transport_id=transport_functional_category_2.transport_id,
            local_settings={},
        )
        product_transport_functional_product_2 = models.ProductToTransport(
            product_id=product_functional_category_2.product_id,
            transport_id=transport_functional_category_3.transport_id,
            local_settings={"minimum_requirement": 20},
        )
        dbsession.add_all(
            [
                product_attribute_functional_category_1,
                product_attribute_functional_category_2,
                product_attribute_functional_category_3,
                product_attribute_functional_category_4,
                product_attribute_functional_category_5,
                product_attribute_functional_category_6,
                product_attribute_functional_category_7,
                product_attribute_functional_category_8,
                product_attribute_functional_category_9,
                product_attribute_functional_category_10,
                product_attribute_functional_category_11,
                product_attribute_functional_category_12,
                product_attribute_functional_category_13,
                product_attribute_functional_category_14,
                product_attribute_functional_category_15,
                product_attribute_functional_category_16,
                product_attribute_functional_category_17,
                product_attribute_functional_category_18,
                product_attribute_functional_category_19,
                product_attribute_functional_category_20,
                product_attribute_functional_category_21,
                product_attribute_functional_category_22,
                product_attribute_functional_category_23,
                product_attribute_functional_category_24,
                product_attribute_functional_category_25,
                product_attribute_functional_category_26,
                product_attribute_functional_category_27,
                product_attribute_functional_category_28,
                product_collection_functional_category_1,
                product_collection_functional_category_2,
                product_collection_functional_category_3,
                product_collection_functional_category_4,
                product_collection_functional_category_5,
                product_collection_functional_category_6,
                product_collection_functional_category_7,
                product_collection_functional_category_8,
                product_collection_functional_category_9,
                product_collection_functional_category_10,
                product_collection_functional_category_11,
                product_collection_functional_category_12,
                product_category_functional_category_1,
                product_category_functional_category_2,
                product_category_functional_category_3,
                product_category_functional_category_4,
                product_category_functional_category_5,
                product_transport_functional_product_1,
                product_transport_functional_product_2,
            ]
        )
        dbsession.flush()
        helpers.CacheHelpers.flush_cache()
        global product_id_1
        product_id_1 = product_functional_category_1.product_id
        global product_name_1
        product_name_1 = product_functional_category_1.name
        global product_slug_1
        product_slug_1 = product_functional_category_1.seo_slug
        # Manufacturer
        global manufacturer_id_1
        manufacturer_id_1 = manufacturer_functional_category_1.manufacturer_id
        global manufacturer_id_2
        manufacturer_id_2 = manufacturer_functional_category_2.manufacturer_id
        global manufacturer_id_3
        manufacturer_id_3 = manufacturer_functional_category_3.manufacturer_id
        global manufacturer_id_4
        manufacturer_id_4 = manufacturer_functional_category_4.manufacturer_id
        # Category
        global category_id_1
        category_id_1 = category_functional_category_1.category_id
        global category_slug_1
        category_slug_1 = category_description_functional_category_1.seo_slug
        global category_id_2
        category_id_2 = category_functional_category_2.category_id
        global category_slug_2
        category_slug_2 = category_description_functional_category_2.seo_slug
        global category_id_3
        category_id_3 = category_functional_category_3.category_id
        global category_slug_3
        category_slug_3 = category_description_functional_category_3.seo_slug
        global category_id_4
        category_id_4 = category_functional_category_4.category_id
        global category_slug_4
        category_slug_4 = category_description_functional_category_4.seo_slug
        # Collection
        global collection_id_1
        collection_id_1 = collection_functional_category_1.collection_id
        global collection_id_2
        collection_id_2 = collection_functional_category_2.collection_id
        global collection_id_3
        collection_id_3 = collection_functional_category_3.collection_id
        global collection_id_4
        collection_id_4 = collection_functional_category_4.collection_id
        global collection_id_5
        collection_id_5 = collection_functional_category_5.collection_id
        # Attribute
        global attribute_id_1
        attribute_id_1 = attribute_functional_category_1.attribute_id
        global attribute_id_3
        attribute_id_3 = attribute_functional_category_3.attribute_id
        global attribute_id_4
        attribute_id_4 = attribute_functional_category_4.attribute_id
        global attribute_id_5
        attribute_id_5 = attribute_functional_category_5.attribute_id
        global attribute_id_6
        attribute_id_6 = attribute_functional_category_6.attribute_id
        global attribute_id_7
        attribute_id_7 = attribute_functional_category_7.attribute_id
        global attribute_id_8
        attribute_id_8 = attribute_functional_category_8.attribute_id
        global attribute_id_9
        attribute_id_9 = attribute_functional_category_9.attribute_id
        global attribute_id_10
        attribute_id_10 = attribute_functional_category_10.attribute_id


# def test_root( testapp):
#     res = testapp.get("/", status=200)
#     assert res.location == "http://example.com"


def test_display_category(testapp):
    res = testapp.get(
        "/category/" + category_slug_1 + "-" + str(category_id_1), status=200
    )
    assert b"Functional category testing first product name" in res.body
    assert b"Functional category testing second product Name" not in res.body
    assert b"Functional category testing third product Name" not in res.body
    assert b"Functional category testing fourth product Name" not in res.body


def test_display_category_filter_term(testapp):
    res = testapp.get(
        "/category/" + category_slug_2 + "-" + str(category_id_2) + "?term=second",
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_term_no_results(testapp):
    res = testapp.get(
        "/category/" + category_slug_1 + "-" + str(category_id_1) + "?term=second",
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_manufacturer(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_3
        + "-"
        + str(category_id_3)
        + "?manufacturer="
        + str(manufacturer_id_2),
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_manufacturer_no_results(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_1
        + "-"
        + str(category_id_1)
        + "?manufacturer="
        + str(manufacturer_id_2),
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_collection(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_1
        + "-"
        + str(category_id_1)
        + "?collections="
        + str(collection_id_2),
        status=200,
    )
    assert b"Functional category testing first product name" in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_collection_no_results(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_3
        + "-"
        + str(category_id_3)
        + "?collections="
        + str(collection_id_5),
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_price_max(testapp):
    res = testapp.get(
        "/category/" + category_slug_2 + "-" + str(category_id_2) + "?price_max=38",
        status=200,
    )
    assert b"Functional category testing first product name" in res.body
    assert b"Functional category testing second product name" in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_price_max_no_results(testapp):
    res = testapp.get(
        "/category/" + category_slug_4 + "-" + str(category_id_4) + "?price_max=38",
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_price_min(testapp):
    res = testapp.get(
        "/category/" + category_slug_3 + "-" + str(category_id_3) + "?price_min=38",
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_price_min_no_results(testapp):
    res = testapp.get(
        "/category/" + category_slug_2 + "-" + str(category_id_2) + "?price_min=38",
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_price_min_max(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_1
        + "-"
        + str(category_id_1)
        + "?price_min=18&price_max=28",
        status=200,
    )
    assert b"Functional category testing first product name" in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_price_min_max_no_results(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_2
        + "-"
        + str(category_id_2)
        + "?price_min=38&price_max=60",
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_collection_price_min_max(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_1
        + "-"
        + str(category_id_1)
        + "?collections="
        + str(collection_id_2)
        + "&price_min=18&price_max=28",
        status=200,
    )
    assert b"Functional category testing first product name" in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_collection_price_min_max_no_results(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_2
        + "-"
        + str(category_id_2)
        + "?collections="
        + str(collection_id_2)
        + "&price_min=38&price_max=60",
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_manufacturer_price_min_max(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_3
        + "-"
        + str(category_id_3)
        + "?manufacturer="
        + str(manufacturer_id_2)
        + "&price_min=18&price_max=58",
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_manufacturer_price_min_max_no_results(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_1
        + "-"
        + str(category_id_1)
        + "?manufacturer="
        + str(manufacturer_id_2)
        + "&price_min=38&price_max=60",
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_collection_manufacturer_price_min_max(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_1
        + "-"
        + str(category_id_1)
        + "?manufacturer="
        + str(manufacturer_id_1)
        + "?collections="
        + str(collection_id_2)
        + "&price_min=18&price_max=58",
        status=200,
    )
    assert b"Functional category testing first product name" in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_collection_manufacturer_price_min_max_no_results(
    testapp,
):
    res = testapp.get(
        "/category/"
        + category_slug_1
        + "-"
        + str(category_id_1)
        + "?manufacturer="
        + str(manufacturer_id_2)
        + "?collections="
        + str(collection_id_1)
        + "&price_min=38&price_max=60",
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_attribute(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_1
        + "-"
        + str(category_id_1)
        + "?attribute-"
        + str(attribute_id_1),
        status=200,
    )
    assert b"Functional category testing first product name" in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_category_filter_attribute_no_results(testapp):
    res = testapp.get(
        "/category/"
        + category_slug_4
        + "-"
        + str(category_id_4)
        + "?attribute-"
        + str(attribute_id_3),
        status=200,
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def text_display_category_filter_searchterm(testapp):
    res = testapp.get(
        "/category/" + category_slug_2 + "-" + str(category_id_2) + "?term=second"
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def text_display_category_filter_searchterm_no_results(testapp):
    res = testapp.get(
        "/category/" + category_slug_2 + "-" + str(category_id_2) + "?term=techno"
    )
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def text_display_search_filter_searchterm(testapp):
    res = testapp.get("/search/?term=second")
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def text_display_search_filter_searchterm_no_results(testapp):
    res = testapp.get("/search/?term=techno")
    assert b"Functional category testing first product name" not in res.body
    assert b"Functional category testing second product name" not in res.body
    assert b"Functional category testing third product name" not in res.body
    assert b"Functional category testing fourth product name" not in res.body


def test_display_all_products_category(testapp):
    res = testapp.get(
        "/search/",
        status=200,
    )
    assert b"Functional category testing first product name" in res.body
    assert b"Functional category testing second product name" in res.body
    assert b"Functional category testing third product name" in res.body
    assert b"Functional category testing fourth product name" in res.body
