import pytest
import transaction

from emporium import models
from tests import functional

basic_login = {"login": "basic", "password": "basic", "role": "customer"}
editor_login = {"login": "editor", "password": "editor", "role": "emperor"}


@pytest.fixture(scope="session", autouse=True)
def dummy_data(app):
    """
    Add some dummy data to the database.

    Note that this is a session fixture that commits data to the database.
    Think about it similarly to running the ``initialize_db`` script at the
    start of the test suite.

    This data should not conflict with any other data added throughout the
    test suite or there will be issues - so be careful with this pattern!

    """
    tm = transaction.TransactionManager(explicit=True)
    with tm:
        dbsession = models.get_tm_session(app.registry["dbsession_factory"], tm)
        functional.clearDatabase(dbsession)

        setting1 = models.Setting(key="cache", value=True, setting_type=0)
        setting2 = models.Setting(key="shop_status", value=True, setting_type=0)
        setting3 = models.Setting(
            key="name", value="Emporatorium test name", setting_type=0
        )
        editor = models.User(
            first_name="Jack", last_name="Black", role="emperor", login="editor"
        )
        editor.set_password("editor")
        basic = models.User(
            first_name="Jack", last_name="Black", role="customer", login="basic"
        )
        basic.set_password("basic")
        dbsession.add_all([basic, editor, setting1, setting2, setting3])
        dbsession.flush()
        post1 = models.Post(
            title="First post",
            content="This is the front post",
            status=1,
            seo_slug="first-post",
            creator_id=editor.id,
        )
        post1.creator_id = editor.id
        post2 = models.Post(
            title="BackPost",
            content="This is the back post",
            status=1,
            creator_id=editor.id,
        )
        post2.creator_id = basic.id
        dbsession.add_all([post1, post2])
        dbsession.flush()
        global post_id_1
        post_id_1 = post1.post_id
        global post_id_2
        post_id_2 = post2.post_id


# def test_root(testapp):
#     res = testapp.get("/", status=200)
#     assert res.location == "http://example.com"


def test_blog_post_list(testapp):
    res = testapp.get("/blog/", status=200)
    assert b"First post" in res.body


def test_display_selected_post(testapp):
    res = testapp.get("/blog/first-post-" + str(post_id_1), status=200)
    assert b"First post" in res.body


def test_missing_post(testapp):
    res = testapp.get("/blog/somepost-997766", status=301)
    assert res.location == "http://example.com/"


def test_anonymous_user_cannot_edit(testapp):
    res = testapp.get(
        "/emporatorium/post_edit?post_id=" + str(post_id_1), status=303
    ).follow()
    assert b"Login" in res.body


def test_anonymous_user_cannot_add(testapp):
    res = testapp.get("/emporatorium/post_create", status=303).follow()
    assert b"Login" in res.body


def test_customer_cannot_edit(testapp):
    testapp.login(basic_login)
    res = testapp.get(
        "/emporatorium/post_edit?post_id=" + str(post_id_1), status=303
    ).follow()
    assert b"Login" in res.body


def test_customer_can_add(testapp):
    testapp.login(basic_login)
    res = testapp.get("/emporatorium/post_create", status=303).follow()
    assert b"Login" in res.body


def test_emperors_member_user_can_edit(testapp):
    testapp.login(editor_login)
    res = testapp.get("/emporatorium/post_edit?post_id=" + str(post_id_1), status=200)
    assert b"post_edit_header" in res.body


def test_emperors_member_user_can_add(testapp):
    testapp.login(editor_login)
    res = testapp.get("/emporatorium/post_create", status=200)
    assert b"post_create_header" in res.body


def test_emperors_member_user_can_view(testapp):
    testapp.login(editor_login)
    res = testapp.get("/blog/first-post-" + str(post_id_1), status=200)
    assert b"First post" in res.body


def test_redirect_to_edit_for_existing_post(testapp):
    testapp.login(editor_login)
    res = testapp.get("/emporatorium/post_edit?post_id=" + str(post_id_1), status=200)
    assert b"post_edit_header" in res.body
    assert b"First post" in res.body
