from pyramid.testing import DummySecurityPolicy
from webob.multidict import MultiDict

from emporium import models


def makeUser(first_name, last_name, login, password, role):
    user = models.User(
        first_name=first_name, last_name=last_name, login=login, role=role
    )
    user.set_password(password)
    return user


def setUser(config, user):
    config.set_security_policy(DummySecurityPolicy(identity=user))


def makePost(content, status, title, creator_id, seo_slug):
    return models.Post(
        content=content,
        status=status,
        title=title,
        creator_id=creator_id,
        seo_slug=seo_slug,
    )


def makeSetting(key, value, setting_type):
    return models.Setting(key=key, value=value, setting_type=setting_type)


class Test_post_view:
    def _callFUT(self, request, postname, post_id):
        from emporium.views.post import post_view

        request.matchdict["postname"] = postname
        request.matchdict["id"] = post_id
        request.referer = "/"
        return post_view(request)

    def _makeContext(self, post):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route("s_post", "/blog/{postname}-{id}")

    def test_it(self, dummy_config, dummy_request, dbsession):
        # add a post to the db
        setting_1 = makeSetting("cache", True, 0)
        setting_2 = makeSetting("shop_status", True, 0)
        setting_3 = makeSetting("name", "Emporatorium test name", 0)
        user = makeUser("Jack", "Black", "editor", "password", "emperor")
        dbsession.add_all([setting_1, setting_2, setting_3, user])
        dbsession.flush()
        post = makePost(
            "Cruel Blog Post Content",
            1,
            "Cruel Blog Title Post",
            user.id,
            "cruel-blog-title-post",
        )
        dbsession.add_all([post])
        dbsession.flush()
        # create a request asking for the post we've created
        self._addRoutes(dummy_config)
        dummy_request.context = self._makeContext(post)
        setUser(dummy_config, user)
        # call the view we're testing and check its behavior
        info = self._callFUT(dummy_request, "cruel-blog-title-post", post.post_id)
        assert info["post"] is post
        assert info["post"].content == "Cruel Blog Post Content"
        assert info["post"].title == "Cruel Blog Title Post"
        assert info["post"].creator_id == user.id
        assert info["post"].status == True


class Test_add_post:
    def _callFUT(self, request, post_id=None):
        from emporium.views.post import post_edit

        request.matchdict["action"] = "create"
        request.params["post_id"] = post_id
        request.referer = "/emporatorium/post_list"
        return post_edit(request)

    def _makeContext(self):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route("post_action", "/emporatorium/post_{action}")

    def test_submit_works(self, dummy_config, dummy_request, dbsession):
        user = makeUser("Jack", "Black", "editor", "password", "emperor")
        setting_1 = makeSetting("cache", True, 0)
        setting_2 = makeSetting("shop_status", True, 0)
        setting_3 = makeSetting("name", "Emporatorium test name", 0)
        dbsession.add_all([setting_1, setting_2, setting_3, user])
        dbsession.flush()
        dummy_request.method = "POST"
        dummy_request.POST = MultiDict(
            {
                "form.submitted": True,
                "content": "CruelWorld Exists",
                "status": 1,
                "title": "AnotherPost",
            }
        )
        dummy_request.context = self._makeContext()
        setUser(dummy_config, user)
        dummy_config.testing_securitypolicy(userid=dummy_request.identity.id)
        self._addRoutes(dummy_config)
        self._callFUT(dummy_request, None)
        post = dbsession.query(models.Post).filter_by(title="AnotherPost").one()
        assert post.content == "CruelWorld Exists"


class Test_post_edit:
    def _callFUT(self, request, action, post_id):
        from emporium.views.post import post_edit

        request.matchdict["action"] = action
        request.params["post_id"] = post_id
        request.referer = "/emporatorium/post_list"
        return post_edit(request)

    def _makeContext(self):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route("post_action", "/emporatorium/post_{action}")
        # config.add_route("s_post", "/blog/{postname}-{id}")

    def test_add(self, dummy_config, dummy_request, dbsession):
        setting_1 = makeSetting("cache", True, 0)
        setting_2 = makeSetting("shop_status", True, 0)
        setting_3 = makeSetting("name", "Emporatorium test name", 0)
        user = makeUser("Jack", "Black", "editor", "password", "emperor")
        dbsession.add_all([setting_1, setting_2, setting_3, user])
        dbsession.flush()
        dummy_request.context = self._makeContext()
        dummy_request.method = "POST"
        dummy_request.POST = MultiDict(
            {
                "form.submitted": True,
                "content": "Hello CruelWorld IDoExist",
                "title": "IDoExist(sortof)",
                "status": 1,
            }
        )
        setUser(dummy_config, user)
        dummy_config.testing_securitypolicy(userid=dummy_request.identity.id)
        self._addRoutes(dummy_config)
        self._callFUT(dummy_request, "create", None)
        post = dbsession.query(models.Post).filter_by(title="IDoExist(sortof)").one()
        assert post.content == "Hello CruelWorld IDoExist"

    def test_edit(self, dummy_config, dummy_request, dbsession):

        user = makeUser("Jack", "Black", "editor", "password", "emperor")
        setting_1 = makeSetting("cache", True, 0)
        setting_2 = makeSetting("shop_status", True, 0)
        setting_3 = makeSetting("name", "Emporatorium test name", 0)
        dbsession.add_all([setting_1, setting_2, setting_3, user])
        dbsession.flush()
        post = makePost("Hello CruelWorld IDoExist", 1, "IDoExist", user.id, "idoexist")
        dbsession.add_all([post])
        dbsession.flush()
        self._addRoutes(dummy_config)
        dummy_request.method = "POST"
        dummy_request.POST = MultiDict(
            {
                "form.submitted": True,
                "content": "Hello yo!",
                "post_id": post.post_id,
                "title": "Totally new title it is",
                "status": post.status,
                "creator_id": post.creator_id,
            }
        )
        setUser(dummy_config, user)
        dummy_request.context = self._makeContext()
        response = self._callFUT(dummy_request, "edit", post.post_id)
        assert response.location == "post_list"
        post = (
            dbsession.query(models.Post)
            .filter_by(title="Totally new title it is")
            .one()
        )
        assert post.content == "Hello yo!"
