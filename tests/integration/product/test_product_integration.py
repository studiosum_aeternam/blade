from datetime import datetime

from pyramid.testing import DummySecurityPolicy
from webob.multidict import MultiDict, NestedMultiDict

from emporium import helpers
from emporium import models
from emporium import services
from emporium import data_containers


from tests import integration


class Test_product_view:
    def _callFUT(self, request, seoslug, product_id):
        from emporium.views.product import product_view

        request.matchdict["seo_slug"] = seoslug
        request.matchdict["id"] = product_id
        request.referer = "/emporatorium/product_list"
        return product_view(request)

    def _makeContext(self, product):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route("product_edit", "/emporatorium/product_{action}")
        config.add_route("s_product", "/{seoslug}-{product_id}")
        # Search declaration is required by the products' meta_options
        config.add_route("s_search", "/search/")

    def test_it(self, dummy_config, dummy_request, dbsession):
        # add a product to the db
        setting_1 = integration.makeSetting("cache", False, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        setting_4 = integration.makeSetting(
            "image_settings",
            "Image settings",
            None,
            {
                "image_sizes": {"base": [1200, 1200], "thumbnails": [500, 500]},
                "image_extensions": [["JPEG", "jpg", "RGB"], ["WEBP", "webp", "RGBA"]],
            },
        )
        setting_5 = integration.makeSetting(
            "product_settings",
            "Product settings",
            None,
            {"description_technical": 1},
        )
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        tax_13 = integration.makeTax(1, 1, 48.00)
        tax_14 = integration.makeTax(1, 1, 56.00)
        attribute_display_type_1 = integration.makeAttributeDisplayType(
            "checkbox", "Checkbox", 1, 1
        )
        attribute_display_type_2 = integration.makeAttributeDisplayType(
            "radio", "Radio button", 2, 1
        )
        dbsession.add_all(
            [
                setting_1,
                setting_2,
                setting_3,
                setting_4,
                setting_5,
                user,
                tax_13,
                tax_14,
                attribute_display_type_1,
                attribute_display_type_2,
            ]
        )
        dbsession.flush()
        attribute_group1 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Antislip",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group2 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Availability",
            required=True,
            sort_order=3,
            status=1,
        )
        attribute_group3 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Frost",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group4 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Measurements",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group5 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Pei",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group6 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Quality",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group7 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Rekt",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group8 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Surface",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group9 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Tiles",
            required=False,
            sort_order=3,
            status=1,
        )
        tax_description_1 = integration.makeTaxDescription(
            description="Inzkrypszyn",
            language_id=2,
            name="VAT 48%",
            tax_id=tax_13.tax_id,
        )
        tax_description_2 = integration.makeTaxDescription(
            description="Dezkrypszyn",
            language_id=2,
            name="VAT 56%",
            tax_id=tax_14.tax_id,
        )
        dbsession.add_all(
            (
                attribute_group1,
                attribute_group2,
                attribute_group3,
                attribute_group4,
                attribute_group5,
                attribute_group6,
                attribute_group7,
                attribute_group8,
                attribute_group9,
                tax_description_1,
                tax_description_2,
            )
        )
        dbsession.flush()
        attribute1 = integration.makeAttribute(
            attribute_group_id=attribute_group1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute2 = integration.makeAttribute(
            attribute_group_id=attribute_group2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Magazyn",
        )
        attribute3 = integration.makeAttribute(
            attribute_group_id=attribute_group3.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute4 = integration.makeAttribute(
            attribute_group_id=attribute_group4.attribute_group_id,
            sort_order=1,
            status=1,
            value="10x20",
        )
        attribute5 = integration.makeAttribute(
            attribute_group_id=attribute_group5.attribute_group_id,
            sort_order=1,
            status=1,
            value="PEI III",
        )
        attribute6 = integration.makeAttribute(
            attribute_group_id=attribute_group6.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pierwsza",
        )
        attribute7 = integration.makeAttribute(
            attribute_group_id=attribute_group7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute8 = integration.makeAttribute(
            attribute_group_id=attribute_group8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Poler",
        )
        discount = integration.makeDiscount(
            status=1, subtract=1, value=0.00, default=True
        )
        transport = integration.makeTransport(
            special_method=True,
            excerpt="Courier - palette with max weight per unit",
            module_name="courier",
            module_settings={"fee": 0, "unit_fee": 160, "max_weight": 1000},
            name="Courier - palette with max weight per unit",
        )
        dbsession.add_all(
            (
                attribute1,
                attribute2,
                attribute3,
                attribute4,
                attribute5,
                attribute6,
                attribute7,
                attribute8,
                discount,
                transport,
            )
        )
        dbsession.flush()
        manufacturer = integration.makeManufacturer(
            code="",
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fist-manufacturer.jpg",
            manufacturer_type=0,
            name="Fist Manufacturer",
            priority=0,
            seo_slug="fist_manufacturer",
            sort_order=1,
            status=1,
        )
        dbsession.add_all([manufacturer])
        dbsession.flush()
        product = integration.makeProduct(
            # allowed_free_transport=0,
            # allowed_free_transport_minimum_amount==2,
            availability_id=attribute2.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount.discount_id,
            ean="",
            # forbidden_transport_methods==[1, 2],
            height=1.00,
            image="folder/testing-product-model.jpg",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer.manufacturer_id,
            minimum=1.01,
            model="Testing product model",
            mpn=2,
            name="Testing product name",
            one_batch=True,
            pieces=1,
            points=0,
            price=95.55,
            quantity=1,
            seo_slug="testing-product-name",
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_13.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=28.00,
            weight_class_id=1,
            width=1,
        )
        dbsession.add_all([product])
        dbsession.flush()
        product_description = integration.makeProductDescription(
            description="This is a product description",
            description_technical="This is a technical description",
            language_id=2,
            meta_description="this is a meta_description",
            name="This is THE OTHER name",
            product_id=product.product_id,
            tag="This is a tag",
            u_h1="This is a h1 headline",
            u_title="This is a u_title",
        )
        dbsession.add_all((product_description,))
        dbsession.flush()
        product_attribute_1 = integration.makeProductToAttribute(
            attribute_id=attribute1.attribute_id, product_id=product.product_id
        )
        product_attribute_2 = integration.makeProductToAttribute(
            attribute_id=attribute2.attribute_id, product_id=product.product_id
        )
        product_attribute_3 = integration.makeProductToAttribute(
            attribute_id=attribute3.attribute_id, product_id=product.product_id
        )
        product_attribute_4 = integration.makeProductToAttribute(
            attribute_id=attribute4.attribute_id, product_id=product.product_id
        )
        product_attribute_5 = integration.makeProductToAttribute(
            attribute_id=attribute5.attribute_id, product_id=product.product_id
        )
        product_attribute_6 = integration.makeProductToAttribute(
            attribute_id=attribute6.attribute_id, product_id=product.product_id
        )
        product_attribute_7 = integration.makeProductToAttribute(
            attribute_id=attribute7.attribute_id, product_id=product.product_id
        )
        product_attribute_8 = integration.makeProductToAttribute(
            attribute_id=attribute8.attribute_id, product_id=product.product_id
        )
        dbsession.add_all(
            [
                product_attribute_1,
                product_attribute_2,
                product_attribute_3,
                product_attribute_4,
                product_attribute_5,
                product_attribute_6,
                product_attribute_7,
                product_attribute_8,
            ]
        )
        dbsession.flush()
        self._addRoutes(dummy_config)
        dummy_request.context = self._makeContext(product)
        integration.setUser(dummy_config, user)
        # call the view we're testing and check its behavior
        helpers.CacheHelpers.flush_cache()
        info = self._callFUT(dummy_request, product.seo_slug, product.product_id)
        assert info["product"].name == product.name  # .title()
        assert info["product"].model == product.model.title()
        assert info["product"].slug == product.seo_slug
        assert info["product"].availability_id == product.availability_id


class Test_add_product:
    def _callFUT(self, request, product_id=None):
        from emporium.views.product import product_edit

        request.referer = "/emporatorium/product_list"

        return product_edit(request)

    def _makeContext(self):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route("product_edit", "/emporatorium/product_{action}")
        config.add_route("s_product", "/{seoslug}-{product_id}")

    def test_submit_works(self, dummy_config, app_request, dbsession):
        # add a product to the db
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        setting_4 = integration.makeSetting(
            "image_settings",
            "Image settings",
            None,
            {
                "image_sizes": {"base": [1200, 1200], "thumbnails": [500, 500]},
                "image_extensions": [["JPEG", "jpg", "RGB"], ["WEBP", "webp", "RGBA"]],
            },
        )
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        tax_1 = integration.makeTax(1, 1, 23)

        attribute_display_type_1 = integration.makeAttributeDisplayType(
            "checkbox", "Checkbox", 1, 1
        )
        attribute_display_type_2 = integration.makeAttributeDisplayType(
            "radio", "Radio button", 2, 1
        )
        dbsession.add_all(
            [
                setting_1,
                setting_2,
                setting_3,
                setting_4,
                user,
                tax_1,
                attribute_display_type_1,
                attribute_display_type_2,
            ]
        )
        dbsession.flush()
        attribute_group1 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Antislip",
            required=False,
            sort_order=3,
            status=1,
            attribute_name="",
        )
        attribute_group2 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Availability",
            required=True,
            sort_order=3,
            status=1,
            attribute_name="availability",
        )
        attribute_group3 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Frost",
            required=False,
            sort_order=3,
            status=1,
            attribute_name="",
        )
        attribute_group4 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Measurements",
            required=False,
            sort_order=3,
            status=1,
            attribute_name="format_group",
        )
        attribute_group5 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Pei",
            required=False,
            sort_order=3,
            status=1,
            attribute_name="",
        )
        attribute_group6 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Quality",
            required=False,
            sort_order=3,
            status=1,
            attribute_name="quality",
        )
        attribute_group7 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Rekt",
            required=False,
            sort_order=3,
            status=1,
            attribute_name="",
        )
        attribute_group8 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Surface",
            required=False,
            sort_order=3,
            status=1,
            attribute_name="",
        )
        attribute_group9 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Tiles",
            required=False,
            sort_order=3,
            status=1,
            attribute_name="",
        )
        tax_description_1 = integration.makeTaxDescription(
            description="Tax just tax",
            language_id=2,
            name="Tax description uan",
            tax_id=tax_1.tax_id,
        )
        dbsession.add_all(
            (
                attribute_group1,
                attribute_group2,
                attribute_group3,
                attribute_group4,
                attribute_group5,
                attribute_group6,
                attribute_group7,
                attribute_group8,
                attribute_group9,
                tax_description_1,
            )
        )
        dbsession.flush()
        attribute1 = integration.makeAttribute(
            attribute_group_id=attribute_group1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute2 = integration.makeAttribute(
            attribute_group_id=attribute_group2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Magazyn",
        )
        attribute3 = integration.makeAttribute(
            attribute_group_id=attribute_group3.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute4 = integration.makeAttribute(
            attribute_group_id=attribute_group4.attribute_group_id,
            sort_order=1,
            status=1,
            value="10x20",
        )
        attribute5 = integration.makeAttribute(
            attribute_group_id=attribute_group5.attribute_group_id,
            sort_order=1,
            status=1,
            value="PEI III",
        )
        attribute6 = integration.makeAttribute(
            attribute_group_id=attribute_group6.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pierwsza",
        )
        attribute7 = integration.makeAttribute(
            attribute_group_id=attribute_group7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute8 = integration.makeAttribute(
            attribute_group_id=attribute_group8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Poler",
        )
        discount = integration.makeDiscount(
            status=1, subtract=1, value=0.00, default=True
        )
        transport = integration.makeTransport(
            special_method=True,
            excerpt="Courier - palette with max weight per unit",
            module_name="courier",
            module_settings={"fee": 0, "unit_fee": 160, "max_weight": 1000},
            name="Courier - palette with max weight per unit",
        )
        setting_template_1 = models.SettingTemplate(
            description="Setting template description",
            name="Tile template",
            order=1,
            status=1,
            template_json={
                "manufactured": 1,
                "virtual": 0,
                "subtract_quantity": 0,
                "storage_type": "",
                "attribute_type_list": [
                    attribute_group1.attribute_group_id,
                    attribute_group2.attribute_group_id,
                    attribute_group3.attribute_group_id,
                    attribute_group4.attribute_group_id,
                    attribute_group5.attribute_group_id,
                    attribute_group6.attribute_group_id,
                    attribute_group7.attribute_group_id,
                    attribute_group8.attribute_group_id,
                    attribute_group9.attribute_group_id,
                ],
                "unit_label": 2,
            },
            template_type=1,
        )
        dbsession.add_all(
            (
                attribute1,
                attribute2,
                attribute3,
                attribute4,
                attribute5,
                attribute6,
                attribute7,
                attribute8,
                discount,
                setting_template_1,
                transport,
            )
        )
        dbsession.flush()
        manufacturer = integration.makeManufacturer(
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fist-manufacturer.jpg",
            name="Fist Manufacturer",
            priority=0,
            seo_slug="fist_manufacturer",
            sort_order=1,
            status=1,
        )
        dbsession.add_all([manufacturer])
        dbsession.flush()
        app_request.method = "POST"
        app_request.matchdict = dict(action="create")
        product_dict = {
            # "allowed_free_transport": 0,
            # "allowed_free_transport_minimum_amount=": 2,
            "availability_id": attribute2.attribute_id,
            "catalog_price": 136.50,
            "date_added": "2021-01-01",
            "date_available": "1970-01-01",
            "date_modified": "2021-01-01",
            "description": "This is a product description",
            "discount_id": discount.discount_id,
            "ean": "",
            # "forbidden_payment_methods": "1 2",
            # "forbidden_transport_methods=": "1 2",
            "height": 1.00,
            "isbn": 2,
            "jan": 1,
            "language_id": 2,
            "length": 1.00,
            "length_class_id": 1,
            "location": 4,
            "manual_discount": 0,
            "manufactred": 1,
            "manufacturer_id": manufacturer.manufacturer_id,
            "meta_description": "this is a meta_description",
            "minimum": 1.01,
            "model": "Testing create form - product model FRST",
            "name": "Testing create form - product name",
            "one_batch": 1,
            "pieces": 1,
            "points": 0,
            "price": 95.55,
            "product_status": 1,
            "product_type": 0,
            "quantity": 1,
            "seo_slug": "testing-product-name",
            "setting_template_id": setting_template_1.setting_template_id,
            "sku": 5,
            "sort_order": 0,
            "special_type": 1,
            "square_meter": 1,
            "tax_id": tax_1.tax_id,
            "u_h1": "This is a h1 headline",
            "u_title": "This is a u_title",
            "unit": 1.07,
            "upc": 6,
            "viewed": 0,
            "virtual": 0,
            "weight": 28.00,
            "weight_class_id": 1,
            "width": 1,
        }
        stringed_dict = "&".join(str(k) + "=" + str(v) for k, v in product_dict.items())
        attribute_list = (
            attribute1.attribute_id,
            attribute3.attribute_id,
            attribute4.attribute_id,
            attribute5.attribute_id,
            attribute6.attribute_id,
            attribute7.attribute_id,
            attribute8.attribute_id,
        )
        stringed_dict += "&"
        stringed_dict += "&".join(
            "attribute_list=" + str(item) for item in attribute_list
        )
        app_request.body = str.encode(stringed_dict)
        app_request.context = self._makeContext()
        integration.setUser(dummy_config, user)
        dummy_config.testing_securitypolicy(userid=user.id)
        self._addRoutes(dummy_config)
        self._callFUT(app_request, None)
        product = (
            dbsession.query(models.Product)
            .filter_by(model="Testing create form - product model FRST")
            .one()
        )
        assert product.name == "Testing create form - product name"
        product_description = (
            dbsession.query(models.ProductDescription)
            .filter_by(product_id=product.product_id)
            .one()
        )
        original_product = (
            services.ProductService.product_with_description_filter_product_id(
                app_request,
                product.product_id,
                False,
            )
        )
        # TU ZACZNIJ DUBANIE
        assert original_product.name == "Testing create form - product name"
        assert original_product.ean == ""

    def test_submit_works_missing_description(
        self, dummy_config, app_request, dbsession
    ):
        # add a product to the db
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        setting_4 = integration.makeSetting(
            "image_settings",
            "Image settings",
            None,
            {
                "image_sizes": {"base": [1200, 1200], "thumbnails": [500, 500]},
                "image_extensions": [["JPEG", "jpg", "RGB"], ["WEBP", "webp", "RGBA"]],
            },
        )
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        tax_1 = integration.makeTax(1, 1, 23)

        attribute_display_type_1 = integration.makeAttributeDisplayType(
            "checkbox", "Checkbox", 1, 1
        )
        attribute_display_type_2 = integration.makeAttributeDisplayType(
            "radio", "Radio button", 2, 1
        )
        dbsession.add_all(
            [
                setting_1,
                setting_2,
                setting_3,
                setting_4,
                user,
                tax_1,
                attribute_display_type_1,
                attribute_display_type_2,
            ]
        )
        dbsession.flush()
        attribute_group2 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Availability",
            required=True,
            sort_order=3,
            status=1,
        )
        dbsession.add_all((attribute_group2,))
        dbsession.flush()
        attribute2 = integration.makeAttribute(
            attribute_group_id=attribute_group2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Magazyn",
        )
        discount = integration.makeDiscount(
            status=1, subtract=1, value=0.00, default=True
        )
        transport = integration.makeTransport(
            special_method=True,
            excerpt="Courier - palette with max weight per unit",
            module_name="courier",
            module_settings={"fee": 0, "unit_fee": 160, "max_weight": 1000},
            name="Courier - palette with max weight per unit",
        )
        manufacturer = integration.makeManufacturer(
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fist-manufacturer.jpg",
            name="Fist Manufacturer",
            priority=0,
            seo_slug="fist_manufacturer",
            sort_order=1,
            status=1,
        )
        tax_description_1 = integration.makeTaxDescription(
            description="Tax just tax",
            language_id=2,
            name="Tax description uan",
            tax_id=tax_1.tax_id,
        )
        setting_template_1 = models.SettingTemplate(
            description="Setting template description",
            name="Tile template",
            order=1,
            status=1,
            template_json={
                "manufactured": 1,
                "virtual": 0,
                "subtract_quantity": 0,
                "storage_type": "",
                "attribute_type_list": [],
                "unit_label": 2,
            },
            template_type=1,
        )
        dbsession.add_all(
            (
                attribute2,
                discount,
                transport,
                manufacturer,
                tax_description_1,
                setting_template_1,
            )
        )
        dbsession.flush()
        app_request.method = "POST"
        app_request.matchdict = dict(action="create")
        product_dict = {
            # "allowed_free_transport": 0,
            # "allowed_free_transport_minimum_amount=": 2,
            "availability_id": attribute2.attribute_id,
            "catalog_price": 1111.50,
            "date_added": "2021-01-01",
            "date_available": "1970-01-01",
            "date_modified": "2021-01-01",
            "discount_id": discount.discount_id,
            "ean": "5909690592940",
            # "forbidden_transport_methods=": [1, 2],
            "height": 1.00,
            # "image": False,
            "isbn": 2,
            "jan": 1,
            "language_id": 2,
            "length": 1.00,
            "length_class_id": 1,
            "location": 4,
            "manual_discount": 0,
            "manufacturer_id": manufacturer.manufacturer_id,
            "meta_description": "this is a meta_description",
            "minimum": 1.01,
            "model": "Testing create form - missing description model",
            "mpn": 2,
            "name": "Testing create form - missing description name",
            "one_batch": 1,
            "pieces": 1,
            "points": 0,
            "price": 95.55,
            "product_type": 0,
            "product_status": 1,
            "quantity": 1,
            "seo_slug": "Testing create form - missing description name",
            "setting_template_id": setting_template_1.setting_template_id,
            "sku": 5,
            "sort_order": 0,
            "special_type": 1,
            "square_meter": 1,
            "product_status": 1,
            "stock_status_id": 8,
            "subtract": 1,
            "tag": "This is a tag",
            "tax_id": tax_1.tax_id,
            "u_h1": "This is a h1 headline",
            "u_title": "This is a u_title",
            "unit": 1.07,
            "upc": 6,
            "viewed": 0,
            "virtual": 0,
            "weight": 28.00,
            "weight_class_id": 1,
            "width": 1,
        }
        stringed_dict = "&".join(str(k) + "=" + str(v) for k, v in product_dict.items())
        app_request.body = str.encode(stringed_dict)
        app_request.context = self._makeContext()
        integration.setUser(dummy_config, user)
        dummy_config.testing_securitypolicy(userid=user.id)
        self._addRoutes(dummy_config)
        self._callFUT(app_request, "create")
        # Simple product query
        product_id = (
            dbsession.query(models.Product.product_id)
            .filter_by(model="Testing create form - missing description model")
            .scalar()
        )
        # Real product query used by the product view
        original_product = (
            services.ProductService.product_with_description_filter_product_id(
                app_request,
                product_id,
                False,
            )
        )
        # Asserting base elements and all that are formatted
        assert original_product.name == "Testing create form - missing description name"
        assert original_product.ean == "5909690592940"
        assert original_product.description == None


class Test_edit_product:
    def _callFUT(self, request, action, product_id):
        from emporium.views.product import product_edit

        request.referer = "/emporatorium/product_list"
        return product_edit(request)

    def _makeContext(self):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route("product_edit", "/emporatorium/product_{action}")
        config.add_route("s_product", "/{seoslug}-{product_id}")

    def test_submit_works(self, dummy_config, app_request, dbsession):
        # add a product to the db
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        setting_4 = integration.makeSetting(
            "image_settings",
            "Image settings",
            None,
            {
                "image_sizes": {"base": [1200, 1200], "thumbnails": [500, 500]},
                "image_extensions": [["JPEG", "jpg", "RGB"], ["WEBP", "webp", "RGBA"]],
            },
        )
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        tax_1 = integration.makeTax(1, 1, 23)

        attribute_display_type_1 = integration.makeAttributeDisplayType(
            "checkbox", "Checkbox", 1, 1
        )
        attribute_display_type_2 = integration.makeAttributeDisplayType(
            "radio", "Radio button", 2, 1
        )
        dbsession.add_all(
            [
                setting_1,
                setting_2,
                setting_3,
                setting_4,
                user,
                tax_1,
                attribute_display_type_1,
                attribute_display_type_2,
            ]
        )
        dbsession.flush()
        attribute_group1 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Antislip",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group2 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Availability",
            required=True,
            sort_order=3,
            status=1,
        )
        attribute_group3 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Frost",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group4 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Measurements",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group5 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Pei",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group6 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Quality",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group7 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Rekt",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group8 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Surface",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group9 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Tiles",
            required=False,
            sort_order=3,
            status=1,
        )
        tax_description_1 = integration.makeTaxDescription(
            description="Tax just tax",
            language_id=2,
            name="Tax description uan",
            tax_id=tax_1.tax_id,
        )
        dbsession.add_all(
            (
                attribute_group1,
                attribute_group2,
                attribute_group3,
                attribute_group4,
                attribute_group5,
                attribute_group6,
                attribute_group7,
                attribute_group8,
                attribute_group9,
                tax_description_1,
            )
        )
        dbsession.flush()
        attribute1 = integration.makeAttribute(
            attribute_group_id=attribute_group1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute2 = integration.makeAttribute(
            attribute_group_id=attribute_group2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Magazyn",
        )
        attribute3 = integration.makeAttribute(
            attribute_group_id=attribute_group3.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute4 = integration.makeAttribute(
            attribute_group_id=attribute_group4.attribute_group_id,
            sort_order=1,
            status=1,
            value="10x20",
        )
        attribute5 = integration.makeAttribute(
            attribute_group_id=attribute_group5.attribute_group_id,
            sort_order=1,
            status=1,
            value="PEI III",
        )
        attribute6 = integration.makeAttribute(
            attribute_group_id=attribute_group6.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pierwsza",
        )
        attribute7 = integration.makeAttribute(
            attribute_group_id=attribute_group7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute8 = integration.makeAttribute(
            attribute_group_id=attribute_group8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Poler",
        )
        discount = integration.makeDiscount(
            status=1, subtract=1, value=0.00, default=True
        )
        transport = integration.makeTransport(
            special_method=True,
            excerpt="Courier - palette with max weight per unit",
            module_name="courier",
            module_settings={"fee": 0, "unit_fee": 160, "max_weight": 1000},
            name="Courier - palette with max weight per unit",
        )
        dbsession.add_all(
            (
                attribute1,
                attribute2,
                attribute3,
                attribute4,
                attribute5,
                attribute6,
                attribute7,
                attribute8,
                discount,
                transport,
            )
        )
        manufacturer = integration.makeManufacturer(
            code="",
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fist-manufacturer.jpg",
            manufacturer_type=0,
            name="Fist Manufacturer",
            priority=0,
            sort_order=1,
            seo_slug="fist_manufacturer",
            status=1,
        )
        setting_template_1 = models.SettingTemplate(
            description="Setting template description",
            name="Tile template",
            order=1,
            status=1,
            template_json={
                "manufactured": 1,
                "virtual": 0,
                "subtract_quantity": 0,
                "storage_type": "",
                "attribute_type_list": [
                    attribute_group1.attribute_group_id,
                    attribute_group2.attribute_group_id,
                    attribute_group3.attribute_group_id,
                    attribute_group4.attribute_group_id,
                    attribute_group5.attribute_group_id,
                    attribute_group6.attribute_group_id,
                    attribute_group7.attribute_group_id,
                    attribute_group8.attribute_group_id,
                    attribute_group9.attribute_group_id,
                ],
                "unit_label": 2,
            },
            template_type=1,
        )
        dbsession.add_all([manufacturer, setting_template_1])
        dbsession.flush()
        product = integration.makeProduct(
            # allowed_free_transport=0,
            # allowed_free_transport_minimum_amount==2,
            availability_id=attribute2.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount.discount_id,
            ean="",
            # forbidden_transport_methods==[1, 2],
            height=1.00,
            # image="",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer.manufacturer_id,
            minimum=1.01,
            model="Testing product model editing - unchanged",
            mpn=2,
            name="Testing product name for editing - unchanged",
            one_batch=True,
            pieces=1,
            points=0,
            price=95.55,
            quantity=1,
            seo_slug="testing-product-name-editing",
            setting_template_id=setting_template_1.setting_template_id,
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=28.00,
            weight_class_id=1,
            width=1,
        )
        dbsession.add_all([product])
        dbsession.flush()
        product_description = integration.makeProductDescription(
            description="This is a product description",
            description_technical="This is a technical description",
            language_id=2,
            meta_description="this is a meta_description",
            name="This is THE OTHER name",
            product_id=product.product_id,
            tag="This is a tag",
            u_h1="This is a h1 headline",
            u_title="This is a u_title",
        )
        dbsession.add_all((product_description,))
        dbsession.flush()
        app_request.method = "POST"
        app_request.matchdict = dict(action="edit")
        product_dict = {
            # "allowed_free_transport": 0,
            # "allowed_free_transport_minimum_amount=": 2,
            "availability_id": attribute2.attribute_id,
            "catalog_price": 136.50,
            "date_added": "2021-01-01",
            "date_available": "1970-01-01",
            "date_modified": "2021-01-01",
            "description": "This is a product description",
            "discount_id": discount.discount_id,
            "ean": "5902610512339",
            # "forbidden_transport_methods=": [1, 2],
            "height": 1.00,
            # "image": False,
            "isbn": 2,
            "jan": 1,
            "language_id": 2,
            "length": 1.00,
            "length_class_id": 1,
            "location": 4,
            "manual_discount": 0,
            "manufacturer_id": manufacturer.manufacturer_id,
            "meta_description": "this is a meta_description",
            "minimum": 1.01,
            "model": "Testing create form - product changed model",
            "mpn": 2,
            "name": "Testing create form - product changed name",
            "one_batch": 1,
            "pieces": 1,
            "points": 0,
            "price": 95.55,
            "product_id": product.product_id,
            "product_type": 0,
            "quantity": 1,
            "seo_slug": "testing-product-name",
            "sku": 5,
            "sort_order": 0,
            "setting_template_id": setting_template_1.setting_template_id,
            "special_type": 1,
            "square_meter": 1,
            "product_status": 1,
            "stock_status_id": 8,
            "subtract": 1,
            "tag": "This is a tag",
            "tax_id": tax_1.tax_id,
            "u_h1": "This is a h1 headline",
            "u_title": "This is a u_title",
            "unit": 1.07,
            "upc": 6,
            "viewed": 0,
            "virtual": 0,
            "weight": 28.00,
            "weight_class_id": 1,
            "width": 1,
        }
        stringed_dict = "&".join(str(k) + "=" + str(v) for k, v in product_dict.items())
        app_request.body = str.encode(stringed_dict)
        app_request.context = self._makeContext()
        integration.setUser(dummy_config, user)
        dummy_config.testing_securitypolicy(userid=user.id)

        self._addRoutes(dummy_config)
        self._callFUT(app_request, "edit", product.product_id)
        updated_product = (
            dbsession.query(models.Product)
            .filter_by(model="Testing create form - product changed model")
            .one()
        )
        assert product.product_id == updated_product.product_id
        assert updated_product.name == "Testing create form - product changed name"
        assert updated_product.ean == "5902610512339"

    # def test_special_transport_method(self, dummy_config, app_request, dbsession):
    #     # add a product to the db
    #     setting_1 = integration.makeSetting("cache", True, 0)
    #     setting_2 = integration.makeSetting("shop_status", True, 0)
    #     setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
    #     user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
    #     tax_1 = integration.makeTax(1, 1, 23)

    #     attribute_display_type_1 = integration.makeAttributeDisplayType(
    #         "checkbox", "Checkbox", 1, 1
    #     )
    #     attribute_display_type_2 = integration.makeAttributeDisplayType(
    #         "radio", "Radio button", 2, 1
    #     )
    #     transport_1 = integration.makeTransport(
    #         True,
    #         "",
    #         "",
    #         "courier",
    #         {
    #             "manipulation_fee": "0",
    #             "unit_fee": "139",
    #             "unit_weight": "1000",
    #         },
    #         "Kurier",
    #         True,
    #     )
    #     transport_2 = integration.makeTransport(
    #         False,
    #         "",
    #         "",
    #         "courier_bulk",
    #         {
    #             "manipulation_fee": "180",
    #             "removed_transports": [],
    #             "unit_fee": "139",
    #             "unit_weight": "1000",
    #         },
    #         "Kurier gabaryt",
    #         True,
    #     )
    #     dbsession.add_all(
    #         [
    #             setting_1,
    #             setting_2,
    #             setting_3,
    #             user,
    #             tax_1,
    #             attribute_display_type_1,
    #             attribute_display_type_2,
    #             transport_1,
    #             transport_2,
    #         ]
    #     )
    #     dbsession.flush()
    #     transport_2.module_settings["removed_transports"] = [transport_1.transport_id]
    #     attribute_group1 = integration.makeAttributeGroup(
    #         attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
    #         name="Antislip",
    #         required=False,
    #         sort_order=3,
    #         status=1,
    #     )
    #     attribute_group2 = integration.makeAttributeGroup(
    #         attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
    #         name="Availability",
    #         required=True,
    #         sort_order=3,
    #         status=1,
    #     )
    #     attribute_group3 = integration.makeAttributeGroup(
    #         attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
    #         name="Frost",
    #         required=False,
    #         sort_order=3,
    #         status=1,
    #     )
    #     attribute_group4 = integration.makeAttributeGroup(
    #         attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
    #         name="Measurements",
    #         required=False,
    #         sort_order=3,
    #         status=1,
    #     )
    #     attribute_group5 = integration.makeAttributeGroup(
    #         attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
    #         name="Pei",
    #         required=False,
    #         sort_order=3,
    #         status=1,
    #     )
    #     attribute_group6 = integration.makeAttributeGroup(
    #         attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
    #         name="Quality",
    #         required=False,
    #         sort_order=3,
    #         status=1,
    #     )
    #     attribute_group7 = integration.makeAttributeGroup(
    #         attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
    #         name="Rekt",
    #         required=False,
    #         sort_order=3,
    #         status=1,
    #     )
    #     attribute_group8 = integration.makeAttributeGroup(
    #         attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
    #         name="Surface",
    #         required=False,
    #         sort_order=3,
    #         status=1,
    #     )
    #     attribute_group9 = integration.makeAttributeGroup(
    #         attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
    #         name="Tiles",
    #         required=False,
    #         sort_order=3,
    #         status=1,
    #     )
    #     tax_description_1 = integration.makeTaxDescription(
    #         description="Tax just tax",
    #         language_id=2,
    #         name="Tax description uan",
    #         tax_id=tax_1.tax_id,
    #     )
    #     dbsession.add_all(
    #         (
    #             attribute_group1,
    #             attribute_group2,
    #             attribute_group3,
    #             attribute_group4,
    #             attribute_group5,
    #             attribute_group6,
    #             attribute_group7,
    #             attribute_group8,
    #             attribute_group9,
    #             tax_description_1,
    #         )
    #     )
    #     dbsession.flush()
    #     attribute1 = integration.makeAttribute(
    #         attribute_group_id=attribute_group1.attribute_group_id,
    #         sort_order=1,
    #         status=1,
    #         value="Tak",
    #     )
    #     attribute2 = integration.makeAttribute(
    #         attribute_group_id=attribute_group2.attribute_group_id,
    #         sort_order=1,
    #         status=1,
    #         value="Magazyn",
    #     )
    #     attribute3 = integration.makeAttribute(
    #         attribute_group_id=attribute_group3.attribute_group_id,
    #         sort_order=1,
    #         status=1,
    #         value="Tak",
    #     )
    #     attribute4 = integration.makeAttribute(
    #         attribute_group_id=attribute_group4.attribute_group_id,
    #         sort_order=1,
    #         status=1,
    #         value="10x20",
    #     )
    #     attribute5 = integration.makeAttribute(
    #         attribute_group_id=attribute_group5.attribute_group_id,
    #         sort_order=1,
    #         status=1,
    #         value="PEI III",
    #     )
    #     attribute6 = integration.makeAttribute(
    #         attribute_group_id=attribute_group6.attribute_group_id,
    #         sort_order=1,
    #         status=1,
    #         value="Pierwsza",
    #     )
    #     attribute7 = integration.makeAttribute(
    #         attribute_group_id=attribute_group7.attribute_group_id,
    #         sort_order=1,
    #         status=1,
    #         value="Tak",
    #     )
    #     attribute8 = integration.makeAttribute(
    #         attribute_group_id=attribute_group8.attribute_group_id,
    #         sort_order=1,
    #         status=1,
    #         value="Poler",
    #     )
    #     discount = integration.makeDiscount(
    #         status=1, subtract=1, value=0.00, default=True
    #     )
    #     dbsession.add_all(
    #         (
    #             attribute1,
    #             attribute2,
    #             attribute3,
    #             attribute4,
    #             attribute5,
    #             attribute6,
    #             attribute7,
    #             attribute8,
    #             discount,
    #         )
    #     )
    #     manufacturer = integration.makeManufacturer(
    #         code="",
    #         date_added="2021-01-01",
    #         date_available="2021-01-01",
    #         date_modified="2021-01-01",
    #         image="folder/fist-manufacturer.jpg",
    #         manufacturer_type=0,
    #         name="Fist Manufacturer",
    #         priority=0,
    #         sort_order=1,
    #         status=1,
    #     )
    #     setting_template_1 = models.SettingTemplate(
    #         description="Setting template description",
    #         name="Tile template",
    #         order=1,
    #         status=1,
    #         template_json={
    #             "manufactured": 1,
    #             "virtual": 0,
    #             "subtract_quantity": 0,
    #             "storage_type": "",
    #             "attribute_type_list": [
    #                 attribute_group1.attribute_group_id,
    #                 attribute_group2.attribute_group_id,
    #                 attribute_group3.attribute_group_id,
    #                 attribute_group4.attribute_group_id,
    #                 attribute_group5.attribute_group_id,
    #                 attribute_group6.attribute_group_id,
    #                 attribute_group7.attribute_group_id,
    #                 attribute_group8.attribute_group_id,
    #                 attribute_group9.attribute_group_id,
    #             ],
    #             "unit_label": 2,
    #         },
    #         template_type=1,
    #     )
    #     dbsession.add_all([manufacturer, setting_template_1])
    #     dbsession.flush()
    #     product = integration.makeProduct(
    #         #allowed_free_transport=0,
    #         #allowed_free_transport_minimum_amount==2,
    #         availability_id=attribute2.attribute_id,
    #         catalog_price=136.50,
    #         date_added="2021-01-01",
    #         date_available="1970-01-01",
    #         date_modified="2021-01-01",
    #         discount_id=discount.discount_id,
    #         ean="",
    #         #forbidden_transport_methods==[1, 2],
    #         height=1.00,
    #         # image="",
    #         isbn=2,
    #         jan=1,
    #         length=1.00,
    #         length_class_id=1,
    #         location=4,
    #         manual_discount=False,
    #         manufacturer_id=manufacturer.manufacturer_id,
    #         minimum=1.01,
    #         model="Testing product model editing - unchanged",
    #         mpn=2,
    #         name="Testing product name for editing - unchanged",
    #         one_batch=True,
    #         pieces=1,
    #         points=0,
    #         price=95.55,
    #         quantity=1,
    #         seo_slug="testing-product-name-editing",
    #         setting_template_id=setting_template_1.setting_template_id,
    #         sku=5,
    #         sort_order=0,
    #         square_meter=1,
    #         status=1,
    #         stock_status_id=8,
    #         subtract=1,
    #         tax_id=tax_1.tax_id,
    #         unit=1.07,
    #         upc=6,
    #         viewed=0,
    #         virtual=0,
    #         weight=28.00,
    #         weight_class_id=1,
    #         width=1,
    #     )
    #     dbsession.add_all([product])
    #     dbsession.flush()
    #     product_description = integration.makeProductDescription(
    #         description="This is a product description",
    #         language_id=2,
    #         meta_description="this is a meta_description",
    #         name="This is THE OTHER name",
    #         product_id=product.product_id,
    #         tag="This is a tag",
    #         u_h1="This is a h1 headline",
    #         u_title="This is a u_title",
    #     )
    #     dbsession.add_all((product_description,))
    #     dbsession.flush()
    #     app_request.method = "POST"
    #     app_request.matchdict = dict(action="edit")
    #     product_dict = {
    #        #"allowed_free_transport": 0,
    #         #"allowed_free_transport_minimum_amount=": 2,
    #         "availability_id": attribute2.attribute_id,
    #         "catalog_price": 136.50,
    #         "date_added": "2021-01-01",
    #         "date_available": "1970-01-01",
    #         "date_modified": "2021-01-01",
    #         "description": "This is a product description",
    #         "discount_id": discount.discount_id,
    #         "ean": "5902610512339",
    #         #"forbidden_transport_methods=": [1, 2],
    #         "transport_methods": [transport_2.transport_id],
    #         "height": 1.00,
    #         # "image": False,
    #         "isbn": 2,
    #         "jan": 1,
    #         "language_id": 2,
    #         "length": 1.00,
    #         "length_class_id": 1,
    #         "location": 4,
    #         "manual_discount": 0,
    #         "manufacturer_id": manufacturer.manufacturer_id,
    #         "meta_description": "this is a meta_description",
    #         "minimum": 1.01,
    #         "model": "Testing create form - product changed model",
    #         "mpn": 2,
    #         "name": "Testing create form - product changed name",
    #         "one_batch": 1,
    #         "pieces": 1,
    #         "points": 0,
    #         "price": 95.55,
    #         "product_id": product.product_id,
    #         "product_type": 0,
    #         "quantity": 1,
    #         "seo_slug": "testing-product-name",
    #         "sku": 5,
    #         "sort_order": 0,
    #         "setting_template_id": setting_template_1.setting_template_id,
    #         "special_type": 1,
    #         "square_meter": 1,
    #         "product_status": 1,
    #         "stock_status_id": 8,
    #         "subtract": 1,
    #         "tag": "This is a tag",
    #         "tax_id": tax_1.tax_id,
    #         "u_h1": "This is a h1 headline",
    #         "u_title": "This is a u_title",
    #         "unit": 1.07,
    #         "upc": 6,
    #         "viewed": 0,
    #         "virtual": 0,
    #         "weight": 28.00,
    #         "weight_class_id": 1,
    #         "width": 1,
    #     }
    #     stringed_dict = "&".join(str(k) + "=" + str(v) for k, v in product_dict.items())
    #     app_request.body = str.encode(stringed_dict)
    #     app_request.context = self._makeContext()
    #     integration.setUser(dummy_config, user)
    #     dummy_config.testing_securitypolicy(userid=user.id)

    #     self._addRoutes(dummy_config)
    #     self._callFUT(app_request, "edit", product.product_id)
    #     product_transport = (
    #         dbsession.query(models.ProductToTransport)
    #         .filter_by(product_id=product.product_id)
    #         .one()
    #     )

    #     assert module_transport_courier_bulk == transport_2.transport_id
    #     assert product_transport.local_settings == {}

    def test_submit_works_missing_description(
        self, dummy_config, app_request, dbsession
    ):
        # add a product to the db
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        setting_4 = integration.makeSetting(
            "image_settings",
            "Image settings",
            None,
            {
                "image_sizes": {"base": [1200, 1200], "thumbnails": [500, 500]},
                "image_extensions": [["JPEG", "jpg", "RGB"], ["WEBP", "webp", "RGBA"]],
            },
        )
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        tax_1 = integration.makeTax(1, 1, 23)

        attribute_display_type_1 = integration.makeAttributeDisplayType(
            "checkbox", "Checkbox", 1, 1
        )
        attribute_display_type_2 = integration.makeAttributeDisplayType(
            "radio", "Radio button", 2, 1
        )
        dbsession.add_all(
            [
                setting_1,
                setting_2,
                setting_3,
                setting_4,
                user,
                tax_1,
                attribute_display_type_1,
                attribute_display_type_2,
            ]
        )
        dbsession.flush()
        attribute_group1 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Antislip",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group2 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Availability",
            required=True,
            sort_order=3,
            status=1,
        )
        attribute_group3 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Frost",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group4 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Measurements",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group5 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Pei",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group6 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Quality",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group7 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Rekt",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group8 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Surface",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group9 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Tiles",
            required=False,
            sort_order=3,
            status=1,
        )
        tax_description_1 = integration.makeTaxDescription(
            description="Tax just tax",
            language_id=2,
            name="Tax description uan",
            tax_id=tax_1.tax_id,
        )
        dbsession.add_all(
            (
                attribute_group1,
                attribute_group2,
                attribute_group3,
                attribute_group4,
                attribute_group5,
                attribute_group6,
                attribute_group7,
                attribute_group8,
                attribute_group9,
                tax_description_1,
            )
        )
        dbsession.flush()
        attribute1 = integration.makeAttribute(
            attribute_group_id=attribute_group1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute2 = integration.makeAttribute(
            attribute_group_id=attribute_group2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Magazyn",
        )
        attribute3 = integration.makeAttribute(
            attribute_group_id=attribute_group3.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute4 = integration.makeAttribute(
            attribute_group_id=attribute_group4.attribute_group_id,
            sort_order=1,
            status=1,
            value="10x20",
        )
        attribute5 = integration.makeAttribute(
            attribute_group_id=attribute_group5.attribute_group_id,
            sort_order=1,
            status=1,
            value="PEI III",
        )
        attribute6 = integration.makeAttribute(
            attribute_group_id=attribute_group6.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pierwsza",
        )
        attribute7 = integration.makeAttribute(
            attribute_group_id=attribute_group7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute8 = integration.makeAttribute(
            attribute_group_id=attribute_group8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Poler",
        )
        discount = integration.makeDiscount(
            status=1, subtract=1, value=0.00, default=True
        )
        transport = integration.makeTransport(
            special_method=True,
            excerpt="Courier - palette with max weight per unit",
            module_name="courier",
            module_settings={"fee": 0, "unit_fee": 160, "max_weight": 1000},
            name="Courier - palette with max weight per unit",
        )
        setting_template_1 = models.SettingTemplate(
            description="Setting template description",
            name="Tile template",
            order=1,
            status=1,
            template_json={
                "manufactured": 1,
                "virtual": 0,
                "subtract_quantity": 0,
                "storage_type": "",
                "attribute_type_list": [
                    attribute_group1.attribute_group_id,
                    attribute_group2.attribute_group_id,
                    attribute_group3.attribute_group_id,
                    attribute_group4.attribute_group_id,
                    attribute_group5.attribute_group_id,
                    attribute_group6.attribute_group_id,
                    attribute_group7.attribute_group_id,
                    attribute_group8.attribute_group_id,
                    attribute_group9.attribute_group_id,
                ],
                "unit_label": 2,
            },
            template_type=1,
        )

        dbsession.add_all(
            (
                attribute1,
                attribute2,
                attribute3,
                attribute4,
                attribute5,
                attribute6,
                attribute7,
                attribute8,
                discount,
                transport,
                setting_template_1,
            )
        )
        dbsession.flush()
        manufacturer = integration.makeManufacturer(
            code="",
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fist-manufacturer.jpg",
            manufacturer_type=0,
            name="Fist Manufacturer",
            priority=0,
            seo_slug="fist_manufacturer",
            sort_order=1,
            status=1,
        )
        dbsession.add_all([manufacturer])
        dbsession.flush()
        product = integration.makeProduct(
            # allowed_free_transport=0,
            # allowed_free_transport_minimum_amount==2,
            availability_id=attribute2.attribute_id,
            catalog_price=136.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount.discount_id,
            ean="",
            # forbidden_transport_methods==[1, 2],
            height=1.00,
            # image="",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer.manufacturer_id,
            minimum=1.01,
            model="Testing edit form model - missing product description",
            mpn=2,
            name="Testing edit form name - missing product description",
            one_batch=True,
            pieces=1,
            points=0,
            price=95.55,
            quantity=1,
            seo_slug="testing-product-name-editing",
            sku=5,
            sort_order=0,
            setting_template_id=setting_template_1.setting_template_id,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=28.00,
            weight_class_id=1,
            width=1,
        )
        dbsession.add_all([product])
        dbsession.flush()
        product_description = integration.makeProductDescription(
            description="This is a product description",
            description_technical="This is a technical description",
            language_id=2,
            meta_description="this is a meta_description",
            name="This is THE OTHER name",
            product_id=product.product_id,
            tag="This is a tag",
            u_h1="This is a h1 headline",
            u_title="This is a u_title",
        )
        dbsession.add_all((product_description,))
        dbsession.flush()
        app_request.method = "POST"
        app_request.matchdict = dict(action="edit")
        product_dict = {
            # "allowed_free_transport": 0,
            # "allowed_free_transport_minimum_amount=": 2,
            "availability_id": attribute2.attribute_id,
            "catalog_price": 136.50,
            "date_added": "2021-01-01",
            "date_available": "1970-01-01",
            "date_modified": "2021-01-01",
            "discount_id": discount.discount_id,
            "ean": "5902610512339",
            # "forbidden_transport_methods=": [1, 2],
            "height": 1.00,
            # "image": False,
            "isbn": 2,
            "jan": 1,
            "language_id": 2,
            "length": 1.00,
            "length_class_id": 1,
            "location": 4,
            "manual_discount": 0,
            "manufacturer_id": manufacturer.manufacturer_id,
            "meta_description": "this is a meta_description",
            "minimum": 1.01,
            "model": "Testing edit form model - missing product description",
            "mpn": 2,
            "name": "Testing edit form name - missing product description",
            "one_batch": 1,
            "pieces": 1,
            "points": 0,
            "price": 95.55,
            "product_id": product.product_id,
            "product_type": 0,
            "quantity": 1,
            "seo_slug": "testing-product-name",
            "sku": 5,
            "sort_order": 0,
            "special_type": 1,
            "square_meter": 1,
            "product_status": 1,
            "stock_status_id": 8,
            "subtract": 1,
            "setting_template_id": setting_template_1.setting_template_id,
            "tag": "This is a tag",
            "tax_id": tax_1.tax_id,
            "u_h1": "This is a h1 headline",
            "u_title": "This is a u_title",
            "unit": 1.07,
            "upc": 6,
            "viewed": 0,
            "virtual": 0,
            "weight": 28.00,
            "weight_class_id": 1,
            "width": 1,
            "attribute_list": attribute1.attribute_id,
            "attribute_list": attribute3.attribute_id,
            "attribute_list": attribute4.attribute_id,
            "attribute_list": attribute5.attribute_id,
            "attribute_list": attribute6.attribute_id,
            "attribute_list": attribute7.attribute_id,
            "attribute_list": attribute8.attribute_id,
        }
        stringed_dict = "&".join(str(k) + "=" + str(v) for k, v in product_dict.items())
        app_request.body = str.encode(stringed_dict)
        app_request.context = self._makeContext()
        integration.setUser(dummy_config, user)
        dummy_config.testing_securitypolicy(userid=user.id)

        self._addRoutes(dummy_config)
        self._callFUT(app_request, "edit", product.product_id)
        # Establish id of the edited product
        product = (
            dbsession.query(models.Product)
            .filter_by(model="Testing edit form model - missing product description")
            .one()
        )
        # Use original query (used by the product view)
        original_product = (
            services.ProductService.product_with_description_filter_product_id(
                app_request,
                product.product_id,
                False,
            )
        )
        assert (
            original_product.name
            == "Testing edit form name - missing product description"
        )
        assert original_product.description == None


class Test_clone_product:
    def _callFUT(self, request, action, product_id):
        from emporium.views.product import product_edit

        request.referer = "/emporatorium/product_list"
        return product_edit(request)

    def _makeContext(self):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route("product_edit", "/emporatorium/product_{action}")
        config.add_route("s_product", "/{seoslug}-{product_id}")

    def test_submit_works(self, dummy_config, app_request, dbsession):
        # add a product to the db
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        setting_4 = integration.makeSetting(
            "image_settings",
            "Image settings",
            None,
            {
                "image_sizes": {"base": [1200, 1200], "thumbnails": [500, 500]},
                "image_extensions": [["JPEG", "jpg", "RGB"], ["WEBP", "webp", "RGBA"]],
            },
        )
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        tax_1 = integration.makeTax(1, 1, 23)

        attribute_display_type_1 = integration.makeAttributeDisplayType(
            "checkbox", "Checkbox", 1, 1
        )
        attribute_display_type_2 = integration.makeAttributeDisplayType(
            "radio", "Radio button", 2, 1
        )
        dbsession.add_all(
            [
                setting_1,
                setting_2,
                setting_3,
                setting_4,
                user,
                tax_1,
                attribute_display_type_1,
                attribute_display_type_2,
            ]
        )
        dbsession.flush()
        attribute_group1 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Antislip",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group2 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Availability",
            required=True,
            sort_order=3,
            status=1,
        )
        attribute_group3 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Frost",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group4 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Measurements",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group5 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Pei",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group6 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Quality",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group7 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Rekt",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group8 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Surface",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group9 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Tiles",
            required=False,
            sort_order=3,
            status=1,
        )
        tax_description_1 = integration.makeTaxDescription(
            description="Tax just tax",
            language_id=2,
            name="Tax description uan",
            tax_id=tax_1.tax_id,
        )
        dbsession.add_all(
            (
                attribute_group1,
                attribute_group2,
                attribute_group3,
                attribute_group4,
                attribute_group5,
                attribute_group6,
                attribute_group7,
                attribute_group8,
                attribute_group9,
                tax_description_1,
            )
        )
        dbsession.flush()
        attribute1 = integration.makeAttribute(
            attribute_group_id=attribute_group1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute2 = integration.makeAttribute(
            attribute_group_id=attribute_group2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Magazyn",
        )
        attribute3 = integration.makeAttribute(
            attribute_group_id=attribute_group3.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute4 = integration.makeAttribute(
            attribute_group_id=attribute_group4.attribute_group_id,
            sort_order=1,
            status=1,
            value="10x20",
        )
        attribute5 = integration.makeAttribute(
            attribute_group_id=attribute_group5.attribute_group_id,
            sort_order=1,
            status=1,
            value="PEI III",
        )
        attribute6 = integration.makeAttribute(
            attribute_group_id=attribute_group6.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pierwsza",
        )
        attribute7 = integration.makeAttribute(
            attribute_group_id=attribute_group7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute8 = integration.makeAttribute(
            attribute_group_id=attribute_group8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Poler",
        )
        discount = integration.makeDiscount(
            status=1, subtract=1, value=0.00, default=True
        )
        transport = integration.makeTransport(
            special_method=True,
            excerpt="Courier - palette with max weight per unit",
            module_name="courier",
            module_settings={"fee": 0, "unit_fee": 160, "max_weight": 1000},
            name="Courier - palette with max weight per unit",
        )
        dbsession.add_all(
            (
                attribute1,
                attribute2,
                attribute3,
                attribute4,
                attribute5,
                attribute6,
                attribute7,
                attribute8,
                discount,
                transport,
            )
        )
        dbsession.flush()
        manufacturer = integration.makeManufacturer(
            code="",
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fist-manufacturer.jpg",
            manufacturer_type=0,
            name="Fist Manufacturer",
            priority=0,
            seo_slug="fist_manufacturer",
            sort_order=1,
            status=1,
        )
        setting_template_1 = models.SettingTemplate(
            description="Setting template description",
            name="Tile template",
            order=1,
            status=1,
            template_json={
                "manufactured": 1,
                "virtual": 0,
                "subtract_quantity": 0,
                "storage_type": "",
                "attribute_type_list": [
                    attribute_group1.attribute_group_id,
                    attribute_group2.attribute_group_id,
                    attribute_group3.attribute_group_id,
                    attribute_group4.attribute_group_id,
                    attribute_group5.attribute_group_id,
                    attribute_group6.attribute_group_id,
                    attribute_group7.attribute_group_id,
                    attribute_group8.attribute_group_id,
                    attribute_group9.attribute_group_id,
                ],
                "unit_label": 2,
            },
            template_type=1,
        )
        tax_description_1 = integration.makeTaxDescription(
            description="Tax just tax",
            language_id=2,
            name="Tax description uan",
            tax_id=tax_1.tax_id,
        )
        dbsession.add_all([manufacturer, setting_template_1, tax_description_1])
        dbsession.flush()
        product = integration.makeProduct(
            # allowed_free_transport=0,
            # allowed_free_transport_minimum_amount==2,
            availability_id=attribute2.attribute_id,
            catalog_price=101.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount.discount_id,
            ean="5902610512339",
            # forbidden_transport_methods==[1, 2],
            height=1.00,
            # image="",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer.manufacturer_id,
            minimum=1.01,
            model="Testing product model to be cloned",
            mpn=2,
            name="Testing product name  to be cloned",
            one_batch=True,
            pieces=1,
            points=0,
            price=95.55,
            quantity=1,
            seo_slug="",
            setting_template_id=setting_template_1.setting_template_id,
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=28.00,
            weight_class_id=1,
            width=1,
        )
        dbsession.add_all([product])
        dbsession.flush()
        product_description = integration.makeProductDescription(
            description="This is a product description",
            description_technical="This is a technical description",
            language_id=2,
            meta_description="this is a meta_description",
            name="This is THE OTHER name",
            product_id=product.product_id,
            tag="This is a tag",
            u_h1="This is a h1 headline",
            u_title="This is a u_title",
        )
        dbsession.add_all((product_description,))
        dbsession.flush()
        app_request.method = "POST"
        app_request.matchdict = dict(action="clone")
        product_dict = {
            # "allowed_free_transport": 0,
            # "allowed_free_transport_minimum_amount=": 2,
            "availability_id": attribute2.attribute_id,
            "catalog_price": 1111.50,
            "date_added": "2021-01-01",
            "date_available": "1970-01-01",
            "date_modified": "2021-01-01",
            "description": "This is a product description",
            "discount_id": discount.discount_id,
            "ean": "5902610512340",
            # "forbidden_transport_methods=": [1, 2],
            "height": 1.00,
            # "image": False,
            "isbn": 2,
            "jan": 1,
            "language_id": 2,
            "length": 1.00,
            "length_class_id": 1,
            "location": 4,
            "manual_discount": 0,
            "manufacturer_id": manufacturer.manufacturer_id,
            "meta_description": "this is a meta_description",
            "minimum": 1.01,
            "model": "Testing create form - product cloned model",
            "mpn": 2,
            "name": "Testing create form - product cloned name",
            "one_batch": 1,
            "pieces": 1,
            "points": 0,
            "price": 95.55,
            "product_id": product.product_id,
            "product_status": 1,
            "product_type": 0,
            "quantity": 1,
            "seo_slug": "testing-product-name",
            "setting_template_id": product.setting_template_id,
            "sku": 5,
            "sort_order": 0,
            "special_type": 1,
            "square_meter": 1,
            "stock_status_id": 8,
            "subtract": 1,
            "tag": "This is a tag",
            "tax_id": tax_1.tax_id,
            "u_h1": "This is a h1 headline",
            "u_title": "This is a u_title",
            "unit": 1.07,
            "upc": 6,
            "viewed": 0,
            "virtual": 0,
            "weight": 28.00,
            "weight_class_id": 1,
            "width": 1,
            "attribute_list": attribute1.attribute_id,
            "attribute_list": attribute3.attribute_id,
            "attribute_list": attribute4.attribute_id,
            "attribute_list": attribute5.attribute_id,
            "attribute_list": attribute6.attribute_id,
            "attribute_list": attribute7.attribute_id,
            "attribute_list": attribute8.attribute_id,
        }
        stringed_dict = "&".join(str(k) + "=" + str(v) for k, v in product_dict.items())
        app_request.body = str.encode(stringed_dict)
        app_request.context = self._makeContext()
        integration.setUser(dummy_config, user)
        dummy_config.testing_securitypolicy(userid=user.id)

        self._addRoutes(dummy_config)
        self._callFUT(app_request, "clone", product.product_id)
        original_product = (
            dbsession.query(models.Product)
            .filter_by(model="Testing product model to be cloned")
            .one()
        )
        assert original_product.name == "Testing product name  to be cloned"
        assert original_product.ean == "5902610512339"
        cloned_product = (
            dbsession.query(models.Product)
            .filter_by(model="Testing create form - product cloned model")
            .one()
        )
        assert cloned_product.name == "Testing create form - product cloned name"
        assert cloned_product.ean == "5902610512340"

    def test_submit_works_missing_description(
        self, dummy_config, app_request, dbsession
    ):
        # add a product to the db
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        setting_4 = integration.makeSetting(
            "image_settings",
            "Image settings",
            None,
            {
                "image_sizes": {"base": [1200, 1200], "thumbnails": [500, 500]},
                "image_extensions": [["JPEG", "jpg", "RGB"], ["WEBP", "webp", "RGBA"]],
            },
        )
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        tax_1 = integration.makeTax(1, 1, 23)

        attribute_display_type_1 = integration.makeAttributeDisplayType(
            "checkbox", "Checkbox", 1, 1
        )
        attribute_display_type_2 = integration.makeAttributeDisplayType(
            "radio", "Radio button", 2, 1
        )
        dbsession.add_all(
            [
                setting_1,
                setting_2,
                setting_3,
                setting_4,
                user,
                tax_1,
                attribute_display_type_1,
                attribute_display_type_2,
            ]
        )
        dbsession.flush()
        attribute_group1 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Antislip",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group2 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Availability",
            required=True,
            sort_order=3,
            status=1,
        )
        attribute_group3 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Frost",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group4 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Measurements",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group5 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Pei",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group6 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Quality",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group7 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Rekt",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group8 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Surface",
            required=False,
            sort_order=3,
            status=1,
        )
        attribute_group9 = integration.makeAttributeGroup(
            attribute_display_type_id=attribute_display_type_2.attribute_display_type_id,
            name="Tiles",
            required=False,
            sort_order=3,
            status=1,
        )
        tax_description_1 = integration.makeTaxDescription(
            description="Tax just tax",
            language_id=2,
            name="Tax description uan",
            tax_id=tax_1.tax_id,
        )
        dbsession.add_all(
            (
                attribute_group1,
                attribute_group2,
                attribute_group3,
                attribute_group4,
                attribute_group5,
                attribute_group6,
                attribute_group7,
                attribute_group8,
                attribute_group9,
                tax_description_1,
            )
        )
        dbsession.flush()
        attribute1 = integration.makeAttribute(
            attribute_group_id=attribute_group1.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute2 = integration.makeAttribute(
            attribute_group_id=attribute_group2.attribute_group_id,
            sort_order=1,
            status=1,
            value="Magazyn",
        )
        attribute3 = integration.makeAttribute(
            attribute_group_id=attribute_group3.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute4 = integration.makeAttribute(
            attribute_group_id=attribute_group4.attribute_group_id,
            sort_order=1,
            status=1,
            value="10x20",
        )
        attribute5 = integration.makeAttribute(
            attribute_group_id=attribute_group5.attribute_group_id,
            sort_order=1,
            status=1,
            value="PEI III",
        )
        attribute6 = integration.makeAttribute(
            attribute_group_id=attribute_group6.attribute_group_id,
            sort_order=1,
            status=1,
            value="Pierwsza",
        )
        attribute7 = integration.makeAttribute(
            attribute_group_id=attribute_group7.attribute_group_id,
            sort_order=1,
            status=1,
            value="Tak",
        )
        attribute8 = integration.makeAttribute(
            attribute_group_id=attribute_group8.attribute_group_id,
            sort_order=1,
            status=1,
            value="Poler",
        )
        discount = integration.makeDiscount(
            status=1, subtract=1, value=0.00, default=True
        )
        transport = integration.makeTransport(
            special_method=True,
            excerpt="Courier - palette with max weight per unit",
            module_name="courier",
            module_settings={"fee": 0, "unit_fee": 160, "max_weight": 1000},
            name="Courier - palette with max weight per unit",
        )
        dbsession.add_all(
            (
                attribute1,
                attribute2,
                attribute3,
                attribute4,
                attribute5,
                attribute6,
                attribute7,
                attribute8,
                discount,
                transport,
            )
        )
        dbsession.flush()
        manufacturer = integration.makeManufacturer(
            code="",
            date_added="2021-01-01",
            date_available="2021-01-01",
            date_modified="2021-01-01",
            image="folder/fist-manufacturer.jpg",
            manufacturer_type=0,
            name="Fist Manufacturer",
            priority=0,
            seo_slug="fist_manufacturer",
            sort_order=1,
            status=1,
        )
        tax_description_1 = integration.makeTaxDescription(
            description="Tax just tax",
            language_id=2,
            name="Tax description uan",
            tax_id=tax_1.tax_id,
        )
        setting_template_1 = models.SettingTemplate(
            description="Setting template description",
            name="Tile template",
            order=1,
            status=1,
            template_json={
                "manufactured": 1,
                "virtual": 0,
                "subtract_quantity": 0,
                "storage_type": "",
                "attribute_type_list": [
                    attribute_group1.attribute_group_id,
                    attribute_group2.attribute_group_id,
                    attribute_group3.attribute_group_id,
                    attribute_group4.attribute_group_id,
                    attribute_group5.attribute_group_id,
                    attribute_group6.attribute_group_id,
                    attribute_group7.attribute_group_id,
                    attribute_group8.attribute_group_id,
                    attribute_group9.attribute_group_id,
                ],
                "unit_label": 2,
            },
            template_type=1,
        )
        dbsession.add_all([manufacturer, tax_description_1, setting_template_1])
        dbsession.flush()
        product = integration.makeProduct(
            # allowed_free_transport=0,
            # allowed_free_transport_minimum_amount==2,
            availability_id=attribute2.attribute_id,
            catalog_price=101.50,
            date_added="2021-01-01",
            date_available="1970-01-01",
            date_modified="2021-01-01",
            discount_id=discount.discount_id,
            ean="5902610512339",
            # forbidden_transport_methods==[1, 2],
            height=1.00,
            # image="",
            isbn=2,
            jan=1,
            length=1.00,
            length_class_id=1,
            location=4,
            manual_discount=False,
            manufacturer_id=manufacturer.manufacturer_id,
            minimum=1.01,
            model="Testing product model to be cloned",
            mpn=2,
            name="Testing product name  to be cloned",
            one_batch=True,
            pieces=1,
            points=0,
            price=95.55,
            quantity=1,
            seo_slug="",
            setting_template_id=setting_template_1.setting_template_id,
            sku=5,
            sort_order=0,
            square_meter=1,
            status=1,
            stock_status_id=8,
            subtract=1,
            tax_id=tax_1.tax_id,
            unit=1.07,
            upc=6,
            viewed=0,
            virtual=0,
            weight=28.00,
            weight_class_id=1,
            width=1,
        )
        dbsession.add_all([product])
        dbsession.flush()
        product_description = integration.makeProductDescription(
            description="This is a product description - not to be cloned",
            language_id=2,
            meta_description="this is a meta_description",
            name="This is THE OTHER name",
            product_id=product.product_id,
            tag="This is a tag",
            u_h1="This is a h1 headline",
            u_title="This is a u_title",
        )
        dbsession.add_all((product_description,))
        dbsession.flush()
        app_request.method = "POST"
        app_request.matchdict = dict(action="clone")
        product_dict = {
            # "allowed_free_transport": 0,
            # "allowed_free_transport_minimum_amount=": 2,
            "availability_id": attribute2.attribute_id,
            "catalog_price": 1111.50,
            "date_added": "2021-01-01",
            "date_available": "1970-01-01",
            "date_modified": "2021-01-01",
            "description": "",
            "discount_id": discount.discount_id,
            "ean": "5902610512344",
            # "forbidden_transport_methods=": [1, 2],
            "height": 1.00,
            # "image": False,
            "isbn": 2,
            "jan": 1,
            "language_id": 2,
            "length": 1.00,
            "length_class_id": 1,
            "location": 4,
            "manual_discount": 0,
            "manufacturer_id": manufacturer.manufacturer_id,
            "meta_description": "this is a meta_description",
            "minimum": 1.01,
            "model": "Testing create form - product cloned model",
            "mpn": 2,
            "name": "Testing create form - product cloned name",
            "one_batch": 1,
            "pieces": 1,
            "points": 0,
            "price": 95.55,
            "product_id": product.product_id,
            "product_type": 0,
            "quantity": 1,
            "seo_slug": "testing-product-name",
            "setting_template_id": setting_template_1.setting_template_id,
            "sku": 5,
            "sort_order": 0,
            "special_type": 1,
            "square_meter": 1,
            "product_status": 1,
            "stock_status_id": 8,
            "subtract": 1,
            "tag": "This is a tag",
            "tax_id": tax_1.tax_id,
            "u_h1": "This is a h1 headline",
            "u_title": "This is a u_title",
            "unit": 1.07,
            "upc": 6,
            "viewed": 0,
            "virtual": 0,
            "weight": 28.00,
            "weight_class_id": 1,
            "width": 1,
            "attribute_list": attribute1.attribute_id,
            "attribute_list": attribute3.attribute_id,
            "attribute_list": attribute4.attribute_id,
            "attribute_list": attribute5.attribute_id,
            "attribute_list": attribute6.attribute_id,
            "attribute_list": attribute7.attribute_id,
            "attribute_list": attribute8.attribute_id,
        }
        stringed_dict = "&".join(str(k) + "=" + str(v) for k, v in product_dict.items())
        app_request.body = str.encode(stringed_dict)
        app_request.context = self._makeContext()
        integration.setUser(dummy_config, user)
        dummy_config.testing_securitypolicy(userid=user.id)
        self._addRoutes(dummy_config)
        self._callFUT(app_request, "clone", product.product_id)
        original_product = (
            services.ProductService.product_with_description_filter_product_id(
                app_request,
                product.product_id,
                False,
            )
        )
        assert original_product.name == "Testing product name  to be cloned"
        assert original_product.ean == "5902610512339"
        assert (
            original_product.description
            == "This is a product description - not to be cloned"
        )
        new_product = (
            dbsession.query(models.Product)
            .filter_by(model="Testing create form - product cloned model")
            .one()
        )
        cloned_product = (
            services.ProductService.product_with_description_filter_product_id(
                app_request,
                new_product.product_id,
                False,
            )
        )
        assert cloned_product.name == "Testing create form - product cloned name"
        assert cloned_product.ean == "5902610512344"
        assert cloned_product.description == ""
