from pyramid.testing import DummySecurityPolicy
from webob.multidict import MultiDict

from emporium import models

from tests import integration


class Test_transport_module:
    def _callFUT(self, request, module_name, module_id):
        from emporium.views.transport import transport_edit

        request.matchdict["module_name"] = module_name
        request.matchdict["module_id"] = module_id
        request.referer = "/emporatorium/transport_list"
        return transport_edit(request)

    def _makeContext(self):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route(
            "transport_action", "/emporatorium/transport_{module_name}_{module_id}"
        )

    def test_edit_transport(self, dummy_config, dummy_request, dbsession):
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        dbsession.add_all([setting_1, setting_2, setting_3, user])
        dbsession.flush()
        transport = integration.makeTransport(
            True,
            "Test Courier Edit Description",
            "Test Courier Edit Excerpt",
            "test_courier_edit",
            {},
            "Test Courier Edit Name",
            True,
        )
        dbsession.add_all([transport])
        dbsession.flush()
        self._addRoutes(dummy_config)
        dummy_request.method = "POST"
        dummy_request.POST = MultiDict(
            [
                ("form.submitted", True),
                ("name", "Test Courier Updated Name"),
                ("description", "Test Courier Updated Description"),
                ("excerpt", "Test Courier Updated Excerpt"),
                ("status", True),
                ("special_method", False),
            ]
        )
        integration.setUser(dummy_config, user)
        dummy_request.context = self._makeContext()
        response = self._callFUT(
            dummy_request, transport.module_name, transport.transport_id
        )
        assert response.location == "transport_list"
        new_transport = (
            dbsession.query(models.Transport)
            .filter_by(transport_id=transport.transport_id)
            .one()
        )
        assert new_transport.special_method == False
        assert new_transport.description == "Test Courier Updated Description"
        assert new_transport.excerpt == "Test Courier Updated Excerpt"
        assert new_transport.name == "Test Courier Updated Name"
        assert new_transport.status == True
