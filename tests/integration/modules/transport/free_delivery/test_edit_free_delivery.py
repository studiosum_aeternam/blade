from pyramid.testing import DummySecurityPolicy
from webob.multidict import MultiDict

from emporium import models

from tests import integration


class Test_transport_free_delivery:
    def _callFUT(self, request, module_name, module_id):
        from emporium.views.transport import transport_edit

        request.matchdict["module_name"] = module_name
        request.matchdict["module_id"] = module_id
        request.referer = "/emporatorium/transport_list"
        return transport_edit(request)

    def _makeContext(self):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route(
            "transport_action", "/emporatorium/transport_{module_name}_{module_id}"
        )
        # config.add_route("s_transport", "/blog/{transportname}-{id}")

    def test_edit_transport_free_delivery_settings(
        self, dummy_config, dummy_request, dbsession
    ):
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        dbsession.add_all([setting_1, setting_2, setting_3, user])
        dbsession.flush()
        transport1 = integration.makeTransport(
            True,
            "Test Free Delivery Edit Description",
            "Test Free Delivery Edit Excerpt",
            "free_delivery",
            {"removed_transports": []},
            "Test Free Delivery Edit Name",
            True,
        )
        transport2 = integration.makeTransport(
            True,
            "Test Courier Edit Description",
            "Test Free Delivery Edit Excerpt",
            "courier",
            {},
            "Test Courier Edit Name",
            True,
        )
        transport3 = integration.makeTransport(
            True,
            "Test Pickup Edit Description",
            "Test Pickup Edit Excerpt",
            "pickup",
            {},
            "Test Pickup Edit Name",
            True,
        )
        dbsession.add_all([transport1, transport2, transport3])
        dbsession.flush()
        self._addRoutes(dummy_config)
        dummy_request.method = "POST"
        dummy_request.POST = MultiDict(
            [
                ("form.submitted", True),
                ("name", "Test Free Delivery Updated Name"),
                ("description", "Test Free Delivery Updated Description"),
                ("excerpt", "Test Free Delivery Updated Excerpt"),
                ("module_name", transport1.module_name),
                ("status", True),
                ("special_method", False),
                ("removed_transports", transport2.transport_id),
                ("removed_transports", transport3.transport_id),
            ]
        )
        integration.setUser(dummy_config, user)
        dummy_request.context = self._makeContext()
        response = self._callFUT(
            dummy_request, "free_delivery", transport1.transport_id
        )
        assert response.location == "transport_list"
        new_transport = (
            dbsession.query(models.Transport)
            .filter_by(transport_id=transport1.transport_id)
            .one()
        )
        assert new_transport.special_method == False
        assert new_transport.description == "Test Free Delivery Updated Description"
        assert new_transport.excerpt == "Test Free Delivery Updated Excerpt"
        assert new_transport.name == "Test Free Delivery Updated Name"
        assert new_transport.status == True
        assert new_transport.module_settings == {
            "removed_transports": [transport2.transport_id, transport3.transport_id]
        }

    def test_edit_transport_and_slot_free_delivery(
        self, dummy_config, dummy_request, dbsession
    ):
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        transport = integration.makeTransport(
            True,
            "Test Free Delivery Edit Description",
            "Test Free Delivery Edit Excerpt",
            "free_delivery",
            {},
            "Test Free Delivery Edit Name",
            True,
        )
        dbsession.add_all([setting_1, setting_2, setting_3, transport, user])
        dbsession.flush()
        transport_slot_1 = integration.makeTransportSlot(
            "Test Slot Free Delivery Description",
            "Test Slot Free Delivery Excerpt",
            # "full_package",
            {"min_units": 10},
            "Test Slot Free Delivery Edit Name",
            True,
            transport.transport_id,
        )
        dbsession.add_all([transport_slot_1])
        dbsession.flush()
        self._addRoutes(dummy_config)
        dummy_request.method = "POST"
        slot_1 = transport_slot_1.transport_slot_id
        dummy_request.POST = MultiDict(
            [
                ("special_method", False),
                ("description", "Test Free Delivery Updated Description"),
                ("excerpt", "Test Free Delivery Updated Excerpt"),
                ("form.submitted", True),
                ("module_name", "free_delivery"),
                ("name", "Test Free Delivery Updated Name"),
                ("status", True),
                ("transport_slot_id", slot_1),
                (f"slot_{slot_1}_name", "Slot 1 updated name"),
                (f"slot_{slot_1}_status", True),
                (f"slot_{slot_1}_visibility", True),
                (f"slot_{slot_1}_min_value", 40),
                (f"slot_{slot_1}_applicability", 2),
            ]
        )
        dummy_request.context = self._makeContext()
        integration.setUser(dummy_config, user)
        response = self._callFUT(dummy_request, "free_delivery", transport.transport_id)
        assert response.location == "transport_list"
        transport = (
            dbsession.query(models.Transport)
            .filter_by(transport_id=transport.transport_id)
            .one()
        )
        assert transport.special_method == False
        assert transport.description == "Test Free Delivery Updated Description"
        assert transport.excerpt == "Test Free Delivery Updated Excerpt"
        assert transport.name == "Test Free Delivery Updated Name"
        assert transport.status == True
        transport_slot = (
            dbsession.query(models.TransportSlot)
            .filter_by(transport_slot_id=transport_slot_1.transport_slot_id)
            .one()
        )
        assert transport_slot.name == "Slot 1 updated name"
        assert transport_slot.module_settings == {
            "min_value": 40,
        }

    def test_edit_transport_drop_slot_free_delivery(
        self, dummy_config, dummy_request, dbsession
    ):
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        transport = integration.makeTransport(
            True,
            "Test Free Delivery Edit Description",
            "Test Free Delivery Edit Excerpt",
            "free_delivery",
            {},
            "Test Free Delivery Edit Name",
            True,
        )
        dbsession.add_all([setting_1, setting_2, setting_3, transport, user])
        dbsession.flush()
        transport_slot_1 = integration.makeTransportSlot(
            "Test Slot Free Delivery Description",
            "Test Slot Free Delivery Excerpt",
            {"min_units": 10},
            "Test Slot Free Delivery Edit Name",
            True,
            transport.transport_id,
        )
        dbsession.add_all([transport_slot_1])
        dbsession.flush()
        self._addRoutes(dummy_config)
        dummy_request.method = "POST"
        slot_2 = transport_slot_1.transport_slot_id
        dummy_request.POST = MultiDict(
            [
                ("special_method", False),
                ("description", "Test Free Delivery Updated Description"),
                ("excerpt", "Test Free Delivery Updated Excerpt"),
                ("form.submitted", True),
                ("module_name", "free_delivery"),
                ("name", "Test Free Delivery Updated Name"),
                ("status", True),
            ]
        )
        dummy_request.context = self._makeContext()
        integration.setUser(dummy_config, user)
        response = self._callFUT(dummy_request, "free_delivery", transport.transport_id)
        assert response.location == "transport_list"
        transport_slot = (
            dbsession.query(models.TransportSlot)
            .filter_by(transport_slot_id=slot_2)
            .first()
        )
        assert transport_slot == None
