from pyramid.testing import DummySecurityPolicy
from webob.multidict import MultiDict

from emporium import models

from tests import integration


class Test_transport_courier:
    def _callFUT(self, request, module_name, module_id):
        from emporium.views.transport import transport_edit

        request.matchdict["module_name"] = module_name
        request.matchdict["module_id"] = module_id
        request.referer = "/emporatorium/transport_list"
        return transport_edit(request)

    def _makeContext(self):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route(
            "transport_action", "/emporatorium/transport_{module_name}_{module_id}"
        )
        # config.add_route("s_transport", "/blog/{transportname}-{id}")

    def test_edit_transport_and_slot_courier(
        self, dummy_config, dummy_request, dbsession
    ):
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        transport = integration.makeTransport(
            True,
            "Test Courier Edit Description",
            "Test Courier Edit Excerpt",
            "courier",
            {},
            "Test Courier Edit Name",
            True,
        )
        dbsession.add_all([setting_1, setting_2, setting_3, transport, user])
        dbsession.flush()
        transport_slot_1 = integration.makeTransportSlot(
            "Test Slot Courier Description",
            "Test Slot Courier Excerpt",
            # "full_package",
            {"min_units": 10},
            "Test Slot Courier Edit Name",
            True,
            transport.transport_id,
        )
        dbsession.add_all([transport_slot_1])
        dbsession.flush()
        self._addRoutes(dummy_config)
        dummy_request.method = "POST"
        slot_1 = transport_slot_1.transport_slot_id
        dummy_request.POST = MultiDict(
            [
                ("special_method", False),
                ("description", "Test Courier Updated Description"),
                ("excerpt", "Test Courier Updated Excerpt"),
                ("form.submitted", True),
                ("module_name", "courier"),
                ("name", "Test Courier Updated Name"),
                ("status", True),
                ("transport_slot_id", slot_1),
                (f"slot_{slot_1}_name", "Slot 1 updated name"),
                (f"slot_{slot_1}_status", True),
                (f"slot_{slot_1}_visibility", True),
                (f"slot_{slot_1}_unit_weight", "1000"),
                (f"slot_{slot_1}_unit_fee", "200"),
                (f"slot_{slot_1}_manipulation_fee", 100),
                (f"slot_{slot_1}_applicability", 2),
            ]
        )
        dummy_request.context = self._makeContext()
        integration.setUser(dummy_config, user)
        response = self._callFUT(dummy_request, "courier", transport.transport_id)
        assert response.location == "transport_list"
        transport = (
            dbsession.query(models.Transport)
            .filter_by(transport_id=transport.transport_id)
            .one()
        )
        assert transport.special_method == False
        assert transport.description == "Test Courier Updated Description"
        assert transport.excerpt == "Test Courier Updated Excerpt"
        assert transport.name == "Test Courier Updated Name"
        assert transport.status == True
        transport_slot = (
            dbsession.query(models.TransportSlot)
            .filter_by(transport_slot_id=transport_slot_1.transport_slot_id)
            .one()
        )
        assert transport_slot.name == "Slot 1 updated name"
        assert transport_slot.module_settings == {
            "unit_weight": "1000",
            "unit_fee": "200",
            "manipulation_fee": 100,
        }

    def test_edit_transport_drop_slot_courier(
        self, dummy_config, dummy_request, dbsession
    ):
        user = integration.makeUser("Jack", "Black", "editor", "password", "emperor")
        setting_1 = integration.makeSetting("cache", True, 0)
        setting_2 = integration.makeSetting("shop_status", True, 0)
        setting_3 = integration.makeSetting("name", "Emporatorium test name", 0)
        transport = integration.makeTransport(
            True,
            "Test Courier Edit Description",
            "Test Courier Edit Excerpt",
            "courier",
            {},
            "Test Courier Edit Name",
            True,
        )
        dbsession.add_all([setting_1, setting_2, setting_3, transport, user])
        dbsession.flush()
        transport_slot_1 = integration.makeTransportSlot(
            "Test Slot Courier Description",
            "Test Slot Courier Excerpt",
            {"min_units": 10},
            "Test Slot Courier Edit Name",
            True,
            transport.transport_id,
        )
        dbsession.add_all([transport_slot_1])
        dbsession.flush()
        self._addRoutes(dummy_config)
        dummy_request.method = "POST"
        slot_2 = transport_slot_1.transport_slot_id
        dummy_request.POST = MultiDict(
            [
                ("special_method", False),
                ("description", "Test Courier Updated Description"),
                ("excerpt", "Test Courier Updated Excerpt"),
                ("form.submitted", True),
                ("module_name", "courier"),
                ("name", "Test Courier Updated Name"),
                ("status", True),
            ]
        )
        dummy_request.context = self._makeContext()
        integration.setUser(dummy_config, user)
        response = self._callFUT(dummy_request, "courier", transport.transport_id)
        assert response.location == "transport_list"
        transport_slot = (
            dbsession.query(models.TransportSlot)
            .filter_by(transport_slot_id=slot_2)
            .first()
        )
        assert transport_slot == None
