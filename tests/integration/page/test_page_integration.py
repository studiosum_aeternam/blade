from pyramid.testing import DummySecurityPolicy
from webob.multidict import MultiDict

from emporium import models


def makeUser(first_name, last_name, login, password, role):
    user = models.User(
        first_name=first_name, last_name=last_name, login=login, role=role
    )
    user.set_password(password)
    return user


def setUser(config, user):
    config.set_security_policy(DummySecurityPolicy(identity=user))


def makePage(content, content_extended, status, title, creator_id=None):
    return models.Page(
        content=content,
        content_extended=content_extended,
        status=status,
        title=title,
        creator_id=creator_id,
    )


def makeSetting(key, value, setting_type):
    return models.Setting(key=key, value=value, setting_type=setting_type)


class Test_view_page:
    def _callFUT(self, request, pagename):
        from emporium.views.page import view_page

        request.matchdict["pagename"] = pagename
        request.referer = "/emporatorium/page_list"
        return view_page(request)

    def _makeContext(self, page):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory
        
    def _addRoutes(self, config):
        config.add_route("page_action", "/emporatorium/page_{action}")
        config.add_route("s_page", "/page/{pagename}")

    def test_it(self, dummy_config, dummy_request, dbsession):
        # add a page to the db
        setting_1 = makeSetting("cache", True, 0)
        setting_2 = makeSetting("shop_status", True, 0)
        setting_3 = makeSetting("name", "Emporatorium test name", 0)
        user = makeUser("Jack", "Black", "editor", "password", "emperor")
        dbsession.add_all([setting_1, setting_2, setting_3, user])
        dbsession.flush()
        page = makePage(
            "Hello CruelWorld IDoExist",
            "Hello CruelWorld Extended Content",
            1,
            "IDoExist",
            user.id
        )
        dbsession.add_all([page])
        dbsession.flush()
        # create a request asking for the page we've created
        self._addRoutes(dummy_config)
        dummy_request.context = self._makeContext(page)
        setUser(dummy_config, user)
        # call the view we're testing and check its behavior
        info = self._callFUT(dummy_request, page.title)
        assert info["page"] is page
        assert info["page"].content == "Hello CruelWorld IDoExist"
        assert info["page"].content_extended == "Hello CruelWorld Extended Content"
        assert info["page"].title == "IDoExist"
        assert info["page"].creator_id == user.id
        assert info["page"].status == True


class Test_add_page:
    def _callFUT(self, request, page_id=None):
        from emporium.views.page import page_edit

        request.matchdict["action"] = "create"
        request.params["page_id"] = page_id
        request.referer = "/emporatorium/page_list"
        return page_edit(request)

    def _makeContext(self):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route("page_action", "/emporatorium/page_{action}")
        config.add_route("s_page", "/page/{pagename}")

    def test_submit_works(self, dummy_config, dummy_request, dbsession):
        user = makeUser("Jack", "Black", "editor", "password", "emperor")
        setting_1 = makeSetting("cache", True, 0)
        setting_2 = makeSetting("shop_status", True, 0)
        setting_3 = makeSetting("name", "Emporatorium test name", 0)
        dbsession.add_all([setting_1, setting_2, setting_3, user])
        dbsession.flush()
        dummy_request.method = "POST"
        dummy_request.POST = MultiDict(
            {
                "form.submitted": True,
                "content": "CruelWorld Exists",
                "content_extended": "CruelWorld Extended Pain",
                "status": 1,
                "title": "AnotherPage",
            }
        )
        dummy_request.context = self._makeContext()
        setUser(dummy_config, user)
        dummy_config.testing_securitypolicy(userid=dummy_request.identity.id)
        self._addRoutes(dummy_config)
        self._callFUT(dummy_request, None)
        page = dbsession.query(models.Page).filter_by(title="AnotherPage").one()
        assert page.content_extended == "CruelWorld Extended Pain"


class Test_page_edit:
    def _callFUT(self, request, action, page_id):
        from emporium.views.page import page_edit

        request.matchdict["action"] = action
        request.params["page_id"] = page_id
        request.referer = "/emporatorium/page_list"
        return page_edit(request)

    def _makeContext(self):
        from emporium.routes import EmporiumAdminFactory

        return EmporiumAdminFactory

    def _addRoutes(self, config):
        config.add_route("page_action", "/emporatorium/page_{action}")
        config.add_route("s_page", "/page/{pagename}")

    def test_add(self, dummy_config, dummy_request, dbsession):
        setting_1 = makeSetting("cache", True, 0)
        setting_2 = makeSetting("shop_status", True, 0)
        setting_3 = makeSetting("name", "Emporatorium test name", 0)
        user = makeUser("Jack", "Black", "editor", "password", "emperor")
        dbsession.add_all([setting_1, setting_2, setting_3, user])
        dbsession.flush()
        dummy_request.context = self._makeContext()
        dummy_request.method = "POST"
        dummy_request.POST = MultiDict(
            {
                "form.submitted": True,
                "content": "Hello CruelWorld IDoExist",
                "content_extended": "Hello CruelWorld Extended Content",
                "title": "IDoExist(sortof)",
                "status": 1,
            }
        )
        setUser(dummy_config, user)
        dummy_config.testing_securitypolicy(userid=dummy_request.identity.id)
        self._addRoutes(dummy_config)
        self._callFUT(dummy_request, "create", None)
        page = dbsession.query(models.Page).filter_by(title="IDoExist(sortof)").one()
        assert page.content == "Hello CruelWorld IDoExist"

    def test_edit(self, dummy_config, dummy_request, dbsession):

        user = makeUser("Jack", "Black", "editor", "password", "emperor")
        setting_1 = makeSetting("cache", True, 0)
        setting_2 = makeSetting("shop_status", True, 0)
        setting_3 = makeSetting("name", "Emporatorium test name", 0)
        dbsession.add_all([setting_1, setting_2, setting_3, user])
        dbsession.flush()
        page = makePage(
            "Hello CruelWorld IDoExist",
            "Hello CruelWorld Extended Content",
            1,
            "IDoExist",
            user.id,
        )
        dbsession.add_all([page])
        dbsession.flush()
        self._addRoutes(dummy_config)
        dummy_request.method = "POST"
        dummy_request.POST = MultiDict(
            {
                "form.submitted": True,
                "content": "Hello yo!",
                "content_extended": page.content_extended,
                "page_id": page.page_id,
                "title": "Totally new title it is",
                "status": page.status,
            }
        )
        setUser(dummy_config, user)
        dummy_request.context = self._makeContext()
        response = self._callFUT(dummy_request, "edit", page.page_id)
        assert response.location == "page_list"
        page = (
            dbsession.query(models.Page)
            .filter_by(title="Totally new title it is")
            .one()
        )
        assert page.content == "Hello yo!"
        assert page.content_extended == "Hello CruelWorld Extended Content"
