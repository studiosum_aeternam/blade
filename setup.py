import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

os.environ["PYTHON_EGG_CACHE"] = "/tmp"

requires = [
  'plaster_pastedeploy',
  'Babel', 
  'SQLAlchemy',
  'bcrypt', 
  'beaker', 
  'cryptography',
  'cssutils',
  'dogpile.cache',
  'email_validator',
  'hashids', 
  'lxml',
  'paginate_sqlalchemy', 
  'Paste',
  'pdfkit',
  'pillow-simd',
  'psycopg2',
  'pyramid', 
  'pyramid_debugtoolbar', 
  'pyramid_debugtoolbar_dogpile',
  'pyramid_exclog',
  'pyramid_dogpile_cache', 
  'pyramid_forksafe',
  'pyramid_jinja2',
  'pyramid_mailer', 
  'pyramid_session_redis',
  'pyramid_retry', 
  'pyramid_tm', 
  'pyramid_webassets', 
  'pytest',
  'pytest-cov',
  'python-dateutil',
  'python-slugify[unidecode]',
  'unicode',
  'redis', 
  'requests', 
  'transaction', 
  'waitress', 
  'webhelpers2', 
  'webtest', 
  'wtforms', 
  'zope.sqlalchemy'
  ]


tests_require = [
    'WebTest',
    'pytest',
    'pytest-cov',
    ]

extras_require={
    'testing': tests_require,
  },

    
setup(name='emporium',
      version='2.3411',
      description='emporium',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Pyramid",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
      ],
      author='',
      author_email='',
      url='',
      keywords='web pyramid pylons',
      packages=find_packages(exclude=['tests']),
      include_package_data=True,
      zip_safe=False,
      extras_require={
        'testing': tests_require,
      },
      install_requires=requires,
        entry_points={
            'paste.app_factory': [
                'main = emporium:main',
            ],
            'console_scripts': [
                'initialize_pyramid_scaffold_db=emporium.scripts.initialize_db:main',
            ],
        },
      message_extractors = {'emporium': [
      ('**.py', 'python', None),
      ('templates/**.html', None),
      ('static/**', 'ignore', None)]},
      )



