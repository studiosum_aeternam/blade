Pyramid_Emporium README
==================

Proof of concept e-commerce application based on Pyramid+SQLAlchemy.

Requirements
---------------

- Postgresql

- Redis (for sessions management)


Getting Started
---------------

- import pyramid_emporium.sql to the pgsql db and adjust settings in the dev.ini file (the missing sql file is under way)

- cd <directory containing this file>

- $VENV/bin/pip install -e .

- $VENV/bin/initialize_emporium_db dev.ini

- $VENV/bin/pserve dev.ini


To do
---------------

- clean up the code (its really WET) - redundant code in the order phase (both front and back end)

- JS often hangs during cart contents refreshing action (also the JS library BigNumber.js is ancient and buggy) 

- during registration phase some JS actions are ommited (animations etc.)

- configuration settings sholud be editable (e.g. mail settings, wkhtmltopdf path) - right now they are hardcoded 

- focus on the one language needs to be addressed (view/sqlalchemy hardcoding of the shop main language)

- administration panel has no limit for unsuccessful logging in (no banning mechanism for brute-force type attacks)

- and the most important - add tests :)