from dataclasses import dataclass, field


@dataclass(slots=True)
class DataOptions:
    attribute_dict: dict = field(default_factory=dict)
    attribute_values_dict: dict = field(default_factory=dict)
    availability_dict: dict = field(default_factory=dict)
    band_dict: dict = field(default_factory=dict)
    categories_dict: list = field(default_factory=list)
    category_tree: list = field(default_factory=list)
    collection_dict: list = field(default_factory=list)
    discount_list: list = field(default_factory=list)
    discounts: list = field(default_factory=list)
    featured_list: dict = field(default_factory=dict)
    file_name_display_mode_dict: list = field(default_factory=list)
    file_type_dict: list = field(default_factory=list)
    file_type_json: list = field(default_factory=list)
    footer_menu_dict: dict = field(default_factory=dict)
    header_menu_dict: dict = field(default_factory=dict)
    manufacturer_dict: list = field(default_factory=list)
    manufacturer_dict_supplemental: list = field(default_factory=list)
    number_products: int = 0
    promotion_list: list = field(default_factory=list)
    special_dict: dict = field(default_factory=dict)
    storage_dict: dict = field(default_factory=dict)
    storage_physical: dict = field(default_factory=dict)
    storage_virtual: dict = field(default_factory=dict)
    tax_dict: list = field(default_factory=list)
    template_dict: list = field(default_factory=list)
    transport_dict: list = field(default_factory=list)


@dataclass(slots=True)
class DataOptionsCart:
    attribute_dict: dict = field(default_factory=dict)
    availability_dict: dict = field(default_factory=dict)
    band_dict: dict = field(default_factory=dict)
    categories_dict: list = field(default_factory=list)
    country_dict: dict = field(default_factory=dict)
    footer_menu_dict: dict = field(default_factory=dict)
    header_menu_dict: dict = field(default_factory=dict)
    featured_list: dict = field(default_factory=dict)
    manufacturer_dict: list = field(default_factory=list)
    special_dict: dict = field(default_factory=dict)
    storage_dict: dict = field(default_factory=dict)
    storage_virtual: dict = field(default_factory=dict)
    tax_dict: list = field(default_factory=list)
    transport_dict: list = field(default_factory=list)
    payment_dict: list = field(default_factory=list)


@dataclass(slots=True)
class DataOptionsFiltered:
    attribute_filtered: dict = field(default_factory=dict)
    author_filtered: dict = field(default_factory=dict)
    band_filtered: dict = field(default_factory=dict)
    collection_filtered: dict = field(default_factory=dict)
    footer_menu_dict: dict = field(default_factory=dict)
    header_menu_dict: dict = field(default_factory=dict)
    payment_dict: dict = field(default_factory=dict)
    special_filtered: dict = field(default_factory=dict)
    special_filtered_dict: dict = field(default_factory=dict)
    storage_filtered: dict = field(default_factory=dict)
    transport_dict: dict = field(default_factory=dict)
    transport_filtered: dict = field(default_factory=dict)


@dataclass(slots=True)
class SingleProductDataOptions:
    additional_images_dict: dict = field(default_factory=dict)
    attached_document: list = field(default_factory=list)
    attached_music: list = field(default_factory=list)
    attached_music_json: list = field(default_factory=list)
    attached_video: list = field(default_factory=list)
    attribute_dict: dict = field(default_factory=dict)
    availability_dict: dict = field(default_factory=dict)
    band_dict: dict = field(default_factory=dict)
    categories_dict: dict = field(default_factory=dict)
    collection_dict: dict = field(default_factory=dict)
    footer_menu_dict: dict = field(default_factory=dict)
    header_menu_dict: dict = field(default_factory=dict)
    image_file_extensions: dict = field(default_factory=dict)
    main_category_id: int = None
    main_category_seo_slug: str = None
    manufacturer_dict: dict = field(default_factory=dict)
    product_categories_dict: dict = field(default_factory=dict)
    promotion_list: list = field(default_factory=list)
    related_attribute_dict: dict = field(default_factory=dict)
    related_product_dict: dict = field(default_factory=dict)
    special_dict: dict = field(default_factory=dict)
    storage_dict: dict = field(default_factory=dict)
    tax_dict: dict = field(default_factory=dict)
    transport_dict: dict = field(default_factory=dict)
