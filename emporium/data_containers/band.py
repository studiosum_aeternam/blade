from dataclasses import dataclass


@dataclass(slots=True)
class BandContainer:
    band_id: int
    band_type: int
    name: str
    seo_slug: str
    url: list
