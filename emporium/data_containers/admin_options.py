from dataclasses import dataclass, field


@dataclass(slots=True)
class AdminOptions:
    action: str = ""
    url_next: str = ""
    categories_list: list = field(default_factory=list)
