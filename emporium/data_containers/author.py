from dataclasses import dataclass


@dataclass(slots=True)
class AuthorContainer:
    author_id: int
    callsign: str
    first_name: str
    last_name: str
    order: int
    product_author_id: int
    role: int
    visibility: int
    url: str = ''
