from dataclasses import dataclass, field


@dataclass(slots=True)
class CategoryContainer:
    category_id: int
    name: str
    category_type: int = 0
    depth: int = 0
    description: str = None
    description_long: str = None
    meta_description: str = None
    parent_id: int = None
    placeholder: bool = False
    seo_slug: str = None
    u_h1: str = None
    u_title: str = None
    subcategories: list = field(default_factory=list)
