from dataclasses import dataclass, field


@dataclass(slots=True)
class MiscOptions:
    search_url: str = None
    rotate_collections: bool = False
    collections_list_shuffled: list = field(default_factory=list)
