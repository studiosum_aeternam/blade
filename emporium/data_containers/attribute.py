from dataclasses import dataclass, field


@dataclass(slots=True)
class AttributeGroupContainer:
    attribute_display_type_id: int
    attribute_group_id: int
    attribute_name: str
    name: str
    sort_order: int
    visible: bool
    attributes: list = field(default_factory=list)


@dataclass(slots=True)
class AttributeContainer:
    attribute_id: int
    value: str
    url: list

@dataclass(slots=True)
class AttributeContainerMapping:
    attribute_id: int
    value: str
    url: list
    attribute_settings: dict = field(default_factory=dict)



@dataclass(slots=True)
class AdminAttributeGroupContainer:
    attribute_group_id: int
    name: str
    sort_order: int
    attribute_display_type_id: int
    attributes: list = field(default_factory=list)


@dataclass(slots=True)
class AdminAttributeContainer:
    attribute_id: int
    value: str
    selected: str
