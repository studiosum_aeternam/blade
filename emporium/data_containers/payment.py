from dataclasses import dataclass, field


@dataclass(slots=True)
class PaymentContainer:
    description: str
    # excerpt: str
    module_name: str
    module_settings: dict
    name: str
    payment_id: int
    type: int


@dataclass(slots=True)
class PaymentCalculatedContainer:
    payment_id: int
    payment_fee: float = 0
