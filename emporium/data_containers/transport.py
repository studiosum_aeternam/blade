from dataclasses import dataclass, field


@dataclass(slots=True)
class TransportContainer:
    description: str
    excerpt: str
    module_name: str
    module_settings: dict
    module_name: str
    name: str
    transport_id: int
    transport_method: int
    slots: dict = field(default_factory=dict)
    unit_name: str = ""


@dataclass(slots=True)
class TransportSlotContainer:
    slot_applicability: int
    slot_description: str
    slot_excerpt: str
    slot_manipulation_fee: str
    slot_name: str
    slot_order: int
    slot_special_method: bool
    slot_unit_fee: str
    slot_unit_name: str
    transport_id: int
    transport_slot_id: int
    transport_method: int
    slot_settings: dict = field(default_factory=dict)
    number_of_products_using_slot: int = 0


@dataclass(slots=True)
class ProductTransportContainer:
    name: str
    module_name: str
    module_description: str
    product_id: int
    product_transport_id: int
    transport_id: int
    transport_slot_id: int
    local_settings: dict = field(default_factory=dict)
    module_settings: dict = field(default_factory=dict)


@dataclass(slots=True)
class TransportCalculatedContainer:
    transport_id: int
    transport_fee: float = 0


@dataclass(slots=True)
class TransportSlotSettingsContainer:
    manipulation_fee: str = None
    min_value: str = None
    removed_slots: list = field(default_factory=list)
    removed_transports: list = field(default_factory=list)
    unit_fee: str = None
    unit_weight: str = None
