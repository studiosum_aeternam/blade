from decimal import Decimal

from dataclasses import dataclass, field


@dataclass(slots=True)
class ProductContainerSimplified:
    product_id: int
    image: str
    name: str
    price: float
    slug: str
    special_category_id: int
    special_price: float
    square_meter: str


@dataclass(slots=True)
class ProductContainer:
    ancient_special_price: float
    availability_id: int
    calculated_price: float
    catalog_price: float
    discount_id: int
    ean: str
    file_path: str
    image: str
    manual_discount: int
    manufactured: int
    manufacturer: str
    manufacturer_id: int
    meta_description: str
    meta_title: str
    minimum: float
    model: str
    name: str
    pieces: int
    price: float
    price_net: float
    product_id: int
    product_in_cart: str
    cart_quantity_real_unit: str
    cart_quantity: str
    product_type: int
    slug: str
    special_id: int
    special_price: float
    square_meter: int
    status: int
    unit: float
    # physical_delivery_method: int
    # digital_delivery_method: int
    height: float
    length: float
    width: float
    setting_template_id: int
    availability: list = field(default_factory=list)
    band: list = field(default_factory=list)
    collection: list = field(default_factory=list)
    date_available: str = None
    description: str = ""
    description_technical: str = ""
    display_storage_type: int = 0
    minimum_unit: float = "1"
    not_promoted_storage_summary: int = 0
    sku: str = ""
    special_transport: dict = field(default_factory=dict)
    storage_current: dict = field(default_factory=dict)
    storage_summary: int = 0
    storage_summary_real_unit: int = 0
    tax_id: int = None
    weight: float = 1


@dataclass(slots=True)
class ProductInCartContainer:
    ancient_special_price: float
    availability_id: int
    calculated_price: float
    catalog_price: float
    discount_id: int
    ean: str
    image: str
    manual_discount: int
    manufactured: int
    manufacturer_id: int
    minimum: float
    model: str
    name: str
    one_batch: int
    pieces: int
    price: float
    price_net: float
    product_id: int
    product_type: int
    slug: str
    square_meter: int
    status: int
    transport_method: int  # physical 0/digital 1/mixed 2
    unit: float
    unit_tax: str
    availability: list = field(default_factory=list)
    band: list = field(default_factory=list)
    cart_quantity: str = Decimal("1")
    cart_quantity_real_unit: str = Decimal("1")
    cart_value_detailed: dict = field(default_factory=dict)
    collection: list = field(default_factory=list)
    description: str = ""
    description_technical: str = ""
    minimum_unit: float = "1"
    not_promoted_storage_summary: int = 0
    product_single_set: str = "false"
    sku: str = ""
    special_filtered_dict: dict = field(default_factory=dict)
    special_transport: dict = field(default_factory=dict)
    storage_current: dict = field(default_factory=dict)
    storage_summary: int = 0
    storage_summary_real_unit: int = 0
    tax_id: int = None
    tax_value: str = Decimal("0")
    value: str = Decimal("0")
    weight: float = 1


@dataclass(slots=True)
class ProductContainerZIP:
    availability_id: int
    ancient_special_price: float
    calculated_price: float
    catalog_price: float
    discount_id: int
    ean: str
    image: str
    manufactured: int
    manufacturer_id: int
    minimum: float
    model: str
    name: str
    pieces: int
    price: float
    price_net: float
    product_id: int
    product_type: int
    slug: str
    special_price: float
    square_meter: int
    status: int
    unit: float
    # physical_delivery_method: int
    # digital_delivery_method: int
    band: list = field(default_factory=list)
    collection: list = field(default_factory=list)
    minimum_unit: float = "1"
    product_to_product_relation_settings: dict = field(default_factory=dict)
    purchase_price: float = None
    sku: str = ""
    special_transport: dict = field(default_factory=dict)
    storage_current: dict = field(default_factory=dict)
    tax_id: int = None
    weight: float = 1


@dataclass(slots=True)
class ProductDimensionContainer:
    name: str
    value: str
    url: str


@dataclass(slots=True)
class ProductStorageContainer:
    product_storage_id: int
    storage_quantity: int
    storage_quantity_real_unit: float
    storage_id: int


@dataclass(slots=True)
class ProductCollectionContainer:
    collection_id: int


@dataclass(slots=True)
class ProductBandContainer:
    band_id: int
    product_band_id: int
    seo_slug: str
    name: str


@dataclass(slots=True)
class ProductToSpecialContainer:
    # order: int
    price: str
    product_special_id: int
    quantity: int
    special_id: int
    storage_aware: bool
    tax_id: int
