from dataclasses import dataclass, field


@dataclass(slots=True)
class MetaOptions:
    description: str = ""
    description_long: str = ""
    item_count: int = 0
    meta_description: str = None
    rel_link: str = None
    robots_meta: str = None
    seo_slug: str = ""
    title: str = ""
    u_h1: str = ""
