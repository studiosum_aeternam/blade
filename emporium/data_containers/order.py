from dataclasses import dataclass
from .json_hint import JSON

@dataclass(slots=True)
class OrderContainer:
    date_ordered: str
    last_update: str
    order_id: int
    personal_data: JSON
    status: int
    summary_cost: str
    telephone: str
    uuid: str