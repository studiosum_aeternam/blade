from dataclasses import dataclass, field


@dataclass(slots=True)
class BaseContainer:
    # collections: str
    additional_img: str
    availability: str
    base_category_name: str
    base_measure: int
    brand: str
    categories: str
    color: str
    description: str
    link: str
    main_image: str
    measurement_unit: str
    minimum: str
    name: str
    pattern: str
    price2: str
    price: str
    product_id: str
    product_type: str
    room: str
    square_meter: str
    usage: str
    verify_product_id: str
    weight: str


@dataclass(slots=True)
class CeneoContainer:
    availability: str
    brand: str
    base_category_name: str
    description: str
    ean: str
    main_image: str
    name: str
    price: str
    product_id: str
    url: str
