from dataclasses import dataclass, field
from datetime import datetime


@dataclass(slots=True)
class FilterOptions:
    active_only: bool = True
    attribute_list_checked: bool = False
    attributes: list = field(default_factory=list)
    availability: list = field(default_factory=list)
    band: list = field(default_factory=list)
    band_list_checked: bool = False
    category_first_page: bool = False
    category_id: int = None
    collection: list = field(default_factory=list)
    collection_list_checked: bool = False
    descripted_front_page: bool = False
    description: str = ""
    dimensions: dict = field(default_factory=dict)
    dimensions_checked: bool = False
    display_mode: str = "list"
    filter_params: list = field(default_factory=list)
    first_page: bool = False
    items_per_page: int = 36
    language_id: int = 2
    length_max: float = None
    length_min: float = None
    manual_discount: bool = False
    manufacturer: set = field(default_factory=set)
    manufacturer_checked: bool = False
    page: int = 1
    pager: dict = field(default_factory=dict)
    price_filter: bool = False
    price_max: float = None
    price_max_url: list = field(default_factory=list)
    price_min: float = None
    price_min_url: list = field(default_factory=list)
    searchstring: str = ""
    sort_method: str = "name_asc"
    special_product: bool = False
    status: int = 1
    sticky: bool = False
    switch_mode: str = "grid"
    term: str = ""
    term_url: list = field(default_factory=list)
    today: str = datetime.now().strftime("%Y-%m-%d")
    width_max: float = None
    width_min: float = None
