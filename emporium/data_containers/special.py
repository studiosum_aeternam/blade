from dataclasses import dataclass


@dataclass(slots=True)
class SpecialContainer:
    special_id: int
    name: str
    name_displayed: str
    url: list
