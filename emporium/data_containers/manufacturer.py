from dataclasses import dataclass


@dataclass(slots=True)
class ManufacturerContainer:
    manufacturer_id: int
    manufacturer_type: int
    name: str
    seo_slug: str
    status: int
    url: list
