from dataclasses import dataclass, field


@dataclass(slots=True)
class TaxContainer:
    tax_id: int
    value: str
