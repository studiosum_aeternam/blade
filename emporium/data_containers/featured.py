from dataclasses import dataclass


@dataclass(slots=True)
class FeaturedContainer:
    special_id: int
    date_end: str
    date_start: str
    price: float
    quantity: int
