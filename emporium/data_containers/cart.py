from dataclasses import dataclass, field


@dataclass(slots=True)
class CartOptions:
    product_in_cart: bool = False
    cart_quantity: int = None
    cart_quantity_real_unit: str = None
    transport_fee: int = None


@dataclass(slots=True)
class CartDataOptions:
    band_filtered: dict = field(default_factory=dict)
    collection_filtered: dict = field(default_factory=dict)
    payment_filtered: dict = field(default_factory=dict)
    special_filtered: dict = field(default_factory=dict)
    special_filtered_dict: dict = field(default_factory=dict)
    storage_filtered: dict = field(default_factory=dict)
    transport_filtered: dict = field(default_factory=dict)


@dataclass(slots=True)
class CartProducts:
    cart_quantity_real_unit: str
    product_single_set: str


@dataclass(slots=True)
class CartUnitProducts:
    cart_quantity: str
    cart_quantity_real_unit: str
