from dataclasses import dataclass


@dataclass(slots=True)
class StorageContainer:
    storage_id: int
    name: str
    url: list
    storage_type: int
