from dataclasses import dataclass


@dataclass(slots=True)
class CollectionContainer:
    collection_id: int
    name: str
    url: list = ""
    manufacturer: str = ""


@dataclass(slots=True)
class CollectionContainerWithProducts:
    collection_id: int
    manufacturer_id: int
    name: str
    product_list: list = ""
