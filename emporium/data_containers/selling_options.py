from dataclasses import dataclass, field


@dataclass(slots=True)
class SellingOptions:
    containing_product_sets: list = field(default_factory=list)
    featured_products: dict = field(default_factory=dict)
    product_options: list = field(default_factory=list)
    products_common_band: dict = field(default_factory=dict)
    products_common_collections: dict = field(default_factory=dict)
    related_products: list = field(default_factory=list)
    similar_collections: list = field(default_factory=list)
    similar_products: list = field(default_factory=list)
