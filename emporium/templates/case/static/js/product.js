var default_meters = new BigNumber(product_json.quantity);
var minVal = product_json.minimum;

function changeValue(e, sign) {
  e.preventDefault();
  var fieldName = $(e.target).data("field");
  var parent = $(e.target).closest("div");
  var step = parseFloat($("input[name=" + fieldName + "]").attr("step"));
  var currentVal = parseFloat(
    parent.find("input[name=" + fieldName + "]").val(),
    10
  );

  if (!isNaN(currentVal) && currentVal > 0) {
    if (sign == "plus") {
      new_value = currentVal + step;
    } else {
      new_value = currentVal - step;
    }
    if (fieldName == "pack") {
      new_value = new_value.toFixed(0);
    } else {
      new_value = new_value.toFixed(2);
    }
    parent.find("input[name=" + fieldName + "]").val(new_value);
  } else {
    parent.find("input[name=" + fieldName + "]").val(step);
  }
  if (fieldName == "pack") {
    packChange(parent.find("input[name=" + fieldName + "]").val());
  } else {
    meterChange(parent.find("input[name=" + fieldName + "]").val());
  }
}

$(".cart-input-group").on("click", ".button-plus", function (e) {
  changeValue(e, "plus");
});

$(".cart-input-group").on("click", ".button-minus", function (e) {
  changeValue(e, "minus");
});

function packChange(e) {
  $(e * 1);
  if (e < minVal) {
    e = minVal;
    document.querySelector('input[name="pack"]').value = minVal;
  }
  var quantity = default_meters.multipliedBy(e);
  if (document.querySelector('input[name="meters"]')) {
    document.querySelector('input[name="meters"]').value = quantity;
  }
  document.querySelector('input[name="quantity"]').value = quantity;
}

function meterChange(e) {
  var meters = new BigNumber(parseFloat(e));
  var pack = meters.dividedBy(default_meters).valueOf();
  if (pack < minVal) {
    pack = minVal;
    document.querySelector('input[name="meters"]') = default_meters.multipliedBy(pack);
  }
  document.querySelector('input[name="pack"]').value = pack;
  // document.querySelector("#product_packages").value = pack;
  document.querySelector('input[name="quantity"]').value = meters;
}

let meters_input = document.querySelector('input[name="meters"]');
let pack_input = document.querySelector('input[name="pack"]');

if (meters_input) {
  meters_input.oninput = handleMeterInput;
  meters_input.onchange = handleMeterInput;
}

pack_input.oninput = handlePackInput;
pack_input.onchange = handlePackInput;
var delayTimer;

function handleMeterInput(e) {
  var meters = new BigNumber($(e.target).val().replace(",", "."));
  var pack = new BigNumber(0);
  var quantity = new BigNumber(0);
  clearTimeout(delayTimer);
  delayTimer = setTimeout(function () {
    pack = Math.ceil(parseFloat(meters.dividedBy(default_meters).valueOf()));
    if (pack < minVal) {
      pack = minVal;
    }
    var quantity = default_meters.multipliedBy(pack);
    document.querySelector('input[name="pack"]').value = pack;
    document.querySelector('input[name="meters"]').value = quantity;
    document.querySelector('input[name="quantity"]').valuse = quantity;
  }, 665);
}

function handlePackInput(e) {
  var pack =  Math.floor(parseInt($(e.target).val()));
  // e.value = pack * 1;
  clearTimeout(delayTimer);
  delayTimer = setTimeout(function () {
    if (pack < minVal) {
      pack = minVal;
    }
    
    var quantity = default_meters.multipliedBy(pack);
    document.querySelector('input[name="pack"]').value = pack;
    if (document.querySelector('input[name="meters"]')) {
      document.querySelector('input[name="meters"]').value = quantity;
    };
    document.querySelector('input[name="quantity"]').value= quantity;
    // $("#product_packages").val(pack.valueOf());
  }, 665);
}

function addToCart(product_id) {
  // var ean =  document.getElementById("product_ean_" + product_id).value;
  
  var quantity =  document.getElementById("product_quantity_" + product_id).value;
  
  // var packages =  document.getElementById("product_packages_" + product_id).value;

  updateCart(product_id, quantity);
    
  document.getElementById("shopping_cart_quantity").innerHTML = "+" + packages;
  var update_cart_class_list = document.getElementById("updated_cart").classList;
  update_cart_class_list.remove("hidden", "opacity-0");
  update_cart_class_list.add("opacity-100");
  
  var element = document.getElementById("in_cart");
  if (typeof element != "undefined" && element != null) {
    document.getElementById("in_cart").classList.add("hidden");
  }

  trackCart(
    product_id, 
    document.getElementById("product_name_" + product_id).textContent, 
    document.getElementById("product_quantity_" + product_id).value,  
    document.getElementById('product_manufacturer_' + product_id).innerHTML, 
    (document.getElementById('product_price_' + product_id).innerHTML*document.getElementById("product_quantity_" + product_id).value).toFixed(2), 
    document.getElementById("currency").innerHTML)
  ;
  setTimeout(function () {
    location.reload();
  }, 555);
}


