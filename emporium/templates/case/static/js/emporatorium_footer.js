var token = _("csrf_token").value;
 
document.querySelector('.remove_product_cookies').addEventListener('click', function (event) {
  event.preventDefault();
  removeProductEditCookies();
  var url = event.target.href;
  window.open(url, '_self');
});

//Modal

function toggleModal () {
  const body = document.querySelector('body')
  const modal = document.querySelector('.modal')
  modal.classList.toggle('opacity-0')
  modal.classList.toggle('pointer-events-none')
  body.classList.toggle('modal-active')
}  


var openmodal = document.querySelectorAll('.modal-open')
if (openmodal && openmodal.length > 0 ) {
  for (var i = 0; i < openmodal.length; i++) {
    openmodal[i].addEventListener('click', function(event){
      event.preventDefault()
      toggleModal()
    })
  }
}

var overlay = document.querySelector('.modal-overlay')
if (overlay && overlay.length > 0) {
  overlay.addEventListener('click', toggleModal)
}

var closemodal = document.querySelectorAll('.modal-close')
if (closemodal && closemodal.length > 0 ) {
  for (var i = 0; i < closemodal.length; i++) {
    closemodal[i].addEventListener('click', toggleModal)
  }
}

document.onkeydown = function(evt) {
  evt = evt || window.event
  var isEscape = false;
  if ("key" in evt) {
    isEscape = (evt.key === "Escape" || evt.key === "Esc");
  } else {
    isEscape = (evt.keyCode === 27);
  }
  if (isEscape && document.body.classList.contains('modal-active')) {
    toggleModal();
  }
};


function clearCache() {
  var formData = new FormData();
  var ajax = new XMLHttpRequest();
  ajax.open('POST', 'clear_cache');
  ajax.setRequestHeader('X-Requested-With','XMLHttpRequest');
  ajax.setRequestHeader('X-CSRF-Token', token);
  ajax.send(formData);
  ajax.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       location.reload();
    }
  }
}

