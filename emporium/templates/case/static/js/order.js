function togglePasswordVisibility() {
  var x = _("customer_password");
  if (x.type === "password") {
    x.type = "text";
    _("toggle_visibility").id =
      "toggle_visibility_reveal";
  } else {
    x.type = "password";
    _("toggle_visibility_reveal").id =
      "toggle_visibility";
  }
}

function togglePasswordVisibilityRegister() {
  var x = _("password");
  if (x.type === "password") {
    x.type = "text";
    _("toggle_visibility_register").id =
      "toggle_visibility_reveal_register";
  } else {
    x.type = "password";
    _("toggle_visibility_reveal_register").id =
      "toggle_visibility_register";
  }
}

function removeCookies() {
  var cookies, fLen, i;
  cookies = ["edit_customer", "order_type"];
  fLen = cookies.length;
  for (i = 0; i < fLen; i++) {
    Cookie.delete(cookies[i]);
  }
}

function clearForm() {
  removeCookies();
  var register = _("register");
  if (register != null) {
    register.reset();
  }
  var summary = _("order_summary");
  if (summary != null) {
    summary.reset();
  }
  location.reload();
}

function loginCustomer() {
  removeCookies();
}

var invoice_type = Cookie.get("invoice_type");
if (invoice_type) {
  invoiceTypeGet(invoice_type);
}

function invoiceTypeGet(invoice_type) {
  var elements = document.getElementsByClassName("company_field");
  var blocks = document.getElementsByClassName("company_block");
  var required = false;
  if (invoice_type == "company") {
    if (
      document.forms["order_summary"]["company_nip"].value == 0 ||
      document.forms["order_summary"]["company_name"].value == 0
    ) {
      _("customer_form").style.cssText =
        "visibility:visible !important";
    }

    for (var i = 0; i < blocks.length; i++) {
      blocks[i].classList.remove("hidden");
    }
    required = true;
  } else {
    for (var i = 0; i < blocks.length; i++) {
      blocks[i].classList.add("hidden");
    }
  }
  requiredLoop(elements, required);
}

function invoiceTypeSet(invoice_type) {
  Cookie.set("invoice_type", invoice_type, { SameSite: "strict", Path: "/" });
  invoiceTypeGet(invoice_type);
}

var delivery_details = Cookie.get("delivery_details");
if (delivery_details) {
  deliveryDetailsGet(delivery_details);
}

function deliveryDetailsGet(delivery_details) {
  var elements = document.getElementsByClassName("delivery_field");
  var required = false;
  if (delivery_details == "manual") {
    document
      .getElementById("delivery_address_block")
      .classList.remove("hidden");
    required = true;
  } else {
    _("delivery_address_block").classList.add("hidden");
  }
  requiredLoop(elements, required);
}

function deliveryDetailsSet(delivery_details) {
  Cookie.set("delivery_details", delivery_details, {
    SameSite: "strict",
    Path: "/",
  });
  deliveryDetailsGet(delivery_details);
}

function requiredLoop(elements, required) {
  for (var i = 0; i < elements.length; i++) {
    elements[i].required = required;
  }
}

function transportIdSet(transport_id) {
  // Cookie.set("invoice_type", invoice_type, { SameSite: "strict", Path: "/" });
  _('transport_id').value = transport_id;
}

var transport_methods = document.getElementsByClassName("delivery_method");

for (var i = 0; i < transport_methods.length; i++) {
  if (transport_methods[i].type === 'radio' && transport_methods[i].checked) {
    _('transport_id').value = transport_methods[i].dataset.transport_id;       
  }
}