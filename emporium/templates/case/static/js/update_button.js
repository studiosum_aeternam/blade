var buttons = document.querySelectorAll('.button-update');

buttons.forEach(button => {
  button.addEventListener('click', handleClick, false);
});

function handleClick(e) {
  var request = new XMLHttpRequest();
  var id = e.currentTarget.dataset.id;
  request.open('POST', url + [edit_url]);
  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  request.setRequestHeader('X-Requested-With','XMLHttpRequest');
  request.setRequestHeader('X-CSRF-Token', token);
  var post_params = [parameter_name] + id + "&product_quantity_"+ id + "=0"; 
  request.send(post_params);
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) 
    {
      if (document.getElementById("order_summary") && document.getElementById("cart_summary_block")) {
          document.getElementById("order_summary").classList.add('hidden');
          document.getElementById("order_summary_update").classList.remove('hidden');
          document.getElementById("cart_summary_block").classList.add('change_opacity_on_update');
      }
      
      window.setTimeout(function() { location.reload() }, 500)
    
    }
  };
}