const url = new URL(location.origin);

var add_buttons = document.querySelectorAll('.add_to_basket');

var edit_url = "cart_edit";
var parameter_name = "update_cart&product_id=";
var quantity_name = "product_quantity_";
var single_set_name = "product_single_set_";

add_buttons.forEach(button => {
  button.addEventListener('click', addToBasket, false);
});

var buttons_plus_minus = document.querySelectorAll('.button-plus-minus');


function addToBasket(e){
  var request = new XMLHttpRequest();
  var product_id = e.currentTarget.value;
  var manufacturer = _('product_manufacturer_' + product_id).innerHTML;
  var quantity = _('product_packages_' + product_id).value; // quantity is sq meter
  var price = _('product_price_' + product_id).value;
  var product_name = _('product_name_' + product_id).innerHTML;

  _("modal_title").innerHTML = product_name;
  _("modal_description").innerHTML = add_description;
  updateCart(product_id, quantity, true, false);
}

function updateCart(product_id, quantity, single_set=true, reload=true) {
    buttons_plus_minus.forEach(buton => {
        buton.setAttribute("disabled", "");
    });
    var request = new XMLHttpRequest();
    request.open('POST',  url + [edit_url]);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    request.setRequestHeader('X-Requested-With','XMLHttpRequest');
    request.setRequestHeader('X-CSRF-Token', token);
    var post_params = parameter_name + product_id + "&" + quantity_name + product_id + "=" + quantity;
    if (single_set == true) {
         post_params += "&" + single_set_name + product_id + "=true";
     }
    request.send(post_params);
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) 
        {


            if (_("order_summary") && _("cart_summary_block")) {
                _("order_summary").classList.add('hidden');
                _("order_summary_update").classList.remove('hidden');
                _("cart_summary_block").classList.add('change_opacity_on_update');


            }
            if (reload=true) { 
                window.setTimeout(function() {
                        location.reload()
                }, 1700)

            // window.location.reload();
        }
        _('shopping_cart_quantity').innerHTML= '+' + quantity;
        // _("updated_cart").classList.remove('hidden','opacity-0');
            // _("updated_cart").classList.add('opacity-100');   
        }
  };
}

