var action_url_compare ="compare_edit";
var compare_name = "update_compare&product_id=";

var compare_buttons = document.querySelectorAll('.add_to_compare');

compare_buttons.forEach(button => {
  button.addEventListener('click', addToCompare, false);
});

function addToCompare(e, reload=false){
  var request = new XMLHttpRequest();
  var product_id = e.currentTarget.value;
  var product_name = _('product_name_' + product_id).innerHTML;
  _("modal_title").innerHTML = product_name;
  _("modal_description").innerHTML = compare_description;
  request.open('POST',  url + [action_url_compare]);
  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  request.setRequestHeader('X-Requested-With','XMLHttpRequest');
  request.setRequestHeader('X-CSRF-Token', token);
  var post_params = compare_name + product_id;
  request.send(post_params);
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) 
      {
        if (reload==true) { 

            // window.setTimeout(function() {
            //         location.reload()
            // }, 1200)

            // window.location.reload();

        } else {
          _('compare_quantity').innerHTML= '+1';
          _('compared_' + product_id).classList.add("opacity-100");
          _('compared_' + product_id).classList.remove("opacity-0", "hidden");
          _('add_to_compare_' + product_id).classList.add("opacity-0", "hidden");
          _('add_to_compare_' + product_id).classList.remove("opacity-100");

        }

      }
  };
}
