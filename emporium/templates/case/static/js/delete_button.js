var buttons = document.querySelectorAll('.button-delete');
buttons.forEach(buton => {
  buton.addEventListener('click', handleClick, false);
});

function handleClick(e) {
  var remove = document.getElementById("remove_button");
  remove.dataset.id = e.currentTarget.dataset.id;
  remove.dataset.action_url= e.currentTarget.dataset.action_url;
  remove.dataset.parameter_name = e.currentTarget.dataset.parameter_name;
  remove.addEventListener("click", removeObject, false);
}

function removeObject(e) {
  var dataset = e.currentTarget.dataset;
  const id = dataset.id;
  const action_url = dataset.action_url;
  const parameter_name = dataset.parameter_name;
  var request = new XMLHttpRequest();
  request.open('POST', action_url);
  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  request.setRequestHeader('X-Requested-With','XMLHttpRequest');
  request.setRequestHeader('X-CSRF-Token', token);
  var post_params = parameter_name + '=' + id; 
  request.send(post_params);
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) 
    {
      window.location.reload()    
    }
  };
}