const colors = require('tailwindcss/colors')

// tailwind.config.js
module.exports = {
  // mode: 'jit',
  purge: {
    mode: 'layers',
    //preserveHtmlElements: true,
    enabled: true,
    content: [
       '../**/*.html',
       '../**/**/*.html',
       '../../../emporatorium/common/header.html',
       // '../../../../emporatorium/common/header.html',
    //  ./node_modules/flatpickr/**/*.js',
    ],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
    },
    extend: {
      screens: {
        '3xl': '1712px',
        '4xl': '2100px',
      },
      padding: ['hover'],
    },
    colors: {
            transparent: 'transparent',
            current: 'currentColor',
            amber: colors.amber,
            black: '#000',
            blue: colors.blue,
            blueGray: colors.blueGray,
            coolGray: colors.coolGray,
            cyan: colors.cyan,
            emerald: colors.emerald,
            fuchsia: colors.fuchsia,
            gray: colors.gray,
            green: colors.green,
            indigo: colors.indigo,
            sky: colors.sky,
            lime: colors.lime,
            orange: colors.orange,
            red: colors.red,
            rose: colors.rose,
            teal: colors.teal,
            trueGray: colors.trueGray,
            violet: colors.violet,
            warmGray: colors.warmGray,
            white: '#FFF',
            yellow: colors.yellow,
    }
  },
  variants: {
    textColor: ['hover', 'focus', 'group-hover', 'group-focus'],
    display: ['responsive', 'group-hover'],
  },
  
  // corePlugins: {
  //   container: false
  // },
  // plugins: [
  //   function ({ addComponents }) {
  //     addComponents({
  //       '.container': {
  //         maxWidth: '100%',
  //         '@screen sm': {
  //           maxWidth: '98%',
  //         },
  //         '@screen md': {
  //           maxWidth: '95%',
  //         },
  //         '@screen lg': {
  //           maxWidth: '95%',
  //         },
  //         '@screen xl': {
  //           maxWidth: '95%',
  //         },
  //         '@screen 2xl': {
  //           maxWidth: '1536px',
  //         },

  //       }
  //     })
  //   }
  // ]

}