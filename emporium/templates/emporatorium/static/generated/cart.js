const url = new URL(location.origin);

var add_buttons = document.querySelectorAll('.add_to_basket');

var edit_url = "cart_edit";
var parameter_name = "update_cart&product_id=";
var quantity_name = "product_quantity_";
var single_set_name = "product_single_set_";

add_buttons.forEach(button => {
  button.addEventListener('click', addToBasket, false);
});

function addToBasket(e){
  var request = new XMLHttpRequest();
  var product_id = e.currentTarget.value;
  var manufacturer = document.getElementById('product_manufacturer_' + product_id).innerHTML;
  var quantity = document.getElementById('product_packages_' + product_id).value; // quantity is sq meter
  var price = document.getElementById('product_price_' + product_id).value;
  var product_name = document.getElementById('product_name_' + product_id).innerHTML;

  document.getElementById("modal_title").innerHTML = document.getElementById('product_name_' + product_id).innerHTML;
  document.getElementById("modal_description").innerHTML = add_description;
  updateCart(product_id, quantity, true, false);
  trackCart(product_id, product_name, quantity, manufacturer, price);
}

function updateCart(product_id, quantity, single_set=true, reload=true) {

    var request = new XMLHttpRequest();
    // document.getElementById("modal_title").innerHTML = document.getElementById('product_name_' + product_id).innerHTML;
    // document.getElementById("modal_description").innerHTML = add_description;
    request.open('POST',  url + [edit_url]);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    request.setRequestHeader('X-Requested-With','XMLHttpRequest');
    request.setRequestHeader('X-CSRF-Token', token);
    var post_params = parameter_name + product_id + "&" + quantity_name + product_id + "=" + quantity;
    if (single_set == true) {
         post_params += "&" + single_set_name + product_id + "=true";
     }
    request.send(post_params);
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) 
        {
            if (reload=true) { 
                window.setTimeout(function() {
                        location.reload()
                }, 1200)

            // window.location.reload();
        } else {
            document.getElementById('shopping_cart_quantity').innerHTML= '+1';
        }
            // document.getElementById('shopping_cart_quantity').innerHTML= '+1';
            // document.getElementById("updated_cart").classList.remove('hidden','opacity-0');
            // document.getElementById("updated_cart").classList.add('opacity-100');   
        }
  };
}

function trackCart(product_id, product_name, quantity, manufacturer, price, currency){
// console.log(product_id, product_name, quantity, manufacturer, price);
window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
  event: "add_to_cart", 
  eventModel: {
    allow_enhanced_conversions: true,
    currency: currency,
    value: 'product price',
    tag_source: 'browser_side',
    event_id: uuid(),
      items: [
        {
          name: product_name,  
          item_name: product_name, 
          id: product_id, 
          item_id: product_id,  
          sku: '',
          item_sku: '',
          variant: product_id, 
          item_variant: product_id, 
          price: price, 
          brand: manufacturer, 
          item_brand: manufacturer, 
          category: '', 
          item_category: '', 
          quantity: quantity,
          google_business_vertical: 'retail',
        }
      ]
    }
  });
}
function InputSquare(selected_this){
    setTimeout(() => {
        var product_id = $(selected_this).attr("product_id");
        var selected_input = $(selected_this).attr("selected_input");
        var default_meters = new BigNumber($('input[name="' + product_id + '_unit"]').val());
        var min_packs = new BigNumber($('input[name="' + product_id + '_minimum"]').val());
        var unit_price = new BigNumber($('input[name="' + product_id + '_price"]').val());

        var manufacturer = document.getElementById('product_manufacturer_' + product_id).innerHTML;
        var price = document.getElementById('product_price_' + product_id).value;
        var product_name = document.getElementById('product_name_' + product_id).innerHTML;
        var currency =  document.getElementById("currency").innerHTML;

        if (selected_input === "meters") {

            var meters= $('input[name="' + product_id + '_meters"]').val().replace(",",".");
            meters =  new BigNumber(meters);
            var pack = new BigNumber(0);
            var quantity = new BigNumber(0);
            pack = Math.ceil(parseFloat(meters.dividedBy(default_meters).valueOf()));
        
        } else if (selected_input === "pack") {
            var pack = Math.round(parseInt( $('input[name="' + product_id + '_pack"]').val()));
        }
        if (pack< min_packs) {
          pack = min_packs;
        }
        quantity = default_meters.multipliedBy(pack);

        $('input[name="' + product_id + '_meters"]').val(quantity.valueOf());
        $('input[name="' + product_id + '_pack"]').val(pack.valueOf());
        $('input[name="' + product_id + '_quantity"]').val(quantity.valueOf());

        updateCart(product_id, quantity.valueOf());
        var price = document.getElementById('product_price_' + product_id).innerHTML;
        var pack =  document.getElementById('meter_pack_'+ product_id).value;
        trackCart(product_id, product_name, pack, manufacturer, (price*pack).toFixed(2), currency);
    }, 720);
  
};

function InputSingleSet(selected_this){
    setTimeout(() => {
        var product_id = $(selected_this).attr("product_id");
        product_single_set = $('input[name="' + product_id + '_single_set"]').prop('checked');
        quantity = $('#product_quantity_' + product_id).val();
        updateCart(product_id, quantity, product_single_set);
    }, 240);
};


$('.cart-input-group').on('click', '.button-plus', function(e) {
    changeValue(e, 'plus');
});

$('.cart-input-group').on('click', '.button-minus', function(e) {
    changeValue(e, 'minus');
});

function changeValue(e, sign) {
    e.preventDefault();
    setTimeout(() => {
      var fieldName = $(e.target).data('field');
      var parent = $(e.target).closest('div');
      var step = parseFloat($('input[name=' + fieldName + ']').attr("step"));
      var product_id = $('input[name=' + fieldName + ']').attr("product_id");
      var min_packs = parseFloat($('input[name="' + product_id + '_minimum"]').val(), 10);
      var currentVal = parseFloat(parent.find('input[name=' + fieldName + ']').val(), 10);

      if (!isNaN(currentVal) && currentVal > 0) {
        if (sign == "plus") {
            new_value = currentVal + step;
        } else {
            new_value = currentVal - step;
        }

        if (fieldName.includes('pack')) {
            if (new_value<min_packs) {
              new_value = min_packs;
            }
            new_value = new_value.toFixed(0);
        } else {
            if (new_value<step) {
              new_value = step;
            }
            new_value = new_value.toFixed(2);        
        }
        parent.find('input[name=' + fieldName + ']').val(new_value);
      } else {
        parent.find('input[name=' + fieldName + ']').val(step);
      }
      var other_step = new BigNumber($('input[name="' + product_id + '_meters"]').attr("step"));
      if (fieldName.includes('pack')) {
        if (!isNaN(other_step) && other_step > 0) {
          var quantity = other_step.multipliedBy(parent.find('input[name=' + fieldName + ']').val()); 
        } else {
          var quantity = new_value;
        }
        packChange(quantity, product_id);
      }
      else  {
        quantity = new BigNumber($('input[name="' + product_id + '_meters"]').val()); 
        var pack = quantity.dividedBy(other_step).valueOf();
        meterChange(pack, product_id);
    }
    var product_name = document.getElementById('product_name_' + product_id).innerHTML;

    var manufacturer = document.getElementById('product_manufacturer_' + product_id).innerHTML;
    var price = document.getElementById('product_price_' + product_id).innerHTML;
    var pack =  document.getElementById('meter_pack_'+ product_id).value;
    var currency =  document.getElementById("currency").innerHTML;
    
    updateCart(product_id, quantity.valueOf());
    trackCart(product_id, product_name, pack, manufacturer, (price*pack).toFixed(2), currency);
    }, 360);
};


function packChange(quantity, product_id) {
    $('input[name="' + product_id + '_meters"]').val(quantity.valueOf());
};

function meterChange(pack, product_id) {
    $('input[name="' + product_id + '_pack"]').val(pack.valueOf());
}

var buttons = document.querySelectorAll('.button-update');

buttons.forEach(button => {
  button.addEventListener('click', handleClick, false);
});

function handleClick(e) {
  var request = new XMLHttpRequest();
  var id = e.currentTarget.dataset.id;
  request.open('POST', url + [edit_url]);
  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  request.setRequestHeader('X-Requested-With','XMLHttpRequest');
  request.setRequestHeader('X-CSRF-Token', token);
  var post_params = [parameter_name] + id + "&product_quantity_"+ id + "=0"; 
  request.send(post_params);
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) 
    {
      window.location.reload()    
    }
  };
}