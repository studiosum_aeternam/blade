$("#sort").change(function() {
  var queryParams = new URLSearchParams(window.location.search);
  queryParams.set("sort", $('#sort').val());
  history.replaceState(null, null, "?"+queryParams.toString());
  window.location.reload();
})

selectElem.addEventListener("change", function () {
  var val = selectElem.value;
  if (val == "") {
    _("top_bar_search").action = "/search/";
  } else {
    _("top_bar_search").action = defaultForm;
  }
});


