var buttons = document.querySelectorAll('.button-update');

buttons.forEach(button => {
  button.addEventListener('click', handleClick, false);
});

function handleClick(e) {
  var request = new XMLHttpRequest();
  var id = e.currentTarget.dataset.id;
  request.open('POST', url + [edit_url]);
  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  request.setRequestHeader('X-Requested-With','XMLHttpRequest');
  request.setRequestHeader('X-CSRF-Token', token);
  var post_params = [parameter_name] + id + "&product_quantity_"+ id + "=0"; 
  request.send(post_params);
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) 
    {
      window.location.reload()    
    }
  };
}