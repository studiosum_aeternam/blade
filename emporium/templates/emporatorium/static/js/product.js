
var action_url_compare ="compare_edit";
var compare_name = "update_compare&product_id=";

var default_meters = new BigNumber(product_json.quantity);
var minVal = product_json.minimum;
var CompareOne = Cookie.get("compare_contents");
if (CompareOne) {
  var compare = JSON.parse(window.atob(CompareOne));
  if (
    compare &&
    compare.products &&
    compare.products.includes(product_json.id)
  ) {
    _("in_compare").style.display = "block";
  }
}


function changeValue(e, sign) {
  e.preventDefault();
  var fieldName = $(e.target).data("field");
  var parent = $(e.target).closest("div");
  var step = parseFloat($("input[name=" + fieldName + "]").attr("step"));
  var currentVal = parseFloat(
    parent.find("input[name=" + fieldName + "]").val(),
    10
  );

  if (!isNaN(currentVal) && currentVal > 0) {
    if (sign == "plus") {
      new_value = currentVal + step;
    } else {
      new_value = currentVal - step;
    }
    if (fieldName == "pack") {
      new_value = new_value.toFixed(0);
    } else {
      new_value = new_value.toFixed(2);
    }
    parent.find("input[name=" + fieldName + "]").val(new_value);
  } else {
    parent.find("input[name=" + fieldName + "]").val(step);
  }
  if (fieldName == "pack") {
    packChange(parent.find("input[name=" + fieldName + "]").val());
  } else {
    meterChange(parent.find("input[name=" + fieldName + "]").val());
  }
}

$(".cart-input-group").on("click", ".button-plus", function (e) {
  changeValue(e, "plus");
});

$(".cart-input-group").on("click", ".button-minus", function (e) {
  changeValue(e, "minus");
});

function packChange(e) {
  $(e * 1);
  if (e < minVal) {
    e = minVal;
    $('input[name="pack"]').val(minVal.valueOf());
  }
  var quantity = default_meters.multipliedBy(e);
  $('input[name="meters"]').val(quantity.valueOf());
  $('input[name="quantity"]').val(quantity.valueOf());
  $("#product_packages").val(e);
}

function meterChange(e) {
  var meters = new BigNumber(parseFloat(e));
  var pack = meters.dividedBy(default_meters).valueOf();
  if (pack < minVal) {
    pack = minVal;
    $('input[name="meters"]').val(default_meters.multipliedBy(pack));
  }
  $('input[name="pack"]').val(pack.valueOf());
  $("#product_packages").val(pack.valueOf());
  $('input[name="quantity"]').val(meters.valueOf());
}

let meters_input = document.querySelector('input[name="meters"]');
let pack_input = document.querySelector('input[name="pack"]');

if (meters_input) {
  meters_input.oninput = handleMeterInput;
  meters_input.onchange = handleMeterInput;
}

pack_input.oninput = handlePackInput;
pack_input.onchange = handlePackInput;
var delayTimer;

function handleMeterInput(e) {
  var meters = new BigNumber($(e.target).val().replace(",", "."));
  var pack = new BigNumber(0);
  var quantity = new BigNumber(0);
  clearTimeout(delayTimer);
  delayTimer = setTimeout(function () {
    pack = Math.ceil(parseFloat(meters.dividedBy(default_meters).valueOf()));
    if (pack < minVal) {
      pack = minVal;
    }
    var quantity = default_meters.multipliedBy(pack);
    $('input[name="pack"]').val(pack.valueOf());
    $("#product_packages").val(pack.valueOf());
    $('input[name="quantity"]').val(quantity.valueOf());
    $('input[name="meters"]').val(quantity.valueOf());
  }, 665);
}

function handlePackInput(e) {
  var pack = parseInt($(e.target).val());
  $(e).val(pack * 1);
  clearTimeout(delayTimer);
  delayTimer = setTimeout(function () {
    if (pack > minVal) {
      $('input[name="pack"]').val(Math.floor(pack));
    } else {
      pack = minVal;
      $('input[name="pack"]').val(minVal);
    }
    var quantity = default_meters.multipliedBy(pack);
    $("#product_packages").val(pack.valueOf());
    $('input[name="meters"]').val(quantity.valueOf());
    $('input[name="quantity"]').val(quantity.valueOf());
  }, 665);
}

function addToBasket(selected_this) {
  var product_id = $(selected_this).attr("product_id");
  var ean =  _("product_ean_" + product_id).value;
  var element = _("in_cart");
  var manufacturer = _('product_manufacturer_' + product_id).innerHTML;
  var packages =  _("product_packages").value;
  var price = _('product_price_' + product_id).innerHTML;
  var product_name =  _("product_name_" + product_id).textContent;
  var quantity =  _("product_quantity_" + product_id).value;
  var currency =  _("currency").innerHTML;
  
  $("#cart_product_name").text(product_name);
  updateCart(product_id, quantity);
  _("shopping_cart_quantity").innerHTML = "+" + packages;
  document
    .getElementById("updated_cart")
    .classList.remove("hidden", "opacity-0");
  _("updated_cart").classList.add("opacity-100");
  if (typeof element != "undefined" && element != null) {
    _("in_cart").classList.add("hidden");
  }
  trackCart(product_id, product_name, packages, manufacturer, (price*packages).toFixed(2), currency);
  setTimeout(function () {
    location.reload();
  }, 555);
}

function addToCompare(selected_this) {
  var product_id = $(selected_this).attr("product_id");
  var product_name = $("#product_name_" + product_id).text();
  jQuery.ajax({
    url: "/compare_edit",
    type: "POST",
    async: true,
    headers: { "X-CSRF-Token": token },
    data: "update_compare&product_id=" + product_id,
    dataType: "json",
  });
  _("compare_quantity").innerHTML = "+1";
  _("in_compare").innerHTML =
    '<b class="orange">Dodano do porównania</b>';
  _("in_compare").classList.add("opacity-100").remove("opacity-0");
  $("#add_to_compare").hide();
  $("#updated_compare").show(324);
}
