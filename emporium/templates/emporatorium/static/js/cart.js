function InputSquare(selected_this){
    setTimeout(() => {
        var product_id = $(selected_this).attr("product_id");
        var selected_input = $(selected_this).attr("selected_input");
        var default_meters = new BigNumber($('input[name="' + product_id + '_unit"]').val());
        var min_packs = new BigNumber($('input[name="' + product_id + '_minimum"]').val());
        var unit_price = new BigNumber($('input[name="' + product_id + '_price"]').val());

        var manufacturer = _('product_manufacturer_' + product_id).innerHTML;
        var price = _('product_price_' + product_id).value;
        var product_name = _('product_name_' + product_id).innerHTML;
        var currency =  _("currency").innerHTML;

        if (selected_input === "meters") {

            var meters= $('input[name="' + product_id + '_meters"]').val().replace(",",".");
            meters =  new BigNumber(meters);
            var pack = new BigNumber(0);
            var quantity = new BigNumber(0);
            pack = Math.ceil(parseFloat(meters.dividedBy(default_meters).valueOf()));
        
        } else if (selected_input === "pack") {
            var pack = Math.round(parseInt( $('input[name="' + product_id + '_pack"]').val()));
        }
        if (pack< min_packs) {
          pack = min_packs;
        }
        quantity = default_meters.multipliedBy(pack);

        $('input[name="' + product_id + '_meters"]').val(quantity.valueOf());
        $('input[name="' + product_id + '_pack"]').val(pack.valueOf());
        $('input[name="' + product_id + '_quantity"]').val(quantity.valueOf());

        updateCart(product_id, quantity.valueOf());
        var price = _('product_price_' + product_id).innerHTML;
        var pack =  _('meter_pack_'+ product_id).value;
        trackCart(product_id, product_name, pack, manufacturer, (price*pack).toFixed(2), currency);
    }, 720);
  
};

function InputSingleSet(selected_this){
    setTimeout(() => {
        var product_id = $(selected_this).attr("product_id");
        product_single_set = $('input[name="' + product_id + '_single_set"]').prop('checked');
        quantity = $('#product_quantity_' + product_id).val();
        updateCart(product_id, quantity, product_single_set);
    }, 240);
};


$('.cart-input-group').on('click', '.button-plus', function(e) {
    changeValue(e, 'plus');
});

$('.cart-input-group').on('click', '.button-minus', function(e) {
    changeValue(e, 'minus');
});

function changeValue(e, sign) {
    e.preventDefault();
    setTimeout(() => {
      var fieldName = $(e.target).data('field');
      var parent = $(e.target).closest('div');
      var step = parseFloat($('input[name=' + fieldName + ']').attr("step"));
      var product_id = $('input[name=' + fieldName + ']').attr("product_id");
      var min_packs = parseFloat($('input[name="' + product_id + '_minimum"]').val(), 10);
      var currentVal = parseFloat(parent.find('input[name=' + fieldName + ']').val(), 10);

      if (!isNaN(currentVal) && currentVal > 0) {
        if (sign == "plus") {
            new_value = currentVal + step;
        } else {
            new_value = currentVal - step;
        }

        if (fieldName.includes('pack')) {
            if (new_value<min_packs) {
              new_value = min_packs;
            }
            new_value = new_value.toFixed(0);
        } else {
            if (new_value<step) {
              new_value = step;
            }
            new_value = new_value.toFixed(2);        
        }
        parent.find('input[name=' + fieldName + ']').val(new_value);
      } else {
        parent.find('input[name=' + fieldName + ']').val(step);
      }
      var other_step = new BigNumber($('input[name="' + product_id + '_meters"]').attr("step"));
      if (fieldName.includes('pack')) {
        if (!isNaN(other_step) && other_step > 0) {
          var quantity = other_step.multipliedBy(parent.find('input[name=' + fieldName + ']').val()); 
        } else {
          var quantity = new_value;
        }
        packChange(quantity, product_id);
      }
      else  {
        quantity = new BigNumber($('input[name="' + product_id + '_meters"]').val()); 
        var pack = quantity.dividedBy(other_step).valueOf();
        meterChange(pack, product_id);
    }
    var product_name = _('product_name_' + product_id).innerHTML;

    var manufacturer = _('product_manufacturer_' + product_id).innerHTML;
    var price = _('product_price_' + product_id).innerHTML;
    var pack =  _('meter_pack_'+ product_id).value;
    var currency =  _("currency").innerHTML;
    
    updateCart(product_id, quantity.valueOf());
    trackCart(product_id, product_name, pack, manufacturer, (price*pack).toFixed(2), currency);
    }, 360);
};


function packChange(quantity, product_id) {
    $('input[name="' + product_id + '_meters"]').val(quantity.valueOf());
};

function meterChange(pack, product_id) {
    $('input[name="' + product_id + '_pack"]').val(pack.valueOf());
}
