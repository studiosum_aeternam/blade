var token = _("csrf_token").value;

function readMore() {
  _("read-more").classList.add("hide", "hidden");
  _("read-more-text").classList.remove("hide", "hidden");
}

var selectElem = _("category");
var defaultForm = _("top_bar_search").action;

$("ul.hide-on-load").removeClass("hide-on-load");

/**
 * Add autocompletion to the location search input
 */
const init = () => {
  const input = document.querySelector('input[name="term"]');

  if (!input) {
    return;
  }

  const awesomplete = new Awesomplete(input, { tabSelect: true, minChars: 3 });

  /**
   * Ajax a new request for posts
   */
  function ajaxResults() {
    const endpointURL = "/product_search?term=" + input.value;

    const ajax = new XMLHttpRequest();
    ajax.open("GET", endpointURL, true);
    var u = _("main_logo").href;
    ajax.onload = () => {
      let list = [];
      if (200 === ajax.status) {
        list = JSON.parse(ajax.responseText).map((item) => {
          return { label: item.name, value: item.url };
        });
        awesomplete.list = list;
        $("#product_search").on("awesomplete-selectcomplete", function () {
          location.href = u + this.value;
        });
      }
    };
    ajax.send();
  }

  input.addEventListener("keyup", ajaxResults);
};

// init();

$(function () {
  $(document).on("click", "input[type=number]", function () {
    this.select();
  });
});

