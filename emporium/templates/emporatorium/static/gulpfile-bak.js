const gulp = require('gulp'), fancylog = require('fancy-log');
const imagemin = require('gulp-imagemin');
const purgecss = require('@fullhuman/postcss-purgecss')

/**
 * Define all source paths
 */

var paths = {
    styles: {
        src: './assets/scss/**/*.scss',
        dest: './css'
    },
    scripts: {
        src: './assets/js/*.js',
        dest: './js'
    },
    images: {
        src: './assets/img/*',
        dest: './img'
    }
};


/**
 * Webpack compilation: http://webpack.js.org, https://github.com/shama/webpack-stream#usage-with-gulp-watch
 *
 * build_js()
 */

function build_js() {
    const compiler = require('webpack'),
        webpackStream = require('webpack-stream');

    return gulp.src(paths.scripts.src)
        .pipe(
            webpackStream({
                    config: require('./webpack.config.js')
                },
                compiler, function (err, stats) {
                    if (err) {
                        fancylog(err)
                    }
                })
        )
        .pipe(
            gulp.dest(paths.scripts.dest)
        );
}


/**
 * SASS-CSS compilation: https://www.npmjs.com/package/gulp-sass
 *
 * build_css()
 */

function build_css() {
    const sass= require('gulp-sass'),
        postcss = require('gulp-postcss'),
        sourcemaps = require('gulp-sourcemaps'),
        autoprefixer = require('autoprefixer'),
        cssnano = require('cssnano');

    const plugins = [
        autoprefixer(),
        cssnano(),
        purgecss({
		content: ['../templates/**/*.html'],
		safelist: [/^(js-|is-|has-|sticky-|responsive-|submenu-|first-|opens-|position-)/],
		safelist: [/^(js-|is-|has-|sticky-|responsive-|submenu-|first-|opens-|position-)/]
		})
    	];

    return gulp.src(paths.styles.src)
        .pipe(
            sourcemaps.init()
        )
        .pipe(
            sass({
                   includePaths: ['node_modules/foundation-sites/scss','node_modules/motion-ui/src'],
                   errLogToConsole: true,
               })
                .on('error', sass.logError)
        )
        .pipe(
            postcss(plugins)
        )
        .pipe(
            sourcemaps.write('./')
        )
        .pipe(
            gulp.dest(paths.styles.dest)
        );
}

function build_img() {

    return gulp.src(paths.images.src)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.images.dest));
}

/**
 * Watch task: Webpack + SASS
 *
 * $ gulp watch
 */

gulp.task('watch',
    function () {
        gulp.watch(paths.scripts.src, build_js);
        gulp.watch([paths.styles.src, './assets/scss/*.scss'], build_css);
        gulp.watch(paths.images.src, build_img);
    }
);
