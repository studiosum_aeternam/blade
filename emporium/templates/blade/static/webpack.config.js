const path = require( 'path' );
const webpack = require( 'webpack' );


module.exports = {
    devServer: {
        watchOptions: { ignored: [path.resolve(__dirname, '/src/tailwind.css')]}
    }
    context: path.resolve( __dirname, 'assets' ),
    entry: {
        main: [ './main.js' ],
    },
    output: {
        path: path.resolve( __dirname, 'assets/js' ),
        filename: '[name].bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.(png|svg|jpg|gif|ico)$/,
                use: [
                    'file-loader',
                ],
            },
        
	],
    },
    externals: {},
    plugins: [],
    devtool: 'source-map',
    cache: {
        type: 'filesystem',
    },
    watch: true,
};
