//Modal

function modalAkceptiren() {
  Cookie.set("modal_popup", true, { SameSite: "strict", Path: "/" });
  togglePopupModal();
}


var modal_popup = Cookie.get("modal_popup");
if (modal_popup) {
  togglePopupModal();
}

var openmodalpopup = document.querySelectorAll(".modalpopup-open");
if (openmodalpopup && openmodalpopup.length > 0) {
  for (var i = 0; i < openmodalpopup.length; i++) {
    openmodalpopup[i].addEventListener("click", function (event) {
      event.preventDefault();
      togglePopupModal();
    });
  }
}

var overlay = document.querySelector(".modalpopup-overlay");
if (overlay) {
  overlay.addEventListener("click", togglePopupModal);
}

var closemodalpopup = document.querySelectorAll(".modalpopup-close");
if (closemodalpopup && closemodalpopup.length > 0) {
  for (var i = 0; i < closemodalpopup.length; i++) {
    closemodalpopup[i].addEventListener("click", modalAkceptiren, togglePopupModal);
  }
}

document.onkeydown = function (evt) {
  evt = evt || window.event;
  var isEscape = false;
  if ("key" in evt) {
    isEscape = evt.key === "Escape" || evt.key === "Esc";
  } else {
    isEscape = evt.keyCode === 27;
  }
  if (isEscape && document.body.classList.contains("modalpopup-active")) {
    togglePopupModal();
  }
};

function togglePopupModal() {
  const body = document.querySelector("body");
  const modalpopup = document.querySelector(".modalpopup");
  modalpopup.classList.toggle("opacity-0");
  modalpopup.classList.toggle("pointer-events-none");
  body.classList.toggle("modal-active");
}
