var buttons = document.querySelectorAll('.button-change');
buttons.forEach(buton => {
  buton.addEventListener('click', handleClick, false);
});

function handleClick(e) {
  const action_name = e.currentTarget.dataset.action_name;
  var action_button = document.getElementById("change_button");
  action_button.dataset.id = e.currentTarget.dataset.id;
  action_button.dataset.action_url= e.currentTarget.dataset.action_url;
  action_button.dataset.action_name= action_name;
  action_button.dataset.parameter_name = e.currentTarget.dataset.parameter_name;
  document.getElementById('text_modal').innerHTML =  document.getElementById('txt_' + action_name).innerText;
  document.getElementById('change_button').classList.remove("hidden"); // Reset form - possible multiple updates 
  document.getElementById('change_button_success').classList.add("hidden"); // Reset form - possible multiple updates 
  if (action_name =='update') {
    action_button.dataset.value = document.getElementById('discount_' + action_button.dataset.id).value;
  } else if (action_name =='create') {
    action_button.dataset.value = document.getElementById('new_discount').value;
    action_button.dataset.related_data = e.currentTarget.dataset.related_data;
  }

  action_button.addEventListener("click", updateObject, false);
}

function updateObject(e) {
  var dataset = e.currentTarget.dataset;
  const id = dataset.id;
  const action_name = dataset.action_name;
  const parameter_name = dataset.parameter_name;
  var request = new XMLHttpRequest();
  request.open('POST', dataset.action_url);
  request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  request.setRequestHeader('X-Requested-With','XMLHttpRequest');
  request.setRequestHeader('X-CSRF-Token', token);
  var post_params = '';
  var delete_edit = ['delete', 'update'];
  var create_edit = ['create', 'update'];
  if (delete_edit.includes(action_name)) {
    post_params += parameter_name + '=' + id;
  } 
  if (create_edit.includes(action_name)) {
    post_params += '&subtract=' + dataset.subtract + '&value=' + dataset.value;
  }
  if (action_name == 'create') {
    post_params += '&' + dataset.related_data;
  }
  request.send(post_params);
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) 
    {
      document.getElementById('text_modal').innerHTML = document.getElementById('txt_success_' + action_name).innerText;
      document.getElementById('change_button').classList.replace("button_admin", "hidden");
      document.getElementById('change_button_success').classList.replace("hidden", "button_admin");
      if (action_name == 'delete' and document.getElementById('tr_' + id)) {
        var tr = document.getElementById('tr_' + id);
        tr.remove();
      } else if (action_name == 'create') {
        document.getElementById('change_button_success').onclick = location.reload();
      }
    }
  };
}