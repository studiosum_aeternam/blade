const url = new URL(location.origin);

var add_buttons = document.querySelectorAll('.add_to_basket');

var edit_url = "cart_edit";
var parameter_name = "update_cart&product_id=";
var quantity_name = "product_quantity_";
var single_set_name = "product_single_set_";

add_buttons.forEach(button => {
  button.addEventListener('click', addToBasket, false);
});

var buttons_plus_minus = document.querySelectorAll('.button-plus-minus');


function addToBasket(e){
  var request = new XMLHttpRequest();
  var product_id = e.currentTarget.value;
  var manufacturer = document.getElementById('product_manufacturer_' + product_id).innerHTML;
  var quantity = document.getElementById('product_packages_' + product_id).value; // quantity is sq meter
  var price = document.getElementById('product_price_' + product_id).value;
  var product_name = document.getElementById('product_name_' + product_id).innerHTML;

  document.getElementById("modal_title").innerHTML = product_name;
  document.getElementById("modal_description").innerHTML = add_description;
  updateCart(product_id, quantity, true, false);
  trackCart(product_id, product_name, quantity, manufacturer, price);
}

function updateCart(product_id, quantity, single_set=true, reload=true) {
    buttons_plus_minus.forEach(buton => {
        buton.setAttribute("disabled", "");
    });
    var request = new XMLHttpRequest();
    request.open('POST',  url + [edit_url]);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    request.setRequestHeader('X-Requested-With','XMLHttpRequest');
    request.setRequestHeader('X-CSRF-Token', token);
    var post_params = parameter_name + product_id + "&" + quantity_name + product_id + "=" + quantity;
    if (single_set == true) {
         post_params += "&" + single_set_name + product_id + "=true";
     }
    request.send(post_params);
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) 
        {


            if (document.getElementById("order_summary") && document.getElementById("cart_summary_block")) {
                document.getElementById("order_summary").classList.add('hidden');
                document.getElementById("order_summary_update").classList.remove('hidden');
                document.getElementById("cart_summary_block").classList.add('change_opacity_on_update');


            }
            if (reload=true) { 
                window.setTimeout(function() {
                        location.reload()
                }, 1700)

            // window.location.reload();
        }
        document.getElementById('shopping_cart_quantity').innerHTML= '+' + quantity;
        // document.getElementById("updated_cart").classList.remove('hidden','opacity-0');
            // document.getElementById("updated_cart").classList.add('opacity-100');   
        }
  };
}

function trackCart(product_id, product_name, quantity, manufacturer, price, currency){
// console.log(product_id, product_name, quantity, manufacturer, price);
window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
  event: "add_to_cart", 
  eventModel: {
    allow_enhanced_conversions: true,
    currency: currency,
    value: price,
    tag_source: 'browser_side',
    event_id: uuid(),
      items: [
        {
          name: product_name,  
          item_name: product_name, 
          id: product_id, 
          item_id: product_id,  
          sku: '',
          item_sku: '',
          variant: product_id, 
          item_variant: product_id, 
          price: price, 
          brand: manufacturer, 
          item_brand: manufacturer, 
          category: '', 
          item_category: '', 
          quantity: quantity,
          google_business_vertical: 'retail',
        }
      ]
    }
  });
}