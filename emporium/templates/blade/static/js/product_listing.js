var sorter = document.getElementById("sort");
sorter.addEventListener("change", function() {
  var queryParams = new URLSearchParams(window.location.search);
  queryParams.set("sort", document.getElementById('sort').value);
  history.replaceState(null, null, "?"+queryParams.toString());
  window.location.reload();
})

if (selectElem){
  selectElem.addEventListener("change", function () {
    var val = selectElem.value;
    if (document.getElementById("top_bar_search")) {
      if (val == "") {
        document.getElementById("top_bar_search").action = "/search/";
      } else {
        document.getElementById("top_bar_search").action = defaultForm;
      }
    }
  });
  }

