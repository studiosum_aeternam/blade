var buttons = document.querySelectorAll('.remove_from_compare');
buttons.forEach(buton => {
  buton.addEventListener('click', removeFromCompare, false);
});

function removeFromCompare(e){
    var product_id = e.currentTarget.dataset.product_id;
    var request = new XMLHttpRequest();
    request.open('POST',  "/compare_edit");
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    request.setRequestHeader('X-Requested-With','XMLHttpRequest');
    request.setRequestHeader('X-CSRF-Token', token);
    var post_params = 'update_compare&'+ "remove_id" + "=" +product_id;

    request.send(post_params);
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            window.setTimeout(function() {
                location.reload()
            }, 166)
        }
    }
}
