from sqlalchemy import (
    Column,
    Date,
    DateTime,
    ForeignKey,
    Integer,
    Numeric,
    Text,
    Unicode,
)
from sqlalchemy.orm import relationship
from slugify import slugify

from .meta import Base


class Author(Base):
    __tablename__ = "author"
    author_id = Column(Integer, primary_key=True, nullable=False)
    image = Column(Unicode(255))
    first_name = Column(Unicode(255))
    last_name = Column(Unicode(255))
    callsign = Column(Unicode(255))
    status = Column(Integer)
    author_description = relationship("AuthorDescription", backref="author")


class AuthorDescription(Base):
    __tablename__ = "author_description"
    author_description_id = Column(Integer, primary_key=True, nullable=False)
    author_id = Column(Integer, ForeignKey("author.author_id"), index=True)
    language_id = Column(Integer)
    description = Column(Text)
    meta_description = Column(Unicode(255))
    tag = Column(Text)
    u_title = Column(Unicode(255))
    u_h1 = Column(Unicode(255))

    @property
    def slug(self):
        return slugify(self.name.replace("/", "-"))


class AuthorRole(Base):
    __tablename__ = "author_role"
    author_role_id = Column(Integer, primary_key=True, nullable=False)
    description = Column(Text)
    name = Column(Unicode(255))


class AuthorToImage(Base):
    __tablename__ = "author_image"
    author_image_id = Column(Integer, primary_key=True, nullable=False)
    author_id = Column(
        Integer, ForeignKey("author.author_id", ondelete="CASCADE"), index=True
    )
    image = Column(Unicode(255))
    sort_order = Column(Integer)
