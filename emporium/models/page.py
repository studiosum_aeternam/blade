from sqlalchemy import Boolean, Column, ForeignKey, Index, Integer, Text, Unicode
from slugify import slugify

from .meta import Base


class Page(Base):
    __tablename__ = "page"
    page_id = Column(Integer, primary_key=True)
    page_name = Column(Text, index=True, unique=True)
    status = Column(Boolean, nullable=False, index=True)
    page_type = Column(Integer)


class PageContent(Base):
    __tablename__ = "page_content"
    page_content_id = Column(Integer, primary_key=True)
    creator_id = Column(ForeignKey("users.id"), nullable=False)
    content = Column(Text)
    content_extended = Column(
        Text,
    )
    language_id = Column(Integer)
    page_id = Column(ForeignKey("page.page_id"), nullable=False)
    seo_slug = Column(Unicode(255))
    status = Column(Boolean, nullable=False, index=True)
    title = Column(Text, nullable=False, unique=True)

    @property
    def slug(self):
        return slugify(self.title)


class PageToTag(Base):
    __tablename__ = "page_to_tag"
    page_tag_id = Column(Integer, nullable=False, primary_key=True)
    tag_id = Column(Integer, ForeignKey("tag.tag_id", ondelete="CASCADE"), index=True)
