from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    Text,
    Unicode,
)

from .meta import Base


class Special(Base):
    __tablename__ = "special"
    special_id = Column(Integer, primary_key=True, nullable=False)
    status = Column(Integer)
    price_changing = Column(Boolean)


class SpecialDescription(Base):
    __tablename__ = "special_description"
    special_description_id = Column(Integer, primary_key=True, nullable=False)
    special_id = Column(Integer, ForeignKey("special.special_id"), index=True)
    language_id = Column(Integer, index=True)
    name = Column(Unicode(255), index=True)
    name_displayed = Column(Unicode(255))
    description = Column(Text)
