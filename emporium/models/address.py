from sqlalchemy import Boolean, Column, ForeignKey, Integer, Unicode

from .meta import Base


class Address(Base):
    __tablename__ = "address"
    id = Column(Integer, primary_key=True, nullable=False)
    city = Column(Unicode(64))
    company_name = Column(Unicode(192))
    company_nip = Column(Unicode(14))
    country = Column(Unicode(48))
    customer_id = Column(Integer, ForeignKey("customer.id"))
    defa = Column(Boolean, default=True)
    postcode = Column(Unicode(12))
    state = Column(Unicode(48))
    street = Column(Unicode(64))
