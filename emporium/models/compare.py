from sqlalchemy import Column, DateTime, Integer, PickleType

from .meta import Base


class Compare(Base):
    __tablename__ = "compare"
    compare_id = Column(Integer, primary_key=True, nullable=False)
    customer_id = Column(Integer)
    products = Column(PickleType)
    date_creation = Column(DateTime)
    date_accessed = Column(DateTime)
