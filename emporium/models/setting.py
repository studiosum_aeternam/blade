from sqlalchemy import Column, Integer, JSON, String, Text, Date, Unicode
from sqlalchemy.dialects.postgresql import JSONB
from .meta import Base


class Setting(Base):
    __tablename__ = "settings"
    setting_id = Column(Integer, primary_key=True, nullable=False)
    complex_values = Column(JSONB)
    date = Column(Date)
    key = Column(String(32), index=True)
    setting_type = Column(Integer)
    setting_category = Column(Integer)
    value = Column(Text)


class SettingTemplate(Base):
    __tablename__ = "setting_template"
    setting_template_id = Column(Integer, nullable=False, primary_key=True)
    description = Column(Text)
    name = Column(Unicode(255), index=True)
    order = Column(Integer)
    status = Column(Integer)
    template_json = Column(JSONB)
    template_type = Column(Integer)
