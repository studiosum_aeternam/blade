from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    Numeric,
    Text,
    Unicode,
)

from .meta import Base


class Tax(Base):
    __tablename__ = "tax"
    tax_id = Column(Integer, primary_key=True, nullable=False)
    status = Column(Integer)
    tax_type = Column(Integer)
    value = Column(Numeric(4, 2))

class TaxDescription(Base):
    __tablename__ = "tax_description"
    tax_description_id = Column(Integer, primary_key=True, nullable=False)
    tax_id = Column(Integer, ForeignKey("tax.tax_id"), index=True)
    language_id = Column(Integer, index=True)
    name = Column(Unicode(255), index=True)
    description = Column(Text)
