from decimal import Decimal

from sqlalchemy import (
    Boolean,
    Column,
    Date,
    DateTime,
    ForeignKey,
    Integer,
    JSON,
    Numeric,
    text,
    Text,
    Unicode,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Index
from slugify import slugify

from .meta import Base


class Product(Base):
    __tablename__ = "product"
    product_id = Column(Integer, primary_key=True, nullable=False)
    # Misc
    date_modified = Column(DateTime)
    # Availability
    availability_id = Column(Integer, ForeignKey("attribute.attribute_id"), index=True)
    date_added = Column(DateTime)
    date_available = Column(Date, index=True)
    date_end = Column(Date, index=True)

    # Identifiers
    ean = Column(Unicode(16))
    isbn = Column(Unicode(13))
    jan = Column(Unicode(13))
    model = Column(Unicode(96), index=True, nullable=False, unique=True)
    mpn = Column(Unicode(64))
    sku = Column(Unicode(64))
    upc = Column(Unicode(12))

    image = Column(Unicode(255))

    # Physical parameters
    height = Column(Numeric(15, 2))
    product_type = Column(Integer)  # single 0/set 1
    length = Column(Numeric(15, 2))
    square_meter = Column(Integer)
    transport_method = Column(Integer)  # physical 0/digital 1/mixed 2
    width = Column(Numeric(15, 2))
    weight = Column(Numeric(15, 2))

    # Unit parameters
    minimum = Column(Numeric(15, 2))
    one_batch = Column(Boolean)
    subtract = Column(Integer)
    subtract_quantity = Column(Integer)
    unit = Column(Numeric(15, 2))
    manufactured = Column(Integer)
    manufacturer_id = Column(
        Integer, ForeignKey("manufacturer.manufacturer_id"), index=True
    )
    name = Column(Unicode(255), index=True)

    # Prices
    catalog_price = Column(Numeric(15, 4))
    discount_id = Column(Integer, ForeignKey("discount.discount_id"), index=True)
    manual_discount = Column(Boolean)
    price = Column(Numeric(15, 4), index=True)
    purchase_price = Column(Numeric(15, 4))
    tax_id = Column(Integer, ForeignKey("tax.tax_id"), index=True)

    # Storage
    quantity = Column(Integer)
    pieces = Column(Integer)
    display_storage_type = Column(Integer)

    seo_slug = Column(Unicode(255))
    setting_template_id = Column(
        Integer, ForeignKey("setting_template.setting_template_id")
    )
    sort_order = Column(Integer, index=True)
    status = Column(Integer, index=True)
    viewed = Column(Integer)

    description = relationship("ProductDescription", backref="product")
    manufacturer = relationship("Manufacturer", backref="product")
    product_to_attribute = relationship("ProductToAttribute", backref="product")
    product_to_band = relationship("ProductToBand", backref="product")
    product_to_category = relationship("ProductToCategory", backref="product")
    product_to_collection = relationship("ProductToCollection", backref="product")
    product_to_image = relationship("ProductToImage", backref="product")
    product_to_special = relationship("ProductToSpecial", backref="product")
    product_to_storage = relationship("ProductToStorage", backref="product")
    product_template = relationship("SettingTemplate", backref="product")

    Index(
        "product_name_product_id_idx",
        name,
        product_id,
        postgresql_where=text("status = 1"),
    )
    Index(
        "product_component_id_status_idx",
        product_id,
        postgresql_where=text("status = 1"),
        postgresql_ops={"name": "ASC"},
    )
    Index(
        "product_id_availability_manufacturer_idx",
        product_id,
        manufacturer_id,
        availability_id,
        postgresql_where=text("status = 1"),
    )
    Index("product_ean_finder", ean)

    Index(
        "product_name_idx",
        "name",
        postgresql_using="gin",
        postgresql_ops={
            "name": "gin_trgm_ops",
        },
    )

    @hybrid_property
    def slug(self):
        return slugify(self.name)

    # @slug.expression
    # def slug(cls):
    #     return select(cls.name)

    # @hybrid_property
    # def price_gross_number(self, tax_rate=1.23):
    #     return self.price * Decimal(tax_rate)


class ProductDescription(Base):
    __tablename__ = "product_description"
    product_description_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    description = Column(Text)
    description_technical = Column(Text)
    excerpt = Column(Text)
    language_id = Column(Integer, index=True)
    meta_description = Column(Unicode(255))
    name = Column(Unicode(255), index=True)
    tag = Column(Text)
    u_h1 = Column(Unicode(255))
    u_title = Column(Unicode(255))

    Index("product_description_product_id_language_id_idx", product_id, language_id)


# class ProductPricing(Base):
#     __tablename__ = "product_pricing"
#     product_pricing_id = Column(Integer, primary_key=True)
#     product_id = Column(
#         Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
#     )
#     price = Column(Numeric(15, 4), index=True)
#     date_start = Column(Date, index=True)
#     date_end = Column(Date, index=True)
#     quantity = Column(Integer)

#     Index("product_pricing_product_id_date_start_end", product_id, date_start, date_end)

#     @hybrid_method
#     def tax_value(self, tax_rate):
#         return self.price * Decimal(tax_rate)

#     @hybrid_method
#     def price_gross_number(self, tax_rate):
#         return self.price * Decimal(tax_rate)

#     @hybrid_property
#     def price_gross_formatted(self, tax_rate):
#         current_price = self.price * Decimal(tax_rate)
#         return str(format(current_price, ".2f").replace(".", ","))


class ProductRelations(Base):
    __tablename__ = "product_relations"
    product_relation_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    product_price = Column(Numeric(15, 4), index=True)


class ProductSpecialDescription(Base):
    __tablename__ = "product_special_description"
    product_special_description_id = Column(Integer, primary_key=True, nullable=False)
    name = Column(Unicode(255), index=True)
    status = Column(Integer)
    price_changing = Column(Boolean)


class ProductToAttribute(Base):
    __tablename__ = "product_to_attribute"
    product_attribute_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    attribute_id = Column(
        Integer, ForeignKey("attribute.attribute_id", ondelete="CASCADE"), index=True
    )

    Index("product_to_attribute_product_id_attribute_id_idx", product_id, attribute_id)


class ProductToAuthor(Base):
    __tablename__ = "product_to_author"
    product_author_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    author_id = Column(
        Integer, ForeignKey("author.author_id", ondelete="CASCADE"), index=True
    )
    role = Column(Integer, index=True)
    order = Column(Integer)
    visibility = Column(Integer)

    Index("product_to_author_product_id_author_id_idx", product_id, author_id)


class ProductToCategory(Base):
    __tablename__ = "product_to_category"
    product_category_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    category_id = Column(
        Integer, ForeignKey("category.category_id", ondelete="CASCADE"), index=True
    )

    Index("product_to_attribute_product_id_category_id_idx", product_id, category_id)
    Index("product_tiles_idx", product_id, postgresql_where=text("category_id=60"))


class ProductToCollection(Base):
    __tablename__ = "product_to_collection"
    product_collection_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    collection_id = Column(
        Integer, ForeignKey("collection.collection_id", ondelete="CASCADE"), index=True
    )

    Index(
        "product_to_collection_product_id_collection_id_idx", product_id, collection_id
    )


class ProductToTag(Base):
    __tablename__ = "product_to_tag"
    product_tag_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    tag_id = Column(Integer, ForeignKey("tag.tag_id", ondelete="CASCADE"), index=True)

    Index("product_to_tag_product_id_tag_id_idx", product_id, tag_id)


class ProductToFile(Base):
    __tablename__ = "product_to_file"
    product_file_id = Column(Integer, primary_key=True, nullable=False)
    file_id = Column(
        Integer, ForeignKey("file.file_id", ondelete="CASCADE"), index=True
    )
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    sort_order = Column(Integer)


class ProductToImage(Base):
    __tablename__ = "product_image"
    product_image_id = Column(Integer, primary_key=True, nullable=False)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    image = Column(Unicode(255))
    sort_order = Column(Integer)


class ProductToBand(Base):
    __tablename__ = "product_to_band"
    product_band_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    band_id = Column(
        Integer,
        ForeignKey("band.band_id", ondelete="CASCADE"),
        index=True,
    )
    band_type = Column(Integer, index=True)

    Index(
        "product_to_band_product_id_band_id_idx",
        product_id,
        band_id,
    )
    Index(
        "product_to_band_all_fields_idx",
        product_id,
        band_id,
        band_type,
    )


class ProductToOption(Base):
    __tablename__ = "product_to_option"
    product_option_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    option_id = Column(
        Integer, ForeignKey("option.option_id", ondelete="CASCADE"), index=True
    )
    price = Column(Numeric(15, 4))
    weight = Column(Numeric(15, 8))
    quantity = Column(Integer)


class ProductToProduct(Base):
    __tablename__ = "product_to_product"
    product_component_id = Column(Integer, nullable=False, primary_key=True)
    product1_id = Column(
        Integer,
        ForeignKey("product.product_id", ondelete="CASCADE"),
        index=True,
    )
    product2_id = Column(
        Integer,
        ForeignKey("product.product_id", ondelete="CASCADE"),
        nullable=False,
        index=True,
    )
    product_relation_type = Column(Integer, index=True)
    order = Column(Integer)
    quantity = Column(Integer)
    relation_settings = Column(JSONB)

    product1 = relationship("Product", foreign_keys=[product1_id])
    product2 = relationship("Product", foreign_keys=[product2_id])


class ProductToSpecial(Base):
    __tablename__ = "product_to_special"
    product_special_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    special_id = Column(
        Integer, ForeignKey("special.special_id", ondelete="CASCADE"), index=True
    )
    price = Column(Numeric(15, 4), index=True)
    default = Column(Boolean)
    date_start = Column(Date, index=True)
    date_end = Column(Date, index=True)
    order = Column(Integer)
    quantity = Column(Integer)
    storage_aware = Column(Boolean)

    Index("product_to_special_product_id_special_id_idx", product_id, special_id)


class ProductToStorage(Base):
    __tablename__ = "product_to_storage"
    product_storage_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    storage_id = Column(
        Integer, ForeignKey("storage.storage_id", ondelete="CASCADE"), index=True
    )
    quantity = Column(Integer)

    Index("product_to_storage_product_id_storage_id_idx", product_id, storage_id)


class ProductToTransport(Base):
    __tablename__ = "product_to_transport"
    product_transport_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    transport_slot_id = Column(
        Integer,
        ForeignKey("transport_size_slot.transport_slot_id", ondelete="CASCADE"),
        index=True,
    )
    local_settings = Column(JSONB)
    status = Column(Integer)

    Index("product_to_transport_idx", product_id)


# class ProductToVirtual(Base):
#     __tablename__ = "product_virtual"
#     product_virtual_id = Column(Integer, nullable=False, primary_key=True)
#     product_id = Column(Integer, ForeignKey("product.product_id"), index=True)
#     number_downloads = Column(Integer)


class ProductToXML(Base):
    __tablename__ = "product_xml"
    product_xml_id = Column(Integer, nullable=False, primary_key=True)
    product_id = Column(Integer, ForeignKey("product.product_id"), index=True)
    xml_id = Column(Integer)
    excluded = Column(Boolean)

    Index("product_whith_xml_idx", product_id, xml_id, excluded)
    Index("product_xml_idx", xml_id, excluded)
