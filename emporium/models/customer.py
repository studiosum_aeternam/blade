import bcrypt
from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, Unicode
from sqlalchemy.orm import relationship

from sqlalchemy.ext.hybrid import hybrid_property

from .meta import Base


class Customer(Base):
    __tablename__ = "customer"
    id = Column(Integer, primary_key=True)
    first_name = Column(Unicode(48), nullable=False)
    last_name = Column(Unicode(48), nullable=False)
    mail = Column(Unicode(64), nullable=False)
    password = Column(Unicode(255), nullable=False)
    registration_date = Column(DateTime)
    status = Column(Boolean, default=True)
    telephone = Column(Unicode(24))
    customer_type = Column(Integer)

    customer_reset = relationship("CustomerReset", backref="customer")

    @hybrid_property
    def psswd_hsh(self):
        return self.password

    @psswd_hsh.setter
    def psswd_hsh(self, pw):
        pwhash = bcrypt.hashpw(pw.encode("utf8"), bcrypt.gensalt())
        self.password = pwhash.decode("utf-8")

    def check_password(self, pw):
        if self.password is not None:
            expected_hash = self.password.encode("utf8")
            return bcrypt.checkpw(pw.encode("utf8"), expected_hash)
        return False


class CustomerReset(Base):
    __tablename__ = "customer_reset"
    reset_id = Column(Integer, primary_key=True)
    customer_id = Column(Integer, ForeignKey("customer.id", ondelete="CASCADE"))
    request_uuid = Column(Unicode(32))
    expiration_time = Column(DateTime)
