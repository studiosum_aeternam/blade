from hashlib import md5
import os
import sys

from dogpile.cache.region import make_region
from sqlalchemy import engine_from_config
from sqlalchemy.orm import (
    backref,
    configure_mappers,
    sessionmaker,
    joinedload,
    subqueryload,
    relationship,
    load_only,
    Load,
)
import zope.sqlalchemy

# import or define all models here to ensure they are attached to the
# Base.metadata prior to any initialization routines
from .address import Address
from .attribute import (
    Attribute,
    AttributeDisplayType,
    AttributeGroup,
    AttributeGroupValue,
    AttributeValue,
)
from .author import Author, AuthorDescription, AuthorToImage, AuthorRole
from .banner import Banner
from .band import Band, BandDescription
from .block import Block
from .country import Country
from .cart import Cart
from .category import Category, CategoryDescription, CategoryMetaTreeDescriptions
from .collection import Collection, CollectionDescription, CollectionToCollection
from .compare import Compare
from .customer import Customer, CustomerReset
from .discount import Discount
from .file import File, FileNameDisplayMode, FileType, FilePermission
from .keyword import Keyword, KeywordToTag
from .manufacturer import Manufacturer, ManufacturerDescription, ManufacturerToDiscount
from .option import Option, OptionDescription, OptionGroup, OptionGroupDescription
from .order import Order, OrderHistory, OrderToSpecial, OrderToUUID
from .page import Page, PageContent, PageToTag
from .payment import Payment
from .post import Post
from .product import (
    Product,
    ProductDescription,
    ProductRelations,
    ProductToAttribute,
    ProductToAuthor,
    ProductToBand,
    ProductToCategory,
    ProductToCollection,
    ProductToFile,
    ProductToImage,
    ProductToOption,
    ProductToOption,
    ProductToProduct,
    ProductToSpecial,
    ProductToStorage,
    ProductToTransport,
    ProductToTag,
    # ProductToVirtual,
    ProductToXML,
)
from .setting import Setting, SettingTemplate
from .shelve import ShelveSpecial, ShelveStorage
from .special import Special, SpecialDescription
from .storage import Storage
from .status import Status, StatusDescription, StatusGroup, StatusGroupDescription
from .tax import Tax, TaxDescription
from .tag import Tag
from .transport import Transport, TransportSlot
from .user import User
from ..services import caching_query

# run configure_mappers after defining all of the models to ensure
# all relationships can be setup
configure_mappers()

regions = {}


def md5_key_mangler(key):
    """Receive cache keys as long concatenated strings;
    distill them into an md5 hash.

    """
    return md5(key.encode("utf-8")).hexdigest()


regions["redis-short"] = make_region(key_mangler=md5_key_mangler).configure(
    "dogpile.cache.redis",
    expiration_time=6000,
    arguments={
        "url": "redis://127.0.0.1?db=5",
        "port": 6379,
        "db": 5,
        "redis_expiration_time": 60 * 10 * 2,
        "distributed_lock": True,
        "thread_local_lock": False,
    },
)

regions["redis-long"] = make_region(key_mangler=md5_key_mangler).configure(
    "dogpile.cache.redis",
    expiration_time=864000,
    arguments={
        "url": "redis://127.0.0.1?db=5",
        "port": 6379,
        "db": 5,
        "redis_expiration_time": 60**2 * 48,
        "distributed_lock": True,
        "thread_local_lock": False,
    },
)

regions["redis-eternal"] = make_region(key_mangler=md5_key_mangler).configure(
    "dogpile.cache.redis",
    arguments={
        "url": "redis://127.0.0.1?db=5",
        "port": 6379,
        "db": 5,
        "distributed_lock": True,
        "thread_local_lock": False,
    },
)


def get_engine(settings, prefix="sqlalchemy."):
    return engine_from_config(settings, prefix)


def get_session_factory(engine):
    factory = sessionmaker()
    cache = caching_query.ORMCache(regions)
    cache.listen_on_session(factory)
    factory.configure(bind=engine)
    return factory


def get_tm_session(session_factory, transaction_manager):
    """
    Get a ``sqlalchemy.orm.Session`` instance backed by a transaction.

    This function will hook the session to the transaction manager which
    will take care of committing any changes.

    - When using pyramid_tm it will automatically be committed or aborted
      depending on whether an exception is raised.

    - When using scripts you should wrap the session in a manager yourself.
      For example::

          import transaction

          engine = get_engine(settings)
          session_factory = get_session_factory(engine)
          with transaction.manager:
              dbsession = get_tm_session(session_factory, transaction.manager)

    """
    dbsession = session_factory()
    zope.sqlalchemy.register(dbsession, transaction_manager=transaction_manager)
    return dbsession


def includeme(config):
    """
    Initialize the model for a Pyramid app.

    Activate this setup using ``config.include('tutorial.models')``.

    """
    settings = config.get_settings()
    settings["tm.manager_hook"] = "pyramid_tm.explicit_manager"
    settings["static_location"] = "static"

    # use pyramid_tm to hook the transaction lifecycle to the request
    config.include("pyramid_tm")

    # use pyramid_retry to retry a request when transient exceptions occur
    config.include("pyramid_retry")

    session_factory = get_session_factory(get_engine(settings))
    config.registry["dbsession_factory"] = session_factory

    # make request.dbsession available for use in Pyramid
    config.add_request_method(
        # r.tm is the transaction manager used by pyramid_tm
        lambda r: get_tm_session(session_factory, r.tm),
        "dbsession",
        reify=True,
    )
