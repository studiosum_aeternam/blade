from sqlalchemy import (
    Column,
    Date,
    DateTime,
    ForeignKey,
    Integer,
    Numeric,
    Text,
    Unicode,
)
from sqlalchemy.orm import relationship
from slugify import slugify
from sqlalchemy.dialects.postgresql import JSONB

from .meta import Base


class Band(Base):
    __tablename__ = "band"
    band_id = Column(Integer, primary_key=True, nullable=False)
    code = Column(Unicode(16))
    date_added = Column(DateTime)
    date_available = Column(Date)
    date_modified = Column(DateTime)
    image = Column(Unicode(255))
    band_type = Column(Integer)
    name = Column(Unicode(255))
    priority = Column(Integer)
    seo_slug = Column(Unicode(255))
    sort_order = Column(Integer)
    status = Column(Integer)
    social_links = Column(JSONB)
    band_description = relationship("BandDescription", backref="band")


class BandDescription(Base):
    __tablename__ = "band_description"
    band_description_id = Column(Integer, primary_key=True, nullable=False)
    band_id = Column(Integer, ForeignKey("band.band_id"), index=True)
    language_id = Column(Integer)
    description = Column(Text)
    meta_description = Column(Unicode(255))
    tag = Column(Text)
    u_title = Column(Unicode(255))
    u_h1 = Column(Unicode(255))

    @property
    def slug(self):
        return slugify(self.name.replace("/", "-"))


class BandToTag(Base):
    __tablename__ = "band_to_tag"
    band_tag_id = Column(Integer, primary_key=True, nullable=False)
    tag_id = Column(
        Integer,
        ForeignKey("tag.tag_id"),
        index=True,
    )
