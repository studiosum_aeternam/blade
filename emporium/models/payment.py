from sqlalchemy import (
    Boolean,
    Column,
    Integer,
    JSON,
    Numeric,
    Text,
    Unicode,
)
from sqlalchemy.dialects.postgresql import JSONB

from .meta import Base


# class Payment(Base):
#     __tablename__ = "payment"
#     payment_id = Column(Integer, primary_key=True, nullable=False)
#     api_version = Column(Unicode(16))
#     description = Column(Text)
#     equotation = Column(Unicode(256))
#     fee_percent = Column(Numeric(4, 3))
#     fee_value = Column(Numeric(4, 3))
#     key = Column(Unicode(64))
#     language = Column(Unicode(4))
#     merchant_id = Column(Unicode(64))
#     mode = Column(Integer)
#     name = Column(Unicode(64))
#     pos_id = Column(Unicode(64))
#     status = Column(Boolean, default=True)
#     type = Column(Integer)
#     url_request = Column(Unicode(256))
#     url_test = Column(Unicode(256))
#     url_test_verify = Column(Unicode(256))
#     url_transaction = Column(Unicode(256))
#     url_transaction_verify = Column(Unicode(256))


class Payment(Base):
    __tablename__ = "payment"
    payment_id = Column(Integer, primary_key=True, nullable=False)
    # special_method = Column(Boolean, default=False)
    description = Column(Text)
    excerpt = Column(Unicode(64))
    module_name = Column(Unicode(32))
    module_settings = Column(JSONB)
    name = Column(Unicode(64))
    status = Column(Boolean, nullable=False, default=True)
    type = Column(Integer)
