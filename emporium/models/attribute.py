from sqlalchemy import Boolean, Column, ForeignKey, Integer, Text, Unicode
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB

from .meta import Base


class Attribute(Base):
    __tablename__ = "attribute"
    attribute_id = Column(Integer, primary_key=True, nullable=False)
    attribute_group_id = Column(
        Integer, ForeignKey("attribute_group.attribute_group_id"), index=True
    )
    attribute_settings = Column(JSONB)
    sort_order = Column(Integer)
    status = Column(Integer, index=True)
    value = Column(Unicode(255))

    attribute = relationship("AttributeGroup", backref="attribute")
    product_to_attribute = relationship("ProductToAttribute", backref="attribute")


class AttributeGroup(Base):
    __tablename__ = "attribute_group"
    attribute_group_id = Column(Integer, primary_key=True, nullable=False)
    attribute_display_type_id = Column(
        Integer,
        ForeignKey("attribute_display_type.attribute_display_type_id"),
        index=True,
    )
    description = Column(Text)
    description_hidden = Column(Text)
    name = Column(Unicode(255))
    attribute_name = Column(Unicode(32))
    required = Column(Boolean)
    sort_order = Column(Integer)
    status = Column(Integer, index=True)


class AttributeDisplayType(Base):
    __tablename__ = "attribute_display_type"
    attribute_display_type_id = Column(Integer, primary_key=True, nullable=False)
    name = Column(Unicode(255))
    description = Column(Text)
    sort_order = Column(Integer)
    status = Column(Integer)


class AttributeValue(Base):
    __tablename__ = "attribute_value"
    attribute_value_id = Column(Integer, primary_key=True, nullable=False)
    attribute_id = Column(
        Integer,
        ForeignKey("attribute.attribute_id"),
        index=True,
    )
    language_id = Column(Integer)
    value = Column(Unicode(255))


class AttributeGroupValue(Base):
    __tablename__ = "attribute_group_value"
    attribute_group_value_id = Column(Integer, primary_key=True, nullable=False)
    attribute_group_id = Column(
        Integer,
        ForeignKey("attribute_group.attribute_group_id"),
        index=True,
    )
    attribute_name = Column(Unicode(32))
    description = Column(Text)
    description_hidden = Column(Text)
    language_id = Column(Integer)
    name = Column(Unicode(255))


class AttributeToTag(Base):
    __tablename__ = "attribute_to_tag"
    attribute_tag_id = Column(Integer, primary_key=True, nullable=False)
    tag_id = Column(
        Integer,
        ForeignKey("tag.tag_id"),
        index=True,
    )
