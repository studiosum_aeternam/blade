from sqlalchemy import Boolean, Column, Integer, Numeric
from sqlalchemy.orm import relationship

from .meta import Base


class Discount(Base):
    __tablename__ = "discount"
    discount_id = Column(Integer, primary_key=True, nullable=False)
    default = Column(Boolean, default=False)
    status = Column(Integer, index=True)
    subtract = Column(Integer)
    value = Column(Numeric(4, 2))
    manufacturer_to_discount = relationship(
        "ManufacturerToDiscount", backref="discount"
    )
