from sqlalchemy import Boolean, Column, ForeignKey, Integer, Text

from .meta import Base


class Block(Base):
    __tablename__ = "block"
    block_id = Column(Integer, primary_key=True)
    content = Column(Text, nullable=False)
    creator_id = Column(ForeignKey("users.id"), nullable=False)
    order = Column(Integer)
    page = Column(Integer)
    size = Column(Integer)
    status = Column(Boolean, nullable=False, index=True)
    title = Column(Text, unique=True)
