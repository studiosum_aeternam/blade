from sqlalchemy import Boolean, Column, ForeignKey, Integer, Unicode
from sqlalchemy.dialects.postgresql import JSONB

from .meta import Base


class Country(Base):
    __tablename__ = "country"
    country_id = Column(Integer, primary_key=True, nullable=False)
    country_code = Column(Unicode(64))
    name = Column(Unicode(128))
    sort_order = Column(Integer)
    status = Column(Boolean, default=True)
