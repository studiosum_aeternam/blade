import bcrypt
from sqlalchemy import Column, Integer, Unicode

from .meta import Base


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    first_name = Column(Unicode(48), nullable=False)
    last_name = Column(Unicode(48), nullable=False)
    login = Column(Unicode(16), nullable=False)
    password = Column(Unicode(255), nullable=False)
    role = Column(Unicode(16), nullable=False)

    def set_password(self, pw):
        pwhash = bcrypt.hashpw(pw.encode("utf8"), bcrypt.gensalt())
        self.password = pwhash.decode("utf-8")

    def check_password(self, pw):
        if self.password is not None:
            expected_hash = self.password.encode("utf8")
            return bcrypt.checkpw(pw.encode("utf8"), expected_hash)
        return False
