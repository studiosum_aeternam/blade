from sqlalchemy import (
    Column,
    Date,
    DateTime,
    ForeignKey,
    Integer,
    Numeric,
    Text,
    Unicode,
)
from sqlalchemy.orm import relationship
from slugify import slugify

from .meta import Base


class Manufacturer(Base):
    __tablename__ = "manufacturer"
    manufacturer_id = Column(Integer, primary_key=True, nullable=False)
    code = Column(Unicode(16))
    date_added = Column(DateTime)
    date_available = Column(Date)
    date_modified = Column(DateTime)
    image = Column(Unicode(255))
    manufacturer_type = Column(Integer)
    name = Column(Unicode(255))
    priority = Column(Integer)
    seo_slug = Column(Unicode(255))
    sort_order = Column(Integer)
    status = Column(Integer)
    manufacturer_description = relationship(
        "ManufacturerDescription", backref="manufacturer"
    )
    meta_tree_descriptions = relationship(
        "CategoryMetaTreeDescriptions", backref="manufacturer"
    )

    @property
    def slug(self):
        return slugify(self.name.replace("/", "-"))


class ManufacturerDescription(Base):
    __tablename__ = "manufacturer_description"
    manufacturer_description_id = Column(Integer, primary_key=True, nullable=False)
    manufacturer_id = Column(
        Integer, ForeignKey("manufacturer.manufacturer_id"), index=True
    )
    language_id = Column(Integer)
    description = Column(Text)
    meta_description = Column(Unicode(255))
    tag = Column(Text)
    u_title = Column(Unicode(255))
    u_h1 = Column(Unicode(255))


class ManufacturerToDiscount(Base):
    __tablename__ = "manufacturer_to_discount"
    manufacturer_discount_id = Column(Integer, nullable=False, primary_key=True)
    discount_id = Column(Integer, ForeignKey("discount.discount_id"), index=True)
    manufacturer_id = Column(
        Integer,
        ForeignKey("manufacturer.manufacturer_id", ondelete="CASCADE"),
        index=True,
    )


class ManufacturerToTag(Base):
    __tablename__ = "manufacturer_to_tag"
    manufacturer_tag_id = Column(Integer, primary_key=True, nullable=False)
    tag_id = Column(
        Integer,
        ForeignKey("tag.tag_id"),
        index=True,
    )
