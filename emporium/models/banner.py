from sqlalchemy import Boolean, Column, Date, DateTime, ForeignKey, Integer, Unicode

from .meta import Base


class Banner(Base):
    __tablename__ = "banner"
    banner_id = Column(Integer, primary_key=True)
    caption = Column(Unicode(32))
    caption_secondary = Column(Unicode(32))
    creator_id = Column(ForeignKey("users.id"), nullable=False)
    date_added = Column(DateTime)
    date_end = Column(Date, index=True)
    date_modified = Column(DateTime)
    date_start = Column(Date, index=True)
    image = Column(Unicode(255), nullable=False)
    order = Column(Integer)
    page = Column(Integer, index=True)
    status = Column(Boolean, nullable=False, index=True)
    url = Column(Unicode(255), nullable=False)
    url_secondary = Column(Unicode(255))
