from sqlalchemy import Boolean, Column, ForeignKey, Integer, Text, Unicode

from sqlalchemy.orm import relationship

from .meta import Base


class File(Base):
    __tablename__ = "file"
    file_id = Column(Integer, primary_key=True, nullable=False)
    file_name = Column(Unicode(255))
    file_path = Column(Unicode(255))
    file_permission_id = Column(
        Integer, ForeignKey("file_permission.file_permission_id")
    )
    file_type_id = Column(Integer, ForeignKey("file_type.file_type_id"))
    original_file_name = Column(Unicode(255))
    file_name_display_mode_id = Column(
        Integer, ForeignKey("file_name_display_mode.file_name_display_mode_id")
    )


class FileNameDisplayMode(Base):
    __tablename__ = "file_name_display_mode"
    file_name_display_mode_id = Column(Integer, primary_key=True, nullable=False)
    description = Column(Text)
    name = Column(Unicode(255))
    sort_order = Column(Integer)
    status = Column(Integer)


class FileType(Base):
    __tablename__ = "file_type"
    file_type_id = Column(Integer, primary_key=True, nullable=False)
    description = Column(Text)
    file_type_name = Column(Unicode(255))
    sort_order = Column(Integer)
    status = Column(Integer)
    # original_file_name = Column(Unicode(255))


class FilePermission(Base):
    __tablename__ = "file_permission"
    file_permission_id = Column(Integer, primary_key=True, nullable=False)
    description = Column(Text)
    name = Column(Unicode(255))
    sort_order = Column(Integer)
    status = Column(Integer)
    # original_file_name = Column(Unicode(255))
