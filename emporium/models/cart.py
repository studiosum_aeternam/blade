from sqlalchemy import Column, DateTime, Integer, PickleType, JSON

from sqlalchemy.dialects.postgresql import JSONB

from .meta import Base


class Cart(Base):
    __tablename__ = "cart"
    cart_id = Column(Integer, primary_key=True, nullable=False)
    customer_id = Column(Integer)
    products = Column(PickleType)
    products_json = Column(JSONB)
    date_creation = Column(DateTime)
    date_accessed = Column(DateTime)
