from sqlalchemy import (
    Column,
    Integer,
    Text,
    Unicode,
)

from .meta import Base


class Storage(Base):
    __tablename__ = "storage"
    storage_id = Column(Integer, primary_key=True, nullable=False)
    description = Column(Text)
    name = Column(Unicode(255), index=True)
    order = Column(Integer)
    status = Column(Integer)
    storage_type = Column(Integer)
