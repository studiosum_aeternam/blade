from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    Unicode,
    UnicodeText,
)

from .meta import Base


class Post(Base):
    __tablename__ = "post"
    post_id = Column(Integer, primary_key=True, nullable=False)
    content = Column(UnicodeText)
    creator_id = Column(ForeignKey("users.id"), nullable=False)
    date_added = Column(DateTime)
    date_modified = Column(DateTime)
    date_published = Column(DateTime)
    excerpt = Column(UnicodeText)
    h1 = Column(Unicode(255))
    image = Column(Unicode(255))
    language_id = Column(Integer, index=True)
    meta_description = Column(UnicodeText)
    seo_slug = Column(Unicode(255))
    sort_order = Column(Integer, index=True)
    status = Column(Integer, nullable=False, index=True)
    title = Column(Unicode(255))
