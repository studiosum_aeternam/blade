from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    Text,
    Unicode,
    UnicodeText,
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property

from .meta import Base


class Category(Base):
    __tablename__ = "category"
    category_id = Column(Integer, primary_key=True, nullable=False)
    # column = Column(Integer)
    date_added = Column(DateTime)
    date_modified = Column(DateTime)
    image = Column(Unicode(255))
    name = Column(Unicode(255))
    parent_id = Column(Integer, index=True)
    placeholder = Column(Boolean, default=False)
    sort_order = Column(Integer)
    status = Column(Integer)
    category_type = Column(Integer)
    visibility = Column(Integer)

    category_description = relationship("CategoryDescription", backref="category")
    meta_tree_descriptions = relationship(
        "CategoryMetaTreeDescriptions", backref="category"
    )

    @hybrid_property
    def slug(self):
        return self.name

    @slug.expression
    def slug(cls):
        return cls.name


class CategoryDescription(Base):
    __tablename__ = "category_description"
    category_description_id = Column(Integer, primary_key=True, nullable=False)
    category_id = Column(
        Integer, ForeignKey("category.category_id", ondelete="CASCADE"), index=True
    )
    description = Column(Text)
    description_long = Column(Text)
    language_id = Column(Integer)
    meta_description = Column(Unicode(255))
    seo_slug = Column(Unicode(255))
    u_h1 = Column(Unicode(255))
    u_title = Column(Unicode(255))


class CategoryMetaTreeDescriptions(Base):
    __tablename__ = "category_meta_tree_descriptions"
    category_meta_tree_description_id = Column(
        Integer, nullable=False, primary_key=True
    )
    category_id = Column(
        Integer,
        ForeignKey("category.category_id", ondelete="CASCADE"),
        index=True,
        nullable=True,
    )
    band_id = Column(
        Integer,
        ForeignKey("band.band_id", ondelete="CASCADE"),
        index=True,
        nullable=True,
    )
    collection_id = Column(
        Integer,
        ForeignKey("collection.collection_id", ondelete="CASCADE"),
        index=True,
        nullable=True,
    )
    description = Column(UnicodeText)
    description_long = Column(Text)
    language_id = Column(Integer)
    manufacturer_id = Column(
        Integer,
        ForeignKey("manufacturer.manufacturer_id", ondelete="CASCADE"),
        index=True,
        nullable=True,
    )
    meta_description = Column(Unicode(255))
    seo_slug = Column(Unicode(255))
    u_h1 = Column(Unicode(255))
    u_title = Column(Unicode(255))


class CategoryToTag(Base):
    __tablename__ = "category_to_tag"
    category_tag_id = Column(Integer, primary_key=True, nullable=False)
    tag_id = Column(
        Integer,
        ForeignKey("tag.tag_id"),
        index=True,
    )
