from sqlalchemy import Column, DateTime, ForeignKey, Integer, Text, text, Unicode
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Index
from slugify import slugify

from .meta import Base


class Collection(Base):
    __tablename__ = "collection"
    collection_id = Column(Integer, primary_key=True, nullable=False)
    date_added = Column(DateTime)
    date_modified = Column(DateTime)
    image = Column(Unicode(255))
    manufacturer_id = Column(
        Integer, ForeignKey("manufacturer.manufacturer_id", ondelete="CASCADE")
    )
    name = Column(Unicode(255))
    priority = Column(Integer)
    sort_order = Column(Integer)
    status = Column(Integer, index=True)
    meta_tree_descriptions = relationship(
        "CategoryMetaTreeDescriptions", backref="collection"
    )

    Index(
        "collection_id_name_idx",
        collection_id,
        name,
        postgresql_where=text("status = 1"),
    )


class CollectionDescription(Base):
    __tablename__ = "collection_description"
    collection_description_id = Column(Integer, primary_key=True, nullable=False)
    collection_id = Column(
        Integer,
        ForeignKey("collection.collection_id", ondelete="CASCADE"),
        index=True,
    )
    language_id = Column(Integer)
    name = Column(Unicode(255))
    description = Column(Text)
    description_long = Column(Text)
    meta_description = Column(Unicode(255))
    u_title = Column(Unicode(255))
    u_h1 = Column(Unicode(255))
    seo_slug = Column(Unicode(255))

    @property
    def slug(self):
        return slugify(self.model)


class CollectionToCollection(Base):
    __tablename__ = "collection_to_collection"
    collection_collection_id = Column(Integer, nullable=False, primary_key=True)
    collection1_id = Column(
        Integer,
        ForeignKey("collection.collection_id", ondelete="CASCADE"),
        nullable=False,
        index=True,
    )
    collection2_id = Column(
        Integer,
        ForeignKey("collection.collection_id", ondelete="CASCADE"),
        nullable=False,
        index=True,
    )

    collection1 = relationship(
        "Collection", foreign_keys=[collection1_id], cascade="all,delete"
    )
    collection2 = relationship(
        "Collection", foreign_keys=[collection2_id], cascade="all,delete"
    )


class CollectionToTag(Base):
    __tablename__ = "collection_to_tag"
    collection_tag_id = Column(Integer, primary_key=True, nullable=False)
    tag_id = Column(
        Integer,
        ForeignKey("tag.tag_id"),
        index=True,
    )
