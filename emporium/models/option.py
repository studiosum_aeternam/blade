from sqlalchemy import Column, ForeignKey, Integer, Text, Unicode
from sqlalchemy.orm import relationship

from .meta import Base


class Option(Base):
    __tablename__ = "option"
    option_id = Column(Integer, primary_key=True, nullable=False)
    option_group_id = Column(
        Integer, ForeignKey("option_group.option_group_id"), index=True
    )
    priority = Column(Integer)
    status = Column(Integer)
    option_type = Column(Integer)
    option_description = relationship("OptionDescription", backref="option")


class OptionDescription(Base):
    __tablename__ = "option_description"
    option_description_id = Column(Integer, nullable=False, primary_key=True)
    description = Column(Text)
    language_id = Column(Integer, index=True)
    name = Column(Unicode(255), index=True)
    option_id = Column(Integer, ForeignKey("option.option_id"))


class OptionGroup(Base):
    __tablename__ = "option_group"
    option_group_id = Column(Integer, primary_key=True, nullable=False)
    status = Column(Integer)
    option_type = Column(Integer)
    sort_order = Column(Integer)
    option = relationship("Option", backref="option_group")
    option_group_description = relationship(
        "OptionGroupDescription", backref="option_group"
    )
    option = relationship("Option", backref="option_group")


class OptionGroupDescription(Base):
    __tablename__ = "option_group_description"
    option_description_group_id = Column(Integer, nullable=False, primary_key=True)
    option_group_id = Column(Integer, ForeignKey("option_group.option_group_id"))
    language_id = Column(Integer, index=True)
    name = Column(Unicode(255), index=True)
    description = Column(Text)


class OptionToTag(Base):
    __tablename__ = "option_to_tag"
    option_tag_id = Column(Integer, primary_key=True, nullable=False)
    tag_id = Column(
        Integer,
        ForeignKey("tag.tag_id"),
        index=True,
    )
