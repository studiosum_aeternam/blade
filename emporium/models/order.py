from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    Index,
    JSON,
    text,
    Text,
    Unicode,
)
from sqlalchemy.dialects.postgresql import JSONB

from .meta import Base


class Order(Base):
    __tablename__ = "order"
    additional_info = Column(Text)
    client_json = Column(JSONB)
    customer_id = Column(Integer)
    customer_type = Column(Integer)
    date_accessed = Column(DateTime)
    date_creation = Column(DateTime)
    date_ordered = Column(DateTime)
    delivery_json = Column(JSONB)
    digital_delivery_active = Column(Boolean)
    invoice = Column(Integer)
    order_id = Column(Integer, primary_key=True, nullable=False)
    params_json = Column(JSONB)
    payment = Column(Integer)
    payment_params_json = Column(JSONB)
    physical_delivery_method = Column(Integer)
    products_json = Column(JSONB)
    registration_type = Column(Text)  # TODO change to int and differentiate
    status = Column(Integer)
    transport_params_json = Column(JSONB)
    uuid = Column(Unicode(37))

    Index(
        "order_id_date_status_idx",
        order_id,
        date_ordered,
        status,
        postgresql_where=text("status > 1"),
        postgresql_ops={"date_ordered": "DESC"},
    )


class OrderToTransport(Base):
    __tablename__ = "order_to_transport"
    order_transport_id = Column(Integer, nullable=False, primary_key=True)
    discount_id = Column(Integer, ForeignKey("discount.discount_id"), index=True)
    manufacturer_id = Column(
        Integer,
        ForeignKey("manufacturer.manufacturer_id", ondelete="CASCADE"),
        index=True,
    )


class OrderHistory(Base):
    __tablename__ = "order_history"
    order_history_id = Column(Integer, primary_key=True, nullable=False)
    additional_info = Column(Text)
    date = Column(DateTime)
    mail_sent = Column(Boolean)
    status_type = Column(Integer)  # 0 is universal, 1 physical, 2 digital?
    order_id = Column(Integer, ForeignKey("order.order_id"))
    status_id = Column(Integer, ForeignKey("status.status_id"))

    Index(
        "order_history_id_status_idx",
        order_history_id,
        status_id,
        postgresql_where=text("status_id > 1"),
        postgresql_ops={"date": "DESC"},
    )


class OrderToSpecial(Base):
    __tablename__ = "order_to_special"
    order_special_id = Column(Integer, nullable=False, primary_key=True)
    order_id = Column(
        Integer, ForeignKey("order.order_id", ondelete="CASCADE"), index=True
    )
    product_special_id = Column(
        Integer,
        ForeignKey("product_to_special.product_special_id", ondelete="CASCADE"),
        index=True,
    )
    quantity = Column(Integer)


class OrderToUUID(Base):
    __tablename__ = "order_to_uuid"
    order_uuid_id = Column(Integer, nullable=False, primary_key=True)
    order_id = Column(
        Integer, ForeignKey("order.order_id", ondelete="CASCADE"), index=True
    )
    product_id = Column(
        Integer, ForeignKey("product.product_id", ondelete="CASCADE"), index=True
    )
    order_product_uuid = Column(Unicode(64), index=True)
    date_used = Column(DateTime)
    status = Column(Integer)
