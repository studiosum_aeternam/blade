from sqlalchemy import Column, ForeignKey, Integer, Text, Unicode

from .meta import Base


class Status(Base):
    __tablename__ = "status"
    status_id = Column(Integer, primary_key=True, nullable=False)
    status_group_id = Column(
        Integer, ForeignKey("status_group.status_group_id"), index=True
    )
    priority = Column(Integer)
    status = Column(Integer)
    status_type = Column(Integer)


class StatusDescription(Base):
    __tablename__ = "status_description"
    status_description_id = Column(Integer, nullable=False, primary_key=True)
    status_id = Column(Integer, ForeignKey("status.status_id"))
    description = Column(Text)
    language_id = Column(Integer, index=True)
    name = Column(Unicode(255), index=True)


class StatusGroup(Base):
    __tablename__ = "status_group"
    status_group_id = Column(Integer, primary_key=True, nullable=False)
    sort_order = Column(Integer)
    status = Column(Integer)
    status_type = Column(Integer)


class StatusGroupDescription(Base):
    __tablename__ = "status_group_description"
    status_description_group_id = Column(Integer, nullable=False, primary_key=True)
    description = Column(Text)
    language_id = Column(Integer, index=True)
    name = Column(Unicode(255), index=True)
    status_group_id = Column(Integer, ForeignKey("status_group.status_group_id"))
