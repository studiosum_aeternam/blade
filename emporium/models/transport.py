from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    JSON,
    PickleType,
    Text,
    Unicode,
)
from sqlalchemy.dialects.postgresql import JSONB

from .meta import Base


class Transport(Base):
    __tablename__ = "transport"
    transport_id = Column(Integer, primary_key=True, nullable=False)
    # special_method = Column(Boolean, default=False)
    description = Column(Text)
    excerpt = Column(Unicode(64))
    module_name = Column(Unicode(32))
    module_settings = Column(JSONB)
    module_private_settings = Column(JSONB)
    name = Column(Unicode(64))
    status = Column(Boolean, nullable=False, default=True)
    transport_method = Column(Integer)


class TransportSlot(Base):
    __tablename__ = "transport_size_slot"
    transport_slot_id = Column(Integer, primary_key=True, nullable=False)
    applicability = Column(Integer)
    special_method = Column(Boolean)
    description = Column(Text)
    excerpt = Column(Unicode(255))
    name = Column(Unicode(64))
    order = Column(Integer)
    slot_name = Column(Unicode(64))
    status = Column(Boolean)
    module_settings = Column(JSONB)
    module_private_settings = Column(JSONB)
    transport_method = Column(Integer)
    transport_id = Column(Integer, ForeignKey("transport.transport_id"))
