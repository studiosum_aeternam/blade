import time


from pyramid.config import Configurator
from pyramid_mailer.mailer import Mailer
from pyramid.static import QueryStringConstantCacheBuster


def main(global_config, **settings):
    """This function returns a Pyramid WSGI application."""
    static_location = settings.get("static_location", "emporium:templates/blade/static")
    emporatorium_static_location = settings.get(
        "emporatorium_static", "emporium:templates/emporatorium/static"
    )
    with Configurator(settings=settings) as config:
        # config.add_static_view('emporium:static', 'static', cache_max_age=72000)
        config.include("pyramid_session_redis")
        config.include("pyramid_jinja2")
        config.include("pyramid_forksafe")
        config.add_jinja2_renderer(".html", settings_prefix="jinja2.")
        config.add_subscriber(
            "emporium.subscribers.add_renderer_globals", "pyramid.events.BeforeRender"
        )
        config.add_subscriber(
            "emporium.subscribers.add_localizer", "pyramid.events.NewRequest"
        )
        config.add_static_view(
            name="static_files", path=static_location, cache_max_age=7200
        )
        config.add_static_view(
            name="panel_static_files",
            path=emporatorium_static_location,
            cache_max_age=7200,
        )
        config.add_cache_buster(
            static_location, QueryStringConstantCacheBuster(str(int(time.time())))
        )
        config.include(".models")
        config.include(".routes")
        config.include(".security")
        # config.include('pyramid_exclog')
        config.registry["mailer"] = Mailer.from_settings(settings)
        config.scan()
    return config.make_wsgi_app()
