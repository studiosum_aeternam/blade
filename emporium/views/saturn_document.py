from pyramid.view import view_config


@view_config(route_name="api_saturn_document", xhr=True, request_method="GET")
def api_saturn_document(request):
    pass


@view_config(route_name="api_saturn_document_action", xhr=True, request_method="GET")
def api_saturn_document_action(request):
    pass
