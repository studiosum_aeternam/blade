from datetime import date, datetime
import contextlib
import os

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from slugify import slugify

from ..forms import EditContactForm  # EditContactForm
from emporium import helpers
from emporium import models
from emporium import services


@view_config(
    route_name="contact_action",
    match_param="action=create",
    renderer="contact/contact_edit.html",
    permission="edit",
)
@view_config(
    route_name="contact_action",
    match_param="action=edit",
    renderer="contact/contact_edit.html",
    permission="edit",
)
def edit_contact(request):
    options = helpers.SettingHelpers.default_options(request)
    contact_id = request.params.get("contact_id", -1)
    contact = (
        services.ContactService.contact_filter_id(request, contact_id)
        if options.action == "edit"
        else models.Customer()
    )
    form = EditContactForm(request.POST)
    if request.method == "POST" and form.validate():
        contact.creator_id = request.authenticated_userid
        contact.date_available = (
            date(*map(int, request.params.get("date_available").split("-")))
            if request.params.get("date_available")
            else None
        )
        contact.date_modified = datetime.now()
        contact.first_name = form.first_name.data
        contact.last_name = form.last_name.data
        contact.status = form.status.data
        if options.action == "create":
            contact.date_added = datetime.now()
            request.dbsession.add(contact)
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="contact_list")
    return dict(
        form=form,
        action=options.action,
        contact=(contact if options.action == "edit" else False),
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
        options=options,
    )


@view_config(
    route_name="contact_delete", renderer="json", permission="edit", xhr="True"
)
def contact_delete(request):
    contact_id = request.POST.get("contact_id")
    contact = services.ContactService.contact_filter_id(request, contact_id)
    if contact_id:
        with contextlib.suppress(FileNotFoundError):
            if contact.image:
                os.remove(helpers.CommonHelpers.asset_path("contacts") + contact.image)
        request.dbsession.delete(contact)
        helpers.CacheHelpers.flush_cache()


@view_config(
    route_name="contact_list",
    renderer="contact/contact_list.html",
    permission="edit",
)
def contact_list(request):
    contacts = services.ContactService.contacts_all(request)
    filter_params = dict(
        items_per_page=36,
        # parameters=parameters,
        # sort_method=sort_method,
        # status=status,
        term="",
    )
    return dict(
        contacts=contacts,
        filter_params=filter_params,
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
    )
