import shutil
import os

from pyramid.view import view_config
from slugify import slugify

from ..services import ManufacturerService, ProductService


@view_config(
    route_name="image_folders",
    renderer="common/image_folders.html",
    permission="edit",
)
def image_folders(request):
    pass
