from datetime import datetime, timedelta
import json
import urllib.parse
from pyramid.view import view_config
from webob import Request

from ..helpers import SettingHelpers
from ..services import SettingsService
from ..views.saturn_login import SaturnAuthentication


@view_config(route_name="api_saturn_product", permission="edit")
def api_saturn_product(request):
    ean_list = request.params.get("ean_list")
    settings = SettingHelpers.shop_settings(SettingsService.all(request))
    saturn_authentication = SaturnAuthentication()
    headers = saturn_authentication.session_token(request)
    location = (
        settings["saturn_api_url"]
        + "/product/findProduct?field=name%2Cean&productsEan="
        + ean_list
    )
    api_request = Request.blank(location, headers=headers)
    return api_request.send()


@view_config(route_name="api_saturn_get_product_prices", permission="edit")
def api_saturn_get_product_prices(request, update_type=None):
    settings = SettingHelpers.shop_settings(SettingsService.all(request))
    current_query_time = (datetime.utcnow() - timedelta(minutes=2)).strftime(
        "%Y-%m-%d %H:%M:%S"
    )  # Adjusted for API cache
    saturn_authentication = SaturnAuthentication()
    headers = saturn_authentication.session_token(request)
    location = (
        settings["saturn_api_url"]
        + "/product/findProduct?field=ean%2Cinstock%2Cretailpricenet%2Cqty"
    )
    current_timestamp = SettingsService.by_key(request, 1, "saturn_timestamp")
    if current_timestamp and update_type:
        location += "&" + urllib.parse.urlencode(
            {
                "modificationTimestamp": current_timestamp.value,
            }
        ).replace("+", "%20")
    api_request = Request.blank(location, headers=headers)
    f = api_request.send()
    if f.status != "200 OK" or not json.loads(f.body)["Items"]:
        return False
    current_timestamp = SettingsService.by_key(request, 1, "saturn_timestamp")
    current_timestamp.value = current_query_time
    product_dict = [d for d in json.loads(f.body)["Items"] if d.get("Ean")]
    for item in product_dict:
        item["price"] = item["RetailPriceNet"]["Value"]
    return {d["Ean"]: d for d in json.loads(f.body)["Items"] if d.get("Ean")}


@view_config(route_name="api_saturn_get_product_stock", permission="edit")
def api_saturn_get_product_stock(request, update_type=None):
    settings = SettingHelpers.shop_settings(SettingsService.all(request))
    current_query_time = (datetime.utcnow() - timedelta(minutes=2)).strftime(
        "%Y-%m-%d %H:%M:%S"
    )  # Adjusted for API cache
    saturn_authentication = SaturnAuthentication()
    headers = saturn_authentication.session_token(request)
    location = (
        settings["saturn_api_url"] + "/product/findProduct?field=ean%2Cinstock%2Cqty"
    )
    current_timestamp = SettingsService.by_key(request, 1, "saturn_timestamp")
    if current_timestamp and update_type:
        location += "&" + urllib.parse.urlencode(
            {
                "modificationTimestamp": current_timestamp.value,
            }
        ).replace("+", "%20")
    api_request = Request.blank(location, headers=headers)
    f = api_request.send()
    if f.status != "200 OK" or not json.loads(f.body)["Items"]:
        return False
    current_timestamp = SettingsService.by_key(request, 1, "saturn_timestamp")
    current_timestamp.value = current_query_time
    # product_dict = [d for d in json.loads(f.body)["Items"] if d.get("Ean")]
    # for item in product_dict:
    # item["price"] = item["RetailPriceNet"]["Value"]
    return {d["Ean"]: d for d in json.loads(f.body)["Items"] if d.get("Ean")}


@view_config(route_name="api_saturn_get_single_product_stock", permission="edit")
def api_saturn_get_single_product_stock(request, update_type=None):
    ean = request.params.get("single_stock_update_key_ean")
    settings = SettingHelpers.shop_settings(SettingsService.all(request))
    current_query_time = (datetime.utcnow() - timedelta(minutes=2)).strftime(
        "%Y-%m-%d %H:%M:%S"
    )  # Adjusted for API cache
    saturn_authentication = SaturnAuthentication()
    headers = saturn_authentication.session_token(request)
    location = (
        settings["saturn_api_url"]
        + "/product/findProduct?field=name%2Cean%2Cqty&productsEan="
        + ean
    )
    current_timestamp = SettingsService.by_key(request, 1, "saturn_timestamp")
    if current_timestamp and update_type:
        location += "&" + urllib.parse.urlencode(
            {
                "modificationTimestamp": current_timestamp.value,
            }
        ).replace("+", "%20")
    api_request = Request.blank(location, headers=headers)
    f = api_request.send()
    if f.status != "200 OK" or not json.loads(f.body)["Items"]:
        return False
    current_timestamp = SettingsService.by_key(request, 1, "saturn_timestamp")
    current_timestamp.value = current_query_time
    return {d["Ean"]: d for d in json.loads(f.body)["Items"] if d.get("Ean")}
