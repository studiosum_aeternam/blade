from datetime import datetime
import contextlib
import os
import json
import pathlib
from uuid import uuid4

from pyramid.view import view_config
from slugify import slugify

from emporium import helpers, models, operations, services


@view_config(route_name="image_delete", renderer="json", permission="edit", xhr="True")
def image_delete(request):
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    image_id = request.params.get("image_id")
    product_id = request.params.get("product_id")
    image_type = request.params.get("image_type")
    manufacturer_seo_slug = request.params.get("manufacturer_seo_slug")
    if image_type == "product" and product_id:
        product = services.ProductService.product_filter_product_id(request, product_id)
        helpers.ImageHelpers.delete_image_all_resolutions_and_types(
            manufacturer_seo_slug, product.image
        )
        product.image = None
    elif image_type == "product_additional":
        image = services.ProductToImageService.product_to_image_filter_image_id(
            request, image_id
        )
        helpers.ImageHelpers.delete_image_all_resolutions_and_types(
            manufacturer_seo_slug, image.image
        )
        request.dbsession.delete(image)
    elif image_type == "banner":
        banner = services.BannerService.baner_by_id(request, image_id)
        with contextlib.suppress(FileNotFoundError):
            os.remove(helpers.CommonHelpers.asset_path("banners") + banner.image)
        banner.image = None
    elif image_type == "post":
        post = services.PostService.post_full_filter_by_post_id(request, image_id)
        with contextlib.suppress(FileNotFoundError):
            os.remove(helpers.CommonHelpers.asset_path("posts") + post.image)
        post.image = None
    request.dbsession.flush()
    helpers.CacheHelpers.flush_cache()
    return {}


@view_config(route_name="image_upload", renderer="json", permission="edit", xhr="True")
def image_upload(request):
    source = request.params.get("source")
    upload_type = request.params.get("upload_type")
    file_type = request.params.get("file_type")
    path_prefix = ""
    if upload_type == "post":
        folder_date = str(datetime.now().strftime("%Y/%m"))
        path_prefix = f"{folder_date}/"
    # elif upload_source == "image" and upload_type == "post":
    #     pass
    title = request.params.get("title")
    manufacturer_seo_slug = request.params.get("manufacturer_seo_slug")
    if (
        request.params.get("band_seo_slug")
        and request.params.get("band_seo_slug") != "undefined"
    ):
        manufacturer_seo_slug += "/" + request.params.get("band_seo_slug")
    multiple_images = request.params.getall(f"{file_type}_content")
    upload_status = 400
    if multiple_images:
        path_prefix += slugify(title)
        path_list = []
        product_images = []
        file_uuid = str(uuid4())[:6]
        file_path = f"{path_prefix}_{file_uuid}"
        if upload_type in ("main", "banner"):
            operations.ImageOperations.save_image_using_current_system_settings(
                file_path,
                multiple_images[0],
                manufacturer_seo_slug,
                path_list,
                ("source" if upload_type == "main" else "banners"),
            )
        elif upload_type == "extra":
            image_count = request.params.get("file_count", 0)
            for start, image in enumerate(
                multiple_images, len(multiple_images) + int(image_count) + 1
            ):
                operations.ImageOperations.save_image_using_current_system_settings(
                    file_path,
                    image,
                    manufacturer_seo_slug,
                    path_list,
                    "source",
                )
                product_images.append(models.ProductToImage(image=file_path))
            request.dbsession.add_all(product_images)
            request.dbsession.flush()
        # else:
        #     image, file_ext = helpers.FileHelpers.get_file_data_with_extension(
        #         multiple_images[0]
        #     )

        #     pathlib.Path(helpers.CommonHelpers.asset_path("posts") + folder_date).mkdir(
        #         parents=True, exist_ok=True
        #     )
        #     upload_status = helpers.ImageHelpers.image_save(
        #         image, file_ext, path_prefix + file_ext, "posts"
        #     )
        #     path_list.append(path_prefix + file_ext)
        return path_list, {item.product_image_id: item.image for item in product_images}
