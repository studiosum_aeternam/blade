from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from emporium import forms
from emporium import helpers
from emporium import models
from emporium import services


@view_config(
    route_name="special_list",
    renderer="special/special_list.html",
    permission="edit",
)
def special_list(request):
    special_list = services.SpecialService.get_paginator(request)
    return dict(
        special_list=special_list,
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
    )


@view_config(
    route_name="special_action",
    match_param="action=create",
    renderer="special/special_edit.html",
    permission="edit",
)
def add_special(request):
    form = forms.SpecialCreateForm(request.POST)
    if request.method == "POST" and form.validate():
        special = models.Special()
        special_description = models.SpecialDescription()()
        form.populate_obj(special)
        special.status = 1
        if form.price_changing.data == "on":
            special.price_changing = 1
        request.dbsession.add(special)
        request.dbsession.flush()
        form.populate_obj(special_description)
        special_description.special_id = special.special_id
        request.dbsession.add(special_description)
        request.dbsession.flush()
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="special_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
    )


@view_config(
    route_name="special_action",
    match_param="action=edit",
    renderer="special/special_edit.html",
    permission="edit",
)
def edit_special(request):
    special_id = request.params.get("special_id", -1)
    special = services.SpecialService.by_id(request, special_id)
    special_description = services.SpecialService.special_description_item_filter_id(
        request, special_id
    )
    form = forms.SpecialUpdateForm(request.POST, special)
    if request.method == "POST" and form.validate():
        request.dbsession.flush()
        form.populate_obj(special)
        if form.price_changing.data == "on":
            special.price_changing = 1
        request.dbsession.flush()
        form.populate_obj(special_description)
        request.dbsession.flush()
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="special_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        special=special,
        special_description=special_description,
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
    )


@view_config(
    route_name="product_special_delete",
    renderer="json",
    permission="edit",
    xhr="True",
)
def product_special_delete(request):
    product_special_id = request.POST.get("product_special_id")
    if special := services.ProductToSpecialService.by_special_product(
        request, product_special_id
    ):
        request.dbsession.delete(special)
        helpers.CacheHelpers.flush_cache()
