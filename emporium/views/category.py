"""
Category views (filter search is also a category view)
"""
from datetime import datetime
import json
from time import perf_counter

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config


from emporium import data_containers, forms, helpers, models, services, operations


@view_config(route_name="s_category", renderer="category/s_category.html")
@view_config(route_name="s_search", renderer="category/s_category.html")
@view_config(route_name="s_tag", renderer="category/s_category.html")
def category_page(request):
    """
    Category/search default view, filtered view for search and category
    """
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    if request.GET.get("category_id"):
        seo_slug = services.CategoryService.category_seo_slug_filter_id(
            request, request.GET.get("category_id"), settings
        )
        return HTTPFound(
            location=request.route_url(
                "s_category",
                category_id=request.GET.get("category_id"),
                seo_slug=seo_slug,
                _query=[
                    item
                    for item in request.GET.items()
                    if item[1] and item[0] != "category_id"
                ],
            )
        )
    category_id = request.matchdict.get("category_id") or False
    category = (
        services.CategoryService.category_id_name_parent_description_meta_seo_h1_title_filter_id(
            request, category_id, settings
        )
        if category_id
        else False
    )
    availability_attributes = services.AttributeService.availability_attributes(
        request, settings
    )
    query_items = [
        htem for htem in request.GET.items() if htem[1] != ("None" or len(htem[1] == 0))
    ]
    operations.FilterOperations.filter_items_per_page(request)
    filter_params = operations.FilterOperations.generate_filter_params(
        request, availability_attributes
    )
    display_mode = operations.FilterOperations.get_display_mode_params(
        request, filter_params
    )
    filter_params = helpers.CommonHelpers.term_search(filter_params)
    filter_params = helpers.CommonHelpers.term_filter_format(filter_params, query_items)
    filter_params = helpers.ProductHelpers.filter_params_dimensions(
        filter_params, query_items
    )
    operations.FilterOperations.filter_main_page_description(filter_params)
    operations.FilterOperations.filter_manufacturer_params(filter_params)
    operations.FilterOperations.filter_band_params(filter_params)
    operations.FilterOperations.filter_collection_params(filter_params)
    operations.FilterOperations.filter_attribute_params(filter_params)
    operations.FilterOperations.filter_price_params(filter_params, query_items)
    # Count products it's faster than querying products directly with returned records due to pagination requirements
    product_aggregator = services.ProductService.product_aggregator_v2(
        request, filter_params, settings
    )
    tax_dict = operations.TaxOperations.generate_tax_list(request, settings)
    searched_products = services.ProductService.products_paginator_filter_dynamic_v2(
        request,
        filter_params,
        settings,
        tax_dict,
        item_count=product_aggregator.item_count,
    )
    data_options = operations.ProductOperations.current_data_options(
        request,
        settings,
        filter_params,
        query_items,
        product_aggregator.availabilities,
        tax_dict,
    )
    manufacturer_extended = (
        operations.ManufacturerOperations.get_manufacturer_extended_data(
            request, filter_params
        )
        if filter_params.searchstring
        else []
    )
    collection_extended = (
        operations.CollectionOperations.get_collection_extended_data(
            request, filter_params
        )
        if filter_params.searchstring
        else []
    )
    data_options_filtered = operations.ProductOperations.return_product_related_data(
        request,
        settings,
        [item.product_id for item in searched_products.items],
        data_options.tax_dict,
    )
    product_listing = helpers.ProductHelpers.product_category_format(
        data_options,
        data_options_filtered,
        searched_products.items,
        settings,
    )
    meta_options = data_containers.MetaOptions(
        rel_link=request.route_url("s_search"),
        robots_meta="noindex, follow",
        item_count=product_aggregator.item_count,
    )
    filter_params.pager = searched_products.link_map()
    operations.FilterOperations.check_for_first_category_page(request, filter_params)
    misc_options = data_containers.MiscOptions(search_url="s_search")
    if category:
        filler = operations.CategoryOperations.category_front_page_description_and_meta(
            request, category, filter_params, misc_options, meta_options
        )
        if (
            filter_params.descripted_front_page
            and filter_params.sort_method
            == settings["catalog_settings"].get("default_sorting_method", "name_asc")
            and display_mode != "grid"
            and data_options.manufacturer_dict
            and filter_params.items_per_page > 0
        ):
            operations.CategoryOperations.category_manufacturer_front_page_description_and_meta(
                request,
                category,
                filter_params,
                misc_options,
                data_options,
                meta_options,
                filler,
                settings,
            )
    return dict(
        collection_extended=collection_extended,
        data_options=data_options,
        data_options_filtered=data_options_filtered,
        filter_params=filter_params,
        manufacturer_extended=manufacturer_extended,
        meta_options=meta_options,
        misc_options=misc_options,
        product_listing=product_listing,
        query=query_items,
        settings=settings,
    )


@view_config(
    route_name="category_action",
    match_param="action=create",
    renderer="category/category_edit.html",
    permission="edit",
)
def add_category(request):
    """
    View for category actions creation and edits
    """
    options = helpers.SettingHelpers.default_options(request)
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    categories_list = services.CategoryService.categories_categories_descriptions(
        request, settings
    )
    form = forms.CategoryCreateForm(request.POST)
    if request.method == "POST" and form.validate():
        category = operations.CategoryOperations.create_category(request, form)
        operations.ProductOperations.create_product_to_category(request, form, category)
        operations.CategoryOperations.create_category_meta_tree_description(
            request, form, category
        )
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="category_list")
    return dict(
        categories_list=categories_list,
        form=form,
        pro=[],
        products_ids=[],
        options=options,
        settings=helpers.SettingHelpers.shop_settings(
            services.SettingsService.all(request)
        ),
    )


def category_create(form):
    return models.Category(
        date_modified=datetime.now(),
        date_added=datetime.now(),
        image=forms.image.data,
        name=forms.name.data,
        parent_id=forms.parent_id.data if forms.parent_id.data != "0" else "",
        sort_order=forms.sort_order.data,
        status=forms.status.data,
    )


@view_config(
    route_name="category_action",
    match_param="action=edit",
    renderer="category/category_edit.html",
    permission="edit",
)
def edit_category(request):
    """
    Function for all category related actions create/edit - duplicate
    """
    options = helpers.SettingHelpers.default_options(request)
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    category_id = request.params.get("category_id", -1)
    category = services.CategoryService.category_filter_id(request, category_id)
    category_description = services.CategoryService.category_description_filter_id(
        request, category_id, 2, settings
    )
    categories_list = services.CategoryService.categories_categories_descriptions(
        request, settings
    )
    form = forms.CategoryUpdateForm(request.POST)
    product_list = services.ProductService.products_id_name_filter_category(
        request, category_id, settings
    )
    products = [{"id": item.product_id, "name": item.name} for item in product_list]
    products_ids = [item.product_id for item in product_list]
    if request.method == "POST" and form.validate():
        operations.CategoryOperations.update_category_data(category, form)
        operations.CategoryOperations.update_category_information(
            request, category_id, form, products_ids
        )
        operations.CategoryOperations.update_category_description_data(
            category_description, form
        )
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="category_list")
    return dict(
        categories_list=categories_list,
        category=category,
        category_description=category_description,
        form=form,
        products_json=json.dumps(products),
        products_ids=products_ids,
        options=options,
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
    )


@view_config(
    route_name="category_list",
    renderer="category/category_list.html",
    permission="edit",
)
def category_list(request):
    """
    View for all categories
    """
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    page = request.params.get("page", 1)
    sort_method = request.params.get("sort", "name_asc")
    number_categories = request.params.get("number_categories", 24)
    categories = (
        services.CategoryService.category_id_image_name_status_paginated_sorted(
            request, page, number_categories, sort_method
        )
    )
    categories_list = helpers.CategoryHelpers.categories_format(categories.items)
    return dict(
        categories_list=categories_list, categories=categories, settings=settings
    )


@view_config(
    route_name="category_delete", renderer="json", permission="edit", xhr="True"
)
def category_delete(request):
    """
    View for deletion of category
    """
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    category_id = request.POST.get("category_id")
    category = services.CategoryService.category_filter_id(request, category_id)
    product_to_category = services.ProductService.products_id_name_filter_category(
        request, category_id, settings
    )
    if category:
        request.dbsession.delete(category)
    if product_to_category:
        for item in product_to_category:
            request.dbsession.delete(item[1])
    helpers.CacheHelpers.flush_cache()


@view_config(
    route_name="category_meta_tree",
    renderer="category/category_meta_tree.html",
    permission="edit",
)
def category_meta_tree(request):
    """
    View of the category meta tree
    """
    category_id = request.params.get("category_id")
    collection_id = request.params.getall("collection_id")
    manufacturer_id = request.params.get("manufacturer_id")
    number_elements = request.params.get("number_elements", 48)
    page = request.params.get("page", 1)
    sort_method = request.params.get("sort", "name_asc")
    category_sequence = services.CategoryService.categories_categories_descriptions(
        request, True
    )
    collection_list = services.CollectionService.collection_id_name_filter_category(
        request, category_id
    )
    manufacturer_list = services.ManufacturerService.manufacturers_id_name(request)
    category_meta = services.CategoryService.category_meta_tree(
        request, page, number_elements, category_id, collection_id, manufacturer_id
    )
    filter_params = dict(
        category_id=category_id,
        collection_id=collection_id,
        manufacturer_id=manufacturer_id,
        sort_method=sort_method,
    )
    pager = category_meta.pager
    return dict(
        category_meta=category_meta,
        category_sequence=category_sequence,
        collection_list=collection_list,
        manufacturer_list=manufacturer_list,
        filter_params=filter_params,
        pager=pager,
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
    )
