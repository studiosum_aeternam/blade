import bcrypt

from pyramid.csrf import get_csrf_token
from pyramid.httpexceptions import HTTPFound, HTTPSeeOther
from pyramid.security import remember, forget
from pyramid.view import forbidden_view_config, view_config

from emporium import models
from ..services import CartService, CompareService, UserService, CustomerService
from ..operations import CartOperations, CompareOperations


@forbidden_view_config(renderer="40x/403.html")
def forbidden_view(request):
    identity = request.identity

    if not request.is_authenticated or not identity or identity.role == "customer":
        next_url = request.route_url("login", _query={"next": request.path_qs})
        return HTTPSeeOther(location=next_url, headers=forget(request))
    request.response.status = 403
    return {}


@view_config(route_name="login", renderer="common/login.html")
def login(request):
    if request.authenticated_userid:
        return HTTPFound(location=request.route_url("home"))
    else:
        return {"next_url": request.params.get("next", request.route_url("home"))}


@view_config(
    route_name="auth", match_param="action=in", renderer="string", request_method="POST"
)
@view_config(
    route_name="auth",
    match_param="action=out",
    renderer="string",
    request_method="POST",
)
def sign_in_out(request):
    login = request.POST.get("login")
    next_url = request.params.get("next", request.route_url("login"))
    if request.method == "POST":
        if request.matchdict["action"] == "in":
            user = UserService.by_login(login, request=request)
            if (
                user
                and user.check_password(request.POST.get("password"))
                and user.role == "emperor"
            ):
                headers = remember(request, user.id, tokens=[user.role])
                return HTTPFound(location=next_url, headers=headers)
        elif request.matchdict["action"] == "out":
            headers = forget(request)
            return HTTPFound(location=next_url, headers=headers)
        headers = forget(request)
        return HTTPFound(location=next_url, headers=headers)


@view_config(
    route_name="s_auth",
    match_param="action=in",
    renderer="string",
    request_method="POST",
)
@view_config(route_name="s_auth", match_param="action=out", renderer="string")
def customer_sign_in_out(request):
    mail = request.POST.get("mail")
    customer_type = request.POST.get("customer_type", 0)
    next_url = request.params.get("next")
    next_location = (
        next_url or request.environ.get("HTTP_REFERER") or request.route_url("s_home")
    )
    response = request.response
    headers = response.headers
    get_csrf_token(request)
    session = request.session
    if request.matchdict["action"] == "in":
        customer = CustomerService.customer_single_filter_mail_type(
            request, mail, customer_type
        )
        if customer and customer.check_password(request.POST.get("password")):
            headers = remember(request, customer.id, tokens=["customer"])
            # get_csrf_token(request)
            session["uid"] = customer.id
            CartOperations.cart_merge(request)
            CompareOperations.compare_merge(request)
            cookies = (
                "delivery_details",
                "invoice_type",
            )
            for _ in cookies:
                response.delete_cookie(_)
            session_variables = [
                "attempt",
                "cid",
                "client",
                "delivery",
                "login_error",
                "payment",
                "privacy_policy" "transport",
            ]
            for _ in session_variables:
                if session.get(_):
                    del session[_]
            session["order_type"] = "logged_buy"
        else:
            session["login_error"] = True
            if session.get("attempt"):
                session["attempt"] += 1
            else:
                session["attempt"] = 1
            if session["attempt"] > 5:
                session["login_error"] = "Blocked"
            else:
                headers = forget(request)
            return HTTPFound(location=next_location)
    elif request.matchdict["action"] == "out":
        cookies = ("auth_tkt", "delivery_details", "payment", "invoice_type")
        for _ in cookies:
            response.delete_cookie(_)
        session.invalidate()
        headers = forget(request)
    return HTTPFound(location=next_location, headers=headers)
