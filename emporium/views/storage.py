from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from ..forms import StorageForm
from ..helpers import CacheHelpers, SettingHelpers
from ..models import Storage
from ..services import StorageService, SettingsService


@view_config(
    route_name="storage_action",
    match_param="action=create",
    renderer="storage/storage_edit.html",
    permission="edit",
)
@view_config(
    route_name="storage_action",
    match_param="action=edit",
    renderer="storage/storage_edit.html",
    permission="edit",
)
def edit_storage(request):
    form = StorageForm(request.POST)
    options = SettingHelpers.default_options(request)
    storage_id = request.params.get("storage_id", -1)
    storage = (
        StorageService.storage_filter_id(request, storage_id)
        if options.action == "edit"
        else Storage()
    )
    if request.method == "POST":  # and form.validate():
        storage.creator_id = request.authenticated_userid
        storage.order = request.params.get("order")
        storage.name = request.params.get("name")
        storage.description = request.params.get("description")
        storage.status = 1 if request.params.get("status") == "on" else 0
        storage.storage_type = request.params.get("storage_type")
        if options.action == "create":
            request.dbsession.add(storage)
            request.dbsession.flush()
        CacheHelpers.flush_cache()
        return HTTPFound(location="storage_list")
    return dict(
        action=options.action,
        storage=storage if options.action == "edit" else False,
        form=form,
        options=options,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


@view_config(
    route_name="storage_delete", renderer="json", permission="edit", xhr="True"
)
def storage_delete(request):
    storage_id = request.POST.get("storage_id")
    if storage := StorageService.storage_filter_id(request, storage_id):
        request.dbsession.delete(storage)
        CacheHelpers.flush_cache()


@view_config(
    route_name="storage_list",
    renderer="storage/storage_list.html",
    permission="edit",
)
def storage_list(request):
    page = request.params.get("page", 1)
    items_per_page = request.params.get("items_per_page", 32)
    storage_list = StorageService.storage_paginator(request, page, items_per_page)
    return dict(
        storage_list=storage_list,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )
