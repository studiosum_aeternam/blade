"""
Product views
"""
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
import itertools
import json
from time import perf_counter

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from slugify import slugify

from emporium import (
    data_containers,
    forms,
    helpers,
    models,
    services,
    views,
    operations,
)


@view_config(route_name="s_product", renderer="product/s_product.html", http_cache=1)
def product_view(request):
    """
    Product view
    """
    filter_params = data_containers.FilterOptions()
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    meta_options = data_containers.MetaOptions(rel_link=request.route_url("s_search"))
    misc_options = data_containers.MiscOptions(search_url="s_search")
    product = operations.ProductOperations.return_product(
        request,
        settings,
    )
    # Weird construct - required by SQLAlchemy
    product_list = services.ProductService.product_id_common_data(
        request, product.product_id, settings
    )
    all_related_products = services.ProductService.product_to_product_id_name_order_image_filter_product_list(
        request,
        [product.product_id],
        settings,
    )
    reverse_relation_products = services.ProductService.product_to_product_id_name_order_image_reverse_relation_filter_product_list(
        request, [product.product_id], settings, product_relation_type=1
    )
    # Get attributes used for displaying other options
    related_attribute_list = (
        services.AttributeService.attribute_group_attribute_filter_attribute_list(
            request,
            set(
                itertools.chain.from_iterable(
                    [
                        item.relation_settings["diff_attributes"]
                        for item in all_related_products
                        if item.product_relation_type == 3
                    ]
                )
            ),
            settings,
        )
    )
    data_options = operations.ProductOperations.single_product_data_options(
        request,
        product.product_type,
        product.availability_id,
        product.manufacturer_id,
        product.product_id,
        settings,
        related_attribute_list,
    )
    data_options_filtered = operations.ProductOperations.return_product_related_data(
        request,
        settings,
        [item.product_id for item in all_related_products]
        + [product.product_id]
        + [
            item.product_id
            for item in services.ProductService.special_products_filter_date_active_v2(
                request,
                settings,
                product.product_id,
            )
        ]
        + [item.product_id for item in reverse_relation_products],
        data_options.tax_dict,
    )
    data_options.promotion_list = {
        k: v
        for k, v in data_options.special_dict.items()
        if v["price_changing"] == True
    }
    product_in_cart = helpers.ProductHelpers.product_in_cart(
        request, product.product_id
    )
    product = helpers.ProductHelpers.single_product_format(
        data_options,
        data_options_filtered,
        filter_params,
        product,
        product_in_cart,
        settings,
    )
    data_options.attached_music_json = json.dumps(
        [
            (
                request.static_url("emporium:uploads/")
                + product.file_path
                + product.slug
                + "/"
                + music.file_path,
                music.file_name,
            )
            for music in data_options.attached_music
        ]
    )
    if (
        data_options_filtered.special_filtered.get(product.product_id)
        and product.transport_method == 0
    ):
        for special_item in data_options_filtered.special_filtered.get(
            product.product_id
        ):
            if special_item.storage_aware and special_item.quantity:
                product.not_promoted_storage_summary -= special_item.quantity
            elif special_item.storage_aware:
                product.not_promoted_storage_summary = 0
    product_json = helpers.ProductHelpers.product_json_format(product)
    selling_options = operations.ProductOperations.products_related_to_current_view(
        request,
        all_related_products,
        settings,
        product,
        data_options,
        data_options_filtered,
        4,
        reverse_relation_products,
    )
    cart_options = operations.CartOperations.product_requires_special_transport(
        product, data_options
    )
    return dict(
        cart_options=cart_options,
        data_options=data_options,
        data_options_filtered=data_options_filtered,
        filter_params=filter_params,
        meta_options=meta_options,
        misc_options=misc_options,
        product=product,
        product_json=product_json,
        selling_options=selling_options,
        settings=settings,
    )


@view_config(
    route_name="product_manipulation",
    renderer="product/product_edit/product_edit.html",
    permission="edit",
)
def product_manipulation(request):
    """
    Product add/edit/clone view
    """
    product_id = request.params.get("product_id", -1)
    options = helpers.SettingHelpers.default_options(request)
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.all(request)
    )
    data_options = views.CommonOptions.admin_data_options(request, settings)
    categories_list = helpers.CategoryHelpers.categories_filter_format(
        data_options.categories_dict
    )
    attribute_type_list = []
    data_options_filtered = operations.ProductOperations.return_product_related_data(
        request,
        settings,
        (product_id,),
        data_options.tax_dict,
    )
    cloned_product = None
    if options.action != "edit":
        new_product = operations.ProductOperations.product_create_draft(
            request, datetime.now()
        )
        if options.action == "clone":
            cloned_product = new_product
        elif options.action == "create":
            # product = new_product
            product_id = new_product.product_id
    product = services.ProductService.product_filter_product_id(request, product_id)
    product_template = [
        item
        for item in data_options.template_dict
        if item.setting_template_id == product.setting_template_id
    ][0]
    # Due to tax regulations prices can be stored with or without tax, code below allows
    # viewing prices according to the system settings.
    product = operations.ProductOperations.get_catalog_price_using_shop_settings(
        settings.get("price_input"),
        data_options.tax_dict[product.tax_id].value,
        product,
    )
    product_description_list = (
        services.ProductService.product_description_list_filter_product_id(
            request, product_id
        )
    )
    # Attribute
    product_attributes = (
        services.ProductToAttributeService.product_attribute_list_filter_product_id(
            request, product_id
        )
        or []
    )

    # Collection
    col = [
        {"id": _.collection_id, "name": _.name + " (" + _.manufacturer + ")"}
        for _ in data_options.collection_dict.values()
    ]
    col_ids = (
        services.CollectionService.collection_id_filter_product_id(request, product_id)
        or []
    )

    product_tags = services.TagService.tag_id_filter_product_id(
        request, product_id, settings
    )

    related_files = services.FileService.product_files_filter_product_id(
        request,
        product_id,
        settings,
    )
    product_files = [
        item for item in related_files if related_files and item.file_permission_id == 1
    ]
    product_files_extra = [
        item for item in related_files if related_files and item.file_permission_id == 2
    ]
    product_to_product_all = helpers.ProductHelpers.product_to_product_format(
        services.ProductService.product_to_product_id_name_order_image_filter_product_id(
            request, product_id
        ),
        data_options.manufacturer_dict,
        settings["image_settings"],
    )
    product_components = [
        item
        for item in product_to_product_all
        if item["product_relation_type"] == 1 or item["product_relation_type"] == 999
    ]
    product_options = [
        item for item in product_to_product_all if item["product_relation_type"] == 3
    ]
    product_relations = [
        item for item in product_to_product_all if item["product_relation_type"] == 2
    ]
    attributes_of_product_options = helpers.ProductHelpers.product_options_attributes(
        data_options.attribute_values_dict,
        services.ProductToAttributeService.product_attribute_list_filter_product_list(
            request, [item["product_id"] for item in product_options]
        ),
    )
    product_storages = services.ProductService.product_storages_filter_product_id(
        request, product_id
    )
    product_categories = (
        services.ProductToCategoryService.product_categories_filter_product_id(
            request, product_id
        )
        or []
    )
    product_images = (
        services.ProductToImageService.additional_images_filter_product_id(
            request,
            product_id,
            settings,
        )
        or []
    )
    product_specials = (
        services.ProductToSpecialService.product_special_list_filter_product_id(
            request, product_id
        )
    )
    product_promotion = (
        operations.ProductOperations.get_catalog_special_prices_using_shop_settings(
            settings.get("price_input"),
            data_options.tax_dict[product.tax_id].value,
            [
                item
                for item in product_specials
                if item.special_id in data_options.promotion_list
            ],
        )
    )
    product_featured = [
        item
        for item in product_specials
        if item.special_id not in data_options.promotion_list
    ]
    product_transport = services.ProductToTransportService.product_transport_slot_settings_filter_product_id(
        request,
        (product_id,),
        settings,
    )
    for item in data_options.template_dict:
        if item.setting_template_id == product.setting_template_id:
            attribute_type_list = item.template_json["attribute_type_list"]
    author_roles = services.AuthorService.author_role(request)
    url_next = request.matchdict.get("next", "product_list")
    # FORM
    form = forms.ProductUpdateForm(request.POST)
    if request.method == "POST":
        forms.CustomValidator.product_form_prevalidate(request, form)
        if form.validate():
            operations.ProductOperations.product_update_price_based_on_system_settings(
                settings, data_options, form
            )
            product = operations.ProductOperations.product_update(
                request,
                options.action,
                datetime.now(),
                form,
                cloned_product or product,
            )
            product.price = (
                form.price.data
                if product.manual_discount
                else operations.ProductOperations.calculate_current_price(
                    helpers.DiscountHelpers.create_discount_dict_key_id(
                        data_options.discounts
                    ),
                    product,
                )
            )
            # ATTRIBUTE
            attributes_passed = [
                int(x) for x in request.params.getall("attribute_list") if x
            ]  # All attributes are sent by the form, not all are used
            measurement_attributes = {}
            for v in data_options.attribute_dict.values():
                if v.attribute_name == "format_group":
                    measurement_attributes = v.attributes
            if (form.width.data and form.length.data) == 1:
                operations.ProductOperations.set_product_dimensions(
                    product, measurement_attributes, attributes_passed
                )
            # Attributes
            operations.ProductOperations.product_to_attribute_update(
                request,
                options.action,
                product.product_id,
                product_attributes,
                attributes_passed,
            )
            # AUTHOR
            operations.ProductOperations.product_to_author_update(
                request,
                options,
                product,
                services.ProductToAuthorService.product_author_list_filter_product_id(
                    request, product.product_id
                ),
            )
            # BAND
            operations.ProductOperations.product_to_band_update(
                request,
                options.action,
                product.product_id,
                services.ProductToBandService.product_band_list_filter_product_id(
                    request, product_id
                )
                if options.action == "edit"
                else models.ProductToBand(),
            )
            # CATEGORY
            operations.ProductOperations.product_to_category_update(
                request,
                options.action,
                product.product_id,
                product_categories,
                [
                    int(x) for x in request.params.getall("category_tree") if x
                ],  # All categories are sent by the form, not all are used
                data_options.categories_dict,  # For parent(s) category
            )
            # COLLECTION
            related_collections = []
            if request.params.get("collections"):
                related_collections = [
                    int(e) for e in request.params.get("collections").split(",")
                ]
            operations.ProductOperations.product_to_collection_create(
                request,
                options.action,
                col_ids,
                product.product_id,
                related_collections,
            )
            product_to_collection = services.ProductToCollectionService.product_to_collections_filter_collection_list_product_id(
                request, col_ids, product.product_id
            )
            operations.ProductOperations.product_to_collection_delete(
                request, col_ids, product_to_collection, related_collections
            )
            # COMPONENTS
            operations.ProductOperations.product_to_component_update(
                request,
                product.product_id,
                services.ProductToProductService.product_component_list_filter_product_id_relation_type(
                    request, product.product_id, 1
                ),
                services.ProductToProductService.product_to_component_list_filter_product_component_list_relation_type(
                    request,
                    [int(x) for x in request.params.getall("product_component_id")],
                    (1, 999),
                ),
                1,  # component
            )
            # DESCRIPTION
            operations.ProductOperations.product_description_update_all_languages(
                request,
                options.action,
                form,
                product.product_id,
                product_description_list,
                settings.get("localization_settings"),
            )
            # FILE
            if request.params.getall("product_file_id"):
                operations.ProductOperations.product_files_dispatch(
                    request,
                    [item.product_file_id for item in related_files],
                    services.ProductToFileService.product_to_file_list_filter_file_list(
                        request,
                        [int(x) for x in request.params.getall("product_file_id")],
                    ),
                )

            # OPTIONS
            operations.ProductOperations.product_to_product_option_update(
                request,
                product.product_id,
                services.ProductToProductService.product_component_list_filter_product_id_relation_type(
                    request, product.product_id, 3
                ),
                services.ProductToProductService.product_to_component_list_filter_product_component_list_relation_type(
                    request,
                    [int(x) for x in request.params.getall("product_option_id")],
                    (3,),
                ),
                3,  # options
            )
            # RELATED
            operations.ProductOperations.product_to_product_option_update(
                request,
                product.product_id,
                services.ProductToProductService.product_component_list_filter_product_id_relation_type(
                    request, product.product_id, 2
                ),
                services.ProductToProductService.product_to_component_list_filter_product_component_list_relation_type(
                    request,
                    [int(x) for x in request.params.getall("product_relation_id")],
                    (2,),
                ),
                2,  # relation
            )
            # SPECIAL& FEATURED
            current_special_products = []
            if [x for x in request.params.getall("product_special_id") if x]:
                current_special_products = services.ProductToSpecialService.product_to_special_list_filter_product_special_list(
                    request,
                    [x for x in request.params.getall("product_special_id") if x],
                )
            current_featured_products = []
            if [x for x in request.params.getall("product_featured_id") if x]:
                current_featured_products = services.ProductToSpecialService.product_to_special_list_filter_product_special_list(
                    request,
                    [x for x in request.params.getall("product_featured_id") if x],
                )
            if (
                product_specials
                or current_special_products
                or current_featured_products
            ):
                operations.ProductOperations.product_to_special_update(
                    request,
                    product.product_id,
                    [item for item in product_specials if item.special_id in (2, 4)],
                    current_special_products,
                    settings.get("price_input"),
                    data_options.tax_dict[form.tax_id.data].value,
                    storage_summary,
                )
                operations.ProductOperations.product_to_featured_update(
                    request,
                    product.product_id,
                    [item for item in product_specials if item.special_id in (1, 3)],
                    current_featured_products,
                )
            # STORAGE
            storage_summary = 0
            if request.params.getall("storage_id"):
                product_storage_list = [
                    request.params.get(f"product_storage_{item}")
                    for item in request.params.getall("storage_id")
                ]
                storage_summary = operations.ProductOperations.product_storage_dispatch(
                    request,
                    product.product_id,
                    services.ProductService.product_storages_filter_storage_list(
                        request, product_storage_list
                    ),
                    settings,
                )
            # TAG
            if request.params.get("tags"):
                # BUG - tags with slash cannot be added
                product.tags = [int(e) for e in request.params.get("tags").split(",")]
            # TRANSPORT
            operations.ProductOperations.product_to_transport_update(
                request, product.product_id, product_transport
            )

            # IMAGE
            if request.params.getall("image_id") or product_images:
                product_image_current_elements = services.ProductToImageService.product_to_image_list_filter_image_list(
                    request, request.params.getall("image_id")
                )
                operations.ProductOperations.product_images_dispatch(
                    request,
                    product.product_id,
                    product_images,
                    product_image_current_elements,
                )

            helpers.CacheHelpers.flush_cache()
            for _ in ["manufactured", "product_type", "transport_method"]:
                request.response.delete_cookie(_)
            return HTTPFound(location=url_next)
    return dict(
        attributes_of_product_options=attributes_of_product_options,
        attribute_type_list=attribute_type_list,
        author_roles=author_roles,
        # catalog_price=catalog_price,
        col=col,
        col_ids=col_ids,
        data_options=data_options,
        data_options_filtered=data_options_filtered,
        form=form,
        option_group=services.OptionService.group_active(request),
        option_list=services.OptionService.all_active(request),
        options=options,
        # products_related_id_list=products_related_id_list,
        product=False if options.action == "create" else product,
        product_attributes=[x.attribute_id for x in product_attributes],
        product_categories=[x.category_id for x in product_categories],
        product_components=product_components,
        product_options=product_options,
        # product_components_product_id_list=product_components_product_id_list,
        product_description_list=product_description_list,
        # product_band=product_band,
        product_featured=product_featured,
        product_files=product_files,
        product_files_extra=product_files_extra,
        product_images=product_images,
        product_promotion=product_promotion,
        product_relations=product_relations,
        product_storages=product_storages,
        product_tags=product_tags,
        product_template=product_template,
        product_transport=product_transport,
        product_id=cloned_product.product_id if cloned_product else product.product_id,
        settings=settings,
        url_next=url_next,
    )


@view_config(
    route_name="product_list", renderer="product/product_list.html", permission="edit"
)
@view_config(
    route_name="product_bulk", renderer="product/product_bulk.html", permission="edit"
)
@view_config(
    route_name="product_storage",
    renderer="product/product_storage.html",
    permission="edit",
)
def product_bulk(request):
    """
    Product - mass price update view
    """
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.all(request)
    )
    # settings["image_file_extensions"] = {"WEBP": ["webp", True]}
    query_items = [htem for htem in request.GET.items() if htem[1] != ("None" or "")]
    availability_attributes = services.AttributeService.availability_attributes(
        request, settings
    )
    filter_params = helpers.CommonHelpers.initialize_filter_params(
        request, availability_attributes
    )
    filter_params = helpers.CommonHelpers.term_search(filter_params)
    filter_params = helpers.CommonHelpers.term_filter_format(filter_params, query_items)
    filter_params.active_only = False
    filter_params = helpers.CommonHelpers.term_filter_format(filter_params, query_items)
    filter_params = helpers.ProductHelpers.filter_params_dimensions(
        filter_params, query_items
    )
    data_options = views.CommonOptions.data_options(
        request, filter_params, settings, fake_admin=True
    )
    missing_flag = None
    description_search = None
    session = request.session
    if data_options.number_products:
        session["number_products"] = data_options.number_products
    elif request.cookies.get("session") and request.session.get("number_products"):
        data_options.number_products = session["number_products"]

    searched_products = operations.ProductOperations.product_list_filtered(
        request, settings, filter_params, data_options.tax_dict
    )
    product_id_list = tuple(item.product_id for item in searched_products.items)
    (
        special_filtered,
        special_filtered_dict,
    ) = helpers.ProductHelpers.featured_products_format_dict(
        services.ProductToSpecialService.special_rows_filter_product_list_date(
            request, product_id_list
        ),
        data_options.tax_dict,
    )
    data_options_filtered = operations.ProductOperations.return_product_related_data(
        request,
        settings,
        product_id_list,
        data_options.tax_dict,
    )
    discounts = services.DiscountService.discount_id_value_manufacturer_filter_active(
        request
    )
    discount_list = helpers.DiscountHelpers.discounts_format(discounts)
    product_listing = helpers.ProductHelpers.product_category_format(
        data_options,
        data_options_filtered,
        searched_products.items,
        settings,
    )
    if request.params.get("price_update") and request.method == "POST":
        product_list = request.params.getall("product_id")
        for item in services.ProductService.products_by_list(request, product_list):
            new_price = request.params.get(f"price_{str(item.product_id)}")
            if new_price:
                operations.ProductOperations.update_catalog_price(
                    request.params.get(f"price_{str(item.product_id)}"), item
                )
            new_discount = request.params.get(f"discount_{str(item.product_id)}")
            if new_discount:
                item.discount_id = operations.ProductOperations.update_discount_value(
                    request.params.get(f"discount_{str(item.product_id)}"), item
                )
            if new_discount or new_price:
                item.catalog_price = operations.ProductOperations.product_bulk_update_price_based_on_system_settings(
                    settings, data_options, item, new_price
                )
                item.price = operations.ProductOperations.calculate_current_price(
                    helpers.DiscountHelpers.create_discount_dict_key_id(discounts), item
                )
            if new_ean := request.params.get(f"ean_{str(item.product_id)}"):
                item.ean = operations.ProductOperations.strip_value(new_ean)
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location=request.route_url("product_bulk"))
    if request.params.get("storage_update") and request.method == "POST":
        storage_summarized = operations.ProductOperations.mass_product_storage_dispatch(
            request,
            services.ProductService.product_storages_filter_storage_list(
                request, request.params.getall("product_storage_id")
            ),
        )
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location=request.route_url("product_storage"))
    return dict(
        data_options=data_options,
        data_options_filtered=data_options_filtered,
        description_search=description_search,
        discount_list=discount_list,
        filter_params=filter_params,
        missing_flag=missing_flag,
        pager=searched_products.link_map(),
        product_listing=product_listing,
        settings=settings,
    )


@view_config(
    route_name="product_special",
    renderer="product/product_special.html",
    permission="edit",
)
def product_special(request):
    """
    Special product (promotion/sale) list view
    """
    special_list = services.SpecialService.specials_values_id_name(request)
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    product_special_list = services.ProductService.products_specials_products(request)
    return dict(
        product_special_list=product_special_list,
        special_list=special_list,
        settings=settings,
    )


@view_config(
    route_name="product_special_action",
    match_param="action=create",
    renderer="product/product_special_edit.html",
    permission="edit",
)
def product_special_create(request):
    """
    Product special (promotion/sale) add view
    """
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    special_list = helpers.SpecialHelpers.specials_format(
        services.SpecialService.specials_values_id_name_filter_active(request)
    )
    featured_list = {
        k: v for k, v in special_list.items() if v["price_changing"] == False
    }
    form = forms.ProductSpecialCreateForm(request.POST)
    if request.method == "POST" and form.validate():
        if form.product_list.data:
            for product_id in form.product_list.data.split(","):
                product_to_special = models.ProductToSpecial()
                product_to_special.special_id = request.params.get("special_id")
                product_to_special.product_id = product_id
                date_start = str(
                    request.params.get("date_start")
                    or datetime.now().date() - relativedelta(days=1)
                ).split("-")
                product_to_special.date_start = date(*map(int, date_start))
                date_end = request.params.get("date_end", None)
                product_to_special.date_end = (
                    date(*map(int, date_end.split("-"))) if date_end else None
                )
                request.dbsession.add(product_to_special)
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location=request.route_url("product_special"))
    return dict(
        action=request.matchdict.get("action"),
        form=form,
        pro=[],
        pro_ids=[],
        featured_list=featured_list,
        settings=settings,
    )


@view_config(
    route_name="product_special_action",
    match_param="action=edit",
    renderer="product/product_special_edit.html",
    permission="edit",
)
def product_special_edit(request):
    """
    Product special (promotion/sale) edit view
    """
    product_special_list = services.ProductService.products_specials_products(request)
    special_list = services.SpecialService.specials_values_id_name(request)
    form = forms.ProductSpecialCreateForm(request.POST)
    pro = []
    pro_ids = []
    if product_special_list:
        for item in product_special_list:
            element = {"id": item[1].product_id, "name": item[1].name}
            pro.append(element)
            pro_ids.append(item[0].product_id)
    if request.method == "POST" and form.validate():
        if form.product_list.data:
            for product_id in form.product_list.data.split(","):
                request.dbsession.add(
                    models.ProductToSpecial(
                        special_id=request.params.get("special_id"),
                        date_start=request.params.get("date_start"),
                        date_end=request.params.get("date_end"),
                        product_id=product_id,
                    )
                )
        helpers.CacheHelpers.flush_cache()
    return dict(
        action=request.matchdict.get("action"),
        product_special_list=product_special_list,
        form=form,
        pro=pro,
        pro_ids=pro_ids,
        special_list=special_list,
    )


@view_config(
    route_name="product_xml",
    renderer="product/product_xml.html",
    permission="edit",
)
def product_xml(request):
    """
    Special product (promotion/sale) list view
    """
    xml_list = (
        {"name": "Google Base", "xml_id": "1"},
        {"name": "Ceneo", "xml_id": "2"},
    )
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    product_xml_list = services.ProductToXMLService.products_excluded_from_xml(request)
    return dict(
        product_xml_list=product_xml_list,
        xml_list=xml_list,
        settings=settings,
    )


@view_config(
    route_name="product_xml_action",
    match_param="action=create",
    renderer="product/product_xml_edit.html",
    permission="edit",
)
def product_xml_create(request):
    """
    Product xml (promotion/sale) add view
    """
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    xml_formatted_list = helpers.XMLHelpers.xmls_format(
        services.ProductToXMLService.products_excluded_from_xml(request)
    )
    xml_list = dict(xml_formatted_list.items())
    form = forms.ProductSpecialCreateForm(request.POST)
    if request.method == "POST" and form.validate():
        if form.product_list.data:
            for product_id in form.product_list.data.split(","):
                request.dbsession.add(
                    models.ProductToXML(
                        xml_id=request.params.get("xml_id"),
                        product_id=product_id,
                        excluded=True,
                    )
                )
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location=request.route_url("product_xml"))
    return dict(
        action=request.matchdict.get("action"),
        form=form,
        pro=[],
        pro_ids=[],
        xml_formatted_list=xml_formatted_list,
        settings=settings,
    )


@view_config(
    route_name="product_delete",
    renderer="json",
    permission="edit",
    xhr="True",
)
def product_delete(request):
    """
    Product delete view (image removal is separated due to file presence)
    """
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    product_id = request.POST.get("product_id")
    product = services.ProductService.product_filter_product_id(request, product_id)
    if request.method != "POST" or not product:
        return helpers.CommonHelpers.delete_failure(request, product)
    images_list = [product.image] if product.image else []
    images_list.extend(
        item.image
        for item in services.ProductToImageService.additional_images_filter_product_id(
            request, product_id, settings
        )
    )
    if images_list:
        helpers.CommonHelpers.delete_image(
            images_list, ("images", "thumbnails", "base")
        )  # List of default image and all related images
    request.dbsession.delete(product)
    helpers.CacheHelpers.flush_cache()


@view_config(
    route_name="product_to_product_action",
    match_param="action=delete",
    renderer="json",
    permission="edit",
    xhr="True",
)
def product_to_product_delete(request):
    """
    Product delete view (image removal is separated due to file presence)
    """
    product_component_id = request.POST.get("product_component_id")
    product_to_product = (
        services.ProductService.product_to_product_filter_product_to_product_id(
            request, product_component_id
        )
    )
    if request.method != "POST" or not product_to_product:
        return helpers.CommonHelpers.delete_failure(request, product_to_product)
    request.dbsession.delete(product_to_product)
    helpers.CacheHelpers.flush_cache()


@view_config(
    route_name="product_author_add", renderer="json", permission="edit", xhr="True"
)
def product_author_add(request):
    author_id = request.params.get("author_id")
    upload_status = 400
    author = models.ProductToAuthor(author_id=author_id)
    request.dbsession.add(author)
    request.dbsession.flush()
    return author.product_author_id


@view_config(
    route_name="product_component_add", renderer="json", permission="edit", xhr="True"
)
def product_component_add(request):
    product1_id = request.params.get("product1_id")
    product2_id = request.params.get("product2_id")
    product_relation_type = request.params.get("product_relation_type", 999)
    if product_relation_type == "3":
        product_component_2 = models.ProductToProduct(
            product1_id=product2_id,
            product2_id=product1_id,
            product_relation_type=product_relation_type,
            relation_settings={"diff_atributes": []},
        )
        product_component_1 = models.ProductToProduct(
            product1_id=product1_id,
            product2_id=product2_id,
            product_relation_type=product_relation_type,
            relation_settings={"diff_atributes": []},
        )
        request.dbsession.add_all([product_component_1, product_component_2])
        request.dbsession.flush()
        return product_component_1.product_component_id
    else:
        product_component = models.ProductToProduct(
            product1_id=product1_id,
            product2_id=product2_id,
            product_relation_type=product_relation_type,
            relation_settings={},
        )
        request.dbsession.add(product_component)
        request.dbsession.flush()
        return product_component.product_component_id


@view_config(
    route_name="product_special_add", renderer="json", permission="edit", xhr="True"
)
def product_special_add(request):
    special_id = request.params.get("special_id")
    special = models.ProductToSpecial(special_id=special_id)
    request.dbsession.add(special)
    request.dbsession.flush()
    return special.product_special_id


@view_config(
    route_name="product_tag_add", renderer="json", permission="edit", xhr="True"
)
def product_tag_add(request):
    tag_id = request.params.get("tag_id")
    product_id = request.params.get("product_id")
    upload_status = 400
    tag_relation = models.ProductToTag(tag_id=tag_id, product_id=product_id)
    request.dbsession.add(tag_relation)
    request.dbsession.flush()
    return tag_relation.product_tag_id


@view_config(
    route_name="new_tag_product_tag_add", renderer="json", permission="edit", xhr="True"
)
def new_tag_product_tag_add(request):
    # tag_id = request.params.get("tag_id")
    product_id = request.params.get("product_id")
    tag_value = request.params.get("tag_value")
    tag = models.Tag(tag_value=tag_value, tag_slug=slugify(tag_value))
    request.dbsession.add(tag)
    request.dbsession.flush()
    product_tag = models.ProductToTag(tag_id=tag.tag_id, product_id=product_id)
    request.dbsession.add(product_tag)
    request.dbsession.flush()
    return product_tag.product_tag_id


@view_config(
    route_name="product_tag_delete", renderer="json", permission="edit", xhr="True"
)
def product_tag_delete(request):
    product_tag_id = request.params.get("product_tag_id")
    removed_tag = services.ProductToTagService.product_to_tag_filter_product_to_tag(
        request, product_tag_id
    )
    request.dbsession.delete(removed_tag)
    request.dbsession.flush()
    return 202
