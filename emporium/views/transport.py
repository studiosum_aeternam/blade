from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from emporium import forms, helpers, models, operations, services


@view_config(
    route_name="transport_edit",
    renderer="transport/transport_edit.html",
    permission="edit",
)
def transport_edit(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    options = helpers.SettingHelpers.default_options(request)
    transport = (
        services.TransportService.transport_description_excerpt_name_status_filter_id(
            request, request.matchdict.get("module_id"), settings
        )
    )
    other_transports = (
        services.TransportService.transport_id_name_exclude_current_transport(
            request, request.matchdict.get("module_name"), settings
        )
    )
    transport_size_slots = (
        services.TransportService.transport_slots_full_filter_transport_id(
            request, settings, transport.transport_id
        )
    )
    form = forms.TransportUpdateForm(request.POST, transport)
    if request.method == "POST" and form.validate():
        # transport.special_method = form.special_method.data
        transport.description = form.description.data
        transport.excerpt = form.excerpt.data
        transport.transport_method = form.transport_method.data
        transport.name = form.name.data
        transport.status = form.status.data
        transport.module_settings = (
            operations.TransportOperations.update_transport_settings(request)
        )
        helpers.CacheHelpers.flush_cache()
        operations.TransportOperations.update_transport_size_slots(
            request, settings, transport.transport_id, transport_size_slots
        )
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="transport_list")
    return dict(
        form=form,
        transport=transport,
        settings=settings,
        options=options,
        other_transports=other_transports,
        transport_size_slots=transport_size_slots,
    )


@view_config(
    route_name="transport_list",
    renderer="transport/transport_list.html",
    permission="edit",
)
def transport_list(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    transports = services.TransportService.transport_all_objects(request, settings)
    return dict(transports=transports, settings=settings)


@view_config(
    route_name="transport_delete",
    renderer="json",
    permission="edit",
    xhr="True",
)
def transport_delete(request):
    transport_id = request.POST.get("transport_id")
    if transport := services.TransportService.transport_dbobject_filter_id(
        request, transport_id
    ):
        request.dbsession.delete(transport)
        helpers.CacheHelpers.flush_cache()


@view_config(
    route_name="transport_size_slot_add", renderer="json", permission="edit", xhr="True"
)
def transport_size_slot_add(request):
    transport_id = request.POST.get("transport_id")
    upload_status = 400
    transport_size_slot = models.TransportSlot(transport_id=transport_id)
    request.dbsession.add(transport_size_slot)
    request.dbsession.flush()
    return transport_size_slot.transport_slot_id
