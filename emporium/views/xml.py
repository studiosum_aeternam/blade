import re
from datetime import datetime
import lxml.builder as lb
from lxml import etree
from pyramid.view import view_config
from time import perf_counter

from emporium import data_containers, helpers, services, views


@view_config(route_name="s_base", renderer="xml/s_base.html")
def base(request):
    today = datetime.now().strftime("%Y-%m-%d")
    request.response.content_type = "text/plain"
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    disabled_categories = services.CategoryService.category_id_filter_status(
        request, 0, settings
    )

    disabled_collections = (
        services.CollectionService.disabled_collections(request) or []
    )
    disabled_products = services.ProductService.unavailable_products(
        request,
        [
            item.manufacturer_id
            for item in data_options.manufacturer_dict.values()
            if item.status == 0
        ],
    )
    products_active = services.ProductService.products_active(
        request, disabled_products
    )
    products_with_grouped_categories = (
        services.ProductService.products_with_grouped_categories(
            request, disabled_categories, disabled_products
        )
    )
    products_with_grouped_collections = (
        services.ProductService.products_with_grouped_collections(
            request, disabled_collections, disabled_products
        )
    )
    product_attributes = {
        item.product_id: item.attributes
        for item in services.AttributeService.products_with_grouped_attributes(
            request, [], []
        )
    }
    products_with_grouped_additional_images = (
        services.ProductService.products_with_grouped_additional_images(
            request, disabled_products
        )
    )
    special_active_prices = helpers.ProductHelpers.create_special_price_dict_key_id(
        services.ProductToSpecialService.active_special_prices(request)
    )
    base_category_name = "Sprzęt > Materiały budowlane > Płytki"
    category_mappings = {
        "Armatura": "Sprzęt > Hydraulika > Armatury > Baterie",
        "Odpływy liniowe ": "Hydraulika  > Armatura wodociągowa i części > Odpływy",
    }
    product_listing = helpers.XMLHelpers.generate_xml_custom_rules(
        request,
        data_options,
        category_mappings,
        base_category_name,
        special_active_prices,
        zip(
            products_active,
            products_with_grouped_categories,
            products_with_grouped_collections,
            products_with_grouped_additional_images,
            product_attributes.values(),
        ),
    )
    return dict(product_listing=product_listing)


@view_config(route_name="s_ceneo", renderer="xml/s_ceneo.html")
def ceneo(request):
    today = datetime.now().strftime("%Y-%m-%d")
    request.response.content_type = "text/plain"
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    disabled_products = services.ProductService.unavailable_products(
        request,
        [
            item.manufacturer_id
            for item in data_options.manufacturer_dict.values()
            if item.status == 0
        ],
    )
    products_active = services.ProductService.products_active(
        request,
        disabled_products,
    )
    disabled_attributes = [
        x.attribute_id for x in services.AttributeService.disabled_attributes(request)
    ]
    products_with_grouped_attributes = (
        services.AttributeService.products_with_grouped_attributes(
            request, disabled_attributes, disabled_products
        )
    )
    special_active_prices = helpers.ProductHelpers.create_special_price_dict_key_id(
        services.ProductToSpecialService.active_special_prices(request)
    )
    product_listing = []
    availability_mappings = {"1": (46, 111), "3": (47, 52), "7": (48,), "14": (49, 50)}
    base_category_name = "Budowa i remont/Materiały wykończeniowe/Płytki"
    for item in zip(products_active, products_with_grouped_attributes):
        availability = 99
        for k, v in availability_mappings.items():
            if item[0].availability_id in v:
                availability = k
        product_id = item[0].product_id
        product_listing.append(
            data_containers.CeneoContainer(
                availability=availability,
                base_category_name=base_category_name,
                description=helpers.CommonHelpers.tag_cleaner(
                    re.sub("<", " <", str(item[0].description))
                    if item[0].description
                    else ""
                ),
                ean=item[0][1],
                main_image=request.static_url("emporium:images/")
                + "thumbnails/"
                + data_options.manufacturer_dict[item[0].manufacturer_id].seo_slug
                + "/"
                + item[0].image
                + ".jpg"
                if item[0].image
                else "no_image.jpg",
                name=re.sub("&", "&#38;", item[0].name.capitalize()),
                price=helpers.CommonHelpers.price_gross_number(
                    (special_active_prices.get(product_id) or item[0].price),
                    data_options.tax_dict[item[0].tax_id],
                ),
                product_id=product_id,
                url=request.route_url("s_home")
                + item[0].seo_slug
                + "-"
                + str(product_id),
                brand=data_options.manufacturer_dict[item[0].manufacturer_id].name,
            )
        )
    return dict(product_listing=product_listing)


@view_config(route_name="s_sitemap", renderer="xml/s_sitemap.html")
def sitemap(request):
    request.response.content_type = "text/plain"
    all_current_products = services.ProductService.sitemap(request)
    home = request.route_url("s_home")  # Lol
    filter_params = data_containers.FilterOptions()
    url_listing = helpers.ProductHelpers.add_products_to_urls_container(
        all_current_products, home
    )
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    helpers.CollectionHelpers.add_collection_list_to_urls_container(
        home,
        url_listing,
        services.CategoryService.category_id_name_parent_description_meta_seo_h1_title_filter_id(
            request, 60, settings
        ),
        services.CollectionService.collections_id_name_filter_dynamic(
            request,
            filter_params,
            settings,
        ),
    )
    helpers.PageHelpers.add_pages_to_urls_container(
        home, url_listing, services.PageService.active(request)
    )
    helpers.PostHelpers.add_posts_to_urls_container(
        home, url_listing, services.PostService.active(request)
    )
    return dict(url_listing=url_listing)


@view_config(
    route_name="product_xml_delete",
    renderer="json",
    permission="edit",
    xhr="True",
)
def product_xml_delete(request):
    product_xml_id = request.POST.get("product_xml_id")
    if xml := services.ProductToXMLService.product_xml_filter_xml(
        request, product_xml_id
    ):
        request.dbsession.delete(xml)
        helpers.CacheHelpers.flush_cache()
