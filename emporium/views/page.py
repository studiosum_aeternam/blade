"""
Page views - pages are elements of the content which present static\
 information about the shop. For cms/blog capabilities use posts.
"""
from datetime import datetime
from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from pyramid.view import view_config


from emporium import (
    data_containers,
    forms,
    helpers,
    models,
    operations,
    services,
    views,
)


@view_config(
    route_name="page_action",
    match_param="action=create",
    renderer="page/page_edit.html",
    permission="edit",
)
@view_config(
    route_name="page_action",
    match_param="action=edit",
    renderer="page/page_edit.html",
    permission="edit",
)
def page_edit(request):
    """
    Page add/edit view
    """
    options = helpers.SettingHelpers.default_options(request)
    page = False
    if options.action == "edit":
        page = services.PageService.by_id(request, request.params.get("page_id"))
    form = forms.PageForm(request.POST)
    if request.method == "POST" and form.validate():
        if options.action == "create":
            page = models.Page(creator_id=request.authenticated_userid)
        page.content = form.content.data
        page.content_extended = form.content_extended.data
        page.title = operations.ProductOperations.strip_value(form.title.data)
        page.status = form.status.data
        if options.action == "create":
            request.dbsession.add(page)
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="page_list")
    return dict(
        form=form,
        action=options.action,
        options=options,
        page=page,
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
    )


@view_config(
    route_name="page_delete",
    renderer="json",
    request_method="POST",
    permission="edit",
    xhr="True",
)
def page_delete(request):
    """
    Page delete - unused method left for backup
    """
    page = services.PageService.by_id(request, request.POST.get("page_id"))
    if request.method != "POST" or not page:
        return helpers.CommonHelpers.delete_failure(request, page)
    request.dbsession.delete(page)
    helpers.CacheHelpers.flush_cache()


@view_config(
    route_name="page_list",
    renderer="page/page_list.html",
    permission="edit",
)
def page_list(request):
    """
    Page list view for admin
    """
    pages = services.PageService.all(request)
    return dict(
        pages=pages,
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
    )


@view_config(
    route_name="s_page",
    renderer="page/s_page.html",
    # http_cache=0,
)
def view_page(request):
    """
    Page view - it has hardcoded transport recalculation loop duplicating cart \
    methods (WET) and send form logic from the contact page (WET).
    """
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    meta_options = data_containers.MetaOptions(rel_link=request.route_url("s_search"))
    misc_options = data_containers.MiscOptions(search_url="s_search")
    page = services.PageService.page_with_content_filter_pagename(
        request, request.matchdict["pagename"], settings
    )
    if not page:
        raise HTTPNotFound("No such page")
    blocks = services.BlockService.blocks_values_content_title_size_filter_page_date(
        request, page.page_id, settings
    )
    transport_and_payment_methods_summarized = []
    contact_form = None
    msg = request.params.get("msg", None)
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    # Contact form
    if request.matchdict["pagename"] == "contact":
        contact_form = forms.ContactForm(request.POST)
        if request.method == "POST" and contact_form.validate():
            sender = settings["mail_username"]
            recipients = settings["mail_username"]
            html = (
                "<b>Imię:</b><br>"
                + str(contact_form.name.data)
                + "<br><b>Telefon:</b><br>"
                + str(contact_form.telephone.data)
                + "<br><b>Mail:</b><br>"
                + str(contact_form.mail.data)
                + "<br><b>Treść:</b><br>"
                + str(contact_form.message.data)
            )
            subject = "Formularz kontaktowy ze sklepu"
            kwargs = {"cookies": request.cookies, "host": request.host}
            link = request.route_url(
                "s_mail",
                _query=(
                    ("sender", sender),
                    ("recipients", recipients),
                    ("html", html),
                    ("subject", subject),
                ),
            )
            subrequest = request.blank(link, **kwargs)
            # Verify if it can be safely removed
            response = request.invoke_subrequest(subrequest, use_tweens=True)
            return HTTPFound(
                location=request.route_url(
                    "s_page",
                    page_id=page.page_id,
                    pagename=page.page_name,
                    _query=(("msg", True),),
                )
            )
    return dict(
        blocks=blocks,
        contact_form=contact_form,
        data_options=data_options,
        filter_params=filter_params,
        transport_and_payment_methods_summarized=transport_and_payment_methods_summarized,
        misc_options=misc_options,
        msg=msg,
        page=page,
        settings=settings,
        meta_options=meta_options,
    )
