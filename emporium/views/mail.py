import email

from pyramid_mailer.message import Message, Attachment
from pyramid.view import view_config

from ..helpers import CommonHelpers, SettingHelpers
from ..services import SettingsService


@view_config(route_name="system_mail", permission="edit")
def system_mail(request):
    """
    View for administration (system) emails
    """
    settings = SettingHelpers.shop_settings(SettingsService.all(request))
    message = Message(
        subject=request.params.get("subject"),
        sender=settings["mail_username"],
        recipients=[request.params.get("recipients")],
        html=request.params.get("mail_text"),
        bcc=[request.params.get("bcc") or ""],
        extra_headers={"date", email.utils.formatdate(localtime=True)},
    )
    if "mail_attachment" in request.params:
        file = request.params.get("mail_file")
        file_type = request.params.get("mail_file_type")
        attachment = Attachment(
            CommonHelpers.asset_path("tmp") + file,
            file_type,
            open(CommonHelpers.asset_path("tmp") + file, "rb"),
        )
        message.attach(attachment)
    mailer = request.registry["mailer"]
    mailer.send_immediately(message)
    return request.response


@view_config(route_name="s_mail")
def s_mail(request):
    """
    Route for customer mail
    """
    recipients = request.params.get("recipients")
    html = request.params.get("html")
    subject = request.params.get("subject")
    sender = request.params.get("sender") or ""
    bcc = request.params.get("bcc") or ""
    # if "mail_attachment" in request.params:
    #     file = request.params.get("mail_file")
    #     file_type = request.params.get("mail_file_type")
    #     attachment = Attachment(
    #         CommonHelpers.asset_path("tmp") + file,
    #         file_type,
    #         open(CommonHelpers.asset_path("tmp") + file, "rb"),
    #     )
    #     message.attach(attachment)
    message = Message(
        subject=subject,
        sender=sender,
        recipients=[recipients],
        html=html,
        bcc=[bcc],
        extra_headers={"date", email.utils.formatdate(localtime=True)},
    )
    mailer = request.registry["mailer"]
    mailer.send_immediately(message)
    return request.response

    # to = "youremail@gmail.com"
    # sender = "myemail@gmail.com"
    # subject = "subject test1"
    # message_text_html = r"Hi<br/>Html <b>hello</b>"
    # message_text_plain = "Hi\nPlain Email"
    # attached_file = r"C:\Users\Me\Desktop\audio.m4a"
    # create_message_and_send(
    #     sender, to, subject, message_text_plain, message_text_html, attached_file
    # )
