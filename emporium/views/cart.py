from datetime import datetime
from decimal import Decimal
import json
import math


from pyramid.httpexceptions import HTTPFound, HTTPMovedPermanently
from pyramid.view import view_config

from emporium import data_containers, helpers, models, operations, services, views


@view_config(route_name="s_cart", renderer="cart/s_cart.html")
def cart_view(request):
    """
    Basic cart view. If cart data is missing remove all session data related to cart/order.
    """
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    data_options = views.CommonOptions.data_options_cart(request, settings)
    misc_options = data_containers.MiscOptions(search_url="s_search")
    cart_parameters = False
    cart_contents = {}
    related_products_list = []
    data_options_filtered = operations.ProductOperations.return_product_related_data(
        request, settings, [], data_options.tax_dict
    )
    selling_options = operations.ProductOperations.formatted_featured_products_v2(
        request, settings, data_options, data_options_filtered, 4
    )
    cart = operations.CartOperations.cart_filter_cart_or_customer(request)
    if cart:
        products_json = {
            int(k): v
            for k, v in (
                helpers.CartHelpers.return_dict_from_cart_products(
                    cart.products_json or cart.products
                )
            ).items()
        }
        product_list = services.ProductService.products_base_price_filter_available(
            request,
            [x for x in products_json.keys()],
            False,
        )
        related_products = services.ProductService.product_to_product_id_name_order_image_filter_product_list(
            request,
            [item.product_id for item in product_list if item.product_type == 1],
            settings,
        )
        data_options_filtered = operations.ProductOperations.return_cart_related_data(
            request,
            settings,
            [item.product_id for item in related_products if related_products]
            + [x for x in products_json.keys()],
            data_options.tax_dict,
        )
        related_products_list = (
            helpers.ProductHelpers.product_to_product_combined_format(
                related_products,
                data_options,
                data_options_filtered,
                settings["image_settings"],
            )
        )
        cart_contents = helpers.CartHelpers.cart_contents_get_current_product_values(
            products_json,
            helpers.ProductHelpers.product_format_cart_dict(
                data_options,
                data_options_filtered,
                product_list,
                settings,
            ),
        )
        # Cart parameters are where all values are calculated and row values are returned.
        cart_parameters = (
            operations.CartOperations.calculate_cart_parameters(
                request,
                cart_contents,
                data_options,
                data_options_filtered,
            )
            if cart_contents
            else {}
        )
        data_options = (
            helpers.CartHelpers.return_possible_transport_and_payment_methods(
                request,
                cart_parameters,
                cart_contents,
                data_options,
                data_options_filtered,
            )
        )
        if (
            settings["cart_settings"].get("display_estimated_transport_cost")
            and settings["cart_settings"]["display_estimated_transport_cost"] == "1"
        ):
            cart_parameters = (
                operations.TransportOperations.calculate_cheapest_transport_method(
                    cart_parameters, data_options.transport_dict
                )
            )
    return dict(
        cart_contents=cart_contents,
        cart_parameters=cart_parameters,
        data_options=data_options,
        data_options_filtered=data_options_filtered,
        misc_options=misc_options,
        filter_params=data_containers.FilterOptions(),
        meta_options=data_containers.MetaOptions(
            rel_link=request.route_url("s_search")
        ),
        related_products_list=related_products_list,
        settings=settings,
        selling_options=selling_options,
    )


@view_config(route_name="s_cart_edit", renderer="string", request_method="POST")
def cart_edit(request):
    """
    Cart edit view (invoked by the js).
    Every action related to the cart contents (update quantity or delete items)
    is handled by this view. Update function merges exisitng cart
    with new data or creates a new cart. To update item its quanity must be over 0
    (and higher than item's minium).  Item quantity 0 is used for item deletion
    (0 value cannot be put inside input field). When the last item is removed from the cart,
    the session variables are removed. The record in the database is preserved
    (for statistical analysis) any other client related data (personal info) is stored in session variables
    (on to customer's browser session cookie).
    """
    cart = (
        operations.CartOperations.cart_filter_cart_or_customer(request) or models.Cart()
    )
    if "update_cart" in request.params:
        # Format new item data
        new_items = (
            operations.ProductOperations.return_decimal_dict_values_for_new_items(
                request
            )
        )
        # Format old cart data (JSON cannot store Decial).
        old_items = helpers.CartHelpers.return_decimal_dict_from_cart_products(
            cart.products_json
        )
        # Add new items or overwrite cart data
        products_in_cart = (
            old_items | new_items if old_items and new_items else new_items
        )
        # Find which items are left (drop items with quantity <0 those should be removed from the cart)
        products_in_cart = {
            str(k): v
            for k, v in products_in_cart.items()
            if Decimal(v["product_quantity"]) > 0
        }
        if not products_in_cart:
            # Remove empty cart from the session
            helpers.OrderHelpers.remove_order_cookies(request)
            helpers.OrderHelpers.remove_session_order_variables(request.session)
            helpers.CartHelpers.remove_cart_from_db_and_session(request, cart)
        else:
            current_product_units = (
                services.ProductService.product_id_unit_filter_product_list(
                    request, products_in_cart
                )
            )
            # Create or update cart
            cart.date_accessed = datetime.now()
            if not cart.cart_id:
                # cart can be a model or db object, and cart.cart_id is only for updated cart
                cart.date_creation = cart.date_accessed
                cart.customer_id = request.authenticated_userid or None
                request.dbsession.add(cart)
                request.dbsession.flush()
            # The session variable should be calculated on every cart update. This grants product
            # quantities for top menu (check if checking cart variables can be disabled for direct cart_view)
            new_product_units = {}
            for k, v in products_in_cart.items():
                new_product_units[k] = data_containers.CartUnitProducts(
                    product_quantity=v["product_quantity"],
                    product_packages=next(
                        iter(
                            v["product_quantity"] / item.unit
                            for item in current_product_units
                            if item.product_id == int(k)
                        )
                    ),
                )
            operations.CartOperations.push_current_cart_quantities_to_session(
                request, new_product_units
            )
            cart.products_json = (
                helpers.CartHelpers.return_json_compatible_dict_from_cart_products(
                    products_in_cart
                )
            )
            cart.products_json = cart.products_json
            # Why are there cid and cart_id called like this?
            if not request.authenticated_userid:
                request.session["cid"] = cart.cart_id
            request.session["cart_id"] = cart.cart_id
    return dict(session_cart=cart)
