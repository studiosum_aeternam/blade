from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config


from emporium import operations, services, models, forms, helpers


@view_config(
    route_name="attribute_list",
    renderer="attribute/attribute_list.html",
    permission="edit",
)
def attribute_list(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    attributes = services.AttributeService.attribute_group_id_name_status_sort_order_attributes_paginated(
        request
    )
    attribute_list = [
        {
            "attribute_group_id": item.attribute_group_id,
            "attributes": ", ".join(sorted(iter(item.attributes))),
            "name": item.name,
            "attribute_name": item.attribute_name,
            "sort_order": item.sort_order,
            "status": item.status,
        }
        for item in attributes
    ]
    return dict(attribute_list=attribute_list, settings=settings)


@view_config(
    route_name="attribute_action",
    match_param="action=create",
    renderer="attribute/attribute_edit.html",
    permission="edit",
)
def add_attribute(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    options = helpers.SettingHelpers.default_options(request)
    form = forms.AttributeGroupCreateForm(request.POST)
    attribute_display_type = (
        services.AttributeService.attribute_display_type_id_description_name_list(
            request
        )
    )
    if request.method == "POST" and form.validate():
        attribute_group = models.AttributeGroup()
        form.populate_obj(attribute_group)
        attribute_group.sort_order = 999
        attribute_group.status = 1
        request.dbsession.add(attribute_group)
        request.dbsession.flush()
        if form.attribute_values.data:
            for _ in form.attribute_values.data.split(";"):
                request.dbsession.add(
                    models.AttributeGroup(
                        attribute_group_id=attribute_group.attribute_group_id,
                        status=1,
                        sort_order=999,
                    )
                )
            helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="attribute_list")
    return dict(
        action=request.matchdict.get("action"),
        attribute_group=None,
        attribute_list=[],
        form=form,
        options=options,
        settings=settings,
        single_attribute_group=None,
        attribute_display_type=attribute_display_type,
    )


@view_config(
    route_name="attribute_action",
    match_param="action=edit",
    renderer="attribute/attribute_edit.html",
    permission="edit",
)
def edit_attribute(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    options = helpers.SettingHelpers.default_options(request)
    attribute_group_id = request.params.get("attribute_group_id", -1)
    single_attribute_group = services.AttributeService.attribute_group_filter_id(
        request, attribute_group_id
    )
    attribute_group = services.AttributeService.by_group_id(request, attribute_group_id)
    attribute_display_type = (
        services.AttributeService.attribute_display_type_id_description_name_list(
            request
        )
    )
    categories_list = services.CategoryService.categories_categories_descriptions(
        request, settings
    )
    if request.method == "POST":
        single_attribute_group.attribute_display_type_id = request.params.get(
            "attribute_display_type_id"
        )
        single_attribute_group.attribute_name = request.params.get("attribute_name")
        single_attribute_group.description = request.params.get("description")
        single_attribute_group.name = request.params.get("name")
        single_attribute_group.status = request.params.get("status")
        single_attribute_group.description_hidden = request.params.get(
            "description_hidden"
        )
        for group in attribute_group:
            attribute_id = str(group.attribute_id)
            group.value = request.params.get(f"attribute_value_{attribute_id}")
            group.sort_order = request.params.get(f"attribute_sort_{attribute_id}")
            group.attribute_settings = {
                "mapped_category": request.params.get(
                    f"attribute_settings_mapped_category_{attribute_id}"
                )
            }
        if request.params.get("attribute_values"):
            for value in request.params.get("attribute_values").split(";"):
                request.dbsession.add(
                    models.Attribute(
                        attribute_group_id=int(attribute_group_id),
                        status=1,
                        sort_order=999,
                        value=operations.ProductOperations.strip_value(value),
                    )
                )
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="attribute_list")
    return dict(
        action=request.matchdict.get("action"),
        attribute_group=attribute_group,
        attribute_list=attribute_list,
        attribute_display_type=attribute_display_type,
        categories_list=categories_list,
        options=options,
        settings=settings,
        single_attribute_group=single_attribute_group,
    )


@view_config(
    route_name="attribute_delete", renderer="json", permission="edit", xhr="True"
)
def attribute_delete(request):
    attribute_id = request.POST.get("attribute_id")
    attribute = services.AttributeService.by_id(request, attribute_id)
    if products_by_attribute := services.ProductToAttributeService.products_by_attribute(
        request, attribute_id
    ):
        for item in products_by_attribute:
            request.dbsession.delete(item)
    request.dbsession.delete(attribute)
    helpers.CacheHelpers.flush_cache()


@view_config(
    route_name="attribute_group_delete", renderer="json", permission="edit", xhr="True"
)
def attribute_group_delete(request):
    attribute_group_id = request.POST.get("attribute_group_id")
    attribute_group = services.AttributeService.by_group_id(request, attribute_group_id)
    if products_by_attribute := services.ProductToAttributeService.products_by_attributes_list(
        request, [x.attribute_id for x in attribute_group]
    ):
        for item in products_by_attribute:
            request.dbsession.delete(item)
    attributes_list = services.AttributeService.by_attribute_list(
        request, [x.attribute_id for x in attribute_group]
    )
    for item in attributes_list:
        request.dbsession.delete(item)
    attribute = services.AttributeService.bare_by_id(request, attribute_group_id)
    for item in attribute:
        request.dbsession.delete(item)
    helpers.CacheHelpers.flush_cache()
