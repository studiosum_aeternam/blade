"""
Post views - they give cms/blog capabilities to the engine.
"""
from datetime import datetime
import contextlib
import os
from bs4 import BeautifulSoup

from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from pyramid.view import view_config
from slugify import slugify

from emporium import (
    data_containers,
    forms,
    helpers,
    models,
    operations,
    services,
    views,
)


@view_config(route_name="s_post_list", renderer="post/s_post_list.html")
def post_archive(request):
    """
    All posts view
    """
    today = datetime.now().strftime("%Y-%m-%d")
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    filter_params = data_containers.FilterOptions()
    items_per_page = 24
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    page = request.params.get("page", 1)
    parameters = request.params.get("parameters", None)
    searchstring = ""
    sort_method = request.params.get("sort", "mod_desc")
    status = request.params.get("status", 1)
    if term := request.params.get("term", ""):
        term = helpers.CommonHelpers.tag_cleaner(term)
        searchstring = "%%%s%%" % ("%%".join(term.split()))
    posts_listing = services.PostService.full_search_paginator(
        request,
        page,
        items_per_page,
        sort_method,
        searchstring,
        status,
        settings,
    )
    filter_params.items_per_page = (items_per_page,)
    filter_params.filter_params = (parameters,)
    filter_params.sort_method = (sort_method,)
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    misc_options = data_containers.MiscOptions(search_url="s_search")
    search = (request.route_url("s_search"),)
    return dict(
        cart=operations.CartOperations.cart_filter_cart_or_customer(request),
        filter_params=filter_params,
        posts_listing=posts_listing,
        search=search,
        settings=settings,
        misc_options=misc_options,
        data_options=data_options,
    )


@view_config(route_name="s_post", renderer="post/s_post.html")
def post_view(request):
    """
    Post view
    """
    post_id = request.matchdict.get("id")
    if not post_id:
        raise HTTPNotFound("No such post")
    post = services.PostService.post_full_filter_by_post_id(request, post_id)
    if not post or post.seo_slug != str(request.matchdict["postname"]):
        raise HTTPNotFound("No such post")
    content = post.content
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    misc_options = data_containers.MiscOptions(search_url="s_search")
    content = BeautifulSoup(
        helpers.CommonHelpers.tag_cleaner(post.content), features="lxml"
    )
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    return dict(
        post=post,
        settings=settings,
        content=content,
        misc_options=misc_options,
        data_options=data_options,
        filter_params=filter_params,
    )


@view_config(
    route_name="post_action",
    match_param="action=create",
    renderer="post/post_edit.html",
    permission="edit",
)
@view_config(
    route_name="post_action",
    match_param="action=edit",
    renderer="post/post_edit.html",
    permission="edit",
)
def post_edit(request):
    """
    Add/edit post
    """
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    url_next = request.matchdict.get("next", "post_list")
    options = helpers.SettingHelpers.default_options(request)
    post_id = request.params.get("post_id", -1)
    form = forms.PostForm(request.POST)
    post = (
        services.PostService.post_full_filter_by_post_id(request, post_id)
        if options.action == "edit"
        else models.Post()
    )
    if request.method == "POST" and form.validate():
        form.populate_obj(post)
        post.date_modified = datetime.now()
        post.seo_slug = slugify(form.seo_slug.data or form.title.data)
        post.image = form.post_image_url.data
        if options.action == "create":
            post.creator_id = request.authenticated_userid
            post.date_added = post.date_modified
            request.dbsession.add(post)
        return HTTPFound(location=url_next)
    return dict(
        action=request.matchdict.get("action"),
        form=form,
        options=options,
        post=post if options.action == "edit" else False,
        url_next=options.url_next,
        settings=settings,
    )


@view_config(
    route_name="post_list",
    renderer="post/post_list.html",
    permission="edit",
)
def post_list(request):
    """
    Admin all posts view
    """
    filter_params = data_containers.FilterOptions()
    items_per_page = 48
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.all(request)
    )
    page = request.params.get("page", 1)
    searchstring = ""
    sort_method = request.params.get("sort", "mod_desc")
    status = request.params.get("status", "")
    if term := request.params.get("term", ""):
        term = helpers.CommonHelpers.tag_cleaner(term)
        searchstring = "%%%s%%" % ("%%".join(term.split()))
    posts_listing = services.PostService.full_search_paginator(
        request,
        page,
        items_per_page,
        sort_method,
        searchstring,
        status,
        settings,
    )

    return dict(
        filter_params=filter_params, posts_listing=posts_listing, settings=settings
    )


@view_config(
    route_name="post_delete",
    renderer="json",
    request_method="POST",
    permission="edit",
    xhr="True",
)
def post_delete(request):
    """
    Delete post
    """
    post = services.PostService.post_full_filter_by_post_id(
        request, request.POST.get("post_id")
    )
    # post_images = services.PostService.prod_image_all(request, post_id)
    if request.method != "POST" or not post:
        request.response.status_code = 400
        request.response.errors = {
            "error": "Wrong request method" if post else "Object doesn't exist"
        }
        return request.response
    else:
        images_types = ("images", "thumbnails", "base")
        if post.image:
            with contextlib.suppress(FileNotFoundError):
                for im_type in images_types:
                    os.remove(helpers.CommonHelpers.asset_path(im_type) + post.image)
        request.dbsession.delete(post)
        helpers.CacheHelpers.flush_cache()
