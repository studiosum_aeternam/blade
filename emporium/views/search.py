from pyramid.view import view_config

from emporium import data_containers, helpers, operations, services


@view_config(route_name="s_product_search", renderer="json")
def product_search(request):
    # data_containers.FilterOptions()
    filter_params = data_containers.FilterOptions(term=request.params.get("term", ""))
    helpers.CommonHelpers.term_search(filter_params)
    try:
        products = services.ProductService.product_search(
            request, term.searchstring, True
        )
        return [
            {
                "name": str(product[1])
                + (f" (EAN {product[3]})" if product[3] else ""),
                "url": product[2] + "-" + str(product[0]),
            }
            for product in products
        ]
    except ValueError:
        return dict(query=term)


@view_config(route_name="limitless_product_search", renderer="json")
def limitless_product_search(request):
    # data_containers.FilterOptions()
    filter_params = data_containers.FilterOptions(term=request.params.get("term", ""))
    helpers.CommonHelpers.term_search(filter_params)
    try:
        products = services.ProductService.product_search(
            request, filter_params.searchstring, False
        )
        return [
            {
                "id": product.product_id,
                "name": product.name,
                "slug": product.seo_slug,
                "model": product.model,
                "image": "",
                "transport_method": product.transport_method,
                "product_type": product.product_type,
            }
            for product in products
        ]
    except ValueError:
        return dict(query=term)


@view_config(route_name="limitless_tag_search", renderer="json")
def limitless_tag_search(request):
    # data_containers.FilterOptions()
    filter_params = data_containers.FilterOptions(term=request.params.get("term", ""))
    helpers.CommonHelpers.term_search(filter_params)
    try:
        tags = services.TagService.tag_id_name_filter_tag_value(
            request, filter_params.searchstring
        )
        return [
            {
                "tag_id": tag.tag_id,
                "tag_value": tag.tag_value,
            }
            for tag in tags
        ]
    except ValueError:
        return dict(query=term)


@view_config(route_name="s_collection_search", renderer="json", xhr="True")
def collection_search(request):
    filter_params = data_options.FilterOptions(
        term=request.params.get("term", ""), searchstring=None
    )
    filter_params.term = helpers.CommonHelpers.initialize_filter_params(
        request, availability_attributes
    )
    filter_params = helpers.CommonHelpers.term_search(filter_params)

    term = filter_params.term
    searchstring = filter_params.searchstring
    try:
        collections = services.CollectionService.collection_search(
            request, searchstring
        )
        return [
            {"id": col.collection_id, "name": col.name + " (" + col.manufacturer + ")"}
            for col in collections
        ]
    except ValueError:
        return dict(query=term)


@view_config(route_name="s_attribute_search", renderer="json", xhr="True")
def attribute_search(request):
    filter_params = data_containers.FilterOptions()
    helpers.CommonHelpers.term_search(filter_params)
    try:
        attributes = services.AttributeService.attribute_search(
            request, term.searchstring
        )
        return [{"id": col[0].attribute_id, "name": col[1].name} for col in attributes]

    except ValueError:
        return dict(query=term)


@view_config(route_name="s_attribute_group_search", renderer="json", xhr="True")
def attribute_group_search(request):
    filter_params = data_containers.FilterOptions()
    helpers.CommonHelpers.term_search(filter_params)
    try:
        attributes = services.AttributeService.attribute_group_search(
            request, term.searchstring
        )
        return [{"id": col.attribute_group_id, "name": col.name} for col in attributes]
    except ValueError:
        return dict(query=term)
