import json

from emporium import data_containers, helpers, operations, services


class CommonOptions(object):
    @classmethod
    def data_options(cls, request, filter_params, settings, fake_admin=False):
        attribute_list = services.AttributeService.attributes_filter_active(
            request, settings, fake_admin=fake_admin
        )
        attribute_ids = [x.attribute_id for x in attribute_list]
        return data_containers.DataOptions(
            availability_dict=helpers.AttributeHelpers.create_attribute_dict_id_value(
                services.AttributeService.attribute_id_value_filter_attribute_group_availability(
                    request,
                    settings,
                )
            ),
            attribute_dict=helpers.AttributeHelpers.attributes_filter_format(
                services.AttributeService.attribute_groups_filter_attribute_list_active(
                    request, attribute_ids, settings
                ),
                attribute_list,
                [],
                [],
            ),
            band_dict=helpers.BandHelpers.bands_format(
                services.BandService.bands_common(
                    request,
                    settings,
                )
            ),
            categories_dict=helpers.CategoryHelpers.build_category_tree(
                helpers.CategoryHelpers.categories_filter_format(
                    services.CategoryService.category_id_image_name_parent_placeholder_slug_filter_dynamic(
                        request,
                        filter_params,
                        settings=settings,
                    )
                )
            ),
            collection_dict=helpers.CollectionHelpers.collections_filter_format(
                [], services.CollectionService.collections_common(request, settings), []
            ),
            header_menu_dict=services.PageService.page_with_content_filter_page_names_current_language(
                request,
                settings["header_menu"].keys() if settings.get("header_menu") else [],
                settings,
            ),
            footer_menu_dict=services.PageService.page_with_content_filter_page_names_current_language(
                request,
                settings["footer_menu"].keys() if settings.get("footer_menu") else [],
                settings,
            ),
            manufacturer_dict=helpers.ManufacturerHelpers.manufacturers_filter_format(
                [],
                services.ManufacturerService.manufacturers_common(request, settings),
                [],
            ),
            number_products=0,
            special_dict=helpers.SpecialHelpers.specials_format(
                services.SpecialService.specials_values_id_name_filter_active(request)
            ),
            storage_dict=helpers.StorageHelpers.storages_format(
                services.StorageService.storage_id_name_filter_active_storage_type(
                    request, 1, settings
                )
            ),
            tax_dict=operations.TaxOperations.generate_tax_list(request, settings),
        )

    @classmethod
    def data_options_cart(cls, request, settings):
        attribute_list = services.AttributeService.attributes_filter_active(
            request, settings, fake_admin=False
        )
        attribute_ids = [x.attribute_id for x in attribute_list]
        return data_containers.DataOptionsCart(
            availability_dict=helpers.AttributeHelpers.create_attribute_dict_id_value(
                services.AttributeService.attribute_id_value_filter_attribute_group_availability(
                    request,
                    settings,
                )
            ),
            attribute_dict=helpers.AttributeHelpers.attributes_filter_format(
                services.AttributeService.attribute_groups_filter_attribute_list_active(
                    request, attribute_ids, settings
                ),
                attribute_list,
                [],
                [],
            ),
            categories_dict=helpers.CategoryHelpers.build_category_tree(
                helpers.CategoryHelpers.categories_filter_format(
                    services.CategoryService.category_id_image_name_parent_placeholder_slug_filter_active(
                        request, settings
                    )
                )
            ),
            country_dict=helpers.CountryHelpers.generate_country_dict(
                services.CountryService.country_list_filter_status(request, settings)
            ),
            header_menu_dict=services.PageService.page_with_content_filter_page_names_current_language(
                request,
                settings["header_menu"].keys() if settings.get("header_menu") else [],
                settings,
            ),
            footer_menu_dict=services.PageService.page_with_content_filter_page_names_current_language(
                request,
                settings["footer_menu"].keys() if settings.get("footer_menu") else [],
                settings,
            ),
            manufacturer_dict=helpers.ManufacturerHelpers.manufacturers_filter_format(
                [],
                services.ManufacturerService.manufacturers_common(request, settings),
                [],
            ),
            special_dict=helpers.SpecialHelpers.specials_format(
                services.SpecialService.specials_values_id_name_filter_active(request)
            ),
            storage_dict=helpers.StorageHelpers.storages_format(
                services.StorageService.storage_id_name_filter_active_storage_type(
                    request, 1, settings
                )
            ),
            tax_dict=operations.TaxOperations.generate_tax_list(request, settings),
            payment_dict=helpers.PaymentHelpers.generate_payment_container(
                services.PaymentService.payment_excerpt_name_status_visibility_filter_status(
                    request, settings, True
                ),
            ),
            transport_dict=helpers.TransportHelpers.transport_with_slots_format(
                services.TransportService.transport_description_excerpt_name_status_visibility_filter_status(
                    request, settings
                ),
                services.TransportService.transport_slots(request, settings),
            ),
        )

    @classmethod
    def data_options_order(cls, request, transport_slot_id, settings):
        return data_containers.DataOptionsCart(
            availability_dict=helpers.AttributeHelpers.create_attribute_dict_id_value(
                services.AttributeService.attribute_id_value_filter_attribute_group_availability(
                    request,
                    settings,
                )
            ),
            categories_dict=helpers.CategoryHelpers.build_category_tree(
                helpers.CategoryHelpers.categories_filter_format(
                    services.CategoryService.category_id_image_name_parent_placeholder_slug_filter_active(
                        request, settings
                    )
                )
            ),
            header_menu_dict=services.PageService.page_with_content_filter_page_names_current_language(
                request,
                settings["header_menu"].keys() if settings.get("header_menu") else [],
                settings,
            ),
            manufacturer_dict=helpers.ManufacturerHelpers.manufacturers_filter_format(
                [],
                services.ManufacturerService.manufacturers_common(request, settings),
                [],
            ),
            special_dict=helpers.SpecialHelpers.specials_format(
                services.SpecialService.specials_values_id_name_filter_active(request)
            ),
            storage_dict=helpers.StorageHelpers.storages_format(
                services.StorageService.storage_id_name_filter_active_storage_type(
                    request, 1, settings
                )
            ),
            tax_dict=operations.TaxOperations.generate_tax_list(request, settings),
            payment_dict=helpers.PaymentHelpers.generate_payment_container(
                services.PaymentService.payment_excerpt_name_status_visibility_filter_status(
                    request, settings, True
                ),
            ),
            transport_dict=helpers.TransportHelpers.transport_with_slots_format(
                services.TransportService.transport_description_excerpt_name_status_visibility_filter_transport_slot_id(
                    request, transport_slot_id, settings
                ),
                services.TransportService.transport_slot_filter_transport_slot_id(
                    request, transport_slot_id, settings
                ),
            ),
        )

    @classmethod
    def admin_data_options_limited(cls, request, settings, today):
        attribute_dict = helpers.AttributeHelpers.attributes_filter_format(
            services.AttributeService.attribute_groups_filter_active(request, settings),
            services.AttributeService.attribute_group_attribute_filter_active(
                request, settings
            ),
            [],
            [],
        )
        categories_dict = helpers.CategoryHelpers.categories_filter_format(
            services.CategoryService.category_id_image_name_parent_placeholder_slug_filter_active(
                request, settings
            )
        )
        manufacturer_dict = helpers.ManufacturerHelpers.manufacturers_filter_format(
            [], services.ManufacturerService.manufacturers_common(request, settings), []
        )
        collection_dict = {}
        tax_dict = helpers.TaxHelpers.create_tax_dict_key_id(
            services.TaxService.tax_id_name_value_filter_active(request)
        )
        return data_containers.DataOptions(
            attribute_dict=attribute_dict,
            categories_dict=categories_dict,
            collection_dict=collection_dict,
            manufacturer_dict=manufacturer_dict,
            tax_dict=tax_dict,
        )

    @classmethod
    def admin_data_options(cls, request, settings):
        # MANUFACTURER - used by other data
        all_manufacturers = services.ManufacturerService.manufacturers_common(
            request, settings
        )
        manufacturer_dict = helpers.ManufacturerHelpers.manufacturers_filter_format(
            [], [item for item in all_manufacturers if item.manufacturer_type == 0], []
        )
        manufacturer_dict_supplemental = (
            helpers.ManufacturerHelpers.manufacturers_filter_format(
                [],
                [item for item in all_manufacturers if item.manufacturer_type == 1],
                [],
            )
        )
        # ATTRIBUTE
        attributes_with_group = (
            services.AttributeService.attribute_group_attribute_filter_active(
                request, settings
            )
        )
        attribute_dict = helpers.AttributeHelpers.attributes_filter_format_mapping(
            services.AttributeService.attribute_groups_filter_active(request, settings),
            attributes_with_group,
            [],
            [],
        )
        attribute_values_dict = helpers.AttributeHelpers.attributes_values_and_names(
            attributes_with_group
        )
        # BAND
        band_dict = helpers.BandHelpers.bands_format(
            services.BandService.bands_common(request, settings)
        )
        # CATEGORY
        categories_dict = helpers.CategoryHelpers.categories_filter_format(
            services.CategoryService.category_id_image_name_parent_placeholder_slug(
                request, settings
            )
        )
        # COLLECTION
        collection_dict = helpers.CollectionHelpers.admin_collections_format(
            services.CollectionService.collections_common(request, settings),
            manufacturer_dict,
        )
        category_tree = helpers.CategoryHelpers.build_category_tree(categories_dict)
        ## DISCOUNT ##
        # discounts = (
        #     services.DiscountService.discount_id_value_manufacturer_filter_active(
        #         request
        #     )
        # )
        # discount_list = helpers.DiscountHelpers.discounts_format(discounts)
        # FILE
        file_type_dict = {
            item.file_type_id: item.file_type_name
            for item in services.FileService.file_type_name_description_filter_status(
                request, settings
            )
        }
        file_type_json = json.dumps(file_type_dict)
        # MANUFACTURER - is first on the list, used by other data
        # SPECIAL- featurerd/promo
        special_dict = helpers.SpecialHelpers.specials_format(
            services.SpecialService.specials_values_id_name_filter_active(request)
        )
        promotion_list = {
            k: v for k, v in special_dict.items() if v["price_changing"] == True
        }
        featured_list = {
            k: v for k, v in special_dict.items() if v["price_changing"] == False
        }
        # STORAGE
        storages = services.StorageService.storage_id_name_filter_active(request)
        storage_virtual = helpers.StorageHelpers.storages_format(
            [item for item in storages if item.storage_type == 2]
        )
        storage_physical = helpers.StorageHelpers.storages_format(
            [item for item in storages if item.storage_type == 1]
        )
        # TAX
        tax_dict = helpers.TaxHelpers.create_tax_dict_key_id(
            services.TaxService.tax_id_name_value_filter_active(request, settings)
        )
        # TRANSPORT
        transport_dict = helpers.TransportHelpers.transport_with_slots_format(
            services.TransportService.transport_description_excerpt_name_status_visibility_filter_status(
                request, settings
            ),
            services.TransportService.transport_slots_filter_special_method(
                request, settings
            ),
        )
        # TEMPLATE
        template_dict = services.SettingsService.setting_templates_filter_active(
            request
        )
        return data_containers.DataOptions(
            attribute_dict=attribute_dict,
            attribute_values_dict=attribute_values_dict,
            band_dict=band_dict,
            categories_dict=categories_dict,
            category_tree=category_tree,
            collection_dict=collection_dict,
            featured_list=featured_list,
            file_type_dict=file_type_dict,
            file_type_json=file_type_json,
            manufacturer_dict=manufacturer_dict,
            manufacturer_dict_supplemental=manufacturer_dict_supplemental,
            promotion_list=promotion_list,
            special_dict=special_dict,
            storage_physical=storage_physical,
            storage_virtual=storage_virtual,
            tax_dict=tax_dict,
            template_dict=template_dict,
            transport_dict=transport_dict,
        )
