from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config


from emporium import forms
from emporium import helpers
from emporium import models
from emporium import services


@view_config(
    route_name="settings_list",
    renderer="settings/settings_list.html",
    permission="edit",
)
def settings_list(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    return dict(admin_settings=settings, settings_list=settings, settings=settings)


@view_config(
    route_name="settings_visibility",
    renderer="settings/settings_visibility/settings_visibility_main.html",
    match_param="action=main",
    permission="edit",
)
@view_config(
    route_name="settings_visibility",
    renderer="settings/settings_visibility/settings_visibility_list.html",
    match_param="action=list",
    permission="edit",
)
@view_config(
    route_name="settings_visibility",
    renderer="settings/settings_visibility/settings_visibility_product.html",
    match_param="action=product",
    permission="edit",
)
@view_config(
    route_name="settings_main",
    renderer="settings/settings_main.html",
    permission="edit",
)
def settings_main(request):
    settings_list = services.SettingsService.all(request)
    settings_dict = {}
    for item in settings_list:
        if item.setting_type == 0:
            settings_dict[item.key] = item.value
        elif item.setting_type == 2:
            settings_dict[item.key] = item.complex_values
    settings_dates = {
        item.key: item.date
        for item in settings_list
        if item.key in ("date_start", "date_end")
    }
    order_status_list = (
        services.StatusService.status_description_name_id_filter_active_group_id(
            request, 1
        )
    )  # 1 is a order status list
    template_list = services.SettingsService.admin_setting_templates(request)
    pages = services.PageService.all(request)
    if request.method == "POST":
        for k in settings_list:
            if k.key == "mail_password":
                if request.params.get("mail_password") != "":
                    k.value = request.params.get("mail_password")
            elif k.value != request.params.get(k.key):
                k.value = request.params.get(k.key)
            if k.date and k.date != request.params.get(k.key):
                k.date = request.params.get(k.key)
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="settings_main")
    return dict(
        settings_dates=settings_dates,
        order_status_list=order_status_list,
        settings_dict=settings_dict,
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
        template_list=template_list,
        pages=pages,
    )


@view_config(
    route_name="settings_action",
    match_param="action=edit",
    renderer="settings/settings_edit.html",
    permission="edit",
)
@view_config(
    route_name="settings_action",
    match_param="action=create",
    renderer="settings/settings_edit.html",
    permission="edit",
)
def edit_settings(request):
    action = request.matchdict.get("action")
    setting = models.Setting()
    if action == "edit":
        setting_id = request.params.get("setting_id")
        setting = services.SettingsService.by_id(request, setting_id)
    form = forms.SettingForm(request.POST)
    if request.method == "POST" and form.validate():
        setting.value = form.value.data
        setting.key = form.key.data
        setting.setting_type = form.setting_type.data
        if action == "create":
            request.dbsession.add(setting)
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="settings_list")
    return dict(
        form=form,
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
        action=action,
        setting=setting if action == "edit" else "",
    )


@view_config(
    route_name="settings_delete", renderer="json", permission="edit", xhr="True"
)
def settings_delete(request):
    if setting := services.SettingsService.col_by_id(
        request, request.POST.get("settings_id", -1)
    ):
        request.dbsession.delete(setting)


@view_config(
    route_name="settings_template_list",
    renderer="settings/settings_template_list.html",
    permission="edit",
)
def settings_template_list(request):
    """
    Setting templates - list view
    """
    # special_list = SpecialService.specials_values_id_name(request)
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    template_list = services.SettingsService.admin_setting_templates(request)
    return dict(template_list=template_list, settings=settings)


@view_config(
    route_name="settings_template_action",
    match_param="action=create",
    renderer="settings/settings_template_edit.html",
    permission="edit",
)
@view_config(
    route_name="settings_template_action",
    match_param="action=edit",
    renderer="settings/settings_template_edit.html",
    permission="edit",
)
def settings_template_action(request):
    """
    Template add/edit view
    """
    options = helpers.SettingHelpers.default_options(request)
    attributes_dict = (
        helpers.AttributeHelpers.create_attribute_dict_type_example_groups(
            services.AttributeService.attribute_group_list(request)
        )
    )
    setting_template = False
    if options.action == "edit":
        setting_template = services.SettingsService.setting_template_full_filter_by_id(
            request, request.params.get("setting_template_id")
        )
    form = forms.SettingTemplateForm(request.POST)
    if request.method == "POST" and form.validate():
        if options.action == "create":
            setting_template = models.SettingTemplate()
        setting_template.description = form.description.data
        setting_template.name = form.name.data
        setting_template.order = form.order.data
        setting_template.status = form.status.data
        setting_template.template_type = form.template_type.data
        attribute_type_list = [
            int(x) for x in request.params.getall("attribute_group_list") if x
        ]
        setting_template.template_json = {
            "manufactured": form.manufactured.data,
            "transport_method": form.transport_method.data,
            "subtract_quantity": form.subtract_quantity.data,
            "storage_type": "",
            "attribute_type_list": attribute_type_list,
            "unit_label": form.unit_label.data,
        }
        if options.action == "create":
            request.dbsession.add(setting_template)
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="settings_template_list")
    return dict(
        action=options.action,
        attributes_dict=attributes_dict,
        form=form,
        options=options,
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
        setting_template=setting_template,
    )


@view_config(
    route_name="settings_template_delete",
    renderer="json",
    request_method="POST",
    permission="edit",
    xhr="True",
)
def settings_template_delete(request):
    """
    Delete setting template
    """
    setting_template = services.SettingsService.setting_template_full_filter_by_id(
        request, request.POST.get("setting_template_id")
    )
    if request.method != "POST" or not setting_template:
        request.response.status_code = 400
        request.response.errors = {
            "error": "Wrong request method"
            if setting_template
            else "Object doesn't exist"
        }
        return request.response
    else:
        request.dbsession.delete(setting_template)
        helpers.CacheHelpers.flush_cache()
