from datetime import datetime

# from pathlib import Path
from slugify import slugify
import os
import pickle

from sqlalchemy import delete
from pyramid.view import view_config

from emporium import data_containers, helpers, operations, models, services, views
from ..views.saturn_product import (
    api_saturn_get_product_prices,
    api_saturn_get_product_stock,
    api_saturn_get_single_product_stock,
)


@view_config(route_name="s_home", renderer="home/s_home.html")
def shop_index(request):
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    data_options_filtered = operations.ProductOperations.return_product_related_data(
        request, settings, [], data_options.tax_dict
    )
    banners = services.BannerService.banners_values_captions_urls_filter_date_page(
        request, 0, settings
    )  # Main with and without dynamic rotation
    blocks = services.BlockService.blocks_values_content_title_size_filter_page_date(
        request, 0, settings
    )
    selling_options = operations.ProductOperations.formatted_featured_products_v2(
        request, settings, data_options, data_options_filtered, 4
    )
    page_content = None
    if settings.get("main_page"):
        page_content = services.PageService.page_filter_id(
            request, settings.get("main_page")
        )
    meta_options = data_containers.MetaOptions(rel_link=request.route_url("s_search"))
    misc_options = data_containers.MiscOptions(search_url="s_search")
    if request.params.get("CART"):
        session = request.session
        session_values = [
            "order_confirmed",
            "oid",
            "delivery",
            "payment",
            "transport",
            "login_error",
            "attempt",
        ]
        for sv in session_values:
            if session.get(sv):
                del session[sv]
        response = request.response
        cookies = ["cart_contents", "delivery_details"]
        for cookie in cookies:
            response.delete_cookie(cookie)
    return dict(
        banners=banners,
        blocks=blocks,
        main_page=True,
        filter_params=filter_params,
        data_options=data_options,
        data_options_filtered=data_options_filtered,
        meta_options=meta_options,
        misc_options=misc_options,
        page_content=page_content,
        settings=settings,
        selling_options=selling_options,
        query=[
            htem
            for htem in request.GET.items()
            if htem[0] != "page"
            and "search" in request.url
            or htem[0] != "page"
            and "category" in request.url
        ],
    )


@view_config(
    route_name="home",
    renderer="home/home.html",
    permission="edit",
    http_cache=1,
)
def admin_index(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    status_list = (
        services.StatusService.status_description_name_id_filter_active_group_id(
            request, 1
        )
    )
    order_list = services.OrderService.order_order_history_filter_status_number_orders(
        request, 24
    )
    formatted_order_list = []
    updated_product_list = []
    all_prices_are_updated = False
    for item in order_list:
        order = data_containers.OrderContainer(
            date_ordered=item.date_ordered.strftime("%d/%m/%Y %H:%M"),
            personal_data=item.client_json["first_name"]
            + " "
            + item.client_json["last_name"],
            telephone="t. " + item.client_json["telephone"],
            summary_cost=item.params_json.get("final_cost")
            or item.params_json.get("summary"),
            order_id=item.order_id,
            uuid=item.uuid,
            status=[x.name for x in status_list if x.status_id == item.status][0],
            last_update=item.date_updated.strftime("%d/%m/%Y %H:%M"),
        )
        formatted_order_list.append(order)
    recent_carts = services.CartService.carts_filter_number_days(request)
    date_format = "%Y/%m/%d"
    possible_carts = []
    for item in recent_carts:
        if item.date_creation.strftime(date_format) not in (
            x["date"] for x in possible_carts
        ):
            days_cart = {
                "date": item.date_creation.strftime(date_format),
                "number_carts": 0,
                "number_orders": 0,
                "order_summary": 0,
            }
            possible_carts.append(days_cart)
        days_cart["number_carts"] += 1
    recent_orders = services.OrderService.orders_filter_number_days(request)
    for item in recent_orders:
        if item[0].strftime(date_format) not in (x["date"] for x in possible_carts):
            days_cart = {
                "date": item[0].strftime(date_format),
                "number_carts": 0,
                "number_orders": 0,
                "order_summary": 0,
            }
            possible_carts.append(days_cart)
        for elem in possible_carts:
            if elem["date"] == item[0].strftime(date_format):
                elem["order_summary"] += float(
                    (item.params_json.get("final_cost")).replace(",", ".")
                )
                elem["number_orders"] += 1
    if request.params.get("flush_redis"):
        helpers.CacheHelpers.flush_cache()
    if request.params.get("refresh_thumbnails"):
        product_manufacturers_bands = services.ProductService.file_upload_paths(request)
        source_image_paths = {
            item.band_id: item.manufacturer_slug + "/" + item.band_slug
            for item in product_manufacturers_bands
        }
        product_images = services.ProductService.select_product_thumbnails_with_band(
            request
        )
        images_paths = []
        for item in product_images:
            images_paths.append((source_image_paths[item.band_id], item.image))
        extra_images = (
            services.ProductService.select_additional_product_thumbnails_with_band(
                request
            )
        )
        for item in extra_images:
            images_paths.append((source_image_paths[item[1]], item[0].image))
        # Verify
        helpers.ImageHelpers.generate_image_thumbnail(images_paths)
    if request.params.get("duplicate_language_descriptions"):
        product_descriptions = (
            services.ProductService.product_description_list_filter_language_id(
                request, language_id=2
            )
        )
        new_descriptions = []
        for item in product_descriptions:
            new_descriptions.append(
                models.ProductDescription(
                    product_id=item.product_id,
                    description=item.description,
                    description_technical=item.description_technical,
                    excerpt=item.excerpt,
                    language_id=1,
                    meta_description=item.meta_description,
                    name=item.name,
                    tag=item.tag,
                    u_h1=item.u_h1,
                    u_title=item.u_title,
                )
            )
        request.dbsession.add_all(new_descriptions)
    if request.params.get("generate_source_images"):
        product_manufacturers_bands = services.ProductService.file_upload_paths(request)
        extra_images = services.ProductService.select_additional_product_thumbnails(
            request
        )
        images_paths = []
        manufacturers = {
            item.manufacturer_id: item.seo_slug
            for item in services.ManufacturerService.manufacturers_return_all(request)
        }
        # helpers.ImageHelpers.generate_source_images_from_existing_uploads(images_paths)
        # images_paths = []
        product_images = services.ProductService.select_product_thumbnails(request)
        for item in product_images:
            images_paths.append(
                (
                    item.image,
                    manufacturers[item.manufacturer_id]
                    + "/"
                    + item.image[item.image.find("/") + 1 :],
                ),
            )
        # To update field I need to call the same table once more
        extra_images = services.ProductService.select_extra_product_thumbnails(request)
        helpers.ImageHelpers.generate_source_images_from_existing_uploads(images_paths)
        for item in extra_images + product_images:
            current_image = str(item.image[item.image.find("/") + 1 :])
            item.image = current_image[: str(current_image).find(".")]
        for item in product_images:
            current_image = str(item.image[item.image.find("/") + 1 :])
        # item.image = current_image[: str(current_image).find(".") + 1]
    if request.params.get("create_folders_for_missing_manufacturers"):
        # bands = services.BandService.bands_return_all(request)
        # for item in bands:
        #     item.seo_slug = slugify(item.name)
        manufacturers = services.ManufacturerService.manufacturers_return_all(request)
        product_manufacturers_bands = services.ProductService.file_upload_paths(request)
        for slug in product_manufacturers_bands:
            operations.CommonOperations.generate_image_upload_folders(slug[0], slug[1])
    if request.params.get("full_update"):
        if updated_products := api_saturn_get_product_prices(request):
            product_list = services.ProductService.product_object_filter_ean(
                request, updated_products
            )
            current_date = datetime.now()
            merged_dicts = helpers.ProductHelpers.merge_product_dicts(
                updated_products,
                helpers.ProductHelpers.create_product_dict_key_ean(product_list),
            )
            updated_product_list = operations.ProductOperations.update_customer_prices(
                helpers.ProductHelpers.product_list_filter_manual_prices(
                    operations.ProductOperations.update_catalog_prices(
                        merged_dicts, product_list, current_date
                    )
                ),
                services.DiscountService.discount_list_id_value(request),
                current_date,
            )
        else:
            all_prices_are_updated = True
    if request.params.get("stock_create"):
        product_list = services.ProductService.product_id_filter_ean(request)
        new_stocks = []
        for item in product_list:
            new_stocks.append(
                models.ProductToStorage(product_id=item, storage_id=1, quantity=0)
            )
        request.dbsession.add_all(new_stocks)

    if request.params.get("stock_update"):
        if updated_products := api_saturn_get_product_stock(request):
            product_list = (
                services.ProductService.product_id_storage_id_quantity_filter_ean_list(
                    request, updated_products, 1
                )
            )
            merged_dicts = helpers.ProductHelpers.merge_product_storage(
                updated_products,
                helpers.ProductHelpers.create_product_stock_dict_key_ean(product_list),
            )
            updated_product_list = operations.ProductOperations.update_stock(
                merged_dicts,
                services.ProductService.product_storage_filter_storage_ean_list(
                    request, updated_products, 1
                ),
            )
        else:
            storage_is_updated = True
    # if request.params.get("single_stock_update_key_ean"):
    #     if updated_products := api_saturn_get_single_product_stock(request):
    #         print("--------------\n", updated_products)
    if request.params.get("json_cart"):
        carts = services.CartService.carts_all_elements(request)
        for ghem in carts:
            if ghem.date_accessed:
                if isinstance(ghem.products, dict):
                    for k, v in ghem.products.items():
                        ghem.products_json = {
                            str(k): {
                                "product_single_set": v["product_single_set"]
                                if isinstance(v, dict)
                                else v.product_single_set,
                                "product_quantity": str(v["product_quantity"])
                                if isinstance(v, dict)
                                else v.product_quantity,
                            }
                        }
                else:
                    for item in ghem.products:
                        ghem.products_json = {
                            str(item["product_id"]): {
                                "product_single_set": item.get(
                                    "product_single_set", True
                                )
                                if isinstance(item, dict)
                                else item.product_single_set,
                                "product_quantity": str(item["product_quantity"])
                                if isinstance(item, dict)
                                else item.product_quantity,
                            }
                        }
            else:
                del ghem
    if request.params.get("update_transport"):
        free_products = services.ProductService.special_products_filter_free_delivery(
            request
        )
        special_delivery = []
        for item in free_products:
            special_delivery.append(
                models.ProductToTransport(
                    product_id=item.product_id,
                    transport_slot_id=5,
                    local_settings={
                        "min_units": str(item.allowed_free_transport_minimum_amount),
                    },
                    status=1,
                )
            )

        special_products = (
            services.ProductService.special_products_filter_large_delivery(request)
        )
        for item in special_products:
            if (
                item.forbidden_transport_methods
                and len(item.forbidden_transport_methods) > 0
                and (
                    "2" in item.forbidden_transport_methods
                    or item.forbidden_transport_methods == 2,
                )
            ):
                special_delivery.append(
                    models.ProductToTransport(
                        product_id=item.product_id,
                        transport_slot_id=2,
                        local_settings={},
                        status=1,
                    )
                )
            elif (
                item.forbidden_transport_methods
                and len(item.forbidden_transport_methods) > 0
                and (
                    "1" in item.forbidden_transport_methods
                    or item.forbidden_transport_methods == 1,
                )
            ):
                special_delivery.append(
                    models.ProductToTransport(
                        product_id=item.product_id,
                        transport_slot_id=4,
                        local_settings={},
                        status=1,
                    )
                )
        request.dbsession.add_all(special_delivery)
    if request.params.get("refresh_category_description"):
        category_desc_list = services.CategoryService.category_description_all(request)
        for item in category_desc_list:
            category_description = models.CategoryMetaTreeDescriptions(
                category_id=item.category_id,
                collection_id=None,
                description=item.description,
                description_long=item.description_long,
                language_id=item.language_id,
                manufacturer_id=None,
                meta_description=item.meta_description,
                seo_slug=item.seo_slug,
                u_h1=item.u_h1,
                u_title=item.u_title,
            )
            request.dbsession.add(category_description)
    admin_buttons = {
        "flush_redis": ("Wyczyść cache", True, "red"),
        "duplicate_language_descriptions": ("Zduplikuj opisy językowe", True, "blue"),
        # "full_update": ("Pobierz wszystkie ceny (SATURN)", True, "blue"),
        # "stock_update": ("Pobierz stany magazynowe (SATURN)", True, "blue"),
        # "single_stock_update_key_ean": (
        #     "Pobierz jednostkowy stan magazynu (SATURN)",
        #     True,
        #     "blue",
        # ),
        # "inpost_szit": ("Inpost logił", True, "blue"),
    }
    return dict(
        all_prices_are_updated=all_prices_are_updated,
        formatted_order_list=formatted_order_list,
        possible_carts=possible_carts,
        settings=settings,
        transport_methods=services.TransportService.transport_list_filter_active(
            request, settings
        ),
        updated_product_list=updated_product_list,
        admin_buttons=admin_buttons,
    )
