from datetime import datetime


from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config


from ..data_containers import CategoryContainer
from ..forms import CollectionCreateForm, CollectionUpdateForm
from ..helpers import CacheHelpers, SettingHelpers
from ..models import (
    CategoryMetaTreeDescriptions,
    Collection,
    CollectionDescription,
    CollectionToCollection,
    ProductToCollection,
)
from ..services import (
    CategoryService,
    ManufacturerService,
    ProductService,
    CollectionService,
    SettingsService,
)

from emporium import operations


@view_config(
    route_name="collection_action",
    match_param="action=create",
    renderer="collection/collection_edit.html",
    permission="edit",
)
def add_collection(request):
    options = SettingHelpers.default_options(request)
    form = CollectionCreateForm(request.POST)
    manufacturers = ManufacturerService.manufacturers_id_name(request, status=False)
    if request.method == "POST" and form.validate():
        collection = Collection(
            status=1,
            date_modified=datetime.now(),
            date_added=datetime.now(),
            name=operations.ProductOperations.strip_value(form.name.data),
        )
        form.populate_obj(collection)
        if form.properties.data:
            collection.properties = [int(e) for e in form.properties.data.split(",")]
        request.dbsession.add(collection)
        request.dbsession.flush()
        if form.products.data:
            for elem in form.products.data.split(","):
                request.dbsession.add(
                    ProductToCollection(
                        collection_id=collection.collection_id,
                        product_id=elem,
                    )
                )
        if form.similar_collections.data:
            for elem in form.similar_collections.data.split(","):
                request.dbsession.add(
                    CollectionToCollection(
                        collection1_id=collection.collection_id,
                        collection2_id=elem,
                    )
                )
        request.dbsession.add(
            CollectionDescription(
                collection_id=collection.collection_id,
                language_id=form.language_id.data,
                name=form.name.data,
                description=form.description.data,
                description_long=form.description_long.data,
                meta_description=form.meta_description.data,
                u_title=form.u_title.data,
                u_h1=form.u_h1.data,
            )
        )
        CacheHelpers.flush_cache()
        return HTTPFound(location="collection_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        manufacturers=manufacturers,
        products="",
        pro=[],
        pro_ids=[],
        col=[],
        col_ids=[],
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
        options=options,
    )


@view_config(
    route_name="collection_action",
    match_param="action=edit",
    renderer="collection/collection_edit.html",
    permission="edit",
)
def edit_collection(request):
    collection_id = request.params.get("collection_id", -1)
    options = SettingHelpers.default_options(request)
    options.categories_list = []
    collections = []
    collections_ids = []
    settings = SettingHelpers.admin_settings(SettingsService.admin_settings(request))
    manufacturers = ManufacturerService.manufacturers_id_name(request, status=False)
    form = CollectionUpdateForm(request.POST)
    if collection_id and options.action == "edit":
        collection = CollectionService.collection_filter_collection_id_active(
            request, collection_id
        )
        collections_related = (
            CollectionService.collections_related_filter_collection_id_values_id_name(
                request, collection_id, language_id=2
            )
        )
        for item in collections_related:
            collections.append({"id": item[0], "name": item[1]})
            collections_ids.append(item[0])
        products = []
        products_ids = []
        product_list = ProductService.product_filter_collection_id_values_id_name(
            request, collection_id
        )
        for item in product_list:
            products.append({"id": item[0], "name": item[1]})
            products_ids.append(item[0])
        category_meta_tree = CategoryService.category_meta_tree_filtered(
            request,
            None,
            None,
            collection_id,
            collection.manufacturer_id,
        )
        category_list = CategoryService.category_id_name_parent_description_meta_seo_h1_title_filter_manufacturer(
            request, collection.manufacturer_id, collection_id, False
        )
        categories_id_list = [item.category_id for item in category_list]
        category_list += CategoryService.category_id_name_parent_filter_manufacturer_exclude_category_list(
            request,
            collection.manufacturer_id,
            categories_id_list,
            settings,
            collection_id,
        )
        for item in category_list:
            options.categories_list.append(
                CategoryContainer(
                    category_id=item.category_id,
                    description=item.description if len(item) > 4 else None,
                    description_long=item.description_long if len(item) > 4 else None,
                    meta_description=item.meta_description if len(item) > 4 else None,
                    name=item.name,
                    parent_id=item.parent_id,
                    subcategories=[],
                    u_h1=item.u_h1 if len(item) > 4 else None,
                    u_title=item.u_title if len(item) > 4 else None,
                )
            )
    if request.method == "POST" and form.validate():
        # Simple parameters
        collection.date_modified = datetime.now()
        collection.name = operations.ProductOperations.strip_value(form.name.data)
        collection.status = form.status.data
        collection.sort_order = form.sort_order.data
        collection.priority = form.priority.data
        # Combined/modified on the fly elements
        for jtem in category_meta_tree:
            jtem.description = request.params.get(
                "description_" + str(jtem.category_id)
            )
            jtem.description_long = request.params.get(
                "description_long_" + str(jtem.category_id)
            )
            jtem.meta_description = request.params.get(
                "meta_description_" + str(jtem.category_id)
            )
            jtem.u_title = request.params.get("u_title_" + str(jtem.category_id))
            jtem.u_h1 = request.params.get("u_h1_" + str(jtem.category_id))
        for item in [
            x
            for x in category_list
            if x.category_id not in (y.category_id for y in category_meta_tree)
        ]:
            new_meta = CategoryMetaTreeDescriptions(
                category_id=item.category_id,
                manufacturer_id=collection.manufacturer_id,
                collection_id=collection_id,
                language_id=2,
                description=request.params.get("description_" + str(item.category_id)),
                description_long=request.params.get(
                    "description_long_" + str(item.category_id)
                ),
                meta_description=request.params.get(
                    "meta_description_" + str(item.category_id)
                ),
                u_title=request.params.get("u_title_" + str(item.category_id)),
                u_h1=request.params.get("u_h1_" + str(item.category_id)),
            )

            if any(
                x
                for x in [
                    new_meta.description,
                    new_meta.description_long,
                    new_meta.meta_description,
                    new_meta.u_title,
                    new_meta.u_h1,
                ]
                if x
            ):
                request.dbsession.add(new_meta)
        if form.products.data:
            new_collection_elements = [int(e) for e in form.products.data.split(",")]
            to_add = set(new_collection_elements).difference(products_ids)
            for elem in to_add:
                request.dbsession.add(
                    ProductToCollection(collection_id=collection_id, product_id=elem)
                )
            to_delete = set(products_ids).difference(new_collection_elements)
            if to_delete:
                for item in ProductService.col_to_prod_list(
                    request, collection_id, to_delete
                ):
                    request.dbsession.delete(item)
                request.dbsession.flush()
        if form.similar_collections.data:
            new_elements = [int(e) for e in form.similar_collections.data.split(",")]
            to_add = set(new_elements).difference(collections_ids)
            for elem in to_add:
                request.dbsession.add(
                    CollectionToCollection(
                        collection1_id=collection_id, collection2_id=elem
                    )
                )
            to_delete = set(products_ids).difference(new_elements)
            if to_delete:
                for item in CollectionService.collection_array_filter_colection_list(
                    request, to_delete
                ):
                    request.dbsession.delete(item)
        elif form.similar_collections.data == "":
            collections = CollectionService.collection_array_filter_colection_list(
                request, list(collections_ids)
            )
            if collections:
                for item in collections:
                    request.dbsession.delete(item)
        if form.properties.data:
            collection.properties = [int(e) for e in form.properties.data.split(",")]

        CacheHelpers.flush_cache()
        return HTTPFound(location="collection_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        collection=collection,
        manufacturers=manufacturers,
        pro=products,
        pro_ids=products_ids,
        col=collections,
        col_ids=collections_ids,
        options=options,
        category_list=category_list,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


@view_config(
    route_name="collection_list",
    renderer="collection/collection_list.html",
    permission="edit",
)
def collection_list(request):
    settings = SettingHelpers.admin_settings(SettingsService.admin_settings(request))
    page = request.params.get("page", 1)
    search = request.params.get("search")
    sort_method = request.params.get("sort", "mod_desc")
    number_collections = request.params.get("number_collections", 108)
    filter_params = {}
    searchstring, manufacturer = (None,) * 2
    if search:
        fragment = request.params.get("term", None)
        if fragment:
            keywords = fragment.split()
            searchstring = "%%".join(keywords)
            searchstring = "%%%s%%" % (searchstring)
        else:
            searchstring = None
        category = request.params.get("category", None)
        manufacturer = request.params.getall("manufacturer") or None
        filter_params = dict(
            category=category,
            manufacturer=int(manufacturer[0]) if manufacturer else None,
            term=searchstring,
            sort_method=sort_method,
        )
    collections = CollectionService.collections_paginator_filter_dynamic(
        request,
        page,
        number_collections,
        sort_method,
        searchstring,
        manufacturer,
    )
    manufacturers = ManufacturerService.manufacturers_id_name(request, status=False)
    return dict(
        collections=collections,
        manufacturers=manufacturers,
        filter_params=filter_params,
        settings=settings,
    )


@view_config(
    route_name="collection_delete",
    renderer="json",
    permission="edit",
    xhr="True",
)
def collection_delete(request):
    collection_id = request.POST.get("collection_id")
    collection = CollectionService.collection_filter_collection_id_active(
        request, collection_id
    )
    collection_description = (
        CollectionService.collection_description_filter_collection_id_language_id(
            request, collection_id, language_id=2
        )
    )
    product_to_collection = CollectionService.product_collections_filter_collection_id(
        request, collection_id
    )
    collection_to_collection = CollectionService.collection_array_filter_colection_list(
        request, [collection_id]
    )
    if request.method != "POST" or not collection:
        request.response.status_code = 400
        request.response.errors = {
            "error": "Wrong request method" if collection else "Object doesn't exist"
        }
        return request.response
    else:
        if collection_to_collection:
            for item in collection_to_collection:
                if (
                    item.collection1_id == collection_id
                    or item.collection2_id == collection_id
                ):
                    request.dbsession.delete(item)
                continue
        if collection_description:
            request.dbsession.delete(collection_description)
        if product_to_collection:
            for item in list(product_to_collection):
                request.dbsession.delete(item)
        if collection:
            request.dbsession.delete(collection)
        CacheHelpers.flush_cache()
