from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from ..forms import PaymentCreateForm, PaymentUpdateForm
from ..helpers import CacheHelpers, SettingHelpers
from ..models import Payment
from ..services import PaymentService, SettingsService


@view_config(
    route_name="payment_action",
    match_param="action=create",
    renderer="payment/payment_edit.html",
    permission="edit",
)
@view_config(
    route_name="payment_action",
    match_param="action=edit",
    renderer="payment/payment_edit.html",
    permission="edit",
)
def edit_payment(request):
    options = SettingHelpers.default_options(request)
    if options.action == "edit":
        payment_id = request.params.get("payment_id", -1)
        payment = PaymentService.payment_by_id(request, payment_id)
        form = PaymentUpdateForm(request.POST, payment)
    else:
        payment = Payment()
        form = PaymentCreateForm(request.POST)
    if request.method == "POST" and form.validate():
        payment.api_version = form.api_version.data
        payment.description = form.description.data
        payment.equotation = form.equotation.data
        payment.fee_percent = form.fee_percent.data
        payment.fee_value = form.fee_value.data
        payment.name = form.name.data
        payment.status = form.status.data
        payment.type = form.type.data
        payment.key = form.key.data
        payment.merchant_id = form.merchant_id.data
        payment.mode = form.mode.data
        payment.pos_id = form.pos_id.data or form.merchant_id.data
        payment.url_request = form.url_request.data
        payment.url_test = form.url_test.data
        payment.url_transaction = form.url_transaction.data
        payment.url_test_verify = form.url_test_verify.data
        payment.url_transaction_verify = form.url_transaction_verify.data
        if options.action == "create":
            request.dbsession.add(payment)
            request.dbsession.flush()
        CacheHelpers.flush_cache()
        return HTTPFound(location="payment_list")
    return dict(
        action=request.matchdict.get("action"),
        form=form,
        payment=payment if options.action == "edit" else False,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
        options=options,
    )


@view_config(
    route_name="payment_delete", renderer="json", permission="edit", xhr="True"
)
def payment_delete(request):
    payment_id = request.POST.get("payment_id")
    payment = PaymentService.payment_by_id(request, payment_id)
    if payment_id and payment:
        request.dbsession.delete(payment)
        CacheHelpers.flush_cache()


@view_config(
    route_name="payment_list",
    renderer="payment/payment_list.html",
    permission="edit",
)
def payments(request):
    payments = PaymentService.payment_all(request)
    return dict(
        payments=payments,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )
