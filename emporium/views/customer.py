from datetime import datetime, timedelta
from pathlib import Path
from uuid import uuid4

from hashids import Hashids
from pyramid.csrf import get_csrf_token
from pyramid.httpexceptions import HTTPFound
from pyramid.security import remember
from pyramid.view import view_config


from emporium import (
    helpers,
    data_containers,
    forms,
    models,
    operations,
    services,
    views,
    operations,
)

tmp_path = helpers.CommonHelpers.asset_path("tmp", asset_type="tmp")
hashids = Hashids(salt="PerfeCT_Man")


@view_config(route_name="s_customer", renderer="customer/s_customer.html")
def customer_view(request):
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    misc_options = data_containers.MiscOptions(search_url="s_search")
    next_location = request.route_url("s_customer")
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    filter_params = data_containers.FilterOptions()
    error_level = None
    address = models.Address()
    customer = models.Customer()
    customer_data = {
        "customer": models.Customer(),
        "default_address": models.Address(),
        "orders": [],
        "address_list": [],
    }
    form = forms.CreateCustomerForm(request.POST)
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    payment_methods = services.PaymentService.payment_filter_active(request, settings)
    session = request.session
    status_list = services.StatusService.all_active(request)
    transport_methods = services.TransportService.transport_list_filter_active(
        request, settings
    )
    if session.get("uid"):
        address_list = services.AddressService.by_customer(request, session.get("uid"))
        customer = services.CustomerService.by_id(request, session.get("uid"))
        address = [x for x in address_list if x.defa][0]
        orders = services.OrderService.orders_filter_finalized_customer_id(
            request, session.get("uid")
        )
        customer_data = {
            "customer": customer,
            "default_address": address,
            "orders": orders,
            "address_list": [x for x in address_list if x.id != address.id],
        }
        form = forms.EditCustomerForm(request.POST)
    if session.get("login_error"):
        if session.get("login_error") == "True":
            error_level = "login_error"
        elif session.get("login_error") == "Blocked":
            error_level = "hard_login_error"
    form_data = {}
    if request.method == "POST":
        form_data = {
            "company_nip": form.company_nip.data,
            "company_name": form.company_name.data,
            "first_name": form.first_name.data,
            "last_name": form.last_name.data,
            "telephone": form.telephone.data,
            "mail": form.mail.data,
            "psswd_hsh": "",
            "street": form.street.data,
            "postcode": operations.ProductOperations.strip_value(form.postcode.data),
            "city": form.city.data,
            "country": form.country.data,
            "privacy_policy": form.privacy_policy.data,
        }
        session["privacy_policy"] = form.privacy_policy.data
    if request.method == "POST" and form.validate():
        if not session.get(
            "uid"
        ) and services.CustomerService.customer_single_filter_mail_type(
            request, form.mail.data, 0
        ):  # Customer exists
            form.mail.errors = "Ten mail jest już zarejestrowany w bazie danych!"
        else:
            form.populate_obj(customer)
            form.populate_obj(address)
            if not session.get("uid"):
                customer.registration_date = datetime.now()
                customer.customer_type = 0
                request.dbsession.add(customer)
                request.dbsession.flush()
                address.defa = True
                address.status = True
                address.customer_id = customer.id
                request.dbsession.add(address)
                # headers = remember(request, customer.id)
                session["uid"] = customer.id
                session["order_type"] = "logged_buy"
                operations.CartOperations.cart_merge(request)
                if cid := session.get("cid"):
                    del cid
            headers = remember(request, session.get("uid"), tokens=["customer"])
            return HTTPFound(location=request.route_url("s_customer"), headers=headers)
    return dict(
        customer_data=customer_data,
        data_options=data_options,
        error_level=error_level,
        filter_params=filter_params,
        form=form,
        form_data=form_data,
        hashids=hashids,
        misc_options=misc_options,
        meta_options=data_containers.MetaOptions(
            rel_link=request.route_url("s_search")
        ),
        next_location=next_location,
        payment_methods=payment_methods,
        settings=settings,
        status_list=status_list,
        transport_methods=transport_methods,
    )


@view_config(route_name="s_customer_delete", renderer="emporium:templates/json")
def customer_delete(request):
    session = request.session
    uid = session.get("uid")
    address_list = services.AddressService.by_customer(request, uid)
    customer = services.CustomerService.by_id(request, uid)
    orders = services.OrderService.orders_filter_finalized_customer_id(request, uid)
    pending_order = services.OrderService.orders_filter_pending_customer_id(
        request, uid
    )
    cart = services.CartService.cart_filter_user_id(request, uid)
    if customer:
        if address_list:
            for item in address_list:
                request.dbsession.delete(item)
        if orders:
            for item in orders:
                request.dbsession.delete(item)
        if pending_order:
            request.dbsession.delete(pending_order)
        if cart:
            request.dbsession.delete(cart)
        request.dbsession.delete(customer)
        return HTTPFound(request.route_url("s_auth", action="out"))


@view_config(
    route_name="s_customer_reset",
    renderer="customer/s_customer_reset.html",
)
def customer_reset(request):
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    misc_options = data_containers.MiscOptions(search_url="s_search")
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    success = request.params.get("success", None)
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    tmp_path = helpers.CommonHelpers.asset_path("tmp", asset_type="tmp")
    form = forms.ResetCustomerForm(request.POST)
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    cookies = ("delivery_details", "payment")
    for _ in cookies:
        request.response.delete_cookie(_)
    if request.method == "POST" and form.validate():
        if customer_exists := services.CustomerService.customer_single_filter_mail_type(
            request, form.mail.data, 0
        ):
            expiration_time = datetime.now() + timedelta(minutes=18)
            request_uuid = str(uuid4()).replace("-", "")
            request.dbsession.add(
                models.CustomerReset(
                    customer_id=customer_exists.id,
                    request_uuid=request_uuid,
                    expiration_time=expiration_time,
                )
            )
            recipients = form.mail.data
            with open(f"{tmp_path}reset.html", "w+b") as handle:
                kwargs = {"cookies": request.cookies, "host": request.host}
                link = request.route_url(
                    "s_customer_reset_mail",
                    _query=(("recipients", recipients), ("request_uuid", request_uuid)),
                )
                subrequest = request.blank(link, **kwargs)
                response = request.invoke_subrequest(subrequest, use_tweens=True)
                handle.write(response.body)
                html = Path(f"{tmp_path}reset.html").read_text()
            kwargs = {
                "cookies": request.cookies,
                "host": request.host,
            }  # None type can be called
            link = request.route_url(
                "s_mail",
                _query=(
                    ("html", html),
                    ("subject", "Reset hasła - " + settings["name"]),
                    ("recipients", recipients),
                    ("sender", settings["mail_username"]),
                ),
            )
            subrequest = request.blank(link, **kwargs)
            response = request.invoke_subrequest(subrequest, use_tweens=True)
            success = "True"
            return HTTPFound(
                location=request.route_url(
                    "s_customer_reset", _query=(("success", success),)
                )
            )
        else:
            success = "Failure"
    return dict(
        form=form,
        error_level=success,
        success=success,
        settings=settings,
        misc_options=misc_options,
        data_options=data_options,
        filter_params=filter_params,
    )


@view_config(
    route_name="s_customer_reset_mail",
    renderer="customer/s_customer_reset_mail.html",
)
def customer_reset_mail(request):
    recipients = str(request.params.get("recipients")).encode()
    request_uuid = request.params.get("request_uuid")
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    return dict(recipients=recipients, request_uuid=request_uuid, settings=settings)


@view_config(
    route_name="s_customer_passwd",
    renderer="customer/s_customer_passwd.html",
)
def customer_passwd(request):
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    misc_options = data_containers.MiscOptions(search_url="s_search")
    current_time = datetime.now()
    filter_params = data_containers.FilterOptions()
    error_level = None
    request_uuid = request.params.get("q", None)
    form = forms.NewPasswordForm(request.POST)
    error_level = 99
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    if request.method == "POST" and form.validate():
        customer = services.CustomerService.customer_filter_request_id(
            request, request_uuid
        )
        customer.psswd_hsh = form.password.data
        request.dbsession.flush()
        return HTTPFound(location=request.route_url("s_customer"))
    elif request_uuid:
        customer = (
            services.CustomerService.customer_id_request_expirations_filter_request_id(
                request, request_uuid
            )
        )
        if (customer.expiration_time > current_time) and customer:
            error_level = 0
        else:
            error_level = 411
    else:
        error_level = 417
    return dict(
        error_level=error_level,
        form=form,
        request_uuid=request_uuid,
        settings=settings,
        misc_options=misc_options,
        data_options=data_options,
        filter_params=filter_params,
    )


@view_config(route_name="s_customer_form", renderer="string", request_method="POST")
def customer_form(request):
    location = request.environ.get("HTTP_REFERER", "s_home")
    response = request.response
    headers = response.headers
    register_customer = request.params.get("register_customer")
    if register_customer and register_customer == "True":
        get_csrf_token(request)
        session = request.session
        session["register_customer"] = True
        session["order_type"] = "logged_buy"
    elif request.session.get("register_customer"):
        del request.session["register_customer"]
        # session_values = (
        #     "delivery",
        #     "payment",
        #     "transport",
        #     "order_type",
        #     "privacy_policy",
        # )
        # for _ in session_values:
        #     if request.session.get(_):
        #         del request.session[_]
        # cookies = ("delivery_details", "invoice_type")
        # for _ in cookies:
        #     request.response.delete_cookie(_)
    return HTTPFound(location=location, headers=headers)
