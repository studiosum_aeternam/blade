"""
Block views - dynamic external elements to the static/fixed pages.
"""
from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from ..forms import BlockForm
from ..helpers import CacheHelpers, SettingHelpers
from ..models import Block
from ..services import BlockService, SettingsService


@view_config(
    route_name="block_action",
    match_param="action=create",
    renderer="block/block_edit.html",
    permission="edit",
)
@view_config(
    route_name="block_action",
    match_param="action=edit",
    renderer="block/block_edit.html",
    permission="edit",
)
def block_edit(request):
    """
    Add/edit block
    """
    options = SettingHelpers.default_options(request)
    block_id = request.params.get("block_id", -1)
    block = (
        BlockService.block_full_filter_by_block_id(request, block_id)
        if options.action == "edit"
        else Block()
    )
    form = BlockForm(request.POST)
    if request.method == "POST" and form.validate():
        block.content = request.params.get("content")
        block.creator_id = request.authenticated_userid
        block.order = request.params.get("order")
        block.page = request.params.get("page")
        block.size = request.params.get("size")
        block.status = 1 if request.params.get("status") == "on" else 0
        block.title = request.params.get("title")
        if options.action == "create":
            request.dbsession.add(block)
            request.dbsession.flush()
        CacheHelpers.flush_cache()
        return HTTPFound(location="block_list")
    return dict(
        action=options.action,
        block=block if options.action == "edit" else False,
        form=form,
        options=options,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


@view_config(route_name="block_delete", renderer="json", permission="edit", xhr="True")
def block_delete(request):
    """
    Delete block
    """
    block = BlockService.block_full_filter_by_block_id(
        request, request.POST.get("block_id")
    )
    if request.method != "POST" or not block:
        request.response.status_code = 400
        request.response.errors = {
            "error": "Wrong request method" if block else "Object doesn't exist"
        }
        return request.response
    else:
        request.dbsession.delete(block)
        CacheHelpers.flush_cache()


@view_config(
    route_name="block_list",
    renderer="block/block_list.html",
    permission="edit",
)
def block_list(request):
    """
    List all blocks
    """
    blocks = BlockService.blocks_all(request)
    return dict(
        blocks=blocks,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )
