from pyramid.view import view_config


@view_config(route_name="api_saturn_address_country", xhr=True, request_method="POST")
def api_saturn_address_country(request):
    pass


@view_config(route_name="api_saturn_address_region", xhr=True, request_method="POST")
def api_saturn_address_region(request):
    pass


@view_config(
    route_name="api_saturn_address_client_address", xhr=True, request_method="GET"
)
@view_config(
    route_name="api_saturn_address_client_address", xhr=True, request_method="POST"
)
def api_saturn_client_address(request):
    pass
