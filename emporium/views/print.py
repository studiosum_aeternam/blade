from hashids import Hashids
import pdfkit
from pyramid.httpexceptions import HTTPFound
from pyramid.response import FileResponse
from pyramid.view import view_config

# from weasyprint import HTML - alternative pdf generator
from ..helpers import CommonHelpers
from ..services import CustomerService, OrderService

pdf_config = pdfkit.configuration(wkhtmltopdf="/usr/bin/wkhtmltopdf")
pdf_options = {
    "page-size": "A4",
    "encoding": "UTF-8",
    "margin-top": "6",
    "margin-right": "6",
    "margin-bottom": "6",
    "margin-left": "6",
    "no-outline": None,
    "orientation": "Portrait",
    "quiet": "",
}


@view_config(route_name="system_print", permission="edit")
def system_print(request):
    object_type = request.params.get("object_type", None)
    object_id = request.params.get("id", None)
    filename = f"{str(object_type)}_admin_{str(object_id)}"
    tmp_path = CommonHelpers.asset_path("tmp", asset_type="tmp")
    with open(tmp_path + object_type + ".html", "wb") as f:
        kwargs = {"cookies": request.cookies, "host": request.host}
        link = request.route_url("order_print", id=object_id)
        subrequest = request.blank(link, **kwargs)
        response = request.invoke_subrequest(subrequest, use_tweens=True)
        f.write(response.body)
    #    HTML(tmp_path + object_type + ".html").write_pdf(tmp_path + filename + ".pdf")
    pdfkit.from_file(
        tmp_path + object_type + ".html",
        tmp_path + filename + ".pdf",
        options=pdf_options,
        configuration=pdf_config,
    )
    response = FileResponse(tmp_path + filename + ".pdf")
    response.headers["Content-Disposition"] = f"attachment; filename={filename}.pdf"
    return response


@view_config(route_name="s_print")
def s_print(request):
    session = request.session
    orders = OrderService.orders_filter_finalized_customer_id(
        request, session.get("uid")
    )
    if not orders:
        return HTTPFound(location=request.route_url("s_auth", action="out"))
    object_type = request.params.get("object_type", None)
    hash_order_id = request.params.get("z", None)
    hashids = Hashids(salt="PerfeCT_Man")
    test_order_id = hashids.decode(hash_order_id)
    if (
        not isinstance(test_order_id, tuple)
        or len(test_order_id) != 1
        or "object_type" != "order"
    ):
        return HTTPFound(location=request.route_url("s_customer"))
    object_id = list(test_order_id)[0]
    order_list = [item.order_id for item in orders]
    if object_id not in order_list:
        return HTTPFound(location=request.route_url("s_customer"))
    tmp_path = CommonHelpers.asset_path("tmp", asset_type="tmp")
    with open(tmp_path + object_type + ".html", "wb") as f:
        kwargs = {"cookies": request.cookies, "host": request.host}
        filename = f"{str(object_type)}_{str(object_id)}"
        link = request.route_url("s_order_print", id=object_id)
        subrequest = request.blank(link, **kwargs)
        response = request.invoke_subrequest(subrequest, use_tweens=True)
        f.write(response.body)
    #    HTML(tmp_path + object_type + ".html").write_pdf(tmp_path + filename + ".pdf")
    pdfkit.from_file(
        tmp_path + object_type + ".html",
        tmp_path + filename + ".pdf",
        options=pdf_options,
        configuration=pdf_config,
    )
    response = FileResponse(tmp_path + filename + ".pdf")
    response.headers["Content-Disposition"] = f"attachment; filename={filename}.pdf"
    return response
