import uuid

from dogpile.cache.region import make_region
from pyramid.view import view_config

from emporium import helpers


def uuid_key_mangler(key):
    """Receive cache keys as long concatenated strings;
    distill them into an md5 hash.
    """
    return uuid.uuid4().hex


regions = {
    "redis-short": make_region(key_mangler=uuid_key_mangler).configure(
        "dogpile.cache.redis",
        expiration_time=6000,
        arguments={
            "url": "redis://127.0.0.1",
            "port": 6379,
            "db": 0,
            "redis_expiration_time": 60 * 10 * 2,
            "distributed_lock": True,
        },
    ),
    "redis-long": make_region(key_mangler=uuid_key_mangler).configure(
        "dogpile.cache.redis",
        expiration_time=864000,
        arguments={
            "url": "redis://127.0.0.1",
            "port": 6379,
            "db": 0,
            "redis_expiration_time": 60**2 * 4,
            "distributed_lock": True,
        },
    ),
    "redis-eternal": make_region(key_mangler=uuid_key_mangler).configure(
        "dogpile.cache.redis",
        arguments={
            "url": "redis://127.0.0.1",
            "port": 6379,
            "db": 0,
            "distributed_lock": True,
        },
    ),
}


@view_config(route_name="clear_cache", renderer="json", permission="edit", xhr="True")
def clear_cache(request):
    """
    Clear cache view
    """
    helpers.CacheHelpers.flush_cache()
