from datetime import datetime
import os
import shutil

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from slugify import slugify


from emporium import data_containers, forms, helpers, operations, services, models


@view_config(
    route_name="band_action",
    match_param="action=create",
    renderer="band/band_edit.html",
    permission="edit",
)
def add_band(request):
    options = helpers.SettingHelpers.default_options(request)
    form = forms.BandCreateForm(request.POST)
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    if request.method == "POST" and form.validate():
        band = models.Band()
        form.populate_obj(band)
        band.date_added = datetime.now()
        band.date_modified = band.date_added
        band.date_available = band.date_added
        band.seo_slug = slugify(band.name)
        band.name = operations.ProductOperations.strip_value(form.name.data)
        band.social_links = operations.BandOperations.update_social_links(
            request, settings
        )
        request.dbsession.add(band)
        request.dbsession.flush()
        request.dbsession.add(
            models.BandDescription(
                description=form.description.data,
                language_id=form.language_id.data,
                band_id=band.band_id,
                meta_description=form.meta_description.data,
                tag=form.tag.data,
                u_h1=form.u_h1.data,
                u_title=form.u_title.data,
            )
        )
        # request.dbsession.flush()
        try:
            for item in ("images", "thumbnails", "base", "source"):
                os.mkdir(helpers.CommonHelpers.asset_path(f"{item}/{band.seo_slug}"))
        except OSError:
            print("Can't create folder for image upload")
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="band_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        settings=settings,
        options=options,
    )


@view_config(
    route_name="band_action",
    match_param="action=edit",
    renderer="band/band_edit.html",
    permission="edit",
)
def edit_band(request):
    band_id = request.params.get("band_id", -1)
    action = str(request.matchdict.get("action"))
    url_next = request.params.get("next", request.referer)
    options = helpers.SettingHelpers.default_options(request)
    options.categories_list = []
    band = services.BandService.band_filter_id(request, band_id)
    old_name = slugify(band.name)
    band_description = services.BandService.band_description_filter_band_language(
        request, band_id, language_id=2
    )
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    form = forms.BandUpdateForm(request.POST, band)
    if request.method == "POST" and form.validate():
        band.name = operations.ProductOperations.strip_value(form.name.data)
        band_description.description = form.description.data
        band_description.meta_description = form.meta_description.data
        band_description.tag = form.tag.data
        band_description.u_title = form.u_title.data
        band_description.u_h1 = form.u_h1.data
        band_description.language_id = form.language_id.data
        band_name = slugify(form.name.data)
        band.social_links = operations.BandOperations.update_social_links(
            request, settings
        )
        if old_name != band_name:
            pass
            # for item in products:
            #     item.image = item.image.replace(old_name, band_name) or ""
            #     # product_to_image = ProductService.prod_to_img(request, item.product_id) #ERRROR
            #     # for htem in product_to_image:
            #     #     htem.image = htem.image.replace(old_name, band_name) or ""
            # try:
            #     for item in images_types:
            #         os.rename(
            #             str(helpers.CommonHelpers.asset_path(item)) + "/" + old_name,
            #             str(helpers.CommonHelpers.asset_path(item)) + "/" + band_name,
            #         )
            # except OSError:
            #     return "Cannot rename image folder"
        # band.date_added = form.date_added.data or datetime.now().date()
        band.date_available = form.date_available.data or datetime.now().date()
        band.date_modified = datetime.now().date()
        band.code = form.code.data
        band.band_type = form.band_type.data
        return HTTPFound(location="band_list")
    return dict(
        action=request.matchdict.get("action"),
        form=form,
        band=band,
        band_description=band_description,
        options=options,
        settings=settings,
    )


@view_config(
    route_name="band_list",
    renderer="band/band_list.html",
    permission="edit",
)
def band_list(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    page = request.params.get("page", 1)
    sort_method = request.params.get("sort", "name_asc")
    items_per_page = request.params.get("number_bands", 72)
    bands = services.BandService.band_paginator(
        request, page, items_per_page, sort_method
    )
    bands_set = {item.band_id for item in bands}
    band_active = set()
    band_inactive = set()
    orphans = set()
    if "find_orphan" in request.params:
        page = request.params.get("page", 1)
        products = services.ProductService.all(request)
        for item in products:
            if item.status == 1:
                band_active.add(item.band_id)
            elif not item.status:
                band_inactive.add(item.band_id)
        for item in bands_set:
            if item not in band_active:
                band_inactive.add(item)
        orphans = set(band_inactive).difference(band_active)
    return dict(bands=bands, orphans=orphans, settings=settings)


@view_config(
    route_name="band_delete",
    renderer="json",
    permission="edit",
    xhr="True",
)
def band_delete(request):
    band_id = request.POST.get("band_id")
    band = services.BandService.band_filter_id(request, band_id)
    purge = str(request.POST.get("purge_all", None))
    if band and purge == "True":
        operations.ProductOperations.delete_all_products_dispatch_filter_band_id(
            request, band_id
        )
        services.BandService.delete_band_description_filter_band_id(request, band_id)
        band_name = slugify(band.name)
        try:
            shutil.rmtree(helpers.CommonHelpers.asset_path("images") + band_name)
            shutil.rmtree(helpers.CommonHelpers.asset_path("thumbnails") + band_name)
        except FileNotFoundError:
            print("Remove dir error")
        services.BandService.delete_band_filter_band_id(request, band_id)
    else:
        print("Error")
    helpers.CacheHelpers.flush_cache()
