from uuid import uuid4

from pyramid.view import view_config
from slugify import slugify

from emporium import helpers, operations, models


@view_config(route_name="file_upload", renderer="json", permission="edit", xhr="True")
def file_upload(request):
    manufacturer_seo_slug = request.params.get("manufacturer_seo_slug")
    if (
        request.params.get("band_seo_slug")
        and request.params.get("band_seo_slug") != "undefined"
    ):
        manufacturer_seo_slug += "/" + request.params.get("band_seo_slug")
    multiple_files = request.params.getall("file_content")
    product_name = request.params.get("title")
    parent_id = request.params.get("parent_id")
    parent_name = request.params.get("parent_name")
    file_count = request.params.get("file_count", 0)
    upload_type = request.params.get("upload_type")
    file_type = request.params.get("file_type")
    path_prefix = f"{manufacturer_seo_slug}/{slugify(product_name)}/"
    path_list = []
    upload_status = 400
    files = []
    product_files = []
    if multiple_files:
        # Make sure that the destination folders exist
        operations.CommonOperations.generate_file_upload_folders(
            path_prefix, upload_type
        )
        for start, file in enumerate(
            multiple_files, len(multiple_files) + 1 + int(file_count)
        ):
            file_uuid = str(uuid4())[:21]
            (
                file,
                file_ext,
                original_file_name,
            ) = helpers.FileHelpers.get_file_data_with_extension(file)
            file_name = file_uuid + file_ext
            upload_status = helpers.FileHelpers.write_file_to_disk(
                file,
                file_ext,
                f"{path_prefix}{file_name}",
                (f"{upload_type}_" if upload_type == "secured" else "") + "uploads",
            )
            path_list.append(file_name)
            # Default file type is a document
            file_type_id = 1
            file_ext = file_ext.replace(".", "")
            if file_ext in ("mp3", "ogg", "flac"):
                file_type_id = 2
            elif file_ext in ("jpeg", "jpg", "png", "web"):
                file_type_id = 4
            # elif file_ext in ("pdf", "doc", "docx", "odt", "xsl", "xlsx"):
            #     file_type_id = 1
            # 3 is for a video
            files.append(
                models.File(
                    file_type_id=file_type_id,
                    file_permission_id=1,  # By default the file is protected
                    file_path=file_name,
                    original_file_name=original_file_name,
                )
            )
        request.dbsession.add_all(files)
        request.dbsession.flush()
        # Add product_to_file object but without existing product
        if parent_name == "product":
            product_files.extend(
                models.ProductToFile(
                    file_id=file.file_id,
                    product_id=parent_id,
                    sort_order=1,
                )
                for file in files
            )
            request.dbsession.add_all(product_files)
            request.dbsession.flush()
    return (
        path_list,
        {p2f.product_file_id: f.file_path for f, p2f in zip(files, product_files)},
        {
            p2f.product_file_id: (
                f.file_path,
                f.file_type_id,
            )
            for f, p2f in zip(files, product_files)
        },
    )
