from datetime import datetime, timedelta
import hashlib

from pyramid.request import RequestLocalCache
from pyramid.view import view_config
import requests

from ..helpers import SettingHelpers
from ..services import SettingsService


@view_config(route_name="api_saturn_login")
def api_saturn_login(request):
    pass


class SaturnAuthentication:
    def __init__(self):
        self.identity_cache = RequestLocalCache(self.load_token)

    def load_token(self, request):
        session = request.session
        token = self.validate_token(request, session)
        if token is not None:
            return token

    def session_token(self, request):
        return self.identity_cache.get_or_create(request)

    def generate_token(self, request, session):
        settings = SettingHelpers.shop_settings(SettingsService.all(request))
        api_key = settings["saturn_api_key"]
        client_id = settings["saturn_client_id"]
        api_url = settings["saturn_api_url"] + "/token"
        timestamp = (datetime.utcnow()).strftime("%Y-%m-%d %H:%M:%S")
        api_hash = hashlib.md5(
            (api_key + timestamp + client_id).encode("utf-8")
        ).hexdigest()
        login_request = requests.post(
            api_url,
            {
                "Hash": api_hash,
                "ClientId": client_id,
                "Timestamp": timestamp,
            },
        )
        if login_request.status_code == 200:
            session["saturn_token"] = {
                "Authorization": "Bearer " + login_request.json()["AccessToken"]
            }
            session["saturn_expires"] = datetime.utcnow() + timedelta(
                minutes=8
            )  # 10 min for login timeout
            return session["saturn_token"]
        return login_request.status_code

    def validate_token(self, request, session):
        current_time = datetime.utcnow()
        if (
            session
            and session.get("saturn_token")
            and session.get("saturn_expires")
            and current_time < session.get("saturn_expires")
        ):
            return session["saturn_token"]
        else:
            return self.generate_token(request, session)

    # def forget(self, request):
    #     return self.authtkt.forget(request, **kw)
