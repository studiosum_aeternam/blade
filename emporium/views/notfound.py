from pyramid.httpexceptions import HTTPMovedPermanently
from pyramid.view import notfound_view_config


@notfound_view_config(renderer="emporium:templates/404.html")
def notfound_view(request):
    request.response.status = 301
    return HTTPMovedPermanently(location=request.route_url("s_home"))
