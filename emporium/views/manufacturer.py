from datetime import datetime
import os
import shutil

from decimal import Decimal
from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from slugify import slugify

from ..data_containers import AdminOptions, CategoryContainer
from ..forms import ManufacturerCreateForm, ManufacturerUpdateForm
from ..helpers import CacheHelpers, CommonHelpers, SettingHelpers
from ..models import (
    Discount,
    Manufacturer,
    ManufacturerDescription,
    ManufacturerToDiscount,
    CategoryMetaTreeDescriptions,
)
from ..services import (
    CategoryService,
    DiscountService,
    ManufacturerService,
    SettingsService,
    ProductService,
)
from emporium import operations


@view_config(
    route_name="manufacturer_action",
    match_param="action=create",
    renderer="manufacturer/manufacturer_edit.html",
    permission="edit",
)
def add_manufacturer(request):
    options = SettingHelpers.default_options(request)
    form = ManufacturerCreateForm(request.POST)
    if request.method == "POST" and form.validate():
        manufacturer = Manufacturer()
        form.populate_obj(manufacturer)
        manufacturer.date_added = datetime.now()
        manufacturer.date_modified = manufacturer.date_added
        manufacturer.date_available = manufacturer.date_added
        manufacturer.name = operations.ProductOperations.strip_value(form.name.data)
        manufacturer.seo_slug = slugify(manufacturer.name)
        request.dbsession.add(manufacturer)
        request.dbsession.flush()
        request.dbsession.add(
            ManufacturerDescription(
                description=form.description.data,
                language_id=form.language_id.data,
                manufacturer_id=manufacturer.manufacturer_id,
                meta_description=form.meta_description.data,
                tag=form.tag.data,
                u_h1=form.u_h1.data,
                u_title=form.u_title.data,
            )
        )
        request.dbsession.flush()
        operations.CommonOperations.generate_image_upload_folders(manufacturer.seo_slug)
        if request.params.get("new_discount"):
            discount = Discount()
            discount.value = Decimal(
                request.params.get("new_discount").replace(",", ".")
            )
            discount.status = 1
            request.dbsession.add(discount)
            request.dbsession.flush()
            request.dbsession.add(
                ManufacturerToDiscount(
                    manufacturer_id=manufacturer.manufacturer_id,
                    discount_id=discount.discount_id,
                )
            )
            request.dbsession.flush()
        CacheHelpers.flush_cache()
        return HTTPFound(location="manufacturer_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
        options=options,
    )


@view_config(
    route_name="manufacturer_action",
    match_param="action=edit",
    renderer="manufacturer/manufacturer_edit.html",
    permission="edit",
)
def edit_manufacturer(request):
    settings = SettingHelpers.shop_settings(SettingsService.all(request))
    current_date = datetime.now()
    manufacturer_id = request.params.get("manufacturer_id", -1)
    options = AdminOptions(
        action=str(request.matchdict.get("action")),
        url_next=request.params.get("next", request.referer),
        categories_list=[],
    )
    manufacturer = ManufacturerService.manufacturer_filter_id(request, manufacturer_id)
    old_name = slugify(manufacturer.name)
    manufacturer_description = ManufacturerService.desc_by_id(
        request, manufacturer_id, language_id=2
    )
    manufacturer_discount = DiscountService.discount_subtract_value_filter_manufacturer(
        request, manufacturer_id, settings["cache"]
    )
    category_meta_tree = CategoryService.category_meta_tree_filtered(
        request, None, None, None, manufacturer_id
    )
    category_list = CategoryService.category_id_name_parent_description_meta_seo_h1_title_filter_manufacturer(
        request, manufacturer_id, False, False
    )
    categories_id_list = [item.category_id for item in category_list]
    category_list += CategoryService.category_id_name_parent_filter_manufacturer_exclude_category_list(
        request, manufacturer_id, categories_id_list, settings
    )
    for item in category_list:
        options.categories_list.append(
            CategoryContainer(
                category_id=item.category_id,
                description=item.description if len(item) > 4 else None,
                description_long=item.description_long if len(item) > 4 else None,
                meta_description=item.meta_description if len(item) > 4 else None,
                name=item.name,
                parent_id=item.parent_id,
                subcategories=[],
                u_h1=item.u_h1 if len(item) > 4 else None,
                u_title=item.u_title if len(item) > 4 else None,
            )
        )
    images_types = ("images", "thumbnails", "base")
    form = ManufacturerUpdateForm(request.POST, manufacturer)
    if request.method == "POST" and form.validate():
        for jtem in category_meta_tree:
            jtem.description = request.params.get(
                f"description_{str(jtem.category_id)}"
            )
            jtem.description_long = request.params.get(
                f"description_long_{str(jtem.category_id)}"
            )
            jtem.meta_description = request.params.get(
                f"meta_description_{str(jtem.category_id)}"
            )
            jtem.u_h1 = request.params.get(f"u_h1_{str(jtem.category_id)}")
            jtem.u_title = request.params.get("u_title_" + str(jtem.category_id))
        for item in [
            x
            for x in categories_id_list
            if x not in (y.category_id for y in category_meta_tree)
        ]:
            new_meta = CategoryMetaTreeDescriptions(
                category_id=item,
                manufacturer_id=manufacturer_id,
                language_id=2,
                description=request.params.get(f"description_{str(item)}"),
                description_long=request.params.get("description_long_" + str(item)),
                meta_description=request.params.get("meta_description_" + str(item)),
                u_title=request.params.get(f"u_title_{str(item)}"),
                u_h1=request.params.get(f"u_h1_{str(item)}"),
            )
            if any(
                x != None
                for x in [
                    new_meta.description,
                    new_meta.description_long,
                    new_meta.meta_description,
                    new_meta.u_title,
                    new_meta.u_h1,
                ]
            ):
                request.dbsession.add(new_meta)
        products = ProductService.by_manufacturer(request, manufacturer_id)
        disable_products = request.params.get("product_status")
        if disable_products == "53":
            for item in products:
                item.availability_id = 53
        manufacturer.name = operations.ProductOperations.strip_value(form.name.data)
        manufacturer.seo_slug = slugify(manufacturer.name)
        manufacturer_description.description = form.description.data
        manufacturer_description.meta_description = form.meta_description.data
        manufacturer_description.tag = form.tag.data
        manufacturer_description.u_title = form.u_title.data
        manufacturer_description.u_h1 = form.u_h1.data
        manufacturer_description.language_id = form.language_id.data
        # If name change, rename image folder
        if old_name != manufacturer.seo_slug:
            operations.CommonOperations.rename_image_upload_folders(
                old_name, manufacturer.seo_slug
            )
        #     pass
        # for item in products:
        #     item.image = item.image.replace(old_name, manufacturer_name) or ""
        #     # product_to_image = ProductService.prod_to_img(request, item.product_id) #ERRROR
        #     # for htem in product_to_image:
        #     #     htem.image = htem.image.replace(old_name, manufacturer_name) or ""
        # try:
        #     for item in images_types:
        #         os.rename(
        #             str(CommonHelpers.asset_path(item)) + "/" + old_name,
        #             str(CommonHelpers.asset_path(item)) + "/" + manufacturer_name,
        #         )
        # except OSError:
        #     return "Cannot rename image folder"
        manufacturer.date_added = form.date_added.data or datetime.now().date()
        manufacturer.date_available = form.date_available.data or datetime.now().date()
        manufacturer.date_modified = datetime.now().date()
        manufacturer.code = form.code.data
        manufacturer.manufacturer_type = form.manufacturer_type.data
        if request.params.get("change_price"):
            update_prices_after_discount_change(current_date, products, request.params)
        if request.params.get("new_discount"):
            pass
            # discount = Discount(
            #     value=Decimal(request.params.get("new_discount").replace(",", ".")),
            #     status=1,
            #     dsubtract=request.params.get("subtract", 1),
            # )
            # request.dbsession.add(discount)
            # request.dbsession.flush()
            # request.dbsession.add(
            #     ManufacturerToDiscount(
            #         manufacturer_id=manufacturer_id, discount_id=discount.discount_id
            #     )
            # )
        # discounts = [int(x) for x in request.params.getall("discount_id")]
        # update_prices(request, manufacturer_discount, products, discounts)
        # flush_cache()
        CacheHelpers.flush_cache()
        return HTTPFound(location="manufacturer_list")
    return dict(
        action=request.matchdict.get("action"),
        form=form,
        manufacturer=manufacturer,
        manufacturer_description=manufacturer_description,
        manufacturer_discount=manufacturer_discount,
        options=options,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


def update_prices_after_discount_change(current_date, products, params):
    price_modification_percent = Decimal(
        params.get("change_price").replace(",", ".") / 100
    )
    for item in products:
        item.date_modified = current_date
        modification_value = item.price * price_modification_percent
        if params.get("plus_minus") == "on":
            item.catalog_price += modification_value
        else:
            item.catalog_price -= modification_value


def update_prices(request, manufacturer_discount, products, discounts):
    if not discounts:
        return False
    for m in manufacturer_discount:
        if m.discount_id in discounts:
            d_id = str(m.discount_id)
            if request.params.get(f"discount_{d_id}"):
                m.value = Decimal(
                    request.params.get(f"discount_{d_id}").replace(",", ".")
                )
            for gtem in products:
                if not gtem.manual_discount and str(gtem.discount_id) == d_id:
                    p = Decimal(gtem.catalog_price)
                    d = Decimal(m.value / 100)
                    gtem.price = p + (p * d)


@view_config(
    route_name="manufacturer_list",
    renderer="manufacturer/manufacturer_list.html",
    permission="edit",
)
def manufacturer_list(request):
    page = request.params.get("page", 1)
    sort_method = request.params.get("sort", "name_asc")
    items_per_page = request.params.get("number_manufacturers", 72)
    manufacturers = ManufacturerService.manufacturer_paginator(
        request, page, items_per_page, sort_method
    )
    manufacturers_set = {item.manufacturer_id for item in manufacturers}
    manufacturer_active = set()
    manufacturer_inactive = set()
    orphans = set()
    if "find_orphan" in request.params:
        page = request.params.get("page", 1)
        products = ProductService.all(request)
        for item in products:
            if item.status == 1:
                manufacturer_active.add(item.manufacturer_id)
            elif not item.status:
                manufacturer_inactive.add(item.manufacturer_id)
        for item in manufacturers_set:
            if item not in manufacturer_active:
                manufacturer_inactive.add(item)
        orphans = set(manufacturer_inactive).difference(manufacturer_active)
    return dict(
        manufacturers=manufacturers,
        orphans=orphans,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


@view_config(
    route_name="manufacturer_delete",
    renderer="json",
    permission="edit",
    xhr="True",
)
def manufacturer_delete(request):
    manufacturer_id = request.POST.get("manufacturer_id")
    manufacturer = ManufacturerService.manufacturer_filter_id(request, manufacturer_id)
    purge = str(request.POST.get("purge_all", None))
    images_paths = ("source/", "thumbnails/", "base/")
    if manufacturer and purge == "True":
        operations.ProductOperations.delete_all_products_dispatch_filter_manufacturer_id(
            request, manufacturer_id
        )
        CategoryService.delete_meta_tree_description_filter_manufacturer_id(
            request, manufacturer_id
        )
        operations.CollectionOperations.delete_all_collections_dispatch_filter_manufacturer_id(
            request, manufacturer_id
        )
        ManufacturerService.delete_manufacturer_description_filter_manufacturer_id(
            request, manufacturer_id
        )
        operations.CommonOperations.delete_image_upload_folders(manufacturer.seo_slug)
        ManufacturerService.delete_manufacturer_filter_manufacturer_id(
            request, manufacturer_id
        )
    else:
        print("Error")
    CacheHelpers.flush_cache()
