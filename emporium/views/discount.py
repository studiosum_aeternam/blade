"""
Discount views - used from manufacturer editions
"""

from datetime import datetime
from decimal import Decimal

from pyramid.view import view_config

from ..forms import DiscountForm
from ..helpers import CacheHelpers, ProductHelpers, SettingHelpers
from ..models import Discount, ManufacturerToDiscount
from ..operations import ProductOperations
from ..services import DiscountService, ManufacturerService, ProductService


@view_config(
    route_name="discount_delete",
    renderer="json",
    permission="edit",
    xhr="True",
)
def discount_delete(request):
    discount_id = request.POST.get("discount_id")
    discount = DiscountService.discount_filter_id(request, discount_id)
    manufacturer_to_discount = ManufacturerService.manufacturer_discount_filter_id(
        request, discount_id
    )
    products = ProductService.by_manufacturer(
        request, manufacturer_to_discount.manufacturer_id
    )
    for item in products:
        item.discount_id = 1
        item.price = item.catalog_price
    if manufacturer_to_discount:
        request.dbsession.delete(manufacturer_to_discount)
    if discount:
        request.dbsession.delete(discount)
    CacheHelpers.flush_cache()


@view_config(
    route_name="discount_action",
    renderer="json",
    match_param="action=update",
    permission="edit",
    xhr="True",
)
@view_config(
    route_name="discount_action",
    renderer="json",
    match_param="action=create",
    permission="edit",
    xhr="True",
)
def discount_edit(request):
    options = SettingHelpers.default_options(request)
    form = DiscountForm()
    discount = False
    if options.action == "update":
        discount = DiscountService.discount_filter_id(
            request, request.POST.get("discount_id")
        )
    else:
        discount = Discount()
        manufacturer_to_discount = ManufacturerToDiscount()
    if request.method == "POST" and form.validate():
        discount.value = Decimal(request.params.get("value").replace(",", "."))
        if options.action == "create":
            discount.status = 1
            request.dbsession.add(discount)
            request.dbsession.flush()
            manufacturer_to_discount.discount_id = discount.discount_id
            manufacturer_to_discount.manufacturer_id = request.params.get(
                "manufacturer_id"
            )
            request.dbsession.add(manufacturer_to_discount)
            request.dbsession.flush()
        if options.action == "update":
            discounts = DiscountService.discount_list_id_value(request)
            current_date = datetime.now()
            product_list = ProductService.product_object_filter_discount_id(
                request, request.POST.get("discount_id")
            )
            product_list = ProductOperations.update_customer_prices(
                product_list, discounts, current_date
            )
        CacheHelpers.flush_cache()
