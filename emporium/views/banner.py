from datetime import date, datetime
import contextlib
import os

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from slugify import slugify

from emporium import forms, helpers, models, services


@view_config(
    route_name="banner_action",
    match_param="action=create",
    renderer="banner/banner_edit.html",
    permission="edit",
)
@view_config(
    route_name="banner_action",
    match_param="action=edit",
    renderer="banner/banner_edit.html",
    permission="edit",
)
def edit_banner(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    options = helpers.SettingHelpers.default_options(request)
    banner_id = request.params.get("banner_id", -1)
    banner = (
        services.BannerService.banner_filter_id(request, banner_id)
        if options.action == "edit"
        else models.Banner()
    )
    form = forms.BannerForm(request.POST)
    if request.method == "POST" and form.validate():
        banner.creator_id = request.authenticated_userid
        banner.date_added = datetime.now()
        banner.date_end = (
            date(*map(int, request.params.get("date_end").split("-")))
            if request.params.get("date_end")
            else None
        )
        banner.date_start = (
            date(*map(int, request.params.get("date_start").split("-")))
            if request.params.get("date_start")
            else None
        )
        banner.date_modified = banner.date_added
        banner.order = form.order.data
        banner.page = form.page.data
        banner.status = form.status.data
        banner.caption_secondary = form.caption_secondary.data
        banner.url_secondary = form.url_secondary.data
        banner.caption = form.caption.data
        banner.url = form.url.data
        banner.image = form.banner_image_url.data
        if options.action == "create":
            request.dbsession.add(banner)
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="banner_list")
    return dict(
        form=form,
        action=options.action,
        banner=(banner if options.action == "edit" else False),
        settings=settings,
        options=options,
    )


@view_config(route_name="banner_delete", renderer="json", permission="edit", xhr="True")
def banner_delete(request):
    banner_id = request.POST.get("banner_id")
    banner = services.BannerService.banner_filter_id(request, banner_id)
    if banner_id:
        with contextlib.suppress(FileNotFoundError):
            if banner.image:
                os.remove(helpers.CommonHelpers.asset_path("banners") + banner.image)
        request.dbsession.delete(banner)
        helpers.CacheHelpers.flush_cache()


@view_config(
    route_name="banner_list",
    renderer="banner/banner_list.html",
    permission="edit",
)
def banner_list(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    banners = services.BannerService.all(request)
    banner_list = helpers.BannerHelpers.banners_format(banners)
    return dict(banner_list=banner_list, settings=settings)
