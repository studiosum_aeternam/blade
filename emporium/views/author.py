from datetime import date, datetime
import contextlib
import os

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from slugify import slugify

from emporium import data_containers, forms, helpers, models, operations, services


@view_config(
    route_name="author_action",
    match_param="action=create",
    renderer="author/author_edit.html",
    permission="edit",
)
@view_config(
    route_name="author_action",
    match_param="action=edit",
    renderer="author/author_edit.html",
    permission="edit",
)
def edit_author(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    options = helpers.SettingHelpers.default_options(request)
    author_id = request.params.get("author_id", -1)
    author = (
        services.AuthorService.author_full_filter_by_author_id(request, author_id)
        if options.action == "edit"
        else models.Author()
    )
    form = forms.EditAuthorForm(request.POST)
    if request.method == "POST" and form.validate():
        author.date_available = (
            date(*map(int, request.params.get("date_available").split("-")))
            if request.params.get("date_available")
            else None
        )
        author.date_modified = datetime.now()
        author.first_name = operations.ProductOperations.strip_value(
            form.first_name.data
        )
        author.last_name = operations.ProductOperations.strip_value(form.last_name.data)
        author.callsign = operations.ProductOperations.strip_value(form.callsign.data)
        author.status = form.status.data
        if options.action == "create":
            author.date_added = datetime.now()
            request.dbsession.add(author)
        helpers.CacheHelpers.flush_cache()
        return HTTPFound(location="author_list")
    return dict(
        form=form,
        action=options.action,
        author=(author if options.action == "edit" else False),
        settings=settings,
        options=options,
    )


@view_config(
    route_name="author_delete",
    renderer="json",
    permission="edit",
    xhr="True",
)
def author_delete(request):
    author_id = request.POST.get("author_id")
    author = services.AuthorService.author_full_filter_by_author_id(request, author_id)
    purge = str(request.POST.get("purge_all", None))
    if author and purge == "True":
        with contextlib.suppress(FileNotFoundError):
            if author.image:
                os.remove(helpers.CommonHelpers.asset_path("authors") + author.image)
        request.dbsession.delete(author)
        helpers.CacheHelpers.flush_cache()


@view_config(
    route_name="author_list",
    renderer="author/author_list.html",
    permission="edit",
)
def author_list(request):
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    filter_params = helpers.CommonHelpers.initialize_object_filter_params(request)
    filter_params = helpers.CommonHelpers.term_search(filter_params)
    authors = services.AuthorService.authors_filter_dynamic(
        request,
        filter_params.page,
        filter_params.searchstring,
    )
    return dict(authors=authors, filter_params=filter_params, settings=settings)


@view_config(route_name="author_search", renderer="json")
def author_search(request):
    # data_containers.FilterOptions()
    filter_params = data_containers.FilterOptions(term=request.params.get("term", ""))
    helpers.CommonHelpers.term_search(filter_params)
    try:
        authors = services.AuthorService.author_term_search(
            request, filter_params.searchstring
        )
        return [
            {
                "author_id": aut.author_id,
                "first_name": aut.first_name or "",
                "last_name": aut.last_name or "",
                "callsign": aut.callsign or "",
            }
            for aut in authors
        ]
    except ValueError:
        return dict(query=term)
