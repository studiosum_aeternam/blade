from pyramid.view import view_config


@view_config(route_name="api_saturn_basket", xhr=True, request_method="DELETE")
def api_saturn_basket(request):
    pass


@view_config(route_name="api_saturn_basket", xhr=True, request_method="GET")
def api_saturn_basket(request):
    pass


@view_config(route_name="api_saturn_basket", xhr=True, request_method="PATCH")
def api_saturn_basket(request):
    pass


@view_config(route_name="api_saturn_basket", xhr=True, request_method="POST")
def api_saturn_basket(request):
    pass


@view_config(route_name="api_saturn_basket_action", xhr=True, request_method="GET")
def api_saturn_basket_action(request):
    pass


@view_config(route_name="api_saturn_basket_action", xhr=True, request_method="PUT")
def api_saturn_basket_action(request):
    pass


@view_config(route_name="api_saturn_basket_action", xhr=True, request_method="POST")
def api_saturn_basket_action(request):
    pass
