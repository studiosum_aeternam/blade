from datetime import datetime

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from emporium import data_containers, helpers, models, operations, services, views


@view_config(route_name="s_compare", renderer="compare/s_compare.html")
def compare_view(request):
    product_listing, selling_options = [], []
    compare_products = services.CompareService.compare_content(request)
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    data_options_filtered = None
    if not compare_products:
        data_options_filtered = (
            operations.ProductOperations.return_product_related_data(
                request, settings, [], data_options.tax_dict
            )
        )
        selling_options = operations.ProductOperations.formatted_featured_products_v2(
            request, settings, data_options, data_options_filtered, 4
        )
    else:
        product_list = services.ProductService.products_base_price_filter_product_list(
            request, compare_products.products
        )
        product_items = [item.product_id for item in product_list]
        data_options_filtered = (
            operations.ProductOperations.return_product_related_data(
                request,
                settings,
                tuple(item.product_id for item in product_list),
                data_options.tax_dict,
            )
        )
        products = helpers.ProductHelpers.product_format_cart_dict(
            data_options,
            data_options_filtered,
            product_list,
            settings,
        )
        # (
        #     special_filtered,
        #     special_filtered_dict,
        # ) = helpers.ProductHelpers.featured_products_format_dict(
        #     services.ProductToSpecialService.special_rows_filter_product_list_date(
        #         request, product_items, price_promotions=True
        #     ),
        #     data_options.tax_dict,
        # )
        attributes_collections = services.ProductService.product_list_condensed(
            request, product_items
        )
        product_listing = {
            "attributes": [],
            "availability": [],
            "calculated_price": [],
            "collections": [],
            "ean": [],
            "manufacturer": [],
            "minimum": [],
            "other": [],
            "pieces": [],
            "price": [],
            "product": [],
            "product_full_info": [],
            "unit": [],
        }
        for item, ktem in zip(products.values(), attributes_collections):
            special_price = False
            if data_options_filtered.special_filtered_dict.get(
                item.product_id
            ) and data_options_filtered.special_filtered_dict[item.product_id].get(
                "price"
            ):
                special_price = data_options_filtered.special_filtered_dict[
                    item.product_id
                ]["price"]
            availability = [
                x
                for x in data_options.availability_dict.keys()
                if x == item.availability_id
            ]
            product_listing["product_full_info"].append(
                max(
                    tuple(
                        element
                        for element in products.values()
                        if element.product_id == item.product_id
                    ),
                    default=None,
                )
            )
            product_listing["calculated_price"].append(item.calculated_price)
            product_listing["product"].append(
                [
                    item.product_id,
                    item.slug,
                    helpers.CommonHelpers.capword(item.name),
                ]
            )
            product_listing["ean"].append(item.ean)
            product_listing["price"].append(
                [
                    item.price,
                    special_price,
                    item.square_meter,
                ]
            )
            product_listing["unit"].append([str(item.unit), item.square_meter])
            product_listing["minimum"].append(
                [str(int(float(item.minimum))), item.square_meter]
            )
            product_listing["pieces"].append(str(item.pieces or ""))
            # product_listing["other"].append()
            product_listing["availability"].append(
                [
                    v
                    for k, v in data_options.availability_dict.items()
                    if k == item.availability_id
                ][0]
                if availability
                else ""
            )
            product_listing["manufacturer"].append(
                [ktem.manufacturer_id, ktem.name, item.product_id]
            )
            product_listing["collections"].append(ktem.collections)
            attributes = ktem.attributes + availability
            product_listing["attributes"].append(attributes)
        # Attribute list is passed twice to generate filter url-s
        attribute_list = (
            services.AttributeService.attribute_group_attribute_filter_products_ids(
                request,
                {item.availability_id for item in products.values()},
                {item.product_id for item in products.values()},
                settings,
            )
        )
        attribute_ids = [x.attribute_id for x in attribute_list]
        data_options.attribute_dict = helpers.AttributeHelpers.attributes_filter_format(
            services.AttributeService.attribute_groups_filter_attribute_list_active(
                request, attribute_ids, settings
            ),
            attribute_list,
            [],
            [],
        )
        compare_collection_list = (
            services.CollectionService.collection_id_name_filter_product_list(
                request, product_items, settings
            )
        )
        # Collection list is passed twice to generate filter url-s
        data_options.collection_dict = (
            helpers.CollectionHelpers.collections_filter_format(
                compare_collection_list, compare_collection_list, []
            )
        )
    return dict(
        data_options=data_options,
        data_options_filtered=data_options_filtered,
        filter_params=data_containers.FilterOptions(),
        misc_options=data_containers.MiscOptions(search_url="s_search"),
        product_listing=product_listing,
        settings=settings,
        selling_options=selling_options,
    )


@view_config(route_name="s_compare_edit", renderer="string", request_method="POST")
def compare_edit(request):
    now = datetime.now()
    current_compare = services.CompareService.compare_content(request)
    if "update_compare" in request.params:
        remove_items = [int(r) for r in request.params.getall("remove_id")]
        new_items = [int(n) for n in request.params.getall("product_id")]
        if current_compare:
            old_items = [
                int(x) for x in current_compare.products if x not in remove_items
            ]
            current_compare.products = [
                *old_items,
                *[int(n) for n in new_items if n not in old_items],
            ]
            current_compare.date_accessed = now
        else:
            current_compare = models.Compare()
            current_compare.date_creation = now
            current_compare.date_accessed = now
            current_compare.products = new_items
            session = request.session
            if session.get("uid"):
                current_compare.customer_id = session.get("uid")
            request.dbsession.add(current_compare)
            request.dbsession.flush()
            if not session.get("uid"):
                request.session["compare_id"] = current_compare.compare_id
        if current_compare.products:
            request.session["compare_contents"] = current_compare.products
        else:
            request.dbsession.delete(current_compare)
            session_variables = ("compare_contents", "compare_id")
            for _ in session_variables:
                if request.session.get(_):
                    del request.session[_]
