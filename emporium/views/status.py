from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from ..forms import StatusGroupCreateForm, StatusGroupUpdateForm
from ..models import (
    Status,
    StatusDescription,
    StatusGroup,
    StatusGroupDescription,
)
from ..helpers import CacheHelpers, SettingHelpers
from ..services import StatusService, SettingsService


@view_config(
    route_name="status_list",
    renderer="status/status_list.html",
    permission="edit",
)
def status_list(request):
    status_list = StatusService.get_paginator(request)
    return dict(
        status_list=status_list,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


@view_config(
    route_name="status_action",
    match_param="action=create",
    renderer="status/status_edit.html",
    permission="edit",
)
def add_status(request):
    form = StatusGroupCreateForm(request.POST)
    if request.method == "POST" and form.validate():
        dbsession = request.dbsession
        status_group = StatusGroup()
        form.populate_obj(status_group)
        dbsession.add(status_group)
        dbsession.flush()
        status_group_description = StatusGroupDescription()
        status_group_description.status_group_id = status_group.status_group_id
        dbsession.add(status_group_description)
        dbsession.flush()
        if form.status_values.data:
            for _ in form.status_values.data.split(","):
                status = Status()
                status.status_group_id = status_group.status_group_id
                status.status = 1
                dbsession.add(status)
                dbsession.flush()
                status_description = StatusDescription()
                status_description.status_id = status.status_id
                status_description.language_id = 2
                status_description.name = _
                dbsession.add(status_description)
        CacheHelpers.flush_cache()
        return HTTPFound(location="status_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


@view_config(
    route_name="status_action",
    match_param="action=edit",
    renderer="status/status_edit.html",
    permission="edit",
)
def edit_status(request):
    status_group_id = request.params.get("status_group_id", -1)
    status_groups = StatusService.status_group_id(request, status_group_id)
    statuses = []
    statuses_ids = []
    form = StatusGroupUpdateForm(request.POST, status_groups)
    if request.method == "POST" and form.validate():
        if form.status_values.data:
            statuses_ids = [int(_) for _ in form.status_values.data.split(",")]  # WWBBD
        CacheHelpers.flush_cache()
        return HTTPFound(location="status_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        statuses=statuses,
        statuses_ids=statuses_ids,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
        status_groups=status_groups,
    )


@view_config(
    route_name="status_single_action",
    match_param="action=create",
    renderer="status/status_edit_single.html",
    permission="edit",
)
def status_single_add(request):
    statuses = StatusService.all_active(request)
    form = StatusGroupCreateForm(request.POST)
    status_groups = None
    if request.method == "POST" and form.validate():
        status_group = StatusGroup()
        form.populate_obj(status_group)
        request.dbsession.add(status_group)
        request.dbsession.flush()
        if form.status_values.data:
            for _ in form.status_values.data.split(","):
                status = Status()
                status.status_group_id = status_group.status_group_id
                status.status = 1
                request.dbsession.add(status)
        CacheHelpers.flush_cache()
        return HTTPFound(location="status_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        statuses=statuses,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
        status_groups=status_groups,
    )


@view_config(
    route_name="status_single_action",
    match_param="action=edit",
    renderer="status/status_edit_single.html",
    permission="edit",
)
def status_single_edit(request):
    status_id = request.params.get("status_id", -1)
    status = StatusService.status_filter_status_id(request, status_id)
    statuses = StatusService.all_active(request)
    form = StatusGroupUpdateForm(request.POST, status_id)
    if request.method == "POST" and form.validate():
        CacheHelpers.flush_cache()
        return HTTPFound(location="status_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        status=status,
        statuses=statuses,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


@view_config(route_name="status_delete", renderer="json", permission="edit", xhr="True")
def status_delete(request):
    status_id = request.POST.get("status_id")
    if status_description := StatusService.status_description_filter_status_id(
        request, status_id
    ):
        request.dbsession.delete(status_description)
    if status := StatusService.status_filter_status_id(request, status_id):
        request.dbsession.delete(status)
    CacheHelpers.flush_cache()
