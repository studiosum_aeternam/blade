"""
Views for order - all stages and admin/customer views
"""
from datetime import datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from pathlib import Path
from uuid import uuid4


from hashids import Hashids
from pyramid.httpexceptions import HTTPFound
from pyramid.security import remember
from pyramid.view import view_config


from emporium import (
    data_containers,
    forms,
    helpers,
    models,
    operations,
    services,
    views,
)

_query = []  # VSCode reports undefined bug(?)


@view_config(
    route_name="order_list",
    renderer="order/order_list.html",
    permission="edit",
)
def order_list(request):
    """
    Admin view of paginated orders
    """
    settings = helpers.SettingHelpers.admin_settings(
        services.SettingsService.admin_settings(request)
    )
    page = request.params.get("page", 1)
    number_orders = int(request.params.get("number_orders", 32))
    order_id = request.params.get("order_id")
    uuid = request.params.get("uuid")
    name = request.params.get("name")
    telephone = request.params.get("telephone")
    summary_from = request.params.get("summary_from")
    summary_to = request.params.get("summary_to")
    sort_method = request.params.get("sort", "mod_desc")
    status = request.params.get("status")
    filter_params = {
        "order_id": order_id,
        "uuid": uuid,
        "name": name,
        "number_orders": number_orders,
        "sort_method": sort_method,
        "status": status,
        "summary_from": summary_from,
        "summary_to": summary_to,
        "telephone": telephone,
    }
    order_list = services.OrderService.list_orders(
        request,
        name,
        number_orders,
        order_id,
        page,
        sort_method,
        status,
        summary_from,
        summary_to,
        telephone,
        uuid,
    )
    return dict(
        filter_params=filter_params,
        order_list=order_list,
        status_list=services.StatusService.all_active(request),
        transport_methods=services.TransportService.transport_list_filter_active(
            request, settings
        ),
        payment_methods=services.PaymentService.payment_filter_active(
            request, settings
        ),
        settings=settings,
    )


@view_config(
    route_name="order",
    renderer="order/order_view.html",
    permission="edit",
)
def order_view(request):
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    order_id = request.matchdict["id"]
    order = services.OrderService.order_filter_id(request, order_id)
    order_history = services.OrderService.order_history_filter_id(request, order_id)
    eans = [item["ean"] for item in order.products_json if item.get("ean")]
    data_options = views.CommonOptions.data_options_order(
        request, order.transport, settings
    )
    product_eans = {
        item["product_id"]: item for item in order.products_json if item.get("ean")
    }
    product_missing_eans = [
        item["product_id"] for item in order.products_json if not item.get("ean")
    ]
    if missing_ean_query := services.ProductService.product_id_ean_filter_product_list(
        request, product_missing_eans, settings
    ):
        eans += [item.ean for item in missing_ean_query]  # Temporary solution
        product_eans |= {item["product_id"]: item for item in missing_ean_query}
    for slot in data_options.transport_dict.values():
        transport_method = slot
    inpost_order_id = None
    operations.OrderOperations.check_external_data_sources(
        request, order, transport_method.module_name, settings
    )
    if request.method == "POST" and request.params.get("status"):
        order.status = request.params.get("status")
        new_order_history = models.OrderHistory(
            date=datetime.now(),
            status_id=request.params.get("status"),
            order_id=order.order_id,
            additional_info=request.params.get("additional_info", ""),
        )
        request.dbsession.add(new_order_history)
        request.dbsession.flush()
        if request.params.get("mail_new_status") == "1":
            operations.OrderOperations.send_update_order_mail(
                request, order, new_order_history, settings
            )
        return HTTPFound(location=request.route_url("order", id=order_id))
    products_json = []
    return dict(
        data_options=data_options,
        order=order,
        order_history=order_history,
        status_list=services.StatusService.all_active(request),
        transport_method=transport_method,
        payment_methods=services.PaymentService.payment_filter_active(
            request, settings
        ),
        settings=helpers.SettingHelpers.admin_settings(
            services.SettingsService.admin_settings(request)
        ),
        products_json=products_json,
    )


@view_config(
    route_name="order_print",
    renderer="order/order_print.html",
    permission="edit",
)
def order_print(request):
    """
    Admin order print - for hard backup, admins have additional info about the order
    """
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    today = datetime.now().strftime("%Y-%m-%d")
    order_id = request.matchdict["id"]
    order = services.OrderService.order_filter_id(request, order_id)
    data_options = views.CommonOptions.data_options_order(
        request, order.transport, settings
    )
    for slot in data_options.transport_dict.values():
        transport_method = slot
    return dict(
        data_options=data_options,
        order=order,
        status_list=services.StatusService.all_active(request),
        transport_method=transport_method,
        payment_methods=services.PaymentService.payment_filter_active(
            request, settings
        ),
        settings=settings,
    )


@view_config(
    route_name="s_order_print",
    renderer="order/s_order_print.html",
)
def s_order_print(request):
    """
    Client order print - without internal info
    """
    today = datetime.now().strftime("%Y-%m-%d")
    order_id = request.matchdict["id"]
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    order = services.OrderService.order_filter_id(request, order_id)
    data_options = views.CommonOptions.admin_data_options_limited(
        request, settings, today
    )
    return dict(
        data_options=data_options,
        now=datetime.now(),
        order=order,
        payment_methods=services.PaymentService.payment_filter_active(
            request, settings
        ),
        settings=settings,
        status_list=services.StatusService.all_active(request),
        transport_methods=services.TransportService.transport_list_filter_active(
            request, settings
        ),
    )


@view_config(
    route_name="s_order_mail",
    renderer="order/s_order_mail.html",
)
def s_order_mail(request):
    """
    View which sends successful order to the customer
    """
    order_id = request.matchdict["id"]
    order = services.OrderService.order_filter_id(request, order_id)
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    return dict(
        data_options=data_options,
        now=datetime.now(),
        order=order,
        payment_methods=services.PaymentService.payment_filter_active(
            request, settings
        ),
        settings=settings,
        status_list=services.StatusService.all_active(request),
        transport_methods=services.TransportService.transport_list_filter_active(
            request, settings
        ),
    )


@view_config(route_name="s_order", renderer="order/s_order.html")
def s_order(request):
    """
    Basic order view (gather all data from the client)
    """
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    data_options = views.CommonOptions.data_options_cart(request, settings)
    misc_options = data_containers.MiscOptions(search_url="s_search")
    response = request.response
    headers = response.headers
    next_location = request.route_url("s_order_summary")
    cart = operations.CartOperations.cart_filter_cart_or_customer(request)
    if not cart:
        return HTTPFound(location=request.route_url("s_home"))
    products_json = {
        int(k): v
        for k, v in (
            helpers.CartHelpers.return_dict_from_cart_products(
                cart.products_json or cart.products
            )
        ).items()
    }
    session = request.session
    product_list = services.ProductService.products_base_price_filter_available(
        request,
        products_json.keys(),
        False,
    )
    product_items = [item.product_id for item in product_list]
    related_products = (
        services.ProductService.product_to_product_id_filter_product_list(
            request,
            [item.product_id for item in product_list if item.product_type == 1],
        )
    )
    data_options_filtered = operations.ProductOperations.return_cart_related_data(
        request,
        settings,
        (
            [item.product_id for item in related_products if related_products]
            + list(products_json.keys())
        ),
        data_options.tax_dict,
    )
    cart_contents = helpers.CartHelpers.cart_contents_get_current_product_values(
        products_json,
        helpers.ProductHelpers.product_format_cart_dict(
            data_options,
            data_options_filtered,
            product_list,
            settings,
        ),
    )
    cart_parameters = (
        operations.CartOperations.calculate_cart_parameters(
            request,
            cart_contents,
            data_options,
            data_options_filtered,
        )
        if cart_contents
        else {}
    )
    data_options = helpers.CartHelpers.return_possible_transport_and_payment_methods(
        request,
        cart_parameters,
        cart_contents,
        data_options,
        data_options_filtered,
    )
    # Transport and payment fees are influenced by the products that are currently in
    # cart (or rather are in the process of ordering)
    # cart_parameters = operations.OrderOperations.calculate_transport_and_payment_fees(
    #     cart_parameters, data_options, order
    # )
    customer = None
    form = forms.OrderForm(request.POST, the_request=request, data_options=data_options)
    oid = session.get("oid", None)
    order = services.OrderService.order_filter_id(request, oid) if oid else None
    first_address = None
    if uid := session.get("uid", None):
        customer = services.CustomerService.by_id(request, uid)
        # addresses = services.AddressService.by_customer(request, uid)
        first_address = services.AddressService.by_customer_first(request, uid)
        # orders = services.OrderService.orders_filter_finalized_customer_id(request, uid)
    form_data = operations.OrderOperations.update_customer_data(
        request, form, order, customer, first_address
    )
    error_level = None
    if session.get("login_error"):
        error_level = "login_error"
    elif session.get("login_error") == "Blocked":
        error_level = "hard_login_error"
    if request.method == "POST" and form.validate():
        order_type = request.params["order_type"]
        if order_type == "register_buy":
            customer = models.Customer()
            form.populate_obj(customer)
            customer.customer_type = 0
            customer.psswd_hsh = form.psswd_hsh.data
            customer.registration_date = datetime.now()
            request.dbsession.add(customer)
            request.dbsession.flush()
            # headers = remember(request, customer.id)
            session["uid"] = customer.id
            session["order_type"] = "register_buy"
            operations.CartOperations.cart_merge(request)
            request.dbsession.add(
                models.Address(
                    city=form.city.data,
                    company_name=form.company_name.data,
                    company_nip=form.company_nip.data,
                    country=form.country.data,
                    customer_id=session.get("uid"),
                    defa=True,
                    postcode=operations.ProductOperations.strip_value(
                        form.postcode.data
                    ),
                    street=form.street.data,
                )
            )
            request.dbsession.flush()
            # Remove no longer used elements
            if cid := session.get("cid"):
                del cid
            cookies = ["delivery_details", "client", "payment", "transport"]
            for item in cookies:
                response.delete_cookie(item)
        if not oid:
            order = models.Order()
        order.customer_id = session.get("uid")
        order.client_json = {
            "city": form.city.data,
            "company_name": form.company_name.data,
            "company_nip": form.company_nip.data,
            "country": form.country.data,
            "first_name": form.first_name.data,
            "last_name": form.last_name.data,
            "mail": form.mail.data,
            "postcode": operations.ProductOperations.strip_value(form.postcode.data),
            "street": form.street.data,
            "telephone": form.telephone.data,
        }
        order.delivery_json = {
            "city": form.delivery_city.data or form.city.data,
            "company_name": form.delivery_company_name.data or form.company_name.data,
            "company_nip": form.delivery_company_nip.data or form.company_nip.data,
            "country": form.country.data
            if request.params["delivery_address"] == "cloned"
            else form.delivery_country.data,
            "first_name": form.delivery_first_name.data or form.first_name.data,
            "last_name": form.delivery_last_name.data or form.last_name.data,
            "postcode": form.delivery_postcode.data
            or operations.ProductOperations.strip_value(form.postcode.data),
            "street": form.delivery_street.data or form.street.data,
            "telephone": form.delivery_telephone.data or form.telephone.data,
        }
        order.date_accessed = datetime.now()
        order.invoice = form.invoice.data
        order.invoice_type = form.invoice_type.data
        order.payment = form.payment_method.data
        order.status = 1
        order.transport = form.delivery_method.data
        operations.TransportOperations.custom_order_transport_variables(
            request, form, order, data_options.transport_dict
        )
        order.order_type = "logged_buy" if order_type == "register_buy" else order_type
        session["order_type"] = order.order_type
        order.uuid = str(uuid4())[:11]
        if not oid:
            order.date_creation = order.date_accessed
            request.dbsession.add(order)
            request.dbsession.flush()
        session["oid"] = order.order_id
        if (
            request.cookies.get("delivery_details") == "manual"
            and order_type == "register_buy"
        ):
            request.dbsession.add(
                models.Address(
                    city=form.delivery_city.data,
                    country=form.delivery_country.data,
                    customer_id=session.get("uid"),
                    defa=False,
                    postcode=form.delivery_postcode.data,
                    street=form.delivery_street.data,
                )
            )
            request.dbsession.flush()
            # When new account was created customer is treated as a logged user
        if order_type == "register_buy":
            headers = remember(request, session.get("uid"), tokens=["customer"])
        return HTTPFound(location=request.route_url("s_order_summary"), headers=headers)
    return dict(
        cart_parameters=cart_parameters,
        customer=customer,
        data_options=data_options,
        error_level=error_level,
        filter_params=data_containers.FilterOptions(),
        form=form,
        form_data=form_data,
        misc_options=misc_options,
        next_location=next_location,
        order=order,
        session=session,
        settings=settings,
        meta_options=data_containers.MetaOptions(
            rel_link=request.route_url("s_search")
        ),
    )


@view_config(
    route_name="s_order_summary",
    renderer="order/s_order_summary.html",
)
def order_summary(request):
    """
    View order summary - view required by the EU law.
    """
    # Remove information describing problems with external payment gate
    if request.session.get("external_gate_failure"):
        del request.session["external_gate_failure"]
    oid = request.session.get("oid")
    cart = operations.CartOperations.cart_filter_cart_or_customer(request)
    order = services.OrderService.order_filter_id(request, oid)
    if not oid or not cart or not cart.cart_id:
        return HTTPFound(location=request.route_url("s_home"))
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    data_options = views.CommonOptions.data_options_cart(request, settings)
    misc_options = data_containers.MiscOptions(search_url="s_search")
    next_location = request.route_url("s_order_summary")
    products_json = {
        int(k): v
        for k, v in (
            helpers.CartHelpers.return_dict_from_cart_products(cart.products_json)
        ).items()
    }
    product_list = services.ProductService.products_base_price_filter_available(
        request,
        products_json.keys(),
        False,
    )
    related_products = (
        services.ProductService.product_to_product_id_filter_product_list(
            request,
            [item.product_id for item in product_list if item.product_type == 1],
        )
    )
    data_options_filtered = operations.ProductOperations.return_cart_related_data(
        request,
        settings,
        [item.product_id for item in related_products if related_products]
        + [x for x in products_json.keys()],
        data_options.tax_dict,
    )
    cart_contents = helpers.CartHelpers.cart_contents_get_current_product_values(
        products_json,
        helpers.ProductHelpers.product_format_cart_dict(
            data_options,
            data_options_filtered,
            product_list,
            settings,
        ),
    )
    cart_parameters = (
        operations.CartOperations.calculate_cart_parameters(
            request,
            cart_contents,
            data_options,
            data_options_filtered,
        )
        if cart_contents
        else {}
    )
    data_options = helpers.CartHelpers.return_possible_transport_and_payment_methods(
        request,
        cart_parameters,
        cart_contents,
        data_options,
        data_options_filtered,
    )
    # Transport and payment fees are influenced by the products that are currently in
    # cart (or rather are in the process of ordering)
    cart_parameters = operations.OrderOperations.calculate_transport_and_payment_fees(
        cart_parameters, data_options, order
    )
    order.params_json = {
        "final_cost": str(
            helpers.CommonHelpers.price_precision(Decimal(cart_parameters["summary"]))
        ),
        "transport_unit": cart_parameters["selected_slot"]["physical"].get(
            "transport_unit", ""
        ),
        "payment": cart_parameters["payment"]["payment_name"],
        "payment_description": cart_parameters["payment"]["payment_description"],
        "payment_excerpt": cart_parameters["payment"]["payment_excerpt"],
        "payment_fee": cart_parameters["payment"]["payment_fee"],
        "payment_type": cart_parameters["payment"]["payment_type"],
        "tax": str(helpers.CommonHelpers.price_precision(cart_parameters["tax"])),
        "tax_dict": {k: str(v) for k, v in cart_parameters.get("tax_dict").items()},
        "transport": cart_parameters["selected_slot"]["physical"].get(
            "transport_name", ""
        ),
        "transport_desc": cart_parameters["selected_slot"]["physical"].get(
            "transport_description", ""
        ),
        "transport_excerpt": cart_parameters["selected_slot"]["physical"].get(
            "transport_excerpt", ""
        ),
        "transport_slot_excerpt": cart_parameters["selected_slot"]["physical"].get(
            "transport_slot_excerpt", ""
        ),
        "transport_module_name": cart_parameters["selected_slot"]["physical"].get(
            "transport_module_name", ""
        ),
        "transport_slot_name": cart_parameters["selected_slot"]["physical"].get(
            "transport_slot_name", ""
        ),
        "transport_fee": str(
            cart_parameters["selected_slot"]["physical"].get("transport_fee", "")
        ),
        "transport_digital": cart_parameters["selected_slot"]["digital"].get(
            "transport_name"
        ),
        "weight": str(cart_parameters.get("weight", "")),
    }
    if request.method == "POST":
        order.additional_info = request.params.get("additional_info")
        order.date_ordered = datetime.now()
        # Convert decimal fields to str, required by JSON
        order.products_json = [
            {
                "product_id": item.product_id,
                "calculated_price": str(item.calculated_price),
                "catalog_price": str(item.catalog_price),
                "ean": item.ean,
                "manufacturer_id": item.manufacturer_id,
                "model": str(item.model),
                "name": str(item.name),
                "price": str(item.price),
                "price_net": str(item.price_net),
                "product_packages": item.product_packages,
                "product_quantity": str(item.product_quantity),
                "product_single_set": item.product_single_set,
                "product_type": item.product_type,
                "real_price": str(item.price),
                "square_meter": item.square_meter,
                "tax_value": str(item.tax_value),
                "unit": str(item.unit),
                "unit_tax": str(item.unit_tax),
                "value": str(item.value),
            }
            for item in cart_contents.values()
        ]
        request.session["order_confirmed"] = order.order_id
        request.dbsession.delete(
            services.CartService.cart_filter_cart_id(request, cart.cart_id)
        )
        order.status = 4 if order.params_json["payment_type"] == 1 else 2
        order_history = models.OrderHistory(
            date=order.date_ordered,
            mail_sent=True,
            order_id=order.order_id,
            status_id=order.status,
        )
        request.dbsession.add(order_history)
        if order.params_json["payment_type"] == 1:
            route_url = "s_order_processing"
        else:
            route_url = "s_order_success"
        return HTTPFound(location=request.route_url(route_url))
    return dict(
        cart_contents=cart_contents,
        cart_parameters=cart_parameters,
        data_options_filtered=data_options_filtered,
        misc_options=misc_options,
        data_options=data_options,
        order=order,
        settings=settings,
        filter_params=data_containers.FilterOptions(),
        meta_options=data_containers.MetaOptions(
            rel_link=request.route_url("s_search")
        ),
    )


@view_config(route_name="s_order_processing")
def order_processing(request):
    if request.route_url("s_order_summary") not in request.referrer:
        return HTTPFound(location=request.route_url("s_home"))
    oid = request.session.get("oid")
    order = services.OrderService.order_filter_id(request, oid)
    payment_method = services.PaymentService.payment_by_id(
        request, order.payment
    )  # payment_id
    payment_methods_gates = {"p24": "p24_request", "paypal": "paypal_request"}
    return HTTPFound(
        location=request.route_url(
            payment_methods_gates.get(payment_method.module_name)
        )
    )


@view_config(
    route_name="s_order_success",
    renderer="order/s_order_success.html",
)
def order_success(request):
    """
    Success order page - del all session variables, when anything is wrong redirect to the homepage
    """
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    settings["delivery_date"] = str(
        (datetime.now().date() + relativedelta(days=5)).strftime("%Y-%m-%d")
    )
    misc_options = data_containers.MiscOptions(search_url="s_search")
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    response = request.response
    cookies = ["cart_contents"]
    for _ in cookies:
        response.delete_cookie(_)
    session = request.session
    ceneo_shop_product_ids = ""
    if not session.get("order_confirmed"):
        session.invalidate
        return HTTPFound(location=request.route_url("s_home"))
    else:
        order = services.OrderService.order_filter_id(
            request, session["order_confirmed"]
        )
        operations.StorageOperations.update_product_status_and_storage_info(
            request, order, settings
        )
        operations.OrderOperations.send_order_confirmation_email(
            request, order, settings
        )
        operations.OrderOperations.remove_order_session_variables(request)
    for item in order.products_json:
        item[
            "brand"
        ] = (
            []
        )  # [htem.name for htem in options['manufacturer_list'] if htem.manufacturer_id == item['manufacturer_id']][0]
        product = ("#" + str(item["product_id"])) * int(
            float(
                item["product_packages"]
                if item["square_meter"]
                else item["product_quantity"]
            )
        )
        ceneo_shop_product_ids += product
    return dict(
        ceneo_shop_product_ids=ceneo_shop_product_ids,
        data_options=data_options,
        filter_params=filter_params,
        misc_options=misc_options,
        meta_options=data_containers.MetaOptions(
            rel_link=request.route_url("s_search")
        ),
        order=order,
        settings=settings,
    )


@view_config(
    route_name="s_order_cancelled",
    renderer="order/s_order_cancelled.html",
)
def order_cancelled(request):
    """
    Success order page - del all session variables, when anything is wrong redirect to the homepage
    """
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    misc_options = data_containers.MiscOptions(search_url="s_search")
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    response = request.response
    session = request.session
    session_values = [
        "attempt",
        "cart_contents",
        "cid",
        "delivery",
        "login_error",
        "oid",
        "order_confirmed",
        "order_type",
        "payment",
        "privacy_policy",
        "transport",
    ]
    for _ in session_values:
        if session.get(_):
            del session[_]
    session.invalidate
    cookies = ["delivery_details", "invoice_type", "cart_contents"]
    for _ in cookies:
        response.delete_cookie(_)
    return dict(
        data_options=data_options,
        filter_params=filter_params,
        misc_options=misc_options,
        meta_options=data_containers.MetaOptions(
            rel_link=request.route_url("s_search")
        ),
        settings=settings,
    )


@view_config(
    route_name="s_order_view",
    renderer="order/s_order_view.html",
)
def s_order_view(request):
    """
    Single order preview for client - with weird url coding/decoding
    """
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    misc_options = data_containers.MiscOptions(search_url="s_search")
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    session = request.session
    hash_order_id = request.params.get("z")
    hashids = Hashids(salt="PerfeCT_Man")
    test_order_id = hashids.decode(hash_order_id)
    if not isinstance(test_order_id, tuple) or len(test_order_id) != 1:
        return HTTPFound(location=request.route_url("s_customer"))
    order_id = list(hashids.decode(hash_order_id))[0]
    order = services.OrderService.order_filter_id(request, order_id)
    orders = services.OrderService.orders_filter_finalized_customer_id(
        request, session.get("uid")
    )
    order_list = [item.order_id for item in orders]
    if not order_id or order_id not in order_list:
        return HTTPFound(location=request.route_url("s_customer"))
    order_history = services.OrderService.order_history_filter_id(request, order_id)
    return dict(
        misc_options=misc_options,
        meta_options=data_containers.MetaOptions(
            rel_link=request.route_url("s_search")
        ),
        data_options=data_options,
        filter_params=filter_params,
        hash_order_id=hash_order_id,
        hashids=hashids,
        order=order,
        order_history=order_history,
        payment_methods=services.PaymentService.payment_filter_active(
            request, settings
        ),
        status_list=services.StatusService.all_active(request),
        transport_methods=services.TransportService.transport_list_filter_active(
            request, settings
        ),
    )


@view_config(
    route_name="s_update_status_mail",
    renderer="mail/s_update_status_mail.html",
)
def update_status_mail(request):
    """
    View used for updating order status - possible duplicate of order mail view
    """
    status_id = request.params.get("status_id")
    uuid = request.params.get("uuid")
    additional_info = request.params.get("additional_info")
    url = request.params.get("url", "")
    status = services.StatusService.status_status_description_filter_status_id(
        request, status_id
    )
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    return dict(
        additional_info=additional_info,
        status=status,
        url=url,
        uuid=uuid,
        settings=settings,
    )


@view_config(
    route_name="s_order_change_data",
    renderer="order/s_order_select.html",
)
def order_form(request):
    """
    Simple view to verify type of order (register/one time/logged).
    It's slow but saves a lot of JS hassle.
    """

    cart = operations.CartOperations.cart_filter_cart_or_customer(request)
    if not cart:
        return HTTPFound(location=request.route_url("s_home"))
    next_location = request.route_url("s_order")
    session = request.session
    # If user is logged and cart is preseng - go to the next order stage
    if session.get("order_type") and session["order_type"] == "logged_buy":
        return HTTPFound(location=next_location)
    misc_options = data_containers.MiscOptions(search_url="s_search")
    settings = helpers.SettingHelpers.shop_settings(
        services.SettingsService.all(request)
    )
    filter_params = data_containers.FilterOptions()
    data_options = views.CommonOptions.data_options(request, filter_params, settings)
    response = request.response
    headers = response.headers
    error_level = None
    if session.get("login_error"):
        error_level = "login_error"
    elif session.get("login_error") == "Blocked":
        error_level = "hard_login_error"
    if request.params.get("order_type"):
        order_type = request.params.get("order_type")
        if order_type and order_type in ("quick", "register"):
            session["order_type"] = (
                "register_buy" if order_type == "register" else "quick_buy"
            )
        else:
            if order := services.OrderService.order_filter_id(
                request, session.get("oid")
            ):
                request.dbsession.delete(order)
            session_values = (
                "delivery",
                "payment",
                "transport",
                "order_type",
                "privacy_policy",
                "oid",
            )
            for _ in session_values:
                if session.get(_):
                    del session[_]
            cookies = ("delivery_details", "invoice_type")
            for _ in cookies:
                request.response.delete_cookie(_)
        return HTTPFound(location=next_location)
    return dict(
        data_options=data_options,
        error_level=error_level,
        misc_options=misc_options,
        next_location=next_location,
        filter_params=filter_params,
        meta_options=data_containers.MetaOptions(
            rel_link=request.route_url("s_search")
        ),
        session=session,
        settings=settings,
    )
