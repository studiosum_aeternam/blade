from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from ..forms import OptionGroupCreateForm, OptionGroupUpdateForm
from ..helpers import (
    CacheHelpers,
    SettingHelpers,
)
from ..models import (
    Option,
    OptionDescription,
    OptionGroup,
    OptionGroupDescription,
)
from ..services import (
    OptionService,
    SettingsService,
)


@view_config(
    route_name="option_list",
    renderer="option/option_list.html",
    permission="edit",
)
def option_list(request):
    option_list = OptionService.get_paginator(request)
    return dict(
        option_list=option_list,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


@view_config(
    route_name="option_action",
    match_param="action=create",
    renderer="option/option_edit.html",
    permission="edit",
)
def add_option(request):
    form = OptionGroupCreateForm(request.POST)
    if request.method == "POST" and form.validate():
        option_group = OptionGroup()
        form.populate_obj(option_group)
        request.dbsession.add(option_group)
        request.dbsession.flush()
        option_group_description = OptionGroupDescription()
        option_group_description.option_group_id = option_group.option_group_id
        request.dbsession.add(option_group_description)
        request.dbsession.flush()
        if form.option_values.data:
            for e in form.option_values.data.split(","):
                option = Option()
                option.option_group_id = option_group.option_group_id
                option.status = 1
                request.dbsession.add(option)
                request.dbsession.flush()
                option_description = OptionDescription()
                option_description.option_id = option.option_id
                option_description.language_id = 2
                option_description.name = e
                request.dbsession.add(option_description)
        CacheHelpers.flush_cache()
        return HTTPFound(location="option_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


@view_config(
    route_name="option_action",
    match_param="action=edit",
    renderer="option/option_edit.html",
    permission="edit",
)
def edit_option(request):
    option_group_id = request.params.get("option_group_id", -1)
    option_group = OptionService.option_group_id(request, option_group_id)
    # options = []
    # options_ids = []
    form = OptionGroupUpdateForm(request.POST, option_group)
    # if request.method == "POST" and form.validate():
    #     if form.option_values.data:
    #         new_option_elements = [int(e) for e in form.option_values.data.split(",")]
    #         to_add = set(new_option_elements).difference(products_ids)
    #         for e in list(to_add):
    #             product_to_option = ProductToCollection()
    #             product_to_option.option_id = option_id
    #             product_to_option.product_id = e
    #             request.dbsession.add(product_to_option)
    #         to_delete = set(products_ids).difference(new_option_elements)
    #         if to_delete:
    #             options = ProductService.product_options_filter_product_id(
    #                 request, option_id, list(to_delete)
    #             )
    #             for item in list(options):
    #                 request.dbsession.delete(item)
    #     CacheHelpers.flush_cache()
    #     return HTTPFound(location="option_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        # option=option,
        # option_description=option_description,
        # pro=products,
        # pro_ids=products_ids,
        # col=options,
        # col_ids=options_ids,
        # settings=SettingHelpers.admin_settings( SettingsService.admin_settings(request)),
    )


@view_config(
    route_name="option_single_action",
    match_param="action=create",
    renderer="option/option_edit_single.html",
    permission="edit",
)
def option_single_add(request):
    options = OptionService.all_active(request)
    form = OptionGroupCreateForm(request.POST)
    if request.method == "POST" and form.validate():
        option_group = OptionGroup()
        form.populate_obj(option_group)
        request.dbsession.add(option_group)
        request.dbsession.flush()
        if form.option_values.data:
            for e in form.option_values.data.split(","):
                request.dbsession.add(
                    Option(option_group_id=option_group.option_group_id, status=1)
                )
        CacheHelpers.flush_cache()
        return HTTPFound(location="option_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        options=options,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


@view_config(
    route_name="option_single_action",
    match_param="action=edit",
    renderer="option/option_edit_single.html",
    permission="edit",
)
def option_single_edit(request):
    option_id = request.params.get("option_id", -1)
    options = OptionService.all_active(request, option_id)
    form = OptionGroupUpdateForm(request.POST, option_id)
    if request.method == "POST" and form.validate():
        CacheHelpers.flush_cache()
        return HTTPFound(location="option_list")
    return dict(
        form=form,
        action=request.matchdict.get("action"),
        options=options,
        settings=SettingHelpers.admin_settings(SettingsService.admin_settings(request)),
    )


@view_config(route_name="option_delete", renderer="json", permission="edit", xhr="True")
def option_delete(request):
    option_id = request.POST.get("option_id")
    option = OptionService.col_by_id(request, option_id)
    option_description = (
        OptionService.collection_description_filter_collection_id_language_id(
            request, option_id, language_id=2
        )
    )
    product_to_option = OptionService.prod_col_by_id(request, option_id)
    if option_description:
        request.dbsession.delete(option_description)
    if product_to_option:
        for item in list(product_to_option):
            request.dbsession.delete(item)
    if not option_description and option:
        request.dbsession.delete(option)
    CacheHelpers.flush_cache()
