from pyramid.view import view_config


@view_config(route_name="api_saturn_form", xhr=True, request_method="GET")
def api_saturn_form(request):
    pass


@view_config(route_name="api_saturn_form_action", xhr=True, request_method="GET")
def api_saturn_form_action(request):
    pass


@view_config(route_name="api_saturn_form_action", xhr=True, request_method="POST")
def api_saturn_form_action(request):
    pass


@view_config(route_name="api_saturn_form_action", xhr=True, request_method="PATCH")
def api_saturn_form_action(request):
    pass
