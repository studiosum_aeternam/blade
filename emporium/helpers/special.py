"""
Special helper functions
"""
import random

from emporium import data_containers


class SpecialHelpers(object):
    @classmethod
    def special_products_v2(cls, data_special_dict, special_products, number_elements):
        fetured_product_groups = {
            v["name"]: [
                item
                for item in special_products
                if item.product_id in v["featured_product_ids"]
            ]
            for k, v in data_special_dict.items()
        }
        for v in fetured_product_groups.values():
            if v:
                random.shuffle(v)
        return {k: v[:number_elements] for k, v in fetured_product_groups.items() if v}

    @classmethod
    def specials_format(cls, special_list):
        return {
            item.special_id: {
                "name": item.name,
                "price_changing": item.price_changing,
                "name_displayed": item.name_displayed,
                "featured_product_ids": item.featured_product_ids,
            }
            for item in special_list
        }

    @classmethod
    def specials_filter_format(cls, special_list: list, query_items: list) -> dict:
        return {
            item.special_id: data_containers.SpecialContainer(
                special_id=item.special_id,
                name=item.name,
                name_displayed=item.name_displayed,
                url={},
            )
            for item in special_list
        }
