from emporium import data_containers


class ManufacturerHelpers(object):
    @classmethod
    def manufacturers_filter_format(
        cls, manufacturer: str, manufacturer_list: list, query_items: list
    ) -> dict:
        return {
            item.manufacturer_id: data_containers.ManufacturerContainer(
                manufacturer_id=item.manufacturer_id,
                manufacturer_type=item.manufacturer_type,
                name=item.name,
                seo_slug=item.seo_slug,
                status=item.status,
                url=(
                    [
                        htem
                        for htem in query_items
                        if htem[0] != "manufacturer"
                        or int(htem[1]) != item.manufacturer_id
                    ]
                    if item.manufacturer_id in manufacturer
                    else None
                ),
            )
            for item in manufacturer_list
        }
