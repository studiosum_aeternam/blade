"""
Settings helper functions
"""
from emporium import data_containers


class SettingHelpers(object):
    @classmethod
    def shop_settings(cls, request, xsettings: list) -> dict:
        # Temporary solution - settings need to be rewritten
        settings = {item.key: item.value for item in xsettings}
        for item in xsettings:
            if item.key == "image_settings":
                settings["image_sizes"] = item.complex_values["image_sizes"]
                settings["image_file_extensions"] = {
                    item[0]: item[1] for item in item.complex_values["image_extensions"]
                }
                settings["image_settings"] = {
                    "image_sizes": settings["image_sizes"],
                    "image_file_extensions": settings["image_file_extensions"],
                }
            elif item.setting_type == 2:
                settings[item.key] = item.complex_values
        settings["currency"] = " zł"
        settings["iso_currency"] = "PLN"
        settings["iso_country"] = "PL"
        settings["language"] = "pl"
        request.cache = settings["main_settings"]["cache"] == 1
        # settings["today"] = today

        return settings

    @classmethod
    def single_complex_setting(cls, xsettings: list) -> dict:
        return xsettings.complex_values

    @classmethod
    def admin_settings(cls, request, xsettings: list) -> dict:
        settings = {item.key: item.complex_values or item.value for item in xsettings}
        settings["currency"] = " zł"
        settings["iso_currency"] = "PLN"
        settings["iso_country"] = "PL"
        settings["language"] = "pl"
        if settings.get("top_menu"):
            settings["top_menu"] = dict(
                sorted(settings["top_menu"].items(), key=lambda h: h[1])
            )
        for item in xsettings:
            if item.key == "image_settings":
                #  TODO this is bonkers - to be repaired, there is mix in new and old type of loading settings
                settings["image_sizes"] = item.complex_values["image_sizes"]
                settings["image_file_extensions"] = {
                    item[0]: item[1] for item in item.complex_values["image_extensions"]
                }
                settings["image_settings"] = {
                    "image_sizes": settings["image_sizes"],
                    "image_file_extensions": settings["image_file_extensions"],
                }
            elif item.setting_type == 2:
                settings[item.key] = item.complex_values
        request.cache = settings["main_settings"]["cache"] == 1
        return settings

    @classmethod
    def default_options(cls, request):
        action = str(request.matchdict.get("action"))
        url_next = request.params.get("next", request.referer)
        return data_containers.AdminOptions(action=action, url_next=url_next)
