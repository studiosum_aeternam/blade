import os
import shutil
import uuid

from emporium import helpers


class FileHelpers:
    @classmethod
    def write_file_to_disk(cls, file, file_ext, destination, file_type):
        path = os.path.join("/tmp", f"{uuid.uuid4()}.{file_ext}")
        try:
            with open(f"{path}~", "wb") as output_file:
                shutil.copyfileobj(file, output_file)
            os.rename(f"{path}~", path)
            shutil.copyfile(
                path, helpers.CommonHelpers.asset_path(file_type, "tmp") + destination
            )
            return 200
        except IOError:
            return 411

    @classmethod
    def get_file_data_with_extension(cls, current_file):
        file_ext = None
        if current_file != b"":
            file = current_file.file
            file.seek(0)
            file_name, file_ext = os.path.splitext(current_file.filename)
            return file, file_ext, file_name

    @classmethod
    def move_file_to_different_location(
        cls, file_type, file_name, path_source, path_destination
    ):
        file_path = request.params.get("manufacturer_seo_slug")
        if (
            request.params.get("band_seo_slug")
            and request.params.get("band_seo_slug") != "undefined"
        ):
            manufacturer_seo_slug += "/" + request.params.get("band_seo_slug")

        try:
            if file_type == "image":
                shutil.move(path_source, path_destination)
            return 200
        except IOError:
            return 411
