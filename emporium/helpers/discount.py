"""
Discount helper functions - used from manufacturer editions
"""


class DiscountHelpers:
    @classmethod
    def create_discount_dict_key_id(cls, discounts):
        return {item.discount_id: item for item in discounts}

    @classmethod
    def discounts_format(cls, discounts):
        return [
            {
                "discount_id": item.discount_id,
                "manufacturer_id": item.manufacturer_id,
                "value": item.value,
            }
            for item in discounts
        ]
