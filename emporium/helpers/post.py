"""
Post helper functions
"""


class PostHelpers(object):
    @classmethod
    def add_posts_to_urls_container(cls, home, url_listing, posts):
        for item in posts:
            url_listing.append(
                {
                    "url": f"{home}blog/{item.seo_slug}-{str(item.post_id)}",
                    "date_modified": format(item.date_modified, "%Y-%m-%d"),
                }
            )
