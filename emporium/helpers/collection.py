from emporium import data_containers


class CollectionHelpers:
    @classmethod
    def collections_filter_format(
        cls, collection, collection_list, query_items, manufacturer=False
    ):
        return {
            item.collection_id: data_containers.CollectionContainer(
                collection_id=item.collection_id,
                name=item.name,
                url=[
                    htem
                    for htem in query_items
                    if htem[0] != "collections" or int(htem[1]) != item.collection_id
                ]
                if item.collection_id in collection
                else None,
                manufacturer=item.manufacturer if manufacturer else "",
            )
            for item in collection_list
        }

    @classmethod
    def collections_related_format(cls, collection_list):
        return {
            item.collection_id: data_containers.CollectionContainerWithProducts(
                collection_id=item.collection_id,
                name=item.name,
                manufacturer_id=item.manufacturer_id,
                product_list=item.products,
            )
            for item in collection_list
        }

    @classmethod
    def admin_collections_format(cls, collection_list, manufacturer_list):
        return {
            item.collection_id: data_containers.CollectionContainer(
                collection_id=item.collection_id,
                name=item.name,
                manufacturer=manufacturer_list[item.manufacturer_id].name,
            )
            for item in collection_list
        }

    @classmethod
    def add_collection_list_to_urls_container(
        cls, home, url_listing, category, collection_list
    ):
        for item in collection_list:
            url_listing.append(
                {
                    "url": f"{home}category/{category.seo_slug}-{str(category.category_id)}?collection={str(item[0])}",
                    "date_modified": "",
                }
            )
