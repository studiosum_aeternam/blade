"""
Banner helpers functions
"""
import typing
from emporium import data_containers


class AuthorHelpers(object):
    @classmethod
    def authors_format(cls, authors: typing.Dict = None) -> typing.List:
        """
        Build and return a list of authors.
        """
        author_list = []
        for item in authors:
            single_data = {
                "author_id": item.author_id,
                "first_name": item.first_name,
                "last_name": item.last_name,
                "status": item.status,
            }
            author_list.append(single_data)
        return author_list

    @classmethod
    def product_author_format(cls, product_author_list: list = None) -> typing.List:
        """
        Every authored product is a work of one or more person
        """
        authors = {}
        for item in product_author_list:
            if item.product_id not in authors:
                authors[item.product_id] = []
            if item.product_author_id:
                authors[item.product_id].append(
                    data_containers.AuthorContainer(
                        author_id=item.author_id,
                        product_author_id=item.product_author_id,
                        role=item.role,
                        first_name=item.first_name,
                        last_name=item.last_name,
                        callsign=item.callsign,
                        order=item.order,
                        visibility=item.visibility,
                    )
                )
        return authors
