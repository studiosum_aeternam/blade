"""
Transport helper functions
"""
from emporium import data_containers, helpers, operations

from webob.multidict import MultiDict


class TransportHelpers(object):
    @classmethod
    def create_transport_dict_key_id(cls, transports):
        """
        No-cache query for current data
        """
        return {item.transport_id: item for item in transports}

    @classmethod
    def transport_with_product_local_settings_format(
        cls, product_to_transport_local_settings: list
    ) -> dict:
        transport_filtered = {}
        for item in product_to_transport_local_settings:
            if not transport_filtered.get(item.product_id):
                transport_filtered[item.product_id] = {}
            transport_filtered[item.product_id][
                item.transport_slot_id
            ] = data_containers.ProductTransportContainer(
                item.name,
                item.module_name,
                item.module_settings,
                item.module_description,
                item.product_id,
                item.product_transport_id,
                item.transport_id,
                item.transport_slot_id,
                item.local_settings or {},
            )
        return transport_filtered

    @classmethod
    def generate_transport_container(cls, item):
        """
        Unified container generator for single transport method
        """
        return (
            data_containers.TransportContainer(
                # item.special_method,
                item.description,
                item.excerpt,
                item.module_name,
                item.module_settings,
                item.name,
                item.transport_id,
                item.transport_method,
                {},
            )
            if item
            else MultiDict()
        )

    @classmethod
    def generate_transport_slot_container(cls, item):
        """
        Unified container generator for single transport slot
        """
        return (
            data_containers.TransportSlotContainer(
                item.slot_applicability,
                item.slot_description,
                item.slot_excerpt,
                item.slot_settings.get("slot_manipulation_fee", "0"),
                item.slot_name,
                item.slot_order,
                item.slot_special_method,
                item.slot_settings.get("slot_unit_fee", "0"),
                item.slot_settings.get("slot_unit_name", ""),
                item.transport_id,
                item.transport_slot_id,
                item.transport_method,
                item.slot_settings,
            )
            if item
            else MultiDict()
        )

    @classmethod
    def transport_with_slots_format(
        cls, transport_list: list, transport_slots: list
    ) -> dict:
        transport_settings = {}
        for element in transport_list:
            transport_id = element.transport_id
            cls.build_dictionary_of_transport_methods(element, transport_settings)
            cls.build_dictionary_of_transport_slots(
                element, transport_settings, transport_slots
            )
        transport_settings = (
            operations.TransportOperations.remove_transport_methods_without_slots(
                transport_settings
            )
        )
        return transport_settings

    @classmethod
    def build_dictionary_of_transport_methods(cls, element, transport_settings):
        if not transport_settings.get(element.transport_id):
            transport_settings[element.transport_id] = cls.generate_transport_container(
                element
            )

    @classmethod
    def build_dictionary_of_transport_slots(
        cls, element, transport_settings, transport_slots
    ):
        for item in transport_slots:
            if item.transport_id == element.transport_id:
                transport_settings[element.transport_id].slots[
                    item.transport_slot_id
                ] = cls.generate_transport_slot_container(item)

    @classmethod
    def delete_transport_size_slots(
        cls, request, slot_for_deletion, transport_slots_old_elements
    ):
        """
        If element is in the removal list pass it to the deleter function
        """
        helpers.CommonHelpers.delete_from_database(
            request,
            [
                item
                for item in transport_slots_old_elements
                if item.transport_slot_id in slot_for_deletion
            ],
        )
