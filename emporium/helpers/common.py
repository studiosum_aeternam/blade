"""
Common (universal) methods used in different parts of the system
"""
from decimal import setcontext, Context, Decimal, DefaultContext
from lxml import html
from lxml.html.clean import Cleaner

# from pathlib import Path
from pyramid.path import AssetResolver

from time import perf_counter
import contextlib
import os
import re
import multiprocessing
import threading


from emporium import data_containers


TWOPLACES = Decimal(10) ** -2
THREEPLACES = Decimal(10) ** -3
FOURPLACES = Decimal(10) ** -4
FIVEPLACES = Decimal(10) ** -5
SIXPLACES = Decimal(10) ** -5


# def log_timer(wrapped):
#     def wrapper(context, request):
#         start = perf_counter()
#         response = wrapped(context, request)
#         duration = perf_counter() - start
#         response.headers["X-View-Time"] = "%.3f" % (duration,)
#         log.info("view took %.3f seconds", duration)
#         return response

#     return wrapper


class CommonHelpers:
    @classmethod
    def asset_path(cls, asset_name, asset_type="img", upload_type=""):
        if asset_type == "tmp":
            return AssetResolver("emporium").resolve(asset_name).abspath() + "/"
        elif asset_type == "file":
            return (
                AssetResolver("emporium")
                .resolve(
                    f"{f'{upload_type}_' if upload_type == 'secured' else ''}uploads/{asset_name}"
                )
                .abspath()
                + "/"
            )
        else:
            return (
                AssetResolver("emporium").resolve(f"images/{asset_name}").abspath()
                + "/"
            )

    @classmethod
    def delete_failure(cls, request, item):
        request.response.status_code = 400
        request.response.errors = (
            dict(error="Wrong request method")
            if item
            else dict(error="Object doesn't exist")
        )
        return request.response

    @classmethod
    def delete_image(cls, image_list, image_types):
        with contextlib.suppress(FileNotFoundError):
            for element in image_list:
                for im_type in image_types:
                    os.remove(cls.asset_path(im_type) + element)

    @classmethod
    def format_date(cls, date):
        return format(date, "%Y-%m-%d")

    @classmethod
    def price_gross_number(cls, price, tax):
        if price:
            if not isinstance(price, Decimal):
                price = Decimal(price)
            return cls.quantize_number(Decimal(price) * (1 + (tax.value / 100)))[:-2]
        else:
            return ""

    @classmethod
    def price_net(cls, price, tax_value):
        return Decimal(price) / (1 + (tax_value / 100)) if price else ""

    @classmethod
    def price_gross_replacement(cls, price, tax_value):
        return (
            cls.price_precision(Decimal(price) * (1 + (tax_value / 100)))
            if price
            else ""
        )

    @classmethod
    def price_tax(cls, price, tax):
        return cls.price_precision(Decimal(price) * (tax.value / 100)) if price else ""

    @classmethod
    def price_precision(cls, price):
        if price or price == 0:
            cls.set_price_precision()
            return cls.quantize_number(price)[:-2]
        else:
            return ""

    @classmethod
    def decimal_with_two_places(cls, number):
        if number:
            number = Decimal(str(number))
        cls.quantize_number
        return number

    @classmethod
    def quantize_number(cls, number):
        return f"{number.quantize(THREEPLACES, context=Context(prec=12)):.4f}"

    @classmethod
    def set_price_precision(cls):
        DefaultContext.prec = 6
        setcontext(DefaultContext)

    @classmethod
    def decimal_jsonb_compatible(cls, numerical_value):
        # Payment fee can be integer (fixed price) or float (percent of the amount)
        # This is simple if there was no other rounding in value but here it's weird
        return str(
            numerical_value
            if isinstance(numerical_value, int)
            else cls.price_precision(numerical_value)
        )

    @classmethod
    def capword(cls, word):
        return word.title().replace(" Cm", " cm")

    @classmethod
    def tag_cleaner(cls, text):
        clean_text = ""
        with contextlib.suppress(Exception):
            if text:
                cleaner = Cleaner(
                    javascript=True,
                    style=True,
                    allow_tags=[""],
                    remove_unknown_tags=False,
                    page_structure=False,
                )
                clean_text = cleaner.clean_html(html.fromstring(text)).text
                clean_text = re.sub(r"\s+", "  ", clean_text)
        return clean_text

    @classmethod
    def plural_pl(cls, value, sex):
        suffix = ""
        if sex == "female":
            if value == 1:
                suffix = "a"
            if 1 < value < 5:
                suffix = "i"
            if value > 4:
                suffix = "ek"
        elif sex == "male":
            if value == 1:
                suffix = "a"
            if 1 < value < 5:
                suffix = "y"
            if value > 4:
                suffix = ""
        elif sex == "other":
            if 2 < value < 5:
                suffix = "y"
            if value > 4:
                suffix = "ów"

    @classmethod
    def term_search(cls, filter_params):
        if filter_params.term:
            filter_params.term = cls.tag_cleaner(filter_params.term)
            filter_params.searchstring = f"%%%{'%%'.join(filter_params.term.split())}%%"
        return filter_params

    @classmethod
    def term_filter_format(cls, filter_params, query_items):
        if filter_params.term:
            filter_params.term = filter_params.term.replace("%%", " ").replace("%", "")
            filter_params.term_url = [htem for htem in query_items if htem[0] != "term"]
            filter_params.descripted_front_page = False
        return filter_params

    @classmethod
    def inpost_telephone_format(cls, phone):
        return re.sub(r"[^0-9]", "", phone)[-9:]

    @classmethod
    def initialize_filter_params(cls, request, availability_attributes):
        return data_containers.FilterOptions(
            attributes={
                int(x)
                for x in request.params.getall("attributes")
                if x.isdigit() and int(x) not in availability_attributes
            },
            availability={
                int(x)
                for x in request.params.getall("attributes")
                if x.isdigit() and int(x) in availability_attributes
            },
            band={int(x) for x in request.params.getall("band")},
            category_id=request.params.get("category"),
            collection={int(x) for x in request.params.getall("collection")},
            description=request.params.get("description", False),
            manufacturer={int(x) for x in request.params.getall("manufacturer")},
            page=request.params.get("page", 1),
            price_max=request.params.get("price_max", None),
            price_min=request.params.get("price_min", None),
            sort_method=request.params.get("sort", "mod_desc"),
            special_product=False,
            status=0,
            term=request.params.get("term", ""),
        )

    @classmethod
    def initialize_object_filter_params(cls, request):
        return data_containers.FilterOptions(
            page=request.params.get("page", 1),
            sort_method=request.params.get("sort", ""),
            status=request.params.get("status", None),
            term=request.params.get("term", ""),
        )

    @classmethod
    def multiprocess_function_call(cls, function, arguments_list):
        cpu = multiprocessing.cpu_count()
        start_time = perf_counter()
        for _ in range(cpu):
            list_chunk = arguments_list[
                int(_ * len(arguments_list) / cpu) : int(
                    (_ + 1) * len(arguments_list) / cpu
                )
            ]
            threading.Thread(target=function, args=(list_chunk,)).start()

    @classmethod
    def generate_delete_list(cls, old_id_list, current_id_list):
        return set(old_id_list).difference(list(current_id_list))
