from emporium import data_containers, helpers


class CategoryHelpers:
    @classmethod
    def traverse_category_tree(cls, main_tree, category_list):
        head, *tail = category_list
        for item in main_tree:
            if head.parent_id == item.category_id:
                item.subcategories.append(
                    data_containers.CategoryContainer(
                        category_id=head.category_id,
                        name=head.name,
                        parent_id=head.parent_id,
                        subcategories=[],
                        seo_slug=head.seo_slug,
                    )
                )
        if tail:
            cls.traverse_category_tree(main_tree, tail)
        else:
            return main_tree

    @classmethod
    def build_category_tree(cls, category_list):
        nodes = {item.category_id: item for item in category_list}
        categories_list = []
        for item in category_list:
            category_id = item.category_id
            parent_id = item.parent_id
            node = nodes[category_id]
            if not item.parent_id or not nodes.get(parent_id):
                categories_list.append(node)
            else:
                item.depth = nodes[parent_id].depth + 1
                nodes[parent_id].subcategories.append(node)
        return categories_list

    @classmethod
    def categories_format(cls, categories, settings):
        category_list = []
        for item in categories:
            single_data = {
                "category_id": item.category_id,
                "name": item.name,
                "image": helpers.ImageHelpers.image_resolver(
                    item.image, None, settings["image_settings"]
                ),
                "status": item.status,
            }
            category_list.append(single_data)
        return category_list

    @classmethod
    def categories_filter_format(cls, category_list):
        return [
            data_containers.CategoryContainer(
                category_id=item.category_id,
                category_type=item.category_type,
                name=item.name,
                parent_id=item.parent_id,
                placeholder=item.placeholder,
                seo_slug=item.seo_slug,
                subcategories=[],
            )
            for item in category_list
        ]

    @classmethod
    def categories_filter_format_with_main(cls, category_list):
        categories_list = []
        main_category = None
        for item in category_list:
            categories_list.append(
                data_containers.CategoryContainer(
                    category_id=item.category_id,
                    category_type=item.category_type,
                    name=item.name,
                    parent_id=item.parent_id,
                    placeholder=item.placeholder,
                    seo_slug=item.seo_slug,
                    subcategories=[],
                )
            )
            if item.category_type == 1 and not item.placeholder:
                main_category = item.category_id
        return categories_list, main_category
