"""
Tax helper functions - used from product/manufacturer editions
"""


class TaxHelpers(object):
    @classmethod
    def create_tax_dict_key_id(cls, taxes):
        """
        No-cache query for current data
        """
        return {item.tax_id: item for item in taxes}
