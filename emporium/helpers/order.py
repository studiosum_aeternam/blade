"""
Order helpers functions
"""


class OrderHelpers(object):
    @classmethod
    def remove_session_order_variables(cls, session):
        """
        Remove session variables related to the cart and order variables
        """
        for _ in ("delivery", "client", "payment", "transport"):  # Session variables
            if session.get(_):
                del session[_]

    @classmethod
    def remove_order_cookies(cls, request):
        """
        Remove cookies related to the order
        """
        for _ in ("delivery_details", "payment"):  # Cookies
            request.response.delete_cookie(_)
