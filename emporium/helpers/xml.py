"""
Special helper functions
"""
import re

from emporium import data_containers, helpers


class XMLHelpers(object):
    @classmethod
    def xmls_format(cls, xml_list):
        return {item.xml_id: {"name": item.name} for item in xml_list}

    @classmethod
    def generate_xml_custom_rules(
        cls,
        request,
        data_options,
        category_mappings,
        base_category_name,
        special_active_prices,
        zipped_product_data,
    ):
        """
        Generates XML custom rules based on the provided parameters.

        Args:
            cls: The class itself.
            request: The request object.
            data_options: The data options.
            category_mappings: The category mappings.
            base_category_name: The base category name.
            special_active_prices: The special active prices.
            zipped_product_data: The zipped products.

        Returns:
            list: A list of data containers representing the generated XML custom rules.

        Example:
            ```python
            request = ...
            zipped_product_data = ...
            data_options = ...
            category_mappings = ...
            base_category_name = ...
            special_active_prices = ...

            xml_custom_rules = ProductHelpers.generate_xml_custom_rules(
                request, data_options, category_mappings, base_category_name, special_active_prices, zipped_product_data,
            )
            print(xml_custom_rules)
            # Output: [data_containers.BaseContainer(...), data_containers.BaseContainer(...), ...]
            ```
        """
        products_with_grouped_attribute_color = (
            data_options.attribute_dict.get(8, {})
            and data_options.attribute_dict[8].attributes
        )
        products_with_grouped_attribute_pattern = (
            data_options.attribute_dict.get(7, {})
            and data_options.attribute_dict[7].attributes
        )
        products_with_grouped_attribute_room = (
            data_options.attribute_dict.get(6, {})
            and data_options.attribute_dict[6].attributes
        )
        products_with_grouped_attribute_usage = (
            data_options.attribute_dict.get(3, {})
            and data_options.attribute_dict[3].attributes
        )
        product_listing = []
        for item in zipped_product_data:
            categories = ", ".join(item[1][1]) if item[1][1][0] is not None else ""
            collections = ", ".join(item[2][1]) if item[2][1][0] is not None else ""
            product_id = item[1].product_id
            # price = [x.price for x in special_active_prices if product_id == x.product_id]
            for k, v in category_mappings.items():
                if k in categories:
                    base_category_name = v
            product_listing.append(
                data_containers.BaseContainer(
                    availability="niedostępny"
                    if item[0].availability_id == 53
                    else "w magazynie",
                    additional_img="\n\t".join(
                        (
                            f'<g:additional_image_link>{request.static_url("emporium:images/") + "base/" + img}</g:additional_image_link>'
                            for img in item[3][1]
                            if item[3][1][0] is not None
                        )
                    ),
                    base_measure=item[0].unit,
                    base_category_name=base_category_name,
                    brand=data_options.manufacturer_dict[item[0].manufacturer_id].name,
                    description=helpers.CommonHelpers.tag_cleaner(
                        item[0].description
                        and re.sub("<", " <", str(item[0].description))
                    ),
                    main_image=request.static_url("emporium:images/")
                    + "thumbnails/"
                    + data_options.manufacturer_dict[item[0].manufacturer_id].seo_slug
                    + "/"
                    + item[0].image
                    + ".webp"
                    if item[0].image
                    else "no_image.jpg",
                    minimum=item[0].minimum,
                    measurement_unit=item[0].unit,
                    name=re.sub("&", "&#38;", item[0].name.capitalize()),
                    price=helpers.CommonHelpers.price_gross_number(
                        (special_active_prices.get(product_id) or item[0].price),
                        data_options.tax_dict[item[0].tax_id],
                    ),
                    price2=helpers.CommonHelpers.price_gross_number(
                        (special_active_prices.get(product_id) or item[0].price)
                        * item[0].unit,
                        data_options.tax_dict[item[0].tax_id],
                    ),
                    link=request.route_url("s_home")
                    + item[0].seo_slug
                    + "-"
                    + str(product_id),
                    product_id=product_id,
                    square_meter=item[0].square_meter,
                    weight=round(
                        item[0].weight * item[0].unit, 2
                    ),  # Shop stores product weight per square meter
                    categories=categories,
                    product_type=re.sub(
                        "&",
                        "&#38;",
                        (
                            categories
                            + (", " if categories and collections else "")
                            + collections
                        ),
                    ),
                    color=",".join(
                        element.value
                        for element in products_with_grouped_attribute_color.values()
                        if element.attribute_id in item[4]
                    ),
                    pattern=",".join(
                        element.value
                        for element in products_with_grouped_attribute_pattern.values()
                        if element.attribute_id in item[4]
                    ),
                    usage=",".join(
                        element.value
                        for element in products_with_grouped_attribute_usage.values()
                        if element.attribute_id in item[4]
                    ),
                    room=",".join(
                        element.value
                        for element in products_with_grouped_attribute_room.values()
                        if element.attribute_id in item[4]
                    ),
                    verify_product_id=(
                        ("PRODUCT ID MISMATCH")
                        if product_id
                        != (
                            item[1].product_id
                            or item[2].product_id
                            or item[3].product_id
                        )
                        else ""
                    ),
                )
            )

        return product_listing
