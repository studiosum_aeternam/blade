"""
Storage helper functions - used for storage editions
"""
from emporium import data_containers


class StorageHelpers(object):
    @classmethod
    def storages_format(cls, storages):
        return {item.storage_id: item.name for item in storages}

    @classmethod
    def storages_filter_format(
        cls, storage: str, storage_list: list, query_items: list
    ) -> dict:
        return {
            item.storage_id: data_containers.StorageContainer(
                storage_id=item.storage_id,
                storage_type=item.storage_type,
                name=item.name,
                url=(),  # Placeholder for
            )
            for item in storage_list
        }
