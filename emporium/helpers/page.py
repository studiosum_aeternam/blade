"""
Page helper functions
"""


class PageHelpers(object):
    @classmethod
    def add_pages_to_urls_container(cls, home, url_listing, pages):
        for item in pages:
            url_listing.append(
                {
                    "url": f"{home}page/{item.slug}-{str(item.page_id)}",
                    "date_modified": "",
                }
            )
