"""
Banner helpers functions
"""
import typing


class BannerHelpers(object):
    @classmethod
    def banners_format(cls, banners: typing.List = None) -> typing.List:
        """
        Build and return a list of banners based on a passed list.
        """
        if banners is None:
            banners = []
        banner_list = []
        for item in banners:
            single_data = {
                "banner_id": item.banner_id,
                "image": item.image,
                "caption": item.caption,
                "caption_secondary": item.caption_secondary,
                "status": item.status,
            }
            banner_list.append(single_data)
        return banner_list
