"""
Tax helper functions - used from product/manufacturer editions
"""
from emporium import data_containers

from webob.multidict import MultiDict


class PaymentHelpers(object):
    @classmethod
    def generate_payment_container(cls, payment_list: list) -> dict:
        return (
            {
                item.payment_id: data_containers.PaymentContainer(
                    # item.special_method,
                    item.description,
                    # item.excerpt,
                    item.module_name,
                    item.module_settings,
                    item.name,
                    item.payment_id,
                    item.type,
                )
                for item in payment_list
            }
            if payment_list
            else MultiDict()
        )
