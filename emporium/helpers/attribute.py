import typing
from emporium import data_containers


class AttributeHelpers:
    @classmethod
    def build_base_attribute_dict(cls, attribute_group_list: typing.List) -> dict:
        return {
            item.attribute_group_id: data_containers.AttributeGroupContainer(
                attribute_display_type_id=item.attribute_display_type_id,
                attribute_group_id=item.attribute_group_id,
                attribute_name=item.attribute_name,
                attributes={},
                name=item.name,
                sort_order=item.sort_order,
                visible=False,
            )
            for item in attribute_group_list
        }

    @classmethod
    def attributes_values_and_names(cls, attribute_list: typing.List) -> dict:
        return {item.attribute_id: item.value for item in attribute_list}

    @classmethod
    def build_extended_attribute_dict(
        cls,
        attribute_group_list: typing.List,
        attribute_list: typing.Dict,
        attribute_set: typing.Set,
        query_items: typing.List,
    ):
        for attribute in attribute_list:
            if attribute.attribute_id in attribute_set:
                attribute_group_list[attribute.attribute_group_id].visible = True
            if attribute_group_list.get(attribute.attribute_group_id):
                attribute_group_list[attribute.attribute_group_id].attributes[
                    attribute.attribute_id
                ] = data_containers.AttributeContainer(
                    attribute_id=attribute.attribute_id,
                    value=attribute.value,
                    url=(
                        [
                            htem
                            for htem in query_items
                            if htem[0] != "attributes"
                            or int(htem[1]) != attribute.attribute_id
                        ]
                        if attribute.attribute_id in attribute_set
                        else None
                    ),
                )
        return {k: v for k, v in attribute_group_list.items() if v.attributes}

    @classmethod
    def build_extended_attribute_dict_for_mapping(
        cls,
        attribute_group_list: typing.List,
        attribute_list: typing.Dict,
        attribute_set: typing.Set,
        query_items: typing.List,
    ):
        for attribute in attribute_list:
            if attribute.attribute_id in attribute_set:
                attribute_group_list[attribute.attribute_group_id].visible = True
            if attribute_group_list.get(attribute.attribute_group_id):
                attribute_group_list[attribute.attribute_group_id].attributes[
                    attribute.attribute_id
                ] = data_containers.AttributeContainerMapping(
                    attribute_id=attribute.attribute_id,
                    value=attribute.value,
                    url=(
                        [
                            htem
                            for htem in query_items
                            if htem[0] != "attributes"
                            or int(htem[1]) != attribute.attribute_id
                        ]
                        if attribute.attribute_id in attribute_set
                        else None
                    ),
                    attribute_settings=attribute.attribute_settings,
                )
        return {k: v for k, v in attribute_group_list.items() if v.attributes}

    @classmethod
    def attributes_filter_format(
        cls,
        attribute_group_list: typing.List,
        attribute_list: typing.List,
        attributes: typing.Set,
        query_items: typing.List,
    ) -> typing.Dict:
        attribute_group_list = cls.build_base_attribute_dict(attribute_group_list)
        return cls.build_extended_attribute_dict(
            attribute_group_list,
            attribute_list,
            attributes,
            query_items,
        )

    @classmethod
    def attributes_filter_format_mapping(
        cls,
        attribute_group_list: typing.List = None,
        attribute_list: typing.List = None,
        attributes: typing.Set = set(),
        query_items: typing.List = None,
    ) -> typing.Dict:
        attribute_group_list = attribute_group_list or []
        attribute_list = attribute_list or []
        query_items = query_items or []
        attribute_group_list = cls.build_base_attribute_dict(attribute_group_list)
        return cls.build_extended_attribute_dict_for_mapping(
            attribute_group_list,
            attribute_list,
            attributes,
            query_items,
        )

    @classmethod
    def create_attribute_dict_id_value(
        cls, attributes: typing.Dict[int, str] = dict()
    ) -> typing.Dict:
        return {item.attribute_id: item.value for item in attributes}

    @classmethod
    def create_attribute_dict_type_example_groups(
        cls, attributes: typing.Dict[int, tuple] = dict()
    ) -> typing.Dict:
        return {
            item.attribute_group_id: (item.name, item.attribute_name)
            for item in attributes
        }
