"""
Product helper functions
"""
from datetime import datetime
from decimal import Decimal
import re

from emporium import data_containers, helpers, operations


class ProductHelpers(object):
    @classmethod
    def additional_images_format(
        cls, images, manufacturer_dict, image_settings, show_placeholder
    ) -> list:
        """
        Formats additional images for a product.

        Args:
            images (list): List of images.
            manufacturer_dict (dict): Dictionary of manufacturer information.
            image_settings (dict): Image settings.
            show_placeholder (bool): Flag indicating whether to show a placeholder image.

        Returns:
            list: List of formatted additional images.

        Example:
            ```python
            images = ["image1.jpg", "image2.jpg"]
            manufacturer_dict = {"1": {"name": "Manufacturer 1"}}
            image_settings = {"size": "medium"}
            show_placeholder = True

            additional_images = ProductHelpers.additional_images_format(
                images, manufacturer_dict, image_settings, show_placeholder
            )
            print(additional_images)
            # Output: [{'image': 'formatted_image1.jpg'}, {'image': 'formatted_image2.jpg'}]
            ```
        """
        images_list = []
        for item in images:
            single_image = dict(
                image=helpers.ImageHelpers.image_resolver(
                    item.image, manufacturer_dict, image_settings, show_placeholder
                ),
            )
            if single_image["image"] != None:
                images_list.append(single_image)
        return images_list

    @classmethod
    def create_product_dict_key_ean(cls, product_list: list) -> dict:
        """
        Creates a dictionary with EAN as the key and catalog price as the value.

        Args:
            product_list (list): List of products.

        Returns:
            dict: Dictionary with EAN as the key and catalog price as the value.

        Example:
            ```python
            product_list = [
                {"ean": "12345", "catalog_price": 10.0},
                {"ean": "67890", "catalog_price": 20.0}
            ]

            product_dict = ProductHelpers.create_product_dict_key_ean(product_list)
            print(product_dict)
            # Output: {'12345': {'catalog_price': 10.0}, '67890': {'catalog_price': 20.0}}
            ```
        """
        return {
            item.ean: {"catalog_price": item.catalog_price} for item in product_list
        }

    @classmethod
    def create_product_stock_dict_key_ean(cls, product_list: list) -> dict:
        """
        Creates a dictionary with EAN as the key and product stock information as the value.

        Args:
            product_list (list): List of products.

        Returns:
            dict: Dictionary with EAN as the key and product stock information as the value.

        Example:
            ```python
            product_list = [
                {"ean": "12345", "quantity": 10, "unit": "pcs", "product_id": 1},
                {"ean": "67890", "quantity": 20, "unit": "pcs", "product_id": 2}
            ]

            product_stock_dict = ProductHelpers.create_product_stock_dict_key_ean(product_list)
            print(product_stock_dict)
            # Output: {'12345': {'quantity': 10, 'unit': 'pcs', 'product_id': 1}, '67890': {'quantity': 20, 'unit': 'pcs', 'product_id': 2}}
            ```
        """
        return {
            item.ean: {
                "quantity": item.quantity,
                "unit": item.unit,
                "product_id": item.product_id,
            }
            for item in product_list
        }

    @classmethod
    def merge_product_dicts(cls, updated_products: dict, product_dict: dict) -> dict:
        """
        Merges two dictionaries of products.

        Args:
            updated_products (dict): Dictionary of updated products.
            product_dict (dict): Dictionary of existing products.

        Returns:
            dict: Merged dictionary of products.

        Example:
            ```python
            updated_products = {'12345': {'catalog_price': 15.0}, '67890': {'catalog_price': 25.0}}
            product_dict = {'12345': {'catalog_price': 10.0}, '67890': {'catalog_price': 20.0}}

            merged_dict = ProductHelpers.merge_product_dicts(updated_products, product_dict)
            print(merged_dict)
            # Output: {'12345': {'catalog_price': 15.0}, '67890': {'catalog_price': 25.0}}
            ```
        """
        return {
            key: value | updated_products.get(key)
            if updated_products.get(key)
            else value
            for key, value in product_dict.items()
        }

    @classmethod
    def merge_product_storage(cls, updated_products: dict, product_dict: dict) -> dict:
        """
        Merges two dictionaries of product storage information.

        Args:
            updated_products (dict): Dictionary of updated product storage information.
            product_dict (dict): Dictionary of existing product storage information.

        Returns:
            dict: Merged dictionary of product storage information.

        Example:
            ```python
            updated_products = {'12345': {'quantity': 15, 'unit': 'pcs', 'product_id': 1}, '67890': {'quantity': 25, 'unit': 'pcs', 'product_id': 2}}
            product_dict = {'12345': {'quantity': 10, 'unit': 'pcs', 'product_id': 1}, '67890': {'quantity': 20, 'unit': 'pcs', 'product_id': 2}}

            merged_dict = ProductHelpers.merge_product_storage(updated_products, product_dict)
            print(merged_dict)
            # Output: {1: {'quantity': 15, 'unit': 'pcs', 'product_id': 1}, 2: {'quantity': 25, 'unit': 'pcs', 'product_id': 2}}
            ```
        """
        my_map = {}
        for key, value in product_dict.items():
            if updated_products.get(key) and updated_products.get(key)["InStock"]:
                updated_products[key]["quantity"] = int(
                    Decimal(str(updated_products.get(key)["Qty"])) / value["unit"]
                )
                my_map[value["product_id"]] = value | updated_products[key]
        return my_map

    @classmethod
    def other_products_from_common_collections(
        cls, collection_dict, products_from_collections, product_id
    ):
        """
        Retrieves other products from common collections.

        Args:
            collection_dict: Dictionary of collections.
            products_from_collections: List of products from collections.
            product_id: ID of the current product.

        Returns:
            List of dictionaries representing other products from common collections.

        Example:
            ```python
            collection_dict = {
                "1": {"collection_id": 1, "name": "Collection 1", "product_list": [2, 3, 4]},
                "2": {"collection_id": 2, "name": "Collection 2", "product_list": [3, 4, 5]}
            }
            products_from_collections = [
                {"product_id": 1, "name": "Product 1"},
                {"product_id": 2, "name": "Product 2"},
                {"product_id": 3, "name": "Product 3"},
                {"product_id": 4, "name": "Product 4"},
                {"product_id": 5, "name": "Product 5"}
            ]
            product_id = 1

            other_products = ProductHelpers.other_products_from_common_collections(
                collection_dict, products_from_collections, product_id
            )
            print(other_products)
            # Output: [
            #     {
            #         "collection_id": 1,
            #         "name": "Collection 1",
            #         "products": [
            #             {"product_id": 2, "name": "Product 2"},
            #             {"product_id": 3, "name": "Product 3"},
            #             {"product_id": 4, "name": "Product 4"}
            #         ]
            #     },
            #     {
            #         "collection_id": 2,
            #         "name": "Collection 2",
            #         "products": [
            #             {"product_id": 3, "name": "Product 3"},
            #             {"product_id": 4, "name": "Product 4"},
            #             {"product_id": 5, "name": "Product 5"}
            #         ]
            #     }
            # ]
            ```
        """
        return [
            dict(
                collection_id=int(item.collection_id),
                name=item.name,
                products=[
                    element
                    for element in products_from_collections
                    if element.product_id != product_id
                    and element.product_id in item.product_list
                ][:4],
            )
            for item in collection_dict.values()
        ]

    @classmethod
    def product_list_filter_manual_prices(cls, product_list: list) -> list:
        """
        Filters the product list to remove manually discounted products.

        Args:
            product_list (list): List of products.

        Returns:
            list: Filtered list of products.

        Example:
            ```python
            product_list = [
                {"product_id": 1, "manual_discount": False},
                {"product_id": 2, "manual_discount": True},
                {"product_id": 3, "manual_discount": False}
            ]

            filtered_list = ProductHelpers.product_list_filter_manual_prices(product_list)
            print(filtered_list)
            # Output: [
            #     {"product_id": 1, "manual_discount": False},
            #     {"product_id": 3, "manual_discount": False}
            # ]
            ```
        """
        return [item for item in product_list if item.manual_discount == False]

    @classmethod
    def price_filter_format(cls, filter_params: list, query_items: list):
        """
        When any of the price filters is used disable front page description and enable
        sidebar filter stickyness. Switch values if needed (min < max).
        Return url without price filter(s).
        """
        filter_params.descripted_front_page = False
        filter_params.sticky = True

        filter_params.price_filter = True
        if (
            filter_params.price_min
            and filter_params.price_max
            and float(filter_params.price_min) > float(filter_params.price_max)
        ):
            filter_params.price_min, filter_params.price_max = (
                filter_params.price_max,
                filter_params.price_min,
            )
        if filter_params.price_min:
            filter_params.price_min_url = [
                htem for htem in query_items if htem[0] != "price_min"
            ]
        if filter_params.price_max:
            filter_params.price_max_url = [
                htem for htem in query_items if htem[0] != "price_max"
            ]
        return dict(filter_params=filter_params)

    @classmethod
    def product_json_format(cls, item):
        """
        Formats a product item into a JSON-like dictionary.

        Args:
            item: The product item to be formatted.

        Returns:
            dict: A dictionary containing the formatted product information.

        Example:
            ```python
            item = Product()
            item.unit = 2
            item.minimum = 5
            item.minimum_unit = 0.5
            item.model = "ABC123"
            item.name = "Product A"
            item.price = 10.99
            item.price_net = 9.99
            item.special_price = 8.99
            item.product_id = 12345
            item.weight = 1.5
            item.square_meter = True
            item.pieces = 10
            item.manufacturer_id = 54321
            item.sku = "SKU123"

            formatted_item = ProductHelpers.product_json_format(item)
            print(formatted_item)
            # Output: {
            #     "unit": 2.0,
            #     "minimum_packages": 5.0,
            #     "minimum_meters": 0.5,
            #     "minimum": 5.0,
            #     "model": "ABC123",
            #     "name": "Product A",
            #     "price": 10.99,
            #     "price_net": 9.99,
            #     "real_price": 8.99,
            #     "id": 12345,
            #     "weight": 1.5,
            #     "special_price": 8.99,
            #     "square_meter": True,
            #     "pieces": 10,
            #     "manufacturer_id": 54321,
            #     "quantity": 2.0,
            #     "sku": "SKU123"
            # }
            ```
        """
        return {
            "id": item.product_id,
            "manufacturer_id": item.manufacturer_id,
            "minimum": float(item.minimum),
            "minimum_meters": float(item.minimum_unit),
            "minimum_packages": float(item.minimum),
            "model": item.model,
            "name": item.name,
            "pieces": item.pieces or 1,
            "price": float(item.price),
            "price_net": float(item.price_net),
            "quantity": float(item.unit if item.square_meter == 1 else 1),
            "real_price": float(item.special_price or item.price),
            "sku": item.sku,
            # "special_price": float(item.special_price) if item.special_price else "", OlyaTEST
            "square_meter": item.square_meter,
            "unit": float(item.unit or 1),
            "weight": float(format(item.weight, ".2f")),
        }

    @classmethod
    def product_to_product_format(cls, products, manufacturer_dict, image_settings):
        """
        Formats a list of product items into a standardized format.

        Args:
            products (list): List of product items to be formatted.
            manufacturer_dict (dict): Dictionary mapping manufacturer IDs to manufacturer information.
            image_settings (dict): Settings for image resolution.

        Returns:
            list: A list of dictionaries containing the formatted product information.

        Example:
            ```python
            products = [
                {"product_component_id": 1, "product_id": 12345, "image": "image_url1", "model": "ABC123", "name": "Product A", "seo_slug": "product-a", "status": "active", "product_relation_type": "related", "order": 1, "relation_settings": {}, "transport_method": "shipping", "product_type": "physical"},
                {"product_component_id": 2, "product_id": 67890, "image": "image_url2", "model": "DEF456", "name": "Product B", "seo_slug": "product-b", "status": "inactive", "product_relation_type": "cross_sell", "order": 2, "relation_settings": {}, "transport_method": "pickup", "product_type": "digital"}
            ]
            manufacturer_dict = {1: {"seo_slug": "manufacturer-a"}, 2: {"seo_slug": "manufacturer-b"}}
            image_settings = {"width": 500, "height": 500}

            formatted_products = ProductHelpers.product_to_product_format(products, manufacturer_dict, image_settings)
            print(formatted_products)
            # Output: [
            #     {
            #         "product_component_id": 1,
            #         "product_id": 12345,
            #         "image": "formatted_image_url1",
            #         "model": "ABC123",
            #         "name": "Product A",
            #         "slug": "product-a",
            #         "status": "active",
            #         "product_relation_type": "related",
            #         "order": 1,
            #         "relation_settings": {},
            #         "transport_method": "shipping",
            #         "product_type": "physical"
            #     },
            #     {
            #         "product_component_id": 2,
            #         "product_id": 67890,
            #         "image": "formatted_image_url2",
            #         "model": "DEF456",
            #         "name": "Product B",
            #         "slug": "product-b",
            #         "status": "inactive",
            #         "product_relation_type": "cross_sell",
            #         "order": 2,
            #         "relation_settings": {},
            #         "transport_method": "pickup",
            #         "product_type": "digital"
            #     }
            # ]
            ```
        """

        return [
            dict(
                product_component_id=item.product_component_id,
                product_id=item.product_id,
                image=helpers.ImageHelpers.image_resolver(
                    item.image,
                    manufacturer_dict.get(item.manufacturer_id).seo_slug
                    if manufacturer_dict and manufacturer_dict.get(item.manufacturer_id)
                    else "",
                    image_settings,
                ),
                model=item.model,
                name=helpers.CommonHelpers.capword(item.name),
                slug=item.seo_slug,
                status=item.status,
                product_relation_type=item.product_relation_type,
                order=item.order,
                relation_settings=item.relation_settings,
                # physical_transport_method=item.physical_transport_method,
                # digital_transport_method=item.digital_transport_method,
                product_type=item.product_type,
            )
            for item in products
        ]

    @classmethod
    def product_to_product_combined_format(
        cls, products, data_options, data_options_filtered, image_settings
    ):
        """
        Formats a list of product items into a combined format with related products.

        Args:
            products (list): List of product items to be formatted.
            data_options (object): Data options object containing additional data.
            data_options_filtered (object): Filtered data options object.
            image_settings (dict): Settings for image resolution.

        Returns:
            dict: A dictionary containing the formatted product information with related products.

        Example:
            ```python
            products = [
                {"product_component_id": 1, "product_id": 12345, "image": "image_url1", "model": "ABC123", "name": "Product A", "seo_slug": "product-a", "status": "active", "product_relation_type": "related", "order": 1, "relation_settings": {"product_parent_id": 54321, "quantity": 2}, "transport_method": "shipping", "product_type": "physical"},
                {"product_component_id": 2, "product_id": 67890, "image": "image_url2", "model": "DEF456", "name": "Product B", "seo_slug": "product-b", "status": "inactive", "product_relation_type": "cross_sell", "order": 2, "relation_settings": {"product_parent_id": 54321, "quantity": 1}, "transport_method": "pickup", "product_type": "digital"}
            ]
            data_options = DataOptions()
            data_options.manufacturer_dict = {1: {"seo_slug": "manufacturer-a"}, 2: {"seo_slug": "manufacturer-b"}}
            data_options_filtered = DataOptionsFiltered()
            data_options_filtered.band_filtered = {12345: {"seo_slug": "band-a"}}
            image_settings = {"width": 500, "height": 500}

            formatted_products = ProductHelpers.product_to_product_combined_format(products, data_options, data_options_filtered, image_settings)
            print(formatted_products)
            # Output: {
            #     54321: [
            #         {
            #             "product_component_id": 1,
            #             "product_id": 12345,
            #             "image": "formatted_image_url1",
            #             "model": "ABC123",
            #             "name": "Product A",
            #             "slug": "product-a",
            #             "status": "active",
            #             "product_relation_type": "related",
            #             "order": 1,
            #             "relation_settings": {"product_parent_id": 54321, "quantity": 2},
            #             "transport_method": "shipping",
            #             "product_type": "physical"
            #         },
            #         {
            #             "product_component_id": 2,
            #             "product_id": 67890,
            #             "image": "formatted_image_url2",
            #             "model": "DEF456",
            #             "name": "Product B",
            #             "slug": "product-b",
            #             "status": "inactive",
            #             "product_relation_type": "cross_sell",
            #             "order": 2,
            #             "relation_settings": {"product_parent_id": 54321, "quantity": 1},
            #             "transport_method": "pickup",
            #             "product_type": "digital"
            #         }
            #     ]
            # }
            ```
        """

        related_product_dict = {}
        for item in products:
            if item.relation_settings["product_parent_id"] not in related_product_dict:
                related_product_dict[item.relation_settings["product_parent_id"]] = []
            related_product_dict[item.relation_settings["product_parent_id"]].append(
                {
                    "image": helpers.ImageHelpers.image_resolver(
                        item.image,
                        (
                            data_options.manufacturer_dict[
                                item.manufacturer_id
                            ].seo_slug
                            if data_options.manufacturer_dict.get(item.manufacturer_id)
                            else ""
                        )
                        + (
                            f"/{data_options_filtered.band_filtered[item.product_id].seo_slug}"
                            if data_options_filtered.band_filtered.get(item.product_id)
                            else ""
                        ),
                        image_settings,
                    ),
                    "model": item.model,
                    "name": helpers.CommonHelpers.capword(item.name),
                    "order": item.order,
                    "product_component_id": item.product_component_id,
                    "product_id": item.product_id,
                    "product_relation_type": item.product_relation_type,
                    "product_type": item.product_type,
                    "quantity": item.relation_settings["quantity"],
                    "slug": item.seo_slug,
                    "status": item.status,
                    "transport_method": item.transport_method,
                }
            )
        return related_product_dict

    @classmethod
    def product_to_author_format(cls, authors):
        """
        Formats a list of author items into a standardized format.

        Args:
            authors (list): List of author items to be formatted.

        Returns:
            list: A list of dictionaries containing the formatted author information.

        Example:
            ```python
            authors = [
                {"author_id": 1, "callsign": "Author1", "first_name": "John", "last_name": "Doe", "order": 1, "product_author_id": 123, "product_id": 54321, "role": "Author", "visibility": "public"},
                {"author_id": 2, "callsign": "Author2", "first_name": "Jane", "last_name": "Smith", "order": 2, "product_author_id": 456, "product_id": 98765, "role": "Editor", "visibility": "private"}
            ]

            formatted_authors = ProductHelpers.product_to_author_format(authors)
            print(formatted_authors)
            # Output: [
            #     {
            #         "author_id": 1,
            #         "callsign": "Author1",
            #         "first_name": "John",
            #         "last_name": "Doe",
            #         "order": 1,
            #         "product_author_id": 123,
            #         "product_id": 54321,
            #         "role": "Author",
            #         "visibility": "public"
            #     },
            #     {
            #         "author_id": 2,
            #         "callsign": "Author2",
            #         "first_name": "Jane",
            #         "last_name": "Smith",
            #         "order": 2,
            #         "product_author_id": 456,
            #         "product_id": 98765,
            #         "role": "Editor",
            #         "visibility": "private"
            #     }
            # ]
            ```
        """

        return [
            dict(
                author_id=item.author_id,
                callsign=item.callsign,
                first_name=helpers.CommonHelpers.capword(item.first_name),
                last_name=helpers.CommonHelpers.capword(item.last_name),
                order=item.order,
                product_author_id=item.product_author_id,
                product_id=item.product_id,
                role=item.role,
                visibility=item.visibility,
            )
            for item in authors
        ]

    @classmethod
    def single_product_format(
        cls,
        data_options,
        data_options_filtered,
        filter_params,
        product,
        product_in_cart,
        settings,
    ):
        """
        Formats a single product item into a standardized format.

        Args:
            data_options (object): Data options object containing additional data.
            data_options_filtered (object): Filtered data options object.
            filter_params (object): Filter parameters object.
            product (object): Product item to be formatted.
            product_in_cart (int): Quantity of the product in the cart.
            settings (dict): Settings for product formatting.

        Returns:
            object: A ProductContainer object containing the formatted product information.

        Example:
            ```python
            data_options = DataOptions()
            data_options_filtered = DataOptionsFiltered()
            filter_params = FilterParams()
            product = Product()
            product_in_cart = 2
            settings = {
                "product_settings": {"description_technical": 1},
                "image_settings": {"width": 500, "height": 500}
            }

            formatted_product = ProductHelpers.single_product_format(data_options, data_options_filtered, filter_params, product, product_in_cart, settings)
            print(formatted_product)
            # Output: <ProductContainer object at 0x7f8a2c6a5a90>
            ```
        """
        product_minimum = helpers.CommonHelpers.decimal_with_two_places(product.minimum)
        product_unit = helpers.CommonHelpers.decimal_with_two_places(product.unit)
        file_path = (
            data_options.manufacturer_dict.get(product.manufacturer_id).seo_slug
            + (
                f"/{data_options_filtered.band_filtered.get(product.product_id).seo_slug}"
                if data_options_filtered.band_filtered.get(product.product_id)
                else ""
            )
            + "/"
        )
        product_container = data_containers.ProductContainer(
            ancient_special_price=helpers.CommonHelpers.price_gross_number(
                product.ancient_special_price, data_options.tax_dict[product.tax_id]
            ),
            availability_id=product.availability_id,
            calculated_price=operations.ProductOperations.calculate_unit_price(
                product, data_options.tax_dict[product.tax_id]
            ),
            catalog_price=None,
            collection=None,
            description=product.description,
            discount_id=None,
            ean=product.ean,
            file_path=file_path,
            height=product.height,
            image=helpers.ImageHelpers.image_resolver(
                product.image,
                file_path,
                settings["image_settings"],
            ),
            length=product.length,
            manual_discount=None,
            manufactured=product.manufactured,
            manufacturer=data_options.manufacturer_dict.get(
                product.manufacturer_id
            ).name,
            manufacturer_id=product.manufacturer_id,
            meta_description=product.meta_description,
            meta_title=product.u_title,
            minimum=product_minimum,
            minimum_unit=helpers.CommonHelpers.decimal_with_two_places(
                int(product.minimum) * product.unit
            ),
            model=helpers.CommonHelpers.capword(product.model or ""),
            name=product.name or "",
            pieces=product.pieces,
            price=helpers.CommonHelpers.price_gross_number(
                product.price, data_options.tax_dict[product.tax_id]
            ),
            price_net=helpers.CommonHelpers.price_precision(product.price),
            product_id=product.product_id,
            product_in_cart=product_in_cart,
            cart_quantity=(product_in_cart / product_unit if product_in_cart else 1),
            cart_quantity_real_unit=product_in_cart or product_unit,
            product_type=product.product_type,
            setting_template_id=product.setting_template_id,
            slug=product.seo_slug,
            special_id=None,
            special_price=helpers.CommonHelpers.price_gross_number(
                product.special_price, data_options.tax_dict[product.tax_id]
            ),
            special_transport=data_options.transport_dict.get(product.product_id)
            if data_options
            else "",
            square_meter=product.square_meter,
            status=product.status,
            storage_current="",
            tax_id=product.tax_id,
            # transport_method=product.transport_method,
            unit=product_unit,
            width=product.width,
        )
        if settings["product_settings"]["description_technical"] == 1:
            product_container.description_technical = product.description_technical
        cls.return_single_product_storages(
            data_options_filtered, product, product_container
        )
        if (
            product.date_available
            > datetime.strptime(filter_params.today, "%Y-%m-%d").date()
        ):
            product_container.date_available = product.date_available.strftime(
                "%d-%m-%Y"
            )
        return product_container

    @classmethod
    def return_single_product_storages(
        cls, data_options_filtered, product, product_container
    ):
        if data_options_filtered.storage_filtered.get(product.product_id):
            product_container.storage_current = data_options_filtered.storage_filtered[
                product.product_id
            ]
            product_container.storage_summary = (
                cls.calculate_number_of_unpromoted_products(
                    product, product_container.storage_current
                )
            )
            product_container.not_promoted_storage_summary = (
                product_container.storage_summary
            )
            product_container.storage_summary_real_unit = (
                helpers.CommonHelpers.price_precision(
                    product_container.storage_summary * product.unit
                )
            )

    @classmethod
    def product_category_format(
        cls,
        data_options,
        data_options_filtered,
        product_list,
        image_settings,
    ):
        """
        Formats a list of product items into a standardized format for a product category.

        Args:
            data_options (object): Data options object containing additional data.
            data_options_filtered (object): Filtered data options object.
            product_list (list): List of product items to be formatted.
            settings.image_settings (dict): Settings for image resolution.

        Returns:
            list: A list of ProductContainerZIP objects containing the formatted product information.

        Example:
            ```python
            data_options = DataOptions()
            data_options_filtered = DataOptionsFiltered()s
            product_list = [
                {"product_id": 1, "availability_id": 123, "ancient_special_price": 9.99, "band_id": 1, "catalog_price": 19.99, "discount_id": None, "ean": "1234567890", "image": "image_url1", "minimum_unit": 1.0},
                {"product_id": 2, "availability_id": 456, "ancient_special_price": 14.99, "band_id": 2, "catalog_price": 24.99, "discount_id": 1, "ean": "0987654321", "image": "image_url2", "minimum_unit": 2.0}
            ]
            settings,image_settings = {"width": 500, "height": 500}

            formatted_products = ProductHelpers.product_category_format(data_options, data_options_filtered, product_list, settings_image_settings)
            print(formatted_products)
            # Output: [
            #     <ProductContainerZIP object at 0x7f8a2c6a5a90>,
            #     <ProductContainerZIP object at 0x7f8a2c6a5b00>
            # ]
            ```
        """
        return [
            data_containers.ProductContainerZIP(
                product_id=item.product_id,
                availability_id=item.availability_id,
                ancient_special_price=helpers.CommonHelpers.price_gross_number(
                    item.ancient_special_price, data_options.tax_dict[item.tax_id]
                ),
                band=data_options_filtered.band_filtered.get(item.product_id).band_id
                if data_options_filtered.band_filtered.get(item.product_id)
                else "",
                catalog_price=helpers.CommonHelpers.price_gross_number(
                    item.catalog_price, data_options.tax_dict[item.tax_id]
                ),
                calculated_price=operations.ProductOperations.calculate_unit_price(
                    item, data_options.tax_dict[item.tax_id]
                ),
                collection=data_options_filtered.collection_filtered.get(
                    item.product_id, ""
                ),
                discount_id=item.discount_id,
                ean=item.ean,
                image=helpers.ImageHelpers.image_resolver(
                    item.image,
                    (
                        data_options.manufacturer_dict[item.manufacturer_id].seo_slug
                        if data_options.manufacturer_dict.get(item.manufacturer_id)
                        else ""
                    )
                    + (
                        "/"
                        + data_options_filtered.band_filtered.get(
                            item.product_id
                        ).seo_slug
                        if data_options_filtered.band_filtered.get(item.product_id)
                        else ""
                    ),
                    image_settings,
                ),
                minimum_unit=helpers.CommonHelpers.decimal_with_two_places(
                    int(item.minimum) * item.unit
                ),
                manufactured=item.manufactured,
                manufacturer_id=item.manufacturer_id,
                minimum=float(item.minimum),
                model=item.model,  # helpers.CommonHelpers.capword(item.model or ""),
                name=item.name,  # helpers.CommonHelpers.capword(item.name or ""),
                pieces=item.pieces,
                price=helpers.CommonHelpers.price_gross_number(
                    item.price, data_options.tax_dict[item.tax_id]
                ),
                product_type=item.product_type,
                # cart_quantity=round(            Decimal(item.cart_quantity_real_unit) / item.unit  ),
                price_net=helpers.CommonHelpers.price_precision(item.price),
                slug=item.seo_slug,
                special_transport=data_options_filtered.transport_filtered.get(
                    item.product_id
                )
                if data_options_filtered
                else "",
                special_price=helpers.CommonHelpers.price_gross_number(
                    item.special_price, data_options.tax_dict[item.tax_id]
                ),
                storage_current=data_options_filtered.storage_filtered.get(
                    item.product_id
                )
                if data_options_filtered
                else "",
                square_meter=item.square_meter,
                status=item.status,
                unit=float(item.unit),
                # physical_transport_method=item.physical_transport_method,
                # digital_transport_method=item.digital_transport_method,
            )
            for item in product_list
        ]

    @classmethod
    def product_option_format(
        cls,
        data_options,
        data_options_filtered,
        product_list,
        settings_image_settings,
    ):
        """
        Formats a list of product items into a standardized format for a product category.

        Args:
            data_options (object): Data options object containing additional data.
            data_options_filtered (object): Filtered data options object.
            product_list (list): List of product items to be formatted.
            settings_image_settings (dict): Settings for image resolution.

        Returns:
            list: A list of ProductContainerZIP objects containing the formatted product information.

        Example:
            ```python
            data_options = DataOptions()
            data_options_filtered = DataOptionsFiltered()
            product_list = [
                {"product_id": 1, "availability_id": 123, "ancient_special_price": 9.99, "band_id": 1, "catalog_price": 19.99, "discount_id": None, "ean": "1234567890", "image": "image_url1", "minimum_unit": 1.0},
                {"product_id": 2, "availability_id": 456, "ancient_special_price": 14.99, "band_id": 2, "catalog_price": 24.99, "discount_id": 1, "ean": "0987654321", "image": "image_url2", "minimum_unit": 2.0}
            ]
            settings_image_settings = {"width": 500, "height": 500}

            formatted_products = ProductHelpers.product_option_format(data_options, data_options_filtered, product_list, settings_image_settings)
            print(formatted_products)
            # Output: [
            #     <ProductContainerZIP object at 0x7f8a2c6a5a90>,
            #     <ProductContainerZIP object at 0x7f8a2c6a5b00>
            # ]
            ```
        """
        return [
            data_containers.ProductContainerZIP(
                product_id=item.product_id,
                availability_id=item.availability_id,
                ancient_special_price=helpers.CommonHelpers.price_gross_number(
                    item.ancient_special_price, data_options.tax_dict[item.tax_id]
                ),
                band=data_options_filtered.band_filtered.get(item.product_id).band_id
                if data_options_filtered.band_filtered.get(item.product_id)
                else "",
                catalog_price=helpers.CommonHelpers.price_gross_number(
                    item.catalog_price, data_options.tax_dict[item.tax_id]
                ),
                calculated_price=operations.ProductOperations.calculate_unit_price(
                    item, data_options.tax_dict[item.tax_id]
                ),
                collection=data_options_filtered.collection_filtered.get(
                    item.product_id, ""
                ),
                discount_id=item.discount_id,
                ean=item.ean,
                image=helpers.ImageHelpers.image_resolver(
                    item.image,
                    (
                        data_options.manufacturer_dict[item.manufacturer_id].seo_slug
                        if data_options.manufacturer_dict.get(item.manufacturer_id)
                        else ""
                    )
                    + (
                        "/"
                        + data_options_filtered.band_filtered.get(
                            item.product_id
                        ).seo_slug
                        if data_options_filtered.band_filtered.get(item.product_id)
                        else ""
                    ),
                    settings_image_settings,
                ),
                minimum_unit=helpers.CommonHelpers.decimal_with_two_places(
                    int(item.minimum) * item.unit
                ),
                manufactured=item.manufactured,
                manufacturer_id=item.manufacturer_id,
                minimum=float(item.minimum),
                model=item.model,  # helpers.CommonHelpers.capword(item.model or ""),
                name=item.name,  # helpers.CommonHelpers.capword(item.name or ""),
                pieces=item.pieces,
                price=helpers.CommonHelpers.price_gross_number(
                    item.price, data_options.tax_dict[item.tax_id]
                ),
                product_to_product_relation_settings=item.relation_settings,
                product_type=item.product_type,
                price_net=helpers.CommonHelpers.price_precision(item.price),
                slug=item.seo_slug,
                special_transport=data_options_filtered.transport_filtered.get(
                    item.product_id
                )
                if data_options_filtered
                else "",
                special_price=helpers.CommonHelpers.price_gross_number(
                    item.special_price, data_options.tax_dict[item.tax_id]
                ),
                storage_current=data_options_filtered.storage_filtered.get(
                    item.product_id
                )
                if data_options_filtered
                else "",
                square_meter=item.square_meter,
                status=item.status,
                unit=float(item.unit),
                # physical_transport_method=item.physical_transport_method,
                # digital_transport_method=item.digital_transport_method,
            )
            for item in product_list
        ]

    @classmethod
    def calculate_number_of_unpromoted_products(cls, item, storage_filtered):
        """
        Calculates the number of unpromoted products for a given item.

        Args:
            cls: The class itself.
            item (object): The item for which to calculate the number of unpromoted products.
            storage_filtered (object): Filtered storage object.

        Returns:
            int or None: The number of unpromoted products if the item's transport method is 0, otherwise None.

        Example:
            ```python
            item = Item()
            storage_filtered = StorageFiltered()

            num_unpromoted_products = ProductHelpers.calculate_number_of_unpromoted_products(item, storage_filtered)
            print(num_unpromoted_products)
            # Output: 10
            ```
        """
        return (
            sum(
                filter(
                    None,
                    (item.storage_quantity for item in storage_filtered),
                )
            )
            if item.product_type == 0
            else 0
        )

    @classmethod
    def product_in_cart(cls, request, product_id):
        """
        Checks if a product is in the cart.

        Args:
            cls: The class itself.
            request (object): The request object.
            product_id: The ID of the product to check.

        Returns:
            Decimal or None: The quantity of the product in the cart as a Decimal if it exists, otherwise None.

        Example:
            ```python
            request = Request()
            product_id = 123

            quantity_in_cart = ProductHelpers.product_in_cart(request, product_id)
            print(quantity_in_cart)
            # Output: Decimal('2')
            ```
        """
        return (
            request.session.get("cart_contents")
            and request.session["cart_contents"]["products"].get(product_id)
            and Decimal(request.session["cart_contents"]["products"][product_id])
        )

    @classmethod
    def product_format_cart_dict(
        cls,
        data_options,
        data_options_filtered,
        product_list,
        settings,
    ):
        """
        Formats a dictionary of product items into a standardized format for the cart.

        Args:
            cls: The class itself.
            data_options (object): Data options object containing additional data.
            data_options_filtered (object): Filtered data options object.
            product_list (list): List of product items to be formatted.
            settings (dict): Settings for the cart.

        Returns:
            dict: A dictionary of ProductInCartContainer objects containing the formatted product information.

        Example:
            ```python
            data_options = DataOptions()
            data_options_filtered = DataOptionsFiltered()
            product_list = [
                {"product_id": 1, "availability_id": 123, "ancient_special_price": 9.99, "band_id": 1, "catalog_price": 19.99, "discount_id": None, "ean": "1234567890", "image": "image_url1", "minimum_unit": 1.0},
                {"product_id": 2, "availability_id": 456, "ancient_special_price": 14.99, "band_id": 2, "catalog_price": 24.99, "discount_id": 1, "ean": "0987654321", "image": "image_url2", "minimum_unit": 2.0}
            ]
            settings = {"price_input": "net", "image_settings": {"width": 500, "height": 500}}

            formatted_products = ProductHelpers.product_format_cart_dict(data_options, data_options_filtered, product_list, settings)
            print(formatted_products)
            # Output: {
            #     1: <ProductInCartContainer object at 0x7f8a2c6a5a90>,
            #     2: <ProductInCartContainer object at 0x7f8a2c6a5b00>
            # }
            ```
        """
        product_dict = {}
        for item in product_list:
            product_dict[item.product_id] = data_containers.ProductInCartContainer(
                product_id=item.product_id,
                ancient_special_price=helpers.CommonHelpers.price_gross_number(
                    item.ancient_special_price, data_options.tax_dict[item.tax_id]
                ),
                availability_id=item.availability_id,
                calculated_price=operations.ProductOperations.calculate_base_unit_price(
                    item, data_options.tax_dict[item.tax_id]
                ),
                catalog_price=operations.ProductOperations.get_price_based_on_system_settings(
                    settings.get("price_input"),
                    data_options.tax_dict[item.tax_id].value,
                    item.catalog_price,
                ),
                discount_id=item.discount_id,
                ean=item.ean,
                image=helpers.ImageHelpers.image_resolver(
                    item.image,
                    (
                        data_options.manufacturer_dict[item.manufacturer_id].seo_slug
                        if data_options.manufacturer_dict.get(item.manufacturer_id)
                        else ""
                    )
                    + (
                        "/"
                        + data_options_filtered.band_filtered.get(
                            item.product_id
                        ).seo_slug
                        if data_options_filtered.band_filtered.get(item.product_id)
                        else ""
                    ),
                    settings["image_settings"],
                ),
                manual_discount=item.manual_discount,
                manufactured=item.manufactured,
                manufacturer_id=item.manufacturer_id,
                minimum=helpers.CommonHelpers.price_precision(item.minimum),
                not_promoted_storage_summary=0,
                model=item.model,
                name=item.name,
                one_batch=item.one_batch,
                pieces=item.pieces,
                price=helpers.CommonHelpers.price_gross_number(
                    item.price, data_options.tax_dict[item.tax_id]
                ),
                product_type=item.product_type,
                # "cart_quantity= operations.CartOperations.calculate_packages(item),
                price_net=helpers.CommonHelpers.price_precision(item.price),
                slug=item.seo_slug,
                square_meter=item.square_meter,
                status=item.status,
                tax_id=item.tax_id,
                unit=Decimal(str(item.unit)),
                unit_tax=helpers.CommonHelpers.price_tax(
                    item.price,
                    data_options.tax_dict[item.tax_id],
                ),
                transport_method=item.transport_method,
                # digital_transport_method=item.digital_transport_method,
                weight=Decimal(str(item.weight)),
            )
            if data_options_filtered.band_filtered.get(item.product_id):
                product_dict[
                    item.product_id
                ].band = data_options_filtered.band_filtered.get(item.product_id)
            if data_options_filtered.storage_filtered.get(item.product_id):
                product_dict[
                    item.product_id
                ].storage_current = data_options_filtered.storage_filtered.get(
                    item.product_id
                )
                product_dict[
                    item.product_id
                ].storage_summary = cls.calculate_number_of_unpromoted_products(
                    item, product_dict[item.product_id].storage_current
                )
                product_dict[
                    item.product_id
                ].not_promoted_storage_summary = product_dict[
                    item.product_id
                ].storage_summary
                product_dict[
                    item.product_id
                ].storage_summary_real_unit = helpers.CommonHelpers.price_precision(
                    product_dict[item.product_id].storage_summary * item.unit
                )
                product_dict[
                    item.product_id
                ].special_filtered_dict = data_options_filtered.special_filtered_dict
                # product_dict[item.product_id].detailed_pricing[0] = [
                #     product_dict[
                #         item.product_id
                #     ].storage_summary_real_unit,
                #     0,
                #     0,
                # ]

        return product_dict

    @classmethod
    def product_storage_format(cls, product_storage_list):
        """
        Formats a list of product storage items into a dictionary of standardized format.

        Args:
            cls: The class itself.
            product_storage_list (list): List of product storage items to be formatted.

        Returns:
            dict: A dictionary where the keys are product IDs and the values are lists of ProductStorageContainer objects containing the formatted product storage information.

        Example:
            ```python
            product_storage_list = [
                {"product_storage_id": 1, "product_id": 1, "quantity": 10, "storage_id": 1, "unit": 1.0},
                {"product_storage_id": 2, "product_id": 1, "quantity": 5, "storage_id": 2, "unit": 1.0},
                {"product_storage_id": 3, "product_id": 2, "quantity": 8, "storage_id": 1, "unit": 1.0},
                {"product_storage_id": 4, "product_id": 2, "quantity": 3, "storage_id": 2, "unit": 1.0}
            ]

            formatted_storage = ProductHelpers.product_storage_format(product_storage_list)
            print(formatted_storage)
            # Output: {
            #     1: [
            #         <ProductStorageContainer object at 0x7f8a2c6a5a90>,
            #         <ProductStorageContainer object at 0x7f8a2c6a5b00>
            #     ],
            #     2: [
            #         <ProductStorageContainer object at 0x7f8a2c6a5b10>,
            #         <ProductStorageContainer object at 0x7f8a2c6a5b20>
            #     ]
            # }
            ```
        """
        storage_formatted_dict = {}
        for item in product_storage_list:
            if not storage_formatted_dict.get(item.product_id):
                storage_formatted_dict[item.product_id] = []
            if item.quantity and item.quantity > 0:
                storage_formatted_dict[item.product_id].append(
                    data_containers.ProductStorageContainer(
                        product_storage_id=item.product_storage_id,
                        storage_quantity=item.quantity,
                        storage_id=item.storage_id,
                        storage_quantity_real_unit=helpers.CommonHelpers.price_precision(
                            item.quantity * item.unit
                        ),
                    )
                )
        return storage_formatted_dict

    @classmethod
    def featured_products_format_dict(cls, featured_product, tax_dict):
        """
        Formats a list of featured product items into two dictionaries of standardized format.

        Args:
            cls: The class itself.
            featured_product (list): List of featured product items to be formatted.
            tax_dict (dict): Dictionary containing tax information.

        Returns:
            tuple: A tuple containing two dictionaries:
                - The first dictionary has product IDs as keys and lists of ProductToSpecialContainer objects as values.
                - The second dictionary has product IDs as keys and nested dictionaries as values, where the nested dictionaries have special IDs as keys and ProductToSpecialContainer objects as values.

        Example:
            ```python
            featured_product = [
                {"product_id": 1, "product_special_id": 1, "price": 9.99, "quantity": 10, "storage_aware": True, "tax_id": 1, "special_id": 1},
                {"product_id": 1, "product_special_id": 2, "price": 14.99, "quantity": 5, "storage_aware": False, "tax_id": 1, "special_id": 2},
                {"product_id": 2, "product_special_id": 3, "price": 19.99, "quantity": 8, "storage_aware": True, "tax_id": 2, "special_id": 3},
                {"product_id": 2, "product_special_id": 4, "price": None, "quantity": None, "storage_aware": False, "tax_id": 2, "special_id": 4}
            ]
            tax_dict = {1: 0.1, 2: 0.2}

            formatted_specials, formatted_specials_dict = ProductHelpers.featured_products_format_dict(featured_product, tax_dict)
            print(formatted_specials)
            # Output: {
            #     1: [
            #         <ProductToSpecialContainer object at 0x7f8a2c6a5a90>,
            #         <ProductToSpecialContainer object at 0x7f8a2c6a5b00>
            #     ],
            #     2: [
            #         <ProductToSpecialContainer object at 0x7f8a2c6a5b10>,
            #         <ProductToSpecialContainer object at 0x7f8a2c6a5b20>
            #     ]
            # }

            print(formatted_specials_dict)
            # Output: {
            #     1: {
            #         1: <ProductToSpecialContainer object at 0x7f8a2c6a5a90>,
            #         2: <ProductToSpecialContainer object at 0x7f8a2c6a5b00>
            #     },
            #     2: {
            #         3: <ProductToSpecialContainer object at 0x7f8a2c6a5b10>,
            #         4: <ProductToSpecialContainer object at 0x7f8a2c6a5b20>
            #     }
            # }
            ```
        """
        special_filtered_dict = {}
        for item in featured_product:
            element = data_containers.ProductToSpecialContainer(
                price=Decimal(
                    helpers.CommonHelpers.price_gross_number(
                        item.price, tax_dict[item.tax_id]
                    )
                )
                if item.price
                else None,
                product_special_id=item.product_special_id,
                quantity=item.quantity or None,
                storage_aware=item.storage_aware,
                tax_id=item.tax_id,
                special_id=item.special_id,
            )
            if not special_filtered_dict.get(item.product_id):
                special_filtered_dict[item.product_id] = {}
            special_filtered_dict[item.product_id].update({item.special_id: element})
        return special_filtered_dict

    @classmethod
    def product_transport_format(cls, product_transport_list):
        """
        Formats a list of product transport items into a dictionary of standardized format.

        Args:
            cls: The class itself.
            product_transport_list (list): List of product transport items to be formatted.

        Returns:
            dict: A dictionary where the keys are product IDs and the values are nested dictionaries. The nested dictionaries have transport slot IDs as keys and ProductTransportContainer objects as values, containing the formatted product transport information.

        Example:
            ```python
            product_transport_list = [
                {"product_id": 1, "product_transport_id": 1, "transport_slot_id": 1, "module_name": "Module1", "module_settings": {"setting1": "value1"}, "module_description": "Description1", "name": "Transport1", "transport_id": 1, "local_settings": {"setting2": "value2"}},
                {"product_id": 1, "product_transport_id": 2, "transport_slot_id": 2, "module_name": "Module2", "module_settings": {"setting3": "value3"}, "module_description": "Description2", "name": "Transport2", "transport_id": 2, "local_settings": {"setting4": "value4"}},
                {"product_id": 2, "product_transport_id": 3, "transport_slot_id": 1, "module_name": "Module3", "module_settings": {"setting5": "value5"}, "module_description": "Description3", "name": "Transport3", "transport_id": 3, "local_settings": {"setting6": "value6"}},
                {"product_id": 2, "product_transport_id": 4, "transport_slot_id": 2, "module_name": "Module4", "module_settings": {"setting7": "value7"}, "module_description": "Description4", "name": "Transport4", "transport_id": 4, "local_settings": {}}
            ]

            formatted_transports = ProductHelpers.product_transport_format(product_transport_list)
            print(formatted_transports)
            # Output: {
            #     1: {
            #         1: <ProductTransportContainer object at 0x7f8a2c6a5a90>,
            #         2: <ProductTransportContainer object at 0x7f8a2c6a5b00>
            #     },
            #     2: {
            #         1: <ProductTransportContainer object at 0x7f8a2c6a5b10>,
            #         2: <ProductTransportContainer object at 0x7f8a2c6a5b20>
            #     }
            # }
            ```
        """

        transport_formatted_dict = {}
        for item in product_transport_list:
            if not transport_formatted_dict.get(item.product_id):
                transport_formatted_dict[item.product_id] = {}
            transport_formatted_dict[item.product_id].update(
                {
                    item.transport_slot_id: data_containers.ProductTransportContainer(
                        module_name=item.module_name,
                        module_settings=item.module_settings,
                        module_description=item.module_description,
                        name=item.name,
                        product_id=item.product_id,
                        product_transport_id=item.product_transport_id,
                        transport_id=item.transport_id,
                        transport_slot_id=item.transport_slot_id,
                        local_settings=item.local_settings or {},
                    )
                }
            )
        return transport_formatted_dict

    @classmethod
    def product_collection_format(cls, product_collection_list):
        """
        Formats a list of product collection items into a dictionary of standardized format.

        Args:
            cls: The class itself.
            product_collection_list (list): List of product collection items to be formatted.

        Returns:
            dict: A dictionary where the keys are product IDs and the values are collection names.

        Example:
            ```python
            product_collection_list = [
                {"product_id": 1, "collection_name": "Collection1"},
                {"product_id": 2, "collection_name": "Collection2"},
                {"product_id": 3, "collection_name": "Collection3"}
            ]

            formatted_collections = ProductHelpers.product_collection_format(product_collection_list)
            print(formatted_collections)
            # Output: {
            #     1: "Collection1",
            #     2: "Collection2",
            #     3: "Collection3"
            # }
            ```
        """

        return {
            item.product_id: item.collection_name for item in product_collection_list
        }

    @classmethod
    def add_products_to_urls_container(cls, products_list: list, url_base: str) -> list:
        """
        Adds products to a URLs container by formatting the product information into a list of dictionaries.

        Args:
            cls: The class itself.
            products_list (list): List of product items to be added to the URLs container.
            url_base (str): The base URL for the products.

        Returns:
            list: A list of dictionaries, where each dictionary represents a product in the URLs container. Each dictionary contains the following keys:
                - "url": The complete URL for the product, formed by concatenating the URL base, the product's SEO slug, and the product ID.
                - "date_modified": The formatted date of the product's last modification.

        Example:
            ```python
            products_list = [
                {"product_id": 1, "seo_slug": "product1", "date_modified": "2022-01-01"},
                {"product_id": 2, "seo_slug": "product2", "date_modified": "2022-01-02"},
                {"product_id": 3, "seo_slug": "product3", "date_modified": "2022-01-03"}
            ]
            url_base = "https://example.com/products/"

            urls_container = ProductHelpers.add_products_to_urls_container(products_list, url_base)
            print(urls_container)
            # Output: [
            #     {"url": "https://example.com/products/product1-1", "date_modified": "2022-01-01"},
            #     {"url": "https://example.com/products/product2-2", "date_modified": "2022-01-02"},
            #     {"url": "https://example.com/products/product3-3", "date_modified": "2022-01-03"}
            # ]
            ```
        """
        return [
            {
                "url": url_base + item.seo_slug + "-" + str(item.product_id),
                "date_modified": helpers.CommonHelpers.format_date(item.date_modified),
            }
            for item in products_list
        ]

    @classmethod
    def create_special_price_dict_key_id(cls, special_price):
        """
        No-cache query for current data
        """
        return {item.product_id: item.price for item in special_price}

    @classmethod
    def dimension_filter_format(
        cls, dimension_name: str, dimension_value: float, query_items: list
    ) -> dict:
        """
        Formats a dimension filter into a dictionary representation.

        Args:
            cls: The class itself.
            dimension_name (str): The name of the dimension.
            dimension_value (float): The value of the dimension.
            query_items (list): The list of query items.

        Returns:
            dict: A dictionary representation of the dimension filter, using the ProductDimensionContainer class. The dictionary contains the following keys:
                - "name": The name of the dimension.
                - "value": The value of the dimension.
                - "url": A list of query items, excluding the dimension name.

        Example:
            ```python
            dimension_name = "size"
            dimension_value = 10.0
            query_items = [("color", "red"), ("brand", "Nike"), ("size", "10.0")]

            dimension_filter = ProductHelpers.dimension_filter_format(dimension_name, dimension_value, query_items)
            print(dimension_filter)
            # Output: {
            #     "name": "size",
            #     "value": 10.0,
            #     "url": [("color", "red"), ("brand", "Nike")]
            # }
            ```
        """

        return data_containers.ProductDimensionContainer(
            name=dimension_name,
            value=dimension_value,
            url=([htem for htem in query_items if htem[0] != dimension_name]),
        )

    @classmethod
    def filter_params_dimensions(cls, filter_params: dict, query_items: list) -> dict:
        """
        Filters and formats dimension parameters in the filter parameters dictionary.

        Args:
            cls: The class itself.
            filter_params (dict): The filter parameters dictionary.
            query_items (list): The list of query items.

        Returns:
            dict: The updated filter parameters dictionary. The dictionary contains the following keys:
                - "descripted_front_page": A boolean indicating whether the front page is descripted.
                - "dimensions_checked": A boolean indicating whether the dimensions are checked.
                - "sticky": A boolean indicating whether the filter is sticky.
                - "dimensions": A dictionary of dimension filters, where the keys are dimension names and the values are dictionaries representing the dimension filters.

        Example:
            ```python
            filter_params = {
                "length_min": "10",
                "length_max": "20",
                "width_min": "5",
                "width_max": "15",
                "descripted_front_page": True,
                "dimensions_checked": False,
                "sticky": False,
                "dimensions": {}
            }
            query_items = [("color", "red"), ("brand", "Nike")]

            updated_filter_params = ProductHelpers.filter_params_dimensions(filter_params, query_items)
            print(updated_filter_params)
            # Output: {
            #     "length_min": "10",
            #     "length_max": "20",
            #     "width_min": "5",
            #     "width_max": "15",
            #     "descripted_front_page": False,
            #     "dimensions_checked": True,
            #     "sticky": True,
            #     "dimensions": {
            #         "length_min": {
            #             "name": "length_min",
            #             "value": 10.0,
            #             "url": [("color", "red"), ("brand", "Nike")]
            #         },
            #         "length_max": {
            #             "name": "length_max",
            #             "value": 20.0,
            #             "url": [("color", "red"), ("brand", "Nike")]
            #         },
            #         "width_min": {
            #             "name": "width_min",
            #             "value": 5.0,
            #             "url": [("color", "red"), ("brand", "Nike")]
            #         },
            #         "width_max": {
            #             "name": "width_max",
            #             "value": 15.0,
            #             "url": [("color", "red"), ("brand", "Nike")]
            #         }
            #     }
            # }
            ```
        """

        for item in (
            filter_params.length_min,
            filter_params.length_max,
            filter_params.width_min,
            filter_params.width_max,
        ):
            value = None
            if item:
                value = item.replace(",", ".")
            if value and float(value):
                filter_params.descripted_front_page = False
                filter_params.dimensions_checked = True
                filter_params.sticky = True
                filter_params.dimensions[item] = ProductHelpers.dimension_filter_format(
                    item, value, query_items
                )
        return filter_params

    @classmethod
    def special_transport_products_format_dict(cls, special_transports, cart_products):
        """
        Formats a dictionary of special transport products into a more structured format.

        Args:
            cls: The class itself.
            special_transports (list): List of special transport products.
            cart_products: The cart products.

        Returns:
            dict: A dictionary where the keys are product IDs and the values are nested dictionaries representing the special transport products. Each nested dictionary contains the following keys:
                - "local_settings": The local settings of the special transport product.
                - "quantity": The quantity of the special transport product.
                - "transport_id": The transport ID of the special transport product.

        Example:
            ```python
            special_transports = [
                {"product_id": 1, "local_settings": "settings1", "transport_id": 123},
                {"product_id": 2, "local_settings": "settings2", "transport_id": 456},
                {"product_id": 3, "local_settings": "settings3", "transport_id": 789}
            ]
            cart_products = [...]

            special_transport_dict = ProductHelpers.special_transport_products_format_dict(special_transports, cart_products)
            print(special_transport_dict)
            # Output: {
            #     1: {
            #         "local_settings": "settings1",
            #         "quantity": "",
            #         "transport_id": 123
            #     },
            #     2: {
            #         "local_settings": "settings2",
            #         "quantity": "",
            #         "transport_id": 456
            #     },
            #     3: {
            #         "local_settings": "settings3",
            #         "quantity": "",
            #         "transport_id": 789
            #     }
            # }
            ```
        """
        special_transport_dict = {}
        if special_transports:
            for item in special_transports:
                special_transport_dict[item.product_id] = {
                    "local_settings": item.local_settings,
                    "quantity": "",
                    "transport_id": item.transport_id,
                }
        return special_transport_dict

    @classmethod
    def product_options_attributes(
        cls, attributes: dict, product_attributes_list: list
    ) -> dict:
        """
        Generates a dictionary of product attributes based on the provided attributes and product attributes list.

        Args:
            cls: The class itself.
            attributes (dict): The dictionary of attributes.
            product_attributes_list (list): The list of product attributes.

        Returns:
            dict: A dictionary where the keys are product IDs and the values are lists of attribute dictionaries. Each attribute dictionary contains the attribute ID as the key and the corresponding attribute value as the value.

        Example:
            ```python
            attributes = {
                1: "attribute1",
                2: "attribute2",
                3: "attribute3"
            }
            product_attributes_list = [
                {"product_id": 1, "attribute_id": 1},
                {"product_id": 1, "attribute_id": 2},
                {"product_id": 2, "attribute_id": 2},
                {"product_id": 2, "attribute_id": 3},
                {"product_id": 3, "attribute_id": 1}
            ]

            product_attributes = ProductHelpers.product_options_attributes(attributes, product_attributes_list)
            print(product_attributes)
            # Output: {
            #     1: [
            #         {1: "attribute1"},
            #         {2: "attribute2"}
            #     ],
            #     2: [
            #         {2: "attribute2"},
            #         {3: "attribute3"}
            #     ],
            #     3: [
            #         {1: "attribute1"}
            #     ]
            # }
            ```
        """
        product_attributes = {}
        for item in product_attributes_list:
            single_attribute = {item.attribute_id: attributes.get(item.attribute_id)}
            if not product_attributes.get(item.product_id):
                product_attributes[item.product_id] = [single_attribute]
            else:
                product_attributes[item.product_id].append(single_attribute)
        return product_attributes

    @classmethod
    def route_name_filters(cls, request, filter_params):
        """ """
        route_names = {"product_special": cls.product_special_filter_param}
        if route_names.get(request.matched_route.name):
            return route_names[request.matched_route.name](filter_params)
        return filter_params

    @classmethod
    def product_special_filter_param(cls, filter_params):
        filter_params.special_product = True
        return filter_params
