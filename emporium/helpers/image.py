# from collections import namedtuple
from functools import namedtuple

from pathlib import Path
import os
import shutil
import uuid
import contextlib

from PIL import Image, ImageOps

from emporium import helpers, operations

path = "/srv/modna/emporium/images/"
banner_size = (320, 220)
Sizes = namedtuple("Sizes", "name size")
FileExt = namedtuple("FileExt", "extension file_type")
# image_sizes = (Sizes(name="base", size=(800, 800)), Sizes("thumbnail", (344, 344)))

IMAGE_SIZES = (
    Sizes(name="base", size=(1200, 1200)),
    Sizes(name="thumbnails", size=(500, 500)),
)
BANNER_SIZES = (Sizes(name="banners", size=()),)

FILE_EXTENSIONS = (
    FileExt(extension="webp", file_type="WEBP"),
    FileExt(extension="jpg", file_type="JPEG"),
)


class ImageHelpers:
    @classmethod
    def calculate_image_size(cls, image_pil, image_size, offset=0):
        ratio_w = image_size[0] / image_pil.width
        ratio_h = image_size[1] / image_pil.height
        resize_width = image_size[0]
        resize_height = image_size[1]
        if ratio_w < ratio_h:
            resize_height = round(ratio_w * image_pil.height)
        else:
            resize_width = round(ratio_h * image_pil.width)
        return resize_height - (2 * offset), resize_width - (2 * offset)

    @classmethod
    def image_resize(cls, image_pil, resize_height, resize_width):
        return image_pil.resize((resize_width, resize_height), Image.LANCZOS)

    @classmethod
    def image_add_border(cls, image):
        return ImageOps.expand(image, border=1, fill=0)

    @classmethod
    def image_add_background(cls, image_pil, image_size, resize_height, resize_width):
        offset = (
            round((image_size[0] - resize_width) / 2),
            round((image_size[1] - resize_height) / 2),
        )
        background = Image.new(
            "RGBA", (image_size[0], image_size[1]), (255, 255, 255, 255)
        )
        background.paste(image_pil, offset)
        background.convert("RGB")
        return background

    @classmethod
    def add_background_color_mask(cls, image, bg_color="WHITE"):
        if image:
            try:
                image = image.convert("RGBA")
                return Image.new("RGBA", image.size, bg_color).paste(image, mask=image)
            except Exception as e:
                print(e)

    @classmethod
    def set_rgb_color(cls, image):
        if image.mode != "RGB":
            image = image.convert("RGB")
        return image

    @classmethod
    def image_resolver(
        cls,
        image,
        manufacturer_seo_slug="",
        image_settings: dict = None,
        show_placeholder=True,
    ):
        for value in image_settings["image_file_extensions"].values():
            for image_size in image_settings["image_sizes"]:  # image_size
                if not Path(
                    f"{helpers.CommonHelpers.asset_path(image_size)}{manufacturer_seo_slug}/{image}.{value}"
                ).is_file():
                    return "no_image" if show_placeholder else None
        return f"{manufacturer_seo_slug}/{image}"

    @classmethod
    def generate_image_thumbnail(cls, args):
        helpers.CommonHelpers.multiprocess_function_call(
            cls.generate_image_from_file_web_jpg, args
        )

    @classmethod
    def generate_source_images_from_existing_uploads(cls, args):
        helpers.CommonHelpers.multiprocess_function_call(
            cls.generate_source_images, args
        )

    @classmethod
    def generate_image_from_file_web_jpg(cls, file_list):
        counter = 0
        for infile in file_list:
            image_file = Path(
                f"{helpers.CommonHelpers.asset_path('source/')}{str(infile[0])}/{ infile[1]} .webp"
            )
            if image_file.is_file():
                im = Image.open(image_file)
                cls.resize_and_save_image_using_system_settings(im, infile, IMAGE_SIZES)
            else:
                counter += 1
                print("Missing image", image_file)
        print(
            "Process finished failed conversions/files total", counter, len(file_list)
        )

    @classmethod
    def generate_source_images(cls, file_list):
        for infile in file_list:
            image_file = Path(
                helpers.CommonHelpers.asset_path("images") + str(infile[0])
            )
            if image_file.is_file():
                im = Image.open(image_file)
                cls.add_background_color_mask(im)
                cls.save_uploaded_image_as_webp(
                    im,
                    (infile[0], str(infile[1])[: str(infile[1]).find(".")]),
                    "source",
                    f"/{infile[1]}",
                )

    @classmethod
    def save_uploaded_images(cls, file_list):
        for infile in file_list:
            image_file = Path(os.path.join("/tmp", f"{infile[1]}"))
            if image_file.is_file():
                im = Image.open(image_file)
                infile = (
                    infile[0],
                    str(infile[1][infile[1].find("/", 2) + 1 :]),
                    infile[2],
                )
                operations.CommonOperations.generate_image_upload_folders(infile[0])
                cls.save_uploaded_image_as_webp(
                    im, infile, "source", f"/{infile[0]}/{infile[1]}"
                )
                if infile[2] == "banners":
                    file_name = f"{str(Path(helpers.CommonHelpers.asset_path(infile[0])))}/{str(infile[1])}"
                    cls.save_image(im, file_name)
                else:
                    cls.resize_and_save_image_using_system_settings(
                        im, infile, IMAGE_SIZES
                    )

    @classmethod
    def save_uploaded_image_as_webp(cls, im, infile, asset_path, path):
        try:
            im.save(
                str(Path(helpers.CommonHelpers.asset_path(asset_path)))
                + path
                + ".webp",
                "WEBP",
            )
        except Exception as e:
            print(e)

    @classmethod
    def resize_and_save_image_using_system_settings(cls, im, infile, image_sizes):
        # Border must me added before checking image sizes
        im = cls.image_add_border(im)
        for item in image_sizes:
            file_name = f"{(Path(helpers.CommonHelpers.asset_path(item.name)))}/{infile[0]}/{str(infile[1])}"
            resize_height, resize_width = cls.calculate_image_size(im, item.size)
            im = cls.image_resize(im, resize_height, resize_width)
            im = cls.image_add_background(im, item.size, resize_height, resize_width)
            cls.save_image(im, file_name)

    @classmethod
    def save_image(cls, im, file_name):
        for extension in FILE_EXTENSIONS:
            if extension.file_type == "JPEG":
                im = im.convert("RGB")
            im.save(f"{file_name}.{extension.extension}", extension.file_type)

    @classmethod
    def delete_image_all_resolutions_and_types(cls, manufacturer_seo_slug, image_path):
        if file_list := [
            f"{helpers.CommonHelpers.asset_path(item)}{manufacturer_seo_slug}/{image_path}.{value}"
            for value in ("webp", "jpg")
            for item in ("source", "thumbnails", "base")
        ]:
            for item in file_list:
                try:
                    os.remove(item)
                except FileNotFoundError:
                    continue
        # return value

    @classmethod
    def image_save(cls, file, file_ext, path_prefix, img_type, seo_slug=None):
        uploaded_file = os.path.join("/tmp", f"{uuid.uuid4()}")
        try:
            with open(f"{uploaded_file}~", "wb") as output_file:
                shutil.copyfileobj(file, output_file)
            os.rename(f"{uploaded_file}~", uploaded_file)
            shutil.copyfile(uploaded_file, f"/tmp/{path_prefix}")
            cls.save_uploaded_images(
                [
                    (
                        seo_slug if img_type == "source" else img_type,
                        f"/tmp/{path_prefix}",
                        img_type,
                    ),
                ]
            )
            return 200
        except IOError:
            return 411
