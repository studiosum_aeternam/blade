"""
Cache helper functions
"""

import redis

from emporium import services


class CacheHelpers(object):
    @classmethod
    def flush_cache(cls, request=None):
        """
        Cached regions have short (32 signs) keys (some customer credentials are also stored in Redis - to be changed)
        """
        if request:
            services.ProductService.product_delete_filter_string(request, "DRAFT_")
        r = redis.Redis(host="127.0.0.1", db=2)
        with r.pipeline() as pipe:
            for x in r.scan_iter(match=(32 * "?")):  # Yup this sucks
                pipe.delete(x)
            pipe.execute()
