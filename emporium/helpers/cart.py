import typing
from decimal import Decimal
from emporium import data_containers, operations


class CartHelpers:
    @classmethod
    def remove_cart_from_db_and_session(cls, request, cart) -> None:
        cls.remove_cart_from_db(request, cart)
        cls.remove_cart_variables_from_session(request)

    @classmethod
    def remove_cart_from_db(cls, request, session_cart) -> None:
        request.dbsession.delete(session_cart)

    @classmethod
    def remove_cart_variables_from_session(cls, request) -> None:
        for _ in ("cart_id", "cart_contents"):
            if request.session.get(_):
                del request.session[_]

    @classmethod
    def cart_contents_get_current_product_values(cls, cart_products, product_list):
        if cart_products and product_list:
            for cart_products_keys, cart_products_values in product_list.items():
                cart_products_values.cart_quantity_real_unit = cart_products.get(
                    cart_products_keys
                ).cart_quantity_real_unit
                cart_products_values.product_single_set = cart_products.get(
                    cart_products_keys
                ).product_single_set
            return product_list
        else:
            return {}

    @classmethod
    def return_possible_transport_and_payment_methods(
        cls,
        request,
        cart_parameters,
        cart_contents: typing.Dict = dict(),
        data_options: typing.Dict = dict(),
        data_options_filtered: typing.Dict = dict(),
    ):
        data_options.transport_dict = (
            operations.TransportOperations.return_applicable_transport_methods(
                cart_contents,
                cart_parameters,
                data_options.transport_dict,
                data_options_filtered.transport_filtered,
            )
        )
        data_options.transport_dict = (
            operations.TransportOperations.calculate_transport_method_fees(
                cart_parameters.get("weight", 0),
                data_options.transport_dict,
            )
        )
        data_options.payment_dict = (
            operations.PaymentOperations.return_applicable_payment_methods(
                data_options.payment_dict,
                {item.transport_method for item in cart_contents.values()},
                data_options.transport_dict,
            )
        )
        data_options.payment_dict = (
            operations.PaymentOperations.calculate_cost_of_payment_methods(
                cart_parameters, data_options.payment_dict
            )
        )
        return data_options

    @classmethod
    def return_dict_from_cart_products(cls, cart_products: typing.Dict = dict()):
        return (
            {
                int(k): data_containers.CartProducts(
                    cart_quantity_real_unit=v["cart_quantity_real_unit"],
                    product_single_set=v["product_single_set"],
                )
                for k, v in cart_products.items()
            }
            if cart_products
            else False
        )

    @classmethod
    def return_json_compatible_dict_from_cart_products(
        cls, cart_products: typing.Dict = dict()
    ):
        return (
            {
                str(k): {
                    "cart_quantity_real_unit": str(v["cart_quantity_real_unit"]),
                    "product_single_set": v["product_single_set"],
                }
                for k, v in cart_products.items()
            }
            if cart_products
            else False
        )

    @classmethod
    def get_current_cart_parameters(cls, cart_products):
        products = {}
        updated_cart_quantity = Decimal(0)
        for k, v in cart_products.items():
            updated_cart_quantity += Decimal(v.cart_quantity)
            products[int(k)] = v.cart_quantity_real_unit
        return products, updated_cart_quantity
