"""
Band helper functions - used from band editions
"""
import typing

from emporium import data_containers


class BandHelpers(object):
    @classmethod
    def bands_format(cls, bands: typing.List = None) -> typing.Dict:
        """
        Build and return a dictionary of bands based on a passed list.
        """
        if bands is None:
            bands = []
        return {
            item.band_id: data_containers.BandContainer(
                band_id=item.band_id,
                seo_slug=item.seo_slug,
                band_type=None,
                name=item.name,
                url=None,
            )
            for item in bands
        }

    @classmethod
    def bands_filter_format(
        cls,
        band: str = "",
        band_list: typing.List = None,
        query_items: typing.List = None,
    ) -> typing.Dict:
        """
        Build and return a dictionary of bands based on a passed list.
        The URL parameter allows fast removal of the unwanted filter.
        For example, we want to display products that are from the bands 'queen' and 'abba'.
        The 'abba' band receives a URL with the removed 'abba' parameter (containing only 'queen').
        Also, the 'queen' receives a URL parameter containing the 'abba' band only.
        """
        if band_list is None:
            band_list = []
        if query_items is None:
            query_items = []
        return {
            item.band_id: data_containers.BandContainer(
                band_id=item.band_id,
                seo_slug=item.seo_slug,
                band_type=item.band_type,
                name=item.name,
                url=(
                    [
                        htem
                        for htem in query_items
                        if htem[0] != "band" or int(htem[1]) != item.band_id
                    ]
                    if item.band_id in band
                    else None
                ),
            )
            for item in band_list
        }

    @classmethod
    def product_band_format(cls, product_band_list):
        return {
            item.product_id: data_containers.ProductBandContainer(
                band_id=item.band_id,
                product_band_id=item.product_band_id,
                seo_slug=item.seo_slug,
                name=item.name,
            )
            for item in product_band_list
            if item.product_band_id
        }
