from datetime import datetime, timedelta
from paginate_sqlalchemy import SqlalchemyOrmPage

from sqlalchemy import and_, asc, exists, func, or_

from emporium import models, services


class ProductToSpecialService(object):
    @classmethod
    def special_rows_filter_product_id_special_list(
        cls, request, product_id, special_list
    ):
        query = request.dbsession.query(models.ProductToSpecial).filter(
            models.ProductToSpecial.product_id == product_id,
            models.ProductToSpecial.special_id.in_(special_list),
        )
        return query.all()

    @classmethod
    def special_rows_filter_product_list(cls, request, product_list):
        query = request.dbsession.query(models.ProductToSpecial).filter(
            models.ProductToSpecial.product_id.in_(product_list)
        )
        return query.all()

    @classmethod
    def special_rows_filter_product_list_date(cls, request, product_list=[]):
        query = (
            request.dbsession.query(
                # models.ProductToSpecial.order,
                models.ProductToSpecial.price,
                models.ProductToSpecial.product_id,
                models.ProductToSpecial.product_special_id,
                models.ProductToSpecial.quantity,
                models.ProductToSpecial.special_id,
                models.ProductToSpecial.storage_aware,
                models.Product.tax_id,
            )
            .join(
                models.Product,
                models.Product.product_id == models.ProductToSpecial.product_id,
            )
            .filter(
                models.Product.status == 1,
                func.date(models.ProductToSpecial.date_start) <= func.current_date(),
                or_(
                    exists().where(
                        func.date(models.ProductToSpecial.date_end)
                        >= func.current_date()
                    ),
                    func.date(models.ProductToSpecial.date_end)
                    == None,  # this is used to check NULL values
                ),
            )
            .order_by(asc(models.ProductToSpecial.price))
        )
        if product_list:
            query = query.filter(models.ProductToSpecial.product_id.in_(product_list))
        return query.all()

    @classmethod
    def special_products_filter_product_list_date_price(cls, request, product_list):
        query = (
            request.dbsession.query(models.ProductToSpecial)
            .filter(
                models.ProductToSpecial.special_id.in_([4, 2]),
                func.date(models.ProductToSpecial.date_start) <= func.current_date(),
                or_(
                    exists().where(
                        func.date(models.ProductToSpecial.date_end)
                        >= func.current_date()
                    ),
                    func.date(models.ProductToSpecial.date_end)
                    == None,  # this is used to check NULL values
                ),
            )
            .order_by(asc(models.ProductToSpecial.price))
            .filter(models.ProductToSpecial.product_id.in_(product_list))
        )
        return query.all()

    @classmethod
    def by_special_product(cls, request, product_special_id):
        query = request.dbsession.query(models.ProductToSpecial).filter(
            models.ProductToSpecial.product_special_id == product_special_id
        )
        return query.one()

    @classmethod
    def specials_active(cls, request):
        query = (
            request.dbsession.query(models.ProductToSpecial)
            .filter(
                func.date(models.ProductToSpecial.date_start) <= func.current_date()
            )
            .filter(
                or_(
                    exists().where(
                        func.date(models.ProductToSpecial.date_end)
                        >= func.current_date()
                    ),
                    func.date(models.ProductToSpecial.date_end) == None,
                )
            )
        )
        return query.all()

    @classmethod
    def current_special_subquery(cls, request):
        return (
            request.dbsession.query(
                models.ProductToSpecial.special_id,
                models.ProductToSpecial.product_id,
            )
            .filter(
                func.date(models.ProductToSpecial.date_start) <= func.current_date(),
                or_(
                    func.date(models.ProductToSpecial.date_end) == None,
                    exists().where(
                        func.date(models.ProductToSpecial.date_end)
                        >= func.current_date()
                    ),
                ),
            )
            .group_by(
                models.ProductToSpecial.product_id,
                models.ProductToSpecial.special_id,
            )
            .subquery()
        )

    @classmethod
    def current_price_subquery_special_id(cls, request):
        # subquery_price_special = cls.current_price_subquery_min_price(request)
        return (
            request.dbsession.query(
                models.ProductToSpecial.product_id,
                # models.ProductToSpecial.special_id,
                func.min(models.ProductToSpecial.price).label("price"),
            )
            .filter(
                or_(
                    func.date(models.ProductToSpecial.date_end) == None,
                    exists().where(
                        func.date(models.ProductToSpecial.date_end)
                        >= func.current_date()
                    ),
                ),
            )
            .group_by(models.ProductToSpecial.product_id)
            .subquery()
        )

    @classmethod
    def price_subquery_special_id_filter_date(cls, request, days=0):
        # subquery_price_special = cls.current_price_subquery_min_price(request)
        # now = datetime.now()
        return (
            (
                request.dbsession.query(
                    models.ProductToSpecial.product_id,
                    # models.ProductToSpecial.special_id,
                    func.min(models.ProductToSpecial.price).label("price"),
                )
            )
            .filter(
                or_(
                    func.date(models.ProductToSpecial.date_end) == None,
                    exists().where(
                        func.date(models.ProductToSpecial.date_end)
                        >= func.current_date() - timedelta(days=days)
                    ),
                ),
            )
            .group_by(models.ProductToSpecial.product_id)
            .subquery()
        )

    @classmethod
    def delete_all_product_to_special_filter_band_id(cls, request, band_id):
        if subquery := (
            request.dbsession.query(
                func.array_agg(models.ProductToSpecial.product_special_id)
            )
            .join(models.Product, models.ProductToBand)
            .filter(models.ProductToBand.band_id == band_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToSpecial).filter(
                models.ProductToSpecial.product_special_id.in_(subquery)
            )
            return query.delete(synchronize_session=False)

    @classmethod
    def delete_all_product_to_special_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductToSpecial.product_special_id)
            )
            .join(models.Product)
            .filter(models.Product.manufacturer_id == manufacturer_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToSpecial).filter(
                models.ProductToSpecial.product_special_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)

    @classmethod
    def product_to_special_list_filter_product_special_list(
        cls, request, product_special_list
    ):
        query = request.dbsession.query(models.ProductToSpecial).filter(
            models.ProductToSpecial.product_special_id.in_(product_special_list),
        )
        return query.all()

    @classmethod
    def active_special_prices(cls, request):
        query = (
            request.dbsession.query(
                models.ProductToSpecial.product_id, models.ProductToSpecial.price
            )
            .filter(
                models.ProductToSpecial.special_id.in_((2, 4)),
                # models.ProductToSpecial.default == True,
            )
            .filter(
                func.date(models.ProductToSpecial.date_start) <= func.current_date()
            )
            .filter(
                or_(
                    func.date(models.ProductToSpecial.date_end) == None,
                    exists().where(
                        func.date(models.ProductToSpecial.date_end)
                        >= func.current_date()
                    ),
                )
            )
        ).options(services.FromCache("redis-short"))
        return query.all()

    @classmethod
    def product_special_list_filter_product_id_special_list(
        cls, request, product_id, special_list
    ):
        query = request.dbsession.query(models.ProductToSpecial).filter(
            models.ProductToSpecial.product_id == product_id,
            models.ProductToSpecial.special_id.in_(special_list),
        )
        return query.all()

    @classmethod
    def product_special_list_filter_product_id(cls, request, product_id):
        query = request.dbsession.query(models.ProductToSpecial).filter(
            models.ProductToSpecial.product_id == product_id,
            # models.ProductToSpecial.special_id.in_(special_list),
        )
        return query.all()

    @classmethod
    def product_special_id_filter_monetary_product_id(cls, request, product_id):
        query = request.dbsession.query(models.ProductToSpecial).filter(
            models.ProductToSpecial.special_id.in_((2, 4)),
            models.ProductToSpecial.product_id == product_id,
        )
        return query.all()
