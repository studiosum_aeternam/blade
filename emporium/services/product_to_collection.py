from paginate_sqlalchemy import SqlalchemyOrmPage
from sqlalchemy import (
    and_,
    asc,
    desc,
    or_,
    exists,
    func,
    func,
    text,
)
from sqlalchemy.orm import Load

from emporium import models, services


class ProductToCollectionService(object):
    @classmethod
    def col_to_prod_list(cls, request, collection_id, product_list):
        query = request.dbsession.query(models.ProductToCollection).filter(
            models.ProductToCollection.product_id.in_(product_list),
            models.ProductToCollection.collection_id == collection_id,
        )
        return query.all()

    @classmethod
    def product_to_collections_filter_collection_list_product_id(
        cls, request, collection_list, product_id
    ):
        query = request.dbsession.query(models.ProductToCollection).filter(
            models.ProductToCollection.collection_id.in_(collection_list),
            models.ProductToCollection.product_id == product_id,
        )
        return query.all()

    @classmethod
    def delete_all_product_to_collection_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        if subquery := (
            request.dbsession.query(
                func.array_agg(models.ProductToCollection.product_collection_id)
            )
            .join(models.Product)
            .filter(models.Product.manufacturer_id == manufacturer_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToCollection).filter(
                models.ProductToCollection.product_collection_id.in_(subquery)
            )
            return query.delete(synchronize_session=False)

    @classmethod
    def delete_all_product_to_collection_filter_band_id(cls, request, band_id):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductToCollection.product_collection_id)
            )
            .join(models.Product, models.ProductToBand)
            .filter(models.ProductToBand.band_id == band_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToCollection).filter(
                models.ProductToCollection.product_collection_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)
