from paginate_sqlalchemy import SqlalchemyOrmPage
from sqlalchemy import asc, desc, func, or_, or_, text
from sqlalchemy.dialects.postgresql import aggregate_order_by

from ..services.caching_query import FromCache
from emporium import services
from ..models import (
    Collection,
    CollectionDescription,
    CollectionToCollection,
    Manufacturer,
    Product,
    ProductToBand,
    ProductToCategory,
    ProductToCollection,
)


class CollectionService(object):
    @classmethod
    def collection_filter_collection_id_active(cls, request, collection_id):
        return (
            request.dbsession.query(Collection)
            .filter(Collection.collection_id == collection_id, Collection.status == 1)
            .first()
        )

    @classmethod
    def product_collections_filter_collection_id(cls, request, collection_id):
        query = request.dbsession.query(ProductToCollection).filter(
            ProductToCollection.collection_id == collection_id
        )
        return query.all()

    @classmethod
    def collection_id_name_filter_product_id(cls, request, product_id, settings):
        query = (
            request.dbsession.query(
                Collection.collection_id, Collection.name, Collection.manufacturer_id
            )
            .join(ProductToCollection)
            .filter(ProductToCollection.product_id == product_id)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def collection_id_name_manufacturer_filter_product_id(
        cls, request, product_id, settings
    ):
        query = (
            request.dbsession.query(
                Collection.collection_id, Collection.name, Collection.manufacturer_id
            )
            .filter(
                Collection.status == 1, ProductToCollection.product_id == product_id
            )
            .order_by(asc(Collection.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def collection_id_name_filter_product_list(cls, request, settings, product_list):
        query = (
            request.dbsession.query(
                Collection.collection_id, Collection.name, Collection.manufacturer_id
            )
            .join(ProductToCollection)
            .filter(ProductToCollection.product_id.in_(product_list))
            .distinct()
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def collection_id_name_filter_collection_list(
        cls, request, collection_list, settings
    ):
        query = (
            request.dbsession.query(
                Collection.collection_id,
                Collection.name,
                Collection.manufacturer_id,
                func.array_agg(ProductToCollection.product_id).label("products"),
            )
            .join(ProductToCollection)
            .filter(ProductToCollection.collection_id.in_(collection_list))
            .group_by(
                Collection.collection_id, Collection.name, Collection.manufacturer_id
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def collection_id_filter_product_id(cls, request, product_id, language_id=2):
        return (
            request.dbsession.query(func.array_agg(Collection.collection_id))
            .join(
                ProductToCollection,
                ProductToCollection.collection_id == Collection.collection_id,
            )
            .filter(ProductToCollection.product_id == product_id)
            .scalar()
        )

    @classmethod
    def collection_description_filter_collection_id_language_id(
        cls, request, collection_id, language_id=2
    ):
        return (
            request.dbsession.query(CollectionDescription)
            .filter(
                CollectionDescription.collection_id == collection_id,
                CollectionDescription.language_id == language_id,
            )
            .first()
        )

    @classmethod
    def collections_related_filter_collection_id_values_id_name(
        cls, request, collection_id, language_id
    ):
        subquery = (
            request.dbsession.query(
                text(
                    "array_agg(DISTINCT collection_to_collection.collection1_id) || array_agg(DISTINCT collection_to_collection.collection2_id)"
                )
            )
            .filter(
                or_(
                    CollectionToCollection.collection1_id == collection_id,
                    CollectionToCollection.collection2_id == collection_id,
                )
            )
            .scalar()
        )

        return (
            request.dbsession.query(Collection.collection_id, Collection.name)
            .filter(Collection.collection_id.in_(subquery or {}))
            .all()
        )

    @classmethod
    def collection_array_filter_colection_list(cls, request, collection_list, settings):
        query = (
            request.dbsession.query(
                func.array_agg(CollectionToCollection.collection1_id),
                func.array_agg(CollectionToCollection.collection2_id),
            )
            .filter(
                or_(
                    CollectionToCollection.collection1_id.in_(collection_list),
                    CollectionToCollection.collection2_id.in_(collection_list),
                )
            )
            .distinct()
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.scalar()

    @classmethod
    def collections_common(cls, request, settings):
        query = (
            request.dbsession.query(
                Collection.collection_id, Collection.name, Collection.manufacturer_id
            )
            .filter(Collection.status == 1)
            .order_by(asc(Collection.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def collection_id_name_filter_category(cls, request, category_id):
        query = (
            request.dbsession.query(Collection.collection_id, Collection.name)
            .outerjoin(
                ProductToCollection,
                ProductToCollection.collection_id == Collection.collection_id,
            )
            .join(Product, Product.product_id == ProductToCollection.product_id)
            .join(ProductToCategory, ProductToCategory.product_id == Product.product_id)
            .filter(Collection.status == 1)
            .filter(ProductToCategory.category_id == category_id)
            .order_by(asc(Collection.name))
            .group_by(Collection.collection_id)
            .options(FromCache("redis-eternal"))
        )
        return query.all()

    @classmethod
    def collections_paginator_filter_dynamic(
        cls, request, page, number_collections, sort_method, searchstring, manufacturer
    ):
        query = request.dbsession.query(
            Collection.collection_id,
            Collection.manufacturer_id,
            Collection.status,
            CollectionDescription.name,
        ).join(CollectionDescription)
        if manufacturer:
            query = query.filter(Collection.manufacturer_id.in_(manufacturer))
        if searchstring:
            query = query.filter(CollectionDescription.name.ilike(searchstring))
        if sort_method == "name_asc":
            query = query.order_by(asc(CollectionDescription.name))
        elif sort_method == "name_desc":
            query = query.order_by(desc(CollectionDescription.name))
        else:
            query = query.order_by(asc(CollectionDescription.name))
        query_params = request.GET.mixed()

        def url_maker(link_page):
            # replace page param with values generated by paginator
            query_params["page"] = link_page
            return request.current_route_url(_query=query_params)

        return SqlalchemyOrmPage(
            query, page, items_per_page=number_collections, url_maker=url_maker
        )

    @classmethod
    def collection_search(self, request, searchstring):
        return (
            request.dbsession.query(
                Collection.collection_id,
                Collection.name,
                Manufacturer.name.label("manufacturer"),
            )
            .order_by(asc(Collection.name))
            .join(
                Manufacturer, Manufacturer.manufacturer_id == Collection.manufacturer_id
            )
            .filter(
                or_(
                    Collection.name.ilike(searchstring),
                    func.concat(Collection.name, " ", Collection.name).ilike(
                        searchstring
                    ),
                ),
                Collection.status != "0",
            )
            .limit(16)
        )

    @classmethod
    def collection_id_name_manufacturer_id_filter_searchstring_term(
        self, request, searchstring, fragment
    ):
        query = (
            request.dbsession.query(
                Collection.collection_id,
                Collection.manufacturer_id,
                CollectionDescription.name,
                Manufacturer.name.label("manufacturer"),
            )
            .join(CollectionDescription)
            .join(
                Manufacturer, Manufacturer.manufacturer_id == Collection.manufacturer_id
            )
            .order_by(asc(CollectionDescription.name))
        )
        temp_list = list(fragment.split())
        query = query.filter(
            or_(
                *[CollectionDescription.name.ilike(y) for y in temp_list],
                CollectionDescription.name.ilike(searchstring),
                func.concat(
                    CollectionDescription.name, " ", CollectionDescription.name
                ).ilike(searchstring),
            ),
            Collection.status != "0",
        )
        return query.all()

    @classmethod
    def collections_id_name_filter_dynamic(
        cls,
        request,
        filter_params,
        settings,
    ):
        subquery = cls.subquery_collections_ids_filter_dynamic(
            request,
            filter_params,
            settings,
        )
        query = (
            request.dbsession.query(Collection.collection_id, Collection.name)
            .filter(Collection.status == 1)
            .filter(Collection.collection_id.in_(subquery))
            .order_by(asc(Collection.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def subquery_collections_ids_filter_dynamic(cls, request, filter_params, settings):
        subquery = request.dbsession.query(
            ProductToCollection.collection_id.label("collection_id")
        )
        if any(
            (
                filter_params.attributes,
                filter_params.availability,
                filter_params.band,
                filter_params.dimensions,
                filter_params.manufacturer,
                filter_params.price_max,
                filter_params.price_min,
                filter_params.searchstring,
                filter_params.status,
            )
        ):
            subquery = subquery.join(
                Product, Product.product_id == ProductToCollection.product_id
            )
        subquery = services.ProductService.product_query_filter_manufacturer(
            filter_params.manufacturer, subquery
        )
        subquery = services.ProductService.product_query_filter_dimensions(
            filter_params.dimensions, subquery
        )
        subquery = services.ProductService.product_query_filter_searchstring(
            filter_params.searchstring, subquery
        )
        subquery = services.ProductService.product_query_filter_price(
            filter_params, subquery
        )
        if filter_params.collection:
            collection_filter = (
                ProductToCollection.collection_id.notin_(filter_params.collection)
                if filter_params.collection == 999
                else ProductToCollection.collection_id.in_(filter_params.collection)
            )
            subquery = subquery.filter(collection_filter)
        if filter_params.category_id:
            subquery = subquery.join(
                ProductToCategory,
                ProductToCategory.product_id == ProductToCollection.product_id,
            ).filter(ProductToCategory.category_id == filter_params.category_id)
        if filter_params.band:
            subquery = subquery.join(
                ProductToBand,
                ProductToBand.product_id == ProductToCollection.product_id,
            ).filter(ProductToBand.band_id.in_(filter_params.band))
        subquery = services.AttributeService.attribute_query_filter_attributes(
            request, filter_params.attributes, settings, subquery
        )
        subquery = services.ProductService.product_query_filter_availability(
            filter_params.availability, subquery
        )
        subquery = services.ProductService.product_description_query_filter_description_searchstring(
            filter_params, subquery
        )
        # subquery = services.ProductService.product_query_filter_status(
        #     filter_params.status, subquery
        # )
        subquery = services.Cache.cache_query(settings["cache"], subquery)
        return subquery.distinct().scalar_subquery()

    @classmethod
    def disabled_collections(self, request):
        query = (
            request.dbsession.query(func.array_agg(Collection.collection_id)).filter(
                Collection.status == 0
            )
        ).options(FromCache("redis-eternal"))
        return query.scalar()

    @classmethod
    def delete_collection_filter_manufacturer_id(self, request, manufacturer_id):
        query = request.dbsession.query(Collection).filter(
            Collection.manufacturer_id == manufacturer_id
        )
        return query.delete(synchronize_session=False)

    @classmethod
    def delete_collection_description_filter_manufacturer_id(
        self, request, manufacturer_id
    ):
        subquery = (
            request.dbsession.query(
                func.array_agg(CollectionDescription.collection_description_id)
            )
            .join(Collection)
            .filter(Collection.manufacturer_id == manufacturer_id)
            .scalar()
        )
        if subquery:
            query = request.dbsession.query(CollectionDescription).filter(
                CollectionDescription.collection_description_id.in_(subquery)
            )
            return query.delete(synchronize_session=False)
