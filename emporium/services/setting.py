from sqlalchemy import asc

from ..models import Setting, SettingTemplate
from emporium import services


class SettingsService(object):
    @classmethod
    def all(cls, request):
        query = request.dbsession.query(Setting).order_by(
            asc(Setting.setting_type), asc(Setting.key)
        )
        return query.all()

    @classmethod
    def all_list(cls, request):
        query = request.dbsession.query(Setting).order_by(
            asc(Setting.setting_type), asc(Setting.key)
        )
        return query.all()

    @classmethod
    def main(cls, request):
        return (
            request.dbsession.query(Setting)
            .filter(Setting.setting_type in (0, 2))
            .order_by(asc(Setting.setting_type), asc(Setting.key))
            .all()
        )

    @classmethod
    def single_setting_filter_key(cls, request, key, settings):
        query = request.dbsession.query(Setting).filter(Setting.key == key)
        query = services.Cache.cache_query(settings["cache"], query)
        return query.one()

    @classmethod
    def admin_settings(cls, request):
        query = request.dbsession.query(Setting)
        return query.all()

    @classmethod
    def by_id(cls, request, setting_id):
        return (
            request.dbsession.query(Setting)
            .filter(Setting.setting_id == setting_id)
            .one()
        )

    @classmethod
    def by_key(cls, request, setting_type, key):
        return (
            request.dbsession.query(Setting)
            .filter(Setting.key == key, Setting.setting_type == setting_type)
            .one()
        )

    @classmethod
    def admin_setting_templates(cls, request):
        query = request.dbsession.query(SettingTemplate)
        return query.all()

    @classmethod
    def setting_templates_filter_active(cls, request):
        query = (
            request.dbsession.query(
                SettingTemplate.setting_template_id,
                SettingTemplate.description,
                SettingTemplate.name,
                SettingTemplate.template_json,
            )
            .filter(SettingTemplate.status == 1)
            .order_by(asc(SettingTemplate.order), asc(SettingTemplate.name))
        )
        return query.all()

    @classmethod
    def admin_setting_template_filter_id(cls, request, setting_template_id):
        query = request.dbsession.query(
            SettingTemplate.setting_template_id,
            SettingTemplate.description,
            SettingTemplate.name,
            SettingTemplate.order,
            SettingTemplate.status,
            SettingTemplate.template_json,
            SettingTemplate.template_type,
        ).filter(SettingTemplate.setting_template_id == setting_template_id)
        return query.one()

    @classmethod
    def setting_template_full_filter_by_id(cls, request, setting_template_id):
        query = request.dbsession.query(SettingTemplate).filter(
            SettingTemplate.setting_template_id == setting_template_id
        )
        return query.one()
