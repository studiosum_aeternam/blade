from ..models import Customer, CustomerReset


class CustomerService(object):
    @classmethod
    def by_id(cls, request, _id):
        query = request.dbsession.query(Customer)
        return query.get(_id)

    @classmethod
    def customer_single_filter_mail_type(cls, request, mail, customer_type):
        query = request.dbsession.query(Customer).filter(
            Customer.mail == mail, Customer.customer_type == customer_type
        )
        return query.first()

    @classmethod
    def customer_id_request_expirations_filter_request_id(cls, request, request_uuid):
        return (
            request.dbsession.query(
                CustomerReset.customer_id,
                CustomerReset.request_uuid,
                CustomerReset.expiration_time,
            )
            .filter(CustomerReset.request_uuid == request_uuid)
            .one()
        )

    @classmethod
    def customer_filter_request_id(cls, request, request_uuid):
        return (
            request.dbsession.query(Customer)
            .join(CustomerReset)
            .filter(CustomerReset.request_uuid == request_uuid)
            .one()
        )
