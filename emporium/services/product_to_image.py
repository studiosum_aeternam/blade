from paginate_sqlalchemy import SqlalchemyOrmPage
from sqlalchemy import (
    and_,
    asc,
    desc,
    or_,
    exists,
    func,
    func,
    text,
)
from sqlalchemy.orm import Load

from emporium import models, services


class ProductToImageService(object):
    @classmethod
    def product_to_image_filter_image_id(cls, request, product_image_id):
        query = request.dbsession.query(models.ProductToImage).filter(
            models.ProductToImage.product_image_id == product_image_id
        )
        return query.one()

    @classmethod
    def product_to_image_list_filter_image_list(cls, request, image_list):
        query = request.dbsession.query(models.ProductToImage).filter(
            models.ProductToImage.product_image_id.in_(image_list)
        )
        return query.all()

    @classmethod
    def additional_images_filter_product_id(cls, request, product_id, settings):
        query = (
            request.dbsession.query(models.ProductToImage)
            .filter(models.ProductToImage.product_id == product_id)
            .order_by(asc(models.ProductToImage.sort_order))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def delete_all_product_to_image_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        if subquery := (
            request.dbsession.query(
                func.array_agg(models.ProductToImage.product_image_id)
            )
            .join(models.Product)
            .filter(models.Product.manufacturer_id == manufacturer_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToImage).filter(
                models.ProductToImage.product_image_id.in_(subquery)
            )
            return query.delete(synchronize_session=False)

    @classmethod
    def delete_all_product_to_image_filter_band_id(cls, request, band_id):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductToImage.product_image_id)
            )
            .join(models.Product, models.ProductToBand)
            .filter(models.ProductToBand.band_id == band_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToImage).filter(
                models.ProductToImage.product_image_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)
