from paginate_sqlalchemy import SqlalchemyOrmPage

import sqlalchemy as sa
from emporium import models

from emporium import services


class AuthorService(object):
    @classmethod
    def authors_filter_dynamic(
        cls, request, page=1, searchstring=None, items_per_page=36
    ):
        query = request.dbsession.query(models.Author).order_by(
            sa.asc(models.Author.last_name), sa.asc(models.Author.first_name)
        )
        if searchstring:
            query = query.filter(
                sa.or_(
                    models.Author.first_name.ilike(searchstring),
                    models.Author.last_name.ilike(searchstring),
                    models.Author.callsign.ilike(searchstring),
                    sa.func.concat(
                        models.Author.first_name, " ", models.Author.last_name
                    ).ilike(searchstring),
                ),
            )
        query_params = request.GET.mixed()

        def url_maker(link_page):
            # replace page param with values generated by paginator
            query_params["page"] = link_page
            return request.current_route_url(_query=query_params)

        return SqlalchemyOrmPage(
            query, page, items_per_page=items_per_page, url_maker=url_maker
        )

    @classmethod
    def author_full_filter_by_author_id(cls, request, author_id):
        query = request.dbsession.query(models.Author).filter(
            models.Author.author_id == author_id
        )
        return query.first()

    @classmethod
    def author_term_search(cls, request, searchstring):
        query = (
            request.dbsession.query(
                models.Author.author_id,
                models.Author.first_name,
                models.Author.last_name,
                models.Author.callsign,
            )
            .order_by(sa.asc(models.Author.first_name), sa.asc(models.Author.last_name))
            .filter(
                sa.or_(
                    models.Author.first_name.ilike(searchstring),
                    models.Author.last_name.ilike(searchstring),
                    models.Author.callsign.ilike(searchstring),
                    sa.func.concat(
                        models.Author.first_name, " ", models.Author.last_name
                    ).ilike(searchstring),
                ),
                models.Author.status != "0",
            )
        )
        return query.all()

    @classmethod
    def author_role(cls, request):
        return (
            request.dbsession.query(models.AuthorRole)
            .order_by(sa.asc(models.AuthorRole.name))
            .all()
        )

