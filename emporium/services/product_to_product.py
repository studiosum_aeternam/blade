from paginate_sqlalchemy import SqlalchemyOrmPage
from sqlalchemy import (
    and_,
    asc,
    desc,
    or_,
    exists,
    func,
    func,
    text,
)
from sqlalchemy.orm import Load

from emporium import models, services


class ProductToProductService(object):
    @classmethod
    def product_component_list_filter_product_id_relation_type(
        cls, request, product_id, relation_type
    ):
        query = request.dbsession.query(models.ProductToProduct).filter(
            # or_(
            models.ProductToProduct.product1_id == product_id,
            # models.ProductToProduct.product2_id == product_id,
            # ),
            models.ProductToProduct.product_relation_type == relation_type,
        )
        return query.all()

    @classmethod
    def product_to_component_list_filter_product_component_list_relation_type(
        cls, request, product_component_list, relation_type
    ):
        query = request.dbsession.query(models.ProductToProduct).filter(
            models.ProductToProduct.product_component_id.in_(product_component_list),
            models.ProductToProduct.product_relation_type.in_(relation_type),
        )
        return query.all()

    @classmethod
    def product_to_product_filter_product_to_product_id(
        cls, request, product_component_id
    ):
        return (
            request.dbsession.query(models.ProductToProduct).filter(
                models.ProductToProduct.product_component_id == product_component_id
            )
        ).one()

    @classmethod
    def delete_all_product_to_product_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        if subquery := (
            request.dbsession.query(
                func.array_agg(models.ProductToProduct.product_component_id)
            )
            .join(
                models.Product,
                # or_(
                models.ProductToProduct.product2_id == models.Product.product_id,
                # models.ProductToProduct.product1_id == models.Product.product_id,
                # ),
            )
            .filter(models.Product.manufacturer_id == manufacturer_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToProduct).filter(
                models.ProductToProduct.product_component_id.in_(subquery)
            )
            return query.delete(synchronize_session=False)

    @classmethod
    def delete_all_product_to_product_filter_band_id(cls, request, band_id):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductToProduct.product_component_id)
            )
            .join(
                models.Product,
                # or_(
                models.ProductToProduct.product2_id == models.Product.product_id,
                # models.ProductToProduct.product1_id == models.Product.product_id,
                # ),
            )
            .join(models.ProductToBand)
            .filter(models.ProductToBand.band_id == band_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToProduct).filter(
                models.ProductToProduct.product_component_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)
