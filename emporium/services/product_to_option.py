from paginate_sqlalchemy import SqlalchemyOrmPage
from sqlalchemy import (
    and_,
    asc,
    desc,
    or_,
    exists,
    func,
    func,
    text,
)
from sqlalchemy.orm import Load

from emporium import models, services


class ProductToOptionService(object):
    @classmethod
    def product_options_filter_product_id(cls, request, option_id):
        query = request.dbsession.query(models.ProductToOption).filter(
            models.ProductToOption.product_option_id == option_id
        )
        return query.all()

    @classmethod
    def delete_all_product_to_option_filter_band_id(cls, request, band_id):
        if subquery := (
            request.dbsession.query(
                func.array_agg(models.ProductToOption.product_option_id)
            )
            .join(models.Product, models.ProductToBand)
            .filter(models.ProductToBand.band_id == band_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToOption).filter(
                models.ProductToOption.product_option_id.in_(subquery)
            )
            return query.delete(synchronize_session=False)

    @classmethod
    def delete_all_product_to_option_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductToOption.product_option_id)
            )
            .join(models.Product)
            .filter(models.Product.manufacturer_id == manufacturer_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToOption).filter(
                models.ProductToOption.product_option_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)
