import sqlalchemy as sa

from emporium import models, services


class TransportService(object):
    @classmethod
    def transport_list_filter_active(cls, request, settings):
        query = (
            request.dbsession.query(models.Transport)
            .filter(models.Transport.status == True)
            .order_by(sa.desc(models.Transport.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def transport_description_excerpt_name_status_filter_id(
        cls, request, transport_id, settings
    ):
        query = (
            request.dbsession.query(models.Transport)
            .filter(models.Transport.transport_id == transport_id)
            .order_by(sa.desc(models.Transport.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.one()

    @classmethod
    def transport_id_name_exclude_current_transport(
        cls, request, module_name, settings
    ):
        query = (
            request.dbsession.query(
                models.Transport.transport_id,
                models.Transport.name,
            )
            .filter(models.Transport.module_name != module_name)
            .order_by(sa.desc(models.Transport.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def transport_all_objects(cls, request, settings):
        query = request.dbsession.query(models.Transport).order_by(
            sa.asc(models.Transport.name)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def transport_description_excerpt_name_status_visibility_filter_status(
        cls,
        request,
        settings,
        status=True,
    ):
        query = (
            request.dbsession.query(
                #  models.TransportSlot.special_method,
                models.Transport.description,
                models.Transport.excerpt,
                models.Transport.module_name,
                models.Transport.module_settings,
                models.Transport.name,
                models.Transport.description,
                models.Transport.transport_id,
                models.Transport.transport_method,
            )
            .order_by(sa.asc(models.Transport.name))
            .filter(models.Transport.status == status)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def transport_slots_full_filter_transport_id(
        cls,
        request,
        settings,
        transport_id=None,
    ):
        """
        Get transport slots for single method.
        """
        query = (
            request.dbsession.query(models.TransportSlot)
            .order_by(sa.asc(models.TransportSlot.order))
            .filter(
                models.TransportSlot.transport_id == transport_id,
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def transport_slots_full_filter_transport_list(
        cls, request, settings, transport_id_list=[]
    ):
        """
        Get transport slots for single method.
        """
        query = (
            request.dbsession.query(models.TransportSlot)
            .order_by(sa.asc(models.TransportSlot.order))
            .filter(
                models.TransportSlot.transport_slot_id.in_(transport_id_list),
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def transport_data_with_slots_special(
        cls, request, product_id, settings, status=True
    ):
        query = (
            request.dbsession.query(
                #  models.TransportSlot.special_method,
                models.Transport.description,
                models.Transport.excerpt,
                models.Transport.module_name,
                models.Transport.module_settings,
                models.Transport.name,
                models.Transport.status,
                models.Transport.transport_id,
                models.ProductToTransport.local_settings,
            )
            .join(
                models.TransportSlot,
                models.TransportSlot.transport_slot_id
                == models.ProductToTransport.transport_slot_id,
            )
            .join(
                models.Transport,
                models.TransportSlot.transport_id == models.Transport.transport_id,
            )
            .filter(
                models.ProductToTransport.product_id == product_id,
                models.Transport.status == status,
                models.TransportSlot.special_method == False,
            )
            .order_by(sa.asc(models.Transport.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def transport_slots(cls, request, settings, status=True):
        query = (
            request.dbsession.query(
                models.TransportSlot.applicability.label("slot_applicability"),
                models.TransportSlot.special_method.label("slot_special_method"),
                models.TransportSlot.description.label("slot_description"),
                models.TransportSlot.excerpt.label("slot_excerpt"),
                models.TransportSlot.name.label("slot_name"),
                models.TransportSlot.order.label("slot_order"),
                # models.TransportSlot.slot_name.label("slot_slot_name"),
                models.TransportSlot.transport_id,
                models.TransportSlot.transport_slot_id,
                models.TransportSlot.module_settings.label("slot_settings"),
            )
            .join(models.Transport)
            .filter(
                models.Transport.status == status,
                models.TransportSlot.status == status,
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def transport_slots_filter_special_method(
        cls,
        request,
        settings,
        status=True,
    ):
        query = (
            request.dbsession.query(
                models.TransportSlot.applicability.label("slot_applicability"),
                models.TransportSlot.special_method.label("slot_special_method"),
                models.TransportSlot.description.label("slot_description"),
                models.TransportSlot.excerpt.label("slot_excerpt"),
                models.TransportSlot.name.label("slot_name"),
                models.TransportSlot.order.label("slot_order"),
                # models.TransportSlot.slot_name.label("slot_slot_name"),
                models.TransportSlot.transport_id,
                models.TransportSlot.transport_slot_id,
                models.TransportSlot.module_settings.label("slot_settings"),
            )
            .join(models.Transport)
            .filter(
                models.TransportSlot.special_method == True,
                models.Transport.status == status,
                models.TransportSlot.status == status,
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def transport_slot_filter_transport_slot_id(
        cls, request, transport_slot_id, settings
    ):
        query = request.dbsession.query(
            models.TransportSlot.applicability.label("slot_applicability"),
            models.TransportSlot.special_method.label("slot_special_method"),
            models.TransportSlot.description.label("slot_description"),
            models.TransportSlot.excerpt.label("slot_excerpt"),
            models.TransportSlot.name.label("slot_name"),
            models.TransportSlot.order.label("slot_order"),
            models.TransportSlot.transport_id,
            models.TransportSlot.transport_slot_id,
            models.TransportSlot.module_settings.label("slot_settings"),
        ).filter(models.TransportSlot.transport_slot_id == transport_slot_id)
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def transport_description_excerpt_name_status_visibility_filter_transport_slot_id(
        cls, request, transport_slot_id, settings
    ):
        query = (
            request.dbsession.query(
                #  models.TransportSlot.special_method,
                models.TransportSlot.transport_id,
                models.Transport.description,
                models.Transport.description,
                models.Transport.excerpt,
                models.Transport.module_name,
                models.Transport.module_settings,
                models.Transport.name,
                models.Transport.transport_method,
            )
            .join(models.Transport)
            .filter(models.TransportSlot.transport_slot_id == transport_slot_id)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()
