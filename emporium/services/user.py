from ..models import User


class UserService(object):
    @classmethod
    def by_login(cls, login, request):
        return request.dbsession.query(User).filter(User.login == login).first()

    @classmethod
    def by_id(cls, request, _id):
        return request.dbsession.query(User).get(_id)
