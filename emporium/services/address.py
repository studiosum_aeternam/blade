from ..models import Address


class AddressService(object):
    @classmethod
    def by_customer(cls, request, customer_id):
        return (
            request.dbsession.query(Address)
            .filter(Address.customer_id == customer_id)
            .all()
        )

    @classmethod
    def by_customer_first(cls, request, customer_id):
        return (
            request.dbsession.query(Address)
            .filter(Address.customer_id == customer_id)
            .first()
        )
