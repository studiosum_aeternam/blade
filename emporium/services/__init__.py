"""
Mass service import to the scope
"""
from .address import AddressService
from .attribute import AttributeService
from .author import AuthorService
from .band import BandService
from .banner import BannerService
from .block import BlockService
from .caching_query import Cache, FromCache, ORMCache
from .cart import CartService
from .category import CategoryService
from .collection import CollectionService
from .compare import CompareService
from .country import CountryService
from .customer import CustomerService
from .discount import DiscountService
from .file import FileService
from .manufacturer import ManufacturerService
from .option import OptionService
from .order import OrderService
from .page import PageService
from .page_to_tag import PageToTagService
from .payment import PaymentService
from .post import PostService
from .product import ProductService
from .product_to_attribute import ProductToAttributeService
from .product_to_author import ProductToAuthorService
from .product_to_band import ProductToBandService
from .product_to_category import ProductToCategoryService
from .product_to_collection import ProductToCollectionService
from .product_to_file import ProductToFileService
from .product_to_image import ProductToImageService
from .product_to_option import ProductToOptionService
from .product_to_product import ProductToProductService
from .product_to_relation import ProductToRelationService
from .product_to_special import ProductToSpecialService
from .product_to_tag import ProductToTagService
from .product_to_transport import ProductToTransportService
from .product_to_xml import ProductToXMLService
from .setting import SettingsService
from .special import SpecialService
from .status import StatusService
from .storage import StorageService
from .tag import TagService
from .tax import TaxService
from .transport import TransportService
from .user import UserService
