"""
Compare services - view current, filter for logged and non-logged customer and merge method for authorized customers
"""
from datetime import datetime

from ..models import Compare


class CompareService(object):
    @classmethod
    def compare_filter_customer_id(cls, request, customer_id):
        """
        Returns compare object for logged (current) customer
        """
        return (
            request.dbsession.query(Compare)
            .filter(Compare.customer_id == customer_id)
            .first()
        )

    @classmethod
    def compare_filter_compare_id(cls, request, compare_id):
        """
        Returns compare object for non-logged customer using session variable
        """
        return (
            request.dbsession.query(Compare)
            .filter(Compare.compare_id == compare_id)
            .first()
        )

    @classmethod
    def compare_content(cls, request):
        """
        Returns current compare object for logged and non-logged customers
        """
        current_compare = None
        if request.authenticated_userid:
            current_compare = CompareService.compare_filter_customer_id(
                request, request.authenticated_userid
            )
        if request.session.get("compare_id"):
            current_compare = CompareService.compare_filter_compare_id(
                request, request.session["compare_id"]
            )
        return current_compare
