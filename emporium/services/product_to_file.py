from paginate_sqlalchemy import SqlalchemyOrmPage
from sqlalchemy import (
    and_,
    asc,
    desc,
    or_,
    exists,
    func,
    func,
    text,
)
from sqlalchemy.orm import Load

from emporium import models, services


class ProductToFileService(object):
    @classmethod
    def delete_all_product_to_file_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        if subquery := (
            request.dbsession.query(
                func.array_agg(models.ProductToFile.product_file_id)
            )
            .join(models.Product)
            .filter(models.Product.manufacturer_id == manufacturer_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToFile).filter(
                models.ProductToFile.product_file_id.in_(subquery)
            )
            return query.delete(synchronize_session=False)

    @classmethod
    def delete_all_product_to_file_filter_band_id(cls, request, band_id):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductToFile.product_file_id)
            )
            .join(models.Product, models.ProductToBand)
            .filter(models.ProductToBand.band_id == band_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToFile).filter(
                models.ProductToFile.product_file_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)

    @classmethod
    def product_to_file_list_filter_file_list(cls, request, file_list):
        query = (
            request.dbsession.query(models.ProductToFile)
            .filter(models.ProductToFile.product_file_id.in_(file_list))
            .order_by(asc(models.ProductToFile.sort_order))
        )
        return query.all()

    @classmethod
    def product_file_list_filter_product_id(cls, request, product_id, settings):
        query = request.dbsession.query(models.ProductToFile).filter(
            models.ProductToFile.product_id == product_id
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_file_list_filter_permission_product_id(
        cls, request, permission_id, product_id, settings
    ):
        query = (
            request.dbsession.query(
                models.File.file_path,
                models.File.file_name,
                models.File.file_type_id,
            )
            .join(models.ProductToFile)
            .filter(
                models.File.file_permission_id == permission_id,
                models.ProductToFile.product_id == product_id,
            )
            .order_by(asc(models.File.file_name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()
