import sqlalchemy as sa
from emporium import models, services


class ProductToTransportService(object):
    @classmethod
    def product_transport_slot_settings_filter_product_list(
        cls, request, settings, product_id_list=None, status=True
    ):
        """
        Get default transport methods and special methods from previously queried list.
        """
        # if product_id_list is None:
        # product_id_list = []
        query = (
            request.dbsession.query(
                models.ProductToTransport.local_settings,
                models.ProductToTransport.product_transport_id,
                models.ProductToTransport.transport_slot_id,
                models.ProductToTransport.product_id,
                models.TransportSlot.transport_id,
                models.TransportSlot.description.label("module_description"),
                models.Transport.module_name,
                models.TransportSlot.module_settings,
                models.Transport.name,
            )
            .join(
                models.TransportSlot,
                models.TransportSlot.transport_slot_id
                == models.ProductToTransport.transport_slot_id,
            )
            .join(
                models.Transport,
                models.Transport.transport_id == models.TransportSlot.transport_id,
            )
            .filter(
                # models.TransportSlot.status == True,
                models.ProductToTransport.product_id.in_(product_id_list)
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_transport_slot_settings_filter_product_id(
        cls, request, product_id, settings, status=True
    ):
        """
        Get default transport methods and special methods from previously queried list.
        """
        query = request.dbsession.query(models.ProductToTransport).filter(
            models.ProductToTransport.product_id == product_id
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_transport_id_filter_product_list(cls, request, settings, product_list):
        query = request.dbsession.query(
            models.ProductToTransport.product_id,
            models.ProductToTransport.transport_slot_id,
        ).filter(
            models.ProductToTransport.product_id.in_(product_list),
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()
