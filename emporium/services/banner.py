from sqlalchemy import desc, exists, func, or_

from ..models import Banner
from emporium import services


class BannerService(object):
    @classmethod
    def all(cls, request):
        query = request.dbsession.query(
            Banner.banner_id,
            Banner.image,
            Banner.caption,
            Banner.caption_secondary,
            Banner.status,
        ).order_by(desc(Banner.caption_secondary))
        return query.all()

    @classmethod
    def banner_filter_id(cls, request, _id):
        query = request.dbsession.query(Banner)
        return query.get(_id)

    @classmethod
    def banners_values_captions_urls_filter_date_page(cls, request, page, settings):
        query = request.dbsession.query(
            Banner.image,
            Banner.caption,
            Banner.caption_secondary,
            Banner.url,
            Banner.url_secondary,
        ).filter(
            Banner.page == page,
            or_(
                func.date(Banner.date_start) <= func.current_date(),
                Banner.date_start == None,  # this is used to check NULL values
            ),
            or_(
                exists().where(func.date(Banner.date_end) >= func.current_date()),
                func.date(Banner.date_end) == None,  # this is used to check NULL values
            ),
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()
