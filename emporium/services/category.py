from sqlalchemy import and_, asc, desc, or_, func
from paginate_sqlalchemy import SqlalchemyOrmPage


from emporium import models, services


class CategoryService(object):
    @classmethod
    def categories_categories_descriptions(
        cls, request, settings, language_id=2, status=1
    ):
        query = (
            request.dbsession.query(
                models.Category.category_id,
                models.Category.name,
                models.Category.parent_id,
            )
            .filter(models.Category.status == status)
            .order_by(asc(models.Category.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def category_id_filter_status(cls, request, status, settings):
        query = request.dbsession.query(
            func.array_agg(models.Category.category_id)
        ).filter(models.Category.status == status)
        query = services.Cache.cache_query(settings["cache"], query)
        return query.scalar()

    @classmethod
    def category_filter_id(cls, request, category_id):
        return (
            request.dbsession.query(models.Category)
            .filter(models.Category.category_id == category_id)
            .options(services.caching_query.FromCache("redis-eternal"))
            .one()
        )

    @classmethod
    def category_id_name_parent_description_meta_seo_h1_title_filter_id(
        cls,
        request,
        category_id,
        settings,
        language_id=2,
        status=1,
    ):
        query = (
            request.dbsession.query(
                models.Category.category_id,
                models.Category.name,
                models.Category.parent_id,
                models.CategoryMetaTreeDescriptions.description,
                models.CategoryMetaTreeDescriptions.description_long,
                models.CategoryMetaTreeDescriptions.meta_description,
                models.CategoryMetaTreeDescriptions.seo_slug,
                models.CategoryMetaTreeDescriptions.u_h1,
                models.CategoryMetaTreeDescriptions.u_title,
            )
            .join(models.CategoryMetaTreeDescriptions)
            .filter(
                models.CategoryMetaTreeDescriptions.manufacturer_id == None,
                models.CategoryMetaTreeDescriptions.collection_id == None,
                models.CategoryMetaTreeDescriptions.band_id == None,
            )
            .filter(
                models.Category.category_id == category_id,
                models.Category.status == status,
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.first()

    @classmethod
    def category_description_filter_id(
        cls, request, category_id, language_id, settings
    ):
        query = request.dbsession.query(models.CategoryMetaTreeDescriptions).filter(
            models.CategoryMetaTreeDescriptions.category_id == category_id,
            models.CategoryMetaTreeDescriptions.collection_id == None,
            models.CategoryMetaTreeDescriptions.manufacturer_id == None,
            models.CategoryMetaTreeDescriptions.band_id == None,
            models.CategoryMetaTreeDescriptions.language_id == 2,
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.one()

    @classmethod
    def category_description_all(cls, request):
        return request.dbsession.query(models.CategoryDescription).all()

    @classmethod
    def categories_array_filter_product_id(cls, request, product_id, settings):
        query = (
            request.dbsession.query(
                models.Category.category_id,
                models.Category.name,
                models.Category.parent_id,
                models.Category.placeholder,
                models.CategoryMetaTreeDescriptions.seo_slug,
            )
            .join(models.CategoryMetaTreeDescriptions)
            .filter(
                models.CategoryMetaTreeDescriptions.manufacturer_id == None,
                models.CategoryMetaTreeDescriptions.collection_id == None,
            )
            .join(
                models.ProductToCategory,
                models.ProductToCategory.category_id == models.Category.category_id,
            )
            .filter(models.ProductToCategory.product_id == product_id)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def category_id_name_parent_description_meta_seo_h1_title_filter_manufacturer(
        cls,
        request,
        manufacturer,
        settings,
        collection=False,
    ):
        subquery = (
            request.dbsession.query(models.ProductToCategory.category_id)
            .join(models.Product)
            .filter(models.Product.manufacturer_id == manufacturer)
            .scalar_subquery()
        )
        query = (
            request.dbsession.query(
                models.Category.category_id,
                models.Category.name,
                models.Category.parent_id,
                models.CategoryMetaTreeDescriptions.description,
                models.CategoryMetaTreeDescriptions.description_long,
                models.CategoryMetaTreeDescriptions.meta_description,
                models.CategoryMetaTreeDescriptions.u_title,
                models.CategoryMetaTreeDescriptions.u_h1,
            )
            .join(models.CategoryMetaTreeDescriptions)
            .filter(
                models.Category.status == 1,
                models.Category.category_id.in_(subquery),
                models.Category.placeholder == None,
                models.CategoryMetaTreeDescriptions.manufacturer_id == manufacturer,
            )
            .order_by(asc(models.Category.name))
        )
        if collection:
            query = query.filter(
                models.CategoryMetaTreeDescriptions.collection_id == collection
            )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def category_id_name_parent_description_meta_seo_h1_title_filter_main_category(
        cls, request, settings
    ):
        query = request.dbsession.query(
            func.array_agg(
                models.CategoryMetaTreeDescriptions.category_meta_tree_description_id
            )
        ).filter(
            ~(models.CategoryMetaTreeDescriptions.category_id == None),
            models.CategoryMetaTreeDescriptions.band_id == None,
            models.CategoryMetaTreeDescriptions.collection_id == None,
            models.CategoryMetaTreeDescriptions.manufacturer_id == None,
        )
        return query.scalar()

    @classmethod
    def category_id_name_parent_description_meta_seo_h1_title_filter_band(
        cls, request, band_id, main_category_meta_id, settings
    ):
        subquery = (
            request.dbsession.query(models.ProductToCategory.category_id)
            .join(
                models.ProductToBand,
                models.ProductToBand.product_id == models.ProductToCategory.product_id,
            )
            .group_by(models.ProductToCategory.category_id)
            .filter(models.ProductToBand.band_id == band_id)
            .scalar_subquery()
        )
        query = (
            request.dbsession.query(
                models.Category.category_id,
                models.Category.name,
                models.Category.parent_id,
                models.CategoryMetaTreeDescriptions.description,
                models.CategoryMetaTreeDescriptions.description_long,
                models.CategoryMetaTreeDescriptions.meta_description,
                models.CategoryMetaTreeDescriptions.u_title,
                models.CategoryMetaTreeDescriptions.u_h1,
                models.CategoryMetaTreeDescriptions.category_meta_tree_description_id,
            )
            .outerjoin(models.CategoryMetaTreeDescriptions)
            .filter(
                models.Category.status == 1,
                models.Category.category_id.in_(subquery),
                models.Category.placeholder == False,
            )
            .order_by(asc(models.Category.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def category_id_name_parent_filter_manufacturer_exclude_category_list(
        cls, request, manufacturer, categories_id_list, settings, collection=None
    ):
        subquery = (
            request.dbsession.query(models.ProductToCategory.category_id)
            .join(models.Product)
            .filter(models.Product.manufacturer_id == manufacturer)
            .scalar_subquery()
        )
        query = (
            request.dbsession.query(
                models.Category.category_id,
                models.Category.name,
                models.Category.parent_id,
            )
            .filter(
                models.Category.status == 1,
                models.Category.category_id.in_(subquery),
                models.Category.placeholder == None,
                ~models.Category.category_id.in_(categories_id_list),
            )
            .order_by(asc(models.Category.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def category_id_name_parent_filter_band_exclude_category_list(
        cls, request, band_id, categories_id_list, settings
    ):
        subquery = (
            request.dbsession.query(models.ProductToCategory.category_id)
            .join(
                models.ProductToBand,
                models.ProductToBand.product_id == models.ProductToCategory.product_id,
            )
            # .group_by(models.ProductToCategory.category_id)
            .filter(models.ProductToBand.band_id == band_id)
            .scalar_subquery()
        )
        query = (
            request.dbsession.query(
                models.Category.category_id,
                models.Category.name,
                models.Category.parent_id,
            )
            .filter(
                models.Category.status == 1,
                models.Category.category_id.in_(subquery),
                models.Category.placeholder == None,
                ~models.Category.category_id.in_(categories_id_list),
            )
            .order_by(asc(models.Category.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def search(cls, request, searchstring, fragment):
        query = request.dbsession.query(
            models.Category, models.CategoryMetaTreeDescriptions
        ).join(models.CategoryMetaTreeDescription)
        query = query.filter(
            or_(
                *[models.Category.name.ilike(y) for y in fragment.split()],
                models.Category.name.ilike(searchstring),
                func.concat(models.Category.name, " ", models.Category.name).ilike(
                    searchstring
                ),
            ),
            models.Category.status != "0",
        )
        return query.all()

    @classmethod
    def category_id_image_name_status_paginated_sorted(
        cls, request, page, number_categories, sort_method
    ):
        query = request.dbsession.query(
            models.Category.category_id,
            models.Category.name,
            models.Category.status,
            models.Category.image,
        )
        if sort_method == "name_asc":
            query = query.order_by(asc(models.Category.name))
        elif sort_method == "name_dsc":
            query = query.order_by(desc(models.Category.name))
        elif sort_method == "sort_asc":
            query = query.order_by(asc(models.Category.sort))
        elif sort_method == "sort_dsc":
            query = query.order_by(desc(models.Category.sort))
        else:
            query = query.order_by(asc(models.Category.name))
        query_params = request.GET.mixed()

        def url_maker(link_page):
            # replace page param with values generated by paginator
            query_params["page"] = link_page
            return request.current_route_url(_query=query_params)

        return SqlalchemyOrmPage(
            query, page, items_per_page=number_categories, url_maker=url_maker
        )

    @classmethod
    def category_meta_tree(
        cls, request, page, number_elements, category_id, manufacturer_id, collection_id
    ):
        query = request.dbsession.query(
            models.CategoryMetaTreeDescriptions.u_title,
            models.CategoryMetaTreeDescriptions.u_h1,
            models.CategoryMetaTreeDescriptions.description,
            models.CategoryMetaTreeDescriptions.description_long,
            models.Manufacturer.name.label("manufacturer"),
            models.Collection.name.label("collection"),
            models.Category.name.label("category"),
        ).outerjoin(models.Category, models.Manufacturer, models.Collection)
        if category_id:
            query = query.filter(
                models.CategoryMetaTreeDescriptions.category_id == category_id
            )
        if collection_id:
            query = query.filter(
                models.CategoryMetaTreeDescriptions.collection_id == collection_id
            )
        if manufacturer_id:
            query = query.filter(
                models.CategoryMetaTreeDescriptions.manufacturer_id == manufacturer_id
            )
        query_params = request.GET.mixed()

        def url_maker(link_page):
            # replace page param with values generated by paginator
            query_params["page"] = link_page
            return request.current_route_url(_query=query_params)

        return SqlalchemyOrmPage(
            query, page, items_per_page=number_elements, url_maker=url_maker
        )

    @classmethod
    def category_meta_tree_filtered(
        cls, request, band_id, category_id, collection_id, manufacturer_id
    ):
        query = (request.dbsession.query(models.CategoryMetaTreeDescriptions)).options(
            services.caching_query.FromCache("redis-eternal")
        )
        if category_id:
            query = query.filter(
                models.CategoryMetaTreeDescriptions.category_id == category_id
            )
        if collection_id:
            query = query.filter(
                models.CategoryMetaTreeDescriptions.collection_id == collection_id
            )
        if manufacturer_id:
            query = query.filter(
                models.CategoryMetaTreeDescriptions.manufacturer_id == manufacturer_id
            )
        if band_id:
            query = query.filter(models.CategoryMetaTreeDescriptions.band_id == band_id)
        query.options(services.caching_query.FromCache("redis-eternal"))
        return query.all()

    @classmethod
    def category_meta_tree_description_filter_band_category_manufacturer_collection(
        cls, request, band_id, category_id, collection, manufacturer_id, settings
    ):
        query = request.dbsession.query(models.CategoryMetaTreeDescriptions).filter(
            models.CategoryMetaTreeDescriptions.category_id == category_id
        )
        if collection and manufacturer_id:
            query = query.filter(
                models.CategoryMetaTreeDescriptions.band_id == None,
                models.CategoryMetaTreeDescriptions.collection_id.in_(collection),
                models.CategoryMetaTreeDescriptions.manufacturer_id == manufacturer_id,
            )
        elif manufacturer_id:
            query = query.filter(
                models.CategoryMetaTreeDescriptions.band_id == None,
                models.CategoryMetaTreeDescriptions.collection_id == None,
                models.CategoryMetaTreeDescriptions.manufacturer_id == manufacturer_id,
            )
        elif band_id:
            query = query.filter(
                models.CategoryMetaTreeDescriptions.band_id == band_id,
                models.CategoryMetaTreeDescriptions.collection_id == None,
                models.CategoryMetaTreeDescriptions.manufacturer_id == None,
            )
        else:
            query = query.filter(
                models.CategoryMetaTreeDescriptions.collection_id == None,
                models.CategoryMetaTreeDescriptions.manufacturer_id == None,
                models.CategoryMetaTreeDescriptions.band_id == None,
            )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.first()

    @classmethod
    def subquery_categories_ids_filter_dynamic(
        cls,
        request,
        filter_params,
        settings,
    ):
        """
        Method for retreiving list of filtered categories ID
        """
        subquery = request.dbsession.query(
            (models.Category.category_id).label("active_category_id")
        )
        if any(
            (
                filter_params.attributes,
                filter_params.availability,
                filter_params.band,
                filter_params.collection,
                filter_params.manufacturer,
                filter_params.price_min,
                filter_params.price_max,
                filter_params.searchstring,
                filter_params.dimensions,
            )
        ):
            subquery = subquery.join(
                models.ProductToCategory,
                models.ProductToCategory.category_id == models.Category.category_id,
            ).join(
                models.Product,
                models.Product.product_id == models.ProductToCategory.product_id,
            )
        subquery = services.ProductService.product_query_filter_searchstring(
            filter_params.searchstring, subquery
        )
        subquery = services.ProductService.product_query_filter_manufacturer(
            filter_params.manufacturer, subquery
        )
        if filter_params.band:
            subquery = subquery.join(
                models.ProductToBand,
                models.ProductToBand.product_id == models.ProductToCategory.product_id,
            ).filter(models.ProductToBand.band_id.in_(filter_params.band))
        if filter_params.collection:
            subquery = subquery.join(
                models.ProductToCollection,
                models.ProductToCollection.product_id
                == models.ProductToCategory.product_id,
            ).filter(
                models.ProductToCollection.collection_id.notin_(
                    filter_params.collection
                )
                if filter_params.collection == 999
                else models.ProductToCollection.collection_id.in_(
                    filter_params.collection
                )
            )
        if filter_params.category_id:
            top_category = (
                request.dbsession.query(models.Category.category_id)
                .filter(models.Category.category_id == filter_params.category_id)
                .cte("cte", recursive=True)
            )
            subcategories = request.dbsession.query(models.Category.category_id).join(
                top_category, models.Category.parent_id == top_category.c.category_id
            )
            recursive_query = top_category.union(subcategories)
            category_tree_query = request.dbsession.query(
                recursive_query
            ).scalar_subquery()
            subquery = subquery.filter(
                models.Category.category_id.in_(category_tree_query)
            )
        if filter_params.attributes:
            attributes_multiple = services.AttributeService.subquery_filter_attributes(
                request, filter_params.attributes, settings
            )
            subquery = subquery.outerjoin(models.ProductToAttribute).filter(
                and_(*attributes_multiple)
            )
        subquery = services.ProductService.product_query_filter_availability(
            filter_params.availability, subquery
        )
        subquery = services.ProductService.product_description_query_filter_description_searchstring(
            filter_params, subquery
        )
        subquery = services.ProductService.product_query_filter_price(
            filter_params, subquery
        )
        subquery = services.Cache.cache_query(settings, subquery)
        # return subquery.group_by(models.ProductToCategory.category_id).scalar_subquery()
        return subquery.distinct().scalar_subquery()

    @classmethod
    def subquery_categories_ids_filter_dynamic_with_disabled(
        cls,
        request,
        filter_params,
        settings,
    ):
        """
        Method for retreiving list of filtered categories ID
        """
        subquery = request.dbsession.query(
            (models.Category.category_id).label("active_category_id")
        )
        if any(
            (
                filter_params.attributes,
                filter_params.availability,
                filter_params.band,
                filter_params.collection,
                filter_params.manufacturer,
                filter_params.price_min,
                filter_params.price_max,
                filter_params.searchstring,
                filter_params.dimensions,
            )
        ):
            subquery = subquery.join(
                models.ProductToCategory,
                models.ProductToCategory.category_id == models.Category.category_id,
            ).join(
                models.Product,
                models.Product.product_id == models.ProductToCategory.product_id,
            )
        subquery = services.ProductService.product_query_filter_searchstring(
            filter_params.searchstring, subquery
        )
        subquery = services.ProductService.product_query_filter_manufacturer(
            filter_params.manufacturer, subquery
        )
        if filter_params.band:
            subquery = subquery.join(
                models.ProductToBand,
                models.ProductToBand.product_id == models.ProductToCategory.product_id,
            ).filter(models.ProductToBand.band_id.in_(filter_params.band))
        if filter_params.collection:
            subquery = subquery.join(
                models.ProductToCollection,
                models.ProductToCollection.product_id
                == models.ProductToCategory.product_id,
            ).filter(
                models.ProductToCollection.collection_id.notin_(
                    filter_params.collection
                )
                if filter_params.collection == 999
                else models.ProductToCollection.collection_id.in_(
                    filter_params.collection
                )
            )
        if filter_params.category_id:
            top_category = (
                request.dbsession.query(models.Category.category_id)
                .filter(models.Category.category_id == filter_params.category_id)
                .cte("cte", recursive=True)
            )
            subcategories = request.dbsession.query(models.Category.category_id).join(
                top_category, models.Category.parent_id == top_category.c.category_id
            )
            recursive_query = top_category.union(subcategories)
            category_tree_query = request.dbsession.query(
                recursive_query
            ).scalar_subquery()
            subquery = subquery.filter(
                models.Category.category_id.in_(category_tree_query)
            )
        if filter_params.attributes:
            attributes_multiple = services.AttributeService.subquery_filter_attributes(
                request, filter_params.attributes, settings
            )
            subquery = subquery.outerjoin(models.ProductToAttribute).filter(
                and_(*attributes_multiple)
            )
        subquery = services.ProductService.product_query_filter_availability(
            filter_params.availability, subquery
        )
        subquery = services.ProductService.product_description_query_filter_description_searchstring(
            filter_params, subquery
        )
        subquery = services.ProductService.product_query_filter_price(
            filter_params, subquery
        )
        subquery = services.Cache.cache_query(settings, subquery)
        # return subquery.group_by(models.ProductToCategory.category_id).scalar_subquery()
        return subquery.distinct().scalar_subquery()

    @classmethod
    def category_id_image_name_parent_placeholder_slug_filter_dynamic(
        cls,
        request,
        filter_params,
        settings,
    ):
        subquery = services.CategoryService.subquery_categories_ids_filter_dynamic(
            request,
            filter_params,
            settings,
        )
        query = (
            request.dbsession.query(
                models.Category.category_id,
                models.Category.category_type,
                models.Category.name,
                models.Category.parent_id,
                models.Category.placeholder,
                models.CategoryMetaTreeDescriptions.seo_slug,
            )
            .outerjoin(models.CategoryMetaTreeDescriptions)
            .filter(
                models.Category.category_id.in_(subquery),
                models.Category.status == 1,
                models.CategoryMetaTreeDescriptions.collection_id == None,
                models.CategoryMetaTreeDescriptions.manufacturer_id == None,
                models.CategoryMetaTreeDescriptions.band_id == None,
            )
            .order_by(asc(models.Category.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def category_id_image_name_parent_placeholder_slug_filter_product_id(
        cls,
        request,
        product_id,
        settings,
    ):
        query = (
            request.dbsession.query(
                models.Category.category_id,
                models.Category.name,
                models.Category.parent_id,
                models.Category.category_type,
                models.Category.placeholder,
                models.CategoryMetaTreeDescriptions.seo_slug,
            )
            .filter(
                ~models.Category.placeholder == True,
                models.Category.status == 1,
                models.CategoryMetaTreeDescriptions.collection_id == None,
                models.CategoryMetaTreeDescriptions.manufacturer_id == None,
                models.CategoryMetaTreeDescriptions.band_id == None,
                models.ProductToCategory.product_id == product_id,
            )
            .join(models.CategoryMetaTreeDescriptions)
            .join(
                models.ProductToCategory,
                models.ProductToCategory.category_id == models.Category.category_id,
            )
            .order_by(asc(models.Category.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def category_id_image_name_parent_placeholder_slug_filter_active(
        cls, request, settings
    ):
        query = (
            request.dbsession.query(
                models.Category.category_id,
                models.Category.category_type,
                models.Category.name,
                models.Category.parent_id,
                models.Category.placeholder,
                models.CategoryMetaTreeDescriptions.seo_slug,
            )
            .join(models.CategoryMetaTreeDescriptions)
            .filter(
                models.Category.status == 1,
                models.CategoryMetaTreeDescriptions.collection_id == None,
                models.CategoryMetaTreeDescriptions.manufacturer_id == None,
                models.CategoryMetaTreeDescriptions.band_id == None,
            )
            .order_by(asc(models.Category.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def category_id_image_name_parent_placeholder_slug(cls, request, settings):
        query = (
            request.dbsession.query(
                models.Category.category_type,
                models.Category.category_id,
                models.Category.name,
                models.Category.parent_id,
                models.Category.placeholder,
                models.CategoryMetaTreeDescriptions.seo_slug,
            )
            .join(models.CategoryMetaTreeDescriptions)
            .filter(
                models.CategoryMetaTreeDescriptions.collection_id == None,
                models.CategoryMetaTreeDescriptions.manufacturer_id == None,
                models.CategoryMetaTreeDescriptions.band_id == None,
            )
            .order_by(asc(models.Category.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def delete_meta_tree_description_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        query = request.dbsession.query(models.CategoryMetaTreeDescriptions).filter(
            models.CategoryMetaTreeDescriptions.manufacturer_id == manufacturer_id,
        )
        return query.delete(synchronize_session=False)

    @classmethod
    def category_seo_slug_filter_id(
        cls,
        request,
        category_id,
        settings,
    ):
        query = (
            request.dbsession.query(
                models.CategoryMetaTreeDescriptions.seo_slug,
            )
            .join(models.Category)
            .filter(
                models.Category.placeholder == None,
                models.Category.status == 1,
                models.CategoryMetaTreeDescriptions.collection_id == None,
                models.CategoryMetaTreeDescriptions.manufacturer_id == None,
                models.CategoryMetaTreeDescriptions.band_id == None,
                models.CategoryMetaTreeDescriptions.category_id == category_id,
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.scalar()
