from paginate_sqlalchemy import SqlalchemyOrmPage

# from sqlalchemy import (
#     and_,
#     asc,
#     desc,
#     or_,
#     exists,
#     func,
#     func,
#     text,
# )
from sqlalchemy.orm import Load

from emporium import models, services


class FileService(object):
    @classmethod
    def product_files_filter_product_id(cls, request, product_id, settings):
        query = (
            request.dbsession.query(
                models.File.file_name,
                models.File.file_name_display_mode_id,
                models.File.file_path,
                models.File.file_permission_id,
                models.File.file_type_id,
                models.File.original_file_name,
                models.ProductToFile.file_id,
                models.ProductToFile.product_file_id,
                models.ProductToFile.sort_order,
            )
            .filter(models.ProductToFile.product_id == product_id)
            .join(models.File)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def file_filter_file_list(
        cls,
        request,
        file_list,
    ):
        query = request.dbsession.query(models.File).filter(
            models.File.file_id.in_(file_list)
        )
        return query.all()

    @classmethod
    def file_type_name_description_filter_status(cls, request, settings):
        query = request.dbsession.query(
            models.FileType.file_type_id,
            models.FileType.file_type_name,
            models.FileType.description,
        ).filter(models.FileType.status == 1)
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def file_name_display_type_name_filter_status(cls, request, settings):
        query = request.dbsession.query(
            models.FileNameDisplayMode.file_name_display_mode_id,
            models.FileNameDisplayMode.name,
        ).filter(models.FileNameDisplayMode.status == 1)
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()
