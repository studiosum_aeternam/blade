from sqlalchemy import and_, asc, func, or_, text
from sqlalchemy.orm import Load
from paginate_sqlalchemy import SqlalchemyOrmPage

# from ..services import FromCache
from ..services.caching_query import FromCache
from ..models import (
    Attribute,
    AttributeGroup,
    AttributeDisplayType,
    Product,
    ProductToAttribute,
    ProductToBand,
    ProductToCategory,
    ProductToCollection,
    ProductToBand,
)
from emporium import services


class AttributeService(object):
    @classmethod
    def attribute_display_type_id_description_name_list(cls, request):
        return request.dbsession.query(
            AttributeDisplayType.attribute_display_type_id,
            AttributeDisplayType.name,
            AttributeDisplayType.description,
        ).all()

    @classmethod
    def attribute_group_list(cls, request):
        return request.dbsession.query(
            AttributeGroup.attribute_group_id,
            AttributeGroup.name,
            AttributeGroup.attribute_name,
        ).all()

    @classmethod
    def by_id(cls, request, attribute_id):
        return (
            request.dbsession.query(Attribute)
            .filter(Attribute.attribute_id == attribute_id)
            .one()
        )

    @classmethod
    def attribute_group_attribute_filter_active(cls, request, settings):
        query = (
            request.dbsession.query(
                AttributeGroup.attribute_display_type_id,
                AttributeGroup.attribute_group_id,
                AttributeGroup.name,
                AttributeGroup.required,
                AttributeGroup.sort_order,
                Attribute.attribute_id,
                Attribute.sort_order,
                Attribute.value,
                Attribute.attribute_settings,
            )
            .join(Attribute)
            .filter(AttributeGroup.status != 0, Attribute.status != 0)
            .order_by(
                asc(AttributeGroup.sort_order),
                asc(Attribute.sort_order),
                asc(Attribute.value),
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def attribute_group_attribute_filter_product_id(
        cls, request, availability_id, product_id, settings
    ):
        query = (
            request.dbsession.query(
                Attribute.attribute_group_id,
                Attribute.attribute_id,
                Attribute.sort_order,
                Attribute.value,
            )
            .outerjoin(ProductToAttribute)
            .filter(
                Attribute.status != 0,
                or_(
                    ProductToAttribute.product_id == product_id,
                    Attribute.attribute_id == availability_id,
                ),
            )
            .order_by(
                asc(Attribute.sort_order),
                asc(Attribute.value),
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def attribute_group_attribute_filter_products_ids(
        cls, request, availabilities, products_ids, settings
    ):
        query = (
            request.dbsession.query(
                Attribute.attribute_group_id,
                Attribute.attribute_id,
                Attribute.sort_order,
                Attribute.value,
            )
            .outerjoin(ProductToAttribute)
            .filter(
                Attribute.status != 0,
                or_(
                    ProductToAttribute.product_id.in_(products_ids),
                    Attribute.attribute_id.in_(availabilities),
                ),
            )
            .order_by(
                asc(Attribute.sort_order),
                asc(Attribute.value),
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def attribute_group_attribute_filter_attribute_list(
        cls, request, filtered_attributes, settings
    ):
        query = (
            request.dbsession.query(
                Attribute.attribute_group_id,
                Attribute.attribute_id,
                Attribute.sort_order,
                Attribute.value,
            )
            .filter(
                Attribute.status != 0,
                Attribute.attribute_id.in_(filtered_attributes),
            )
            .order_by(
                asc(Attribute.sort_order),
                asc(Attribute.value),
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def bare_by_id(cls, request, attribute_group_id):
        return (
            request.dbsession.query(AttributeGroup)
            .filter(AttributeGroup.attribute_group_id == attribute_group_id)
            .all()
        )

    @classmethod
    def attributes_filter_product_id(
        cls, request, product_id, availability_id, settings
    ):
        subquery = (
            request.dbsession.query(ProductToAttribute.attribute_id)
            .filter(ProductToAttribute.product_id == product_id)
            .scalar_subquery()
        )
        query = (
            request.dbsession.query(
                AttributeGroup.attribute_display_type_id,
                AttributeGroup.attribute_group_id,
                AttributeGroup.name,
                AttributeGroup.sort_order,
                Attribute.attribute_id,
                Attribute.value,
            )
            .join(Attribute)
            .filter(
                or_(
                    Attribute.attribute_id.in_(subquery),
                    Attribute.attribute_id == availability_id,
                )
            )
            .order_by(
                asc(AttributeGroup.sort_order),
                asc(Attribute.sort_order),
                asc(Attribute.value),
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def attribute_id_name_filter_product_id(cls, request, product_id, availability_id):
        subquery = (
            request.dbsession.query(ProductToAttribute.attribute_id)
            .filter(ProductToAttribute.product_id == product_id)
            .scalar_subquery()
        )
        query = (
            request.dbsession.query(
                Attribute.attribute_group_id,
                Attribute.attribute_id,
                Attribute.value,
            )
            .filter(
                or_(
                    Attribute.attribute_id.in_(subquery),
                    Attribute.attribute_id == availability_id,
                )
            )
            .order_by(
                asc(Attribute.sort_order),
                asc(Attribute.value),
            )
        )
        return query.all()

    @classmethod
    def attributes_by_product_list(
        cls, request, product_list, availabilities, settings
    ):
        subquery = (
            request.dbsession.query(ProductToAttribute.attribute_id)
            .filter(ProductToAttribute.product_id.in_(product_list))
            .scalar_subquery()
        )
        query = (
            request.dbsession.query(
                AttributeGroup.attribute_display_type_id,
                AttributeGroup.attribute_group_id,
                AttributeGroup.name,
                AttributeGroup.sort_order,
                Attribute.attribute_id,
                Attribute.value,
            )
            .join(Attribute)
            .filter(
                or_(
                    Attribute.attribute_id.in_(subquery),
                    Attribute.attribute_id.in_(availabilities),
                )
            )
            .order_by(
                asc(AttributeGroup.sort_order),
                asc(Attribute.sort_order),
                asc(AttributeGroup.name),
                asc(Attribute.value),
            )
        )
        # if cache:
        #     query = query.options(FromCache("redis-long"))
        return query.all()

    @classmethod
    def availability_attributes(cls, request, settings):
        query = (
            request.dbsession.query(func.array_agg(Attribute.attribute_id))
            .join(AttributeGroup)
            .filter(AttributeGroup.attribute_name == "availability")
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.scalar()

    @classmethod
    def attribute_id_value_filter_attribute_group_availability(cls, request, settings):
        query = (
            request.dbsession.query(Attribute.attribute_id, Attribute.value)
            .join(AttributeGroup)
            .filter(AttributeGroup.attribute_name == "availability")
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def attribute_id_value_filter_product_id(cls, request, availability_id, settings):
        query = request.dbsession.query(Attribute.attribute_id, Attribute.value).filter(
            Attribute.attribute_id == availability_id,
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def attribute_id_value_filter_id(cls, request, availability, settings):
        query = request.dbsession.query(Attribute.attribute_id, Attribute.value).filter(
            Attribute.attribute_id == availability
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def by_group_id(cls, request, attribute_group_id):
        query = (
            request.dbsession.query(Attribute)
            .order_by(
                asc(Attribute.attribute_group_id),
                asc(Attribute.sort_order),
                asc(Attribute.value),
            )
            .filter(Attribute.attribute_group_id == attribute_group_id)
        )
        return query.all()

    @classmethod
    def attribute_group_filter_id(cls, request, attribute_group_id):
        return (
            request.dbsession.query(AttributeGroup)
            .filter(AttributeGroup.attribute_group_id == attribute_group_id)
            .first()
        )

    @classmethod
    def by_attribute_list(cls, request, attribute_list):
        return (
            request.dbsession.query(Attribute)
            .filter(Attribute.attribute_id.in_(attribute_list))
            .options(FromCache("redis-eternal"))
            .all()
        )

    @classmethod
    def attribute_group_id_name_status_sort_order_attributes_paginated(
        cls, request, page=1
    ):
        query = (
            request.dbsession.query(
                AttributeGroup.attribute_group_id,
                AttributeGroup.name,
                AttributeGroup.attribute_name,
                AttributeGroup.sort_order,
                AttributeGroup.status,
                func.array_agg(Attribute.value).label("attributes"),
            )
            .join(AttributeGroup.attribute)
            .order_by(asc(AttributeGroup.name))
            .group_by(AttributeGroup.attribute_group_id)
        )
        query_params = request.GET.mixed()

        def url_maker(link_page):
            # replace page param with values generated by paginator
            query_params["page"] = link_page
            return request.current_route_url(_query=query_params)

        return SqlalchemyOrmPage(query, page, items_per_page=24, url_maker=url_maker)

    @classmethod
    def attribute_search(cls, request, searchstring):
        return (
            request.dbsession.query(Attribute, AttributeGroup)
            .join(AttributeGroup)
            .order_by(asc(AttributeGroup.name))
            .filter(
                or_(
                    AttributeGroup.name.ilike(searchstring),
                    func.concat(AttributeGroup.name, " ", AttributeGroup.name).ilike(
                        searchstring
                    ),
                ),
                Attribute.status != "0",
            )
            .limit(16)
        )

    @classmethod
    def attribute_group_search(cls, request, searchstring):
        return (
            request.dbsession.query(AttributeGroup)
            .order_by(asc(AttributeGroup.name))
            .filter(
                or_(
                    AttributeGroup.name.ilike(searchstring),
                    func.concat(AttributeGroup.name, " ", AttributeGroup.name).ilike(
                        searchstring
                    ),
                ),
                AttributeGroup.status != "0",
            )
            .limit(16)
        )

    @classmethod
    def selected_attributes_aggregated(cls, request, attributes_list, settings):
        query = (
            request.dbsession.query(
                AttributeGroup.attribute_group_id,
                func.array_agg(Attribute.attribute_id).label("attributes"),
            )
            .join(AttributeGroup.attribute)
            .filter(
                AttributeGroup.status == 1,
                Attribute.status == 1,
                Attribute.attribute_id.in_(attributes_list),
            )
            .group_by(AttributeGroup.attribute_group_id)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def products_with_grouped_attributes(
        cls, request, disabled_attributes, disabled_products
    ):
        query = (
            (
                request.dbsession.query(
                    Product.product_id,
                    func.array_agg(ProductToAttribute.attribute_id).label("attributes"),
                )
                .outerjoin(ProductToAttribute)
                .filter(
                    Product.product_id.notin_(disabled_products),
                    ProductToAttribute.attribute_id.notin_(disabled_attributes),
                )
                .group_by(Product.product_id)
            )
            .order_by(Product.product_id)
            .options(FromCache("redis-eternal"))
        )
        return query.all()

    # @classmethod
    # def products_with_grouped_attribute_values(
    #     self, request, disabled_attributes, disabled_products, attribute_group_id
    # ):
    #     attributes = (
    #         request.dbsession.query(
    #             ProductToAttribute, (Attribute.value).label("value")
    #         )
    #         .join(Attribute)
    #         .filter(
    #             Attribute.attribute_id.notin_(disabled_attributes),
    #             Attribute.attribute_group_id == attribute_group_id,
    #         )
    #         .options(FromCache("redis-eternal"))
    #     ).subquery()
    #     query = (
    #         (
    #             request.dbsession.query(
    #                 Product.product_id,
    #                 func.array_agg(attributes.c.value).label("value"),
    #             )
    #             .outerjoin(attributes, attributes.c.product_id == Product.product_id)
    #             .filter(Product.product_id.notin_(disabled_products))
    #             .options(Load(Product).load_only("product_id"))
    #             .group_by(Product.product_id)
    #         )
    #         .order_by(Product.product_id)
    #         .options(FromCache("redis-eternal"))
    #     )
    #     return query.all()

    @classmethod
    def disabled_attributes(cls, request):
        query = (
            request.dbsession.query(Attribute)
            .join(AttributeGroup)
            .options(Load(Attribute).load_only("attribute_id", "status"))
            .options(Load(AttributeGroup).load_only("status"))
            .filter(AttributeGroup.status == 0, Attribute.status == 0)
        ).options(FromCache("redis-eternal"))
        return query.all()

    @classmethod
    def subquery_filter_attributes(cls, request, attributes, settings):
        attributes_list = AttributeService.selected_attributes_aggregated(
            request, attributes, settings
        )
        attributes_multiple_dict = {}
        attributes_multiple = []
        for htem in attributes_list:
            if htem.attribute_group_id in attributes_multiple_dict:
                attributes_multiple_dict[htem.attribute_id].extend(htem.attributes)
            else:
                attributes_multiple_dict[htem.attribute_group_id] = htem.attributes
        for v in attributes_multiple_dict.values():
            operator = f" = {v[0]}" if len(v) == 1 else f" IN {tuple(v)}"
            attributes_multiple.append(
                Product.product_to_attribute.any(
                    text(f"product_to_attribute.attribute_id{operator}")
                )
            )
        return attributes_multiple

    @classmethod
    def attributes_filter_active(cls, request, settings, fake_admin=False):
        if fake_admin:
            query = request.dbsession.query(
                Attribute.attribute_group_id,
                Attribute.attribute_id,
                Attribute.value,
                Attribute.attribute_settings,
            )
        else:
            query = request.dbsession.query(
                Attribute.attribute_group_id,
                Attribute.attribute_id,
                Attribute.value,
            )
        query = query.filter(Attribute.status == 1).order_by(
            asc(Attribute.sort_order),
            asc(Attribute.value),
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def attributes_filter_dynamic(
        cls,
        request,
        filter_params,
        availability_attributes,
        settings,
    ):
        subquery = cls.attributes_prefilter_subquery(
            request,
            filter_params,
        )
        query = (
            request.dbsession.query(
                Attribute.attribute_group_id,
                Attribute.attribute_id,
                Attribute.value,
            )
            .filter(Attribute.status == 1)
            .order_by(
                asc(Attribute.sort_order),
                asc(Attribute.value),
            )
        )
        if availability_attributes:
            query = query.filter(
                or_(
                    Attribute.attribute_id.in_(subquery),
                    Attribute.attribute_id.in_(availability_attributes),
                )
            )
        else:
            query = query.filter(Attribute.attribute_id.in_(subquery))
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def attribute_groups_filter_attribute_list(
        cls,
        request,
        attribute_list,
        settings,
    ):
        query = (
            request.dbsession.query(
                AttributeGroup.attribute_display_type_id,
                AttributeGroup.attribute_group_id,
                AttributeGroup.attribute_name,
                AttributeGroup.name,
                AttributeGroup.sort_order,
            )
            .join(Attribute, Attribute.attribute_id.in_(attribute_list))
            .filter(AttributeGroup.status == 1)
            .order_by(
                asc(AttributeGroup.sort_order),
            )
            .group_by(
                AttributeGroup.attribute_display_type_id,
                AttributeGroup.attribute_group_id,
                AttributeGroup.attribute_name,
                AttributeGroup.name,
                AttributeGroup.sort_order,
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def attribute_groups_filter_attribute_list_active(
        cls, request, attribute_list, settings, exclude=[]
    ):
        query = (
            request.dbsession.query(
                AttributeGroup.attribute_display_type_id,
                AttributeGroup.attribute_group_id,
                AttributeGroup.attribute_name,
                AttributeGroup.name,
                AttributeGroup.sort_order,
            )
            .join(Attribute)
            .filter(
                AttributeGroup.status == 1, Attribute.attribute_id.in_(attribute_list)
            )
            .order_by(
                asc(AttributeGroup.name),
            )
            .group_by(
                AttributeGroup.attribute_display_type_id,
                AttributeGroup.attribute_group_id,
                AttributeGroup.name,
                AttributeGroup.sort_order,
            )
        )
        if exclude:
            query = query.filter(~AttributeGroup.attribute_name.in_(exclude))
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def attribute_groups_filter_active(
        cls,
        request,
        settings,
    ):
        query = (
            request.dbsession.query(
                AttributeGroup.attribute_display_type_id,
                AttributeGroup.attribute_group_id,
                AttributeGroup.name,
                AttributeGroup.sort_order,
                AttributeGroup.attribute_name,
            )
            .filter(AttributeGroup.status == 1)
            .order_by(
                asc(AttributeGroup.name),
            )
            .group_by(
                AttributeGroup.attribute_display_type_id,
                AttributeGroup.attribute_group_id,
                AttributeGroup.name,
                AttributeGroup.sort_order,
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def attributes_prefilter_subquery(cls, request, filter_params):
        subquery = request.dbsession.query(
            (Attribute.attribute_id).label("attributes")
        ).join(ProductToAttribute)
        if filter_params.attributes:
            mini_subquery = (
                (request.dbsession.query(ProductToAttribute.product_id))
                .filter(ProductToAttribute.attribute_id.in_(filter_params.attributes))
                .subquery()
            )
            subquery = subquery.join(
                mini_subquery,
                mini_subquery.c.product_id == ProductToAttribute.product_id,
            )
        if filter_params.category_id:
            subquery = subquery.join(
                ProductToCategory,
                ProductToCategory.product_id == ProductToAttribute.product_id,
            ).filter(ProductToCategory.category_id == filter_params.category_id)
        if any(
            (
                filter_params.manufacturer,
                filter_params.searchstring,
                filter_params.price_min,
                filter_params.price_max,
                filter_params.status,
                filter_params.dimensions,
            )
        ):
            subquery = subquery.join(Product)
        subquery = services.ProductService.product_query_filter_manufacturer(
            filter_params.manufacturer, subquery
        )
        subquery = services.ProductService.product_query_filter_dimensions(
            filter_params.dimensions, subquery
        )
        subquery = services.ProductService.product_query_filter_searchstring(
            filter_params.searchstring, subquery
        )
        if filter_params.band:
            subquery = subquery.join(
                ProductToBand,
                ProductToBand.product_id == ProductToAttribute.product_id,
            ).filter(ProductToBand.band_id.in_(filter_params.band))
        if filter_params.collection and filter_params.collection == 999:
            subquery = subquery.filter(
                ProductToCollection.collection_id.notin_(filter_params.collection)
            )
        elif filter_params.collection:
            subquery = subquery.join(
                ProductToCollection,
                ProductToCollection.product_id == ProductToAttribute.product_id,
            ).filter(ProductToCollection.collection_id.in_(filter_params.collection))
        subquery = services.ProductService.product_description_query_filter_description_searchstring(
            filter_params, subquery
        )
        subquery = services.ProductService.product_query_filter_price(
            filter_params, subquery
        )
        # subquery = services.ProductService.product_query_filter_status(
        #     filter_params.status, subquery
        # )
        return subquery.group_by(Attribute.attribute_id).scalar_subquery()

    @classmethod
    def attribute_query_filter_attributes(cls, request, attributes, settings, query):
        if attributes:
            attributes_multiple = cls.subquery_filter_attributes(
                request, attributes, settings
            )
            query = query.filter(and_(*attributes_multiple))
        return query
