from paginate_sqlalchemy import SqlalchemyOrmPage
from sqlalchemy import (
    and_,
    asc,
    desc,
    or_,
    exists,
    func,
    func,
    text,
)
from sqlalchemy.orm import Load

from emporium import models, services


class ProductToXMLService(object):
    @classmethod
    def product_xml_filter_xml(cls, request, product_xml_id):
        query = request.dbsession.query(models.ProductToXML).filter(
            models.ProductToXML.product_xml_id == product_xml_id
        )
        return query.one()

    @classmethod
    def products_excluded_from_xml(cls, request):
        return (
            request.dbsession.query(
                models.ProductToXML.product_xml_id,
                models.ProductToXML.product_id,
                models.ProductToXML.xml_id,
                models.Product.name,
                models.Manufacturer.name.label("manufacturer"),
            )
            .join(
                models.Product,
                models.Product.product_id == models.ProductToXML.product_id,
            )
            .join(
                models.Manufacturer,
                models.Product.manufacturer_id == models.Manufacturer.manufacturer_id,
            )
            .filter(models.ProductToXML.excluded == True)
            .all()
        )
