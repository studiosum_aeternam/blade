import sqlalchemy as sa
from ..models import Block

from emporium import services


class BlockService(object):
    @classmethod
    def blocks_all(cls, request):
        query = request.dbsession.query(Block)
        return query.order_by(sa.desc(Block.title))

    @classmethod
    def block_full_filter_by_block_id(cls, request, block_id):
        query = request.dbsession.query(Block).filter(Block.block_id == block_id)
        return query.first()

    @classmethod
    def blocks_values_content_title_size_filter_page_date(cls, request, page, settings):
        query = (
            request.dbsession.query(Block.content, Block.size, Block.title)
            .filter(Block.page == page, Block.status == True)
            .order_by(sa.asc(Block.order))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()
