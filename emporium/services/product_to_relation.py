from datetime import datetime
from paginate_sqlalchemy import SqlalchemyOrmPage

from sqlalchemy import asc, exists, func, or_

from emporium import models, services


class ProductToRelationService(object):
    @classmethod
    def delete_all_product_relation_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductRelations.product_relation_id)
            )
            .join(models.Product)
            .filter(models.Product.manufacturer_id == manufacturer_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductRelations).filter(
                models.ProductRelations.product_relation_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)

    @classmethod
    def delete_all_product_relation_filter_band_id(cls, request, band_id):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductRelations.product_relation_id)
            )
            .join(models.Product, models.ProductToBand)
            .filter(models.ProductToBand.band_id == band_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductRelations).filter(
                models.ProductRelations.product_relation_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)
