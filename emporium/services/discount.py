from sqlalchemy import asc

from ..models import Discount, ManufacturerToDiscount
from emporium import services


class DiscountService(object):
    @classmethod
    def discount_id_value_manufacturer_filter_active(cls, request):
        return (
            request.dbsession.query(
                Discount.value,
                Discount.discount_id,
                Discount.default,
                ManufacturerToDiscount.manufacturer_id,
            )
            .join(ManufacturerToDiscount)
            .filter(Discount.status != "0")
            .order_by(asc(Discount.value))
            .all()
        )

    @classmethod
    def discount_filter_id(cls, request, discount_id):
        return (
            request.dbsession.query(Discount)
            .filter(Discount.discount_id == discount_id)
            .one()
        )

    @classmethod
    def discount_subtract_value_filter_manufacturer(
        cls, request, manufacturer_id, cache=True
    ):
        query = (
            request.dbsession.query(Discount)
            .join(ManufacturerToDiscount)
            .filter(
                ManufacturerToDiscount.manufacturer_id == manufacturer_id,
                Discount.status != 0,
            )
            .order_by(asc(Discount.value))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def discount_list_id_value(cls, request):
        return request.dbsession.query(Discount.discount_id, Discount.value).all()

    @classmethod
    def discount_list_id_value_filter_discount_id(cls, request, discount_list):
        return (
            request.dbsession.query(Discount.discount_id, Discount.value)
            .filter(Discount.discount_id.in_(discount_list))
            .all()
        )
