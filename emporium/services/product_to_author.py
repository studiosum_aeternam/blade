# from paginate_sqlalchemy import SqlalchemyOrmPage
import sqlalchemy as sa

from emporium import models, services


class ProductToAuthorService(object):
    @classmethod
    def product_to_author_list_filter_product_author_list(
        cls, request, product_author_list
    ):
        query = request.dbsession.query(models.ProductToAuthor).filter(
            models.ProductToAuthor.product_author_id.in_(product_author_list)
        )
        return query.all()

    @classmethod
    def product_id_author_id_filter_product_list(cls, request, settings, product_list):
        query = (
            request.dbsession.query(
                models.ProductToAuthor.order,
                models.ProductToAuthor.product_author_id,
                models.ProductToAuthor.product_id,
                models.ProductToAuthor.role,
                models.ProductToAuthor.visibility,
                models.Author.author_id,
                models.Author.callsign,
                models.Author.first_name,
                models.Author.last_name,
            )
            .join(
                models.Author,
                models.Author.author_id == models.ProductToAuthor.author_id,
            )
            .filter(models.ProductToAuthor.product_id.in_(product_list))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_author_list_filter_product_id(cls, request, product_id):
        query = request.dbsession.query(models.ProductToAuthor).filter(
            models.ProductToAuthor.product_id == product_id
        )
        return query.all()

    @classmethod
    def product_to_author_id_name_order_role_filter_product_id(
        cls, request, product_id
    ):
        return (
            request.dbsession.query(
                models.ProductToAuthor.order,
                models.ProductToAuthor.product_author_id,
                models.ProductToAuthor.product_id,
                models.ProductToAuthor.role,
                models.ProductToAuthor.visibility,
                models.Author.author_id,
                models.Author.callsign,
                models.Author.first_name,
                models.Author.last_name,
            )
            .join(models.ProductToAuthor)
            .filter(models.ProductToAuthor.product_id == product_id)
            .order_by(sa.asc(models.ProductToAuthor.order))
            .all()
        )
