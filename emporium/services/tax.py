import sqlalchemy as sa
from emporium import models, services


class TaxService(object):
    @classmethod
    def tax_id_name_value_filter_active(cls, request, settings):
        return (
            request.dbsession.query(
                models.Tax.tax_id,
                models.Tax.value,
                models.TaxDescription.name,
            )
            .join(models.TaxDescription)
            .filter(models.Tax.status == 1)
            .order_by(sa.desc(models.Tax.value))
        )

        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def tax_id_name_value_filter_tax_id_active(cls, request, tax_id, settings):
        query = (
            request.dbsession.query(
                models.Tax.tax_id,
                models.Tax.value,
                models.TaxDescription.name,
            )
            .join(models.TaxDescription)
            .filter(models.Tax.status == 1, models.Tax.tax_id == tax_id)
            .order_by(sa.desc(models.Tax.value))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()
