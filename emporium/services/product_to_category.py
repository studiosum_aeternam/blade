from paginate_sqlalchemy import SqlalchemyOrmPage
from sqlalchemy import (
    and_,
    asc,
    desc,
    or_,
    exists,
    func,
    func,
    text,
)
from sqlalchemy.orm import Load

from emporium import models, services


class ProductToCategoryService(object):
    @classmethod
    def product_to_categories_filter_product_list(cls, request, product_list):
        query = request.dbsession.query(models.ProductToCategory).filter(
            models.ProductToCategory.product_id.in_(product_list)
        )
        return query.all()

    @classmethod
    def product_categories_filter_product_id(cls, request, product_id):
        query = request.dbsession.query(models.ProductToCategory).filter(
            models.ProductToCategory.product_id == product_id
        )
        return query.all()

    @classmethod
    def delete_all_product_to_category_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        if subquery := (
            request.dbsession.query(
                func.array_agg(models.ProductToCategory.product_category_id)
            )
            .join(models.Product)
            .filter(models.Product.manufacturer_id == manufacturer_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToCategory).filter(
                models.ProductToCategory.product_category_id.in_(subquery)
            )
            return query.delete(synchronize_session=False)

    @classmethod
    def delete_all_product_to_category_filter_band_id(cls, request, band_id):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductToCategory.product_category_id)
            )
            .join(models.Product, models.ProductToBand)
            .filter(models.ProductToBand.band_id == band_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductToCategory).filter(
                models.ProductToCategory.product_category_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)

    @classmethod
    def product_categories_array_filter_product_id(cls, request, product_id):
        query = request.dbsession.query(
            func.array_agg(models.ProductToCategory.category_id)
        ).filter(models.ProductToCategory.product_id == product_id)
        return query.scalar()
