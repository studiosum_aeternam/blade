from paginate_sqlalchemy import SqlalchemyOrmPage
from sqlalchemy import (
    and_,
    asc,
    desc,
    or_,
    exists,
    func,
    func,
    text,
)
from sqlalchemy.orm import Load

from emporium import models, services


class ProductService(object):
    @classmethod
    def products_active(cls, request, disabled_products):
        product_descriptions = (
            request.dbsession.query(
                models.ProductDescription.description,
                models.ProductDescription.product_id,
            )
            .filter(models.ProductDescription.language_id == 2)
            .subquery()
        )
        query = (
            request.dbsession.query(
                models.Product.availability_id,
                models.Product.ean,
                models.Product.image,
                models.Product.minimum,
                models.Product.manufacturer_id,
                models.Product.name,
                models.Product.price,
                models.Product.product_id,
                models.Product.seo_slug,
                models.Product.square_meter,
                models.Product.status,
                models.Product.tax_id,
                models.Product.unit,
                models.Product.weight,
                product_descriptions.c.description,
            )
            .filter(~models.Product.product_id.in_(disabled_products))
            .outerjoin(
                product_descriptions,
                product_descriptions.c.product_id == models.Product.product_id,
            )
            .order_by(models.Product.product_id)
        ).options(services.FromCache("redis-eternal"))
        return query.all()

    @classmethod
    def product_filter_product_id(cls, request, product_id):
        query = request.dbsession.query(models.Product).filter(
            models.Product.product_id == product_id
        )
        return query.first()

    @classmethod
    def products_by_list(cls, request, product_list):
        query = request.dbsession.query(models.Product).filter(
            models.Product.product_id.in_(product_list)
        )
        return query.all()

    @classmethod
    def product_delete_filter_string(cls, request, match_string):
        query = request.dbsession.query(models.Product).filter(
            models.Product.name.ilike(f"{match_string}%")
        )
        return query.delete(synchronize_session="fetch")

    @classmethod
    def products_base_price_filter_product_list(cls, request, product_list):
        query = cls.product_base_price(request)
        query = query.filter(
            models.Product.product_id.in_(product_list),
        )
        return query.all()

    @classmethod
    def products_base_price_filter_available(cls, request, product_list, settings):
        query = cls.product_base_price(request)
        query = query.filter(
            and_(
                models.Product.product_id.in_(product_list),
                ~models.Product.availability_id.in_((51, 53)),
                models.Product.status != 0,
            ),
        )
        return query.all()

    @classmethod
    def product_id_filter_model(cls, request, model):
        query = request.dbsession.query(models.Product.product_id).filter(
            models.Product.model == model
        )
        return query.first()

    @classmethod
    def by_manufacturer(cls, request, manufacturer_id):
        query = request.dbsession.query(models.Product).filter(
            models.Product.manufacturer_id == manufacturer_id
        )
        return query.all()

    @classmethod
    def product_with_description_filter_product_id(cls, request, product_id, settings):
        (
            query,
            current_price_subuery,
            ancient_price_subquery,
        ) = cls.product_base_info_description_variables(request)
        query = query.filter(
            models.Product.product_id == product_id,
            models.Product.status == 1,
            models.ProductDescription.language_id == request.language_id,
            models.ProductDescription.product_id == product_id,
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.first()

    @classmethod
    def product_id_ean_filter_product_list(cls, request, product_list, settings):
        query = request.dbsession.query(
            models.Product.product_id, models.Product.ean
        ).filter(models.Product.product_id.in_(product_list))
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def products_specials_products(cls, request):
        query = (
            request.dbsession.query(models.ProductToSpecial, models.Product)
            .join(models.Product)
            .order_by(models.Product.name)
        )
        return query.all()

    @classmethod
    def product_description_filter_product_id(cls, request, product_id, language_id=2):
        query = request.dbsession.query(models.ProductDescription).filter(
            models.ProductDescription.product_id == product_id,
            models.ProductDescription.language_id == language_id,
        )
        return query.first()

    @classmethod
    def product_description_list_filter_product_id(cls, request, product_id):
        query = request.dbsession.query(models.ProductDescription).filter(
            models.ProductDescription.product_id == product_id
        )
        return query.all()

    @classmethod
    def product_description_list_filter_language_id(cls, request, language_id):
        query = request.dbsession.query(models.ProductDescription).filter(
            models.ProductDescription.language_id == language_id
        )
        return query.all()

    @classmethod
    def delete_product_description_filter_product_id(
        cls, request, product_id, language_id=2
    ):
        query = request.dbsession.query(models.ProductDescription).filter(
            models.ProductDescription.product_id == product_id,
            models.ProductDescription.language_id == language_id,
        )
        return query.delete()

    @classmethod
    def product_filter_collection_id_values_id_name(cls, request, collection_id):
        query = (
            request.dbsession.query(models.Product.product_id, models.Product.name)
            .join(models.ProductToCollection)
            .filter(
                models.Product.status == 1,
                models.ProductToCollection.collection_id == collection_id,
            )
        )
        return query.all()

    @classmethod
    def product_filter_band_id_values_id_name(cls, request, band_id):
        query = (
            request.dbsession.query(models.Product.product_id, models.Product.name)
            .join(models.ProductToBand)
            .filter(
                models.Product.status == 1,
                models.ProductToBand.band_id == band_id,
            )
        )
        return query.all()

    @classmethod
    def products_filter_band_id(cls, request, band_id):
        query = (
            request.dbsession.query(models.Product)
            .join(models.ProductToBand)
            .filter(
                models.ProductToBand.band_id == band_id,
            )
        )
        return query.all()

    @classmethod
    def products_id_name_filter_category(cls, request, category_id, settings):
        query = (
            request.dbsession.query(models.Product.product_id, models.Product.name)
            .filter(models.Product.status == 1)
            .order_by(asc(models.Product.name))
        )
        query = query.filter(
            and_(
                models.Product.product_to_category.any(
                    models.ProductToCategory.category_id == category_id
                )
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def related_products_filter_product_id(cls, request, product_id):
        subquery = (
            request.dbsession.query(
                text(
                    f"array_remove((array_agg(DISTINCT product_to_product.product1_id) || array_agg(DISTINCT product_to_product.product2_id)), {product_id})"
                )
            )
            .filter(
                or_(
                    models.ProductToProduct.product1_id == product_id,
                    models.ProductToProduct.product2_id == product_id,
                )
            )
            .scalar()
        )
        return (
            request.dbsession.query(models.Product.product_id, models.Product.name)
            .filter(models.Product.product_id.in_(subquery or {}))
            .all()
        )

    @classmethod
    def product_id_name_relation_type_filter_product_id(cls, request, product_id):
        return (
            request.dbsession.query(
                models.Product.product_id,
                models.Product.name,
                models.ProductToProduct.product_relation_type,
            )
            .join(
                models.Product,
                or_(
                    models.ProductToProduct.product2_id == models.Product.product_id,
                    models.ProductToProduct.product1_id == models.Product.product_id,
                ),
            )
            .filter(
                or_(
                    models.ProductToProduct.product1_id == product_id,
                    models.ProductToProduct.product2_id == product_id,
                ),
                models.Product.product_id != product_id,
            )
            .all()
        )

    @classmethod
    def product_to_product_id_name_order_image_filter_product_id(
        cls, request, product_id
    ):
        return (
            request.dbsession.query(
                models.Product.image,
                models.Product.model,
                models.Product.manufacturer_id,
                models.Product.name,
                models.Product.product_id,
                models.Product.product_type,
                models.Product.seo_slug,
                models.Product.status,
                models.Product.transport_method,
                models.ProductToProduct.order,
                models.ProductToProduct.product_component_id,
                models.ProductToProduct.product_relation_type,
                models.ProductToProduct.relation_settings,
            )
            .join(
                models.Product,
                # or_(
                models.ProductToProduct.product2_id == models.Product.product_id,
                # models.ProductToProduct.product1_id == models.Product.product_id,
                # ),
            )
            .filter(
                # or_(
                models.ProductToProduct.product1_id == product_id,
                # models.ProductToProduct.product2_id == product_id,
                # ),
                models.Product.product_id != product_id,
            )
            .order_by(asc(models.ProductToProduct.order))
            .all()
        )

    @classmethod
    def product_to_product_id_name_order_image_filter_product_list(
        cls, request, product_list, settings
    ):
        """
        Related products are treated like every other product - all data is returned.
        This allows further styling/marketing icons on such product.
        """
        query = (
            request.dbsession.query(
                models.Product.availability_id,
                models.Product.catalog_price,
                models.Product.catalog_price.label("special_price"),
                models.Product.catalog_price.label("ancient_special_price"),
                models.Product.discount_id,
                models.Product.ean,
                models.Product.image,
                models.Product.manufactured,
                models.Product.manufacturer_id,
                models.Product.minimum,
                models.Product.model,
                models.Product.name,
                models.Product.pieces,
                models.Product.price,
                models.Product.product_id,
                models.Product.product_type,
                models.Product.seo_slug,
                models.Product.square_meter,
                models.Product.status,
                models.Product.tax_id,
                models.Product.unit,
                models.Product.transport_method,
                models.ProductToProduct.order,
                models.ProductToProduct.product1_id.label("related_product1_id"),
                models.ProductToProduct.product2_id.label("related_product2_id"),
                models.ProductToProduct.product_component_id,
                models.ProductToProduct.product_relation_type,
                models.ProductToProduct.relation_settings,
                models.ProductToBand.band_id,
            )
            .join(
                models.ProductToProduct,
                models.ProductToProduct.product2_id == models.Product.product_id,
            )
            .outerjoin(
                models.ProductToBand,
                models.Product.product_id == models.ProductToBand.product_id,
            )
            .filter(
                models.ProductToProduct.product1_id.in_(product_list),
                models.Product.product_id.notin_(product_list),
            )
            .order_by(asc(models.ProductToProduct.order))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_to_product_id_name_order_image_reverse_relation_filter_product_list(
        cls, request, product_list, settings, product_relation_type
    ):
        """
        Related products are treated like every other product - all data is returned.
        This allows further styling/marketing icons on such product.
        """
        query = (
            request.dbsession.query(
                models.Product.availability_id,
                models.Product.catalog_price,
                models.Product.catalog_price.label("special_price"),
                models.Product.catalog_price.label("ancient_special_price"),
                models.Product.discount_id,
                models.Product.ean,
                models.Product.image,
                models.Product.manufactured,
                models.Product.manufacturer_id,
                models.Product.minimum,
                models.Product.model,
                models.Product.name,
                models.Product.pieces,
                models.Product.price,
                models.Product.product_id,
                models.Product.product_type,
                models.Product.seo_slug,
                models.Product.square_meter,
                models.Product.status,
                models.Product.tax_id,
                models.Product.unit,
                models.Product.transport_method,
                models.ProductToProduct.order,
                models.ProductToProduct.product1_id.label("related_product1_id"),
                models.ProductToProduct.product2_id.label("related_product2_id"),
                models.ProductToProduct.product_component_id,
                models.ProductToProduct.product_relation_type,
                models.ProductToProduct.relation_settings,
                models.ProductToBand.band_id,
            )
            .join(
                models.ProductToProduct,
                models.ProductToProduct.product1_id == models.Product.product_id,
            )
            .outerjoin(
                models.ProductToBand,
                models.Product.product_id == models.ProductToBand.product_id,
            )
            .filter(
                models.ProductToProduct.product2_id.in_(product_list),
                models.Product.product_id.notin_(product_list),
                models.ProductToProduct.product_relation_type == product_relation_type,
            )
            .order_by(asc(models.ProductToProduct.order))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_id_common_data(cls, request, product_id, settings):
        """
        Related products are treated like every other product - all data is returned.
        This allows further styling/marketing icons on such product.
        """
        # VERIFY NEW CODE!
        query = (
            request.dbsession.query(func.array_agg(models.Product.product_id))
            .join(
                models.ProductToProduct,
                or_(
                    models.ProductToProduct.product2_id == models.Product.product_id,
                    models.ProductToProduct.product1_id == models.Product.product_id,
                ),
            )
            .filter(
                or_(
                    models.ProductToProduct.product2_id == product_id,
                    models.ProductToProduct.product1_id == product_id,
                ),
                models.Product.product_id != product_id,
            )
            # .order_by(asc(models.ProductToProduct.order))
            # .group_by(models.ProductToProduct.order)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.scalar()

    @classmethod
    def product_id_unit_filter_product_list(cls, request, product_list):
        return (
            request.dbsession.query(models.Product.product_id, models.Product.unit)
            .filter(
                models.Product.product_id.in_(product_list),
            )
            .all()
        )

    @classmethod
    def product_to_product_id_filter_product_list(cls, request, product_list):
        """
        Related products are treated like every other product - all data is returned.
        This allows further styling/marketing icons on such product.
        """
        return (
            request.dbsession.query(
                models.ProductToProduct.product2_id.label("product_id")
            )
            .join(
                models.Product,
                models.ProductToProduct.product2_id == models.Product.product_id,
            )
            .filter(
                models.ProductToProduct.product1_id.in_(product_list),
                models.ProductToProduct.product_relation_type == 1,
                models.Product.product_id.notin_(product_list),
            )
            .all()
        )

    @classmethod
    def product_search(cls, request, searchstring, limit):
        query = (
            request.dbsession.query(
                models.Product.product_id,
                models.Product.name,
                models.Product.seo_slug,
                models.Product.ean,
                models.Product.model,
                models.Product.image,
                models.Product.transport_method,
                models.Product.product_type,
            )
            .order_by(asc(models.Product.name))
            .filter(
                or_(
                    models.Product.name.ilike(searchstring),
                    func.concat(models.Product.name, " ", models.Product.name).ilike(
                        searchstring
                    ),
                    models.Product.ean.ilike(searchstring),
                ),
                models.Product.status == 1,
            )
            .options(services.FromCache("redis-long"))
        )
        if limit:
            query = query.limit(21)
        return query

    @classmethod
    def products_paginator_filter_dynamic_v2(
        cls,
        request,
        filter_params,
        settings,
        tax_dict,
        item_count=None,
        manual_discount=False,
    ):
        (
            query,
            subquery_current_price_special,
            subquery_ancient_price_special,
        ) = cls.product_base_info_variables(request)
        if filter_params.status and filter_params.status == 1:
            query = query.filter(models.Product.status == 1)
        query = cls.product_query_filter_availability(filter_params.availability, query)
        query = cls.product_query_filter_searchstring(filter_params.searchstring, query)
        query = cls.product_query_filter_dimensions(filter_params.dimensions, query)
        query = cls.product_query_filter_manufacturer(filter_params.manufacturer, query)
        if manual_discount:
            query = query.filter(models.Product.manual_discount.is_(True))
        if filter_params.category_id:
            query = query.join(models.Product.product_to_category).filter(
                models.ProductToCategory.category_id == filter_params.category_id
            )
        if filter_params.special_product:
            query = query.join(models.ProductToSpecial)
        if filter_params.band:
            query = query.join(models.ProductToBand).filter(
                models.ProductToBand.band_id.in_(filter_params.band)
            )
        if filter_params.collection and filter_params.collection == 999:
            query = query.outerjoin(models.Product.product_to_collection).filter(
                models.ProductToCollection.collection_id.notin_(
                    filter_params.collection
                )
            )
        elif filter_params.collection:
            query = query.outerjoin(models.Product.product_to_collection).filter(
                models.ProductToCollection.collection_id.in_(filter_params.collection)
            )
        if filter_params.attributes:
            attributes_multiple = services.AttributeService.subquery_filter_attributes(
                request, filter_params.attributes, settings
            )
            query = query.filter(and_(*attributes_multiple))
        if filter_params.searchstring and filter_params.description:
            query = query.filter(
                models.ProductDescription.description.ilike(filter_params.searchstring)
            )
        if filter_params.price_min or filter_params.price_max:
            if filter_params.price_min:
                query = query.filter(
                    helpers.CommonHelpers.price_gross_number(
                        func.coalesce(
                            subquery_current_price_special.c.price, models.Product.price
                        ),
                        tax_dict[product.tax_id],
                    )
                    > float(filter_params.price_min)
                )
            if filter_params.price_max:
                query = query.filter(
                    helpers.CommonHelpers.price_gross_number(
                        func.coalesce(
                            subquery_current_price_special.c.price, models.Product.price
                        ),
                        tax_dict[product.tax_id],
                    )
                    < float(filter_params.price_max)
                )
        query = services.Cache.cache_query(settings["cache"], query)
        query = cls.product_query_sort_filter_dynamic(
            filter_params.sort_method, subquery_current_price_special, query, settings
        )
        query_params = request.GET.mixed()

        def url_maker(link_page):
            # replace page param with values generated by paginator
            query_params["page"] = link_page
            return request.current_route_url(_query=query_params)

        return SqlalchemyOrmPage(
            query,
            filter_params.page,
            items_per_page=filter_params.items_per_page,
            item_count=item_count,
            url_maker=url_maker,
        )

    @classmethod
    def product_query_sort_filter_dynamic(
        cls, sort_method, price_special, query, settings
    ):
        if sort_method == "name_asc":
            query = query.order_by(
                desc(models.Product.sort_order), asc(models.Product.name)
            )
        elif sort_method == "name_desc":
            query = query.order_by(
                desc(models.Product.sort_order), desc(models.Product.name)
            )
        elif sort_method == "price_asc":
            query = query.order_by(
                desc(models.Product.sort_order),
                asc((func.coalesce(price_special.c.price, models.Product.price))),
            )
        elif sort_method == "price_desc":
            query = query.order_by(
                desc(models.Product.sort_order),
                desc((func.coalesce(price_special.c.price, models.Product.price))),
            )
        elif sort_method == "mod_asc":
            query = query.order_by(asc(models.Product.date_modified))
        elif sort_method == "mod_desc":
            query = query.order_by(desc(models.Product.date_modified))
        elif sort_method == "add_asc":
            query = query.order_by(asc(models.Product.date_added))
        elif sort_method == "add_desc":
            query = query.order_by(desc(models.Product.date_added))
        elif sort_method == "pop":
            query = query.order_by(
                desc(models.Product.sort_order), asc(models.Product.viewed)
            )
        else:
            default_sorting_mehtod = settings["catalog_settings"].get(
                "default_sorting_method", "name_asc"
            )
            query = query.order_by(
                # desc(models.Product.sort_order),
                desc(models.Product.date_added),
                asc(models.Product.name),
            )
        return query

    @classmethod
    def special_products_filter_date(cls, request, settings):
        price_special = (
            request.dbsession.query(
                models.ProductToSpecial.product_id,
                models.ProductToSpecial.special_id,
                models.ProductToSpecial.price,
            )
            .filter(
                func.date(models.ProductToSpecial.date_start) <= func.current_date()
            )
            .filter(
                or_(
                    func.date(models.ProductToSpecial.date_end) == None,
                    exists().where(
                        func.date(models.ProductToSpecial.date_end)
                        >= func.current_date()
                    ),
                )
            )
        )
        if cache:
            price_special = price_special.options(services.FromCache("redis-long"))
        price_special = price_special.subquery()
        query = (
            request.dbsession.query(
                models.Product.product_id,
                models.Product.image,
                models.Product.name,
                models.Product.price,
                models.Product.seo_slug,
                models.Product.square_meter,
                price_special.c.special_id,
                price_special.c.price,
            )
            .filter(models.Product.status == 1)
            .join(
                price_special, price_special.c.product_id == models.Product.product_id
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_aggregator_v2(
        cls,
        request,
        filter_params,
        settings,
        status=1,
        manual_discount=False,
        language_id=2,
    ):
        query = request.dbsession.query(
            func.array_agg(func.distinct(models.Product.availability_id)).label(
                "availabilities"
            ),
            func.array_agg(func.distinct(models.Product.manufacturer_id)).label(
                "manufactuters"
            ),
            func.count(models.Product.product_id).label("item_count"),
            # func.array_agg(func.distinct(models.Product.manufacturer_id)).label("manufacturers"),
        )
        if status and status == 1:
            query = query.filter(models.Product.status == 1)
        if filter_params.attributes:
            attributes_multiple = services.AttributeService.subquery_filter_attributes(
                request, filter_params.attributes, settings
            )
            query = query.filter(and_(*attributes_multiple))
        query = cls.product_query_filter_availability(filter_params.availability, query)
        query = cls.product_query_filter_dimensions(filter_params.dimensions, query)
        query = cls.product_query_filter_manufacturer(filter_params.manufacturer, query)
        query = cls.product_query_filter_searchstring(filter_params.searchstring, query)
        if filter_params.band:
            query = query.join(models.ProductToBand).filter(
                models.ProductToBand.band_id.in_(filter_params.band)
            )
        if filter_params.category_id:
            query = query.join(models.ProductToCategory).filter(
                models.ProductToCategory.category_id == filter_params.category_id
            )
        if filter_params.collection and filter_params.collection == 999:
            query = query.outerjoin(models.ProductToCollection).filter(
                models.ProductToCollection.collection_id.notin_(
                    filter_params.collection
                )
            )
        elif filter_params.collection:
            query = query.outerjoin(models.ProductToCollection).filter(
                models.ProductToCollection.collection_id.in_(filter_params.collection)
            )

        query = cls.product_query_filter_price(filter_params, query)
        query = services.Cache.cache_query(settings["cache"], query)
        if manual_discount:
            query = query.filter(models.Product.manual_discount.is_(True))
        return query.one()

    @classmethod
    def products_filter_collection_date(
        cls,
        request,
        collection,
        settings,
        product_id=None,
    ):
        subquery_price_special = (
            services.ProductToSpecialService.price_subquery_special_id_filter_date(
                request, 0
            )
        )
        subquery_ancient_special_price = (
            services.ProductToSpecialService.price_subquery_special_id_filter_date(
                request, 30
            )
        )
        query = (
            request.dbsession.query(
                models.Product.product_id,
                # models.Product.allowed_free_transport,
                # #models.Product.allowed_free_transport_minimum_amount,
                models.Product.availability_id,
                models.Product.catalog_price,
                # models.Product.forbidden_transport_methods,
                # models.Product.forbidden_payment_methods,
                models.Product.discount_id,
                models.Product.ean,
                models.Product.image,
                models.Product.manual_discount,
                models.Product.manufactured,
                models.Product.manufacturer_id,
                models.Product.minimum,
                models.Product.model,
                models.Product.name,
                models.Product.pieces,
                models.Product.price,
                models.Product.product_type,
                models.Product.seo_slug,
                models.Product.sort_order,
                models.Product.square_meter,
                models.Product.status,
                models.Product.tax_id,
                models.Product.unit,
                models.Product.transport_method,
                subquery_price_special.c.price.label("special_price"),
                subquery_ancient_special_price.c.price.label("ancient_special_price"),
                # subquery_price_special.c.special_id.label("special_id"),
            )
            .join(
                models.ProductToCollection,
                models.ProductToCollection.product_id == models.Product.product_id,
            )
            .outerjoin(
                subquery_price_special,
                subquery_price_special.c.product_id == models.Product.product_id,
            )
            .outerjoin(
                subquery_ancient_special_price,
                subquery_ancient_special_price.c.product_id
                == models.Product.product_id,
            )
            .filter(
                models.Product.status == 1,
                models.Product.product_id != product_id,
                models.ProductToCollection.collection_id.in_(collection),
            )
        ).group_by(
            models.Product.product_id,
            # subquery_price_special.c.special_id,
            subquery_price_special.c.price,
            subquery_ancient_special_price.c.price,
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_id_collectons_manufacturer_avaliability_filter_product_list(
        cls, request, settings, product_list
    ):
        query = (
            request.dbsession.query(
                models.Product.product_id,
                func.array_agg(models.Collection.name).label("collection_name"),
            )
            .select_from(models.Product, models.Collection)
            .outerjoin(
                models.ProductToCollection,
                models.ProductToCollection.product_id == models.Product.product_id,
            )
            .outerjoin(
                models.Collection,
                models.ProductToCollection.collection_id
                == models.Collection.collection_id,
            )
            .filter(
                models.Product.product_id.in_(product_list),
                models.Collection.status == 1,
            )
            .group_by(models.Product.product_id)
        )

        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_id_storage_id_quantity_filter_product_list(
        cls, request, settings, product_list
    ):
        """
        One product type can be at the same in few different storage locations/types
        """
        query = (
            request.dbsession.query(
                models.ProductToStorage.product_storage_id,
                models.ProductToStorage.quantity,
                models.ProductToStorage.storage_id,
                models.Product.product_id,
                models.Product.unit,
                models.Storage.storage_type,
            )
            .outerjoin(
                models.Product,
                models.Product.product_id == models.ProductToStorage.product_id,
            )
            .outerjoin(
                models.Storage,
                models.Storage.storage_id == models.ProductToStorage.storage_id,
            )
            .filter(models.Product.product_id.in_(product_list))
            # .group_by(models.Product.product_id)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_id_storage_id_quantity_filter_product_list_physical_storage(
        cls,
        request,
        product_list,
        settings,
    ):
        """
        One product type can be at the same in few different storage locations/types
        """
        query = (
            request.dbsession.query(models.ProductToStorage)
            .join(models.Storage)
            .filter(
                models.Storage.storage_type == 1,
                models.ProductToStorage.product_id.in_(product_list),
            )
            # .group_by(models.Product.product_id)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_id_storage_id_quantity_filter_ean_list(
        cls, request, ean_list, settings
    ):
        """
        One product type can be at the same in few different storage locations/types
        """
        query = (
            request.dbsession.query(
                models.ProductToStorage.product_storage_id,
                models.ProductToStorage.quantity,
                models.ProductToStorage.storage_id,
                models.Product.ean,
                models.Product.unit,
                models.Product.product_id,  # Provides all storages for product
                models.Storage.storage_type,
            )
            .outerjoin(
                models.Product,
                models.Product.product_id == models.ProductToStorage.product_id,
            )
            .outerjoin(
                models.Storage,
                models.Storage.storage_id == models.ProductToStorage.storage_id,
            )
            .filter(models.Product.ean.in_(ean_list))
        )
        # query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_storage_filter_storage_ean_list(
        cls, request, ean_list, storage_id, settings
    ):
        """
        One product type can be at the same in few different storage locations/types
        """
        query = (
            request.dbsession.query(models.ProductToStorage)
            .filter(
                models.ProductToStorage.storage_id == storage_id,
                models.Product.ean.in_(ean_list),
            )
            .join(
                models.Product,
                models.Product.product_id == models.ProductToStorage.product_id,
            )
        )
        return query.all()

    @classmethod
    def product_id_filter_ean(
        cls,
        request,
    ):
        """"""
        query = request.dbsession.query(
            func.array_agg(models.Product.product_id)
        ).filter(
            models.Product.ean.is_not(None),
        )
        return query.scalar()

    @classmethod
    def product_id_filter_ean_no_storage(
        cls,
        request,
    ):
        """
        There was(?) a bug where product was cerated without any record on storage. This adds missing data.
        """

        query = (
            request.dbsession.query(func.array_agg(models.Product.product_id))
            .outerjoin(models.ProductToStorage)
            .filter(
                models.Product.ean.is_not(None),
                models.ProductToStorage.product_storage_id == None,
            )
        )
        return query.scalar()

    @classmethod
    def product_storages_filter_product_id(cls, request, product_id):
        query = request.dbsession.query(models.ProductToStorage).filter(
            models.ProductToStorage.product_id == product_id
        )
        return query.all()

    @classmethod
    def product_storages_filter_storage_list(cls, request, storage_list):
        query = request.dbsession.query(models.ProductToStorage).filter(
            models.ProductToStorage.product_storage_id.in_(storage_list)
        )
        return query.all()

    @classmethod
    def product_list_condensed(cls, request, product_list):
        sub_coll = (
            (
                request.dbsession.query(
                    models.Product.product_id,
                    func.array_agg(models.ProductToCollection.collection_id).label(
                        "collections"
                    ),
                )
            )
            .outerjoin(
                models.ProductToCollection,
                models.ProductToCollection.product_id == models.Product.product_id,
            )
            .outerjoin(models.Collection)
            .filter(
                models.Product.product_id.in_(product_list),
                models.Collection.status == 1,
            )
            .group_by(models.Product.product_id)
            .subquery()
        )
        sub_attr = (
            (
                request.dbsession.query(
                    models.Product.product_id,
                    func.array_agg(models.ProductToAttribute.attribute_id).label(
                        "attributes"
                    ),
                )
            )
            .outerjoin(
                models.ProductToAttribute,
                models.ProductToAttribute.product_id == models.Product.product_id,
            )
            .outerjoin(
                models.Attribute,
                models.ProductToAttribute.attribute_id == models.Attribute.attribute_id,
            )
            .outerjoin(
                models.AttributeGroup,
                models.AttributeGroup.attribute_group_id
                == models.Attribute.attribute_group_id,
            )
            .filter(
                models.Product.product_id.in_(product_list),
                models.Attribute.status == 1,
                models.AttributeGroup.status == 1,
            )
            .group_by(models.Product.product_id)
            .subquery()
        )
        query = (
            request.dbsession.query(
                models.Product.product_id,
                models.Manufacturer.manufacturer_id,
                models.Manufacturer.name,
                sub_coll.c.collections,
                sub_attr.c.attributes,
            )
            .outerjoin(sub_coll, sub_coll.c.product_id == models.Product.product_id)
            .outerjoin(sub_attr, sub_attr.c.product_id == models.Product.product_id)
            .outerjoin(models.Product.manufacturer)
            .filter(models.Product.product_id.in_(product_list))
            .group_by(
                models.Product.product_id,
                models.Manufacturer.manufacturer_id,
                models.Manufacturer.name,
                sub_coll.c.collections,
                sub_attr.c.attributes,
            )
            .options(services.FromCache("redis-long"))
            .order_by(asc(models.Product.product_id))
        )
        return query.all()

    @classmethod
    def sitemap(cls, request):
        query = request.dbsession.query(
            models.Product.name,
            models.Product.date_modified,
            models.Product.product_id,
            models.Product.seo_slug,
        ).filter(models.Product.status == 1)
        query = query.options(services.FromCache("redis-short"))
        return query.all()

    @classmethod
    def disabled_products(cls, request):
        query = (
            request.dbsession.query(func.array_agg(models.Product.product_id))
            .filter(models.Product.status == 0)
            .options(services.FromCache("redis-eternal"))
        )

        return query.scalar()

    @classmethod
    def unavailable_products(cls, request, disabled_manufacturers, limited_status=None):
        query = (
            request.dbsession.query(
                func.array_agg(models.Product.product_id)
            ).outerjoin(models.ProductToXML)
        ).options(services.FromCache("redis-eternal"))
        if limited_status:
            query = query.filter(
                or_(
                    models.Product.manufacturer_id.in_(disabled_manufacturers),
                    models.Product.availability_id == limited_status,
                    models.Product.status == 0,
                    models.ProductToXML.excluded == True,
                ),
            )
        else:
            query = query.filter(
                or_(
                    models.Product.manufacturer_id.in_(disabled_manufacturers),
                    models.Product.status == 0,
                    models.ProductToXML.excluded == True,
                ),
            )
        return query.scalar()

    @classmethod
    def products_with_grouped_categories(
        cls, request, disabled_categories, disabled_products
    ):
        categories = (
            request.dbsession.query(
                models.ProductToCategory.product_id,
                (models.Category.name).label("name"),
            )
            .join(models.Category)
            .filter(models.Category.category_id.notin_(disabled_categories))
        ).subquery()
        query = (
            (
                request.dbsession.query(
                    models.Product.product_id,
                    func.array_agg(categories.c.name).label("categories"),
                )
                .outerjoin(
                    categories, categories.c.product_id == models.Product.product_id
                )
                .filter(models.Product.product_id.notin_(disabled_products))
                .group_by(models.Product.product_id)
            ).order_by(models.Product.product_id)
            # .options(services.FromCache("redis-eternal"))
        )
        return query.all()

    @classmethod
    def products_with_grouped_collections(
        cls, request, disabled_collections, disabled_products
    ):
        collections = (
            request.dbsession.query(
                models.ProductToCollection.product_id, models.Collection.name
            )
            .join(models.Collection)
            .filter(models.Collection.collection_id.notin_(disabled_collections))
            .options(services.FromCache("redis-eternal"))
        ).subquery()
        query = (
            (
                request.dbsession.query(
                    models.Product.product_id,
                    func.array_agg(collections.c.name).label("collections"),
                )
                .outerjoin(
                    collections, collections.c.product_id == models.Product.product_id
                )
                .filter(models.Product.product_id.notin_(disabled_products))
                .group_by(models.Product.product_id)
            )
            .order_by(models.Product.product_id)
            .options(services.FromCache("redis-eternal"))
        )
        return query.all()

    @classmethod
    def products_with_grouped_additional_images(cls, request, disabled_products):
        images = (
            request.dbsession.query(models.ProductToImage)
            .filter(models.ProductToImage.product_id.notin_(disabled_products))
            .options(services.FromCache("redis-eternal"))
        ).subquery()
        query = (
            (
                request.dbsession.query(
                    models.Product.product_id,
                    func.array_agg(images.c.image).label("image"),
                )
                .outerjoin(images, images.c.product_id == models.Product.product_id)
                .filter(models.Product.product_id.notin_(disabled_products))
                .options(Load(models.Product).load_only("product_id"))
                .group_by(models.Product.product_id)
            )
            .order_by(models.Product.product_id)
            .options(services.FromCache("redis-eternal"))
        )
        return query.all()

    @classmethod
    def select_product_thumbnails(cls, request):
        query = (
            request.dbsession.query(models.Product, models.ProductToBand.band_id)
            .filter(models.Product.image != None)
            .join(models.ProductToBand)
        )
        # .options(services.FromCache("redis-eternal"))
        return query.all()

    @classmethod
    def select_product_thumbnails_with_band(cls, request):
        query = (
            request.dbsession.query(models.Product.image, models.ProductToBand.band_id)
            .filter(models.Product.image != None)
            .join(models.ProductToBand)
        )
        # .options(services.FromCache("redis-eternal"))
        return query.all()

    @classmethod
    def select_additional_product_thumbnails(cls, request):
        query = (
            request.dbsession.query(
                models.ProductToImage, models.Product.manufacturer_id
            )
            .join(models.Product)
            .filter(models.Product.image != None)
        )
        # .options(services.FromCache("redis-eternal"))
        return query.all()

    @classmethod
    def select_additional_product_thumbnails_with_band(cls, request):
        query = (
            request.dbsession.query(models.ProductToImage, models.ProductToBand.band_id)
            .outerjoin(
                models.ProductToBand,
                models.ProductToBand.product_id == models.ProductToImage.product_id,
            )
            .filter(models.ProductToImage.image != None)
        )
        # .options(services.FromCache("redis-eternal"))
        return query.all()

    @classmethod
    def select_extra_product_thumbnails(cls, request):
        query = request.dbsession.query(models.ProductToImage).filter(
            models.ProductToImage.image != None
        )
        # .options(services.FromCache("redis-eternal"))
        return query.all()

    @classmethod
    def product_query_filter_price(cls, filter_params, subquery):
        if filter_params.price_max or filter_params.price_min:
            subquery = subquery.outerjoin(
                models.ProductToSpecial,
                and_(
                    models.ProductToSpecial.product_id == models.Product.product_id,
                    models.ProductToSpecial.special_id.in_((2, 4)),
                    # models.ProductToSpecial.default == True,
                    func.date(models.ProductToSpecial.date_start)
                    <= func.current_date(),
                    or_(
                        func.date(models.ProductToSpecial.date_end) == None,
                        exists().where(
                            func.date(models.ProductToSpecial.date_end)
                            >= func.current_date()
                        ),
                    ),
                ),
            )
        if filter_params.price_max:
            subquery = subquery.filter(
                (func.coalesce(models.ProductToSpecial.price, models.Product.price))
                * 1.23
                < float(filter_params.price_max)
            )
        if filter_params.price_min:
            subquery = subquery.filter(
                (func.coalesce(models.ProductToSpecial.price, models.Product.price))
                * 1.23
                > float(filter_params.price_min)
            )
        return subquery

    @classmethod
    def product_special_query_filter_price(cls, filter_params, subquery):
        if filter_params.price_max or filter_params.price_min:
            subquery = subquery.filter(
                models.ProductToSpecial.special_id.in_((2, 4)),
                # models.ProductToSpecial.default == True,
                func.date(models.ProductToSpecial.date_start) <= func.current_date(),
                or_(
                    func.date(models.ProductToSpecial.date_end) == None,
                    exists().where(
                        func.date(models.ProductToSpecial.date_end)
                        >= func.current_date()
                    ),
                ),
            )
        if filter_params.price_max:
            subquery = subquery.filter(
                (func.coalesce(models.ProductToSpecial.price, models.Product.price))
                * 1.23
                < float(filter_params.price_max)
            )
        if filter_params.price_min:
            subquery = subquery.filter(
                (func.coalesce(models.ProductToSpecial.price, models.Product.price))
                * 1.23
                > float(filter_params.price_min)
            )
        return subquery

    @classmethod
    def product_categories_array(cls, request, settings):
        query = (
            request.dbsession.query(
                models.Product.product_id,
                func.array_agg(models.ProductToCategory.category_id).label(
                    "categories"
                ),
            )
            .join(models.ProductToCategory)
            .group_by(models.Product.product_id)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_availability_array(cls, request, availability):
        query = (
            request.dbsession.query(
                models.Product, models.ProductToAttribute.attribute_id
            )
            .join(models.ProductToAttribute)
            .filter(models.ProductToAttribute.attribute_id.in_(availability))
            .all()
        )
        return query

    @classmethod
    def product_object_filter_ean(cls, request, product_eans):
        return (
            request.dbsession.query(models.Product)
            .filter(models.Product.ean.in_(product_eans))
            .order_by(models.Product.name)
        ).all()

    @classmethod
    def product_object_filter_discount_id(cls, request, discount_id):
        return (
            request.dbsession.query(models.Product)
            .filter(models.Product.discount_id == discount_id)
            .all()
        )

    @classmethod
    def product_query_filter_searchstring(cls, searchstring, query):
        if searchstring:
            query = query.filter(
                or_(
                    *[models.Product.name.ilike(y) for y in searchstring.split()],
                    models.Product.name.ilike(searchstring),
                    func.concat(models.Product.name, " ", models.Product.name).ilike(
                        searchstring
                    ),
                    models.Product.ean.ilike(searchstring),
                )
            )
        return query

    @classmethod
    def product_query_filter_manufacturer(cls, manufacturer, query):
        if manufacturer:
            query = query.filter(models.Product.manufacturer_id.in_(manufacturer))
        return query

    @classmethod
    def product_query_filter_dimensions(cls, dimensions, query):
        if dimensions:
            for item in dimensions:
                if "width" in item.name:
                    if "min" in item.name:
                        query = query.filter(models.Product.width >= item.value)
                    else:
                        query = query.filter(models.Product.width <= item.value)
                if "length" in item.name:
                    if "min" in item.name:
                        query = query.filter(models.Product.length >= item.value)
                    else:
                        query = query.filter(models.Product.length <= item.value)
        return query

    @classmethod
    def product_query_filter_availability(cls, availability, query):
        if availability:
            query = query.filter(models.Product.availability_id.in_(availability))
        return query

    @classmethod
    def product_description_query_filter_description_searchstring(
        cls, filter_params, query
    ):
        if filter_params.searchstring and filter_params.description:
            query = query.filter(
                models.ProductDescription.description.ilike(filter_params.searchstring)
            )
        return query

    @classmethod
    def product_query_filter_status(cls, status, query):
        if status:
            query = query.filter(models.Product.status == status)
        return query

    @classmethod
    def delete_all_products_filter_manufacturer_id(cls, request, manufacturer_id):
        query = request.dbsession.query(models.Product).filter(
            models.Product.manufacturer_id == manufacturer_id
        )
        return query.delete(synchronize_session=False)

    @classmethod
    def delete_all_product_description_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductDescription.product_description_id)
            )
            .join(models.Product)
            .filter(models.Product.manufacturer_id == manufacturer_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductDescription).filter(
                models.ProductDescription.product_description_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)

    # @classmethod
    # def delete_all_product_virtual_filter_manufacturer_id(
    #     cls, request, manufacturer_id
    # ):
    #     if (
    #         subquery := request.dbsession.query(
    #             func.array_agg(models.ProductToVirtual.product_virtual_id)
    #         )
    #         .join(models.Product)
    #         .filter(models.Product.manufacturer_id == manufacturer_id)
    #         .scalar()
    #     ):
    #         query = request.dbsession.query(models.ProductToVirtual).filter(
    #             models.ProductToVirtual.product_virtual_id.in_(subquery)
    #         )

    #         return query.delete(synchronize_session=False)

    @classmethod
    def delete_all_products_filter_band_id(cls, request, band_id):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductToBand.product_id)
            )
            .filter(models.ProductToBand.band_id == band_id)
            .scalar()
        ):
            query = request.dbsession.query(models.Product).filter(
                models.Product.product_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)

    @classmethod
    def delete_all_product_description_filter_band_id(cls, request, band_id):
        if (
            subquery := request.dbsession.query(
                func.array_agg(models.ProductDescription.product_description_id)
            )
            .join(models.Product, models.ProductToBand)
            .filter(models.ProductToBand.band_id == band_id)
            .scalar()
        ):
            query = request.dbsession.query(models.ProductDescription).filter(
                models.ProductDescription.product_description_id.in_(subquery)
            )

            return query.delete(synchronize_session=False)

    # @classmethod
    # def delete_all_product_virtual_filter_band_id(cls, request, band_id):
    #     if (
    #         subquery := request.dbsession.query(
    #             func.array_agg(models.ProductToVirtual.product_virtual_id)
    #         )
    #         .join(models.Product, models.ProductToBand)
    #         .filter(models.ProductToBand.band_id == band_id)
    #         .scalar()
    #     ):
    #         query = request.dbsession.query(models.ProductToVirtual).filter(
    #             models.ProductToVirtual.product_virtual_id.in_(subquery)
    #         )

    #         return query.delete(synchronize_session=False)

    @classmethod
    def special_products_filter_date_active_v2(cls, request, settings, product_id=None):
        subquery_current_price_special = (
            services.ProductToSpecialService.price_subquery_special_id_filter_date(
                request, 0
            )
        )
        subquery_ancient_special_price = (
            services.ProductToSpecialService.price_subquery_special_id_filter_date(
                request, 30
            )
        )
        current_special = services.ProductToSpecialService.current_special_subquery(
            request
        )
        query = (
            request.dbsession.query(
                models.Product.product_id,
                models.Product.availability_id,
                models.Product.catalog_price,
                models.Product.discount_id,
                models.Product.ean,
                models.Product.image,
                models.Product.manual_discount,
                models.Product.manufactured,
                models.Product.manufacturer_id,
                models.Product.minimum,
                models.Product.model,
                models.Product.name,
                models.Product.pieces,
                models.Product.price,
                models.Product.product_type,
                models.Product.seo_slug,
                models.Product.sort_order,
                models.Product.square_meter,
                models.Product.status,
                models.Product.tax_id,
                models.Product.unit,
                models.Product.transport_method,
                subquery_current_price_special.c.price.label("special_price"),
                subquery_ancient_special_price.c.price.label("ancient_special_price"),
            )
            .outerjoin(
                subquery_current_price_special,
                subquery_current_price_special.c.product_id
                == models.Product.product_id,
            )
            .outerjoin(
                subquery_ancient_special_price,
                subquery_ancient_special_price.c.product_id
                == models.Product.product_id,
            )
            .group_by(
                models.Product.product_id,
                subquery_current_price_special.c.price.label("special_price"),
                subquery_ancient_special_price.c.price.label("ancient_special_price"),
            )
            .join(
                current_special,
                current_special.c.product_id == models.Product.product_id,
            )
            .filter(models.Product.product_id != product_id, models.Product.status != 0)
        )

        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def special_product_id_filter_date_active(cls, request, settings):
        current_special = services.ProductToSpecialService.current_special_subquery(
            request
        )
        query = (
            request.dbsession.query(
                models.Product.product_id,
            )
            .group_by(
                models.Product.product_id,
            )
            .join(
                current_special,
                current_special.c.product_id == models.Product.product_id,
            )
            .filter(models.Product.status != 0)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def special_products_filter_free_delivery(cls, request):
        query = request.dbsession.query(
            models.Product.product_id,
            models.Product.allowed_free_transport,
            models.Product.allowed_free_transport_minimum_amount,
        ).filter(models.Product.allowed_free_transport == 1)
        return query.all()

    @classmethod
    def special_products_filter_large_delivery(cls, request):
        query = request.dbsession.query(
            models.Product.product_id, models.Product.forbidden_transport_methods
        )
        return query.all()

    @classmethod
    def products_filter_band_date_product(
        cls, request, band_list, product_id, settings
    ):
        (
            query,
            subquery_current_price_special,
            subquery_ancient_special_price,
        ) = cls.product_base_info_variables(request)
        query = (
            query.join(models.ProductToBand)
            .filter(models.ProductToBand.band_id.in_(band_list))
            .filter(models.Product.product_id != product_id)
        )

        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def product_base_info_variables(cls, request):
        subquery_current_price_special = (
            services.ProductToSpecialService.price_subquery_special_id_filter_date(
                request, 0
            )
        )
        subquery_ancient_special_price = (
            services.ProductToSpecialService.price_subquery_special_id_filter_date(
                request, 30
            )
        )
        query = (
            request.dbsession.query(
                models.Product.product_id,
                models.Product.availability_id,
                models.Product.catalog_price,
                models.Product.discount_id,
                models.Product.ean,
                models.Product.image,
                models.Product.manual_discount,
                models.Product.manufactured,
                models.Product.manufacturer_id,
                models.Product.minimum,
                models.Product.model,
                models.Product.name,
                models.Product.pieces,
                models.Product.price,
                models.Product.product_type,
                models.Product.purchase_price,
                models.Product.seo_slug,
                models.Product.sku,
                models.Product.sort_order,
                models.Product.square_meter,
                models.Product.status,
                models.Product.tax_id,
                models.Product.unit,
                models.Product.transport_method,
                models.Product.weight,
                subquery_current_price_special.c.price.label("special_price"),
                subquery_ancient_special_price.c.price.label("ancient_special_price"),
            )
            .outerjoin(
                subquery_current_price_special,
                subquery_current_price_special.c.product_id
                == models.Product.product_id,
            )
            .outerjoin(
                subquery_ancient_special_price,
                subquery_ancient_special_price.c.product_id
                == models.Product.product_id,
            )
        )
        return (query, subquery_current_price_special, subquery_ancient_special_price)

    @classmethod
    def product_base_info_description_variables(cls, request):
        # add_special_type_to_subquery = services.ProductToSpecialService.current_price_subquery_special_id(   request, date)
        subquery_current_special_price = (
            services.ProductToSpecialService.price_subquery_special_id_filter_date(
                request, 0
            )
        )
        subquery_ancient_special_price = (
            services.ProductToSpecialService.price_subquery_special_id_filter_date(
                request, 30
            )
        )
        query = (
            request.dbsession.query(
                models.Product.availability_id,
                models.Product.catalog_price,
                models.Product.date_available,
                models.Product.discount_id,
                models.Product.ean,
                models.Product.height,
                models.Product.image,
                models.Product.length,
                models.Product.manual_discount,
                models.Product.manufactured,
                models.Product.manufacturer_id,
                models.Product.minimum,
                models.Product.model,
                models.Product.name,
                models.Product.pieces,
                models.Product.price,
                models.Product.product_id,
                models.Product.product_type,
                models.Product.seo_slug,
                models.Product.setting_template_id,
                models.Product.sku,
                models.Product.sort_order,
                models.Product.square_meter,
                models.Product.status,
                models.Product.tax_id,
                models.Product.transport_method,
                models.Product.unit,
                models.Product.weight,
                models.Product.width,
                models.Product.display_storage_type,
                models.ProductDescription.description,
                models.ProductDescription.description_technical,
                subquery_current_special_price.c.price.label("special_price"),
                subquery_ancient_special_price.c.price.label("ancient_special_price"),
            )
            .outerjoin(
                subquery_current_special_price,
                subquery_current_special_price.c.product_id
                == models.Product.product_id,
            )
            .outerjoin(
                subquery_ancient_special_price,
                subquery_ancient_special_price.c.product_id
                == models.Product.product_id,
            )
            .outerjoin(
                models.ProductDescription,
                models.ProductDescription.product_id == models.Product.product_id,
            )
        )
        return (query, subquery_current_special_price, subquery_ancient_special_price)

    @classmethod
    def product_base_price(cls, request):
        subquery_ancient_special_price = (
            services.ProductToSpecialService.price_subquery_special_id_filter_date(
                request, 30
            )
        )
        query = request.dbsession.query(
            models.Product.product_id,
            models.Product.availability_id,
            models.Product.catalog_price,
            models.Product.discount_id,
            models.Product.ean,
            models.Product.image,
            models.Product.manual_discount,
            models.Product.manufactured,
            models.Product.manufacturer_id,
            models.Product.minimum,
            models.Product.model,
            models.Product.name,
            models.Product.one_batch,
            models.Product.pieces,
            models.Product.price,
            models.Product.product_type,
            models.Product.seo_slug,
            models.Product.sku,
            models.Product.sort_order,
            models.Product.square_meter,
            models.Product.status,
            models.Product.tax_id,
            models.Product.unit,
            models.Product.transport_method,
            models.Product.weight,
            subquery_ancient_special_price.c.price.label("ancient_special_price"),
        ).outerjoin(
            subquery_ancient_special_price,
            subquery_ancient_special_price.c.product_id == models.Product.product_id,
        )
        return query

    @classmethod
    def product_minimum_unit(cls, request, product_id):
        query = request.dbsession.query(
            models.Product.product_id,
            models.Product.minimum,
            models.Product.unit,
        ).filter(models.Product.product_id == product_id)
        return query.one()

    @classmethod
    def file_upload_paths(cls, request):
        query = (
            request.dbsession.query(
                models.Band.band_id,
                models.Manufacturer.seo_slug.label("manufacturer_slug"),
                models.Band.seo_slug.label("band_slug"),
            )
            .join(
                models.Product,
                models.Manufacturer.manufacturer_id == models.Product.manufacturer_id,
            )
            .outerjoin(
                models.ProductToBand,
                models.ProductToBand.product_id == models.Product.product_id,
            )
            .outerjoin(models.Band, models.ProductToBand.band_id == models.Band.band_id)
            .group_by(
                models.Band.band_id,
                models.Band.seo_slug,
                models.Manufacturer.seo_slug,
            )
        )
        # query = services.Cache.cache_query(settings["cache"], query)
        return query.all()
