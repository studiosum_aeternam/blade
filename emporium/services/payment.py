import sqlalchemy as sa
from emporium import models


class PaymentService(object):
    @classmethod
    def payment_filter_active(cls, request, settings):
        return (
            request.dbsession.query(models.Payment)
            .filter(models.Payment.status == True)
            .order_by(sa.desc(models.Payment.name))
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    # @classmethod
    # def payment_filter_active_excluded(cls, request, excluded_list):
    #     return (
    #         request.dbsession.query(models.Payment)
    #         .filter(
    #             models.Payment.status == True,
    #             ~models.Payment.payment_id.in_(excluded_list),
    #         )
    #         .order_by(sa.desc(models.Payment.name))
    #         .all()
    #     )

    @classmethod
    def payment_all(cls, request):
        return (
            request.dbsession.query(models.Payment)
            .order_by(sa.desc(models.Payment.name))
            .all()
        )

    @classmethod
    def payment_settings_filter_module_name(cls, request, module_name, settings):
        # Returned JSONB field as a scalar is treated like a dictionary
        return (
            request.dbsession.query(models.Payment.module_settings)
            .filter(models.Payment.module_name == module_name)
            .scalar()
        )

    @classmethod
    def payment_by_id(cls, request, _id):
        return request.dbsession.query(models.Payment).get(_id)

    # @classmethod
    # def by_name(cls, request, payment_method):
    #     return request.dbsession.query(models.Payment).filter_by(name=payment_method).first()

    @classmethod
    def payment_excerpt_name_status_visibility_filter_status(
        cls,
        request,
        settings,
        status=True,
    ):
        return (
            request.dbsession.query(
                # models.Payment.special_method,
                models.Payment.description,
                models.Payment.excerpt,
                models.Payment.module_name,
                models.Payment.module_settings,
                models.Payment.name,
                models.Payment.payment_id,
                models.Payment.type,
            )
            .order_by(sa.asc(models.Payment.name))
            .filter(models.Payment.status == status)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()
