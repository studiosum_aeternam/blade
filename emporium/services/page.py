import sqlalchemy as sa
from emporium import models, services


class PageService(object):
    @classmethod
    def active(cls, request):
        query = request.dbsession.query(models.Page).filter(models.Page.status == True)
        return query.all()

    @classmethod
    def all(cls, request):
        query = request.dbsession.query(models.Page)
        return query.order_by(sa.asc(models.Page.title))

    @classmethod
    def by_id(cls, request, _id):
        query = request.dbsession.query(models.Page)
        return query.get(_id)

    @classmethod
    def page_with_content_filter_pagename(cls, request, pagename, settings):
        query = (
            request.dbsession.query(
                models.PageContent.page_content_id,
                models.PageContent.title,
                models.PageContent.content,
                models.PageContent.content_extended,
                models.Page.page_name,
                models.Page.page_id,
            )
            .join(models.Page)
            .filter(
                models.Page.page_name == pagename,
                models.PageContent.language_id == request.language_id,
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.first()

    @classmethod
    def page_with_content_filter_page_id_language(cls, request, page_id, settings):
        query = (
            request.dbsession.query(models.Page, models.PageContent)
            .join(models.PageContent)
            .filter(
                models.Page.page_id == page_id,
                models.PageContent.language_id == request.language_id,
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.first()

    @classmethod
    def page_with_content_filter_page_names_current_language(
        cls, request, menu, settings
    ):
        query = (
            request.dbsession.query(
                models.PageContent.page_content_id,
                models.PageContent.title,
                models.Page.page_name,
                models.Page.page_id,
            )
            .join(models.Page)
            .filter(
                models.Page.page_name.in_(menu),
                models.PageContent.language_id == request.language_id,
            )
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()

    @classmethod
    def page_filter_id(cls, request, page_id):
        query = request.dbsession.query(models.Page).filter(
            models.Page.page_id == page_id
        )
        return query.first()
