from paginate_sqlalchemy import SqlalchemyOrmPage

from emporium import models, services


class ProductToBandService(object):
    @classmethod
    def product_band_list_filter_product_id(cls, request, product_id):
        query = request.dbsession.query(models.ProductToBand).filter(
            models.ProductToBand.product_id == product_id
        )
        return query.first()

    @classmethod
    def product_id_band_id_filter_product_list(
        cls,
        request,
        settings,
        product_list=None,
    ):
        if product_list is None:
            product_list = []
        query = (
            request.dbsession.query(
                models.ProductToBand.product_id,
                models.ProductToBand.band_id,
                models.ProductToBand.product_band_id,
                models.Band.seo_slug,
                models.Band.name,
            )
            .filter(models.ProductToBand.product_id.in_(product_list))
            .outerjoin(models.Band, models.Band.band_id == models.ProductToBand.band_id)
            # .group_by(models.Product.product_id)
        )
        query = services.Cache.cache_query(settings["cache"], query)
        return query.all()
