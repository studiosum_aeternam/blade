from datetime import datetime
import hashlib
import json
import time

from pyramid.request import RequestLocalCache
from pyramid.response import FileResponse
from pyramid.view import view_config
from webob import Request

from emporium import helpers, services


class InpostOperations(object):
    @classmethod
    def return_inpost_authenticated_header(cls, request, settings):
        if (
            not request.session.get("inpost_authorization_header")
            or not request.session.get("inpost_client_id")
            or not request.session.get("inpost_api_url")
        ):
            return cls.inpost_generate_header(request, settings)

    @classmethod
    def inpost_generate_header(cls, request, settings):
        settings = helpers.SettingHelpers.single_complex_setting(
            services.SettingsService.single_setting_filter_key(
                request, "inpost_settings", settings
            )
        )
        client_id = settings["client_id"]
        token = settings["token"]
        api_url = (
            settings["production_url"]
            if settings["mode"] == "1"
            else settings["sandbox_url"]
        )
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {token}",
        }
        login_request = Request.blank(
            f"{api_url}/v1/organizations/{client_id}", headers=headers
        ).send()
        if login_request.status == "200 OK":
            request.session["inpost_client_id"] = settings["client_id"]
            request.session["inpost_api_url"] = api_url
            request.session["inpost_authorization_header"] = {
                "Content-Type": "application/json",
                "Authorization": f"Bearer {token}",
            }
            return 200
        return login_request.status

    @classmethod
    def inpost_get_shipment_list(cls, request):
        api_response = Request.blank(
            (
                request.session["inpost_api_url"]
                + "/v1/organizations/"
                + request.session["inpost_client_id"]
                + "/shipments"
            ),
            headers=request.session["inpost_authorization_header"],
        ).send()
        return api_response if api_response.status == "200 OK" else api_response.status

    @classmethod
    def inpost_get_shipment_info_filter_inpost_id(cls, request, inpost_id):
        api_response = Request.blank(
            (
                request.session["inpost_api_url"]
                + "/v1/organizations/"
                + str(request.session["inpost_client_id"])
                + "/shipments/?id="
                + str(inpost_id)
            ),
            headers=request.session["inpost_authorization_header"],
        ).send()
        return api_response if api_response.status == "200 OK" else api_response.status

    @classmethod
    def inpost_get_shipment_info_filter_inpost_tracking_number(
        cls, request, inpost_tracking_number
    ):
        api_response = Request.blank(
            (
                request.session["inpost_api_url"]
                + "/v1/organizations/"
                + str(request.session["inpost_client_id"])
                + "/shipments/?tracking_number="
                + str(inpost_tracking_number)
            ),
            headers=request.session["inpost_authorization_header"],
        ).send()
        return api_response if api_response.status == "200 OK" else api_response.status

    @classmethod
    def inpost_get_label(cls, request, inpost_tracking_number):
        api_response = Request.blank(
            (
                request.session["inpost_api_url"]
                + "/v1/shipments/"
                + str(inpost_tracking_number)
                + "/label"
            ),
            headers=request.session["inpost_authorization_header"],
        ).send()
        return api_response if api_response.status == "200 OK" else api_response.status

    @classmethod
    def check_inpost_data_source(cls, request, order, settings):
        if (
            settings.get("inpost_settings")
            and settings["inpost_settings"]["display_current_info_in_order_view"] == "1"
        ):
            cls.return_inpost_authenticated_header(request, settings)
            if order.params_json.get("inpost_info") and order.params_json[
                "inpost_info"
            ].get("id"):
                cls.inpost_package_status(request, order)

    @classmethod
    def create_inpost_data_source(cls, request, order, settings):
        cls.return_inpost_authenticated_header(request, settings)
        if order.delivery_json.get("inpost_point_selected"):
            cls.inpost_package_label_create(request, order)

    @classmethod
    def inpost_package_status(cls, request, order):
        api_response = cls.inpost_get_shipment_info_filter_inpost_id(
            request,
            order.params_json["inpost_info"]["id"],
        )
        if api_response.status == "200 OK":
            order.params_json["inpost_info"] = json.loads(
                api_response.body.decode("utf-8")
            )["items"][0]
            order.params_json["inpost_info"]["inpost_status"] = {
                "sent_from_source_branch": "W trasie",
                "adopted_at_source_branch": "Przyjęta w oddziale InPost.",
                "delivered": "Dostarczona",
                # Umieszczona w automacie Paczkomat (odbiorczym).
                # Przekazano do doręczenia.
                # Przyjęta w oddziale InPost.
                "taken_by_courier": "Odebrana od Nadawcy."
                # Paczka nadana w automacie Paczkomat.
                # Przygotowana przez Nadawcę.
            }

    @classmethod
    def inpost_package_tracking_number(cls, request, inpost_id):
        api_response = cls.inpost_get_shipment_info_filter_inpost_id(
            request,
            inpost_id,
        )
        formatted_response = json.loads(api_response.body)["items"][0]
        return formatted_response["tracking_number"]

    @classmethod
    def inpost_create_label(cls, request, order):
        client_json = order.client_json
        delivery_json = order.delivery_json
        params_json = order.params_json
        # fmt: off
        post_params = {
            "receiver": {
                "name": order.uuid,
                "company_name": client_json["company_name"],
                "first_name": client_json["first_name"],
                "last_name": client_json["last_name"],
                "email": client_json["mail"],
                "phone": helpers.CommonHelpers.inpost_telephone_format(client_json["telephone"]),
            },
            "parcels": {"template": "small"},
            "insurance": {"amount": 100, "currency": "PLN"},
            "custom_attributes": {
                "sending_method": "parcel_locker",
                "target_point": delivery_json["inpost_point_selected"],
            },
            "service": "inpost_locker_standard",
            "reference": order.uuid,
            "external_customer_id": ""
        }
        # fmt: on
        post_params_json = json.dumps(post_params).encode("utf-8")
        api_response = Request.blank(
            (
                request.session["inpost_api_url"]
                + "/v1/organizations/"
                + request.session["inpost_client_id"]
                + "/shipments"
            ),
            headers=request.session["inpost_authorization_header"],
            POST=post_params_json,
        ).send()
        return api_response

    @classmethod
    def inpost_print_label(cls, request, inpost_id, inpost_tracking_number, settings):
        tmp_path = helpers.CommonHelpers.asset_path("tmp", asset_type="tmp")
        with open(tmp_path + inpost_tracking_number + ".pdf", "wb") as f:
            cls.return_inpost_authenticated_header(request, settings)
            api_response = cls.inpost_get_label(request, inpost_id)
            f.write(api_response.body)
        response = FileResponse(tmp_path + inpost_tracking_number + ".pdf")
        response.headers[
            "Content-Disposition"
        ] = f"attachment; filename=inpost_label_{inpost_tracking_number}.pdf"
        return response

    @classmethod
    def inpost_package_label_create(cls, request, order):
        api_response = cls.inpost_create_label(request, order)
        if api_response.status == "201 Created":
            # Inpost has two types of ID - kind of a basic request and a package number.
            # There is a delay (in worse case scenario it seems to be around 400-500ms)
            # between creating the first and second object
            inpost_id = None
            while inpost_id is None:
                time.sleep(0.15)
                inpost_id = json.loads(api_response.body.decode("utf-8"))["id"]
            return inpost_id

    @classmethod
    def inpost_delete_label(cls, request, inpost_id):
        api_response = Request.blank(
            (request.session["inpost_api_url"] + "/v1/shipments/" + str(inpost_id)),
            headers=request.session["inpost_authorization_header"],
            environ={"REQUEST_METHOD": "DELETE"},
        ).send()
        return api_response
