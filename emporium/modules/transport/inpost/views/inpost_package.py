from datetime import datetime, timedelta
import json
import urllib.parse
from pyramid.response import FileResponse
from pyramid.view import view_config
import time
from webob import Request

# from sqlalchemy.orm.attributes import flag_modified

from emporium import (
    helpers,
    services,
    operations,
    views,
)


@view_config(
    route_name="inpost_package_list",
    renderer="modules/transport/inpost/templates/emporatorium/package_list.html",
    permission="edit",
)
def inpost_package_list(request):
    settings = helpers.SettingHelpers.admin_settings(
        request, services.SettingsService.admin_settings(request)
    )
    operations.InpostOperations.return_inpost_authenticated_header(request, settings)
    api_response = operations.InpostOperations.inpost_get_shipment_list(request)
    package_list = json.loads(api_response.body)["items"]
    availability_attributes = services.AttributeService.availability_attributes(request)
    filter_params = helpers.CommonHelpers.initialize_filter_params(
        request, availability_attributes
    )
    return dict(
        filter_params=filter_params, package_list=package_list, settings=settings
    )


@view_config(route_name="inpost_label_print", permission="edit")
def inpost_label_print(request):
    settings = helpers.SettingHelpers.admin_settings(
        request, services.SettingsService.admin_settings(request)
    )
    inpost_id = request.params.get("inpost_id", None)
    inpost_tracking_number = operations.InpostOperations.inpost_package_tracking_number(
        request, inpost_id
    )
    if inpost_id and inpost_tracking_number:
        return operations.InpostOperations.inpost_print_label(
            request, inpost_id, inpost_tracking_number, settings
        )


@view_config(route_name="inpost_create_package", permission="edit")
def inpost_create_package(request):
    settings = helpers.SettingHelpers.admin_settings(
        request, services.SettingsService.admin_settings(request)
    )
    operations.InpostOperations.return_inpost_authenticated_header(request, settings)
    order_id = request.params.get("order_id", None)
    order = services.OrderService.order_params_filter_id(request, order_id)
    inpost_id = operations.InpostOperations.inpost_package_label_create(request, order)
    # Order.params_json is a container field for all order-related data - it's a JSONB field.
    # The simplest method for SQLAlchemy to recognize that one of the JSONB fields was modified is
    # to apply the modified flag. SQLAlchemy thinks that data here is a dictionary. The only way
    # to get around it is to make a copy of the object (to get a different ID in memory)
    # and overwrite the whole field.
    order_params = order.params_json.copy()
    order_params["inpost_info"] = {"id": inpost_id}
    inpost_tracking_number = operations.InpostOperations.inpost_package_tracking_number(
        request, inpost_id
    )
    order.params_json = order_params
    if inpost_id and inpost_tracking_number != "None":
        return operations.InpostOperations.inpost_print_label(
            request, inpost_id, inpost_tracking_number, settings
        )


@view_config(route_name="inpost_delete_package", permission="edit")
def inpost_delete_package(request):
    inpost_id = request.params.get("inpost_id", None)
    settings = helpers.SettingHelpers.admin_settings(
        request, services.SettingsService.admin_settings(request)
    )
    # operations.InpostOperations.return_inpost_authenticated_header(request, settings)
    return operations.InpostOperations.inpost_delete_label(request, inpost_id)


@view_config(route_name="inpost_repair_package", permission="edit")
def inpost_repair_package(request):
    settings = helpers.SettingHelpers.admin_settings(
        request, services.SettingsService.admin_settings(request)
    )
    tracking_number = request.params.get("tracking_number", None)
    operations.InpostOperations.return_inpost_authenticated_header(request, settings)
    return operations.InpostOperations.inpost_get_shipment_info_filter_inpost_tracking_number(
        request, tracking_number
    )
