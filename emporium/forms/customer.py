from wtforms import (
    EmailField,
    Form,
    HiddenField,
    IntegerField,
    PasswordField,
    StringField,
    validators,
)


"""
from datetime import datetime, timedelta
from wtforms.ext.csrf.session import SessionSecureForm
from wtforms.csrf.session import SessionCSRF
"""

from wtforms.validators import InputRequired, Email


class BaseForm(Form):
    # see https://wtforms.readthedocs.io/en/latest/csrf.html
    class Meta:
        locales = ("pl_PL", "pl", "PL")
        """"
        csrf = True
        csrf_class = SessionCSRF
        csrf_secret = b'EPj00jpfj8Gx1SjZDAKOwniemoshnaQfnQ9DJYe0Ym'
        csrf_time_limit = timedelta(minutes=20)
        csrf_secret = app.config.get('SECRET_KEY')

        @property
        def csrf_context(self):
           return session

        """


class CreateCustomerForm(BaseForm):
    last_name = StringField(
        validators=(validators.InputRequired(message="Pole wymagane"),)
    )
    first_name = StringField(
        validators=(validators.InputRequired(message="Pole wymagane"),)
    )
    telephone = StringField(
        validators=(validators.InputRequired(message="Pole wymagane"),)
    )
    mail = EmailField(
        "Email",
        [
            InputRequired("Proszę podać adres email."),
            Email("To pole wymaga podania właściwego adresu mailowego"),
        ],
    )
    city = StringField(validators=(validators.InputRequired(message="Pole wymagane"),))
    postcode = StringField(
        validators=(
            validators.InputRequired(message="Pole wymagane"),
            validators.Length(
                max=9, message=("Proszę wprowadzić poprawny kod pocztowy")
            ),
        )
    )
    street = StringField(
        validators=(validators.InputRequired(message="Pole wymagane"),)
    )
    country = StringField()
    state = StringField(validators=(validators.optional(),))
    psswd_hsh = PasswordField(
        validators=(validators.InputRequired(message="Pole wymagane"),)
    )
    company_name = StringField(validators=(validators.optional(),))
    company_nip = StringField(
        validators=(
            validators.optional(),
            validators.Length(
                min=10,
                max=14,
                message=("Proszę wprowadzić poprawny kod NIP min. 10 znaków"),
            ),
        )
    )
    privacy_policy = IntegerField(
        validators=(validators.InputRequired(message="Pole wymagane"),)
    )
    customer_type = IntegerField()


class EditCustomerForm(CreateCustomerForm):
    psswd_hsh = PasswordField(validators=(validators.optional(),))
    privacy_policy = IntegerField(validators=(validators.optional(),))


class ResetCustomerForm(BaseForm):
    existenz = HiddenField()
    mail = EmailField(
        "Email",
        [
            InputRequired("Proszę podać adres email."),
            Email("To pole wymaga podania właściwego adresu mailowego"),
        ],
    )


class NewPasswordForm(BaseForm):
    password = PasswordField(
        validators=(validators.InputRequired(message="Pole wymagane"),)
    )
