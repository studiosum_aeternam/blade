from wtforms import (
    Form,
    HiddenField,
    IntegerField,
    StringField,
    validators,
)


class OptionCreateForm(Form):
    status = IntegerField()
    value = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    option_group_id = IntegerField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )


class OptionUpdateForm(OptionCreateForm):
    id = HiddenField()


class OptionGroupCreateForm(Form):
    name = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    status = IntegerField()
    option_values = StringField()


class OptionGroupUpdateForm(OptionGroupCreateForm):
    id = HiddenField()
