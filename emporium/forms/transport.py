from wtforms import (
    BooleanField,
    Form,
    HiddenField,
    IntegerField,
    StringField,
    TextAreaField,
    validators,
)


class TransportForm(Form):
    # special_method = BooleanField()
    description = TextAreaField()
    excerpt = StringField()
    name = StringField(validators=(validators.InputRequired(message="Pole wymagane"),))
    status = BooleanField()
    transport_method = IntegerField()


class TransportUpdateForm(TransportForm):
    transport_id = HiddenField()
