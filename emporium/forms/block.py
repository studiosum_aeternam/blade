from wtforms import (
    BooleanField,
    Form,
    HiddenField,
    IntegerField,
    StringField,
    TextAreaField,
    validators,
)


class BlockForm(Form):
    title = StringField(validators=(validators.InputRequired(message=u"Pole wymagane"),))
    status = BooleanField()
    content = TextAreaField(validators=(validators.InputRequired(message=u"Pole wymagane"),))
    page = IntegerField(validators=(validators.optional(),))
    size = IntegerField(validators=(validators.optional(),))
    order = IntegerField(validators=(validators.optional(),))
    creator_id = HiddenField()
    block_id = HiddenField()
