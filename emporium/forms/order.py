from wtforms import (
    EmailField,
    Form,
    HiddenField,
    IntegerField,
    PasswordField,
    RadioField,
    StringField,
    validators,
)

from .validator import RequiredIf


class OrderForm(Form):
    """
    Form for orders - sorted by order of fields in the template
    """

    def __init__(
        self, *args, the_request=None, data_options={}, **kwargs
    ):  # accept the object
        super(OrderForm, self).__init__(*args, **kwargs)
        self.transport_dict = data_options.transport_dict
        self.payment_dict = data_options.payment_dict
        self.the_request = the_request

    ## Depending on the product type?

    # Custom validators  have construction - validator_FIELDNAME, so the code won't be found in
    # any other part of the engine
    transport_id = IntegerField()
    physical_delivery_method = IntegerField()

    def validate_transport_id(form, field):
        if form.digital_products_only.data == 1:
            pass
        else:
            physical_delivery_method = IntegerField(
                validators=(validators.InputRequired(message="Wybierz sposób dostawy"),)
            )
            transport_id = IntegerField(
                "Transport",
                [
                    validators.InputRequired(),
                ],
            )
            custom_transport_dispatch = {
                "inpost": form.inpost_validate_transport_requirements
            }
            # WTForm returns something unpredictable

            if form.transport_id.data and form.transport_id.data != "None":
                module_name = form.transport_dict[
                    int(form.transport_id.data)
                ].module_name
                if custom_transport_dispatch.get(module_name):
                    custom_transport_dispatch[module_name](form.the_request)

    def inpost_validate_transport_requirements(self, request):
        if (
            not request.params.get("inpost_point_selected")
            or request.params.get("inpost_point_selected") == "None"
        ):
            raise validators.ValidationError("Nie wybrano paczkomatu!")

    invoice = StringField()
    customer_type = RadioField(
        choices=[("0", "Osoba fizyczna"), ("1", "Firma")],
        validators=(validators.InputRequired(message="Pole wymagane"),),
    )
    company_name = StringField(
        validators=(
            validators.optional(),
            RequiredIf(customer_type="company"),
        ),
    )
    company_nip = StringField(
        validators=(
            validators.optional(),
            RequiredIf(customer_type="company"),
            validators.Length(
                min=10, max=14, message=("Proszę wprowadzić NIP min. 10 znaków")
            ),
        ),
    )
    uniqueness = HiddenField(validators=(validators.optional(),))
    first_name = StringField(
        "Imię",
        validators=(
            validators.InputRequired(message="Pole wymagane"),
            validators.Length(
                min=2, message=("Imię nie może być krótsze niż 2 litery")
            ),
        ),
    )
    last_name = StringField(
        "Nazwisko",
        validators=(
            validators.InputRequired(message="Pole wymagane"),
            validators.Length(
                min=2, message=("Nazwisko nie może być krótsze niż 2 litery")
            ),
        ),
    )
    telephone = StringField(
        "Telefon",
        validators=(
            validators.InputRequired(message="Pole wymagane"),
            validators.Length(
                min=8, message=("Proszę wprowadzić prawidłowy numer telefonu")
            ),
        ),
    )
    mail = EmailField(
        "Mail" + " (na ten adres wyślemy potwierdzenie zakupu)",
        (
            RequiredIf(registration_type=("register_buy", "quick_buy")),
            validators.Email("To pole wymaga podania właściwego adresu mailowego"),
        ),
    )
    psswd_hsh = PasswordField(
        "Hasło" + " (nadaj hasło dla tworzonego konta)",
        validators=(RequiredIf(registration_type="register_buy"),),
    )
    street = StringField(
        "Ulica i numer domu/lokalu",
        validators=(
            validators.InputRequired(message="Pole wymagane"),
            validators.Length(min=6, message=("Proszę wprowadzić prawidłowy adres")),
        ),
    )
    postcode = StringField(
        "Kod pocztowy",
        validators=(
            validators.InputRequired(message="Pole wymagane"),
            validators.Length(
                min=5, message=("Proszę wprowadzić prawidłowy kod pocztowy")
            ),
        ),
    )
    city = StringField(
        "Miasto",
        validators=(
            validators.InputRequired(message="Pole wymagane"),
            validators.Length(
                min=2, message=("Proszę wprowadzić prawidłową nazwę miasta")
            ),
        ),
    )
    country = StringField(
        validators=(validators.InputRequired(message="Pole wymagane"),)
    )
    delivery_address = RadioField(
        choices=[
            ("manual", "Inny adres"),
            ("cloned", "Adres zamawiającego (płatnika)"),
        ],
        validators=(validators.InputRequired(message="Pole wymagane"),),
    )
    delivery_company_name = StringField()
    delivery_company_nip = StringField()
    delivery_first_name = StringField(
        validators=(
            validators.optional(),
            RequiredIf(delivery_address="manual", message="Pole wymagane"),
            validators.Length(
                min=2, message=("Imię nie może być krótsze niż 2 litery")
            ),
        )
    )
    delivery_last_name = StringField(
        validators=(
            validators.optional(),
            RequiredIf(delivery_address="manual", message="Pole wymagane"),
            validators.Length(
                min=2, message=("Nazwisko nie może być krótsze niż 2 litery")
            ),
        )
    )
    delivery_telephone = StringField(
        validators=(
            validators.optional(),
            RequiredIf(delivery_address="manual", message="Pole wymagane"),
            validators.Length(
                min=8, message=("Proszę wprowadzić prawidłowy numer telefonu")
            ),
        )
    )
    delivery_street = StringField(
        validators=(
            validators.optional(),
            RequiredIf(delivery_address="manual", message="Pole wymagane"),
            validators.Length(
                min=6, message=("Proszę wprowadzić prawidłowy adres dostawy")
            ),
        )
    )
    delivery_postcode = StringField(
        validators=(
            validators.optional(),
            RequiredIf(delivery_address="manual", message="Pole wymagane"),
            validators.Length(
                min=5, message=("Proszę wprowadzić prawidłowy kod pocztowy")
            ),
        )
    )
    delivery_city = StringField(
        validators=(
            validators.optional(),
            RequiredIf(delivery_address="company"),
            validators.Length(
                min=2, message=("Proszę wprowadzić prawidłową nazwę miasta")
            ),
        )
    )
    delivery_country = StringField(
        validators=(
            validators.optional(),
            RequiredIf(delivery_address="manual", message="Pole wymagane"),
        )
    )
    # End optional delivery inputs
    digital_products_only = IntegerField()
    payment_method = IntegerField(
        validators=(validators.InputRequired(message="Wybierz metodę płatności"),)
    )
    privacy_policy = IntegerField(
        validators=(
            validators.InputRequired(message="Zaakceptuj politykę prywatności"),
        )
    )
    registration_type = StringField(validators=(validators.InputRequired(),))
    ###########
    additional_info = StringField()
