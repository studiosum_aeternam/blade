from wtforms import (
    Form,
    HiddenField,
    IntegerField,
    StringField,
    validators,
)


class AttributeCreateForm(Form):
    status = IntegerField()
    value = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    attribute_group_id = IntegerField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )


class AttributeUpdateForm(AttributeCreateForm):
    id = HiddenField()


class AttributeGroupCreateForm(Form):
    name = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    status = IntegerField()
    attribute_values = StringField()


class AttributeGroupUpdateForm(AttributeGroupCreateForm):
    id = HiddenField()
