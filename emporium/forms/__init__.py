"""
Combined import for all forms
"""
from .author import EditAuthorForm  # CreateAuthorForm
from .attribute import (
    AttributeCreateForm,
    AttributeUpdateForm,
    AttributeGroupCreateForm,
    AttributeGroupUpdateForm,
)
from .band import BandCreateForm, BandUpdateForm
from .banner import BannerForm
from .block import BlockForm
from .category import CategoryCreateForm, CategoryUpdateForm
from .client import ClientOrderForm, ClientDeliveryOrderForm
from .collection import CollectionCreateForm, CollectionUpdateForm
from .contact import ContactForm, EditContactForm
from .customer import (
    CreateCustomerForm,
    EditCustomerForm,
    NewPasswordForm,
    ResetCustomerForm,
)
from .discount import DiscountForm
from .validator import CustomValidator
from .manufacturer import ManufacturerCreateForm, ManufacturerUpdateForm
from .option import (
    OptionCreateForm,
    OptionUpdateForm,
    OptionGroupCreateForm,
    OptionGroupUpdateForm,
)
from .order import OrderForm
from .page import PageForm
from .payment import PaymentCreateForm, PaymentUpdateForm
from .product import (
    ProductCreateForm,
    ProductSpecialCreateForm,
    ProductTemplateForm,
    ProductUpdateForm,
)
from .post import PostForm
from .setting import SettingForm, SettingTemplateForm
from .special import SpecialCreateForm, SpecialUpdateForm
from .status import (
    StatusCreateForm,
    StatusUpdateForm,
    StatusGroupCreateForm,
    StatusGroupUpdateForm,
)
from .storage import StorageForm
from .transport import (
    TransportForm,
    TransportUpdateForm,
)
