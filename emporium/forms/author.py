from wtforms import (
    EmailField,
    Form,
    HiddenField,
    IntegerField,
    PasswordField,
    StringField,
    validators,
)


"""
from datetime import datetime, timedelta
from wtforms.ext.csrf.session import SessionSecureForm
from wtforms.csrf.session import SessionCSRF
"""

# from wtforms.validators import InputRequired, Email

from ..forms import customer


class CreateAuthorForm(Form):
    last_name = StringField()
    first_name = StringField()
    callsign = StringField()
    status = IntegerField()


class EditAuthorForm(CreateAuthorForm):
    author_id = HiddenField()
