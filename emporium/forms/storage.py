from wtforms import (
    BooleanField,
    Form,
    HiddenField,
    IntegerField,
    StringField,
    TextAreaField,
    validators,
)


class StorageForm(Form):
    storage_id = HiddenField(validators=(validators.optional(),))
    description = TextAreaField()
    name = StringField(validators=(validators.InputRequired(message=u"Pole wymagane"),))
    order = IntegerField()
    status = BooleanField()
    storage_type =IntegerField()