"""
Form for decimal editions
"""
from wtforms import (
    BooleanField,
    Form,
    HiddenField,
    validators,
    IntegerField,
)
from .helper import MyDecimalField
from .validator import RequiredIf


class DiscountForm(Form):
    discount_id = HiddenField(validators=(validators.optional(),))
    default = BooleanField()
    status = IntegerField()
    subtract = IntegerField()
    value = MyDecimalField()
