from wtforms import (
    EmailField,
    Form,
    HiddenField,
    IntegerField,
    StringField,
    SubmitField,
    TextAreaField,
    validators
)


from ..forms import customer


class ContactForm(Form):
    name = StringField("Imię", [validators.InputRequired("Proszę podać imię.")])
    mail = EmailField(
        "Email",
        [
            validators.InputRequired("Proszę podać adres email."),
            validators.Email("To pole wymaga podania właściwego adresu mailowego"),
        ],
    )
    telephone = StringField("Telefon")
    message = TextAreaField(
        "Wiadomość", [validators.InputRequired("Proszę uzupełnić treśc wiadomości")]
    )
    submit = SubmitField("Wyślij")


class CreateContactForm(customer.CreateCustomerForm):
    telephone = StringField()
    city = StringField()
    postcode = StringField()
    street = StringField()
    country = StringField()
    state = StringField()
    password = StringField()
    privacy_policy = IntegerField()



class EditContactForm(CreateContactForm):
    customer_id = HiddenField()
