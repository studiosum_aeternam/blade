from wtforms import (
    DateField,
    Form,
    HiddenField,
    IntegerField,
    StringField,
    validators,
)


class PostForm(Form):
    content = StringField(validators=(validators.optional(),))
    excerpt = StringField(validators=(validators.optional(),))
    creator_id = HiddenField()
    date_added = DateField(validators=(validators.optional(),))
    # date_modified = DateField(validators=(validators.optional(),))
    date_published = DateField(validators=(validators.optional(),))
    h1 = StringField()
    meta_description = StringField()
    seo_slug = StringField()
    status = IntegerField()
    post_image_url = StringField()
    title = StringField(validators=(validators.InputRequired(message=u"Pole wymagane"),))
