from wtforms import (
    EmailField,
    Form,
    IntegerField,
    StringField,
    validators,
)


class ClientOrderForm(Form):
    last_name = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    first_name = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    telephone = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    mail = EmailField(
        "Email",
        [
            validators.InputRequired("Proszę podać adres email."),
            validators.Email("To pole wymaga podania właściwego adresu mailowego"),
        ],
    )
    city = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    postcode = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"), validators.Length(max=9, message=(u'Proszę wprowadzić poprawny kod pocztowy')))
    )
    street = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    country = StringField(validators=(validators.optional(),))
    state = StringField(validators=(validators.optional(),))
    company_name = StringField(validators=(validators.optional(),))
    company_nip = StringField(validators=(validators.optional(), validators.Length(max=10, message=(u'Proszę wprowadzić kod NIP - bez myślników i spacji')))
    )
    delivery_method = IntegerField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    payment_method = IntegerField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )


class ClientDeliveryOrderForm(ClientOrderForm):
    delivery_last_name = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    delivery_first_name = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    delivery_telephone = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    delivery_city = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    delivery_postcode = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    delivery_street = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    delivery_country = StringField(validators=(validators.optional(),))
    delivery_state = StringField(validators=(validators.optional(),))
    delivery_company_name = StringField()
    delivery_company_nip = StringField()
