from wtforms import (
    BooleanField,
    Form,
    HiddenField,
    IntegerField,
    StringField,
    validators,
)


class SpecialCreateForm(Form):
    status = IntegerField()
    price_changing = BooleanField(validators=(validators.optional(),))
    language_id = IntegerField(
        validators=(validators.InputRequired(message="Pole wymagane"),)
    )
    name = StringField(validators=(validators.InputRequired(message="Pole wymagane"),))
    name_displayed = StringField()
    description = StringField(validators=(validators.optional(),))


class SpecialUpdateForm(SpecialCreateForm):
    special_id = HiddenField()
