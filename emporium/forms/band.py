from wtforms import (
    DateField,
    Form,
    FileField,
    HiddenField,
    IntegerField,
    StringField,
    TextAreaField,
    validators,
)

from .validator import BandValidate


class BandCreateForm(Form):
    code = StringField()
    date_added = DateField(validators=(validators.optional(),))
    date_available = DateField(validators=(validators.optional(),))
    date_modified = DateField(validators=(validators.optional(),))
    description = TextAreaField()
    image = FileField()
    language_id = IntegerField()
    band_type = IntegerField()
    meta_description = StringField()
    name = StringField(
        validators=(BandValidate(),),
    )
    priority = IntegerField()
    sort_order = IntegerField()
    status = IntegerField()
    tag = StringField()
    u_h1 = StringField()
    u_title = StringField()


class BandUpdateForm(BandCreateForm):
    band_id = HiddenField()
    band_description_id = HiddenField()
