from wtforms import (
    Form,
    HiddenField,
    IntegerField,
    StringField,
    TextAreaField,
    validators,
)


from .helper import MyFloatField


class PaymentCreateForm(Form):
    description = TextAreaField()
    equotation = StringField()
    fee_percent = MyFloatField(validators=(validators.optional(),))
    fee_value = MyFloatField(validators=(validators.optional(),))
    name = StringField(validators=(validators.InputRequired(message=u"Pole wymagane"),))
    merchant_id = StringField()
    mode = IntegerField()
    key = StringField()
    status = IntegerField()
    type = IntegerField()
    url_test = StringField()
    url_test_verify = StringField()
    url_transaction = StringField()
    url_request = StringField()
    url_transaction_verify = StringField()
    pos_id = StringField()
    api_version = StringField()


class PaymentUpdateForm(PaymentCreateForm):
    payment_id = HiddenField()
