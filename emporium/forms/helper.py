from decimal import Decimal, InvalidOperation
from wtforms import DecimalField, FloatField, validators


class MyFloatField(FloatField):
    def process_formdata(self, valuelist):
        """
        Variable to decimal, only when value is present 
        """
        valuelist = list(filter(lambda x: x!='', valuelist))
        if valuelist:
            try:
                self.data = float(valuelist[0].replace(",", "."))
            except ValueError:
                self.data = None
                raise ValueError(self.gettext("Nieprawidłowa wartość w polu liczbowym"))


class MyDecimalField(DecimalField):
    def process_formdata(self, valuelist):
        """
        Variable to decimal, only when value is present 
        """
        valuelist = list(filter(lambda x: x!='', valuelist))
        if valuelist:
            try:
                self.data = Decimal(str(valuelist[0]).replace(",", "."))
            except (InvalidOperation, ValueError):
                self.data = None
                raise ValueError(self.gettext("Nieprawidłowa wartość w polu liczbowym"))
