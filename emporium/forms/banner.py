from wtforms import (
    BooleanField,
    DateField,
    FileField,
    Form,
    HiddenField,
    IntegerField,
    StringField,
    validators,
)


class BannerForm(Form):
    banner_id = HiddenField()
    caption = StringField()
    caption_secondary = StringField()
    creator_id = HiddenField()
    date_added = DateField()
    date_end = DateField(validators=(validators.optional(),))
    date_modified = DateField()
    date_start = DateField(validators=(validators.optional(),))
    description = StringField()
    banner_image_url = FileField(validators=(validators.optional(),))
    order = IntegerField()
    page = IntegerField()
    status = BooleanField()
    url = StringField(validators=(validators.optional(),))
    url_secondary = StringField(validators=(validators.optional(),))
