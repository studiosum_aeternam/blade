from wtforms import Form, HiddenField, IntegerField, StringField, validators


class StatusCreateForm(Form):
    status = IntegerField()
    value = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    status_group_id = IntegerField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )


class StatusUpdateForm(StatusCreateForm):
    id = HiddenField()


class StatusGroupCreateForm(Form):
    name = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    status = IntegerField()
    status_values = StringField()


class StatusGroupUpdateForm(StatusGroupCreateForm):
    id = HiddenField()
