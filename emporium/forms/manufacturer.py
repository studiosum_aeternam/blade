from wtforms import (
    DateField,
    Form,
    FileField,
    HiddenField,
    IntegerField,
    StringField,
    TextAreaField,
    validators,
)


class ManufacturerCreateForm(Form):
    code = StringField()
    date_added = DateField(validators=(validators.optional(),))
    date_available = DateField(validators=(validators.optional(),))
    date_modified = DateField(validators=(validators.optional(),))
    description = TextAreaField()
    image = FileField()
    language_id = IntegerField()
    manufacturer_type = IntegerField()
    meta_description = StringField()
    name = StringField()
    priority = IntegerField()
    sort_order = IntegerField()
    status = IntegerField()
    tag = StringField()
    u_h1 = StringField()
    u_title = StringField()


class ManufacturerUpdateForm(ManufacturerCreateForm):
    manufacturer_id = HiddenField()
    manufacturer_description_id = HiddenField()
