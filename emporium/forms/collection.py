from wtforms import (
    DateField,
    Form,
    FileField,
    HiddenField,
    IntegerField,
    StringField,
    validators,
)


class CollectionCreateForm(Form):
    date_added = DateField(validators=(validators.optional(),))
    date_modified = DateField(validators=(validators.optional(),))
    description = StringField()
    description_long = StringField()
    image = FileField()
    language_id = IntegerField()
    manufacturer_id = StringField()
    meta_description = StringField()
    name = StringField(validators=(validators.InputRequired(message=u"Pole wymagane"),))
    priority = IntegerField()
    products = StringField()
    properties = StringField()
    seo_slug = StringField()
    similar_collections = StringField()
    sort_order = IntegerField()
    status = IntegerField()
    u_h1 = StringField()
    u_title = StringField()


class CollectionUpdateForm(CollectionCreateForm):
    collection_id = HiddenField()
