from wtforms import (
    BooleanField,
    Form,
    HiddenField,
    StringField,
    TextAreaField,
    validators,
)


class PageForm(Form):
    content = TextAreaField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    page_id = HiddenField(validators=(validators.optional(),))
    status = BooleanField()
    title = StringField(
        validators=(validators.InputRequired(message=u"Pole wymagane"),)
    )
    content_extended = TextAreaField()
