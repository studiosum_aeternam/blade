from datetime import datetime

from wtforms import (
    BooleanField,
    DateField,
    Form,
    FileField,
    HiddenField,
    IntegerField,
    StringField,
    SelectField,
    TextAreaField,
    validators,
)

from .helper import MyDecimalField
from .validator import RequiredIf, Length, ProductValidate


class ProductTemplateForm(Form):
    manufactured = IntegerField(validators=(validators.optional(),))
    subtract_quantity = IntegerField(validators=(validators.optional(),), default="0")
    transport_method = IntegerField(default=0)


class ProductCreateForm(ProductTemplateForm):
    # Misc
    date_modified = DateField(
        validators=(validators.optional(),), default=datetime.now()
    )

    # Availability
    availability_id = IntegerField(
        label="Dostępność",
        validators=(validators.optional(), RequiredIf(transport_method=0)),
    )
    date_added = DateField(validators=(validators.optional(),))
    date_available = DateField(
        validators=(validators.optional(),), default="1970-01-01"
    )
    date_end = DateField(validators=(validators.optional(),))

    description = TextAreaField(validators=(validators.optional(),))
    description_technical = TextAreaField(validators=(validators.optional(),))
    excerpt = TextAreaField(validators=(validators.optional(),))
    # Identifiers
    ean = StringField()
    mpn = StringField()

    isbn = StringField()
    jan = StringField()
    main_image_url = StringField()
    manufacturer_id = IntegerField(
        label="Producent",
        validators=(validators.InputRequired(message="Pole wymagane"),),
    )
    band_id = IntegerField()
    meta_description = StringField()
    minimum = MyDecimalField(
        validators=(validators.InputRequired(message="Pole wymagane"),), default="1"
    )

    # Physcical parameters
    height = MyDecimalField(validators=(validators.optional(),), default="1")
    length = MyDecimalField(validators=(validators.optional(),), default="1")
    product_type = IntegerField()
    square_meter = IntegerField(validators=(validators.optional(),))
    subtract = MyDecimalField()
    weight = MyDecimalField(
        validators=(validators.optional(), RequiredIf(transport_method=0)), default="1"
    )
    width = MyDecimalField(validators=(validators.optional(),), default="1")

    # Prices
    catalog_price = MyDecimalField(
        validators=(validators.InputRequired(message="Pole wymagane"),)
    )
    discount_id = IntegerField(default=1)
    manual_discount = BooleanField(label="Ręczny rabat", default="0")
    price = MyDecimalField(validators=([RequiredIf(manual_discount=True)]))
    purchase_price = MyDecimalField()
    tax_id = IntegerField(validators=(validators.optional(),), default="1")

    model = StringField(
        label="Nazwa producenta",
        validators=(ProductValidate(),),
    )
    name = StringField(
        label="Nazwa handlowa",
        validators=(
            validators.InputRequired(message="Pole wymagane"),
            Length(min=4, max=255),
        ),
    )
    pieces = MyDecimalField(default="1")
    points = IntegerField(validators=(validators.optional(),), default="0")
    product_components = TextAreaField(
        validators=(validators.optional(), RequiredIf(product_type=1)), default=""
    )
    one_batch = IntegerField()
    quantity = IntegerField(validators=(validators.optional(),), default="1")
    seo_slug = StringField(label="Przyjazny link")
    setting_template_id = IntegerField()
    sku = StringField()
    sort_order = IntegerField(validators=(validators.optional(),))
    special_type = IntegerField()
    product_status = IntegerField()
    set_type = StringField()
    tag = StringField()
    u_h1 = StringField()
    u_title = StringField()
    unit = MyDecimalField(
        validators=(validators.InputRequired(message="Pole wymagane"),), default="1"
    )
    upc = StringField()
    viewed = IntegerField(validators=(validators.optional(),), default="0")


class ProductUpdateForm(ProductCreateForm):
    product_id = HiddenField()


class ProductSpecialCreateForm(Form):
    date_start = DateField(validators=(validators.optional(),))
    date_end = DateField(validators=(validators.optional(),))
    special_id = SelectField(
        choices=[
            ("1", "Bestseller"),
            ("2", "Polecane"),
            ("3", "Promocja"),
            ("4", "Wyprzedaż"),
        ],
        validators=(validators.optional(),),
    )
    product_list = StringField()
    discount = MyDecimalField(validators=([RequiredIf(special_id=3)]))


class ProductXMLCreateForm(Form):
    xml_id = SelectField(
        choices=[
            ("1", "Google (base)"),
            ("2", "Ceneo"),
        ],
        validators=(validators.optional(),),
    )
    product_list = StringField()


class ProductSpecialEditForm(ProductSpecialCreateForm):
    special_id = HiddenField()
