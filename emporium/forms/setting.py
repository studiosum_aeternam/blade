from wtforms import (
    Form,
    HiddenField,
    IntegerField,
    StringField,
    validators,
    SelectMultipleField,
)
from ..forms import product


class SettingForm(Form):
    key = StringField(validators=(validators.InputRequired(message=u"Pole wymagane"),))
    setting_type = IntegerField()
    value = StringField()
    setting_id = HiddenField()


class SettingTemplateForm(product.ProductTemplateForm):
    description = StringField(validators=(validators.optional(),))
    name = StringField(validators=(validators.optional(),))
    order = IntegerField()
    status = IntegerField()
    template_type = IntegerField()
    unit_label = IntegerField()
