from decimal import Decimal, InvalidOperation
import transaction
import os

from pyramid.paster import get_appsettings
from sqlalchemy import func
from wtforms import DecimalField, FloatField, validators

from emporium import models, operations


class CustomValidator(object):
    @classmethod
    def product_form_prevalidate(cls, request, form):
        cls.product_spacial_prices_prevalidate(request, form)
        cls.validate_unique_model(request, form)

    @classmethod
    def band_form_prevalidate(cls, request, form):
        cls.validate_unique_band_name(request, form)

    @classmethod
    def product_spacial_prices_prevalidate(cls, request, form):
        if (
            request.params.get("special")
            and int(request.params.get("special")) in [2, 4]
            and not request.params.get("special_price")
        ):
            form.special_type.errors = (
                "Oferty typu wyprzedaż/promocja wymagają ustawenia ceny!"
            )

    @classmethod
    def validate_unique_model(cls, request, form):
        existing_product = (
            request.dbsession.query(models.Product.product_id, models.Product.model)
            .filter(models.Product.model == form.model.data)
            .first()
        )
        product_id = form.product_id.data if hasattr(form, "product_id") else None
        if existing_product and (
            not product_id or str(existing_product.product_id) != product_id
        ):
            form.model.errors = validators.ValidationError(
                "Powielona nazwa katalogowa (model)!"
            )


class BandValidate(object):
    def __init__(self):
        self.message = "jest już w systemie"

    def __call__(self, form, field):
        settings = get_appsettings(os.path.abspath("development.ini"))
        engine = models.get_engine(settings)
        session_factory = models.get_session_factory(engine)
        with transaction.manager:
            dbsession = models.get_tm_session(session_factory, transaction.manager)
            existing_band = (
                dbsession.query(models.Band.band_id, models.Band.name)
                .filter(func.upper(models.Band.name) == func.upper(form.name.data))
                .first()
            )
            band_id = form.band_id.data if hasattr(form, "band_id") else None
            if existing_band and (not band_id or str(existing_band.band_id) != band_id):
                raise validators.ValidationError(self.message)


class RequiredIf(object):
    def __init__(self, *args, **kwargs):
        self.conditions = kwargs

    def __call__(self, form, field):
        for name, data in self.conditions.items():
            if name not in form._fields:
                validators.Optional(form)
            else:
                condition_field = form._fields.get(name)
            if condition_field.data == data and not field.data:
                validators.DataRequired(message="Uzupełnij wymagane pole")(form, field)
                validators.Optional()(form)


class Length(object):
    def __init__(self, min=-1, max=-1, message=None):
        self.min = min
        self.max = max
        if not message:
            message = (
                f"długość pola musi zawierać minimum {min} i maksimum {max} znaków."
            )
        self.message = message

    def __call__(self, form, field):
        l = field.data and len(field.data) or 0
        if l < self.min or self.max != -1 and l > self.max:
            raise validators.ValidationError(self.message)


class ProductValidate(object):
    def __init__(self):
        self.message = "jest już w systemie"

    def __call__(self, form, field):
        settings = get_appsettings(os.path.abspath("development.ini"))
        engine = models.get_engine(settings)
        session_factory = models.get_session_factory(engine)
        with transaction.manager:
            dbsession = models.get_tm_session(session_factory, transaction.manager)
            existing_product = (
                dbsession.query(models.Product.product_id, models.Product.model)
                .filter(models.Product.model == form.model.data)
                .first()
            )
            product_id = form.product_id.data if hasattr(form, "product_id") else None
            if existing_product and (
                not product_id or str(existing_product.product_id) != product_id
            ):
                raise validators.ValidationError(self.message)
