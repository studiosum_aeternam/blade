from wtforms import (
    DateField,
    Form,
    HiddenField,
    IntegerField,
    StringField,
    validators,
)


class CategoryCreateForm(Form):
    date_added = DateField(validators=(validators.optional(),))
    date_modified = DateField(validators=(validators.optional(),))
    description = StringField()
    description_long = StringField()
    image = StringField()
    language_id = IntegerField(validators=(validators.optional(),))
    meta_description = StringField()
    name = StringField()
    parent_id = IntegerField(validators=(validators.optional(),))
    product_list = StringField()
    seo_slug = StringField()
    sort_order = IntegerField(validators=(validators.optional(),))
    status = IntegerField(validators=(validators.optional(),))
    top = IntegerField(validators=(validators.optional(),))
    u_h1 = StringField()
    u_title = StringField()


class CategoryUpdateForm(CategoryCreateForm):
    category_description_id = HiddenField()
    category_id = HiddenField()
