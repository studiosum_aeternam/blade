from pyramid.authorization import Allow, Authenticated, Deny, Everyone
import time
from .i18n import custom_locale_negotiator


def includeme(config):
    # config.add_static_view("emporium:templates/emporatorium/static", "admin_static", cache_max_age=72000)
    # config.add_static_view("emporium:templates/blade/static", "static", cache_max_age=72000)
    # config.add_static_view("emporium:emporatorium/static", "emporatorium/static_files", cache_max_age=7200)
    config.add_translation_dirs("emporium:locale")
    config.add_static_view("images", "images", cache_max_age=3600)
    config.add_static_view("uploads", "uploads", cache_max_age=3600)
    config.add_static_view("secured_uploads", "secured_uploads", cache_max_age=3600)
    config.add_static_view("tmp", "tmp", cache_max_age=3600)
    config.add_route("locale", "/locale")
    config.add_route("auth", "/emporatorium/sign/{action}")
    config.add_route("home", "/emporatorium/", factory=admin_factory)
    config.add_route("login", "/emporatorium")
    config.add_route("author_search", "/author_search", factory=admin_factory)
    config.add_route("author_list", "/emporatorium/author_list", factory=admin_factory)
    config.add_route(
        "author_delete", "/emporatorium/author_delete", factory=admin_factory
    )
    config.add_route(
        "author_action", "/emporatorium/author_{action}", factory=admin_factory
    )
    config.add_route("band_list", "/emporatorium/band_list", factory=admin_factory)
    config.add_route(
        "band_delete",
        "/emporatorium/band_delete",
        factory=admin_factory,
    )
    config.add_route(
        "band_action",
        "/emporatorium/band_{action}",
        factory=admin_factory,
    )
    config.add_route("banner_list", "/emporatorium/banner_list", factory=admin_factory)
    config.add_route(
        "banner_delete", "/emporatorium/banner_delete", factory=admin_factory
    )
    config.add_route(
        "banner_action", "/emporatorium/banner_{action}", factory=admin_factory
    )
    config.add_route("block_list", "/emporatorium/block_list", factory=admin_factory)
    config.add_route(
        "block_action", "/emporatorium/block_{action}", factory=admin_factory
    )
    config.add_route(
        "block_delete", "/emporatorium/block_delete", factory=admin_factory
    )
    config.add_route("clear_cache", "/emporatorium/clear_cache", factory=admin_factory)
    config.add_route(
        "contact_list", "/emporatorium/contact_list", factory=admin_factory
    )
    config.add_route(
        "contact_action", "/emporatorium/contact_{action}", factory=admin_factory
    )
    config.add_route(
        "contact_delete", "/emporatorium/contact_delete", factory=admin_factory
    )
    config.add_route(
        "discount_delete", "/emporatorium/discount_delete", factory=admin_factory
    )
    config.add_route(
        "discount_action", "/emporatorium/discount_{action}", factory=admin_factory
    )
    config.add_route("order_list", "/emporatorium/order_list", factory=admin_factory)
    config.add_route("order", "/emporatorium/order-{id:\\d+}", factory=admin_factory)
    config.add_route(
        "order_print", "/emporatorium/order_print-{id:\\d+}", factory=admin_factory
    )
    config.add_route("page_list", "/emporatorium/page_list", factory=admin_factory)
    config.add_route("page_delete", "/emporatorium/page_delete", factory=admin_factory)
    config.add_route(
        "page_tag_action", "/emporatorium/page_tag_action", factory=admin_factory
    )
    config.add_route(
        "page_action", "/emporatorium/page_{action}", factory=admin_factory
    )
    config.add_route(
        "payment_list", "/emporatorium/payment_list", factory=admin_factory
    )
    config.add_route(
        "payment_delete", "/emporatorium/payment_delete", factory=admin_factory
    )
    config.add_route(
        "payment_action", "/emporatorium/payment_{action}", factory=admin_factory
    )

    config.add_route("post_list", "/emporatorium/post_list", factory=admin_factory)
    config.add_route("post_delete", "/emporatorium/post_delete", factory=admin_factory)
    config.add_route(
        "post_action", "/emporatorium/post_{action}", factory=admin_factory
    )

    config.add_route(
        "transport_list", "/emporatorium/transport_list", factory=admin_factory
    )
    config.add_route(
        "transport_delete", "/emporatorium/transport_delete", factory=admin_factory
    )
    config.add_route(
        "transport_size_slot_add",
        "/emporatorium/transport_size_slot_add",
        factory=admin_factory,
    )
    config.add_route(
        "transport_edit",
        "/emporatorium/transport_{module_name}_{module_id}",
        factory=admin_factory,
    )
    config.add_route(
        "collection_list", "/emporatorium/collection_list", factory=admin_factory
    )
    config.add_route(
        "collection_delete", "/emporatorium/collection_delete", factory=admin_factory
    )
    config.add_route(
        "collection_action", "/emporatorium/collection_{action}", factory=admin_factory
    )
    # This view has to be above file upload/delete - in any other case it stops working
    config.add_route("file_delete", "/emporatorium/file_delete", factory=admin_factory)
    config.add_route("file_upload", "/emporatorium/file_upload", factory=admin_factory)
    config.add_route(
        "image_delete", "/emporatorium/image_delete", factory=admin_factory
    )
    config.add_route(
        "image_upload", "/emporatorium/image_upload", factory=admin_factory
    )
    config.add_route(
        "manufacturer_list", "/emporatorium/manufacturer_list", factory=admin_factory
    )
    config.add_route(
        "manufacturer_delete",
        "/emporatorium/manufacturer_delete",
        factory=admin_factory,
    )
    config.add_route(
        "manufacturer_action",
        "/emporatorium/manufacturer_{action}",
        factory=admin_factory,
    )
    config.add_route("option_list", "/emporatorium/option_list", factory=admin_factory)
    config.add_route(
        "option_delete", "/emporatorium/option_delete", factory=admin_factory
    )
    config.add_route(
        "option_action", "/emporatorium/option_{action}", factory=admin_factory
    )
    config.add_route(
        "option_single_action",
        "/emporatorium/option-single_{action}",
        factory=admin_factory,
    )
    config.add_route(
        "settings_main", "/emporatorium/settings_main", factory=admin_factory
    )
    config.add_route(
        "settings_visibility",
        "/emporatorium/settings_visibility_{action}",
        factory=admin_factory,
    )
    config.add_route(
        "settings_list", "/emporatorium/settings_list", factory=admin_factory
    )
    config.add_route(
        "settings_delete", "/emporatorium/settings_delete", factory=admin_factory
    )
    config.add_route(
        "settings_template_list",
        "/emporatorium/settings_template_list",
        factory=admin_factory,
    )
    config.add_route(
        "settings_template_delete",
        "/emporatorium/settings_template_delete",
        factory=admin_factory,
    )
    config.add_route(
        "settings_template_action",
        "/emporatorium/settings_template_{action}",
        factory=admin_factory,
    )
    config.add_route(
        "settings_action", "/emporatorium/settings_{action}", factory=admin_factory
    )
    config.add_route(
        "special_list", "/emporatorium/special_list", factory=admin_factory
    )
    config.add_route(
        "special_delete", "/emporatorium/special_delete", factory=admin_factory
    )
    config.add_route(
        "special_action", "/emporatorium/special_{action}", factory=admin_factory
    )
    config.add_route(
        "storage_list", "/emporatorium/storage_list", factory=admin_factory
    )
    config.add_route(
        "storage_delete", "/emporatorium/storage_delete", factory=admin_factory
    )
    config.add_route(
        "storage_action", "/emporatorium/storage_{action}", factory=admin_factory
    )
    config.add_route("status_list", "/emporatorium/status_list", factory=admin_factory)
    config.add_route(
        "status_delete", "/emporatorium/status_delete", factory=admin_factory
    )
    config.add_route(
        "status_action", "/emporatorium/status_{action}", factory=admin_factory
    )
    config.add_route(
        "status_single_action",
        "/emporatorium/status-single_{action}",
        factory=admin_factory,
    )
    config.add_route(
        "system_print", "/emporatorium/system_print", factory=admin_factory
    )
    config.add_route("system_mail", "/system_mail", factory=admin_factory)
    config.add_route(
        "product_author_add", "/emporatorium/product_author_add", factory=admin_factory
    )
    config.add_route(
        "product_component_add",
        "/emporatorium/product_component_add",
        factory=admin_factory,
    )
    config.add_route(
        "product_special_add",
        "/emporatorium/product_special_add",
        factory=admin_factory,
    )
    config.add_route(
        "product_bulk", "/emporatorium/product_bulk", factory=admin_factory
    )
    config.add_route(
        "product_storage", "/emporatorium/product_storage", factory=admin_factory
    )
    config.add_route(
        "product_list", "/emporatorium/product_list", factory=admin_factory
    )
    config.add_route(
        "product_delete", "/emporatorium/product_delete", factory=admin_factory
    )

    config.add_route("product_xml", "/emporatorium/product_xml", factory=admin_factory)
    config.add_route(
        "product_xml_delete",
        "/emporatorium/product_xml_delete",
        factory=admin_factory,
    )
    config.add_route(
        "product_xml_action",
        "/emporatorium/product_xml_{action}",
        factory=admin_factory,
    )
    config.add_route(
        "product_special", "/emporatorium/product_special", factory=admin_factory
    )
    config.add_route(
        "product_special_delete",
        "/emporatorium/product_special_delete",
        factory=admin_factory,
    )
    config.add_route(
        "product_special_action",
        "/emporatorium/product_special_{action}",
        factory=admin_factory,
    )
    config.add_route(
        "product_to_product_action",
        "/emporatorium/product_to_product_{action}",
        factory=admin_factory,
    )
    config.add_route(
        "category_meta_tree", "/emporatorium/category_meta_tree", factory=admin_factory
    )
    config.add_route(
        "category_list", "/emporatorium/category_list", factory=admin_factory
    )
    config.add_route(
        "category_delete", "/emporatorium/category_delete", factory=admin_factory
    )
    config.add_route(
        "category_action", "/emporatorium/category_{action}", factory=admin_factory
    )
    config.add_route(
        "attribute_list", "/emporatorium/attribute_list", factory=admin_factory
    )
    config.add_route(
        "attribute_delete", "/emporatorium/attribute_delete", factory=admin_factory
    )
    config.add_route(
        "attribute_group_delete",
        "/emporatorium/attribute_group_delete",
        factory=admin_factory,
    )
    config.add_route(
        "attribute_action", "/emporatorium/attribute_{action}", factory=admin_factory
    )
    config.add_route("cart_list", "/emporatorium/cart_list", factory=admin_factory)

    config.add_route(
        "image_folders", "/emporatorium/image_folders", factory=admin_factory
    )
    config.add_route(
        "limitless_product_search", "/limitless_product_search", factory=admin_factory
    )
    config.add_route(
        "limitless_keyword_search", "/limitless_keyword_search", factory=admin_factory
    )
    config.add_route(
        "tag_delete",
        "/emporatorium/tag_delete",
        factory=admin_factory,
    )
    config.add_route(
        "product_manipulation",
        "/emporatorium/product_manipulation_{action}",
        factory=admin_factory,
    )
    config.add_route(
        "tag_add",
        "/emporatorium/tag_add",
        factory=admin_factory,
    )

    # Front
    config.add_route("s_auth", "/sign/{action}")
    config.add_route("s_register", "/register")
    config.add_route("s_home", "/", factory=customer_factory)
    config.add_route("s_page", "/page/{pagename}", factory=customer_factory)
    config.add_route("s_base", "/xml/base.xml", factory=customer_factory)
    config.add_route("s_cart", "/cart/", factory=customer_factory)
    config.add_route("s_cart_edit", "/cart_edit", factory=customer_factory)
    config.add_route("s_ceneo", "/xml/ceneo.xml", factory=customer_factory)
    config.add_route("s_compare", "/compare/", factory=customer_factory)
    config.add_route("s_compare_edit", "/compare_edit", factory=customer_factory)
    config.add_route("s_customer", "/customer/", factory=customer_factory)
    config.add_route("s_customer_delete", "/customer_delete", factory=customer_factory)
    config.add_route("s_customer_form", "/customer_form/", factory=customer_factory)
    config.add_route("s_customer_reset", "/customer_reset/", factory=customer_factory)
    # config.add_route("s_reset_mail", "/reset_mail", factory=customer_factory)
    config.add_route(
        "s_customer_reset_mail", "/customer_reset_mail/", factory=customer_factory
    )
    config.add_route("s_mail", "/mail", factory=customer_factory)
    config.add_route("s_update_mail", "/update_mail/", factory=customer_factory)
    config.add_route(
        "s_payment_link_mail", "/payment_link_mail/", factory=customer_factory
    )
    config.add_route("s_customer_passwd", "/customer_passwd/", factory=customer_factory)
    # config.add_route("s_order_archive", "/order_archive/", factory=customer_factory)
    config.add_route("s_order", "/order/", factory=customer_factory)
    config.add_route("s_print", "/print", factory=customer_factory)
    config.add_route(
        "s_order_change_data", "/order_selection/", factory=customer_factory
    )
    config.add_route("s_order_mail", "/order_print-{id:\\d+}", factory=customer_factory)
    config.add_route(
        "s_order_print", "/order_print-{id:\\d+}", factory=customer_factory
    )
    config.add_route(
        "s_order_processing", "/order_processing/", factory=customer_factory
    )
    config.add_route("s_order_cancelled", "/order_cancelled/", factory=customer_factory)
    config.add_route("s_order_success", "/order_success/", factory=customer_factory)
    config.add_route("s_order_summary", "/order_summary/", factory=customer_factory)
    config.add_route("s_order_view", "/customer_order_view/", factory=customer_factory)
    config.add_route("s_post", "/blog/{postname}-{id:\\d+}", factory=customer_factory)
    config.add_route("s_post_list", "/blog/", factory=customer_factory)
    config.add_route("s_search", "/search/", factory=customer_factory)
    config.add_route(
        "s_category",
        "/category/{seo_slug}-{category_id:\\d+}",
        factory=customer_factory,
    )
    config.add_route(
        "s_tag",
        "/tag/{tag_term}",
        factory=customer_factory,
    )
    config.add_route("s_product", "/{seo_slug}-{id:\\d+}", factory=customer_factory)
    config.add_route("s_product_search", "/product_search", factory=customer_factory)
    config.add_route("s_sitemap", "/xml/sitemap.xml", factory=customer_factory)
    config.add_route(
        "s_collection_search", "/collection_search", factory=customer_factory
    )
    config.add_route(
        "s_attribute_search", "/attribute_search", factory=customer_factory
    )
    config.add_route(
        "s_attribute_group_search", "/attribute_group_search", factory=customer_factory
    )

    # API
    # SATURN
    config.add_route(
        "api_saturn_address_country",
        "/emporatorium/api_saturn_address_country",
        factory=admin_factory,
    )
    config.add_route(
        "api_saturn_address_region",
        "/emporatorium/api_saturn_address_region",
        factory=admin_factory,
    )
    config.add_route(
        "api_saturn_address_client_address",
        "/emporatorium/api_saturn_address_client_address",
        factory=admin_factory,
    )
    config.add_route(
        "api_saturn_basket", "/emporatorium/api_saturn_basket", factory=admin_factory
    )
    config.add_route(
        "api_saturn_basket_action",
        "/emporatorium/api_saturn_basket/{action}",
        factory=admin_factory,
    )
    config.add_route(
        "api_saturn_document",
        "/emporatorium/api_saturn_document",
        factory=admin_factory,
    )
    config.add_route(
        "api_saturn_document_action",
        "/emporatorium/api_saturn_document/{action}",
        factory=admin_factory,
    )
    config.add_route(
        "api_saturn_form", "/emporatorium/api_saturn_form", factory=admin_factory
    )
    config.add_route(
        "api_saturn_form_action",
        "/emporatorium/api_saturn_form_action",
        factory=admin_factory,
    )
    config.add_route(
        "api_saturn_login", "/emporatorium/api_saturn_login", factory=admin_factory
    )
    config.add_route(
        "api_saturn_product", "/emporatorium/api_saturn_product", factory=admin_factory
    )

    config.add_route(
        "api_saturn_get_product_prices",
        "/emporatorium/api_saturn_get_product_prices",
        factory=admin_factory,
    )
    config.add_route(
        "api_saturn_get_product_stock",
        "/emporatorium/api_saturn_get_product_stock",
        factory=customer_factory,
    )
    config.add_route(
        "api_saturn_get_single_product_stock",
        "/emporatorium/api_saturn_get_single_product_stock",
        factory=customer_factory,
    )

    # API Inpost

    # config.add_route(
    #     "api_inpost_login", "/emporatorium/api_inpost_login", factory=admin_factory
    # )
    config.add_route(
        "inpost_package_list",
        "/emporatorium/inpost_package_list",
        factory=admin_factory,
    )
    config.add_route(
        "inpost_label_print",
        "/emporatorium/inpost_label_print",
        factory=admin_factory,
    )
    config.add_route(
        "inpost_create_package",
        "/emporatorium/inpost_create_package",
        factory=admin_factory,
    )
    config.add_route(
        "inpost_delete_package",
        "/emporatorium/inpost_delete_package",
        factory=admin_factory,
    )
    config.add_route(
        "inpost_repair_package",
        "/emporatorium/inpost_repair_package",
        factory=admin_factory,
    )

    config.add_route(
        "p24_request",
        "/p24_request",
        factory=admin_factory,
    )
    config.add_route(
        "p24_response",
        "/p24_response",
        factory=admin_factory,
    )

    config.add_route(
        "paypal_request",
        "/paypal_request",
        factory=admin_factory,
    )
    config.add_route(
        "paypal_order",
        "/paypal_order",
        factory=admin_factory,
    )
    config.add_route(
        "paypal_capture",
        "/paypal_capture/{order_id}",
        factory=admin_factory,
    )

    config.add_translation_dirs("emporium:locale")
    config.add_subscriber(
        "emporium.i18n.add_renderer_globals", "pyramid.events.BeforeRender"
    )
    config.add_subscriber("emporium.i18n.add_localizer", "pyramid.events.NewRequest")
    config.set_locale_negotiator(custom_locale_negotiator)


def customer_factory(request):
    return EmporiumFactory(request)


class EmporiumFactory:
    def __init__(self, request):
        pass

    def __acl__(self):
        return [(Allow, Everyone, "view"), (Allow, Authenticated, "view")]


def admin_factory(request):
    return EmporiumAdminFactory()


class EmporiumAdminFactory:
    def __init__(self):
        pass

    def __acl__(self):
        return [
            (Deny, Everyone, "view"),
            (Deny, "role:customer", "view"),
            (Allow, "role:emperor", "edit"),
        ]
