# subscribers.py

from pyramid.i18n import get_localizer, TranslationStringFactory
from pyramid.threadlocal import get_current_request
from pyramid.threadlocal import get_current_registry


def add_renderer_globals(event):
    request = event["request"]
    event["_"] = request.translate
    event["localizer"] = request.localizer


_ = TranslationStringFactory("emporium")


def add_localizer(event):
    request = event.request
    localizer = get_localizer(request)

    def auto_translate(*args, **kwargs):
        return localizer.translate(_(*args, **kwargs))

    request.localizer = localizer
    request.translate = auto_translate


def set_accepted_languages_locale(request):
    """Set the language depending on the browser settings."""
    if (
        getattr(request, "_LOCALE_", None) is not None
        or request.params.get("_LOCALE_") is not None
        or request.cookies.get(" _LOCALE_") is not None
    ):
        # _LOCALE_ was explicitly set, do not change
        return

    # _LOCALE_ is not set, try to get the preferred language
    if not request.accept_language:
        return
    accepted = request.accept_language

    settings = get_current_registry().settings
    languages = settings["pyramid.available_languages"].split()
    default_language = settings["pyramid.default_locale_name"]

    request._LOCALE_ = accepted.best_match(languages, default_language)
