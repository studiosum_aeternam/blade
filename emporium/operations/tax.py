from emporium import helpers, services


class TaxOperations(object):
    @classmethod
    def generate_tax_list(cls, request):
        return helpers.TaxHelpers.create_tax_dict_key_id(
            services.TaxService.tax_id_name_value_filter_active(request)
        )
