from emporium import services


class SpecialOperations(object):
    @classmethod
    def set_lowest_price_single_product(cls, request, product_id):
        product_dict = cls.create_special_prices_dict(
            services.ProductToSpecialService.special_rows_filter_product_id_special_list(
                request, product_id, (2, 4)
            )
        )
        cls.set_default_special_price(product_dict.values())

    @classmethod
    def set_lowest_price_multiple_products(cls, request, product_id):
        product_dict = cls.create_special_prices_dict(
            services.SpecialService.special_rows_filter_product_list(
                request, product_id
            )
        )
        cls.set_default_special_price(product_dict.values())

    @classmethod
    def set_default_special_price(cls, product_dict):
        for value in product_dict:
            value.sort(key=lambda x: getattr(x, "price"))
            value[0].default = True
            cls.disable_default_price_for_other_specials(value)

    @classmethod
    def disable_default_price_for_other_specials(cls, value):
        if len(value) > 1:
            for element in value[1:]:
                element.default = False

    @classmethod
    def create_special_prices_dict(cls, special_prices):
        product_dict = {}
        for item in special_prices:
            if not product_dict.get(item.product_id):
                product_dict[item.product_id] = []
            product_dict[item.product_id].append(item)
        return product_dict
