from datetime import datetime
from decimal import Decimal

from emporium import data_containers, helpers, services, operations


class CartOperations(object):
    @classmethod
    def cart_filter_cart_or_customer(cls, request):
        """
        Default method for checking cart contents for logged and nonlogged customers.
        It's really old code to be improved.
        """
        cart = None
        cid = request.session.get("cid")
        cart = (
            services.CartService.cart_filter_user_id(
                request, request.authenticated_userid
            )
            if request.authenticated_userid
            else services.CartService.cart_filter_cart_id(request, cid)
        )

        if cart and not cart.products_json:
            request.dbsession.delete(cart)
        return cart

    @classmethod
    def cart_merge(cls, request):
        """
        Method for merging cart contents for non-logged and logged customers. It's invoked
        during auth-in, also it removes non-logged cart record from the db.
        """
        if not (session := request.session):
            return
        cid_cart_content = (
            services.CartService.cart_filter_cart_id(request, session["cid"])
            if session.get("cid")
            else False
        )
        uid_cart_content = (
            services.CartService.cart_filter_user_id(request, session["uid"])
            if session.get("uid")
            else False
        )
        if cid_cart_content:
            if uid_cart_content:
                uid_cart_content.products_json = (
                    uid_cart_content.products_json | cid_cart_content.products_json
                )
                uid_cart_content.date_accessed = datetime.now()
                request.dbsession.delete(cid_cart_content)
                request.dbsession.flush()
            else:
                cid_cart_content.customer_id = session.get("uid")
                uid_cart_content = cid_cart_content

    @classmethod
    def product_requires_special_transport(cls, product, data_options):
        return data_containers.CartOptions(
            product_in_cart=False,
            transport_fee=None
            # [
            #     item.fee_value
            #     for item in data_options.transport_dict.values()
            #     if item.equotation and item.fee_value and item.transport_id == 4
            # ]
            # if product.forbidden_transport_methods
            # else None,
        )

    @classmethod
    def format_tax_dict_values(cls, params: dict):
        # JSON cannot accept
        for k, v in params.get("tax_dict").items():
            params["tax_dict"][k] = helpers.CommonHelpers.price_precision(v)

        # for k, v  in params.get('tax_dict').items():
        # if v == 0:
        # del params['tax_dict'][k]

    @classmethod
    def update_cart_parameters(
        cls,
        cart_contents: dict,
        params: dict,
        data_options,
        data_options_filtered,
    ):
        """
        Dispatch method for calculating all cart parameters and summary values
        """
        for product_id, item in cart_contents.items():
            featured_products_with_separate_price = []
            if data_options_filtered.special_filtered_dict.get(product_id):
                featured_products_with_separate_price = [
                    item
                    for item in data_options_filtered.special_filtered_dict[
                        product_id
                    ].values()
                    if item.price
                ]
            cls.calculate_value_of_single_product_in_cart(
                item,
                featured_products_with_separate_price,
                params,
                data_options,
            )
            cls.summarize_cart_parameters(params, item)
        params["summary"] = helpers.CommonHelpers.price_precision(params["summary"])
        # operations.TransportOperations.calculate_transport_unit(
        #     params["weight"], data_options
        # )
        CartOperations.format_tax_dict_values(params)

    @classmethod
    def calculate_value_of_single_product_in_cart(
        cls, item, featured_product_list, params, data_options
    ):
        cls.calculate_packages(item)
        cls.calculate_price_taxes_value_of_product(
            item,
            featured_product_list,
            data_options.tax_dict[item.tax_id],
        )

    # @classmethod
    # def calculate_transport_unit(
    #     cls, params: dict, data_options: dict, cheapest_slot=False
    # ):
    #     """
    #     Calculate number of EU palletes needed for the transport
    #     """
    #     selected_slot = {
    #         "name": "",
    #         "slot_name": "",
    #         "unit_weight": 0,
    #         "fee_summarized": 0,
    #         "transport_unit": 0,
    #     }
    #     params["palette"] = 1
    #     params["suffix"] = helpers.CommonHelpers.plural_pl(params["palette"], "male")

    @classmethod
    def calculate_packages(cls, item: dict):
        """
        Calculate number of the sold packages when price is based on dimensions/volume (not unit sold)
        """
        item.cart_quantity = round(Decimal(item.cart_quantity_real_unit) / item.unit)

    @classmethod
    def calculate_price_taxes_value_for_non_virtual_unit_of_special_product(
        cls, item, special_item, summarized_price
    ):
        cart_quantity = item.cart_quantity
        cart_quantity_real_unit = Decimal(item.cart_quantity_real_unit)
        real_stored_units = Decimal(special_item.quantity * item.unit)
        if real_stored_units and cart_quantity_real_unit > real_stored_units:
            cart_quantity_real_unit -= real_stored_units
            calculated_value = Decimal(
                helpers.CommonHelpers.price_precision(
                    special_item.price * real_stored_units
                )
            )
            summarized_price += Decimal(calculated_value)
            item.tax_value = helpers.CommonHelpers.price_precision(
                Decimal(item.unit_tax) * real_stored_units
            )
            cart_detailed_special_quantity = real_stored_units  # this is not a bug
            # Basic price
            item.cart_value_detailed[0] = {
                "cart_detailed_special_real_quantity": cart_quantity_real_unit,
                "cart_detailed_special_price": item.price,
                "cart_detailed_special_price_net": item.price_net,
                "cart_detailed_special_tax": 0,
            }
        else:
            calculated_value = Decimal(
                helpers.CommonHelpers.price_precision(
                    special_item.price * cart_quantity_real_unit
                )
            )
            summarized_price += calculated_value
            item.tax_value = 0
            cart_detailed_special_quantity = cart_quantity_real_unit
            cart_quantity_real_unit = 0
        item.cart_value_detailed[special_item.special_id] = {
            "cart_detailed_special_real_quantity": cart_detailed_special_quantity,
            "cart_detailed_special_price": calculated_value,
            "cart_detailed_special_price_net": 0,
            "cart_detailed_special_tax": 0,
        }
        if special_item.storage_aware and item.not_promoted_storage_summary:
            item.not_promoted_storage_summary -= special_item.quantity
        return (cart_quantity_real_unit, summarized_price)

    @classmethod
    def calculate_price_taxes_value_for_virtual_special_product(
        cls, item, special_item, summarized_price
    ):
        cart_quantity_real_unit = item.cart_quantity_real_unit
        summarized_price += Decimal(
            f"{special_item.price * cart_quantity_real_unit:.2f}"
        )
        item["tax_value"] = item["unit_tax"] * cart_quantity_real_unit
        return summarized_price

    @classmethod
    def calculate_price_taxes_value_for_special_product(
        cls,
        featured_product_list,
        item,
        tax_value,
        summarized_price,
    ):
        """
        Every promotion has separate stock and price values
        """
        cart_quantity_real_unit = item.cart_quantity_real_unit
        for special_item in featured_product_list:
            if (
                special_item.quantity
                and special_item.quantity > 0
                and special_item.price
                and special_item.price > 0
            ):
                # Digital product
                if item.transport_method == 1:
                    summarized_price = (
                        cls.calculate_price_taxes_value_for_virtual_special_product(
                            item,
                            special_item,
                            summarized_price,
                        )
                    )
                # Physical product
                else:
                    (
                        cart_quantity_real_unit,
                        summarized_price,
                    ) = cls.calculate_price_taxes_value_for_non_virtual_unit_of_special_product(
                        item, special_item, summarized_price
                    )
        # You can order multiple products where only few of them have lowered prices
        if cart_quantity_real_unit > 0:
            summarized_price = cls.calculate_price_taxes_value_for_product(
                cart_quantity_real_unit,
                item,
                tax_value,
                summarized_price,
            )
        return summarized_price

    @classmethod
    def calculate_price_taxes_value_for_product(
        cls, cart_quantity_real_unit, item, tax_value, summarized_price
    ):
        calculated_value = Decimal(
            helpers.CommonHelpers.price_precision(
                Decimal(item.price) * cart_quantity_real_unit
            )
        )
        summarized_price += calculated_value
        item.unit_tax = Decimal(helpers.CommonHelpers.price_tax(item.price, tax_value))
        item.tax_value = Decimal(
            helpers.CommonHelpers.price_precision(
                item.unit_tax * cart_quantity_real_unit
            )
        )
        item.cart_value_detailed[0] = {
            "cart_detailed_special_real_quantity": cart_quantity_real_unit,
            "cart_detailed_special_price": calculated_value,
            "cart_detailed_special_price_net": 0,
            "cart_detailed_special_tax": item.tax_value,
        }
        return summarized_price

    @classmethod
    def calculate_single_product_based_on_special_and_normal_price(
        cls,
        featured_product_list,
        item,
        cart_quantity_real_unit,
        summarized_price,
        tax_value,
    ):
        """ """
        if cart_quantity_real_unit > 0 and not featured_product_list:
            summarized_price = cls.calculate_price_taxes_value_for_product(
                cart_quantity_real_unit,
                item,
                tax_value,
                summarized_price,
            )
        else:
            summarized_price = cls.calculate_price_taxes_value_for_special_product(
                featured_product_list,
                item,
                tax_value,
                summarized_price,
            )
        return summarized_price

    @classmethod
    def calculate_price_taxes_value_of_product(
        cls, item: dict, featured_product_list: list, tax_value: int
    ):
        """
        Calculate product value and price
        """
        cart_quantity_real_unit = Decimal(item.cart_quantity_real_unit)
        summarized_price = Decimal(0)
        item.tax_value = Decimal(0)
        summarized_price = (
            cls.calculate_single_product_based_on_special_and_normal_price(
                featured_product_list,
                item,
                cart_quantity_real_unit,
                summarized_price,
                tax_value,
            )
        )
        item.value = Decimal(helpers.CommonHelpers.price_precision(summarized_price))

    @classmethod
    def summarize_cart_parameters(cls, params: dict, item: dict):
        params["product_values"][item.product_id] = item.value
        params["quantity"] += item.cart_quantity
        params["summary"] += item.value
        params["tax"] += item.tax_value
        params["weight"] += Decimal(item.cart_quantity_real_unit) * Decimal(
            str(item.weight)
        )

    @classmethod
    def calculate_cart_parameters(
        cls,
        request,
        cart_contents: dict,
        data_options,
        data_options_filtered: dict,
    ):
        params = {
            "product_values": {},  # TODO
            "quantity": 0,  # Top number in the header?
            "selected_slot": {"physical": {}, "digital": {}},
            "summary": Decimal(0),
            "tax": Decimal(0),
            "tax_dict": {},
            "weight": 0,
        }

        cls.update_cart_parameters(
            cart_contents,
            params,
            data_options,
            data_options_filtered,
        )
        cls.push_current_cart_quantities_to_session(request, cart_contents)
        return params

    @classmethod
    def push_current_cart_quantities_to_session(cls, request, cart_items):
        products, quantity = helpers.CartHelpers.get_current_cart_parameters(cart_items)
        request.session["cart_contents"] = {
            "products": products,
            "quantity": quantity,
        }
