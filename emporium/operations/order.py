from decimal import Decimal
from datetime import datetime

from emporium import data_containers, helpers, operations, models, services


class OrderOperations(object):
    @classmethod
    def update_order_data(cls, request, form, order, customer, first_address):
        form_data = {}
        if request.method == "POST":
            # Send current form
            form_data = cls.format_customer_data(form)
            request.session["privacy_policy"] = form.privacy_policy.data
        elif order or customer:
            # Fill using preexisting data - user can be logged so we start with him/her
            form_data = cls.format_customer_data_json(order, customer, first_address)
            if order:
                # Fill company data
                cls.format_company_invoice_data(
                    order, customer, first_address, form_data
                )
                # Why check special transport rules here??
                cls.special_transport_rules_dispatch(request, form, form_data, order)
        return form_data

    @classmethod
    def special_transport_rules_dispatch(cls, request, form, form_data, order):
        custom_transport_dispatch = {"inpost": cls.inpost_passed_values}
        if form.transport_id.data:
            custom_transport_dispatch.get(
                form.transport_dict[int(form.transport_id.data)].module_name
            )(request, form_data, order)
        elif (
            order
            and order.params_json
            and order.params_json.get("transport_module_name")
            and custom_transport_dispatch.get(
                order.params_json["transport_module_name"]
            )
        ):
            custom_transport_dispatch.get(order.params_json["transport_module_name"])(
                request, form_data, order
            )

    @classmethod
    def inpost_passed_values(cls, request, form_data, order):
        form_data["inpost_point_selected"] = order.delivery_json.get(
            "inpost_point_selected"
        )

    @classmethod
    def format_company_invoice_data(cls, order, customer, first_address, form_data):
        if order.client_json["company_nip"]:
            form_data["company_nip"] = order.client_json["company_nip"]
        elif customer and first_address and first_address.company_nip:
            form_data["company_nip"] = first_address.company_nip
        else:
            form_data["company_nip"] = ""
        if order.client_json["company_name"]:
            form_data["company_name"] = order.client_json["company_name"]
        elif customer and first_address and first_address.company_name:
            form_data["company_name"] = first_address.company_name
        else:
            form_data["company_name"] = ""
        form_data["customer_type"] = order.customer_type
        form_data["payment_method"] = order.payment
        # form_data["invoice"] = order.invoice

    @classmethod
    def format_customer_data_json(cls, order, customer, first_address):
        return {
            "first_name": order.client_json.get("first_name")
            if order
            else customer.first_name,
            "last_name": order.client_json.get("last_name")
            if order
            else customer.last_name,
            "telephone": order.client_json.get("telephone")
            if order
            else customer.telephone,
            "mail": order.client_json.get("mail") if order else customer.mail,
            "street": order.client_json.get("street")
            if order
            else first_address.street,
            "postcode": order.client_json.get("postcode")
            if order
            else first_address.postcode,
            "city": order.client_json.get("city") if order else first_address.city,
            "country": order.client_json.get("country")
            if order
            else first_address.country,
            "additional_info": order.additional_info if order else "",
            "physical_delivery_method": "",
        }

    @classmethod
    def format_customer_data(cls, form):
        return {
            "additional_info": form.additional_info.data,
            "city": form.city.data,
            "company_name": form.company_name.data,
            "company_nip": form.company_nip.data,
            "country": form.country.data,
            "delivery_address": form.delivery_address.data,
            "delivery_city": form.delivery_city.data,
            "delivery_company_name": form.delivery_company_name.data,
            "delivery_company_nip": form.delivery_company_nip.data,
            "delivery_country": form.delivery_country.data,
            "delivery_first_name": form.delivery_first_name.data,
            "delivery_last_name": form.delivery_last_name.data,
            # "delivery_method": form.delivery_method.data,
            "delivery_postcode": form.delivery_postcode.data,
            "delivery_street": form.delivery_street.data,
            "delivery_telephone": form.delivery_telephone.data,
            "first_name": form.first_name.data,
            "invoice": form.invoice.data,
            "customer_type": form.customer_type.data,
            "last_name": form.last_name.data,
            "mail": form.mail.data,
            "registration_type": form.registration_type.data,
            "payment_method": form.payment_method.data,
            "postcode": operations.ProductOperations.strip_value(form.postcode.data),
            "privacy_policy": form.privacy_policy.data,
            "psswd_hsh": "",
            "street": form.street.data,
            "telephone": form.telephone.data,
            "physical_delivery_method": form.physical_delivery_method.data,  # TODO???
            "uniqueness": form.uniqueness.data,
        }

    @classmethod
    def add_transport_fees_to_cart_total(cls, cart_parameters, order):
        """
        Customer can select one physical and one digital delivery method for a single order
        """
        cart_transport_fee = Decimal(0)
        transport_fee = 0
        transport_types = {0: "physical", 1: "digital"}
        for transport_type in transport_types:
            if order.transport_params_json.get(transport_type):
                if isinstance(order.transport_params_json.get(transport_type), dict):
                    cart_transport_fee += cls.add_transport_fee_to_cart_value(
                        order.transport_params_json[transport_type]
                    )
                    # Physical objects are send using a single method - they are always a dict - this is wrong approach to the problem
                    cart_parameters[
                        "transport_unit_name"
                    ] = order.transport_params_json[transport_type].get(
                        "transport_unit_name"
                    )
                elif isinstance(order.transport_params_json.get(transport_type), list):
                    for listed_transport in order.transport_params_json.get(
                        transport_type
                    ):
                        cart_transport_fee += cls.add_transport_fee_to_cart_value(
                            listed_transport
                        )
        cart_parameters["summary"] = helpers.CommonHelpers.decimal_jsonb_compatible(
            Decimal(cart_parameters["summary"]) + cart_transport_fee
        )
        cart_parameters[
            "transport_fee"
        ] = helpers.CommonHelpers.decimal_jsonb_compatible(cart_transport_fee)

    @classmethod
    def add_transport_fee_to_cart_value(cls, single_transport):
        return Decimal(single_transport.get("transport_fee", 0))

    @classmethod
    def calulate_selected_transport_method_fees(cls, cart_weight, transport_methods):
        # This is not an optimal solution, to be repaired
        for transport_data in transport_methods.values():
            if isinstance(transport_data, dict):
                operations.TransportOperations.calculate_cost_of_transport_method(
                    cart_weight, transport_data
                )
            elif isinstance(transport_data, list):
                for digital_transport in transport_data:
                    operations.TransportOperations.calculate_cost_of_transport_method(
                        cart_weight, digital_transport
                    )
        return transport_methods

    @classmethod
    def calulate_selected_payment_method_fees(
        cls, cart_parameters, selected_payment, payment_id
    ):
        cart_parameters[
            "payment"
        ] = operations.PaymentOperations.calculate_cost_of_payment_method(
            cart_parameters,
            {
                "payment_id": payment_id,
                "payment_name": selected_payment.name,
                "payment_description": selected_payment.description,
                # "payment_excerpt": payment_dict[payment_id].excerpt,
                "payment_type": selected_payment.type,
            },
            selected_payment.module_settings,
        )

    @classmethod
    def calculate_selected_transport_and_payment_fees(
        cls, cart_parameters, data_options, order
    ):
        # IThere is no fee for digital delivery - yet
        # Do I still need it? The next cls calculates the same thing?
        order.transport_params_json = cls.calulate_selected_transport_method_fees(
            cart_parameters["weight"],
            order.transport_params_json,
        )
        cls.add_transport_fees_to_cart_total(cart_parameters, order)
        # Should  order.payment be moved to the payment_params_json?
        cls.calulate_selected_payment_method_fees(
            cart_parameters, data_options.payment_dict[order.payment], order.payment
        )
        return cart_parameters, order

    @classmethod
    def send_order_confirmation_email(cls, request, order, settings):
        recipients = order.client_json["mail"]
        kwargs = {
            "cookies": request.cookies,
            "host": request.host,
        }
        request_mail_generate = request.blank(
            request.route_url(
                "s_order_mail",
                id=order.order_id,
            ),
            **kwargs,
        )
        html = request.invoke_subrequest(request_mail_generate, use_tweens=True).text
        subject = "Potwierdzenie zamówienia - " + settings["name"]
        link = request.route_url(
            "s_mail",
            _query=(
                ("recipients", recipients),
                ("html", html),
                ("subject", subject),
                ("sender", settings["mail_username"]),
                ("bcc", settings["mail_username"]),
            ),
        )
        response = request.invoke_subrequest(
            request.blank(link, **kwargs), use_tweens=True
        )
        order_history = services.OrderService.order_filter_id(
            request, request.session["order_confirmed"]
        )
        order_history.mail_sent = True

    @classmethod
    def send_system_mail(cls, request, mail_contents, mail_params):
        mail_body = operations.CommonOperations.generate_mail_body(
            request,
            mail_contents,
            mail_params,
        )
        operations.CommonOperations.send_email(
            request,
            route="s_mail",
            query=(
                ("html", mail_body),
                ("recipients", mail_params["recipients"]),
                ("subject", mail_contents["subject"]),
            ),
        )

    @classmethod
    def check_external_data_sources(cls, request, order, module_names, settings):
        # Placeholder for the future external PaymentOperations
        if module_names:
            for item in module_names:
                operations.TransportOperations.check_external_transport_data_sources(
                    request, order, item, settings
                )

    @classmethod
    def status_recieved_payment_operations(cls, request, order, settings):
        cls.set_new_order_status(request, order)

        # if settings.get("product_settings") and settings["product_settings"].get("downloadable_products", 0) == 1 and :
        #     cls.send_

    @classmethod
    def set_new_order_status(cls, request, order):
        """
        Paid order has ID 12 (change to named status), add new order history status
        """
        order.status = 12
        request.dbsession.add(
            models.OrderHistory(
                date=datetime.now(),
                mail_sent=False,
                order_id=order.order_id,
                status_id=order.status,
            )
        )

    @classmethod
    def remove_order_session_variables(cls, request):
        session_values = [
            "attempt",
            "cart_contents",
            "cid",
            "delivery",
            "login_error",
            "oid",
            "order_confirmed",
            "payment",
            "privacy_policy",
            "physical_delivery_method",
        ]
        if request.session["registration_type"] == "quick_buy":
            session_values.append("registration_type")
        for _ in session_values:
            if request.session.get(_):
                del request.session[_]
        cookies = ["delivery_details", "customer_type"]
        for _ in cookies:
            request.response.delete_cookie(_)

    @classmethod
    def order_transport_params(cls, request, order, data_options, form):
        # TODO - two methods of transport are borked, and only the first is logged?
        # Start with physical go with order, return to the category and add digital
        # There is no digital transport method - to be repaired ASAP
        order.transport_params_json = {}
        for transport in data_options.transport_dict.values():
            if form.digital_products_only.data == 1 and transport.transport_method == 1:
                order.transport_params_json[1] = [
                    operations.TransportOperations.unified_selected_transport_method_parameters(
                        transport, slot
                    )
                    for slot in transport.slots.values()
                ]
            elif slot := transport.slots.get(form.physical_delivery_method.data):
                order.transport_params_json[
                    0
                ] = operations.TransportOperations.unified_selected_transport_method_parameters(
                    transport, slot
                )
            elif transport.transport_method == 1:
                order.transport_params_json[1] = [
                    operations.TransportOperations.unified_selected_transport_method_parameters(
                        transport, slot
                    )
                    for slot in transport.slots.values()
                ]
        operations.TransportOperations.custom_order_transport_variables(
            request, form, order, data_options.transport_dict
        )
