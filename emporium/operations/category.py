from datetime import datetime
from slugify import slugify
import copy
from emporium import services, helpers, data_containers, models


class CategoryOperations(object):
    @classmethod
    def category_manufacturer_front_page_description_and_meta(
        cls,
        request,
        category,
        filter_params,
        misc_options,
        data_options,
        meta_options,
        filler,
    ):
        filter_params.category_first_page = True
        meta_data = services.CategoryService.category_meta_tree_description_filter_band_category_manufacturer_collection(
            request,
            tuple(filter_params.band),
            filter_params.category_id,
            filter_params.collection,
            tuple(filter_params.manufacturer),
        )
        meta_options.robots_meta = "index, follow"
        single_manufacturer = len(filter_params.manufacturer) == 1
        single_collection = len(filter_params.collection) == 1
        single_band = len(filter_params.band) == 1
        if single_manufacturer and data_options.manufacturer_dict:
            query = [("manufacturer", min(filter_params.manufacturer))]
            filler += f" {data_options.manufacturer_dict[min(filter_params.manufacturer)].name}"
            if single_collection and data_options.collection_dict:
                filler += f" {data_options.collection_dict[min(filter_params.collection)].name}"
                query += [("collection", min(filter_params.collection))]
            elif data_options.collection_dict:
                misc_options.rotate_collections = True
            meta_options.description = (meta_data and meta_data.description) or ""
            meta_options.description_long = (
                meta_data and meta_data.description_long
            ) or ""
            meta_options.meta_description = (
                meta_data and meta_data.meta_description
            ) or filler
            meta_options.rel_link = request.route_url(
                misc_options.search_url,
                seo_slug=category.seo_slug,
                category_id=filter_params.category_id,
                _query=query,
            )
        if single_band:
            band_id = min(filter_params.band)
            query = [("band", band_id)]
            filler += f" {data_options.band_dict[band_id].name}"
            meta_options.description = (meta_data and meta_data.description) or ""
            meta_options.description_long = (
                meta_data and meta_data.description_long
            ) or ""
            meta_options.meta_description = (
                meta_data and meta_data.meta_description
            ) or filler
            meta_options.rel_link = request.route_url(
                misc_options.search_url,
                seo_slug=category.seo_slug,
                category_id=filter_params.category_id,
                _query=query,
            )
        meta_options.title = (meta_data and meta_data.u_title) or filler
        meta_options.u_h1 = (meta_data and meta_data.u_h1) or filler
        if not single_manufacturer and not single_collection and not single_band:
            meta_options.description = category.description
            meta_options.description_long = category.description_long

    @classmethod
    def category_front_page_description_and_meta(
        cls, request, category, filter_params, misc_options, meta_options
    ):
        misc_options.search_url = "s_category"
        meta_options.rel_link = request.route_url(
            misc_options.search_url,
            seo_slug=category.seo_slug,
            category_id=filter_params.category_id,
        )
        meta_options.seo_slug = category.seo_slug
        return helpers.CommonHelpers.capword(category.u_title or (category.name))

    @classmethod
    def update_category_information(cls, request, category_id, form, products_ids):
        new_product_list = cls.get_product_list_from_form(form)
        cls.add_category_related_products(
            request, category_id, products_ids, new_product_list
        )
        cls.delete_category_related_products(request, products_ids, new_product_list)

    @classmethod
    def get_product_list_from_form(cls, form):
        return [
            int(e) for e in form.product_list.data.split(",") if form.product_list.data
        ]

    @classmethod
    def add_category_related_products(
        cls, request, category_id, products_ids, new_product_list
    ):
        if to_add := set(new_product_list).difference(products_ids):
            for product_id in list(to_add):  # WTF
                request.dbsession.add(
                    models.ProductToCategory(
                        category_id=category_id, product_id=product_id
                    )
                )

    @classmethod
    def delete_category_related_products(cls, request, products_ids, new_product_list):
        if to_delete := set(products_ids).difference(new_product_list):
            if categories := services.ProductService.product_to_categories_filter_product_list(
                request, to_delete
            ):
                for item in categories:
                    request.dbsession.delete(item)

    @classmethod
    def update_category_data(cls, category, form):
        form.populate_obj(category)
        category.parent_id = form.parent_id.data if form.parent_id.data != "0" else ""
        category.date_modified = datetime.now()
        if not category.date_added:  # Old categories can be damaged
            category.date_added = category.date_modified

    @classmethod
    def update_category_description_data(cls, category_description, form):
        form.populate_obj(category_description)
        category_description.seo_slug = slugify(form.seo_slug.data or form.name.data)

    @classmethod
    def category_create(cls, form):
        date_added = datetime.now()
        return models.Category(
            date_modified=date_added,
            date_added=date_added,
            image=form.image.data,
            name=form.name.data,
            parent_id=form.parent_id.data if form.parent_id.data != "0" else "",
            sort_order=form.sort_order.data,
            status=form.status.data,
        )

    @classmethod
    def create_category(cls, request, form):
        category = cls.category_create(form)
        request.dbsession.add(category)
        request.dbsession.flush()
        return category

    @classmethod
    def create_category_meta_tree_description(cls, request, form, category):
        request.dbsession.add(
            models.CategoryMetaTreeDescriptions(
                category_id=category.category_id,
                collection_id=None,
                description=form.description.data,
                description_long=form.description_long.data,
                language_id=form.language_id.data,
                manufacturer_id=None,
                meta_description=form.meta_description.data,
                seo_slug=slugify(form.seo_slug.data or form.name.data),
                u_h1=form.u_h1.data,
                u_title=form.u_title.data,
            )
        )

    @classmethod
    def recursive_parent_adding(cls, category_tree, categories_passed):
        """
        Repeat looking and adding parents to the stack until the list does not change
        """
        new_categories_passed = cls.add_parent_categories(
            categories_passed, category_tree
        )
        if new_categories_passed == categories_passed:
            return new_categories_passed
        else:
            return cls.recursive_parent_adding(category_tree, new_categories_passed)

    @classmethod
    def add_parent_categories(cls, categories_passed, category_tree):
        """
        Add missing parent category in the nested tree
        """
        backup_categories_passed = copy.deepcopy(categories_passed)
        for item in category_tree:
            if (
                item.category_id in categories_passed
                and item.parent_id not in categories_passed
                and item.parent_id not in (0,)
            ) and item.parent_id:  # Don't add None as a valid category
                backup_categories_passed.append(item.parent_id)
        return backup_categories_passed
