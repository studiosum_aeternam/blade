from decimal import Decimal

from emporium import data_containers, helpers, operations, services


class PaymentOperations(object):
    @classmethod
    def remove_payment_methods_using_current_method_settings(
        cls, current_slot, payment_dict
    ):
        for payment in current_slot.removed_payments:
            del payment_dict[payment]

    @classmethod
    def calculate_cost_of_payment_method(
        cls,
        cart_parameters,
        selected_payment,
        payment_module_settings,
    ):
        payment_fee = cls.calculate_payment_fee_value(
            cart_parameters, payment_module_settings
        )
        selected_payment[
            "payment_fee"
        ] = helpers.CommonHelpers.decimal_jsonb_compatible(payment_fee)
        cart_parameters["summary"] = helpers.CommonHelpers.price_precision(
            Decimal(cart_parameters["summary"]) + payment_fee
        )
        return selected_payment

    @classmethod
    def calculate_payment_fee(cls, cart_parameters, payment_module_settings):
        value = cls.calculate_payment_fee_value(
            cart_parameters, payment_module_settings
        )
        return helpers.CommonHelpers.decimal_jsonb_compatible(value)

    @classmethod
    def calculate_payment_fee_value(cls, cart_parameters, payment_module_settings):
        payment_fee = 0
        if payment_module_settings:
            payment_fee += Decimal(
                payment_module_settings.get("payment_manipulation_fee", 0)
            ) + (
                Decimal(payment_module_settings.get("payment_percent_fee", 0)) / 100
            ) * Decimal(
                cart_parameters["summary"]
            )
        return payment_fee

    @classmethod
    def calculate_cost_of_payment_methods(cls, cart_parameters, payment_dict):
        for payment_id, payment_data in payment_dict.items():
            payment_data.module_settings["payment_fee"] = cls.calculate_payment_fee(
                cart_parameters,
                payment_data.module_settings,
            )
        return payment_dict

    @classmethod
    def return_applicable_payment_methods(
        cls, payment_dict, transport_types, transport_dict
    ):
        return cls.payment_methods_filtered_by_special_transports_rules(
            payment_dict,
            transport_types,
            transport_dict,
        )

    @classmethod
    def payment_methods_filtered_by_special_transports_rules(
        cls,
        payment_dict,
        transport_types,
        transport_dict,
    ):
        # TODO add in admin info/checkbox for transport method with proper flag descriptions
        if (1 or 2) in transport_types:
            return {
                payment_id: payment_data
                for payment_id, payment_data in payment_dict.items()
                if payment_data.module_settings.get("transport_type", "") != 0
            }
        return payment_dict

    @classmethod
    def remove_transport_methods_according_to_special_transports_rules(
        cls,
        cart_contents,
        cart_parameters,
        transport_dict,
        transport_filtered_dict,
    ):
        transport_dict = (
            cls.return_used_special_methods(transport_dict, transport_filtered_dict)
            if transport_filtered_dict
            else cls.remove_all_special_methods(transport_dict)
        )
        transport_dict = cls.check_validity_of_possible_transport_methods(
            cart_contents,
            cart_parameters,
            transport_dict,
            transport_filtered_dict,
        )
        return transport_dict
