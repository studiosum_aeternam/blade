from datetime import datetime

from emporium import data_containers
from emporium import helpers


class FilterOperations(object):
    @classmethod
    def generate_filter_params(cls, request, availability_attributes):
        return data_containers.FilterOptions(
            attributes={
                int(x)
                for x in request.params.getall("attributes")
                if x.isdigit() and int(x) not in availability_attributes
            },
            availability={
                int(x)
                for x in request.params.getall("attributes")
                if x.isdigit() and int(x) in availability_attributes
            },
            band={int(x) for x in request.params.getall("band") if x.isdigit()},
            category_id=request.matchdict.get("category_id"),
            collection={
                int(x) for x in request.params.getall("collections") if x.isdigit()
            },
            descripted_front_page=bool(request.matchdict.get("category_id")),
            description=request.params.get("description", False),
            items_per_page=request.params.get("items_per_page", 36),
            manual_discount=request.params.get("manual_discount", False),
            manufacturer={
                int(x) for x in request.params.getall("manufacturer") if x.isdigit()
            },
            page=request.params.get("page", 1),
            price_max=request.params.get("price_max", None),
            price_min=request.params.get("price_min", None),
            sort_method=request.params.get(
                "sort", request.registry.settings.get("default_sort_method")
            ),  # date_added for Case, name_asc for Blade
            status=1,
            searchstring=None,
            term=request.params.get("term", ""),
            # today=datetime.now().strftime("%Y-%m-%d"),
        )

    @classmethod
    def filter_price_params(cls, filter_params, query_items):
        """
        Verify if price filters are numbers and if they are correct pass them to the price formatter.
        """
        if filter_params.price_min:
            filter_params.price_min = cls.sanitize_price_filters(
                filter_params.price_min
            )
        if filter_params.price_max:
            filter_params.price_max = cls.sanitize_price_filters(
                filter_params.price_max
            )
        if filter_params.price_max or filter_params.price_min:
            helpers.ProductHelpers.price_filter_format(filter_params, query_items)

    @classmethod
    def sanitize_price_filters(cls, price):
        """
        Verify if passed variable is a number. Only dot is removed due
        to filtering performed by JS on the front.
        """
        return price if price.replace(".", "", 1).isdigit() else False

    @classmethod
    def filter_attribute_params(cls, filter_params):
        """
        If any of the attributes or availabilties is selected disable front page
        """
        if filter_params.attributes or filter_params.availability:
            filter_params.attribute_list_checked = True
            filter_params.descripted_front_page = False
            filter_params.sticky = True

    @classmethod
    def filter_collection_params(cls, filter_params):
        """
        If colelction is selected disable front page
        """
        if filter_params.collection:
            filter_params.collection_list_checked = True
            filter_params.sticky = True
            filter_params.descripted_front_page = (
                len(filter_params.collection) == 1
                and len(filter_params.manufacturer) == 1
                and filter_params.descripted_front_page
            )

    @classmethod
    def filter_band_params(cls, filter_params):
        """
        If band is selected enable filter and check if descripted front page can be displayed
        """
        if filter_params.band:
            filter_params.band_list_checked = True
            filter_params.sticky = True
            filter_params.descripted_front_page = len(filter_params.band) == 1

    @classmethod
    def filter_manufacturer_params(cls, filter_params):
        """
        If manufacturer is selected enable filter and check if descripted front page can be displayed
        """
        if filter_params.manufacturer:
            filter_params.manufacturer_checked = True
            filter_params.sticky = True
            filter_params.descripted_front_page = len(filter_params.manufacturer) == 1

    @classmethod
    def filter_items_per_page(cls, request):
        """
        Set how many products are visible (currently disabled)
        """
        if items_per_page := request.params.get("items_per_page", 36):
            request.session["items_per_page"] = items_per_page
        elif request.cookies.get("session") and request.session.get("items_per_page"):
            items_per_page = request.session["items_per_page"]
        return items_per_page

    @classmethod
    def get_display_mode_params(cls, request, filter_params):
        """
        Get and set display mode grid/list
        """
        display_mode = request.params.get("display_mode")
        if display_mode:
            request.session.display_mode = display_mode
        elif request.cookies.get("session") and request.session.get("display_mode"):
            display_mode = request.session["display_mode"]
        filter_params.display_mode = display_mode
        filter_params.switch_mode = "list" if display_mode == "grid" else "grid"
        return display_mode

    @classmethod
    def filter_main_page_description(cls, filter_params):
        """
        When description is searched disable front page description
        """
        if filter_params.description:
            filter_params.descripted_front_page = False

    @classmethod
    def check_for_first_category_page(cls, request, filter_params):
        """
        Verify pagination parameters if this is a front page (filter) or/and this page
        has it's description.
        """
        if not request.params.get("page") and (
            filter_params.pager["first_page"] is None
            and filter_params.pager["current_page"] is None
            and filter_params.pager["next_page"] is None
            or filter_params.pager.get("current_page")
            and filter_params.pager["current_page"].get("value") == 1
        ):
            filter_params.first_page = True
        else:
            filter_params.descripted_front_page = False
