import itertools
from copy import copy
from decimal import Decimal
from operator import methodcaller
from dataclasses import asdict
import math

from emporium import data_containers, helpers, operations, services


class TransportOperations(object):
    @classmethod
    def delete_transport(cls, request, transport_for_deletion, intoduct_to_transport):
        to_delete = [
            item
            for item in product_to_transport
            if item.product_transport_id in transport_for_deletion
        ]
        operations.CommonOperations.delete_from_database(request, to_delete)

    @classmethod
    def update_transport_settings(cls, request):
        """
        Return update function for transport slot upadte
        """
        return cls.transport_settings_dispatcher(request)

    @classmethod
    def update_transport_size_slots(
        cls, request, settings, transport_id, transport_slots_old_elements
    ):
        """
        Return update function for transport slot upadte
        """
        if request.POST.getall("transport_slot_id") or transport_slots_old_elements:
            transport_slots_current_elements = (
                services.TransportService.transport_slots_full_filter_transport_list(
                    request,
                    settings,
                    [int(x) for x in request.POST.getall("transport_slot_id") if x],
                )
            )
            cls.update_transport_size_slots_dispatch(
                request,
                transport_id,
                transport_slots_current_elements,
                transport_slots_old_elements,
            )

    @classmethod
    def update_transport_size_slots_dispatch(
        cls,
        request,
        transport_id,
        transport_slots_current_elements,
        transport_slots_old_elements,
    ):
        """
        First remove unused elements than find the correct transport method
        """
        cls.transport_slots_delete(
            request, transport_slots_current_elements, transport_slots_old_elements
        )
        cls.transport_slots_update(
            request,
            transport_id,
            transport_slots_current_elements,
        )

    @classmethod
    def transport_slots_update(cls, request, transport_id, transport_slots_current):
        # operations.CommonOperations.request_params_are_multidict(request)
        if transport_slots_current:
            for element in transport_slots_current:
                transport_slot_id = element.transport_slot_id
                element.applicability = request.POST.get(
                    f"slot_{transport_slot_id}_applicability"
                )
                element.special_method = (
                    request.POST.get(f"slot_{transport_slot_id}_special_method")
                    == "True"
                )
                element.description = request.POST.get(
                    f"slot_{transport_slot_id}_description"
                )
                element.excerpt = request.POST.get(f"slot_{transport_slot_id}_excerpt")
                element.name = request.POST.get(f"slot_{transport_slot_id}_name")
                element.order = request.POST.get(f"size_slot_order_{transport_slot_id}")
                element.status = (
                    request.POST.get(f"slot_{transport_slot_id}_status") == "True"
                )
                element.transport_id = request.POST.get("transport_id")
                element.module_settings = cls.transport_slot_dispatcher(
                    request, transport_slot_id
                )

    @classmethod
    def transport_slots_delete(
        cls, request, transport_slots_current, transport_slots_old
    ):
        """
        Remove unused elements
        """
        removed_slots = helpers.CommonHelpers.generate_delete_list(
            [x.transport_slot_id for x in transport_slots_old],
            [x.transport_slot_id for x in transport_slots_current],
        )
        to_delete = [
            item
            for item in transport_slots_old
            if item.transport_slot_id in removed_slots
        ]
        operations.CommonOperations.delete_from_database(request, to_delete)

    @classmethod
    def transport_settings_dispatcher(cls, request):
        transport_dispatch = {
            "courier": cls.courier_settings_updater,
            "digital": cls.digital_settings_updater,
            "free_delivery": cls.free_delivery_settings_updater,
            "inpost": cls.inpost_settings_updater,
            "pickup": cls.pickup_settings_updater,
            "post": cls.post_settings_updater,
        }
        return transport_dispatch[request.POST.get("module_name")](request)

    @classmethod
    def transport_slot_dispatcher(cls, request, transport_slot_id):
        transport_slot_dispatch = {
            "courier": cls.courier_slots_settings_updater,
            "digital": cls.digital_slots_settings_updater,
            "inpost": cls.inpost_slots_settings_updater,
            "free_delivery": cls.free_delivery_slots_settings_updater,
            "pickup": cls.pickup_slots_settings_updater,
            "post": cls.post_slots_settings_updater,
        }

        return transport_slot_dispatch[request.POST.get("module_name")](
            request, transport_slot_id
        )

    @classmethod
    def courier_settings_updater(cls, request):
        return {}

    @classmethod
    def free_delivery_settings_updater(cls, request):
        return {}

    @classmethod
    def inpost_settings_updater(cls, request):
        return {}

    @classmethod
    def post_settings_updater(cls, request):
        return {}

    @classmethod
    def digital_settings_updater(cls, request):
        return {}

    @classmethod
    def pickup_settings_updater(cls, request):
        return {}

    @classmethod
    def digital_slots_settings_updater(cls, request, transport_slot_id):
        return {}

    @classmethod
    def courier_slots_settings_updater(cls, request, transport_slot_id):
        return (
            {
                "manipulation_fee": request.POST.get(
                    f"slot_{transport_slot_id}_manipulation_fee"
                ),
                "unit_fee": request.POST.get(f"slot_{transport_slot_id}_unit_fee"),
                "unit_weight": request.POST.get(
                    f"slot_{transport_slot_id}_unit_weight"
                ),
                "removed_transports": [
                    int(e)
                    for e in request.POST.getall(
                        f"slot_{transport_slot_id}_removed_transports"
                    )
                ],
                "removed_slots": [
                    int(e)
                    for e in request.POST.getall(
                        f"slot_{transport_slot_id}_removed_slots"
                    )
                ],
            }
            if transport_slot_id
            else {}
        )

    @classmethod
    def inpost_slots_settings_updater(cls, request, transport_slot_id):
        return (
            {
                "manipulation_fee": request.POST.get(
                    f"slot_{transport_slot_id}_manipulation_fee"
                ),
                "unit_fee": request.POST.get(f"slot_{transport_slot_id}_unit_fee"),
                "unit_weight": request.POST.get(
                    f"slot_{transport_slot_id}_unit_weight"
                ),
                "removed_transports": [
                    int(e)
                    for e in request.POST.getall(
                        f"slot_{transport_slot_id}_removed_transports"
                    )
                ],
                "removed_slots": [
                    int(e)
                    for e in request.POST.getall(
                        f"slot_{transport_slot_id}_removed_slots"
                    )
                ],
            }
            if transport_slot_id
            else {}
        )

    @classmethod
    def free_delivery_slots_settings_updater(cls, request, transport_slot_id):
        return (
            {
                "min_cart_value": request.POST.get(
                    f"slot_{transport_slot_id}_min_cart_value"
                ),
                "removed_transports": [
                    int(e)
                    for e in request.POST.getall(
                        f"slot_{transport_slot_id}_removed_transports"
                    )
                ],
                "removed_slots": [
                    int(e)
                    for e in request.POST.getall(
                        f"slot_{transport_slot_id}_removed_slots"
                    )
                ],
            }
            if transport_slot_id
            else {}
        )

    @classmethod
    def pickup_slots_settings_updater(cls, request, transport_slot_id):
        return (
            {
                "manipulation_fee": request.POST.get(
                    f"slot_{transport_slot_id}_manipulation_fee"
                ),
            }
            if transport_slot_id
            else {}
        )

    @classmethod
    def post_slots_settings_updater(cls, request, transport_slot_id):
        return (
            {
                "manipulation_fee": request.POST.get(
                    f"slot_{transport_slot_id}_manipulation_fee"
                ),
                "unit_fee": request.POST.get(f"slot_{transport_slot_id}_unit_fee"),
                "unit_weight": request.POST.get(
                    f"slot_{transport_slot_id}_unit_weight"
                ),
                "removed_transports": [
                    int(e)
                    for e in request.POST.getall(
                        f"slot_{transport_slot_id}_removed_transports"
                    )
                ],
                "removed_slots": [
                    int(e)
                    for e in request.POST.getall(
                        f"slot_{transport_slot_id}_removed_slots"
                    )
                ],
            }
            if transport_slot_id
            else {}
        )

    @classmethod
    def generate_products_special_transport_list(cls, request, product_id):
        return helpers.TransportHelpers.generate_transport_container(
            services.TransportService.transport_data_with_slots_special(
                request, product_id
            )
        )

    @classmethod
    def merge_common_and_special_transport_rules(cls, basic_slot, special_slot):
        """
        Merge local and common settings for transport rules check.
        Product_id is used to allow checking per product rules.
        """
        return (basic_slot.slot_settings | special_slot.local_settings) | {
            "product_id": special_slot.product_id
        }

    @classmethod
    def return_used_special_methods(cls, transport_dict, transport_filtered_dict):
        used_special_methods = []
        for item in transport_filtered_dict.values():
            used_special_methods.extend(iter(item))
        for k, v in transport_dict.items():
            v.slots = {
                transport_slot_id: slot_value
                for transport_slot_id, slot_value in v.slots.items()
                if slot_value.slot_special_method == False
                or transport_slot_id in used_special_methods
            }
        return transport_dict

    @classmethod
    def remove_transport_methods_without_slots(cls, transport_dict):
        return {k: v for k, v in transport_dict.items() if v.slots}

    @classmethod
    def remove_all_special_methods(cls, transport_dict):
        for k, v in transport_dict.items():
            v.slots = {
                transport_slot_id: slot_value
                for transport_slot_id, slot_value in v.slots.items()
                if slot_value.slot_special_method == False
            }
        return cls.remove_transport_methods_without_slots(transport_dict)

    @classmethod
    def remove_unused_physical_digital_methods(cls, cart_contents, transport_dict):
        """
        Physical objects are 0, digital 1, mixed are 2.
        """
        method_used = {item.product_type for item in cart_contents.values()}
        if 2 in method_used:
            return transport_dict
        return {
            k: v for k, v in transport_dict.items() if v.transport_method in method_used
        }

    @classmethod
    def remove_transport_methods_unrelated_to_special_transports_rules(
        cls,
        # transport_filtered_dict,
        transport_dict,
        cart_contents,
        simplified_cart_parameters,
    ):
        transport_dict = cls.remove_transport_methods_without_slots(transport_dict)
        transport_dict = cls.remove_methods_limited_by_delivery_address(
            # transport_filtered_dict,
            transport_dict,
            cart_contents,
            simplified_cart_parameters,
        )
        transport_dict = cls.remove_unused_physical_digital_methods(
            cart_contents, transport_dict
        )
        return transport_dict

    @classmethod
    def remove_transport_methods_according_to_special_transports_rules(
        cls,
        cart_contents,
        cart_parameters,
        transport_dict,
        transport_filtered_dict,
    ):
        transport_dict = (
            cls.return_used_special_methods(transport_dict, transport_filtered_dict)
            if transport_filtered_dict
            else cls.remove_all_special_methods(transport_dict)
        )
        transport_dict = cls.check_validity_of_possible_transport_methods(
            cart_contents,
            cart_parameters,
            transport_dict,
            transport_filtered_dict,
        )
        return transport_dict

    @classmethod
    def find_cheapest_physical_transport_method(
        cls,
        cart_parameters,
        transport_unit_name,
        single_slot_settings,  # current value
        # single_slot_data,  # previous value
    ):
        if (
            cart_parameters["selected_slot"]["physical"].get("transport_fee") is None
            or cart_parameters["selected_slot"]["physical"]["transport_fee"]
            > single_slot_settings["transport_fee"]
        ):
            cart_parameters["selected_slot"]["physical"] = single_slot_settings
        return cart_parameters

    @classmethod
    def calculate_transport_unit(cls, cart_weight, slot_unit_weight):
        return (
            int(f"{math.ceil(cart_weight / int(slot_unit_weight)):.0f}")
            if slot_unit_weight
            else 1
        )

    @classmethod
    def calculate_transport_fee(cls, transport_method, transport_unit):
        return Decimal(
            transport_method.get("transport_slot_manipulation_fee", 0)
        ) + transport_unit * Decimal(transport_method.get("transport_slot_unit_fee", 0))

    @classmethod
    def calculate_cost_of_transport_method(
        cls,
        cart_weight,
        transport_data,
    ):
        transport_unit = cls.calculate_transport_unit(
            cart_weight, transport_data.get("transport_slot_unit_weight", None)
        )
        transport_data[
            "transport_fee"
        ] = helpers.CommonHelpers.decimal_jsonb_compatible(
            cls.calculate_transport_fee(transport_data, transport_unit)
        )

    @classmethod
    def return_applicable_transport_methods(
        cls,
        cart_contents,
        cart_parameters,
        transport_dict,
        transport_filtered_dict,
    ):
        transport_dict = cls.remove_transport_methods_unrelated_to_special_transports_rules(
            # transport_filtered_dict,
            transport_dict,
            cart_contents,
            cart_parameters,
        )
        # Every product can have one or more special delivery rules
        return cls.remove_transport_methods_according_to_special_transports_rules(
            cart_contents,
            cart_parameters,
            transport_dict,
            transport_filtered_dict,
        )

    @classmethod
    def calculate_cheapest_transport_method(cls, cart_parameters, transport_dict):
        for item in transport_dict.values():
            for single_slot in item.slots.values():
                (
                    single_slot,
                    single_slot_data,
                ) = cls.calculate_single_transport_method_cost(
                    cart_parameters["weight"], item, single_slot
                )
                # Return cheapest transport method for physical objects
                if item.transport_method == 0:
                    cls.find_cheapest_physical_transport_method(
                        cart_parameters,
                        item.unit_name,
                        # single_slot.slot_settings,
                        single_slot_data,
                    )
                else:
                    cart_parameters["selected_slot"]["digital"] = single_slot_data
        return cart_parameters

    @classmethod
    def calculate_transport_method_fees(cls, cart_weight, transport_dict):
        # Very similar to calculate_cheapest_transport_method
        for item in transport_dict.values():
            for single_slot in item.slots.values():
                (
                    single_slot,
                    single_slot_data,
                ) = cls.calculate_single_transport_method_cost(
                    cart_weight, item, single_slot
                )
        return transport_dict

    @classmethod
    def calculate_single_transport_method_cost(cls, cart_weight, item, single_slot):
        single_slot_data = {
            "transport_id": item.transport_id,
            "transport_slot_id": single_slot.transport_slot_id,
            "transport_name": item.name,
            "transport_slot_name": single_slot.slot_name,
            "transport_slot_excerpt": single_slot.slot_excerpt,
        }
        # Other methods use this naming convention
        if single_slot.slot_settings.get("manipulation_fee"):
            single_slot.slot_settings[
                "transport_slot_manipulation_fee"
            ] = single_slot.slot_settings["manipulation_fee"]
        if single_slot.slot_settings.get("unit_fee"):
            single_slot.slot_settings[
                "transport_slot_unit_fee"
            ] = single_slot.slot_settings["unit_fee"]
        #######################
        transport_unit = cls.calculate_transport_unit(
            cart_weight,
            single_slot.slot_settings.get("unit_weight", None),
        )
        transport_fee = cls.calculate_transport_fee(
            single_slot.slot_settings, transport_unit
        )
        single_slot_data["transport_fee"] = transport_fee
        single_slot_data["transport_unit"] = transport_unit
        single_slot.slot_settings["calculate_transport_fee"] = transport_fee
        # Save calculated value for further manipulation
        single_slot.slot_settings["transport_fee"] = transport_fee
        single_slot.slot_settings["transport_unit"] = transport_unit
        return single_slot, single_slot_data

    @classmethod
    def single_product_transport_validity_check(
        cls,
        cart_contents,
        product_special_transport,
        cart_parameters,
        special_slot,
        transport_dict,
        removed_transports,
        removed_slots,
    ):
        transport_slot_id = special_slot.transport_slot_id
        transport_id = special_slot.transport_id
        # Get transport method
        current_transport = transport_dict[transport_id]
        current_slot = current_transport.slots[transport_slot_id]
        # Get basic slot info and custom product settings
        current_slot.slot_settings = cls.merge_common_and_special_transport_rules(
            current_slot,
            special_slot,
        )
        cls.single_method_validity_check(
            cart_contents,
            current_slot,
            cart_parameters,
            transport_dict,
            removed_transports,
            removed_slots,
        )
        return removed_slots, removed_transports

    @classmethod
    def check_transport_rules_per_product(
        cls,
        cart_contents,
        cart_parameters,
        transport_dict,
        transport_filtered_dict,
        removed_slots,
        removed_transports,
    ):
        for product_special_transport in transport_filtered_dict.values():
            for special_slot in product_special_transport.values():
                (
                    removed_slots,
                    removed_transports,
                ) = cls.single_product_transport_validity_check(
                    cart_contents,
                    product_special_transport,
                    cart_parameters,
                    special_slot,
                    transport_dict,
                    removed_transports,
                    removed_slots,
                )
        return cls.remove_transport_methods_without_slots(transport_dict)

    @classmethod
    def check_transport_rules_per_cart(
        cls,
        cart_parameters,
        transport_dict,
        transport_filtered_dict,
        removed_slots,
        removed_transports,
    ):
        """
        Some special transport methods require all products use the same method.
        """
        for transport_id, current_transport in transport_dict.items():
            for slot_id, current_slot in current_transport.slots.items():
                if (
                    transport_id not in removed_transports
                    and slot_id not in removed_slots
                ):
                    # Some mehtods require all products to be allowed special transport
                    if (
                        current_slot.slot_special_method
                        and current_slot.slot_applicability == 2
                        and current_slot.number_of_products_using_slot
                        < len(cart_parameters["product_values"])
                    ):
                        removed_slots.append(slot_id)
                    # If all products pass own reqirements for the same method - apply slot settings on all available methods
                    elif (
                        # current_slot.slot_special_method == True
                        # and
                        current_slot.slot_applicability
                        == 2
                    ):
                        cls.remove_slots_and_methods_disabled_by_current_method_additionally_calculate_transport_fee(
                            cart_parameters,
                            transport_dict,
                            current_slot,
                            removed_transports,
                            removed_slots,
                        )
        return cls.remove_unused_transport_methods(
            transport_dict, removed_transports, removed_slots
        )

    @classmethod
    def remove_methods_limited_by_delivery_address(
        cls,
        # transport_filtered_dict,
        transport_dict,
        cart_contents,
        simplified_cart_parameters,
    ):
        return transport_dict

    @classmethod
    def check_validity_of_possible_transport_methods(
        cls,
        cart_contents,
        cart_parameters,
        transport_dict,
        transport_filtered_dict,
    ):
        """
        Check if special method sustains minimum requirements for a single product. Next check if method can be applied for the cart (as a whole).
        """
        removed_slots, removed_transports = [], []
        transport_dict = cls.check_transport_rules_per_product(
            cart_contents,
            cart_parameters,
            transport_dict,
            transport_filtered_dict,
            removed_slots,
            removed_transports,
        )
        return cls.check_transport_rules_per_cart(
            cart_parameters,
            transport_dict,
            transport_filtered_dict,
            removed_slots,
            removed_transports,
        )

    @classmethod
    def update_number_of_product_using_slots(cls, transport_dict, current_slot):
        transport_dict[current_slot.transport_id].slots[
            current_slot.transport_slot_id
        ].number_of_products_using_slot += 1

    @classmethod
    def remove_slots_and_methods_disabled_by_current_method_additionally_calculate_transport_fee(
        cls,
        cart_parameters,
        transport_dict,
        current_slot,
        removed_transports,
        removed_slots,
    ):
        """
        Applicability 1 means that if single product in cart is present,
        other transport methods are updated
        """
        if (
            transport_dict
            and transport_dict.get(current_slot.transport_id)
            and transport_dict[current_slot.transport_id].slots
            and transport_dict[current_slot.transport_id].slots.get(
                current_slot.transport_slot_id
            )
            and transport_dict[current_slot.transport_id]
            .slots[current_slot.transport_slot_id]
            .slot_applicability
            in [1, 2]
        ):
            transport_unit = cls.calculate_transport_unit(
                cart_parameters["weight"],
                current_slot.slot_settings.get("unit_weight", None),
            )
            current_slot.slot_settings["transport_unit"] = transport_unit
            current_slot.slot_settings["transport_fee"] = cls.calculate_transport_fee(
                current_slot.slot_settings, transport_unit
            )
            transport_dict = (
                cls.remove_transports_and_slots_using_current_method_settings(
                    current_slot, transport_dict, removed_transports, removed_slots
                )
            )

    @classmethod
    def failed_transport_requirements_dispatch(
        cls, cart_contents, slot_settings, simplified_cart_parameters
    ):
        transport_tests = {}
        summary = Decimal(simplified_cart_parameters["summary"])
        product_values = simplified_cart_parameters["product_values"]
        product_id = slot_settings.get("product_id")
        if min_cart_value := slot_settings.get("min_cart_value"):
            transport_tests["min_cart_value"] = summary < Decimal(min_cart_value)
        if max_cart_value := slot_settings.get("max_cart_value"):
            transport_tests["max_cart_value"] = summary > Decimal(max_cart_value)
        if min_product_value := slot_settings.get("min_product_value"):
            transport_tests["min_product_value"] = product_values[product_id] < Decimal(
                min_product_value
            )
        if max_product_value := slot_settings.get("max_product_value"):
            transport_tests["max_product_value"] = product_values[product_id] > Decimal(
                max_product_value
            )
        if min_units := slot_settings.get("min_units"):
            if product_id in cart_contents:
                transport_tests["min_units"] = cart_contents[
                    product_id
                ].cart_quantity < Decimal(min_units)
        if max_units := slot_settings.get("max_units"):
            if product_id in cart_contents:
                transport_tests["max_units"] = cart_contents[
                    product_id
                ].cart_quantity > Decimal(max_units)
        return transport_tests

    @classmethod
    def single_method_validity_check(
        cls,
        cart_contents,
        current_slot,
        cart_parameters,
        transport_dict,
        removed_transports,
        removed_slots,
    ):
        failed_requirements = cls.failed_transport_requirements_dispatch(
            cart_contents, current_slot.slot_settings, cart_parameters
        )
        for k, v in current_slot.slot_settings.items():
            if failed_requirements.get(k):
                cls.check_current_requirement(
                    failed_requirements[k],
                    transport_dict,
                    current_slot,
                    removed_transports,
                    removed_slots,
                )
        if current_slot.transport_slot_id not in removed_slots:
            if current_slot.slot_applicability == 2:
                cls.update_number_of_product_using_slots(transport_dict, current_slot)
            else:
                cls.remove_slots_and_methods_disabled_by_current_method_additionally_calculate_transport_fee(
                    cart_parameters,
                    transport_dict,
                    current_slot,
                    removed_transports,
                    removed_slots,
                )

    @classmethod
    def check_current_requirement(
        cls,
        failed_requirement,
        transport_dict,
        current_slot,
        removed_transports,
        removed_slots,
    ):
        """
        Remove current transport slot if it fails any condition
        """
        if failed_requirement:
            removed_slots.append(current_slot.transport_slot_id)

    @classmethod
    def remove_transports_and_slots_using_current_method_settings(
        cls, current_slot, transport_dict, removed_transports, removed_slots
    ):
        if current_slot.slot_settings.get("removed_slots"):
            for slot in current_slot.slot_settings.get("removed_slots"):
                if slot in transport_dict[current_slot.transport_id].slots:
                    removed_slots.append(slot)
        if current_slot.slot_settings.get("removed_transports"):
            for transport in current_slot.slot_settings.get("removed_transports"):
                if transport in transport_dict:
                    removed_transports.append(transport)
        return cls.remove_unused_transport_methods(
            transport_dict, removed_transports, removed_slots
        )

    @classmethod
    def remove_unused_transport_methods(
        cls, transport_dict, removed_transports, removed_slots
    ):
        new_dict = {}
        for transport_id, current_transport in transport_dict.items():
            if transport_id not in removed_transports:
                new_dict[transport_id] = current_transport
                current_transport.slots = {
                    slot_id: current_slot
                    for slot_id, current_slot in current_transport.slots.items()
                    if slot_id not in removed_slots
                }
        cls.remove_transport_methods_without_slots(new_dict)
        return new_dict

    @classmethod
    def custom_order_transport_variables(cls, request, form, order, transport_dict):
        custom_transport_dispatch = {"inpost": cls.inpost_custom_variables}
        if (
            form.transport_id.data
            and form.transport_id.data != "None"  # WTForms passes weird variables
            and transport_dict.get(int(form.transport_id.data))
        ):
            module_name = transport_dict[int(form.transport_id.data)].module_name
            if custom_transport_dispatch.get(module_name):
                custom_transport_dispatch[module_name](request, order)

    @classmethod
    def inpost_custom_variables(cls, request, order):
        # Inpost requires storage of and additional variable
        order.delivery_json["inpost_point_selected"] = request.params.get(
            "inpost_point_selected"
        )

    @classmethod
    def check_external_transport_data_sources(
        cls, request, order, module_name, settings
    ):
        external_mapping = {
            "inpost": operations.InpostOperations.check_inpost_data_source
        }
        if external_mapping.get(module_name):
            external_mapping[module_name](request, order, settings)

    @classmethod
    def create_external_transport_data_sources(
        cls, request, order, module_name, settings
    ):
        external_mapping = {
            "inpost": operations.InpostOperations.create_inpost_data_source
        }
        if external_mapping.get(module_name):
            external_mapping[module_name](request, order, settings)

    @classmethod
    def unified_selected_transport_method_parameters(cls, transport, slot):
        return {
            "transport": transport.name,
            "transport_desc": transport.description,
            "transport_excerpt": transport.excerpt,
            "transport_fee": str(slot.slot_settings.get("calculate_transport_fee", 0)),
            "transport_method": slot.transport_method,
            "transport_module_name": transport.module_name,
            "transport_slot_id": slot.transport_slot_id,
            "transport_slot_excerpt": slot.slot_excerpt,
            "transport_slot_manipulation_fee": str(
                slot.slot_settings.get("manipulation_fee", 0)
            ),
            "transport_slot_name": slot.slot_name,
            "transport_slot_unit_fee": str(slot.slot_settings.get("unit_fee", 0)),
            "transport_slot_unit_name": slot.slot_unit_name,
            "transport_slot_unit_weight": slot.slot_settings.get("unit_weight", 0),
        }
