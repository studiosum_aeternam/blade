from emporium import services
from emporium import data_containers


class CollectionOperations(object):
    @classmethod
    def delete_all_collections_dispatch_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        services.CollectionService.delete_collection_description_filter_manufacturer_id(
            request, manufacturer_id
        )
        services.CollectionService.delete_collection_filter_manufacturer_id(
            request, manufacturer_id
        )

    @classmethod
    def get_collection_extended_data(cls, request, filter_params):
        return [
            data_containers.CollectionContainer(
                collection_id=item.collection_id,
                manufacturer=item.manufacturer,
                name=item.name,
                url="",
            )
            for item in services.CollectionService.collection_id_name_manufacturer_id_filter_searchstring_term(
                request, filter_params.searchstring, filter_params.term
            )
        ]
