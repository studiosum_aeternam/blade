from emporium import services
from emporium import data_containers


class ManufacturerOperations(object):
    @classmethod
    def get_manufacturer_extended_data(cls, request, filter_params):
        return [
            data_containers.ManufacturerContainer(
                manufacturer_id=item.manufacturer_id,
                name=item.name,
                url="",
                seo_slug=item.seo_slug,
                status=item.status,
                manufacturer_type=item.manufacturer_type,
            )
            for item in (
                services.ManufacturerService.manufacturer_id_name_filter_searchstring_term(
                    request, filter_params.searchstring, filter_params.term
                )
            )
        ]
