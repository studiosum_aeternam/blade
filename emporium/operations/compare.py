"""
Compare operations
"""

from datetime import datetime

# from ..models import Compare
from emporium import services


class CompareOperations(object):
    @classmethod
    def compare_merge(cls, request):
        """
        Method for merging compare contents for non-logged and logged invoked during auth-in. As a side effet it removes non-logged object from the db
        """
        if not (session := request.session):
            return
        cid_content, uid_content = None, None
        if session.get("compare_id"):
            cid_content = services.CompareService.compare_filter_compare_id(
                request, session["compare_id"]
            )
        if session.get("uid"):
            uid_content = services.CompareService.compare_filter_customer_id(
                request, session["uid"]
            )
        if cid_content:
            if uid_content:
                uid_content.products = set(
                    list(cid_content.products) + list(uid_content.products)
                )
                request.dbsession.delete(cid_content)
                if session.get("compare_id"):
                    del session["compare_id"]
            else:
                cid_content.customer_id = session.get("uid")
                uid_content = cid_content
            uid_content.date_accessed = datetime.now()
        if uid_content and uid_content.products:
            request.session["compare_contents"] = uid_content.products
