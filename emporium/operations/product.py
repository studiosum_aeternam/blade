from slugify import slugify
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from time import perf_counter
import math
import re
import uuid

from pyramid.httpexceptions import HTTPNotFound

from emporium import data_containers, helpers, models, operations, services


class ProductOperations(object):
    @classmethod
    def format_date_for_database(cls, input_date):
        return date(*map(int, input_date.split("-")))

    @classmethod
    def product_to_attribute_update(
        cls, request, action, product_id, product_attributes, attributes_passed
    ):
        """
        Product attributes edit
        """
        attributes_passed_ids = [item.attribute_id for item in product_attributes]
        if attributes_passed:
            cls.add_attributes(
                request, action, product_id, attributes_passed, attributes_passed_ids
            )
            cls.delete_attributes(
                request,
                product_id,
                product_attributes,
                attributes_passed,
                attributes_passed_ids,
            )
        elif product_attributes:  # Can be passed as an original set to be cloned
            cls.delete_unused_attributes(request, product_id, product_attributes)

    @classmethod
    def delete_unused_attributes(cls, request, product_id, product_attributes):
        for item in product_attributes:
            if item.product_id == product_id:  # Original product protection
                request.dbsession.delete(item)

    @classmethod
    def delete_attributes(
        cls,
        request,
        product_id,
        product_attributes,
        attributes_passed,
        attributes_passed_ids,
    ):
        if to_delete := set(attributes_passed_ids).difference(attributes_passed):
            for item in product_attributes:
                if (
                    item.product_id == product_id and item.attribute_id in to_delete
                ):  # Protect original product during cloning
                    request.dbsession.delete(item)

    @classmethod
    def add_attributes(
        cls, request, action, product_id, attributes_passed, attributes_passed_ids
    ):
        to_add = (
            set(attributes_passed).difference(attributes_passed_ids)
            if action == "edit"
            else attributes_passed
        )
        request.dbsession.add_all(
            [
                models.ProductToAttribute(product_id=product_id, attribute_id=_)
                for _ in to_add
            ]
        )
        request.dbsession.flush()

    @classmethod
    def product_to_band_update(cls, request, product_id, product_band):
        product_band.product_id = product_id
        product_band.band_id = request.params.get("band_id")
        product_band.band_type = 1  # Secondary band - e.g. band
        if not product_band.product_band_id:
            request.dbsession.add(product_band)

    @classmethod
    def product_description_update_all_languages(
        cls, request, action, form, product_id, product_description_list, shop_languages
    ):
        new_descriptions = []
        for lang, lang_settings in shop_languages.items():
            # Return new object model for updated description or previous data for update
            product_description = cls.product_description(
                request, lang, product_description_list
            )
            # Update data for current language
            product_description = cls.product_description_update(
                request,
                lang,
                lang_settings["language_id"],
                product_id,
                product_description,
            )
            if not product_description.product_description_id:
                # Protect cloned data
                new_descriptions.append(product_description)
        if new_descriptions:
            request.dbsession.add_all(new_descriptions)

    @classmethod
    def product_description(cls, request, lang_code, product_description_list):
        if product_description_list:
            for item in product_description_list:
                if str(item.language_id) == request.params.get(
                    "product_description_language_" + lang_code
                ):
                    return item
        return models.ProductDescription()

    @classmethod
    def product_description_update(
        cls, request, lang, language_id, product_id, product_description
    ):
        product_description.description = request.params.get(f"description_{lang}")
        product_description.description_technical = request.params.get(
            f"description_technical_{lang}"
        )
        product_description.excerpt = request.params.get(f"excerpt_{lang}")
        product_description.language_id = language_id
        product_description.meta_description = request.params.get(
            f"meta_description_{lang}"
        )
        product_description.name = request.params.get(f"name_{lang}")
        product_description.product_id = product_id
        product_description.tag = request.params.get(f"tag_{lang}")
        product_description.u_h1 = request.params.get(f"u_h1_{lang}")
        product_description.u_title = request.params.get(f"u_title_{lang}")
        return product_description

    @classmethod
    def product_update(cls, request, action, datetime, form, product):
        """
        Common view for updating product. Clone prepopulates form with source product information,
        but after form is validated it should be treated the same as new product. Also product_id
        must be returned - without it original product would be updated.
        Method form.populate_object() cannot be used due to bugs in updating images and product
        description.
        """
        # product.allowed_free_transport = form.allowed_free_transport.data
        # product.allowed_free_transport_minimum_amount = (
        #     form.allowed_free_transport_minimum_amount.data
        # )
        product.availability_id = (
            form.availability_id.data
            if form.transport_method.data == 0
            else request.params.get("virtual_availability_id")
        )
        product.date_available = form.date_available.data
        product.date_modified = datetime
        # product.forbidden_transport_methods = form.forbidden_transport_methods.data
        product.ean = cls.sanitize_ean(form.ean.data)
        product.height = form.height.data
        product.isbn = form.isbn.data
        product.jan = form.jan.data
        product.length = form.length.data
        product.image = form.main_image_url.data
        product.manufacturer_id = form.manufacturer_id.data
        product.manufactured = form.manufactured.data
        product.minimum = form.minimum.data
        product.model = cls.sanitize_value(form.model.data or form.name.data)
        product.mpn = form.mpn.data
        product.name = cls.sanitize_value(form.name.data)
        product.one_batch = form.one_batch.data
        product.pieces = form.pieces.data
        product.points = form.points.data

        # Prices
        product.catalog_price = form.catalog_price.data
        product.discount_id = form.discount_id.data
        product.manual_discount = form.manual_discount.data
        product.purchase_price = form.purchase_price.data
        product.tax_id = form.tax_id.data
        # Basic price is set up in the main view

        product.product_type = form.product_type.data
        product.quantity = form.quantity.data
        product.seo_slug = slugify(form.seo_slug.data or form.name.data)
        product.setting_template_id = form.setting_template_id.data
        product.sku = form.sku.data
        product.sort_order = form.sort_order.data
        product.square_meter = form.square_meter.data
        product.status = form.product_status.data
        product.subtract = form.subtract.data
        product.subtract_quantity = form.subtract_quantity.data
        product.unit = form.unit.data
        product.upc = form.upc.data
        product.transport_method = form.transport_method.data or 0
        product.width = form.width.data
        product.weight = form.weight.data
        if action in ("create", "clone"):
            # product.product_id = None # needed when form.populate_object(product) is activated
            product.date_added = product.date_modified
            product.viewed = 0
            request.dbsession.add(product)
        request.dbsession.flush()
        return product

    @classmethod
    def product_create_draft(
        cls,
        request,
        datetime,
    ):
        """
        Temporary(?) solution for creating product related data if product was just created
        """
        product = models.Product()
        draft_name = f"DRAFT_{uuid.uuid4()}"
        product.availability_id = 51
        product.date_available = datetime
        product.date_modified = datetime
        # product.forbidden_transport_methods = form.forbidden_transport_methods.data
        product.ean = 0
        product.height = 0
        product.isbn = 0
        product.jan = 0
        product.length = 0
        product.image = ""
        product.manufacturer_id = 185
        product.manufactured = 0
        product.minimum = 0
        product.model = draft_name
        product.mpn = 0
        product.name = draft_name
        product.one_batch = False
        product.pieces = 0
        product.points = 0

        # Prices
        product.price = 1
        product.catalog_price = 1
        product.discount_id = 1
        product.manual_discount = False
        product.purchase_price = 0
        product.tax_id = 1
        # Basic price is set up in the main view

        product.product_type = 1
        product.quantity = 1
        product.seo_slug = draft_name
        product.setting_template_id = 12
        product.sku = 0
        product.sort_order = 0
        product.square_meter = 0
        product.status = 0
        product.subtract = 1
        product.subtract_quantity = 1
        product.unit = 1
        product.upc = 0
        product.transport_method = 0
        product.width = 0
        product.weight = 0
        product.date_added = product.date_modified
        product.viewed = 0
        request.dbsession.add(product)
        request.dbsession.flush()
        return product

    @classmethod
    def product_to_category_update(
        cls,
        request,
        action,
        product_id,
        product_categories,
        categories_passed,
        category_tree,
    ):
        # CategoryContainer
        if categories_passed:
            updated_categories_passed = set(
                operations.CategoryOperations.recursive_parent_adding(
                    category_tree, categories_passed
                )
            )
            old_categories_ids = {x.category_id for x in product_categories}
            to_add = (
                updated_categories_passed - old_categories_ids
                if action == "edit"
                else updated_categories_passed
            )
            for category_id in to_add:
                request.dbsession.add(
                    models.ProductToCategory(
                        product_id=product_id, category_id=category_id
                    )
                )
            if product_categories:
                to_delete = old_categories_ids - updated_categories_passed
                for item in product_categories:
                    if (
                        item.product_id == product_id and item.category_id in to_delete
                    ):  # Cloned product protection
                        request.dbsession.delete(item)
        elif product_categories:  # Can be passed as an original set to be cloned
            for item in product_categories:
                if item.product_id == product_id:  # Original product protection
                    request.dbsession.delete(item)

    @classmethod
    def update_promotions(cls, request, product_id):
        # Special
        if request.params.get("special"):
            day = str(datetime.now().date() - relativedelta(days=1))
            start_day = request.params.get("special_date_start") or day
            product_to_special = models.ProductToSpecial(
                special_id=request.params.get("special"),
                product_id=product_id,
                date_start=date(*map(int, cls.format_date_for_database(start_day))),
                quantity=request.params.get("special_quantity", None),
            )
            if request.params.get("special_date_end"):
                product_to_special.date_end = date(
                    *map(
                        int,
                        cls.format_date_for_database(
                            request.params.get("special_date_end")
                        ),
                    )
                )
            else:
                product_to_special.date_end = None
            if request.params.get("special_price"):
                product_to_special.price = request.params.get("special_price")
            request.dbsession.add(product_to_special)

    @classmethod
    def product_to_transport_update(cls, request, product_id, old_transport):
        old_transport_slots = [str(x.transport_slot_id) for x in old_transport]
        new_transport_slots = [
            x for x in request.params.getall("transport_slot_id") if x != ""
        ]
        if remove_transports := set(old_transport_slots).difference(
            new_transport_slots
        ):
            operations.CommonOperations.delete_from_database(
                request,
                [
                    x
                    for x in old_transport
                    if str(x.transport_slot_id) in remove_transports
                ],
            )
        if new_transport_list := set(new_transport_slots).difference(
            old_transport_slots
        ):
            cls.create_product_transport(request, product_id, new_transport_list)
        if [
            x for x in old_transport if str(x.transport_slot_id) in new_transport_slots
        ]:
            cls.update_product_transport(
                request,
                [
                    x
                    for x in old_transport
                    if str(x.transport_slot_id) in new_transport_slots
                ],
            )

    @classmethod
    def product_transport_settings_dispatch(cls, request, slot_id):
        values = [
            "max_cart_value",
            "min_cart_value",
            "max_product_value",
            "min_product_value",
            "max_units",
            "min_units",
        ]

        return {
            item: request.params.get(f"transport_slot_{item}_{slot_id}")
            for item in values
            if request.params.get(f"transport_slot_{item}_{slot_id}")
        }

    @classmethod
    def update_product_transport(cls, request, updated_elements):
        for item in updated_elements:
            item.local_settings = cls.product_transport_settings_dispatch(
                request, item.transport_slot_id
            )

    @classmethod
    def create_product_transport(cls, request, product_id, new_elements):
        new_slots = [
            models.ProductToTransport(
                product_id=product_id,
                transport_slot_id=item,
                local_settings=cls.product_transport_settings_dispatch(request, item),
                status=1,
            )
            for item in new_elements
        ]
        request.dbsession.add_all(new_slots)

    @classmethod
    def delete_all_products_dispatch_filter_manufacturer_id(
        cls, request, manufacturer_id
    ):
        services.ProductService.delete_all_product_description_filter_manufacturer_id(
            request, manufacturer_id
        )
        services.ProductToAttributeService.delete_all_product_to_attribute_filter_manufacturer_id(
            request, manufacturer_id
        )
        services.ProductToCategoryService.delete_all_product_to_category_filter_manufacturer_id(
            request, manufacturer_id
        )
        services.ProductToCollectionService.delete_all_product_to_collection_filter_manufacturer_id(
            request, manufacturer_id
        )
        services.ProductToFileService.delete_all_product_to_file_filter_manufacturer_id(
            request, manufacturer_id
        )
        services.ProductToImageService.delete_all_product_to_image_filter_manufacturer_id(
            request, manufacturer_id
        )
        services.ProductToOptionService.delete_all_product_to_option_filter_manufacturer_id(
            request, manufacturer_id
        )
        services.ProductToProductService.delete_all_product_to_product_filter_manufacturer_id(
            request, manufacturer_id
        )
        services.ProductToSpecialService.delete_all_product_to_special_filter_manufacturer_id(
            request, manufacturer_id
        )
        services.ProductToRelationService.delete_all_product_relation_filter_manufacturer_id(
            request, manufacturer_id
        )
        # The product is last!
        services.ProductService.delete_all_products_filter_manufacturer_id(
            request, manufacturer_id
        )

    @classmethod
    def delete_all_products_dispatch_filter_band_id(cls, request, band_id):
        services.ProductService.delete_all_product_description_filter_band_id(
            request, band_id
        )
        services.ProductToAttributeService.delete_all_product_to_attribute_filter_band_id(
            request, band_id
        )
        services.ProductToCategoryService.delete_all_product_to_category_filter_band_id(
            request, band_id
        )
        services.ProductToCollectionService.delete_all_product_to_collection_filter_band_id(
            request, band_id
        )
        services.ProductToFileService.delete_all_product_to_file_filter_band_id(
            request, band_id
        )
        services.ProductToImageService.delete_all_product_to_image_filter_band_id(
            request, band_id
        )
        services.ProductToOptionService.delete_all_product_to_option_filter_band_id(
            request, band_id
        )
        services.ProductToProductService.delete_all_product_to_product_filter_band_id(
            request, band_id
        )
        services.ProductToSpecialService.delete_all_product_to_special_filter_band_id(
            request, band_id
        )
        services.ProductToRelationService.delete_all_product_relation_filter_band_id(
            request, band_id
        )
        # The product is last!
        services.ProductService.delete_all_products_filter_band_id(request, band_id)

    @classmethod
    def product_to_product_create(cls, request, product_id, to_add, relation_type):
        if to_add:
            request.dbsession.add_all(
                [
                    models.ProductToProduct(
                        product1_id=product_id,
                        product2_id=k,
                        product_relation_type=relation_type,
                        order=v.get("order"),
                        quantity=v.get("quantity"),
                    )
                    for k, v in to_add.items()
                ]
            )

    @classmethod
    def delete_related_products(cls, request, product_for_deletion, product_to_product):
        to_delete = [
            item
            for item in product_to_product
            if (item.product2_id or item.product1_id) in product_for_deletion
        ]
        operations.CommonOperations.delete_from_database(request, to_delete)

    @classmethod
    def product_storage_dispatch(
        cls, request, product_id, product_list_storage_data, settings
    ):
        # Add new storages
        old_data = [int(elem.storage_id) for elem in product_list_storage_data]
        storage_list = [
            int(item)
            for item in request.params.getall("storage_id")
            if int(item) not in old_data
        ]
        storage_summarized = cls.product_storage_create(
            request, product_id, storage_list, settings
        )
        # Update storage quantities
        storage_summarized += cls.product_storage_update(
            request, product_list_storage_data
        )
        return storage_summarized

    @classmethod
    def product_storage_create(cls, request, product_id, storage_list, settings):
        storage_summarized = 0
        if storage_list:
            for storage_id in storage_list:
                quantity = request.params.get(f"storage_id_quantity_{storage_id}")
                if (
                    not quantity
                    and settings.get("storage_settings")
                    and settings["storage_settings"].default_external_storage == 1
                    and request.params.get("ean")
                ):
                    quantity = 0
                if quantity and int(quantity) >= 0:
                    request.dbsession.add(
                        models.ProductToStorage(
                            product_id=product_id,
                            quantity=quantity,
                            storage_id=storage_id,
                        )
                    )
                    storage_summarized += int(quantity)
        return storage_summarized

    @classmethod
    def product_storage_update(cls, request, product_list_storage_data):
        storage_summarized = 0
        for item in product_list_storage_data:
            new_quantity = int(
                request.params.get(f"storage_product_id_quantity_{item.storage_id}")
            )
            if new_quantity > 0:
                item.quantity = new_quantity
                storage_summarized += new_quantity
            else:
                request.dbsession.delete(item)
        return storage_summarized

    @classmethod
    def mass_product_storage_dispatch(cls, request, product_list_storage_data):
        # Add new storages
        cls.mass_product_storage_create(request, request.params.getall("product_id"))
        # Update storage quantities
        cls.mass_product_storage_update(request, product_list_storage_data)
        # return storage_summarized

    @classmethod
    def mass_product_storage_create(cls, request, product_list):
        request.dbsession.add_all(
            [
                models.ProductToStorage(
                    product_id=product_id,
                    quantity=request.params.get(f"quantity_{product_id}"),
                    storage_id=request.params.get(f"storage_{product_id}"),
                )
                for product_id in product_list
            ]
        )

    @classmethod
    def mass_product_storage_update(cls, request, product_list_storage_data):
        for item in product_list_storage_data:
            new_quantity = int(
                request.params.get(
                    f"storage_product_id_quantity_{item.product_storage_id}"
                )
            )
            if new_quantity > 0:
                item.quantity = new_quantity
            else:
                request.dbsession.delete(item)

    # ProductToImage
    @classmethod
    def product_images_dispatch(
        cls,
        request,
        product_id,
        product_images_old_elements,
        product_image_current_elements,
    ):
        cls.product_image_update(request, product_id, product_image_current_elements)
        # Delete relations that were not updated
        to_delete = helpers.CommonHelpers.generate_delete_list(
            [x.product_image_id for x in product_images_old_elements],
            [x.product_image_id for x in product_image_current_elements],
        )
        cls.delete_product_image(request, to_delete, product_images_old_elements)

    @classmethod
    def product_image_update(cls, request, product_id, product_image_current_elements):
        for item in product_image_current_elements:
            item.sort_order = request.params.get(
                f"image_order_{str(item.product_image_id)}"
            )

            item.product_id = product_id

    @classmethod
    def delete_product_image(cls, request, image_for_deletion, product_to_image):
        to_delete = [
            item
            for item in product_to_image
            if item.product_image_id in image_for_deletion
        ]
        operations.CommonOperations.delete_from_database(request, to_delete)

    # ProductToAuthor

    @classmethod
    def product_to_author_update(
        cls, request, options, product, product_to_author_old_elements
    ):
        if request.params.getall("product_author_id") or product_to_author_old_elements:
            product_to_author_current_elements = services.ProductToAuthorService.product_to_author_list_filter_product_author_list(
                request,
                [int(x) for x in request.params.getall("product_author_id")],
            )
            cls.product_authors_dispatch(
                request,
                options,
                product.product_id,
                product_to_author_old_elements,
                product_to_author_current_elements,
            )

    @classmethod
    def product_authors_dispatch(
        cls,
        request,
        options,
        product_id,
        product_to_author_old_elements,
        product_to_author_current_elements,
    ):
        action_dispatch = {
            "clone": cls.product_author_clone,
            "edit": cls.product_author_update,
        }
        action_dispatch[options.action](
            request, product_id, product_to_author_current_elements
        )
        to_delete = helpers.CommonHelpers.generate_delete_list(
            [x.product_author_id for x in product_to_author_old_elements],
            [x.product_author_id for x in product_to_author_current_elements],
        )
        operations.CommonOperations.delete_from_database(
            request,
            [
                item
                for item in product_to_author_old_elements
                if item.product_author_id in to_delete
            ],
        )

    @classmethod
    def product_author_update(
        cls, request, product_id, product_to_author_current_elements
    ):
        for item in product_to_author_current_elements:
            item.visibility = request.params.get(
                f"author_visibility_{str(item.product_author_id)}"
            )
            item.role = request.params.get(f"author_role_{str(item.product_author_id)}")
            item.product_id = product_id
            item.order = request.params.get(
                f"author_order_{str(item.product_author_id)}"
            )

    @classmethod
    def product_author_clone(
        cls, request, product_id, product_to_author_current_elements
    ):
        product_author_list = []
        for item in product_to_author_current_elements:
            product_author_list.append(
                models.ProductToAuthor(
                    author_id=request.params.get(
                        f"author_id_{str(item.product_author_id)}"
                    ),
                    order=request.params.get(
                        f"author_order_{str(item.product_author_id)}", 1
                    ),
                    product_id=product_id,
                    role=request.params.get(
                        f"author_role_{str(item.product_author_id)}"
                    ),
                    visibility=request.params.get(
                        f"author_visibility_{str(item.product_author_id)}"
                    ),
                )
            )
        request.dbsession.add_all(product_author_list)
        request.dbsession.flush()

    # ProductToFile
    @classmethod
    def product_files_dispatch(
        cls,
        request,
        product_files_old_elements,
        product_file_current_elements,
    ):
        cls.product_to_file_update(request, product_file_current_elements)

    @classmethod
    def product_special_update(
        cls,
        request,
        product_id,
        product_to_special_current_elements,
        price_input_setting,
        tax_value,
        storage_summary,
    ):
        for item in product_to_special_current_elements:
            item.storage_aware = True
            item.date_start = (
                cls.format_date_for_database(
                    request.params.get(
                        f"special_date_start_{str(item.product_special_id)}"
                    )
                )
                if request.params.get(
                    f"special_date_start_{str(item.product_special_id)}"
                )
                else cls.format_date_for_database(
                    str(datetime.now().date() - relativedelta(days=1))
                )
            )
            item.product_id = product_id
            # If amount is not specified pass all stored quantity
            item.quantity = (
                request.params.get(f"special_quantity_{str(item.product_special_id)}")
                or storage_summary
            )

            if request.params.get(f"special_price_{str(item.product_special_id)}"):
                price = request.params.get(
                    f"special_price_{str(item.product_special_id)}"
                ).replace(",", ".")
                item.price = cls.set_price_based_on_system_settings(
                    price_input_setting, tax_value, price
                )

            if request.params.get(f"special_date_end_{str(item.product_special_id)}"):
                item.date_end = cls.format_date_for_database(
                    request.params.get(
                        f"special_date_end_{str(item.product_special_id)}"
                    )
                )
            if request.params.get(f"special_order_{str(item.product_special_id)}"):
                item.order = request.params.get(
                    f"special_order_{str(item.product_special_id)}"
                )

    @classmethod
    def product_featured_update(
        cls,
        request,
        product_id,
        product_featured_elements,
    ):
        for item in product_featured_elements:
            item.product_id = product_id
            item.storage_aware = False
            item.date_start = (
                cls.format_date_for_database(
                    request.params.get(
                        f"featured_date_start_{str(item.product_special_id)}"
                    )
                )
                if request.params.get(
                    f"featured_date_start_{str(item.product_special_id)}"
                )
                else cls.format_date_for_database(
                    str(datetime.now().date() - relativedelta(days=1))
                )
            )
            if request.params.get(f"featured_date_end_{str(item.product_special_id)}"):
                item.date_end = cls.format_date_for_database(
                    request.params.get(
                        f"featured_date_end_{str(item.product_special_id)}"
                    )
                )

    @classmethod
    def file_and_product_to_file_update(
        cls, request, current_product_file_id_list, product_files_old_elements
    ):
        product_file_current_elements = (
            services.ProductToFileService.product_to_file_list_filter_file_list(
                request, current_product_file_id_list
            )
        )
        operations.ProductToFileOperations.product_to_file_dispatch(
            request, product_files_old_elements, current_product_file_id_list
        )
        operations.FileOperations.file_dispatch(
            request,
            product_files_old_elements,
            product_file_current_elements,
        )

    @classmethod
    def product_component_update(
        cls, request, product_id, product_to_product_current_elements, relation_type
    ):
        for item in product_to_product_current_elements:
            item.product1_id = product_id
            item.product_relation_type = relation_type
            item.order = request.params.get(
                f"component_order_{str(item.product_component_id)}"
            )
            item.quantity = request.params.get(
                f"component_quantity_{str(item.product_component_id)}"
            )  # To be removed
            item.relation_settings = {
                "quantity": item.quantity,
                "product_parent_id": product_id,
            }

    @classmethod
    def product_option_update(
        cls, request, product_id, product_to_product_current_elements, relation_type
    ):
        for item in product_to_product_current_elements:
            item.order = request.params.get(
                f"component_order_{str(item.product_component_id)}"
            )
            item.relation_settings = {
                "diff_attributes": request.params.getall(
                    f"product_diff_attribute_{str(item.product_component_id)}"
                )
            }

    @classmethod
    def product_related_update(
        cls,
        request,
        product_id,
        product_to_product_old_elements,
        product_to_product_current_elements,
    ):
        if new_items := [
            models.ProductToProduct(
                product1_id=product_id,
                product2_id=item,
                product_relation_type=2,
                relation_settings={},
            )
            for item in product_to_product_current_elements
            if item
            and item
            not in [item.product2_id for item in product_to_product_old_elements]
        ]:
            request.dbsession.add_all(new_items)

    @classmethod
    def product_to_collection_create(
        cls, request, action, col_ids, product_id, related_collections
    ):
        """
        Add collections related to product
        """
        to_add = related_collections
        if action == "edit" and col_ids:
            to_add = set(related_collections).difference(col_ids)
        if to_add:
            for _ in to_add:
                request.dbsession.add(
                    models.ProductToCollection(collection_id=_, product_id=product_id)
                )

    @classmethod
    def product_to_collection_delete(
        cls, request, col_ids, product_to_collection, related_collections
    ):
        """"""
        if to_delete := set(col_ids).difference(related_collections):
            for item in product_to_collection:
                if item.collection_id in (to_delete):
                    request.dbsession.delete(item)

    # @classmethod
    # def product_to_related_element_create(
    #     cls, request, action, parent_id, db_model, *params
    # ):
    #     """
    #     Add element related to the product
    #     """
    #     elements_new = params[1]
    #     if action == "edit" and elements_new:
    #         elements_new = set(elements_new).difference(params[0])
    #     if elements_new:
    #         list_attributes = [attr for attr, value in db_model.__dict__.items()][1:]
    #         for _ in elements_new:
    #             db_model.list_attributes[0] = parent_id
    #             db_model.list_attributes[1] = _
    #             # for item in list_attributes[2:]:
    #                 # db_model.item
    #             # db_model[0] = parent_id, db_model._, *params[2:]))
    #             request.dbsession.add(db_model)

    @classmethod
    def calculate_current_price(cls, discounts, item):
        """
        Calculate new price based on current discount value
        """
        helpers.CommonHelpers.set_price_precision()
        item.price = Decimal(item.catalog_price)
        discount_id = int(item.discount_id)
        if discounts.get(discount_id):
            selected_discount = (
                discounts[discount_id].value / 100
                if discounts[discount_id].value
                else 0
            )
            item.price = item.price + (
                Decimal(item.catalog_price) * Decimal(selected_discount)
            )
        return helpers.CommonHelpers.quantize_number(item.price)

    @classmethod
    def calculate_base_unit_price(cls, product, tax):
        """
        Calculate unit price based on minimum quantity and current price
        """
        unit = 1 if product.square_meter == 0 else product.unit
        helpers.CommonHelpers.set_price_precision()
        base_unit = helpers.CommonHelpers.quantize_number(
            Decimal(product.minimum)
            * Decimal(unit)
            * Decimal(product.price)
            * Decimal(1 + (Decimal(tax.value) / 100))
        )
        if base_unit:
            base_unit = base_unit[:-2]
        return base_unit

    @classmethod
    def calculate_unit_price(cls, product, tax):
        """
        Calculate unit price based on minimum quantity and current price
        """
        unit = 1 if product.square_meter == 0 else product.unit
        helpers.CommonHelpers.set_price_precision()
        return helpers.CommonHelpers.price_precision(
            Decimal(product.minimum)
            * Decimal(unit)
            * Decimal(cls.real_price(product))
            * Decimal(1 + (Decimal(tax.value) / 100))
        )

    @classmethod
    def real_price(cls, product):
        """
        Return current price
        """
        return product.special_price or product.price

    @classmethod
    def update_discount_value(cls, discount_id, item):
        """
        Update discount value
        """
        if discount_id:
            item.discount_id = discount_id
        return item.discount_id

    @classmethod
    def update_catalog_price(cls, catalog_price, item):
        """
        Update catalog price
        """
        if catalog_price:
            item.catalog_price = catalog_price.replace(",", ".")

    @classmethod
    def sanitize_value(cls, value):
        """
        Strip passed value (used by: EAN, name, model)
        """
        if value:
            cls.strip_value(value)
            cls.replace_non_unicode_characters(value)
        return value

    @classmethod
    def sanitize_ean(cls, value):
        """
        Strip '13:'' from EAN - products data was imported from a different engine
        """
        if value:
            value = value.replace(" ", "")
            if value.startswith("13:"):
                value = value.replace("13:", "")
            cls.sanitize_value(value)
        return value

    @classmethod
    def strip_value(cls, value):
        """
        Strip passed value (used by: EAN, name, model)
        """
        return value.strip() if value else ""

    @classmethod
    def replace_non_unicode_characters(cls, value):
        """
        Strip passed value (used by: EAN, name, model)
        """
        if value:
            REPLACEMENT_PATTERNS = {"„": '"', "”": '"'}
            for i, j in REPLACEMENT_PATTERNS.items():
                value = value.replace(i, j)
        return value

    @classmethod
    def update_customer_prices(
        cls, product_list: list, discounts: list, current_date: str
    ) -> list:
        """
        Update product prices using current discount values. This function assumes that all passed products should have updated prices.
        """
        discounts_dict = helpers.DiscountHelpers.create_discount_dict_key_id(discounts)
        for item in product_list:
            item.date_modified = current_date
            item.price = item.catalog_price
            if discounts_dict.get(item.discount_id):
                discount = (
                    discounts_dict[item.discount_id].value / 100
                    if discounts_dict[item.discount_id].value
                    else 0
                )
                item.price += item.catalog_price * Decimal(discount)
        helpers.CacheHelpers.flush_settings()
        return product_list

    @classmethod
    def update_catalog_prices(
        cls, merged_dicts: dict, product_list: list, current_date: str
    ) -> list:
        """
        Update product catalog prices using current discount values.
        """
        for item in product_list:
            item.catalog_price = Decimal(
                str(merged_dicts[item.ean]["RetailPriceNet"]["Value"])
            )
            # item.status = 1 if merged_dicts[item.ean]["InStock"] else 0 # temporarly removed it disables 30% of stock
            item.date_modified = current_date
        return product_list

    @classmethod
    def set_product_dimensions(cls, product, measurement_attributes, attributes_passed):
        """
        Set product dimensions based on fixed attribute values
        """
        for item in attributes_passed:
            if (
                measurement_attributes.get(item)
                and measurement_attributes[item].value[0].isnumeric()
            ):
                product.width = measurement_attributes[item].value.split("x")[0]
                product.length = measurement_attributes[item].value.split("x")[1]
                break

    @classmethod
    def product_components_update(
        cls,
        request,
        action,
        product_id,
        product_components,
        products_passed,
        relation_type,
    ):  # sourcery skip: use-named-expression
        """
        Product products edit
        """
        if products_passed:
            old_products_ids = {x.category_id for x in product_components}
            to_add = (
                set(products_passed).difference(old_products_ids)
                if action == "edit"
                else products_passed
            )
            for _ in to_add:
                request.dbsession.add(
                    models.ProductToProduct(
                        product1_id=product_id, product2_id=_, product_relation_type=1
                    )
                )
            if product_components:
                if to_delete := old_products_ids.difference(products_passed):
                    for item in product_components:
                        if (
                            item.product_id == product_id
                            and item.category_id in to_delete
                        ):  # Cloned product protection
                            request.dbsession.delete(item)
        elif product_components:  # Can be passed as an original set to be cloned
            for item in product_components:
                if (
                    item.product1_id or item.product2_id
                ) == product_id:  # Original product protection
                    request.dbsession.delete(item)

    @classmethod
    def get_price_based_on_system_settings(
        cls, settings_price_input, tax_value, product_catalog_price
    ):
        catalog_price = product_catalog_price
        if settings_price_input == 1:
            catalog_price = helpers.CommonHelpers.price_gross_replacement(
                product_catalog_price, tax_value
            )
        return catalog_price

    @classmethod
    def return_price_based_on_tax_system_settings(cls, tax_value, price):
        return helpers.CommonHelpers.price_gross_replacement(price, tax_value)

    @classmethod
    def product_bulk_update_price_based_on_system_settings(
        cls, data_options, item, new_price, price_input
    ):
        if price_input == 1:
            return helpers.CommonHelpers.price_net(
                new_price.replace(",", "."), data_options.tax_dict[item.tax_id].value
            )

    @classmethod
    def product_update_price_based_on_system_settings(
        cls, data_options, form, price_input
    ):
        if price_input == 1:
            form.catalog_price.data = helpers.CommonHelpers.price_net(
                form.catalog_price.data, data_options.tax_dict[form.tax_id.data].value
            )

    @classmethod
    def set_price_based_on_system_settings(cls, price_input_setting, tax_value, price):
        if price_input_setting == 1:
            price = helpers.CommonHelpers.price_net(price, tax_value)
        return price

    @classmethod
    def product_list_filtered(cls, request, filter_params, settings, tax_dict):
        product_aggregator = services.ProductService.product_aggregator_v2(
            request,
            filter_params,
            manual_discount=filter_params.manual_discount,
        )
        return services.ProductService.products_paginator_filter_dynamic_v2(
            request,
            filter_params,
            settings,
            tax_dict,
            item_count=product_aggregator.item_count,
        )

    @classmethod
    def current_data_options(
        cls,
        request,
        filter_params,
        query_items,
        product_availabilities,
        tax_dict,
    ):
        return data_containers.DataOptions(
            availability_dict=cls.generate_availability_list(request),
            attribute_dict=cls.generate_attribute_list(
                request, filter_params, query_items, product_availabilities
            ),
            band_dict=cls.generate_band_list(request, filter_params, query_items),
            categories_dict=cls.generate_categories_list(request, filter_params),
            collection_dict=cls.generate_collection_list(
                request, filter_params, query_items
            ),
            manufacturer_dict=cls.generate_manufacturer_list(
                request, filter_params, query_items
            ),
            special_dict=cls.generate_special_list_v2(
                request, filter_params, query_items
            ),
            storage_dict=cls.generate_storage_list(request, filter_params, query_items),
            tax_dict=tax_dict,
        )

    @classmethod
    def generate_attribute_list(
        cls, request, filter_params, query_items, product_availabilities
    ):
        attribute_list = services.AttributeService.attributes_filter_dynamic(
            request,
            filter_params,
            product_availabilities,
        )
        attribute_id_list = {x.attribute_id for x in attribute_list}
        attribute_group_list = (
            services.AttributeService.attribute_groups_filter_attribute_list(
                request,
                attribute_id_list,
            )
        )
        return helpers.AttributeHelpers.attributes_filter_format(
            attribute_group_list,
            attribute_list,
            filter_params.attributes.union(filter_params.availability),
            query_items,
        )

    @classmethod
    def generate_band_list(cls, request, filter_params, query_items):
        return helpers.BandHelpers.bands_filter_format(
            filter_params.band,
            services.BandService.bands_id_name_filter_dynamic(
                request,
                filter_params,
            ),
            query_items,
        )

    @classmethod
    def generate_categories_list(cls, request, filter_params):
        return helpers.CategoryHelpers.build_category_tree(
            helpers.CategoryHelpers.categories_filter_format(
                services.CategoryService.category_id_image_name_parent_placeholder_slug_filter_dynamic(
                    request,
                    filter_params,
                )
            )
        )

    @classmethod
    def generate_collection_list(cls, request, filter_params, query_items):
        return helpers.CollectionHelpers.collections_filter_format(
            filter_params.collection,
            services.CollectionService.collections_id_name_filter_dynamic(
                request,
                filter_params,
            ),
            query_items,
        )

    @classmethod
    def generate_manufacturer_list(cls, request, filter_params, query_items):
        return helpers.ManufacturerHelpers.manufacturers_filter_format(
            filter_params.manufacturer,
            services.ManufacturerService.manufacturers_id_name_filter_dynamic(
                request,
                filter_params,
            ),
            query_items,
        )

    @classmethod
    def generate_storage_list(cls, request, filter_params, query_items):
        """
        BUG = uinused variable
        """
        return helpers.StorageHelpers.storages_filter_format(
            filter_params.manufacturer,
            services.StorageService.storage_id_name_filter_dynamic(
                request,
                filter_params,
            ),
            query_items,
        )

    @classmethod
    def single_product_data_options(
        cls,
        request,
        availability,
        filtered_attributes,
        manufacturer,
        product_id,
        product_type,
        settings,
        query_items=[],
    ):
        attribute_list = (
            services.AttributeService.attribute_group_attribute_filter_product_id(
                request,
                availability,
                product_id,
            )
        )
        attribute_ids = [x.attribute_id for x in attribute_list]
        (
            product_categories_dict,
            product_main_category,
        ) = helpers.CategoryHelpers.categories_filter_format_with_main(
            services.CategoryService.category_id_image_name_parent_placeholder_slug_filter_product_id(
                request, product_id
            )
        )
        main_category = (
            [
                item
                for item in product_categories_dict
                if item.category_id == product_main_category
            ][0]
            if product_categories_dict
            else None
        )
        manufacturer_dict = helpers.ManufacturerHelpers.manufacturers_filter_format(
            [],
            services.ManufacturerService.manufacturers_common(request),
            [],
        )
        band_dict = cls.generate_band_list_filter_products_ids(
            request,
            [product_id],
        )
        related_unrestricted_files = services.ProductToFileService.product_file_list_filter_permission_product_id(
            request, 2, product_id
        )
        return data_containers.SingleProductDataOptions(
            additional_images_dict=(
                cls.generate_additional_images_list_filter_id(
                    request,
                    product_id,
                    manufacturer_dict.get(manufacturer).seo_slug
                    + (
                        f"/{band_dict.get(product_id).seo_slug}"
                        if band_dict.get(product_id)
                        else ""
                    )
                    if manufacturer_dict and manufacturer_dict.get(manufacturer)
                    else "",
                    settings,
                    False,
                )
            ),
            attached_document=[
                item for item in related_unrestricted_files if item.file_type_id == 1
            ],
            attached_music=[
                item for item in related_unrestricted_files if item.file_type_id == 2
            ],
            attached_video=[
                item for item in related_unrestricted_files if item.file_type_id == 3
            ],
            attribute_dict=helpers.AttributeHelpers.attributes_filter_format(
                services.AttributeService.attribute_groups_filter_attribute_list_active(
                    request, attribute_ids, settings
                ),
                attribute_list,
                [],
                [],
            ),
            availability_dict=cls.generate_availability_list_filter_id(
                request, availability
            ),
            categories_dict=helpers.CategoryHelpers.build_category_tree(
                helpers.CategoryHelpers.categories_filter_format(
                    services.CategoryService.category_id_image_name_parent_placeholder_slug_filter_active(
                        request
                    )
                )
            ),
            collection_dict=cls.generate_collection_list_filter_product_id(
                request, product_id, query_items
            ),
            header_menu_dict=services.PageService.page_with_content_filter_page_names_current_language(
                request,
                settings["header_menu"].keys() if settings.get("header_menu") else None,
            ),
            footer_menu_dict=services.PageService.page_with_content_filter_page_names_current_language(
                request,
                settings["footer_menu"].keys() if settings.get("footer_menu") else None,
            ),
            main_category_id=main_category.category_id if main_category else None,
            main_category_seo_slug=main_category.seo_slug if main_category else None,
            manufacturer_dict=manufacturer_dict,
            product_categories_dict=product_categories_dict,
            related_attribute_dict=helpers.AttributeHelpers.attributes_filter_format(
                services.AttributeService.attribute_groups_filter_attribute_list_active(
                    request,
                    [x.attribute_id for x in filtered_attributes],
                    ["availability"],
                ),
                filtered_attributes,
                [],
                [],
            ),
            special_dict=helpers.SpecialHelpers.specials_format(
                services.SpecialService.specials_values_id_name_filter_active(request)
            ),
            storage_dict=cls.generate_storage_list_filter_product_id(
                request, product_id, query_items
            ),
            tax_dict=operations.TaxOperations.generate_tax_list(request),
            transport_dict=operations.TransportOperations.generate_products_special_transport_list(
                request, product_id
            ),
        )

    @classmethod
    def generate_additional_images_list_filter_id(
        cls, request, product_id, manufacturer_seo_slug, settings, show_placeholder
    ):
        return helpers.ProductHelpers.additional_images_format(
            services.ProductToImageService.additional_images_filter_product_id(
                request,
                product_id,
            ),
            manufacturer_seo_slug,
            settings["image_settings"],
            show_placeholder,
        )

    @classmethod
    def generate_storage_list_filter_product_id(cls, request, product_id, query_items):
        return helpers.StorageHelpers.storages_filter_format(
            [],
            services.StorageService.storage_id_name_filter_product_id(
                request,
                product_id,
            ),
            query_items,
        )

    @classmethod
    def generate_collection_list_filter_product_id(
        cls, request, products_ids, query_items
    ):
        return helpers.CollectionHelpers.collections_filter_format(
            [],
            services.CollectionService.collection_id_name_filter_product_id(
                request,
                products_ids,
            ),
            query_items,
        )

    @classmethod
    def generate_manufacturer_data_filter_manufacturer_id(
        cls, request, manufacturers_ids, query_items
    ):
        return helpers.ManufacturerHelpers.manufacturers_filter_format(
            [],
            services.ManufacturerService.manufacturer_id_name_type_filter_manufacturer_id(
                request,
                manufacturers_ids,
            ),
            query_items,
        )

    @classmethod
    def generate_manufacturer_list_filter_manufacturer_id(
        cls, request, manufacturers_ids, query_items
    ):
        return helpers.ManufacturerHelpers.manufacturers_filter_format(
            [],
            services.ManufacturerService.manufacturer_id_name_type_filter_manufacturer_id(
                request,
                manufacturers_ids,
            ),
            query_items,
        )

    @classmethod
    def generate_band_list_filter_products_ids(cls, request, product_list):
        return helpers.BandHelpers.product_band_format(
            services.ProductToBandService.product_id_band_id_filter_product_list(
                request,
                product_list,
            ),
        )

    @classmethod
    def generate_special_list_v2(cls, request, filter_params, query_items):
        return helpers.SpecialHelpers.specials_filter_format(
            services.SpecialService.special_id_name_filter_dynamic(
                request,
                filter_params,
            )
            or [],
            query_items,
        )

    @classmethod
    def generate_availability_list(cls, request):
        return helpers.AttributeHelpers.create_attribute_dict_id_value(
            services.AttributeService.attribute_id_value_filter_attribute_group_availability(
                request
            )
        )

    @classmethod
    def generate_availability_list_filter_id(cls, request, availability):
        return helpers.AttributeHelpers.create_attribute_dict_id_value(
            services.AttributeService.attribute_id_value_filter_id(
                request, availability
            )
        )

    @classmethod
    def product_to_category_create(cls, category_id, product_id):
        models.ProductToCategory(category_id=category_id, product_id=product_id)

    @classmethod
    def create_product_to_category(cls, request, form, category):
        if form.product_list.data:
            request.dbsession.add_all(
                [
                    models.ProductToCategory(
                        category_id=category.category_id, product_id=product_id
                    )
                    for product_id in form.product_list.data.split(",")
                ]
            )

    @classmethod
    def product_to_component_update(
        cls,
        request,
        product_id,
        product_to_product_old_elements,
        product_to_product_current_elements,
        relation_type,
    ):
        if product_to_product_current_elements or product_to_product_old_elements:
            cls.product_component_update(
                request, product_id, product_to_product_current_elements, relation_type
            )
            to_delete = helpers.CommonHelpers.generate_delete_list(
                [x.product_component_id for x in product_to_product_old_elements],
                [x.product_component_id for x in product_to_product_current_elements],
            )
            operations.CommonOperations.delete_from_database(
                request,
                [
                    item
                    for item in product_to_product_old_elements
                    if item.product_component_id in to_delete
                ],
            )

    @classmethod
    def product_to_product_option_update(
        cls,
        request,
        product_id,
        product_to_product_old_elements,
        product_to_product_current_elements,
        relation_type,
    ):
        if product_to_product_current_elements or product_to_product_old_elements:
            cls.product_option_update(
                request, product_id, product_to_product_current_elements, relation_type
            )
            to_delete = helpers.CommonHelpers.generate_delete_list(
                [x.product_component_id for x in product_to_product_old_elements],
                [x.product_component_id for x in product_to_product_current_elements],
            )
            operations.CommonOperations.delete_from_database(
                request,
                [
                    item
                    for item in product_to_product_old_elements
                    if item.product_component_id in to_delete
                ],
            )

    @classmethod
    def product_update_related_products(
        cls,
        request,
        product_id,
        product_to_product_old_elements,
        product_to_product_current_elements,
    ):
        if product_to_product_current_elements or product_to_product_old_elements:
            cls.product_related_update(
                request,
                product_id,
                product_to_product_old_elements,
                product_to_product_current_elements,
            )
            to_delete = helpers.CommonHelpers.generate_delete_list(
                [x.product2_id for x in product_to_product_old_elements],
                product_to_product_current_elements,
            )
            operations.CommonOperations.delete_from_database(
                request,
                [
                    item
                    for item in product_to_product_old_elements
                    if item.product_component_id in to_delete
                ],
            )

    @classmethod
    def product_to_special_update(
        cls,
        request,
        product_id,
        product_to_special_old_elements,
        product_to_special_current_elements,
        price_input_setting,
        tax_value,
        storage_summary,
    ):
        if product_to_special_current_elements or product_to_special_old_elements:
            # There is no need for add function, all specials are added by an AJAX request
            cls.product_special_update(
                request,
                product_id,
                product_to_special_current_elements,
                price_input_setting,
                tax_value,
                storage_summary,
            )
            to_delete = helpers.CommonHelpers.generate_delete_list(
                [x.product_special_id for x in product_to_special_old_elements],
                [x.product_special_id for x in product_to_special_current_elements],
            )
            operations.CommonOperations.delete_from_database(
                request,
                [
                    item
                    for item in product_to_special_old_elements
                    if item.product_special_id in to_delete
                ],
            )
            operations.SpecialOperations.set_lowest_price_single_product(
                request, product_id
            )

    @classmethod
    def product_to_featured_update(
        cls,
        request,
        product_id,
        product_to_featured_old_elements,
        product_to_featured_current_elements,
    ):
        if product_to_featured_current_elements or product_to_featured_old_elements:
            cls.product_featured_update(
                request,
                product_id,
                product_to_featured_current_elements,
            )
            to_delete = helpers.CommonHelpers.generate_delete_list(
                [x.product_special_id for x in product_to_featured_old_elements],
                [x.product_special_id for x in product_to_featured_current_elements],
            )
            operations.CommonOperations.delete_from_database(
                request,
                [
                    item
                    for item in product_to_featured_old_elements
                    if item.product_special_id in to_delete
                ],
            )

    @classmethod
    def get_catalog_price_using_shop_settings(
        cls, settings_price_input, product_tax_value, product
    ):
        if settings_price_input == 1:
            product.taxed_price = (
                operations.ProductOperations.return_price_based_on_tax_system_settings(
                    product_tax_value, product.catalog_price
                )
            )
        return product

    @classmethod
    def get_catalog_special_prices_using_shop_settings(
        cls, settings_price_input, product_tax_value, product_to_special_list
    ):
        if settings_price_input == 1:
            for item in product_to_special_list:
                item.taxed_price = operations.ProductOperations.return_price_based_on_tax_system_settings(
                    product_tax_value, item.price
                )
        return product_to_special_list

    @classmethod
    def formatted_featured_products_v2(
        cls,
        request,
        data_options,
        data_options_filtered,
        number_elements,
        image_settings,
    ):
        return data_containers.SellingOptions(
            featured_products=helpers.SpecialHelpers.special_products_v2(
                # Add active special types - they will form promotion blocks
                data_options.special_dict,
                # Find special product and format them
                helpers.ProductHelpers.product_category_format(
                    # Required by images paths and price calculation based on taxes
                    data_options,
                    # Required by images paths
                    data_options_filtered,
                    # Find special products
                    services.ProductService.special_products_filter_date_active_v2(
                        request,
                        None,
                    ),
                    image_settings,
                ),
                number_elements,
            ),
        )

    @classmethod
    def products_related_to_current_view(
        cls,
        request,
        all_related_products,
        product,
        data_options,
        data_options_filtered,
        number_elements,
        reverse_relation_products,
        settings,
        current_view="product",
    ):
        return data_containers.SellingOptions(
            containing_product_sets=helpers.ProductHelpers.product_category_format(
                data_options, data_options_filtered, reverse_relation_products, settings
            )
            if settings["product_settings"].get("display_containing_product_sets", 0)
            == 1
            else [],
            featured_products=helpers.SpecialHelpers.special_products_v2(
                data_options.special_dict,
                helpers.ProductHelpers.product_category_format(
                    data_options,
                    data_options_filtered,
                    services.ProductService.special_products_filter_date_active_v2(
                        request, product.product_id
                    ),
                    settings["image_settings"],
                ),
                number_elements,
            )
            if settings["product_settings"].get("display_featured_products", 0) == 1
            else {},
            product_options=helpers.ProductHelpers.product_option_format(
                data_options,
                data_options_filtered,
                [
                    item
                    for item in all_related_products
                    if item.product_relation_type == 3
                ],
                settings["image_settings"],
            )
            if settings["product_settings"].get("display_product_options", 0) == 1
            else [],
            products_common_band=helpers.ProductHelpers.product_category_format(
                data_options,
                data_options_filtered,
                services.ProductService.products_filter_band_date_product(
                    request, list(data_options.band_dict), product.product_id, settings
                ),
                settings["image_settings"],
            )
            if settings["product_settings"].get("display_products_common_band", 0) == 1
            else {},
            products_common_collections=helpers.ProductHelpers.other_products_from_common_collections(
                helpers.CollectionHelpers.collections_related_format(
                    services.CollectionService.collection_id_name_filter_collection_list(
                        request, data_options.collection_dict.keys(), settings
                    )
                ),
                helpers.ProductHelpers.product_category_format(
                    data_options,
                    data_options_filtered,
                    services.ProductService.products_filter_collection_date(
                        request,
                        data_options.collection_dict.keys(),
                        product.product_id,
                    ),
                    settings["image_settings"],
                ),
                product.product_id,
            )
            if settings["product_settings"].get(
                "display_products_common_collections", 0
            )
            == 1
            else {},
            related_products=helpers.ProductHelpers.product_category_format(
                data_options,
                data_options_filtered,
                [
                    item
                    for item in all_related_products
                    if item.product_relation_type == 1
                ],
                settings["image_settings"],
            )
            if product.product_type == 1
            and settings["product_settings"].get("display_related_products", 0) == 1
            else [],
            similar_products=helpers.ProductHelpers.product_category_format(
                data_options,
                data_options_filtered,
                [
                    item
                    for item in all_related_products
                    if item.product_relation_type == 2
                ],
                settings["image_settings"],
            )
            if product.product_type == 0
            and settings["product_settings"].get("display_similar_products", 0) == 1
            else [],
            similar_collections=services.CollectionService.collection_array_filter_colection_list(
                request, data_options.collection_dict.keys(), settings
            )
            if settings["product_settings"].get("display_similar_collections", 0) == 1
            else [],
        )

    @classmethod
    def return_product(cls, request):
        product_id = int(request.matchdict["id"])
        product_slug = str(request.matchdict["seo_slug"])
        if not product_id:
            raise HTTPNotFound("No such page (missing product_id)")
        product = services.ProductService.product_with_description_filter_product_id(
            request, product_id
        )
        if not product or product.seo_slug != product_slug:
            raise HTTPNotFound(
                "No such page (no product matches given product_id or wrong seo_slug)"
            )
        return product

    @classmethod
    def return_decimal_dict_values_for_new_items(cls, request):
        new_items = {}
        for elem in request.params.getall("product_id"):
            updated_product = services.ProductService.product_minimum_unit(
                request, elem
            )
            cart_quantity_real_unit = Decimal(
                request.params.get(f"cart_quantity_real_unit_{elem}", 0)
            )
            unit = Decimal(
                request.params.get(
                    f"cart_quantity_real_unit_{elem}", updated_product.unit
                )
            )
            cart_quantity_real_unit = (
                Decimal(
                    updated_product.unit
                    * (math.ceil(cart_quantity_real_unit / updated_product.unit))
                )
                if not math.isnan(cart_quantity_real_unit)
                else updated_product.unit
            )
            new_items[str(elem)] = {
                "cart_quantity_real_unit": cart_quantity_real_unit,
                "product_single_set": request.params.get(f"product_single_set_{elem}"),
                "cart_quantity": math.ceil(
                    cart_quantity_real_unit / updated_product.unit
                ),
            }
        return new_items

    @classmethod
    def return_product_related_data(cls, request, product_id_list, tax_dict):
        special_filtered_dict = helpers.ProductHelpers.featured_products_format_dict(
            services.ProductToSpecialService.special_rows_filter_product_list_date(
                request,
                product_id_list,
            ),
            tax_dict,
        )
        product_id_list = list(product_id_list) + list(
            special_filtered_dict.keys()
        )  # list(product_id_list) + list(special_filtered.keys())
        return data_containers.DataOptionsFiltered(
            author_filtered=(
                helpers.AuthorHelpers.product_author_format(
                    services.ProductToAuthorService.product_id_author_id_filter_product_list(
                        request,
                        product_id_list,
                    )
                )
            ),
            band_filtered=(
                helpers.BandHelpers.product_band_format(
                    services.ProductToBandService.product_id_band_id_filter_product_list(
                        request,
                        product_id_list,
                    )
                )
            ),
            collection_filtered=(
                helpers.ProductHelpers.product_collection_format(
                    services.ProductService.product_id_collectons_manufacturer_avaliability_filter_product_list(
                        request,
                        product_id_list,
                    )
                )
            ),
            special_filtered_dict=special_filtered_dict,
            storage_filtered=helpers.ProductHelpers.product_storage_format(
                services.ProductService.product_id_storage_id_quantity_filter_product_list(
                    request,
                    product_id_list,
                )
            ),
            transport_filtered=helpers.ProductHelpers.product_transport_format(
                services.ProductToTransportService.product_transport_slot_settings_filter_product_list(
                    request,
                    product_id_list,
                )
            ),
            payment_dict=helpers.PaymentHelpers.generate_payment_container(
                services.PaymentService.payment_excerpt_name_status_visibility_filter_status(
                    request,
                )
            ),
        )

    @classmethod
    def return_cart_related_data(cls, request, product_id_list, tax_dict):
        special_filtered_dict = helpers.ProductHelpers.featured_products_format_dict(
            services.ProductToSpecialService.special_rows_filter_product_list_date(
                request, product_id_list
            ),
            tax_dict,
        )
        return data_containers.CartDataOptions(
            band_filtered=(
                helpers.BandHelpers.product_band_format(
                    services.ProductToBandService.product_id_band_id_filter_product_list(
                        request,
                        product_id_list,
                    )
                )
            ),
            collection_filtered=(
                helpers.ProductHelpers.product_collection_format(
                    services.ProductService.product_id_collectons_manufacturer_avaliability_filter_product_list(
                        request,
                        product_id_list,
                    )
                )
            ),
            payment_filtered=helpers.PaymentHelpers.generate_payment_container(
                services.PaymentService.payment_excerpt_name_status_visibility_filter_status(
                    request,
                )
            ),
            special_filtered_dict=special_filtered_dict,
            storage_filtered=helpers.ProductHelpers.product_storage_format(
                services.ProductService.product_id_storage_id_quantity_filter_product_list(
                    request,
                    product_id_list,
                )
            ),
            # Required by the cart settings merge!
            transport_filtered=helpers.ProductHelpers.product_transport_format(
                services.ProductToTransportService.product_transport_slot_settings_filter_product_list(
                    request,
                    product_id_list,
                )
            ),
            # transport_filtered=helpers.TransportHelpers.transport_with_product_local_settings_format(
            #     services.ProductToTransportService.product_transport_slot_settings_filter_product_list(
            #         request, product_id_list, settings
            #     ),
            # ),
        )

    @classmethod
    def update_stock(cls, merged_dicts, current_products):
        for item in current_products:
            if merged_dicts.get(item.product_id):
                item.quantity = merged_dicts[item.product_id]["quantity"]
