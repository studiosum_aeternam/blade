from ast import literal_eval
import os
from pathlib import Path
import shutil
import time
from webob.multidict import MultiDict

from emporium import helpers, services, views


class CommonOperations(object):
    @classmethod
    def generate_image_upload_folders(cls, manufacturer_slug):
        """
        Uploading images requires creating a backup and two separated folders.
        Thumbnails and fullsized images.
        """
        images_paths = ("source/", "thumbnails/", "base/")
        for image_format in images_paths:
            newpath = str(
                Path(helpers.CommonHelpers.asset_path(image_format) + manufacturer_slug)
            )
            if not os.path.exists(newpath):
                try:
                    os.makedirs(newpath)
                except OSError:
                    print("Can't create folder for image upload")

    @classmethod
    def generate_file_upload_folders(cls, file_path_slug, upload_type):
        """
        Uploading files might require creating a new folder.
        """
        newpath = str(
            Path(
                helpers.CommonHelpers.asset_path(
                    "",
                    asset_type="file",
                    upload_type=upload_type,
                )
                + file_path_slug
            )
        )
        if not os.path.exists(newpath):
            try:
                os.makedirs(newpath)
            except OSError:
                print("Can't create folder for file upload")

    @classmethod
    def rename_image_upload_folders(cls, old_slug, new_slug):
        """
        Uploading images requires creating a backup and two separated folders.
        Thumbnails and fullsized images.
        """
        images_paths = ("source/", "thumbnails/", "base/")
        for image_format in images_paths:
            oldpath = str(
                Path(helpers.CommonHelpers.asset_path(image_format) + old_slug)
            )
            newpath = str(
                Path(helpers.CommonHelpers.asset_path(image_format) + new_slug)
            )
            if os.path.exists(oldpath):
                try:
                    os.rename(oldpath, newpath)
                except OSError:
                    print("Can't rename folder")
            else:
                try:
                    os.makedirs(newpath)
                except OSError:
                    print("Can't create folder for image upload")

    @classmethod
    def request_params_are_multidict(cls, request):
        """
        Integration tests have problems with passing MultiDict
        """
        if isinstance(request.params, dict):
            request.params = MultiDict(request.params)
        return request

    @classmethod
    def delete_image_upload_folders(cls, folder_path):
        """
        Uploading images requires creating a backup and two separated folders
        for thumbnails and fullsized images.
        """
        images_paths = ("source/", "thumbnails/", "base/")
        for image_format in images_paths:
            image_full_path = str(
                Path(helpers.CommonHelpers.asset_path(image_format) + folder_path)
            )
            if os.path.exists(image_full_path):
                try:
                    shutil.rmtree(image_full_path)
                except FileNotFoundError:
                    print("Remove dir error")

    @classmethod
    def delete_files(cls, items_to_delete):
        for item in items_to_delete:
            file_path = str(Path(helpers.CommonHelpers.asset_path(item.file_path)))
            if os.path.exists(file_path):
                try:
                    os.remove(file_path)
                except FileNotFoundError:
                    print("File not found")

    @classmethod
    def delete_from_database(cls, request, removal_list):
        if removal_list:
            for item in removal_list:
                request.dbsession.delete(item)

    @classmethod
    def generate_mail_body(cls, request, mail_contents, mail_params):
        cls.write_mail_body_to_file(request, mail_contents, mail_params)
        return cls.read_mail_body(mail_params["mail_type"], mail_contents["uuid"])

    @classmethod
    def write_mail_body_to_file(cls, request, mail_contents, mail_params):
        query = list(mail_contents.items())
        mail_types = {
            "update": request.route_url(
                "s_update_mail",
                _query=query,
            ),
            "payment": request.route_url(
                "s_payment_link_mail",
                _query=query,
            ),
            "order": request.route_url(
                "s_order_mail",
                id=mail_contents.get("id"),
                _query=query,
            ),
        }
        with open(
            helpers.CommonHelpers.asset_path("tmp", asset_type="tmp")
            + mail_params["mail_type"]
            + "_"
            + mail_contents["uuid"]
            + ".html",
            "w+b",
        ) as handle:
            kwargs = {"cookies": request.cookies, "host": request.host}
            link = mail_types[mail_params["mail_type"]]
            mail_subrequest = request.blank(link, **kwargs)
            mail_response = request.invoke_subrequest(mail_subrequest, use_tweens=True)
            handle.write(mail_response.body)

    @classmethod
    def read_mail_body(
        cls,
        mail_type,
        order_uuid,
    ):
        return Path(
            helpers.CommonHelpers.asset_path("tmp", asset_type="tmp")
            + mail_type
            + "_"
            + order_uuid
            + ".html"
        ).read_text()

    @classmethod
    def send_email(cls, request, route, query):
        link = request.route_url(
            route,
            _query=query,
        )
        kwargs = {"cookies": request.cookies, "host": request.host}
        response = request.invoke_subrequest(
            request.blank(link, **kwargs), use_tweens=True
        )

    @classmethod
    def parse_byte_to_dict(cls, bytedata):
        return literal_eval(str(bytedata.decode("utf-8")))
