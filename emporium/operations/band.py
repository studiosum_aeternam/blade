from emporium import services, data_containers


class BandOperations(object):
    @classmethod
    def update_social_links(cls, request, settings):
        social_links = {}
        if settings.get("social_links"):
            for item in settings["social_links"]:
                if request.POST.get(f"social_links_{item}"):
                    social_links[item] = request.POST.get(f"social_links_{item}")
        return social_links
