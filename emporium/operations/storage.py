import itertools
from copy import copy
from decimal import Decimal
from operator import methodcaller
from dataclasses import asdict
import math

from emporium import data_containers, helpers, operations, services, models


class StorageOperations(object):
    @classmethod
    def shelve_special_products(cls, request, order):
        if special_products := (
            services.ProductToSpecialService.special_products_filter_product_list_date_price(
                request, [item["product_id"] for item in order.products_json]
            )
        ):
            cls.move_special_products_from_storage_to_shelve(
                request, order, special_products
            )

    @classmethod
    def move_special_products_from_storage_to_shelve(
        cls,
        request,
        order,
        special_products,
    ):
        """
        Every product can have multiple promotions, they are quantity limited.
        After code testing refactor it.
        """
        for product in order.products_json:
            cart_quantity_real_unit = int(product["cart_quantity"])
            for special in special_products:
                if (
                    product["product_id"] == special.product_id
                ) and cart_quantity_real_unit:
                    stored_cart_quantity = copy(special.quantity)
                    if cart_quantity_real_unit >= special.quantity:
                        cart_quantity_real_unit -= special.quantity
                        special.quantity = 0
                    else:
                        stored_cart_quantity = copy(cart_quantity_real_unit)
                        special.quantity -= cart_quantity_real_unit
                        cart_quantity_real_unit = 0
                    request.dbsession.add(
                        models.ShelveSpecial(
                            order_id=order.order_id,
                            product_special_id=special.product_special_id,
                            quantity=stored_cart_quantity,
                        )
                    )

    @classmethod
    def shelve_purchased_products(
        cls,
        request,
        order,
        product_storage,
    ):
        """
        Every product can be stored in multiple storages. When the order is processed
        remove the quantity ordered from the storage.
        """
        for product in order.products_json:
            if product.get("product_type") == 0:
                cls.move_products_from_storage_to_shelve(
                    request,
                    order.order_id,
                    product,
                    product_storage[product["product_id"]],
                )
                cls.change_product_availability(
                    request,
                    product["product_id"],
                    sum(
                        storage.quantity
                        for storage in product_storage[product["product_id"]]
                    ),
                )

    @classmethod
    def move_products_from_storage_to_shelve(
        cls, request, order_id, product, product_storage
    ):
        for storage in product_storage:
            if (
                product.get("product_type") == 0 and product["cart_quantity"] > 0
            ):  # Product must be physical and the number of ordered products must be grater than 0
                stored_cart_quantity = copy(storage.quantity)
                if product["cart_quantity"] >= storage.quantity:
                    product["cart_quantity"] -= storage.quantity
                    storage.quantity = 0  # Updates DB?
                else:
                    stored_cart_quantity = copy(product["cart_quantity"])
                    storage.quantity -= product["cart_quantity"]  # Updates DB?
                    product["cart_quantity"] = 0
                request.dbsession.add(
                    models.ShelveStorage(
                        order_id=order_id,
                        storage_id=storage.storage_id,
                        quantity=stored_cart_quantity,
                    )
                )

    @classmethod
    def remove_products_from_storage(
        cls,
        request,
        order,
        product_storage,
    ):
        """
        Every product can be stored in multiple storages. When the order is processed
        remove the quantity ordered from the storage.
        """
        for product in order.products_json:
            if product.get("product_type") == 0:  # Physical product
                for storage in product_storage[product["product_id"]]:
                    if product["cart_quantity"] >= storage.quantity:
                        product["cart_quantity"] -= storage.quantity
                        storage.quantity = 0  # Updates DB?
                    else:
                        product["cart_quantity"] = 0
                        storage.quantity -= product["cart_quantity"]
                cls.change_product_availability(
                    request,
                    product["product_id"],
                    sum(
                        storage.quantity
                        for storage in product_storage[product["product_id"]]
                    ),
                )

    @classmethod
    def change_product_availability(cls, request, product_id, summarized_storage):
        """
        Every product can be stored in multiple storages. When the order is processed
        remove the quantity ordered from the storage.
        """
        if summarized_storage == 0:
            product = services.ProductService.product_filter_product_id(
                request, product_id
            )
            product.availability_id = 53
            cls.remove_product_from_monetary_promotions(request, product_id)

    @classmethod
    def remove_product_from_monetary_promotions(cls, request, product_id):
        monetary_promotions = services.ProductToSpecialService.product_special_id_filter_monetary_product_id(
            request, product_id
        )
        cls.set_promotion_quantity_to_null(request, monetary_promotions)

    @classmethod
    def set_promotion_quantity_to_null(
        cls,
        request,
        item_list,
    ):
        if item_list:
            for item in item_list:
                item.quantity = 0

    @classmethod
    def restore_shelved_order_storage_products(
        cls,
        request,
        order_special,
        special_products,
    ):
        """
        When order is cancelled restore quantities promoted products
        """
        order_dict = {order.product_special_id: item in order_special}
        if order_special:
            for item in special_products:
                if order_dict.get(item.product_special_id):
                    item.quantity += order_dict.get(item.product_special_id).quantity

    @classmethod
    def update_product_status_and_storage_info(
        cls,
        request,
        order,
        settings,
    ):
        """
        Update storages and remove shelved promoted products. Right now
        boh actions are consecutive but this method allows separated operation.
        """
        product_storage = services.ProductService.product_id_storage_id_quantity_filter_product_list_physical_storage(
            request,
            [item["product_id"] for item in order.products_json],
        )
        storage_dict = {}
        for item in product_storage:
            if not storage_dict.get(item.product_id):
                storage_dict[item.product_id] = []
            storage_dict[item.product_id].append(item)
        if (
            settings.get("storage_settings")
            and settings["storage_settings"]["update_storage_during_checkout"] == "1"
            and storage_dict
        ):
            cls.shelve_special_products(request, order)
            cls.shelve_purchased_products(request, order, storage_dict)
            cls.remove_products_from_storage(request, order, storage_dict)
            helpers.CacheHelpers.flush_cache()
