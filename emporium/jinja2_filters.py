import random
import json


def filter_shuffle(seq):
    try:
        result = list(seq)
        random.shuffle(result)
        return result
    except:
        return seq


def number_comma(number):
    try:
        return str(number).replace(".", ",")
    except:
        return number


def number_dot(number):
    try:
        return str(number).replace(",", ".")
    except:
        return number


def collection_json(seq):
    try:
        return json.dumps(seq)
    except:
        return seq
